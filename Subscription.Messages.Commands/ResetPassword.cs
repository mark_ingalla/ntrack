﻿using nTrack.Core.Messages;
using System;
using System.ComponentModel.DataAnnotations;

namespace Subscription.Messages.Commands.Account.User
{
    public class ResetPassword : MessageBase
    {
        public Guid ResetPasswordToken { get; set; }

        [Required]
        public string NewPassword { get; set; }
    }
}
