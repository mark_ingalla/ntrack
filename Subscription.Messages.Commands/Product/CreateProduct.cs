using nTrack.Core.Messages;
using System;

namespace Subscription.Messages.Commands.Product
{
    public class CreateProduct : MessageBase
    {
        public Guid ProductId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string AccessUrl { get; set; }
        public decimal BasePrice { get; set; }
    }
}