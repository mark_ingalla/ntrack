using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Product
{
    public class RevokePermissionProduct : MessageBase
    {
        public Guid ProductId { get; set; }
        public string PermissionCode { get; set; }
    }
}