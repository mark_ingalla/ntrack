﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Product
{
    public class UpdateOfferFeatures : MessageBase
    {
        public Guid ProductId { get; set; }
        public Guid OfferKey { get; set; }
        public List<OfferFeature> Features { get; set; }
    }
}
