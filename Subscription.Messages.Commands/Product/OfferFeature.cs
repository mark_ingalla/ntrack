﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Subscription.Messages.Commands.Product
{
    public class OfferFeature
    {
        public string Code { get; set; }
        public int Limit { get; set; }
        public bool IsIncluded { get; set; }
    }
}
