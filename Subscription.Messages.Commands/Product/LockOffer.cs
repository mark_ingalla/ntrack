using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Product
{
    public class LockOffer : MessageBase
    {
        public Guid ProductId { get; set; }
        public Guid OfferKey { get; set; }
    }
}