using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Product
{
    public class ActivateFeature : MessageBase
    {
        public Guid ProductId { get; set; }
        public string FeatureCode { get; set; }
    }
}