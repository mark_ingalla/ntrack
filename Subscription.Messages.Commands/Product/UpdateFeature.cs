using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Product
{
    public class UpdateFeature : MessageBase
    {
        public Guid ProductId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Limit { get; set; }
    }
}