﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Product
{
    public class UpdatePermissionProduct : MessageBase
    {
        public Guid ProductId { get; set; }
        public string PermissionCode { get; set; }
        public string PermissionName { get; set; }
    }
}
