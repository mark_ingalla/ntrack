using System;
using System.Collections.Generic;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Product
{
    public class AddOfferFeatures : MessageBase
    {
        public Guid ProductId { get; set; }
        public Guid OfferKey { get; set; }
        //public string[] FeatureCodes { get; set; }
        public List<OfferFeature> Features { get; set; }
    }
}