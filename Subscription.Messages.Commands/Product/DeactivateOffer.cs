using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Product
{
    public class DeactivateOffer : MessageBase
    {
        public Guid ProductId { get; set; }
        public Guid OfferKey { get; set; }
        public string Reason { get; set; }

    }
}