﻿using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands
{
    public class LogOut : MessageBase
    {
        public Guid SessionToken { get; set; }
    }
}
