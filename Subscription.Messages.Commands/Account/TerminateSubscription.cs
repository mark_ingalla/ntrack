using nTrack.Core.Messages;
using System;

namespace Subscription.Messages.Commands.Account
{
    public class TerminateSubscription : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid SubscriptionKey { get; set; }
    }
}