﻿using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Account
{
    public class AssignExistingUserToAccount : MessageBase
    {
        public Guid InvitationToken { get; set; }
    }
}
