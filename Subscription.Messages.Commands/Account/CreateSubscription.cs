﻿using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Account
{
    public class CreateSubscription : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid ProductId { get; set; }
        public Guid OfferKey { get; set; }
        public Guid SubscriptionKey { get; set; }
        public int OfferQTY { get; set; }
    }
}
