﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Account.Role
{
    public class AddRole : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid RoleId { get; set; }
        public string Name { get; set; }
        public Dictionary<Guid, List<string>> Permissions { get; set; }
    }
}
