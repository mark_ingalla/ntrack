﻿using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Account.User
{
    public class ChangeUserRole : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid UserKey { get; set; }
        public UserRole Role { get; set; }
    }
}