﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Account.User
{
    public class RetrieveUser : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid UserKey { get; set; }
    }
}
