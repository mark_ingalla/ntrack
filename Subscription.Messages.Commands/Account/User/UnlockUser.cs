using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Account.User
{
    public class UnlockUser : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid UserKey { get; set; }
        public string Reason { get; set; }
    }
}