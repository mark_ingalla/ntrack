﻿using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Account
{
    public class DeactivateAccount : MessageBase
    {
        public Guid AccountId { get; set; }
        public string Reason { get; set; }
    }
}