﻿using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Account
{
    public class RegisterInvitedUser : MessageBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Password { get; set; }
        public Guid InvitationToken { get; set; }
    }
}
