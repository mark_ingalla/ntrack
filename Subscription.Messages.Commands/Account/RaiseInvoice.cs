﻿using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Account
{
    public class RaiseInvoice : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid InvoiceId { get; set; }
    }
}