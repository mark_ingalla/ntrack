﻿using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands
{
    public class LoginUser : MessageBase
    {
        public Guid SessionToken { get; set; }
        public Guid UserId { get; set; }
        public Guid AccountId { get; set; }
    }
}
