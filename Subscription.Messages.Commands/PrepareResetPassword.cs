﻿using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands
{
    public class PrepareResetPassword : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid UserKey { get; set; }
        public string UserEmail { get; set; }
        public string UserFullname { get; set; }

        public Guid ResetPasswordToken { get; set; }
        public string ResetPasswordLink { get; set; }
    }
}
