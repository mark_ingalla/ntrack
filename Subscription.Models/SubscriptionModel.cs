﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Subscription.Models
{
    public class SubscriptionModel
    {
        public string Id { get; set; }
        public Guid SubscriptionId { get; set; }
        public Guid AccountId { get; set; }
        public Guid ProductId { get; set; }

        public List<FeatureModel> Features { get; set; }
    }
}
