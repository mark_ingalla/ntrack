﻿using System.Collections.Generic;

namespace Subscription.Models
{
    public class CountryListModel
    {
        public string Id { get; set; }
        public Dictionary<string,string> Countries { get; set; }
    }
}
