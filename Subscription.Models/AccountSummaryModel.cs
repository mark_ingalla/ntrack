using System;

namespace Subscription.Models
{
    public class AccountSummaryModel
    {
        public string Id { get; set; }
        public Guid AccountId { get; set; }
    }
}