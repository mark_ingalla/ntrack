﻿using System;
using System.Collections.Generic;

namespace Subscription.Models
{
    public class AccountModel
    {
        public string Id { get; set; }

        public Guid AccountId { get; set; }

        public string CompanyName { get; set; }

        public string CompanyPhone { get; set; }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }

        public string CompanySubrub { get; set; }

        public string CompanyState { get; set; }

        public string CompanyPostcode { get; set; }

        public string CompanyCountry { get; set; }

        public string IPAddress { get; set; }
        public string EmailVerificationPageUrl { get; set; }

        public Dictionary<Guid, string> Users { get; set; }
    }
}
