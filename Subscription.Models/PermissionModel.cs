﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Subscription.Models
{
    public class PermissionModel
    {
        public string Id { get; set; }
        public String Name { get; set; }
    }
}
