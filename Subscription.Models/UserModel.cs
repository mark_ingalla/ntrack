using System;
using System.Collections.Generic;

namespace Subscription.Models
{
    public class UserModel
    {
        public List<AccountModel> Accounts { get; set; }

        public string Id { get; set; }
        
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }

        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        public List<Guid> Roles { get; set; }

        public override string ToString()
        {
            return FirstName + " " + LastName;
        }
    }
}