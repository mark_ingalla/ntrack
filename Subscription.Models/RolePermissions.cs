﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Subscription.Models
{
    public class RolePermissions
    {
        public string Id { get; set; }
        public Guid RoleId { get; set; }
        public String Name { get; set; }
        public List<PermissionModel> Permissions { get; set; }
    }
}
