﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Subscription.Models
{
    public class ProductPermissions
    {
        public string Id { get; set; }
        public Guid ProductId { get; set; }
        public String ProductName { get; set; }
        public List<PermissionModel> Permissions { get; set; }
    }
}
