namespace Subscription.Models
{
    public class FeatureModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}