﻿using System;
using System.Collections.Generic;

namespace Subscription.Models
{
    public class ProductDetailsModel
    {
        public string Id { get; set; }
        public Guid ProductId { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public String LargeImageUrl { get; set; }
        public List<FeatureModel> Features { get; set; }

        public List<PermissionModel> Permissions { get; set; }
    }
}
