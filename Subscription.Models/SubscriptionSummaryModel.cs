﻿using System;

namespace Subscription.Models
{
    public class SubscriptionSummaryModel
    {
        public string Id { get; set; }
        public Guid SubscriptionId { get; set; }
        public Guid AccountId { get; set; }
        public Guid ProductId { get; set; }
    }
}
