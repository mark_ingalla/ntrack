﻿using System;

namespace Subscription.Models
{
    public class ProductSummaryModel
    {
        public string Id { get; set; }
        public Guid ProductId { get; set; }
        public String Name { get; set; }
        public String ThumbnailImageUrl { get; set; }
    }
}
