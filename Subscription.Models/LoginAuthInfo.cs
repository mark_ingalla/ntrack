using System;
using System.Collections.Generic;

namespace Subscription.Models
{
    public class LoginAuthInfo
    {
        public List<AccountSummaryModel> Accounts;

        public Guid UserId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}