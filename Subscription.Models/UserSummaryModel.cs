using System;

namespace Subscription.Models
{
    public class UserSummaryModel
    {
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}