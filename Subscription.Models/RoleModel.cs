using System;
using System.Collections.Generic;

namespace Subscription.Models
{
    public class RoleModel
    {
        public string Id { get; set; }
        public Guid RoleId { get; set; }
        public string Name { get; set; }

        public List<Guid> UserIds { get; set; }
        public List<PermissionModel> Permissions { get; set; }
    }
}