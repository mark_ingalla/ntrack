﻿using System;

namespace Subscription.Models
{
    public class RoleSummaryModel
    {
        public string Id { get; set; }
        public Guid RoleId { get; set; }
        public String Name { get; set; }
        public Guid AccountId { get; set; }
    }
}
