﻿using System;

namespace Subscription.Models
{
    public class ActiveUserSession
    {
        public string Id { get; set; }
        public Guid SessionToken { get; set; }
        public Guid UserId { get; set; }
        public bool IsActive { get; set; }
    }
}
