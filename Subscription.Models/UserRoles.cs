﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Subscription.Models
{
    public class UserRoles
    {
        public string Id { get; set; }
        public Guid UserId { get; set; }
        public List<Guid> RolesIds { get; set; }
    }
}
