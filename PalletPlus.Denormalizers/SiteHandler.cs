﻿using System;
using NServiceBus;
using nTrack.Data.Extenstions;
using nTrack.Extension;
using PalletPlus.Data;
using PalletPlus.Messages.Events.Site;
using Raven.Client;
using Raven.Client.Connection;

namespace PalletPlus.Denormalizers
{
    public class SiteHandler :
        IHandleMessages<SiteCreated>,
        IHandleMessages<SiteUpdated>,
        IHandleMessages<SiteDeleted>,
        IHandleMessages<SiteUndeleted>,
        IHandleMessages<SupplierToSiteAdded>,
        IHandleMessages<SiteSupplierUpdated>,
        IHandleMessages<SupplierFromSiteRemoved>,
        IHandleMessages<PartnerForSiteEnabled>,
        IHandleMessages<PartnerForSiteDisabled>,
        IHandleMessages<UserToSiteAdded>,
        IHandleMessages<UserFromSiteRemoved>,
        IHandleMessages<PTAInfoUpdated>
    {
        readonly IDocumentSession _session;
        readonly IDatabaseCommands _storeCommands;

        public SiteHandler(IDocumentSession session, IBus bus)
        {
            _session = session;

            var databaseName = bus.GetDatabaseName();
            _storeCommands = _session.Advanced.DocumentStore.DatabaseCommands.ForDatabase(databaseName);
        }

        public void Handle(SiteCreated message)
        {
            var existing = _storeCommands.Head(message.SiteId.ToStringId<Site>());
            if (existing != null)
                throw new Exception("Site already exists!");

            _session.Store(new Site
                               {
                                   Id = message.SiteId,
                                   AccountId = message.AccountId.ToStringId<Account>(),
                                   AddressLine1 = message.AddressLine1,
                                   AddressLine2 = message.AddressLine2,
                                   Postcode = message.Postcode,
                                   State = message.State,
                                   Suburb = message.Suburb,
                                   CompanyName = message.Name,
                                   Country = message.Country
                               });


        }
        public void Handle(SiteUpdated message)
        {
            var site = _session.Load<Site>(message.SiteId);

            site.AddressLine1 = message.AddressLine1;
            site.AddressLine2 = message.AddressLine2;
            site.CompanyName = message.SiteName;
            site.Suburb = message.Suburb;
            site.State = message.State;
            site.Postcode = message.Postcode;
            site.Country = message.Country;


        }
        public void Handle(SiteDeleted message)
        {
            var site = _session.Load<Site>(message.SiteId);
            site.IsDeleted = true;

            //Task.Factory.StartNew(() => _store.Patch(message.SiteId.ToStringId<Site>(),
            //             new ScriptedPatchRequest
            //             {
            //                 Script = @"this.IsDeleted = true;"
            //             }));
        }
        public void Handle(SiteUndeleted message)
        {
            var site = _session.Load<Site>(message.SiteId);
            site.IsDeleted = false;

            //Task.Factory.StartNew(() => _store.Patch(message.SiteId.ToStringId<Site>(),
            //             new ScriptedPatchRequest
            //             {
            //                 Script = @"this.IsDeleted = false;"
            //             }));
        }
        public void Handle(SupplierToSiteAdded message)
        {
            var site = _session.Load<Site>(message.SiteId);
            site.Suppliers.Add(message.SupplierId.ToStringId<Supplier>(),
                               new CompanySupplier
                                   {
                                       CompanyId = message.SupplierId.ToStringId<Supplier>(),
                                       CompanyName = message.SupplierName,
                                       AccountNumber = message.AccountNumber,
                                       Prefix = message.Prefix,
                                       SequenceNumber = message.SequenceStartNumber,
                                       Suffix = message.Suffix
                                   });


        }

        public void Handle(SiteSupplierUpdated message)
        {
            var site = _session.Load<Site>(message.SiteId);
            var supplier = site.Suppliers[message.SupplierId.ToStringId<Supplier>()];
            supplier.Prefix = message.Prefix;
            supplier.Suffix = message.Suffix;
            supplier.SequenceNumber = message.SequenceStartNumber;


        }

        public void Handle(SupplierFromSiteRemoved message)
        {
            var site = _session.Load<Site>(message.SiteId);
            site.Suppliers.Remove(message.SupplierId.ToStringId<Supplier>());


        }
        public void Handle(PartnerForSiteEnabled message)
        {
            var partner = _session.Load<Partner>(message.PartnerId);
            partner.Sites.Add(message.SiteId.ToStringId<Site>());

            //            var siteIdString = message.SiteId.ToStringId<Site>();

            //Task.Factory.StartNew(() => _store.Patch(message.PartnerId.ToStringId<Partner>(),
            //                                         new[]{
            //                                                 new PatchRequest
            //                                                     {
            //                                                         Name = "Sites",
            //                                                         Type = PatchCommandType.Add,
            //                                                         Value =
            //                                                             RavenJToken.FromObject(siteIdString)
            //                                                     }
            //                                             })).Wait();
            //var site = _session.Load<Site>(message.SiteId);

            //site.Partners.Add(message.PartnerId.ToStringId<Partner>());

            //TODO: should be do bi-directional mapping?
            //var partner = _session.Load<Partner>(message.PartnerId);
            //partner.Sites.Add(message.SiteId.ToStringId<Site>());


        }
        public void Handle(PartnerForSiteDisabled message)
        {
            var partner = _session.Load<Partner>(message.PartnerId);
            partner.Sites.Remove(message.SiteId.ToStringId<Site>());



        }
        public void Handle(UserToSiteAdded message)
        {
            var site = _session.Load<Site>(message.SiteId);
            site.Users.Add(message.UserId);


        }
        public void Handle(UserFromSiteRemoved message)
        {
            var site = _session.Load<Site>(message.SiteId);
            site.Users.Remove(message.UserId);


        }

        public void Handle(PTAInfoUpdated message)
        {
            var site = _session.Load<Site>(message.SiteId);
            site.IsPTAEnabled = message.IsEnabled;
            site.PTASuffix = message.Suffix;
            site.PTASequenceNumber = message.SequenceNumber;


        }
    }
}
