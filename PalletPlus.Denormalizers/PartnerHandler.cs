﻿using System;
using NServiceBus;
using nTrack.Data.Extenstions;
using PalletPlus.Data;
using PalletPlus.Messages.Events.Partner;
using Raven.Client;

namespace PalletPlus.Denormalizers
{
    public class PartnerHandler :
        IHandleMessages<PartnerCreated>,
        IHandleMessages<PartnerUpdated>,
        IHandleMessages<PartnerDeleted>,
        IHandleMessages<PartnerUndeleted>,
        IHandleMessages<SupplierToPartnerAdded>,
        IHandleMessages<SupplierFromPartnerRemoved>,
        IHandleMessages<EquipmentToPartnerAllowed>,
        IHandleMessages<EquipmentToPartnerDisallowed>,
        IHandleMessages<EnableExchangeUpdated>
    {
        readonly IDocumentSession _session;

        public PartnerHandler(IDocumentSession session)
        {
            _session = session;
        }

        public void Handle(PartnerCreated message)
        {
            var existing = _session.Advanced.DocumentStore.DatabaseCommands.Head(message.PartnerId.ToStringId<Partner>());
            if (existing != null)
                throw new Exception("Partner already exists!");

            _session.Store(new Partner
                               {
                                   Id = message.PartnerId,
                                   CompanyName = message.Name,

                                   AddressLine1 = message.AddressLine1,
                                   AddressLine2 = message.AddressLine2,
                                   ContactEmail = message.ContactEmail,
                                   ContactName = message.ContactName,

                                   Suburb = message.Suburb,
                                   State = message.State,
                                   Postcode = message.Postcode,
                                   Country = message.Country,
                                   IsPTAEnabled = message.IsPTAEnabled,
                                   Tags = message.Tags

                               });


        }
        public void Handle(PartnerUpdated message)
        {
            var partner = _session.Load<Partner>(message.PartnerId);

            partner.AddressLine1 = message.AddressLine1;
            partner.AddressLine2 = message.AddressLine2;
            partner.ContactEmail = message.ContactEmail;
            partner.ContactName = message.ContactName;
            partner.CompanyName = message.Name;
            partner.Suburb = message.Suburb;
            partner.State = message.State;
            partner.Postcode = message.Postcode;
            partner.Country = message.Country;
            partner.IsPTAEnabled = message.IsPTAEnabled;
            partner.Tags = message.Tags;


        }
        public void Handle(PartnerDeleted message)
        {
            var partner = _session.Load<Partner>(message.PartnerId);
            partner.IsDeleted = true;

            //Task.Factory.StartNew(() => _store.Patch(message.PartnerId.ToStringId<Partner>(),
            //                                         new ScriptedPatchRequest
            //                                             {
            //                                                 Script = @"this.IsDeleted = true;"
            //                                             }));
        }
        public void Handle(PartnerUndeleted message)
        {
            var partner = _session.Load<Partner>(message.PartnerId);
            partner.IsDeleted = false;

            //Task.Factory.StartNew(() => _store.Patch(message.PartnerId.ToStringId<Partner>(),
            //                                         new ScriptedPatchRequest
            //                                             {
            //                                                 Script = @"this.IsDeleted = false;"
            //                                             }));
        }
        public void Handle(SupplierToPartnerAdded message)
        {
            var partner = _session.Load<Partner>(message.ParnterId);
            partner.Suppliers.Add(message.SupplierId.ToStringId<Supplier>(),
                                  new PartnerSupplier
                                      {
                                          AccountNumber = message.AccountNumber,
                                          SupplierId = message.SupplierId.ToStringId<Supplier>()

                                      });


            //Task.Factory.StartNew(
            //    () =>
            //    _store.Patch(message.ParnterId.ToStringId<Partner>(),
            //                 new ScriptedPatchRequest
            //                     {
            //                         Script = @"var keyValuePair ={'SupplierId':supplierId, 'AccountNumber': accountNumber,'EnableExchange': enableExchange, 'Equipments':[]};this.Suppliers[supplierId] = keyValuePair;",
            //                         Values = new Dictionary<string, object>
            //                             {
            //                                 {"supplierId", message.SupplierId.ToStringId<Supplier>()},
            //                                 {"accountNumber",RavenJToken.FromObject(message.AccountNumber)},
            //                                 {"enableExchange",message.IsExchangeEnabled}
            //                             }
            //                     })).Wait();
        }
        public void Handle(SupplierFromPartnerRemoved message)
        {
            var partner = _session.Load<Partner>(message.PartnerId);
            partner.Suppliers.Remove(message.SupplierId.ToStringId<Supplier>());

            //Task.Factory.StartNew(() => _store.Patch(message.PartnerId.ToStringId<Partner>(),
            //    new ScriptedPatchRequest
            //                 {
            //                     Script = @"delete this.Suppliers[supplierId];",
            //                     Values = new Dictionary<string, object>
            //                                  {
            //                                      {"supplierId", message.SupplierId.ToStringId<Supplier>()}
            //                                  }
            //                 }));
        }
        public void Handle(EquipmentToPartnerAllowed message)
        {
            var partner = _session.Load<Partner>(message.PartnerId);
            partner.Suppliers[message.SupplierId.ToStringId<Supplier>()].Equipments.Add(message.EquipmentCode);

            //Task.Factory.StartNew(() => _store.Patch(message.PartnerId.ToStringId<Partner>(),
            //    new ScriptedPatchRequest
            //    {
            //        Script = @"this.Suppliers[supplierId].Equipments.push(productCode)",
            //        Values = new Dictionary<string, object>
            //                                  {
            //                                      {"supplierId", message.SupplierId.ToStringId<Supplier>()},
            //                                      {"productCode", message.EquipmentCode}
            //                                  }
            //    })).Wait();
        }
        public void Handle(EquipmentToPartnerDisallowed message)
        {
            var partner = _session.Load<Partner>(message.PartnerId);
            var supplier = partner.Suppliers[message.SupplierId.ToStringId<Supplier>()];
            supplier.Equipments.Remove(message.EquipmentCode);


        }

        public void Handle(EnableExchangeUpdated message)
        {
            var partner = _session.Load<Partner>(message.PartnerId);
            var supplier = partner.Suppliers[message.SupplierId.ToStringId<Supplier>()];
            supplier.IsExchangeEnabled = message.EnabledExchange;
        }
    }
}
