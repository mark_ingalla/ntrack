﻿using System.Linq;
using NServiceBus;
using nTrack.Data.Extenstions;
using nTrack.Extension;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Events.Exchange;
using Raven.Client;

namespace PalletPlus.Denormalizers
{
    public class ExchangeHandler : IHandleMessages<ExchangeCreated>
    {
        readonly IDocumentSession _session;
        readonly IBus _bus;

        public ExchangeHandler(IDocumentSession session, IBus bus)
        {
            _session = session;
            _bus = bus;
        }

        public void Handle(ExchangeCreated message)
        {
            var supplierIdString = message.SupplierId.ToStringId<Supplier>();
            var siteIdString = message.SiteId.ToStringId<Site>();

            var equipmentNames = _session.Query<EquipmentIndex.Result, EquipmentIndex>()
                .Where(x => x.SupplierId == supplierIdString)
                .ToDictionary(x => x.EquipmentCode, y => y.EquipmentName);

            var equipmentsIn = message.EquipmentsIn.ToDictionary(x => x.Key, y => new Exchange.EquipmentInfo
                {
                    EquipmentName = equipmentNames[y.Key],
                    Quantity = y.Value
                });

            var equipmentsOut = message.EquipmentsOut.ToDictionary(x => x.Key, y => new Exchange.EquipmentInfo
                {
                    EquipmentName = equipmentNames[y.Key],
                    Quantity = y.Value
                });

            //var siteSupplierInfo = _session.Query<SiteSupplierIndex.Result, SiteSupplierIndex>()
            //    .Select(x => new { x.SiteId, CompanyName = x.SiteName, x.SupplierId, x.SupplierName, x.SupplierAccountNumber, x.Order })
            //    .Where(x => x.SupplierId == supplierIdString)
            //    .OrderBy(x => x.Order).ToList()
            //    .FirstOrDefault(x => x.SiteId == siteIdString || x.SiteId == accountId);

            var siteAddress = _session.Load<Site>(siteIdString);


            var exchange = new Exchange
                               {
                                   Id = message.ExchangeId,
                                   SiteId = message.SiteId.ToStringId<Site>(),
                                   //SiteName = siteAddress.AccountName + " - " + siteAddress.SiteName,
                                   //SiteAccountNumber = siteSupplierInfo.SupplierAccountNumber,
                                   SiteAddress = AddressHelper.GetFormatedAddress(siteAddress.AddressLine1, siteAddress.AddressLine2, siteAddress.Suburb, siteAddress.State, siteAddress.Postcode),

                                   SupplierId = message.SupplierId.ToStringId<Supplier>(),
                                   //SupplierName = siteSupplierInfo.SupplierName,

                                   EffectiveDate = message.EffectiveDate,
                                   ConsignmentReference = message.ConsignmentReference,
                                   DocketNumber = message.DocketNumber,
                                   PartnerReference = message.PartnerReference,
                                   EquipmentsIn = equipmentsIn,
                                   EquipmentsOut = equipmentsOut

                               };


            string parnterId = string.Empty;
            exchange.PartnerId = message.PartnerId.ToStringId<Partner>();
            parnterId = exchange.PartnerId;

            var partnerInfo = _session.Query
                <DestinationNamesAccountNumbersIndex.Result, DestinationNamesAccountNumbersIndex>()
                .FirstOrDefault(x => x.CompanyId == parnterId && x.SupplierId == supplierIdString);

            exchange.PartnerName = partnerInfo.CompanyName;
            //exchange.PartnerAccountNumber = partnerInfo.AccountNumber;
            exchange.PartnerAddress = AddressHelper.GetFormatedAddress(partnerInfo.AddressLine1,
                                                                           partnerInfo.AddressLine2,
                                                                           partnerInfo.Suburb,
                                                                           partnerInfo.State,
                                                                           partnerInfo.Postcode);

            _session.Store(exchange);



        }


    }
}
