﻿using NServiceBus;
using PalletPlus.Data;
using PalletPlus.Messages.Events.AssetBalance;
using Raven.Client;

namespace PalletPlus.Denormalizers
{
    public class AssetBalanceHandler :
        IHandleMessages<TransferAssetBalanceUpdated>
    {
        readonly IDocumentSession _session;

        public AssetBalanceHandler(IDocumentSession session)
        {
            _session = session;
        }

        public void Handle(TransferAssetBalanceUpdated message)
        {
            var site = _session.Load<Site>(message.SiteId);

            //if (site.EquipmentTransferBalance.ContainsKey(message.AssetCode))
            //    site.EquipmentTransferBalance[message.AssetCode] = message.Quantity;
            //else
            //    site.EquipmentTransferBalance.Add(message.AssetCode, message.Quantity);

            
        }
    }
}
