﻿using System;
using System.Collections.Generic;
using System.Linq;
using NServiceBus;
using nTrack.Data;
using nTrack.Data.Extenstions;
using nTrack.Data.Indices;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Events.Stocktake;
using Raven.Client;

namespace PalletPlus.Denormalizers
{
    public class StocktakeHandler :
        IHandleMessages<StocktakeRecorded>,
        IHandleMessages<StockCounted>,
        IHandleMessages<StockCountCorrectionRequested>,
        IHandleMessages<StocktakeCorrectionApproved>,
        IHandleMessages<StocktakeCorrectionDeclined>
    {
        readonly IDocumentSession _session;
        readonly ISystemDb _systemDb;

        public StocktakeHandler(IDocumentSession session, ISystemDb systemDb)
        {
            _session = session;
            _systemDb = systemDb;
        }

        public void Handle(StocktakeRecorded message)
        {
            var existing = _session.Advanced.DocumentStore.DatabaseCommands.Head(message.StocktakeId.ToStringId<Stocktake>());
            if (existing != null)
                throw new Exception("Stocktake already exists!");

            _session.Store(new Stocktake
                               {
                                   Id = message.StocktakeId,
                                   SiteId = message.SiteId.ToStringId<Site>(),
                                   SiteName = _session.Load<Site>(message.SiteId).CompanyName,
                                   SupplierId = message.SupplierId.ToStringId<Supplier>(),
                                   SupplierName = _session.Load<Supplier>(message.SupplierId).CompanyName,
                                   TakenOn = message.TakenOn
                               });


        }
        public void Handle(StockCounted message)
        {
            var stocktake = _session.Load<Stocktake>(message.StocktakeId);
            var equipment = _session.Query<EquipmentIndex.Result, EquipmentIndex>()
                                    .First(x => x.SupplierId == message.SupplierId.ToStringId<Supplier>() &&
                                                         x.EquipmentCode == message.EquipmentCode);

            stocktake.EquipmentCounts.Add(message.EquipmentCode,
                                 new EquipmentCount
                                     {
                                         EquipmentName = equipment.EquipmentName,
                                         Quantity = message.Quantity
                                     });



        }

        public void Handle(StockCountCorrectionRequested message)
        {
            var stocktake = _session.Load<Stocktake>(message.StocktakeId);
            var userFullName = string.Empty;

            using (var session = _systemDb.Store.OpenSession())
            {
                var userResult = session.Query<UserIndex.Result, UserIndex>()
                    .FirstOrDefault(x => x.UserKey == message.UserKey);
                if (userResult != null)
                    userFullName = userResult.UserFullName;
            }


            var count = stocktake.EquipmentCounts[message.AssetCode];
            count.PendingCorrection =
                new StocktakeCorrection(
                    message.CorrectionKey,
                    message.Quantity,
                    stocktake.EquipmentCounts[message.AssetCode].Quantity,
                    message.Reason,
                    new KeyValuePair<Guid, string>(message.UserKey, userFullName));


        }
        public void Handle(StocktakeCorrectionApproved message)
        {
            var stocktake = _session.Load<Stocktake>(message.StocktakeId);
            var userFullName = string.Empty;

            using (var session = _systemDb.Store.OpenSession())
            {
                var userResult = session.Query<UserIndex.Result, UserIndex>()
                    .FirstOrDefault(x => x.UserKey == message.ClosedBy);
                if (userResult != null)
                    userFullName = userResult.UserFullName;
            }
            var count = stocktake.EquipmentCounts.Values
                                 .First(x => x.PendingCorrection != null &&
                                             x.PendingCorrection.CorrectionKey == message.CorrectionKey);


            var closedByIdGuid = message.ClosedBy;
            var closedBy = new KeyValuePair<Guid, string>(closedByIdGuid, userFullName);
            count.PendingCorrection.ChangeCorrectionStatus(true, closedBy);
            count.HistoricalCorrections.Add(count.PendingCorrection);

            count.Quantity = count.PendingCorrection.NewQuantity;

            count.PendingCorrection = null;


        }
        public void Handle(StocktakeCorrectionDeclined message)
        {
            var stocktake = _session.Load<Stocktake>(message.StocktakeId);
            var userFullName = string.Empty;

            using (var session = _systemDb.Store.OpenSession())
            {
                var userResult = session.Query<UserIndex.Result, UserIndex>()
                    .FirstOrDefault(x => x.UserKey == message.ClosedBy);
                if (userResult != null)
                    userFullName = userResult.UserFullName;
            }

            var count = stocktake.EquipmentCounts.Values
                                 .First(x =>
                                        x.PendingCorrection != null &&
                                        x.PendingCorrection.CorrectionKey == message.CorrectionKey);

            var closedByIdGuid = message.ClosedBy;
            var closedBy = new KeyValuePair<Guid, string>(closedByIdGuid, userFullName);

            count.PendingCorrection.ChangeCorrectionStatus(false, closedBy);
            count.HistoricalCorrections.Add(count.PendingCorrection);
            count.PendingCorrection = null;


        }
    }
}