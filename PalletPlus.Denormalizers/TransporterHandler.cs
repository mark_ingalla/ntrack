﻿using System;
using NServiceBus;
using nTrack.Data.Extenstions;
using nTrack.Extension;
using PalletPlus.Data;
using PalletPlus.Messages.Events.Transporter;
using Raven.Client;
using Raven.Client.Connection;

namespace PalletPlus.Denormalizers
{
    public class TransporterHandler :
        IHandleMessages<TransporterCreated>,
        IHandleMessages<TransporterUpdated>,
        IHandleMessages<TransporterDeleted>,
        IHandleMessages<TransporterUndeleted>
    {
        readonly IDocumentSession _session;
        readonly IDatabaseCommands _storeCommands;

        public TransporterHandler(IDocumentSession session, IBus bus)
        {
            _session = session;
            _storeCommands = _session.Advanced.DocumentStore.DatabaseCommands.ForDatabase(bus.GetDatabaseName());
        }

        public void Handle(TransporterCreated message)
        {
            var existing = _storeCommands.Head(message.TransporterId.ToStringId<Transporter>());
            if (existing != null)
                throw new Exception("Transporter already exists!");

            _session.Store(new Transporter
                               {
                                   Id = message.TransporterId,
                                   AddressLine1 = message.AddressLine1,
                                   AddressLine2 = message.AddressLine2,
                                   ContactEmail = message.ContactEmail,
                                   ContactName = message.ContactName,
                                   Postcode = message.Postcode,
                                   State = message.State,
                                   Suburb = message.Suburb,
                                   CompanyName = message.Name,
                               });


        }
        public void Handle(TransporterUpdated message)
        {
            var transporter = _session.Load<Transporter>(message.TransporterId);

            transporter.AddressLine1 = message.AddressLine1;
            transporter.AddressLine2 = message.AddressLine2;
            transporter.ContactEmail = message.ContactEmail;
            transporter.ContactName = message.ContactName;
            transporter.CompanyName = message.Name;
            transporter.Suburb = message.Suburb;
            transporter.State = message.State;
            transporter.Postcode = message.Postcode;



        }
        public void Handle(TransporterDeleted message)
        {
            var transporter = _session.Load<Transporter>(message.TransporterId);
            transporter.IsDeleted = true;


        }
        public void Handle(TransporterUndeleted message)
        {
            var transporter = _session.Load<Transporter>(message.TransporterId);
            transporter.IsDeleted = false;


        }
    }
}
