﻿using System;
using System.Collections.Generic;
using System.Linq;
using NServiceBus;
using nTrack.Data;
using nTrack.Data.Extenstions;
using nTrack.Data.Indices;
using nTrack.Extension;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Events.Movement;
using Raven.Client;
using Account = PalletPlus.Data.Account;

namespace PalletPlus.Denormalizers
{
    public class MovementHandler :
        IHandleMessages<MovementCreated>,
        IHandleMessages<CorrectionRequestIssued>,
        IHandleMessages<QuantityCorrectionWithdrawn>,
        IHandleMessages<CorrectionApproved>,
        IHandleMessages<CorrectionDeclined>,
        IHandleMessages<MovementCancellationIssued>,
        IHandleMessages<MovementCancellationAccepted>,
        IHandleMessages<MovementCancellationDeclined>,
        IHandleMessages<CancellationWithdrawn>,
        IHandleMessages<PTAResolved>,
        IHandleMessages<MovementCompleted>,
        IHandleMessages<MovementExported>
    {
        readonly IDocumentSession _session;
        readonly ISystemDb _systemDb;
        readonly IBus _bus;

        public MovementHandler(IDocumentSession session, ISystemDb systemDb, IBus bus)
        {
            _session = session;
            _systemDb = systemDb;
            _bus = bus;
        }

        public void Handle(MovementCreated message)
        {
            var accountId = _bus.GetAccountId().ToStringId<Data.Account>();

            //prepare data for denormalization
            var supplierIdString = message.SupplierId.ToStringId<Supplier>();
            var siteIdString = message.SiteId.ToStringId<Site>();

            var equipmentNames = _session.Query<EquipmentIndex.Result, EquipmentIndex>()
                .Where(x => x.SupplierId == supplierIdString)
                .ToDictionary(x => x.EquipmentCode, y => y.EquipmentName);

            var equipments = message.Equipments.ToDictionary(x => x.Key, y => new Movement.EquipmentInfo
                {
                    EquipmentName = equipmentNames[y.Key],
                    Quantity = y.Value
                });

            var siteSupplierInfo = _session.Query<SiteSupplierIndex.Result, SiteSupplierIndex>()
                .Select(x => new { x.SiteId, CompanyName = x.SiteName, x.SupplierId, x.SupplierName, x.SupplierAccountNumber, x.Order })
                .Where(x => x.SupplierId == supplierIdString)
                .OrderBy(x => x.Order).ToList()
                .FirstOrDefault(x => x.SiteId == siteIdString || x.SiteId == accountId);



            var siteAddress = _session.Load<Site>(siteIdString);

            var movement = new Movement
                {
                    Id = message.MovementId,
                    SiteId = message.SiteId.ToStringId<Site>(),
                    //SiteName = siteAddress.AccountName + " - " + siteAddress.SiteName,

                    SiteAccountNumber = siteSupplierInfo.SupplierAccountNumber,
                    SiteAddress = AddressHelper.GetFormatedAddress(siteAddress.AddressLine1, siteAddress.AddressLine2, siteAddress.Suburb, siteAddress.State, siteAddress.Postcode),

                    SupplierId = message.SupplierId.ToStringId<Supplier>(),
                    SupplierName = siteSupplierInfo.SupplierName,

                    Equipments = equipments,

                    MovementType = message.MovementType,
                    MovementDate = message.MovementDate,
                    EffectiveDate = message.EffectiveDate,

                    Direction = message.Direction,

                    ConsignmentNote = message.ConsignmentNote,
                    DocketNumber = message.DocketNumber,
                    DestinationReference = message.DestinationReference,
                    MovementReference = message.MovementReference,

                    UnreconciledItems = message.Equipments.Count,
                    DestinationKind = message.DestinationKind,
                    IsPendingPTA = message.IsPTA,
                    IsPending = message.IsPending
                };

            string destinationId = string.Empty;
            if (message.DestinationKind == "Site")
            {
                movement.DestinationId = message.DestinationId.ToStringId<Site>();
                destinationId = movement.DestinationId;
            }

            if (message.DestinationKind == "Partner")
            {
                movement.DestinationId = message.DestinationId.ToStringId<Partner>();
                destinationId = movement.DestinationId;
            }

            if (message.DestinationKind == "Supplier")
            {
                movement.DestinationId = message.DestinationId.ToStringId<Supplier>();
                movement.LocationKey = message.LocationId;
                destinationId = "suppliers/" + message.LocationId;
            }

            var destinationInfo = _session.Query
                <DestinationNamesAccountNumbersIndex.Result, DestinationNamesAccountNumbersIndex>()
                .FirstOrDefault(x => x.CompanyId == destinationId && x.SupplierId == supplierIdString);


            if (message.DestinationKind == "Supplier")
            {
                var locationName =
                    _session.Load<Supplier>(movement.DestinationId).Locations[message.LocationId.Value].CompanyName;

                movement.DestinationName = destinationInfo.CompanyName + " - " + locationName;

            }
            else if (message.DestinationKind == "Site")
            {
                var site = _session.Load<Site>(movement.DestinationId);
                var account = _session.Load<Account>(site.AccountId);

                movement.DestinationName = account.CompanyName + " - " + destinationInfo.CompanyName;
            }
            else
                movement.DestinationName = destinationInfo.CompanyName;

            movement.DestinationAccountNumber = destinationInfo.AccountNumber;
            movement.DestinationAddress = AddressHelper.GetFormatedAddress(destinationInfo.AddressLine1,
                                                                           destinationInfo.AddressLine2,
                                                                           destinationInfo.Suburb,
                                                                           destinationInfo.State,
                                                                           destinationInfo.Postcode);

            _session.Store(movement);


        }
        public void Handle(CorrectionRequestIssued message)
        {
            var movement = _session.Load<Movement>(message.MovementId);
            var requestedByIdGuid = message.RequestedBy;
            var userFullName = string.Empty;

            using (var session = _systemDb.Store.OpenSession())
            {
                var userResult = session.Query<UserIndex.Result, UserIndex>()
                    .FirstOrDefault(x => x.UserKey == requestedByIdGuid);
                if (userResult != null)
                    userFullName = userResult.UserFullName;
            }

            movement.PendingMovementCorrection =
                new MovementCorrection(
                    message.CorrectionKey,
                    new KeyValuePair<Guid, string>(requestedByIdGuid, userFullName),
                    message.RequestedOn,
                    message.AssetCodeQuantity,
                    movement.Equipments.ToDictionary(x => x.Key, y => y.Value.Quantity),
                    movement.MovementDate,
                    message.NewDate,
                    movement.EffectiveDate,
                    message.NewEffectiveDate,
                    message.Reason
                    );


        }
        public void Handle(CorrectionApproved message)
        {
            var movement = _session.Load<Movement>(message.MovementId);
            var closedByIdGuid = message.ClosedBy;
            var userFullName = string.Empty;

            using (var session = _systemDb.Store.OpenSession())
            {
                var userResult = session.Query<UserIndex.Result, UserIndex>()
                    .FirstOrDefault(x => x.UserKey == closedByIdGuid);
                if (userResult != null)
                    userFullName = userResult.UserFullName;
            }

            var closedBy = new KeyValuePair<Guid, string>(message.ClosedBy, userFullName);
            movement.PendingMovementCorrection.ChangeCorrectionStatus(true, closedBy);

            movement.MovementDate = message.NewDate;
            movement.EffectiveDate = message.NewEffectiveDate;
            foreach (var i in message.AssetCodeQuantity)
            {
                if (movement.Equipments.ContainsKey(i.Key))
                    movement.Equipments[i.Key].Quantity = i.Value;
            }

            var pendingCorrection = movement.PendingMovementCorrection;
            movement.HistoricalCorrections.Add(pendingCorrection.CorrectionKey, pendingCorrection);
            movement.PendingMovementCorrection = null;


        }
        public void Handle(CorrectionDeclined message)
        {
            var movement = _session.Load<Movement>(message.MovementId);
            var closedByIdGuid = message.ClosedBy;
            var userFullName = string.Empty;

            using (var session = _systemDb.Store.OpenSession())
            {
                var userResult = session.Query<UserIndex.Result, UserIndex>()
                    .FirstOrDefault(x => x.UserKey == closedByIdGuid);
                if (userResult != null)
                    userFullName = userResult.UserFullName;
            }

            var closedBy = new KeyValuePair<Guid, string>(closedByIdGuid, userFullName);

            movement.PendingMovementCorrection.ChangeCorrectionStatus(false, closedBy);
            var pendingCorrection = movement.PendingMovementCorrection;
            movement.HistoricalCorrections.Add(pendingCorrection.CorrectionKey, pendingCorrection);
            movement.PendingMovementCorrection = null;


        }
        public void Handle(MovementCancellationIssued message)
        {
            var movement = _session.Load<Movement>(message.MovementId);
            var requestedByIdGuid = message.RequestedBy;
            var userFullName = string.Empty;

            using (var session = _systemDb.Store.OpenSession())
            {
                var userResult = session.Query<UserIndex.Result, UserIndex>()
                    .FirstOrDefault(x => x.UserKey == requestedByIdGuid);
                if (userResult != null)
                    userFullName = userResult.UserFullName;
            }

            var requestedBy = new KeyValuePair<Guid, string>(requestedByIdGuid, userFullName);

            movement.PendingMovementCancellation = new MovementCancellation(message.CancellationKey, requestedBy,
                                                                            message.RequestedOn, message.Reason);


        }
        public void Handle(MovementCancellationAccepted message)
        {
            var movement = _session.Load<Movement>(message.MovementId);
            var closedByIdGuid = message.ClosedBy;
            var userFullName = string.Empty;

            using (var session = _systemDb.Store.OpenSession())
            {
                var userResult = session.Query<UserIndex.Result, UserIndex>()
                    .FirstOrDefault(x => x.UserKey == closedByIdGuid);
                if (userResult != null)
                    userFullName = userResult.UserFullName;
            }

            var closedBy = new KeyValuePair<Guid, string>(closedByIdGuid, userFullName);

            movement.IsCancelled = true;
            movement.PendingMovementCancellation.ChangeCorrectionStatus(true, closedBy);


        }
        public void Handle(MovementCancellationDeclined message)
        {
            var movement = _session.Load<Movement>(message.MovementId);
            var closedByIdGuid = message.ClosedBy;
            var userFullName = string.Empty;

            using (var session = _systemDb.Store.OpenSession())
            {
                var userResult = session.Query<UserIndex.Result, UserIndex>()
                    .FirstOrDefault(x => x.UserKey == closedByIdGuid);
                if (userResult != null)
                    userFullName = userResult.UserFullName;
            }

            var closedBy = new KeyValuePair<Guid, string>(closedByIdGuid, userFullName);

            movement.PendingMovementCancellation.ChangeCorrectionStatus(false, closedBy);


        }
        public void Handle(QuantityCorrectionWithdrawn message)
        {
            var movement = _session.Load<Movement>(message.MovementId);

            movement.PendingMovementCorrection = null;


        }
        public void Handle(CancellationWithdrawn message)
        {
            var movement = _session.Load<Movement>(message.MovementId);
            movement.PendingMovementCancellation = null;


        }

        public void Handle(PTAResolved message)
        {
            var movement = _session.Load<Movement>(message.MovemendId);
            movement.IsPendingPTA = false;
            movement.EffectiveDate = message.EffectiveDateUTC;
            foreach (var equipment in message.Equipments)
            {
                movement.Equipments[equipment.Key].Quantity = equipment.Value;
            }

            movement.DocketNumber = message.DocketNumber;

        }

        public void Handle(MovementCompleted message)
        {
            var movement = _session.Load<Movement>(message.MovementId);

            movement.EffectiveDate = message.EffectiveDateUTC;
            movement.DocketNumber = message.DocketNumber;

            if (!string.IsNullOrEmpty(message.DocketNumber))
            {
                movement.IsPending = false;
            }
        }

        public void Handle(MovementExported message)
        {
            var movement = _session.Load<Movement>(message.MovementId);
            movement.IsExported = true;
        }
    }
}
