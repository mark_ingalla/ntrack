﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NServiceBus;
using PalletPlus.Data;
using PalletPlus.Messages.Events.Movement.InternalMovement;
using PalletPlus.Messages.Events.Movement.TransferMovement;
using Raven.Client;

namespace PalletPlus.Denormalizers
{
    class InternalMovementHandler:
        IHandleMessages<InternalMovementCreated>
    {
        readonly IDocumentSession _session;

        public InternalMovementHandler(IDocumentSession session)
        {
            _session = session;
        }

        public void Handle(InternalMovementCreated message)
        {
            _session.Store(new InternalMovement
            {
                Assets = message.Assets,
                Direction = message.Direction,
                MovementDate = message.MovementDate,
                PartnerSiteId = message.PartnerSiteId,
                SiteId = message.SiteId,
                SupplierId = message.SupplierId,
                Id = message.MovementId,

                ConNote = message.ConNote,
                ProviderDocketNo = message.ProviderDocketNo,
                PartnerReference = message.PartnerReference,
                Reference = message.Reference,
                ReceivedViaTransporter = message.ReceivedViaTransporter,
                TransporterId = message.ReceivedViaTransporter ? message.TransporterId : null
            });
        }

        //public void Handle(InternalCorrectionRequestIssued message)
        //{
        //    var movement = _session.Load<TransferMovement>(message.MovementId);

        //    movement.PendingCorrection = new Correction(message.CorrectionKey, message.RequestedBy, message.RequestedOn, message.AssetCodeQuantity, movement.Assets, message.Reason);
        //}

        //public void Handle(InternalCorrectionApproved message)
        //{
        //    var movement = _session.Load<TransferMovement>(message.MovementId);

        //    movement.PendingCorrection.ChangeCorrectionStatus(true, message.ClosedBy);

        //    foreach(var i in message.AssetCodeQuantity)
        //    {
        //        if(movement.Assets.ContainsKey(i.Key))
        //            movement.Assets[i.Key] = i.Value;
        //    }

        //    movement.HistoricalCorrections.Add(movement.PendingCorrection);
        //    movement.PendingCorrection = null;
        //}

        //public void Handle(InternalCorrectionDeclined message)
        //{
        //    var movement = _session.Load<TransferMovement>(message.MovementId);

        //    movement.PendingCorrection.ChangeCorrectionStatus(false, message.ClosedBy);
        //    movement.HistoricalCorrections.Add(movement.PendingCorrection);
        //    movement.PendingCorrection = null;
        //}

        //public void Handle(InternalMovementCancellationIssued message)
        //{
        //    var movement = _session.Load<TransferMovement>(message.MovementId);

        //    movement.PendingCancellation = new Cancellation(message.CancellationKey, message.RequestedBy, message.RequestedOn, message.Reason);
        //}

        //public void Handle(InternalMovementCancellationAccepted message)
        //{
        //    var movement = _session.Load<TransferMovement>(message.MovementId);

        //    movement.IsCancelled = true;
        //    movement.PendingCancellation.ChangeCorrectionStatus(true, message.ClosedBy);
        //}

        //public void Handle(InternalMovementCancellationDeclined message)
        //{
        //    var movement = _session.Load<TransferMovement>(message.MovementId);

        //    movement.PendingCancellation.ChangeCorrectionStatus(false, message.ClosedBy);
        //}

        //public void Handle(InternalQuantityCorrectionWithdrawn message)
        //{
        //    var movement = _session.Load<TransferMovement>(message.MovementId);

        //    movement.PendingCorrection = null;
        //}

        //public void Handle(InternalCancellationWithdrawn message)
        //{
        //    var movement = _session.Load<TransferMovement>(message.MovementId);

        //    movement.PendingCancellation = null;
        //}
    }
}
