﻿using System;
using System.IO;
using System.Linq;
using NServiceBus;
using PalletPlus.Services.Suppliers;
using nTrack.Data.Extenstions;
using nTrack.Extension;
using PalletPlus.Data;
using PalletPlus.Messages.Events;
using PalletPlus.Services;
using Raven.Client;

namespace PalletPlus.Denormalizers
{
    public class ExportMovement : IHandleMessages<DayMovementExported>
    {
        readonly IDocumentSession _session;
        readonly IDocumentStore _store;
        readonly ISupplierIntegration[] _suppliersIntegration;
        readonly IBus _bus;

        public ExportMovement(IDocumentSession session, IDocumentStore store, ISupplierIntegration[] suppliersIntegration, IBus bus)
        {
            _session = session;
            _store = store;
            _suppliersIntegration = suppliersIntegration;
            _bus = bus;
        }

        public void Handle(DayMovementExported message)
        {
            var supplier = _session.Load<Supplier>(message.SupplierId);
            var export = _suppliersIntegration.SingleOrDefault(x => x.Name == supplier.ProviderName);

            if (export == null) return;

            if (message.MovementIds.Length > 1024)
                throw new Exception("Too many movements to export! Limit is 1024");

            var movements = _session.Load<Movement>(message.MovementIds.Select(x => x.ToStringId<Movement>()));
            var siteAccount = movements.First().SiteAccountNumber;
            var fileBytes = export.Export(movements);

            var id = Guid.NewGuid();
            var attachment = new ExportAttachment
                {
                    Id = id,
                    SiteId = message.SiteId.ToStringId<Site>(),
                    SupplierId = message.SupplierId.ToStringId<Supplier>(),
                    MovementDate = message.MovementDate,
                    FileName = "{0}_{1}.csv".Fill(siteAccount, message.MovementDate.Date.ToString("yyy_MM_dd"), id.ToString()),
                    TotalMovements = message.TotalMovements
                };

            _session.Store(attachment);

            using (var mem = new MemoryStream(fileBytes))
                _store.DatabaseCommands.ForDatabase(_bus.GetDatabaseName())
                      .PutAttachment(id.ToStringId<ExportAttachment>(), null, mem, null);

        }
    }
}
