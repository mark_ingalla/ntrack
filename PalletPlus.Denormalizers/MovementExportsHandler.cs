﻿using System;
using System.Collections.Generic;
using System.Linq;
using NServiceBus;
using nTrack.Data.Extenstions;
using nTrack.Extension;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Events.Site;
using PalletPlus.Services;
using PalletPlus.Services.ImportExport.CHEP;
using PalletPlus.Services.ImportExport.Loscam;
using Raven.Client;
using Raven.Client.Connection;
using Raven.Client.Linq;
using Raven.Json.Linq;

namespace PalletPlus.Denormalizers
{
    public class MovementExportsHandler :
        IHandleMessages<ExportMovementDataBuilt>
    {
        readonly IDatabaseCommands _storeCommands;
        readonly IDocumentSession _session;
        readonly List<IExportProvider> _exportProviders;

        public MovementExportsHandler(IDocumentSession session, List<IExportProvider> exportProviders, IBus bus)
        {
            _storeCommands = session.Advanced.DocumentStore.DatabaseCommands.ForDatabase(bus.GetDatabaseName());
            _session = session;
            _exportProviders = exportProviders;
        }

        
        public void Handle(ExportMovementDataBuilt message)
        {
            var supplier = _session.Load<Supplier>(message.SupplierId);
            var supplierIdString = message.SupplierId.ToStringId<Supplier>();
            var siteIdString = message.SiteId.ToStringId<Site>();
            var exportProvider = _exportProviders.FirstOrDefault(x => x.Name == supplier.ProviderName);
            if (exportProvider == null)
                throw new Exception("No Export Provider For {0}!".Fill(supplier.CompanyName));

            var movements = new List<Movement>();
            var pageIndex = 0;
            var foundData = true;

            while (foundData)
            {
                var mResult = _session.Query<Movement, UnexportedMovementIndex>()
                                          .Where(x => x.SupplierId == supplierIdString && x.SiteId == siteIdString
                                           && !string.IsNullOrEmpty(x.DocketNumber))
                                          .Skip(pageIndex * 1000)
                                          .Take(1000)
                                          .OrderBy(x => x.MovementDate)
                                          .ToList();

                movements.AddRange(mResult);
                if (mResult.Count <= 0)
                    foundData = false;

                pageIndex++;
            }

            if (!movements.Any())
                return;

            var siteSupplier = _session.Query<SiteSupplierIndex.Result, SiteSupplierIndex>()
                .SingleOrDefault(x => x.SupplierId == supplier.IdString && x.SiteId == message.SiteId.ToStringId<Site>());

            var siteSupplierAccountNumber = siteSupplier == null ? string.Empty : siteSupplier.SupplierAccountNumber;
            var siteSupplierName = siteSupplier == null ? string.Empty : siteSupplier.SupplierName;
            var destinationsSupplier = GetDestinationsSupplierInfo(movements.ToArray(), supplier);

            if (exportProvider is CHEPProvider)
            {
                var fileName = string.Format("CHEPMate_l{0}", DateTime.UtcNow.ToString("yyyyMMdd"));

                ChepExportMethod(exportProvider,
                                 new KeyValuePair<string, string>(siteSupplierAccountNumber, siteSupplierName),
                                 destinationsSupplier, movements.ToArray(), message.SupplierId, message.SiteId, fileName);
            }
            else if (exportProvider is LoscamProvider)
            {
                LoscamExportMethod(exportProvider,
                                   new KeyValuePair<string, string>(siteSupplierAccountNumber, siteSupplierName),
                                   destinationsSupplier, movements.ToArray(), message.SupplierId, message.SiteId);
            }

            foreach (var m in movements)
            {
                m.IsExported = true;

                //_session.Advanced.Evict(m);

                //_commands.Patch(m.IdString,
                //     new ScriptedPatchRequest
                //     {
                //         Script = @"this.IsExported = true;"
                //     });
            }


        }

        /// <summary>
        /// This is LoscamExportMethod. You may be wondering why CHEP and Loscam don't share the same method.
        /// It's because export models (standards) are different for each of them. We have to generate 2 files:
        /// one for Transfer In movements and 2nd one for Transfer Out movements here, whereas CHEP wants it all in just one file.
        /// It was the easiest to put the division logic in here.
        /// </summary>
        void LoscamExportMethod(IExportProvider exportProvider,
            KeyValuePair<string, string> siteSupplier,
            Dictionary<Guid, KeyValuePair<string, string>> destinationsSupplier, Movement[] movements,
            Guid supplierId, Guid siteId)
        {
            var transferInMovements = movements.Where(x => x.Direction == "In").ToArray();
            var transferOutMovements = movements.Where(x => x.Direction == "Out").ToArray();

            var fileName = string.Format("{0}.{1}", siteSupplier.Key, "065WHATISTHIS?");

            if (transferInMovements.Any())
                ChepExportMethod(exportProvider, siteSupplier, destinationsSupplier, transferInMovements, supplierId, siteId, fileName);
            if (transferOutMovements.Any())
                ChepExportMethod(exportProvider, siteSupplier, destinationsSupplier, transferOutMovements, supplierId, siteId, fileName);
        }

        /// <summary>
        /// This is just a simple export method.
        /// </summary>
        void ChepExportMethod(IExportProvider exportProvider,
            KeyValuePair<string, string> siteSupplier,
            Dictionary<Guid, KeyValuePair<string, string>> destinationsSupplier, Movement[] movements,
            Guid supplierId, Guid siteId, string fileName)
        {
            using (var stream = exportProvider.Export(siteSupplier, destinationsSupplier, movements))
            {
                var id = Guid.NewGuid();

                stream.Position = 0;

                _storeCommands.PutAttachment(id.ToString(), null, stream, new RavenJObject { { "Format", "CSV" } });

                _session.Store(new AttachmentReference()
                {
                    Id = id,
                    SupplierId = supplierId.ToStringId<Supplier>(),
                    SiteId = siteId.ToStringId<Site>(),
                    AttachmentType = "csv",
                    ExportedOn = DateTime.UtcNow,
                    FileSize = stream.Length,
                    TotalDownloads = 0,
                    FileName = fileName
                });
            }
        }

        /// <summary>
        /// This is helper method to extract account numbers of suppliers associated with each type of destination
        /// (either Site, Supplier or Partner). It's very complicated and it does a lot of business logic to distinguish stuff
        /// we didn't plan very well at the planning part of the project. That's why I ask for understanding that it's so messy.
        /// </summary>
        /// <param name="movements"></param>
        /// <param name="supplier"></param>
        /// <returns></returns>
        private Dictionary<Guid, KeyValuePair<string, string>> GetDestinationsSupplierInfo(Movement[] movements, Supplier supplier)
        {
            var destinationIds = movements.Select(y => y.DestinationId).Distinct().ToList();
            var partnerIds = destinationIds.Where(x => x.Split('/')[0] == "partners").ToList();
            var supplierIds = destinationIds.Where(x => x.Split('/')[0] == "suppliers").ToList();

            var sites = _session.Advanced.LoadStartingWith<Site>("sites/");
            var partners = _session.Load<Partner>(partnerIds);
            var suppliers = _session.Load<Supplier>(supplierIds);
            var account = _session.Query<Account>().First();

            //First we get account numbers for suppliers associated with account
            var accountSuppliers = new Dictionary<Guid, KeyValuePair<string, string>>();
            if (account != null)
            {
                var accountSupplier = account.Suppliers.FirstOrDefault(x => x.Key == supplier.IdString);
                if (!string.IsNullOrEmpty(accountSupplier.Key))
                {

                    foreach (var id in sites.Select(x=>x.Id).ToList())
                    {
                        accountSuppliers.Add(id,
                                             new KeyValuePair<string, string>(accountSupplier.Value.AccountNumber,
                                                                              accountSupplier.Value.CompanyName));
                    }
                }
            }

            //account numbers for suppliers associated with partners
            var partnersSupplier = (from p in partners
                                    from s in p.Suppliers
                                    where s.Key == supplier.IdString
                                    select new KeyValuePair<Guid, KeyValuePair<string, string>>(
                                        p.Id,
                                        new KeyValuePair<string, string>(
                                            s.Value.AccountNumber,
                                            p.CompanyName)
                                        )
                                   ).ToList();

            //account numbers for suppliers associated with locations of suppliers
            var locationsSupplier = (from s in suppliers
                                     from l in s.Locations
                                     select new KeyValuePair<Guid, KeyValuePair<string, string>>(
                                         l.Key,
                                         new KeyValuePair<string, string>(
                                             l.Value.AccountNumber,
                                             l.Value.CompanyName)
                                         )
                                    ).ToList();

            //account numbers for suppliers associated with sites
            var sitesSupplier = (from site in sites
                                 from s in site.Suppliers
                                 where s.Key == supplier.IdString
                                 select new KeyValuePair<Guid, KeyValuePair<string, string>>(
                                     site.Id,
                                     new KeyValuePair<string, string>(
                                         s.Value.AccountNumber,
                                         s.Value.CompanyName)
                                     )
                                   ).ToList();

            //add/overwrite account numbers for suppliers associated with sites that aren't/are associated with account
            foreach (var s in sitesSupplier)
            {
                if (!accountSuppliers.Keys.Any(x => x == s.Key))
                {
                    accountSuppliers.Add(s.Key, new KeyValuePair<string, string>(s.Value.Key, s.Value.Value));
                }
                else
                {
                    accountSuppliers[s.Key] = new KeyValuePair<string, string>(s.Value.Key, s.Value.Value);
                }
            }

            partnersSupplier.AddRange(locationsSupplier);
            partnersSupplier.AddRange(accountSuppliers);

            return partnersSupplier.ToDictionary(x => x.Key, y => y.Value);
        }
    }
}
