﻿using System;
using System.Linq;
using NServiceBus;
using nTrack.Data.Extenstions;
using nTrack.Extension;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Events.Supplier;
using Raven.Client;
using Raven.Client.Connection;

namespace PalletPlus.Denormalizers
{
    public class SupplierHandler :
        IHandleMessages<SupplierCreated_V2>,
        IHandleMessages<SupplierUpdated_V2>,
        IHandleMessages<SupplierDeleted>,
        IHandleMessages<SupplierUndeleted>,
        IHandleMessages<EquipmentSupplierAdded>,
        IHandleMessages<EquipmentSupplierDiscontinued>,
        IHandleMessages<EquipmentSupplierUndiscontinued>,
        IHandleMessages<LocationToSupplierAdded>,
        IHandleMessages<SupplierLocationUpdated>,
        IHandleMessages<SupplierLocationRemoved>,
        IHandleMessages<SupplierLocationUnremoved>
    {
        readonly IDocumentSession _session;
        readonly IDatabaseCommands _storeCommands;
        readonly IBus _bus;

        public SupplierHandler(IDocumentSession session, IBus bus)
        {
            _session = session;
            _storeCommands = _session.Advanced.DocumentStore.DatabaseCommands.ForDatabase(bus.GetDatabaseName());
            _bus = bus;
        }

        public void Handle(SupplierCreated_V2 message)
        {
            var existing = _storeCommands.Head(message.SupplierId.ToStringId<Supplier>());
            if (existing != null)
                throw new Exception("Supplier already exists!");

            var supplier = new Supplier
                               {
                                   Id = message.SupplierId,
                                   CompanyName = message.Name,
                                   ContactEmail = message.EmailAddress,
                                   ProviderName = message.ProviderName,
                                   IsPTAEnabled = message.IsPTAEnabled
                               };

            _session.Store(supplier);


        }
        public void Handle(SupplierUpdated_V2 message)
        {
            var supplier = _session.Load<Supplier>(message.SupplierId);
            supplier.CompanyName = message.Name;
            supplier.ContactEmail = message.EmailAddress;
            supplier.ProviderName = message.ProviderName;
            supplier.IsPTAEnabled = message.IsPTAEnabled;

        }
        public void Handle(SupplierDeleted message)
        {
            var supplier = _session.Load<Supplier>(message.SupplierId);
            supplier.IsDeleted = true;
            
        }
        public void Handle(SupplierUndeleted message)
        {
            var supplier = _session.Load<Supplier>(message.SupplierId);
            supplier.IsDeleted = false;

        }
        public void Handle(EquipmentSupplierAdded message)
        {
            var supplier = _session.Load<Supplier>(message.SupplierId);

            if (supplier.Equipments.ContainsKey(message.EquipmentCode))
                supplier.Equipments[message.EquipmentCode].IsDeleted = false;
            else
                supplier.Equipments.Add(message.EquipmentCode, new Equipment
                    {
                        EquipmentCode = message.EquipmentCode,
                        EquipmentName = message.EquipmentName
                    });


            #region Dashboard Equipment Colors

            var accountId = _bus.GetAccountId().ToStringId<Data.Account>();

            var account = _session.Load<Account>(accountId);


            var code =
                account.DashboardEquipmentColors.Values.FirstOrDefault(
                    x => x.EquipmentCode == message.EquipmentCode
                         && x.EquipmentName == message.EquipmentName);

            if (code == null)
            {
                var randonGen = new Random();
                
                var color = new DashboardEquipmentColor()
                {
                    EquipmentCode = message.EquipmentCode,
                    EquipmentName = message.EquipmentName,
                    R = randonGen.Next(255),
                    G = randonGen.Next(255),
                    B = randonGen.Next(255)
                };

                account.DashboardEquipmentColors.Add(Guid.NewGuid(), color);
            }

        

            #endregion


            
        }
        public void Handle(EquipmentSupplierDiscontinued message)
        {
            var supplier = _session.Load<Supplier>(message.SupplierId);
            supplier.Equipments[message.EquipmentCode].IsDeleted = true;

        }

        public void Handle(EquipmentSupplierUndiscontinued message)
        {
            var supplier = _session.Load<Supplier>(message.SupplierId);
            supplier.Equipments[message.EquipmentCode].IsDeleted = false;

        }

        public void Handle(LocationToSupplierAdded message)
        {
            var supplier = _session.Load<Supplier>(message.SupplierId);
            var location = new SupplierLocation
                               {
                                   LocationKey = message.LocationKey,
                                   CompanyName = message.Name,
                                   AccountNumber = message.AccountNumber,
                                   AddressLine1 = message.AddressLine1,
                                   AddressLine2 = message.AddressLine2,
                                   Suburb = message.Suburb,
                                   State = message.State,
                                   Postcode = message.Postcode,
                                   ContactName = message.ContactName,
                                   ContactEmail = message.ContactEmail,
                                   Country = message.Country
                               };

            supplier.Locations.Add(message.LocationKey, location);
        }

        public void Handle(SupplierLocationUpdated message)
        {
            var supplier = _session.Load<Supplier>(message.SupplierId);
            var location = supplier.Locations[message.LocationKey];

            location.CompanyName = message.Name;
            location.AccountNumber = message.AccountNumber;
            location.AddressLine1 = message.AddressLine1;
            location.AddressLine2 = message.AddressLine2;
            location.Suburb = message.Suburb;
            location.State = message.State;
            location.Postcode = message.Postcode;
            location.ContactName = message.ContactName;
            location.ContactEmail = message.ContactEmail;
            location.Country = message.Country;


        }

        public void Handle(SupplierLocationRemoved message)
        {
            var supplier = _session.Load<Supplier>(message.SupplierId);

            var isLocationUsed = _session.Query<MovementIndex.Result, MovementIndex>()
                .Any(x => x.LocationKey == message.LocationKey);

            if (isLocationUsed)
            {
                supplier.Locations[message.LocationKey].IsDeleted = true;

               
            }
            else
            {
                supplier.Locations.Remove(message.LocationKey);

            }
        }

        public void Handle(SupplierLocationUnremoved message)
        {
            var supplier = _session.Load<Supplier>(message.SupplierId);
            supplier.Locations[message.LocationKey].IsDeleted = false;


            
        }
    }
}
