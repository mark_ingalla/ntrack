﻿using System;
using System.Linq;
using NServiceBus;
using nTrack.Data.Extenstions;
using PalletPlus.Data;
using PalletPlus.Messages.Events.SupplierInvoice;
using Raven.Client;

namespace PalletPlus.Denormalizers
{
    public class SupplierInvoiceHandler :
        IHandleMessages<InvoiceCreated>,
        IHandleMessages<InvoiceClosed>,


    IHandleMessages<MovementsReconciled>,
        IHandleMessages<MovementRecordMatched>,
        IHandleMessages<MovementRecordRejected>,
        IHandleMessages<TransactionCreated>
    {
        readonly IDocumentSession _session;

        public SupplierInvoiceHandler(IDocumentSession session)
        {
            _session = session;
        }

        public void Handle(InvoiceCreated message)
        {
            //var invoice = new InvoiceAttachment
            //                  {
            //                      Id = message.InvoiceId,
            //                      SupplierId = message.SupplierId.ToStringId<Supplier>(),
            //                      SupplierName = message.SupplierName,
            //                      FileName = message.FileName,
            //                      DateImported = DateTime.UtcNow,
            //                      UnreconciledItems = message.Invoice.Details.Count,
            //                      Header = Mapper.Map<InvoiceHeader>(message.Invoice.Header),
            //                      Details = message.Invoice.Details != null ? Mapper.Map<List<InvoiceDetail>>(message.Invoice.Details) : null,
            //                      DetailsSummary = message.Invoice.DetailsSummary != null ? Mapper.Map<List<InvoiceDetail>>(message.Invoice.DetailsSummary) : null
            //                  };

            var invoice = new SupplierInvoice
                {
                    Id = message.InvoiceId,
                    SupplierId = message.SupplierId.ToStringId<Supplier>(),
                    InvoiceNumber = message.InvoiceNumber,
                    InvoiceDate = message.InvoiceDate

                };

            _session.Store(invoice);

        }
        public void Handle(InvoiceClosed message)
        {
            var invoice = _session.Load<SupplierInvoice>(message.InvoiceId);
            invoice.IsClosed = true;

            invoice.Details = message.Details.Select(x => new SupplierInvoice.InvoiceDetail
                {
                    Key = x.Key,
                    TransactionCode = x.TransactionCode,
                    DocketNumber = x.DocketNumber,
                    EquipmentCode = x.EquipmentCode,
                    Qty = x.Qty,
                    DaysHire = x.DaysHire,
                    AccountNumber = x.AccountNumber,
                    AccountRef = x.AccountRef,
                    DestinationAccountNumber = x.PartnerAccountNumber,
                    DestinationRef = x.PartnerRef,
                    SupplierRef = x.SupplierRef,
                    ConsignmentRef = x.ConsignmentRef,
                    MovementDate = x.MovementDate,
                    EffectiveDate = x.EffectiveDate,
                    DehireDate = x.DehireDate
                }).ToList();
        }


        public void Handle(MovementsReconciled message)
        {
            ////Mark movements as reconciled
            //var movementsIds = message.MovementDetailsToReconcile.Keys.ToList();
            //var movements = _session.Load<Movement>(movementsIds).ToDictionary(x => x.IdString, y => y);

            ////Mark invoice items as reconciled
            //var invoice = _session.Load<InvoiceAttachment>(message.InvoiceId);
            //invoice.UnreconciledItems -= message.InvoiceDetailsToReconcile.Count;

            //var invoiceDetailsKeys = message.InvoiceDetailsToReconcile.Keys.ToList();
            //var invoiceDetails = invoice.Details.Where(x => x.DetailKey.In(invoiceDetailsKeys)).ToList();
            //invoiceDetails.ForEach(item =>
            //                           {
            //                               var movementId = message.InvoiceDetailsToReconcile[item.DetailKey];

            //                               item.IsReconciled = true;
            //                               item.MovementIdOfReconciliation = movementId;

            //                               movements[movementId].ReconciledEquipment = message.MovementDetailsToReconcile[movementId]
            //                                    .ToDictionary(x => x,
            //                                                  y => new ReconciliationData(DateTime.UtcNow, invoice.IdString, item.DetailKey));

            //                               movements[movementId].UnreconciledItems--;
            //                               if (movements[movementId].UnreconciledItems == 0)
            //                                   movements[movementId].IsReconciled = true;
            //                           });


        }

        public void Handle(MovementRecordMatched message)
        {
            ////Movement match -> store old data in PreReconciliationData, replace data, update unreconciled items count
            //var movement = _session.Load<Movement>(message.MovementId);

            //movement.PreReconciliationData.Add(message.MovementEquipmentCode,
            //    new PreReconciliationData(message.MatchedOn, message.MatchedBy, movement.MovementDate, movement.DocketNumber,
            //        movement.DestinationId, movement.MovementType, movement.Equipments[message.MovementEquipmentCode].Quantity,
            //        message.MatchedMovementDate, message.MatchedDocketNumber, message.MatchedDestinationId, message.MatchedMovementType, message.MatchedQuantity));

            //movement.MovementDate = message.MatchedMovementDate;
            //movement.DocketNumber = message.MatchedDocketNumber;
            //movement.DestinationId = message.MatchedDestinationId;
            //movement.MovementType = message.MatchedMovementType;
            //movement.Equipments[message.MatchedEquipmentCode].Quantity = message.MatchedQuantity;

            //movement.ReconciledEquipment.Add(message.MatchedEquipmentCode, new ReconciliationData(DateTime.UtcNow, message.InvoiceId.ToString(), message.DetailKey));
            //movement.UnreconciledItems--;
            //if (movement.UnreconciledItems == 0)
            //    movement.IsReconciled = true;

            ////Invoice detail match -> just update unreconciled items count and mark item as reconciled
            //var invoice = _session.Load<InvoiceAttachment>(message.InvoiceId);
            //var item = invoice.Details.FirstOrDefault(x => x.DetailKey == message.DetailKey);

            //invoice.UnreconciledItems--;
            //item.IsReconciled = true;
            //item.MovementIdOfReconciliation = movement.IdString;


        }

        public void Handle(MovementRecordRejected message)
        {
            ////Invoice detail match -> store our data in Rejection Data structure, update unreconciled items count and mark item as reconciled + rejected
            //var invoice = _session.Load<InvoiceAttachment>(message.InvoiceId);
            //var item = invoice.Details.FirstOrDefault(x => x.DetailKey == message.DetailKey);

            //item.RejectionData.Add(new RejectionData(message.RejectedOn, message.RejectedBy));

            //invoice.UnreconciledItems--;
            //item.IsReconciled = true;
            //item.IsRejected = true;


        }

        public void Handle(TransactionCreated message)
        {
            ////Reconcile invoice record
            //var invoice = _session.Load<InvoiceAttachment>(message.InvoiceId);
            //var item = invoice.Details.FirstOrDefault(x => x.DetailKey == message.DetailKey);

            //item.IsReconciled = true;
            //item.MovementIdOfReconciliation = message.MovementId.ToStringId<Movement>();
            //invoice.UnreconciledItems--;

            ////Reconcile movment record            
            //var movement = _session.Load<Movement>(message.MovementId);
            //movement.ReconciledEquipment = movement.Equipments
            //    .ToDictionary(x => x.Key,
            //                  y => new ReconciliationData(DateTime.UtcNow, message.InvoiceId.ToString(), message.DetailKey));
            //movement.UnreconciledItems = 0;
            //movement.IsReconciled = true;


        }


    }
}
