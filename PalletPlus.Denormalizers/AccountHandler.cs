﻿using System;
using System.Linq;
using NServiceBus;
using nTrack.Data.Extenstions;
using nTrack.Extension;
using PalletPlus.Data;
using PalletPlus.Messages.Events.Account;
using Raven.Client;
using Raven.Client.Connection;

namespace PalletPlus.Denormalizers
{
    public class AccountHandler :
        IHandleMessages<AccountCreated>,
        IHandleMessages<SupplierToAccountAdded>,
        IHandleMessages<AccountSupplierUpdated>,
        IHandleMessages<SupplierFromAccountRemoved>,
        IHandleMessages<CorrectionReasonAdded>,
        IHandleMessages<CorrectionReasonRemoved>,
        IHandleMessages<CancellationReasonAdded>,
        IHandleMessages<CancellationReasonRemoved>,
        IHandleMessages<DashboardEquipmentColorSet>,

        IHandleMessages<ExportSettingsChanged>,
        IHandleMessages<RequirementsEdited>
    {
        readonly IDocumentSession _session;
        readonly IDatabaseCommands _storeCommands;

        public AccountHandler(IDocumentSession session, IBus bus)
        {
            _session = session;
            _storeCommands = _session.Advanced.DocumentStore.DatabaseCommands.ForDatabase(bus.GetDatabaseName());
        }

        public void Handle(AccountCreated message)
        {
            var existing = _storeCommands.Head(message.AccountId.ToStringId<Account>());
            if (existing != null)
                throw new Exception("Account already exists!");


            //standard colours for dashboard (designed and put into set by graphic-super-man)
            var colors = new[]{
                new { R = 109, G = 64, B = 109 },
                new { R = 157, G = 190, B = 81 },
                new { R = 84, G = 36, B = 28 },
                new { R = 139, G = 59, B = 45 },
                new { R = 74, G = 64, B = 109 },
                new { R = 59, G = 87, B = 139 },
                new { R = 106, G = 139, B = 201 },
                new { R = 83, G = 158, B = 177 },
                new { R = 222, G = 152, B = 139 }
            }.ToDictionary(x => Guid.NewGuid(), y => new DashboardEquipmentColor { R = y.R, G = y.G, B = y.B });

            var account = new Account
                              {
                                  Id = message.AccountId,
                                  CompanyName = message.CompanyName,
                                  DashboardEquipmentColors = colors
                              };

            _session.Store(account);
        }
        public void Handle(SupplierToAccountAdded message)
        {
            var account = _session.Load<Account>(message.AccountId);
            account.Suppliers.Add(message.SupplierId.ToStringId<Supplier>(),
                                  new CompanySupplier
                                      {
                                          CompanyId = message.SupplierId.ToStringId<Supplier>(),
                                          CompanyName = message.SupplierName,
                                          AccountNumber = message.AccountNumber,
                                          Prefix = message.Prefix,
                                          SequenceNumber = message.SequenceStartNumber,
                                          Suffix = message.Suffix,
                                      });

        }
        public void Handle(AccountSupplierUpdated message)
        {
            var company = _session.Load<Account>(message.AccountId);
            var supplier = company.Suppliers[message.SupplierId.ToStringId<Supplier>()];

            supplier.Prefix = message.Prefix;
            supplier.Suffix = message.Suffix;
            supplier.SequenceNumber = message.SequenceStartNumber;

        }
        public void Handle(SupplierFromAccountRemoved message)
        {
            var company = _session.Load<Account>(message.AccountId);
            company.Suppliers.Remove(message.SupplierId.ToStringId<Supplier>());

        }
        public void Handle(CorrectionReasonAdded message)
        {
            var account = _session.Load<Account>(message.AccountId);
            account.CorrectionReasons.Add(message.ReasonKey, message.Reason);

        }
        public void Handle(CorrectionReasonRemoved message)
        {
            var account = _session.Load<Account>(message.AccountId);
            account.CorrectionReasons.Remove(message.ReasonKey);

        }
        public void Handle(CancellationReasonAdded message)
        {
            var account = _session.Load<Account>(message.AccountId);
            account.CancellationReasons.Add(message.ReasonKey, message.Reason);

        }
        public void Handle(CancellationReasonRemoved message)
        {
            var account = _session.Load<Account>(message.AccountId);
            account.CancellationReasons.Remove(message.ReasonKey);

        }
        public void Handle(DashboardEquipmentColorSet message)
        {
            var account = _session.Load<Account>(message.AccountId);
            var color = new DashboardEquipmentColor()
                            {
                                R = message.R,
                                G = message.G,
                                B = message.B,
                                EquipmentCode = message.EquipmentCode,
                                EquipmentName = message.EquipmentName
                            };
            if (!account.DashboardEquipmentColors.ContainsKey(message.ColorKey))
                account.DashboardEquipmentColors.Add(message.ColorKey, color);
            else
            {
                account.DashboardEquipmentColors[message.ColorKey] = color;
            }


        }
        public void Handle(ExportSettingsChanged message)
        {
            var company = _session.Load<Account>(message.AccountId);
            company.ExportMovementIn = message.ExportMovementIn;
            company.ExportMovementOut = message.ExportMovementOut;

        }
        public void Handle(RequirementsEdited message)
        {
            var company = _session.Load<Account>(message.AccountId);
            company.RequiredFields = message.RequiredFields;
        }
    }
}
