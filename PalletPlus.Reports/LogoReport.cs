﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.Reporting;

namespace PalletPlus.Reports
{
    public abstract class LogoReport : Report
    {
        public abstract PictureBox PictureBoxLogo { get; }

        public LogoReport()
        {
            
        }
    }
}
