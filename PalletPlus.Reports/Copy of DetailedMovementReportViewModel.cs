﻿using System;
using System.Collections.Generic;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using nTrack.Data.Indices;

namespace PalletPlus.Reports
{
    public class HireMovementBalanceReportViewModel
    {
        public HireMovementBalanceReport Report { get; set; }

        public HireMovementBalanceReportViewModel()
        {
        }

        public HireMovementBalanceReportViewModel(HireMovementBalanceReport report)
        {
            Report = report;
        }
    }
}
