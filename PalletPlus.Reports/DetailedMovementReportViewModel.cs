﻿using System;
using System.Collections.Generic;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using nTrack.Data.Indices;

namespace PalletPlus.Reports
{
    public class DetailedMovementReportViewModel
    {
        public DetailedMovementReport Report { get; set; }

        public DetailedMovementReportViewModel()
        {
        }

        public DetailedMovementReportViewModel(DetailedMovementReport report)
        {
            Report = report;
        }
    }
}
