using System.Collections.Generic;
using System.Configuration;
using System.Web;
using PalletPlus.Data.Indices;

namespace PalletPlus.Reports
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for DetailedMovementReport.
    /// </summary>
    public partial class DetailedMovementReport : Report, ILogoReport
    {
        public DetailedMovementReport()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            
            //
            // TODO: Add any constructor code after InitializeComponent call
            //                      
            //DataSource = new DetailedMovementReportViewModel();
        }

        public void SetLogo(Image logo)
        {
            if (logo != null)
                pictureBox1.Value = logo;
            else
            {
                pictureBox1.Value = Image.FromFile(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["SystemLogoPath"]));
            }
        }
    }
}