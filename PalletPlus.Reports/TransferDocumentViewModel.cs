﻿using System;
using System.Collections.Generic;
using System.Linq;
using PalletPlus.Data;

namespace PalletPlus.Reports
{
    public class TransferDocumentViewModel
    {
        public Movement Movement { get; set; }

        public List<EquipmentData> Equipments { get; set; }

        public TransferDocumentViewModel()
        {
            Movement = new Movement
                           {
                               SupplierName = "CHEP",
                               DocketNumber = "IDP000064SI",
                               //SiteName = "Site Name",
                               SiteAddress = "Site Address Blah Blah",
                               MovementType = "Transfer",
                               EffectiveDate = DateTime.Now.AddDays(5),
                               SiteAccountNumber = "1234567",
                               DestinationAccountNumber = "7654321",
                               DestinationAddress = "Destination Address Blah Blah",
                               MovementDate = DateTime.Now,
                               DestinationName = "Dest Name",
                               Equipments = new Dictionary<string, Movement.EquipmentInfo>()
                           };

            Movement.Equipments.Add("1", new Movement.EquipmentInfo { EquipmentName = "Name1", Quantity = 20 });
            Movement.Equipments.Add("2", new Movement.EquipmentInfo { EquipmentName = "Name2", Quantity = 20 });
            Movement.Equipments.Add("3", new Movement.EquipmentInfo { EquipmentName = "Name3", Quantity = 20 });

            Equipments = Movement.Equipments.Select(x => new EquipmentData()
            {
                EquipmentCode = x.Key,
                EquipmentName = x.Value.EquipmentName,
                Quantity = x.Value.Quantity,
                SupplierName = Movement.SupplierName
            }).ToList();
        }

        public TransferDocumentViewModel(Movement mov)
        {
            Movement = mov;

            Equipments = mov.Equipments.Select(x => new EquipmentData()
                                                        {
                                                            EquipmentCode = x.Key,
                                                            EquipmentName = x.Value.EquipmentName,
                                                            Quantity = x.Value.Quantity,
                                                            SupplierName = mov.SupplierName
                                                        }).ToList();
        }
    }

    public class EquipmentData
    {
        public string SupplierName { get; set; }
        public string EquipmentCode { get; set; }
        public string EquipmentName { get; set; }
        public int Quantity { get; set; }
    }
}
