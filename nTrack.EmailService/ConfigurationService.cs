﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace nTrack.EmailService
{
    public enum SettingName
    {
        SmtpHost,
        SmtpPort,
        SmtpUsername,
        SmtpPassword,
        SmtpSslEnable,
    }

    public class ConfigurationService
    {
        public static string GetSettingValue(SettingName settingName)
        {
            return ConfigurationManager.AppSettings[settingName.ToString()];
        }
    }
}
