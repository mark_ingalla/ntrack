﻿using System.Net.Mail;
using System.Net.Mime;
using NServiceBus;
using nTrack.EmailService.Messages.Commands;
using nTrack.EmailService.Messages.Events;

namespace nTrack.EmailService
{
    public class EmailHandler : IHandleMessages<SendEmail>
    {
        public IBus Bus { get; set; }

        public void Handle(SendEmail message)
        {
            var msg = new MailMessage
                {
                    From = new MailAddress(message.FromEmail, message.FromName),
                    Subject = message.Subject,
                    Body = message.TextBody,

                };
#if DEBUG
            msg.To.Add(new MailAddress("sarmaad@gmail.com", message.ToName));
#else
            msg.To.Add(new MailAddress(message.ToEmail, message.ToName));
#endif

            msg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(message.HtmlBody, new ContentType("text/html")));
            msg.Bcc.Add(message.Bcc);

            bool successful = true;
            string errorMessage = string.Empty;
            try
            {
                using (var smtp = new SmtpClient())
                    smtp.Send(msg);
            }
            catch (SmtpException exception)
            {
                successful = false;
                errorMessage = exception.Message;
            }

            Bus.Publish(new EmailSent(successful, message.ToEmail, errorMessage));
        }
    }
}
