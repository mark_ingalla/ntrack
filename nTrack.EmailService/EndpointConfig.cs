using System.Linq;
using NServiceBus.UnitOfWork;
using StructureMap;
using StructureMap.Pipeline;

namespace nTrack.EmailService 
{
    using NServiceBus;

	/*
		This class configures this endpoint as a Server. More information about how to configure the NServiceBus host
		can be found here: http://nservicebus.com/GenericHost.aspx
	*/
    [EndpointName("nTrack.EmailService")]
    public class EndpointConfig : IConfigureThisEndpoint, AsA_Server, IWantCustomInitialization
    {
        public void Init()
        {
            ObjectFactory.Configure(
                x => x.Scan(
                    scan =>
                    {
                        scan.AssembliesFromApplicationBaseDirectory();
                        scan.TheCallingAssembly();
                        scan.LookForRegistries();
                        scan.WithDefaultConventions();
                        scan.AddAllTypesOf<Profile>();
                    }));

            Configure.With()
                .StructureMapBuilder(ObjectFactory.Container)
                .DefiningCommandsAs(type => type != null && type.Namespace != null && type.Namespace.Contains("Messages.Commands"))
                //       .DefiningMessagesAs(type => type != null && type.Namespace != null && type.Namespace.StartsWith("nTrack.EmailService.Messages"))
                //.DefiningMessagesAs(type => type != null && type.Namespace != null && type.Namespace.StartsWith("Subscription.Messages"))
                .XmlSerializer()
                .MsmqTransport()
                .RunTimeoutManager()
                .UnicastBus()
                .CreateBus()
                .Start();

        }

    }
}