﻿using System;

namespace nTrack.Core.Messages
{
    public class InvoiceHeader
    {
        public string RECORDTYPE;
        public string INVOICENO;
        public string CUSTCODE;
        public string BATCHID;

        public DateTime INVOICEDATE;
        public string INVOICEYEAR;
        public string INVOICEPERIOD;
        public string INVOICETOTAL;
        public string DOLLARDRTOT;
        public string DOLLARCRTOT;
        public string QTYDRTOTAL;
        public string QTYCRTOTAL;
        public string STATUS;
        public string DEPOT;
        public string REF1DI;
        public string REF2;
        public string REF3;
        public string REF4;
        public string CUSTCODEGLID;
        public string SCPLANTCODE;
    }
}
