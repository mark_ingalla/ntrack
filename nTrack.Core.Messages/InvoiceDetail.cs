﻿using System;

namespace nTrack.Core.Messages
{
    public class InvoiceDetail
    {
        public Guid DetailKey;

        public string TRANSNUMBER;
        public string RECORDTYPE;
        public string CUSTCODE;
        public string INVOICENO;
        public string PCMSDOCKET;
        public string CHEPREF;
        public string CONNOTE;
        public string ITEM;
        public string DEPOT;
        public string EQUIPCODE;
        public string SUNDRYCODE;
        public string TRANSCODE;
        public string STATUS;
        public DateTime? EFFECTDATE;
        public DateTime? TRANSDATE;
        public string NARRATION;
        public string QUANTITY;
        public string VALUE;
        public string DAYSHIRE;
        public string TRACDAYS;
        public string OTHERDOCKET;
        public DateTime? OTHERDATE;
        public string INVOICECUST;
        public DateTime INVOICEDATE;
        public string INVOICEBALANCE;
        public string OTHERPARTY;
        public string OTHERPARTYNAME;
        public string REF1;
        public string REF2;
        public string REF3;
        public string REF4;
        public string REF5;
        public string DEEQUIPCODE;
        public DateTime? DEDATE;
        public DateTime? DISPATCHDATE;
        public DateTime? RECEIPTDATE;
        public string CUSTCODEGLID;
        public string SCPLANTCODE;
        public string INVOICECUSTGLID;
        public string OTHERPARTYGLID;
        public string EQUIPMATNR;
        public string DEEQUIPMATNR;
        public string CHEPCOMMENT1;
        public string CHEPCOMMENT2;
        public string CHEPCOMMENT3;
        public string INFORMER;
        public string TRANSPORTREF;
        public string TRANSPORTREGISTRATION;
        public bool CORRECTION;
    }
}
