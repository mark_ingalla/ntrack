﻿using System;

namespace nTrack.Core.Messages
{
    public abstract class MessageBase
    {
        protected MessageBase()
        {
            Id = Guid.NewGuid();
        }
        public Guid Id { get; set; }
    }
}
