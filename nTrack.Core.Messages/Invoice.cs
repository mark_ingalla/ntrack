﻿using System;
using System.Collections.Generic;

namespace nTrack.Core.Messages
{
    public class Invoice
    {
        public string SupplierId { get; set; }
        public DateTime DateImported { get; set; }
        public string FileName { get; set; }

        public InvoiceHeader Header { get; set; }
        public List<InvoiceDetail> Details { get; set; }
        public List<InvoiceDetail> DetailsSummary { get; set; } 

        public Invoice()
        {
            Details = new List<InvoiceDetail>();
            DetailsSummary = new List<InvoiceDetail>();
        }
    }
}
