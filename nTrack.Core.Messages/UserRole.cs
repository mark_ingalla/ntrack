﻿
namespace nTrack.Core.Messages
{
    public enum UserRole
    {
        Admin,
        Manager,
        User
    }
}