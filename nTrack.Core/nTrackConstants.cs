﻿
namespace nTrack.Core
{
    public static class nTrackConstants
    {
        public const string DatabaseHeaderKey = "nTrack.DatabaseName";
        public static string AccountId = "nTrack.AccountId";
        public static string UserId = "nTrack.UserId";
    }
}
