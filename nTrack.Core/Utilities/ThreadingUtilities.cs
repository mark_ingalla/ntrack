﻿using System;
using System.Threading;

namespace nTrack.Core.Utilities
{
    public class ThreadingUtilities : IThreadingUtilities
    {
        public bool WaitOnPredicate(int delayInMillisec, int timeoutInSec, Func<bool> predicate)
        {
            var deadline = DateTime.Now.AddSeconds(timeoutInSec);

            while (DateTime.Now < deadline)
            {
                Thread.Sleep(delayInMillisec);

                if (predicate.Invoke())
                    return true;
            }

            return false;
        }
    }
}