﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace nTrack.Core.Utilities
{
    public class ValidationUtilities : IValidationUtilities
    {
        public ICollection<ValidationResult> GetValidationErrors(object model)
        {
            var context = new ValidationContext(model, serviceProvider: null, items: null); 
            var results = new List<ValidationResult>();
            Validator.TryValidateObject(model, context, results, validateAllProperties: true);

            return results;
        }

        public bool IsValid(object model)
        {
            var context = new ValidationContext(model, serviceProvider: null, items: null);
            return Validator.TryValidateObject(model, context, new List<ValidationResult>(), validateAllProperties: true);
        }
    }
}