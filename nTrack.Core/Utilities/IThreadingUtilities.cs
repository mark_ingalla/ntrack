﻿using System;

namespace nTrack.Core.Utilities
{
    public interface IThreadingUtilities
    {
        bool WaitOnPredicate(int delayInMillisec, int timeoutInSec, Func<bool> predicate);
    }
}