﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace nTrack.Core.Utilities
{
    public class CryptographyService : ICryptographyService
    {
        private const string SALT = "knHgtV23G4lp1Anv9XkPfJYATk6wMnkl";


        public string Encrypt(string input, string secret)
        {
            // Create the encryptor based on the constant salt value and the given secret.
            byte[] saltBytes = Encoding.ASCII.GetBytes(SALT);
            Rfc2898DeriveBytes keyIVCreator = new Rfc2898DeriveBytes(secret, saltBytes);
            RijndaelManaged encryptionAlgorithm = new RijndaelManaged();
            encryptionAlgorithm.Key = keyIVCreator.GetBytes(encryptionAlgorithm.KeySize / 8);
            encryptionAlgorithm.IV = keyIVCreator.GetBytes(encryptionAlgorithm.BlockSize / 8);

            using (ICryptoTransform encryptor = encryptionAlgorithm.CreateEncryptor())
            {
                using (MemoryStream encryptedStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(encryptedStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter cryptoStreamWriter = new StreamWriter(cryptoStream))
                        {
                            cryptoStreamWriter.Write(input);
                            cryptoStreamWriter.Flush();
                            cryptoStream.FlushFinalBlock();
                            return Convert.ToBase64String(encryptedStream.ToArray());
                        }
                    }
                }
            }
        }
        public string Decrypt(string encryptedInput, string secret)
        {
            // Create the decryptor based on the constant salt value and the given secret.
            byte[] saltBytes = Encoding.ASCII.GetBytes(SALT);
            Rfc2898DeriveBytes keyIVCreator = new Rfc2898DeriveBytes(secret, saltBytes);
            RijndaelManaged encriptionAlgorithm = new RijndaelManaged();
            encriptionAlgorithm.Key = keyIVCreator.GetBytes(encriptionAlgorithm.KeySize / 8);
            encriptionAlgorithm.IV = keyIVCreator.GetBytes(encriptionAlgorithm.BlockSize / 8);

            using (ICryptoTransform decryptor = encriptionAlgorithm.CreateDecryptor())
            {
                // Decode the base 64 encoded bytes.
                byte[] encryptedInputBytes = Convert.FromBase64String(encryptedInput);
                using (MemoryStream encryptedStream = new MemoryStream(encryptedInputBytes))
                {
                    using (CryptoStream cryptoStream = new CryptoStream(encryptedStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader cryptoStreamReader = new StreamReader(cryptoStream))
                        {
                            return cryptoStreamReader.ReadToEnd();
                        }
                    }
                }
            }
        }
        public string GetMD5Hash(string input)
        {
            if (input == null)
                throw new ArgumentNullException("input");

            byte[] originalBytes = Encoding.UTF8.GetBytes(input);
            byte[] hashedBytes;
            using (MD5 md5 = MD5.Create())
            {
                hashedBytes = md5.ComputeHash(originalBytes);
            }
            StringBuilder sBuilder = new StringBuilder();
            foreach (byte b in hashedBytes)
                sBuilder.Append(b.ToString("x2"));

            return sBuilder.ToString();
        }
    }
}