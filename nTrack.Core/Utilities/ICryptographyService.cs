﻿
namespace nTrack.Core.Utilities
{
    public interface ICryptographyService
    {
        string Encrypt(string input, string secret);
        string Decrypt(string encryptedInput, string secret);
        string GetMD5Hash(string input);
    }
}