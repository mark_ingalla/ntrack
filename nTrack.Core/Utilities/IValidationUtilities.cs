﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace nTrack.Core.Utilities
{
    public interface IValidationUtilities
    {
        ICollection<ValidationResult> GetValidationErrors(object model);
        bool IsValid(object model);
    }
}