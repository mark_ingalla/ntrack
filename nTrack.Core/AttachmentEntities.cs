﻿
namespace nTrack.Core.Enums
{
    public enum AttachmentEntities
    {
        AccountLogos,
        ProductLogos,
        PartnersDocuments
    }
}
