﻿using nTrack.Core.Utilities;
using StructureMap.Configuration.DSL;

namespace nTrack.Core
{
    public class CoreRegistry:Registry
    {
        public CoreRegistry()
        {
            For<ICryptographyService>().Use<CryptographyService>();
            For<IThreadingUtilities>().Use<ThreadingUtilities>();
        }
    }
}
