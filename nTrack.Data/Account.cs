﻿using System;
using System.Collections.Generic;
using Raven.Imports.Newtonsoft.Json;

namespace nTrack.Data
{
    public abstract class Account : BaseModel, IClientDatabase
    {
        public string CompanyName { get; set; }
        public string CompanyPhone { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }

        public List<User> Users { get; set; }
        public Dictionary<string, AccountDatabase> Databases { get; set; }

        protected Account()
        {
            Users = new List<User>();
            Databases = new Dictionary<string, AccountDatabase>();
        }
    }

    public class User
    {
        public Guid UserKey { get; set; }

        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }

        public string Role { get; set; }
        public bool IsLocked { get; set; }
        public bool IsAdministrator { get; set; }
        public bool IsDeleted { get; set; }

        [JsonIgnore]
        public string FullName { get { return string.Format("{0} {1}", FirstName, LastName).Trim(); } }

        public List<string> ActivityLog { get; set; }
        public List<Guid> PendingResetPasswordTokens { get; set; }

        public User()
        {
            ActivityLog = new List<string>();
            PendingResetPasswordTokens = new List<Guid>();
        }
    }
}
