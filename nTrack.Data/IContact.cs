﻿namespace nTrack.Data
{
    public interface IContact
    {
        string ContactName { get; set; }
        string ContactEmail { get; set; }
        string ContactPhone { get; set; }
    }
}