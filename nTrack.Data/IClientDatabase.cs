﻿
using System.Collections.Generic;

namespace nTrack.Data
{
    public interface IClientDatabase
    {
        /// <summary>
        /// Product Code is the key to access the database
        /// </summary>
        Dictionary<string, AccountDatabase> Databases { get; set; }
    }

    public class AccountDatabase
    {
        public string Name { get; set; }
        public string ProductId { get; set; }
        public string ProductCode { get; set; }

    }
}
