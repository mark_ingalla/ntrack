﻿
namespace nTrack.Data
{
    public class Countries
    {
        public static string EntityId { get { return "Countries"; } }

        public string Id { get; set; }
        public string[] CountryNames { get; set; }


        public Countries()
        {
            Id = EntityId;
        }
    }
}