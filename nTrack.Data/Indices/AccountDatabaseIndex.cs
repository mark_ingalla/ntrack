﻿using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;
using System.Linq;

namespace nTrack.Data.Indices
{
    public class AccountDatabaseIndex : AbstractIndexCreationTask<Account, AccountDatabaseIndex.Result>
    {
        public class Result
        {
            public string AccountId { get; set; }
            public string CompanyName { get; set; }
            public bool IsActive { get; set; }
            public bool IsDeleted { get; set; }
            public string DatabaseName { get; set; }
            public string ProductCode { get; set; }
            public string ProductId { get; set; }
        }

        public AccountDatabaseIndex()
        {

            Map = accounts => from account in accounts
                              from db in account.Databases
                              select new
                                         {
                                             AccountId = account.IdString,
                                             CompanyName = account.CompanyName,
                                             IsActive = account.IsActive,
                                             IsDeleted = account.IsDeleted,
                                             DatabaseName = db.Value.Name,
                                             ProductCode = db.Key,
                                             ProductId = db.Value.ProductId
                                         };

            Reduce = results => from r in results
                                group r by r.AccountId
                                    into gr
                                    from g in gr
                                    select new Result
                                               {
                                                   AccountId = g.AccountId,
                                                   CompanyName = g.CompanyName,
                                                   IsActive = g.IsActive,
                                                   IsDeleted = g.IsDeleted,
                                                   DatabaseName = g.DatabaseName,
                                                   ProductCode = g.ProductCode,
                                                   ProductId = g.ProductId
                                               };

            StoreAllFields(FieldStorage.Yes);

        }
    }
}
