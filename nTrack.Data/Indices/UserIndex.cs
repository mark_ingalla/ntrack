﻿using System;
using System.Collections.Generic;
using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace nTrack.Data.Indices
{
    public class UserIndex : AbstractIndexCreationTask<Account, UserIndex.Result>
    {
        public class Result
        {
            public string AccountId { get; set; }
            public string CompanyName { get; set; }

            public Guid UserKey { get; set; }
            public string UserFullName { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
            public string UserPhone { get; set; }
            public string UserMobile { get; set; }
            public string Role { get; set; }

            public bool IsLocked { get; set; }
            public bool IsAdministrator { get; set; }
            public bool IsDeleted { get; set; }

            public DateTime AccountUpdateOn { get; set; }
            public List<Guid> ResetPasswordTokens { get; set; }
        }

        public UserIndex()
        {
            Map = accounts => from account in accounts
                              from user in account.Users
                              select new
                                  {
                                      AccountId = account.IdString,
                                      Password = user.Password,
                                      Email = user.Email,
                                      UserKey = user.UserKey,
                                      IsLocked = user.IsLocked,
                                      AccountUpdateOn = account.UpdatedOn,
                                      UserFullName = user.FirstName + " " + user.LastName,
                                      UserPhone = user.Phone,
                                      UserMobile = user.Mobile,
                                      Role = user.Role,
                                      ResetPasswordTokens = user.PendingResetPasswordTokens,
                                      CompanyName = account.CompanyName,
                                      IsAdministrator = user.IsAdministrator,
                                      IsDeleted = user.IsDeleted
                                  };

            Reduce = results => from r in results
                                group r by r.UserKey
                                    into gr
                                    from g in gr
                                    select new Result
                                    {
                                        AccountId = g.AccountId,
                                        UserKey = g.UserKey,
                                        Email = g.Email,
                                        Password = g.Password,
                                        IsLocked = g.IsLocked,
                                        AccountUpdateOn = g.AccountUpdateOn,
                                        UserFullName = g.UserFullName,
                                        UserPhone = g.UserPhone,
                                        UserMobile = g.UserMobile,
                                        Role = g.Role,
                                        ResetPasswordTokens = g.ResetPasswordTokens,
                                        CompanyName = g.CompanyName,
                                        IsAdministrator = g.IsAdministrator,
                                        IsDeleted = g.IsDeleted
                                    };


            Index(x => x.Email, FieldIndexing.NotAnalyzed);
            Index(x => x.Password, FieldIndexing.NotAnalyzed);

            StoreAllFields(FieldStorage.Yes);
        }
    }
}
