﻿using System;

namespace nTrack.Data
{
    public class BaseModel
    {
        public Guid Id { get; set; }
        public string IdString { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
    }
}
