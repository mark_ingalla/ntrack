﻿using System;
using Raven.Client;

namespace nTrack.Data
{
    public interface ISystemDb
    {
        IDocumentStore Store { get; }
        string GetClientDatabaseName(Guid accountId);
    }
}