﻿using System;

namespace nTrack.Data.Extenstions
{
    public static class DocHelpers
    {
        public static string ToStringId<T>(this Guid value) where T : BaseModel
        {
            return Raven.Client.Util.Inflector.Pluralize(typeof(T).Name).ToLower() + "/" + value.ToString();
        }

        public static Guid ToGuidId(this string value)
        {
            var split = value.Split('/');
            if (split.Length <= 0) return Guid.Empty;

            Guid gValue;

            return Guid.TryParse(split[1], out gValue) ? gValue : Guid.Empty;
        }

        public static string CollectionName<T>()
        {
            return Raven.Client.Util.Inflector.Pluralize(typeof(T).Name).ToLower() + "/";
        }
    }
}
