﻿namespace nTrack.Data
{
    public interface ICompany
    {
        string CompanyName { get; set; }
        string CompanyPhone { get; set; }
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
        string Suburb { get; set; }
        string State { get; set; }
        string Postcode { get; set; }
        string Country { get; set; }
    }
}