﻿namespace Subscription.Services.Email.TemplateData
{
    public class ResetPasswordEmailData : BaseTemplateData
    {
        protected override string TemplateName
        {
            get { return "ResetPasswordEmail"; }
        }

        public ResetPasswordEmailData(string fullName, string resetPasswordUrl)
        {
            Data = new { UserFullName = fullName, ResetPasswordUrl = resetPasswordUrl};
        }
    }
}
