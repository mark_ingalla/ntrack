﻿namespace Subscription.Services.Email.TemplateData
{
    public class VerificationEmailData : BaseTemplateData
    {
        protected override string TemplateName
        {
            get { return "VerificationEmail"; }
        }

        public VerificationEmailData(string fullname, string verificationUrl, string offerName, string productName)
        {
            Data = new
                {
                    UserFullName = fullname,
                    OfferName = offerName,
                    VerificationUrl = verificationUrl, 
                    ProductName = productName
                };
        }
    }
}
