﻿namespace Subscription.Services.Email.TemplateData
{
    public class NewPasswordEmailData : BaseTemplateData
    {
        protected override string TemplateName
        {
            get { return "NewPasswordEmail"; }
        }

        public NewPasswordEmailData(string fullName, string newPassword)
        {
            Data = new { UserFullName = fullName, NewPassword = newPassword };
        }
    }
}
