﻿namespace Subscription.Services.Email.TemplateData
{
    public interface ITemplateData
    {
        string PlainTemplateName { get; }
        string HtmlTemplateName { get; }
        dynamic Data { get; }
    }
}
