﻿namespace Subscription.Services.Email.TemplateData
{
    public abstract class BaseTemplateData : ITemplateData
    {
        protected abstract string TemplateName { get; }

        public string PlainTemplateName { get { return "Plain/" + TemplateName + ".html"; } }
        public string HtmlTemplateName { get { return "Html/" + TemplateName + ".html"; } }
        public dynamic Data { get; protected set; }

    }
}
