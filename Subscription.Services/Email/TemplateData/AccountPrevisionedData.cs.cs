﻿namespace Subscription.Services.Email.TemplateData
{
    public class AccountPrevisionedData : BaseTemplateData
    {
        protected override string TemplateName
        {
            get { return "AccountPrevisionedEmail"; }
        }

        public AccountPrevisionedData(string fullname, string productName, string productUrl)
        {
            Data = new
            {
                UserFullName = fullname,
                ProductName = productName,
                ProductUrl = productUrl
            };
        }
    }
}
