﻿using System;

namespace Subscription.Services.Email.TemplateData
{
    public class InvoicePaidEmailData : BaseTemplateData
    {
        protected override string TemplateName
        {
            get { return "InvoicePaidEmail"; }
        }

        public InvoicePaidEmailData(string fullName, DateTime paidOnUTC)
        {
            Data = new { UserFullName = fullName, PaidOnUTC = paidOnUTC };
        }
    }
}
