﻿namespace Subscription.Services.Email.TemplateData
{
    public class UserAddedEmailData : BaseTemplateData
    {
        protected override string TemplateName
        {
            get { return "UserAddedEmail"; }
        }

        public UserAddedEmailData(string fullname, string login, string password, string loginLink)
        {
            Data = new
            {
                UserFullName = fullname,
                Login = login,
                Password = password,
                LoginLink = loginLink
            };
        }
    }
}
