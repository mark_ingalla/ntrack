﻿using System;

namespace Subscription.Services.Email.TemplateData
{
    public class InvoiceCreatedEmailData : BaseTemplateData
    {
        protected override string TemplateName
        {
            get { return "InvoiceCreatedEmail"; }
        }

        public InvoiceCreatedEmailData(string fullName, DateTime createOnUTC)
        {
            Data = new { UserFullName = fullName, CreateOnUTC = createOnUTC };
        }
    }
}
