﻿using System;
using System.IO;
using System.Linq;
using RazorTemplates.Core;
using Subscription.Services.Email.TemplateData;

namespace Subscription.Services.Email
{
    public class EmailMessage
    {
        public string HtmlBody { get; set; }
        public string PlainBody { get; set; }
        public string Subject { get; set; }
    }

    public static class EmailProcess
    {
        public static EmailMessage GetMessage(ITemplateData templateData)
        {
            string stringTemplatePlain = GetPlainTemplate(templateData.PlainTemplateName);
            string stringTemplateHtml = GetPlainTemplate(templateData.HtmlTemplateName);


            string htmlBody = RenderTemplate(stringTemplateHtml, templateData.Data);
            string plainBody = RenderTemplate(stringTemplatePlain, templateData.Data);

            string subject = GetSubject(plainBody);

            RidOffSubject(ref plainBody);
            RidOffSubject(ref htmlBody);

            return new EmailMessage() {HtmlBody = htmlBody, PlainBody = plainBody, Subject = subject};
        }

        private static string GetPlainTemplate(string templateName)
        {
            string contents = File.ReadAllText(ConfigurationService.GetSettingValue(SettingName.EmailTemplatesPath) + templateName);
            return contents;
        }

        private static string GetSubject(string template)
        {
            string firstLine = template.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries).ToArray()[0];

            int startIndex = firstLine.IndexOf("%%", System.StringComparison.Ordinal);


            if (startIndex < 0)
            {
                throw new Exception("Subject was not found in template, it should be placed in first line by '%%' mark");
            }

            return firstLine.Replace("%%", "");
        }

        private static string RenderTemplate(string stringTemplate, dynamic data)
        {
            var template = Template.Compile(stringTemplate);
            return template.Render(data);
        }

        private static void RidOffSubject(ref string template)
        {
            string[] lines = template.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Skip(1).ToArray();
            string output = string.Join(Environment.NewLine, lines);

            template = output;
        }
    }
}
