﻿using System;
using System.Collections.Generic;
using System.Linq;
using NServiceBus;
using NServiceBus.Saga;
using PalletPlus.Messages.Events.Site;
using Raven.Client;
using StructureMap.Attributes;
using Subscription.Data;
using Subscription.Messages.Events.Account.User;
using Subscription.Services.Messages.Commands.Email;

namespace Subscription.Services.Email
{

    public class UserAddedSagaData : ISagaEntity
    {
        public Guid Id { get; set; }
        public string Originator { get; set; }
        public string OriginalMessageId { get; set; }

        public Guid UserKey { get; set; }

        public string Password { get; set; }

    }

    public class UserAddedSaga : Saga<UserAddedSagaData>,
        IAmStartedByMessages<UserAdded>,
        IHandleMessages<UserToSiteAdded>,
        IHandleMessages<UserRemoved>
    {

        [SetterProperty]
        public IDocumentSession Session { get; set; }

        public override void ConfigureHowToFindSaga()
        {
            ConfigureMapping<UserToSiteAdded>(s => s.UserKey, p => p.UserId);
            ConfigureMapping<UserRemoved>(s => s.UserKey, p => p.UserKey);
        }


        public void Handle(UserAdded message)
        {
            Data.Password = message.Password;
            Data.UserKey = message.UserKey;
        }

        public void Handle(UserToSiteAdded message)
        {
            var toEmails = new Dictionary<string, string>();
            toEmails.Add(message.Email, message.FullName);

            var product = Session.Query<Product>().Single(p => p.Code == "PP");


            Bus.Send(new SendUserAddedEmail()
                         {
                             Login = message.Email,
                             Password = Data.Password,
                             LoginLink = product.AccessUrl,
                             ToEmails = toEmails
                         });

            MarkAsComplete();
        }

        public void Handle(UserRemoved message)
        {
            MarkAsComplete();
        }
    }
}
