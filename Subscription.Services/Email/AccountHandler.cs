﻿using System.Collections.Generic;
using System.Linq;
using NServiceBus;
using nTrack.Core.Messages;
using PalletPlus.Messages.Events.Site;
using Raven.Client;
using StructureMap.Attributes;
using Subscription.Data;
using Subscription.Messages.Events.Account;
using Subscription.Services.Messages.Commands.Email;
using nTrack.Data;
using nTrack.Data.Indices;
using Account = Subscription.Data.Account;

namespace Subscription.Services.Email
{
    public class AccountHandler :
        IHandleMessages<InvoicePaid>,
        IHandleMessages<InvoiceRaised>,
        IHandleMessages<PalletPlus.Messages.Events.Account.AccountCreated>,
        IHandleMessages<PalletPlus.Messages.Events.Site.UserToSiteAdded>
    {
        [SetterProperty]
        public IDocumentSession Session { get; set; }

        public IBus Bus { get; set; }

        public void Handle(InvoicePaid message)
        {
            var account = Session.Load<Account>(message.AccountId);
            var admins = account.Users.Where(x => x.Role == UserRole.Admin.ToString() && !x.IsDeleted).ToList();

            Bus.Send(new SendInvoicePaidEmail
            {
                ToEmails = admins.ToDictionary(x => x.Email, y => y.FullName),
                PaidOnUTC = message.PaidOnUTC
            });

        }
        public void Handle(InvoiceRaised message)
        {
            var account = Session.Load<Account>(message.AccountId);
            var admins = account.Users.Where(x => x.Role == UserRole.Admin.ToString() && !x.IsDeleted).ToList();

            Bus.Send(new SendInvoiceRaisedEmail
            {
                ToEmails = admins.ToDictionary(x => x.Email, y => y.FullName),
                InvoiceDateUTC = message.InvoiceDateUTC
            });
        }

        public void Handle(PalletPlus.Messages.Events.Account.AccountCreated message)
        {
            var account = Session.Load<Account>(message.AccountId);
            var admins = account.Users.Where(p => p.Role == UserRole.Admin.ToString() && !p.IsDeleted).ToList();
            var productId = account.Subscriptions.First().ProductId;
            var product = Session.Load<Product>(productId);
            Bus.Send(new SendPrevisionedEmail
                {
                    ProductName = product.Name,
                    ToEmails = admins.ToDictionary(x => x.Email, y => y.FullName),
                    ProductUrl = product.AccessUrl
                });
        }

        //public void Handle(UserAdded message)
        //{
        //    var toEmails = new Dictionary<string, string>();
        //    toEmails.Add(message.Email, message.FirstName + " " + message.LastName);

        //    Bus.Send(new SendUserAddedEmail()
        //                 {
        //                     Login = message.Email,
        //                     Password = message.Password,
        //                     LoginLink = "http://localhost:34308/", //TODO: We need to change it. 
        //                     ToEmails = toEmails
        //                 });
        //}

        public void Handle(UserToSiteAdded message)
        {
            var user = Session.Query<UserIndex.Result, UserIndex>().Single(p => p.UserKey == message.UserId);

            //TODO: at this point, send the user welcome email to the PP product
        }
    }
}
