﻿using System.Collections.Generic;
using System.Configuration;
using NServiceBus;
using nTrack.EmailService.Messages.Commands;
using StructureMap.Attributes;
using Subscription.Services.Email.TemplateData;
using Subscription.Services.Messages.Commands.Email;

namespace Subscription.Services.Email
{
    public class EmailHandler :
        IHandleMessages<SendVerificationEmail>,
        IHandleMessages<SendResetPasswordEmail>,
        IHandleMessages<SendNewPasswordEmail>,
        IHandleMessages<SendInvoicePaidEmail>,
        IHandleMessages<SendInvoiceRaisedEmail>,
        IHandleMessages<SendPrevisionedEmail>,
        IHandleMessages<SendUserAddedEmail>
    {
        [SetterProperty]
        public IBus Bus { get; set; }

        public void Handle(SendVerificationEmail message)
        {
            foreach (var email in message.ToEmails)
            {
                Send(email, new VerificationEmailData(email.Value, message.VerificationUrl, message.OfferName, message.ProductName));
            }
        }
        public void Handle(SendResetPasswordEmail message)
        {
            foreach (KeyValuePair<string, string> email in message.ToEmails)
            {
                Send(email, new ResetPasswordEmailData(email.Value, message.ResetPasswordUrl));
            }
        }
        public void Handle(SendNewPasswordEmail message)
        {
            foreach (KeyValuePair<string, string> email in message.ToEmails)
            {
                Send(email, new NewPasswordEmailData(email.Value, message.NewPassword));
            }
        }
        public void Handle(SendInvoicePaidEmail message)
        {
            foreach (KeyValuePair<string, string> email in message.ToEmails)
            {
                Send(email, new InvoicePaidEmailData(email.Value, message.PaidOnUTC));
            }
        }
        public void Handle(SendInvoiceRaisedEmail message)
        {
            foreach (KeyValuePair<string, string> email in message.ToEmails)
            {
                Send(email, new InvoiceCreatedEmailData(email.Value, message.InvoiceDateUTC));
            }
        }
        public void Handle(SendPrevisionedEmail message)
        {
            foreach (KeyValuePair<string, string> email in message.ToEmails)
            {
                Send(email, new AccountPrevisionedData(email.Value, message.ProductName, message.ProductUrl));
            }
        }

        private void Send(KeyValuePair<string, string> to, ITemplateData templatData)
        {
            var emailMessage = EmailProcess.GetMessage(templatData);

            var fromName = ConfigurationManager.AppSettings["Email/From/Name"];
            var fromEmail = ConfigurationManager.AppSettings["Email/From/Email"];
            var bccEmail = ConfigurationManager.AppSettings["Email/Bcc/Email"];

            Bus.Send(new SendEmail
                {
                    FromEmail = fromEmail,
                    FromName = fromName,
                    HtmlBody = emailMessage.HtmlBody,
                    TextBody = emailMessage.PlainBody,
                    Subject = emailMessage.Subject,
                    ToEmail = to.Key,
                    ToName = to.Value,
                    Bcc = bccEmail
                });
        }

        public void Handle(SendUserAddedEmail message)
        {
            foreach (KeyValuePair<string, string> email in message.ToEmails)
            {
                Send(email, new UserAddedEmailData(email.Value, message.Login, message.Password, message.LoginLink));
            }
        }
    }
}
