﻿using System.Configuration;

namespace Subscription.Services
{
    public enum SettingName
    {
        EmailTemplatesPath
    }

    public class ConfigurationService
    {
        public static string GetSettingValue(SettingName settingName)
        {
            return ConfigurationManager.AppSettings[settingName.ToString()];
        }
    }
}
