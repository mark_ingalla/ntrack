﻿using System.Linq;
using NServiceBus;
using nTrack.Data;
using nTrack.Data.Extenstions;
using nTrack.Data.Indices;
using Raven.Client;
using Raven.Client.Extensions;
using StructureMap.Attributes;
using Subscription.Data;
using Subscription.Messages.Events;
using Subscription.Messages.Events.Account;
using Account = Subscription.Data.Account;

namespace Subscription.Services.Subscription
{
    public class SubscriptionHandler :
        IHandleMessages<SubscriptionCreated>
    {
        public IBus Bus { get; set; }

        [SetterProperty]
        public IDocumentSession Session { get; set; }


        public void Handle(SubscriptionCreated message)
        {
            var account = Session.Load<Account>(message.AccountId);

            //check if a database already exists for this product
            if (account.Databases.ContainsKey(message.ProductCode)) return;

            var dbName = account.CompanyName;
            dbName = dbName.Replace(" ", "");
            dbName = dbName.Replace("+", "");
            dbName = dbName.Replace("/", "");
            dbName = dbName.Replace(@"\", "");
            dbName = dbName.Replace(@".", "");
            dbName = dbName.Replace("\"", "");
            dbName = dbName.Replace("'", "");
            dbName = dbName.Replace("~", "");
            dbName = dbName.Replace("!", "");
            dbName = string.Format("nTrack.{1}_{0}", dbName, message.ProductCode);

            var count = 0;
            while (Session.Query<AccountDatabaseIndex.Result, AccountDatabaseIndex>()
                .Customize(x => x.WaitForNonStaleResults())
                .Any(x => x.DatabaseName == dbName))
            {
                count++;
                dbName = string.Format("{0}_{1}", dbName, count);
            }

            account.Databases.Add(message.ProductCode, new AccountDatabase
                                                           {
                                                               Name = dbName,
                                                               ProductId = message.ProductId.ToStringId<Product>(),
                                                               ProductCode = message.ProductCode
                                                           });

            Session.Advanced.DocumentStore.DatabaseCommands.EnsureDatabaseExists(dbName);

            Bus.Publish(new DatabaseReady
                            {
                                AccountId = message.AccountId,
                                DatabaseName = dbName,
                                ProductCode = message.ProductCode,
                                ProductId = message.ProductId
                            });
        }
    }
}
