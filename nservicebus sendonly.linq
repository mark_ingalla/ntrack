<Query Kind="Program">
  <Reference Relative="packages\CommonDomain.1.4.0\lib\net40\CommonDomain.dll">D:\idp_ntrack\trunk\packages\CommonDomain.1.4.0\lib\net40\CommonDomain.dll</Reference>
  <Reference Relative="packages\EventStore.3.0.11326.44\lib\net40\EventStore.dll">D:\idp_ntrack\trunk\packages\EventStore.3.0.11326.44\lib\net40\EventStore.dll</Reference>
  <Reference Relative="packages\EventStore.Serialization.Json.3.0.11326.44\lib\net40\EventStore.Serialization.Json.dll">D:\idp_ntrack\trunk\packages\EventStore.Serialization.Json.3.0.11326.44\lib\net40\EventStore.Serialization.Json.dll</Reference>
  <Reference Relative="packages\log4net.1.2.10\lib\2.0\log4net.dll">D:\idp_ntrack\trunk\packages\log4net.1.2.10\lib\2.0\log4net.dll</Reference>
  <Reference Relative="packages\Newtonsoft.Json.4.0.8\lib\net40\Newtonsoft.Json.dll">D:\idp_ntrack\trunk\packages\Newtonsoft.Json.4.0.8\lib\net40\Newtonsoft.Json.dll</Reference>
  <Reference Relative="packages\NLog.2.0.0.2000\lib\net40\NLog.dll">D:\idp_ntrack\trunk\packages\NLog.2.0.0.2000\lib\net40\NLog.dll</Reference>
  <Reference Relative="packages\NServiceBus.3.2.0\lib\net40\NServiceBus.Core.dll">D:\idp_ntrack\trunk\packages\NServiceBus.3.2.0\lib\net40\NServiceBus.Core.dll</Reference>
  <Reference Relative="packages\NServiceBus.3.2.0\lib\net40\NServiceBus.dll">D:\idp_ntrack\trunk\packages\NServiceBus.3.2.0\lib\net40\NServiceBus.dll</Reference>
  <Reference Relative="packages\NServiceBus.Structuremap.3.2.0\lib\net40\NServiceBus.ObjectBuilder.StructureMap.dll">D:\idp_ntrack\trunk\packages\NServiceBus.Structuremap.3.2.0\lib\net40\NServiceBus.ObjectBuilder.StructureMap.dll</Reference>
  <Reference Relative="Subscription\bin\Debug\nTrack.Core.Messages.dll">D:\idp_ntrack\trunk\Subscription\bin\Debug\nTrack.Core.Messages.dll</Reference>
  <Reference Relative="Subscription\bin\Debug\nTrack.EmailService.Messages.dll">D:\idp_ntrack\trunk\Subscription\bin\Debug\nTrack.EmailService.Messages.dll</Reference>
  <Reference Relative="packages\RavenDB.Client.1.0.888\lib\net40\Raven.Abstractions.dll">D:\idp_ntrack\trunk\packages\RavenDB.Client.1.0.888\lib\net40\Raven.Abstractions.dll</Reference>
  <Reference Relative="packages\RavenDB.Client.1.0.888\lib\net40\Raven.Client.Lightweight.dll">D:\idp_ntrack\trunk\packages\RavenDB.Client.1.0.888\lib\net40\Raven.Client.Lightweight.dll</Reference>
  <Reference Relative="packages\structuremap.2.6.3\lib\StructureMap.dll">D:\idp_ntrack\trunk\packages\structuremap.2.6.3\lib\StructureMap.dll</Reference>
  <Reference Relative="Subscription\bin\Debug\Subscription.dll">D:\idp_ntrack\trunk\Subscription\bin\Debug\Subscription.dll</Reference>
  <Reference Relative="Subscription\bin\Debug\Subscription.Messages.dll">D:\idp_ntrack\trunk\Subscription\bin\Debug\Subscription.Messages.dll</Reference>
  <Namespace>NServiceBus</Namespace>
  <Namespace>Subscription.Messages.Commands</Namespace>
</Query>

void Main()
{
				var bus = Configure.With(typeof(nTrack.Core.Messages.MessageBase).Assembly)
				.DefineEndpointName("debugrunner")
				.DefaultBuilder()
				.DefiningCommandsAs(type => type != null && type.Namespace == "Subscription.Messages.Commands")
				.DefiningEventsAs(type => type != null && type.Namespace == "Subscription.Messages.Events")
				.DefiningMessagesAs(type => type != null && type.Namespace == "Subscription.Messages")
				.JsonSerializer()
				.MsmqTransport()
				.UnicastBus()
				.SendOnly()
				;								
				
	var accountgid = Guid.NewGuid();	
	accountgid.Dump();
	
	var emailverToken = Guid.NewGuid();
	emailverToken.Dump();		
	
	var userId = Guid.NewGuid();
	userId.Dump();		
				
	var v = bus.CreateInstance<Register>(e =>
		{
				e.UserId = userId;
				e.AdminEmail = "test@mail.com";
				e.AccountId = accountgid;
				e.AddressLine1 = "aaaaaaaa";
				e.CompanyName = "aaaaaaaa";
				e.CompanyPhone = "aaaaaaaa";
				e.EmailVerificationToken = emailverToken;
				e.CompanySubrub = "aaaaaaaa";
				e.CompanyState = "aaaaaaaa";
				e.CompanyPostcode = "aaaaaaaa";
				e.CompanyCountry = "aaaaaaaa";
				e.AdminFirstName = "aaaaaaaa";
				e.AdminLastName = "aaaaaaaa";
				e.AdminPassword = "aaaaaaaa";
				e.IPAddress = "aaaaaaaa";
		});
	
	bus.Send("subscription.service",v);
	
	var cmd = new MarkUserAsActive(userId,emailverToken);
	cmd.Id.Dump();
	
	bus.Send("subscription.service",cmd);
	
	Thread.Sleep(2000);
	
	var roles = new Dictionary<string, Dictionary<Guid, List<string>>>();
	roles.Add("Test",new Dictionary<Guid,List<string>>());
	
	roles["Test"].Add(Guid.Empty,new List<string>{"AAAA"});
	
	var invite = new InviteUser(,accountgid,roles,"aaa@g.com");
	bus.Send("subscription.service",invite);
}

// Define other methods and classes here