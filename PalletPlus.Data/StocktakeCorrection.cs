﻿using System;
using System.Collections.Generic;

namespace PalletPlus.Data
{
    public class StocktakeCorrection
    {
        public Guid CorrectionKey { get; set; }

        public DateTime RequestedOn { get; set; }
        public DateTime? ClosedOn { get; set; }

        public KeyValuePair<Guid, string> RequestedBy { get; set; }
        public KeyValuePair<Guid, string> ClosedBy { get; set; }

        public int NewQuantity { get; set; }
        public int OldQuantity { get; set; }
        public string Reason { get; set; }

        public bool? IsApproved { get; set; } //true accepted, false declined

        public StocktakeCorrection(Guid correctionKey, int newQuantity, int oldQuantity, string reason, KeyValuePair<Guid, string> requestedBy)
        {
            RequestedOn = DateTime.UtcNow;

            CorrectionKey = correctionKey;
            NewQuantity = newQuantity;
            OldQuantity = oldQuantity;
            Reason = reason;
            RequestedBy = requestedBy;
        }

        public void ChangeCorrectionStatus(bool accepted, KeyValuePair<Guid, string> closedBy)
        {
            ClosedOn = DateTime.UtcNow;

            IsApproved = accepted;
            ClosedBy = closedBy;
        }
    }
}
