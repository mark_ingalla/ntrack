﻿using System;
using System.Collections.Generic;

namespace PalletPlus.Data
{
    public class MovementCancellation
    {
        public Guid CorrectionKey { get; set; }

        public DateTime RequestedOn { get; set; }
        public DateTime ClosedOn { get; set; }

        public KeyValuePair<Guid, string> RequestedBy { get; set; }
        public KeyValuePair<Guid, string> ClosedBy { get; set; }

        public string Reason { get; set; }

        public bool? IsApproved { get; set; }//true accepted, false declined, null pending

        public MovementCancellation()
        {
            
        }

        public MovementCancellation(Guid key, KeyValuePair<Guid, string> requestedBy, DateTime requestedOn, string reason)
        {
            CorrectionKey = key;
            RequestedOn = requestedOn;
            RequestedBy = requestedBy;
            Reason = reason;
        }

        public void ChangeCorrectionStatus(bool accepted, KeyValuePair<Guid, string> closedBy)
        {
            ClosedOn = DateTime.UtcNow;

            IsApproved = accepted;
            ClosedBy = closedBy;
        }
    }
}
