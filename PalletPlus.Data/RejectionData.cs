﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PalletPlus.Data
{
    public class RejectionData
    {
        public DateTime RejectedOn { get; set; }
        public Guid RejectedBy { get; set; }

        public RejectionData(DateTime rejectedOn, Guid rejectedBy)
        {
            RejectedOn = rejectedOn;
            RejectedBy = rejectedBy;
        }
    }
}
