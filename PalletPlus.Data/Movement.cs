﻿using System;
using System.Collections.Generic;
using nTrack.Data;

namespace PalletPlus.Data
{
    public class Movement : BaseModel
    {
        public string SiteId { get; set; }
        public string SiteAccountNumber { get; set; }
        public string SiteAddress { get; set; }

        public string DestinationId { get; set; }
        public string DestinationName { get; set; }
        public string DestinationAccountNumber { get; set; }
        public string DestinationAddress { get; set; }
        public string DestinationKind { get; set; }

        public string SupplierId { get; set; }
        public string SupplierName { get; set; }
        public Guid? LocationKey { get; set; }

        public Dictionary<string, EquipmentInfo> Equipments { get; set; } //TODO: fix spelling issue

        public string MovementType { get; set; }
        public string Direction { get; set; }
        public DateTime MovementDate { get; set; }
        public DateTime EffectiveDate { get; set; }

        public string DocketNumber { get; set; }
        public string DestinationReference { get; set; }
        public string MovementReference { get; set; }
        public string ConsignmentNote { get; set; }
        public bool IsPendingPTA { get; set; }
        public bool IsPending { get; set; }

        public MovementCorrection PendingMovementCorrection { get; set; }
        public MovementCancellation PendingMovementCancellation { get; set; }
        public bool IsCancelled { get; set; }
        public bool IsReconciled { get; set; }
        public bool IsExported { get; set; }
        public int UnreconciledItems { get; set; }

        public Dictionary<string, ReconciliationData> ReconciledEquipment { get; set; }
        public Dictionary<string, PreReconciliationData> PreReconciliationData { get; set; } //<EquipmentCode, PreReconciliationData>
        public Dictionary<Guid, MovementCorrection> HistoricalCorrections { get; set; }

        public Movement()
        {
            Equipments = new Dictionary<string, EquipmentInfo>();
            ReconciledEquipment = new Dictionary<string, ReconciliationData>();
            PreReconciliationData = new Dictionary<string, PreReconciliationData>();
            HistoricalCorrections = new Dictionary<Guid, MovementCorrection>();
            IsActive = true;
        }


        public class EquipmentInfo
        {
            public string EquipmentName { get; set; }
            public int Quantity { get; set; }
        }
    }

    public class ReconciliationData
    {
        public DateTime MatchedOn { get; set; }

        public string InvoiceId { get; set; }
        public Guid DetailKey { get; set; }

        public ReconciliationData()
        {

        }

        public ReconciliationData(DateTime matchedOn, string invoiceId, Guid detailKey)
        {
            MatchedOn = matchedOn;

            InvoiceId = invoiceId;
            DetailKey = detailKey;
        }
    }

    public class PreReconciliationData
    {
        public DateTime MatchedOn { get; set; }
        public Guid MatchedBy { get; set; }

        //old data
        public DateTime PreMovementDate { get; set; }
        public string PreDocketNumber { get; set; }
        public string PreDestinationId { get; set; }
        public string PreMovementType { get; set; }
        public int PreQuantity { get; set; }

        //new data
        public DateTime PostMovementDate { get; set; }
        public string PostDocketNumber { get; set; }
        public string PostDestinationId { get; set; }
        public string PostMovementType { get; set; }
        public int PostQuantity { get; set; }

        public PreReconciliationData(DateTime matchedOn, Guid matchedBy,
            DateTime preMovementDate, string preDocketNumber, string preDestinationId, string preMovementType, int preQuantity,
            DateTime postMovementDate, string postDocketNumber, string postDestinationId, string postMovementType, int postQuantity)
        {
            MatchedOn = matchedOn;
            MatchedBy = matchedBy;

            PreMovementDate = preMovementDate;
            PreDocketNumber = preDocketNumber;
            PreDestinationId = preDestinationId;
            PreMovementType = preMovementType;
            PreQuantity = preQuantity;

            PostMovementDate = postMovementDate;
            PostDocketNumber = postDocketNumber;
            PostDestinationId = postDestinationId;
            PostMovementType = postMovementType;
            PostQuantity = postQuantity;
        }
    }
}
