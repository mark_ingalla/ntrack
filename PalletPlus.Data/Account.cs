﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using nTrack.Data;

namespace PalletPlus.Data
{
    public class Account : BaseModel
    {
        public string CompanyName { get; set; }
        public Dictionary<string, CompanySupplier> Suppliers { get; set; }
        public Dictionary<Guid, string> CorrectionReasons { get; set; }
        public Dictionary<Guid, string> CancellationReasons { get; set; }
        public Dictionary<Guid, DashboardEquipmentColor> DashboardEquipmentColors { get; set; }
        public int GenericSequenceNumber { get; set; }
        public HashSet<string> RequiredFields { get; set; }
        public bool ExportMovementIn { get; set; }
        public bool ExportMovementOut { get; set; }

        public Account()
        {
            Suppliers = new Dictionary<string, CompanySupplier>();
            CorrectionReasons = new Dictionary<Guid, string>();
            CancellationReasons = new Dictionary<Guid, string>();
            DashboardEquipmentColors = new Dictionary<Guid, DashboardEquipmentColor>();
            RequiredFields = new HashSet<string>();
        }
    }

    public class DashboardEquipmentColor
    {
        public string EquipmentCode { get; set; }
        public string EquipmentName { get; set; }
        public int R { get; set; }
        public int G { get; set; }
        public int B { get; set; }
    }
}
