using System.Linq;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{
    /// <summary>
    /// Account numbers of either the partner, site or company with supplier must be unique.
    /// This index is created to validate uniqueness of account numbers.
    /// </summary>
    public class AccountNumbersIndex : AbstractMultiMapIndexCreationTask<AccountNumbersIndex.Result>
    {
        public class Result
        {
            public string EntityId { get; set; }
            public string AccountNumber { get; set; }
        }

        public AccountNumbersIndex()
        {
            AddMap<Partner>(partners =>
                            from partner in partners
                            from supplier in partner.Suppliers
                            where supplier.Value.AccountNumber != null
                            select new
                                {
                                    EntityId = partner.IdString,
                                    AccountNumber = supplier.Value.AccountNumber
                                });

            AddMap<Site>(sites =>
                         from site in sites
                         from supplier in site.Suppliers
                         where supplier.Value.AccountNumber != null
                         select new
                             {
                                 EntityId = site.IdString,
                                 AccountNumber = supplier.Value.AccountNumber
                             });

            AddMap<Account>(accounts =>
                            from account in accounts
                            from supplier in account.Suppliers
                            where supplier.Value.AccountNumber != null
                            select new
                                {
                                    EntityId = account.IdString,
                                    AccountNumber = supplier.Value.AccountNumber
                                });

            Reduce = results => from result in results
                                group result by new { result.AccountNumber, result.EntityId }
                                    into gResult
                                    select new Result
                                        {
                                            EntityId = gResult.Key.EntityId,
                                            AccountNumber = gResult.Key.AccountNumber,
                                        };
        }
    }
}