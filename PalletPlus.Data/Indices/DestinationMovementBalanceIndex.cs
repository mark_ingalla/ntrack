﻿using System;
using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{
    public class DestinationMovementBalanceIndex : AbstractIndexCreationTask<Movement, DestinationMovementBalanceIndex.Result>
    {
        public class Result : IRaportIndexResult
        {
            public string DestinationId { get; set; }
            public string DestinationAccountNumber { get; set; }
            public string DestinationName { get; set; }
            public string DestinationKind { get; set; }
            public string SupplierName { get; set; }
            public string Equipment { get; set; }
            public string EquipmentCode { get; set; }
            public int Quantity { get; set; }
            public string SiteId { get; set; }
            public string MovementType { get; set; }
            public string MovementDirection { get; set; }
            public DateTime MovementDate { get; set; }
        }

        public DestinationMovementBalanceIndex()
        {
            Map = movements =>
                  from movement in movements
                  where !movement.IsCancelled
                  from eq in movement.Equipments
                  select new Result()
                             {
                                 MovementDate =  movement.MovementDate,
                                 DestinationId = movement.DestinationId,
                                 DestinationAccountNumber = movement.DestinationAccountNumber,
                                 DestinationName = movement.DestinationName,
                                 DestinationKind = movement.DestinationKind,
                                 Equipment = eq.Value.EquipmentName,
                                 EquipmentCode = eq.Key,
                                 Quantity = movement.Direction == "Out" ? -eq.Value.Quantity : eq.Value.Quantity,
                                 SupplierName = movement.SupplierName,
                                 MovementType = movement.MovementType,
                                 MovementDirection = movement.Direction,
                                 SiteId = movement.SiteId
                             };

            Reduce = results => from r in results
                                group r by new { r.DestinationId, r.DestinationAccountNumber, r.MovementType, r.EquipmentCode } into gr
                                select new Result()
                                           {
                                               DestinationId = gr.Key.DestinationId,
                                               DestinationAccountNumber = gr.Key.DestinationAccountNumber,
                                               DestinationName = gr.Select(x => x.DestinationName).FirstOrDefault(),
                                               SupplierName = gr.Select(x => x.SupplierName).FirstOrDefault(),
                                               DestinationKind = gr.Select(x => x.DestinationKind).FirstOrDefault(),
                                               MovementType = gr.Select(x => x.MovementType).FirstOrDefault(),
                                               EquipmentCode = gr.Key.EquipmentCode,
                                               Equipment = gr.Select(x => x.Equipment).FirstOrDefault(),
                                               Quantity = gr.Sum(x => x.Quantity),
                                               SiteId = gr.Select(x => x.SiteId).FirstOrDefault(),
                                               MovementDate = gr.Select(x => x.MovementDate).FirstOrDefault(),
                                               MovementDirection = gr.Select(x => x.MovementDirection).FirstOrDefault()
                                           };

            StoreAllFields(FieldStorage.Yes);

        }
    }
}
