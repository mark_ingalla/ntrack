using System.Linq;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{
    public class SupplierInvoiceIndex : AbstractIndexCreationTask<SupplierInvoice>
    {
        public SupplierInvoiceIndex()
        {
            Map = invoices => from i in invoices
                              select new
                                  {
                                      i.SupplierId,
                                      i.InvoiceDate,
                                      i.InvoiceNumber,
                                      i.TotalItems,
                                      i.IsClosed,
                                      i.IsReconciled,
                                      Keys = i.Details.Select(x => x.Key).ToList()
                                  };

            TransformResults = (database, invoices) =>
                               from i in invoices
                               let supplier = database.Load<Supplier>(i.SupplierId)
                               select new
                                   {
                                       i.IdString,
                                       i.UpdatedOn,
                                       i.SupplierId,
                                       SupplierName = supplier.CompanyName,
                                       i.InvoiceDate,
                                       i.InvoiceNumber,
                                       i.TotalItems,
                                       i.IsClosed,
                                       i.IsReconciled,
                                       Keys = i.Details.Select(x => x.Key).ToList()
                                   };
        }
    }
}