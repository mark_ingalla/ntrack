﻿using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{
    public class MovementBalanceIndex : AbstractIndexCreationTask<Movement, MovementBalanceIndex.Result>
    {
        public class Result
        {
            public string SiteId { get; set; }
            public string SiteName { get; set; }
            public string EquipmentName { get; set; }
            public string EquipmentCode { get; set; }
            public int Quantity { get; set; }
            public string MovementType { get; set; }
        }

        public MovementBalanceIndex()
        {
            Map = movements =>
                  from movement in movements
                  where !movement.IsCancelled && !movement.IsPendingPTA
                  from eq in movement.Equipments
                  select new Result()
                             {
                                 SiteId = movement.SiteId,
                                 SiteName = LoadDocument<Site>(movement.SiteId).CompanyName,
                                 EquipmentName = eq.Value.EquipmentName,
                                 EquipmentCode = eq.Key,
                                 Quantity = movement.Direction == "Out" ? -eq.Value.Quantity : eq.Value.Quantity,
                                 MovementType = movement.MovementType,
                             };

            Reduce = results => from r in results
                                group r by new { r.SiteId, r.EquipmentCode, r.MovementType } into gr
                                select new Result
                                           {
                                               SiteId = gr.Key.SiteId,
                                               SiteName = gr.Select(x => x.SiteName).FirstOrDefault(),
                                               EquipmentName = gr.Select(x => x.EquipmentName).FirstOrDefault(),
                                               EquipmentCode = gr.Key.EquipmentCode,
                                               Quantity = gr.Sum(x => x.Quantity),
                                               MovementType = gr.Key.MovementType
                                           };

            StoreAllFields(FieldStorage.Yes);

        }
    }
}
