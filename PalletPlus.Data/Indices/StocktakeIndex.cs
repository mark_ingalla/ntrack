using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;
using System;
using System.Linq;

namespace PalletPlus.Data.Indices
{
    public class StocktakeIndex : AbstractIndexCreationTask<Stocktake, StocktakeIndex.Result>
    {
        public class Result
        {
            public string StocktakeId { get; set; }            

            public DateTime StocktakeDate { get; set; }

            public string EquipmentCode { get; set; }
            public string EquipmentName { get; set; }
            public int Quantity { get; set; }

            public bool GotPendingCorrection { get; set; }
            public bool? IsApproved { get; set; }
            public Guid CorrectionRequestedBy { get; set; }

            public string SiteId { get; set; }
            public string SiteName { get; set; }
            public string SupplierId { get; set; }
            public string SupplierName { get; set; }
        }

        public StocktakeIndex()
        {
            Map = stocktakes =>
                from stocktake in stocktakes
                where stocktake.EquipmentCounts != null && stocktake.EquipmentCounts.Any()
                from count in stocktake.EquipmentCounts
                select new
                {
                    StocktakeId = stocktake.IdString,
                    StocktakeDate = stocktake.TakenOn,
                    EquipmentCode = count.Key,
                    EquipmentName = count.Value.EquipmentName,
                    Quantity = count.Value.Quantity,                    
                    GotPendingCorrection = count.Value.PendingCorrection != null,
                    IsApproved = count.Value.PendingCorrection != null ? count.Value.PendingCorrection.IsApproved : null,
                    CorrectionRequestedBy = count.Value.PendingCorrection != null ? count.Value.PendingCorrection.RequestedBy.Key : Guid.Empty,
                    SiteName = stocktake.SiteName,
                    SupplierName = stocktake.SupplierName,
                    SiteId = stocktake.SiteId,
                    SupplierId = stocktake.SupplierId
                };

            Reduce = results => from result in results
                                group result by new { result.StocktakeId, result.EquipmentCode }
                                    into gResult
                                    select new Result
                                    {
                                        StocktakeId = gResult.Key.StocktakeId,
                                        StocktakeDate = gResult.Select(x => x.StocktakeDate).FirstOrDefault(),
                                        EquipmentCode = gResult.Select(x => x.EquipmentCode).FirstOrDefault(),
                                        EquipmentName = gResult.Select(x => x.EquipmentName).FirstOrDefault(),
                                        SupplierName = gResult.Select(x => x.SupplierName).FirstOrDefault(),
                                        SiteName = gResult.Select(x => x.SiteName).FirstOrDefault(),
                                        Quantity = gResult.Select(x => x.Quantity).FirstOrDefault(),

                                        GotPendingCorrection = gResult.Select(x => x.GotPendingCorrection).FirstOrDefault(),
                                        IsApproved = gResult.Select(x => x.IsApproved).First(),
                                        CorrectionRequestedBy = gResult.Select(x => x.CorrectionRequestedBy).First(),
                                        SiteId = gResult.Select(x => x.SiteId).FirstOrDefault(),
                                        SupplierId = gResult.Select(x => x.SupplierId).FirstOrDefault(),
                                    };

            StoreAllFields(FieldStorage.Yes);
        }
    }
}