﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{
    public class PartnerTagIndex : AbstractIndexCreationTask<Partner,PartnerTagIndex.Result>
    {
        public class Result
        {
            public string Tag { get; set; }
        }

        public PartnerTagIndex()
        {
            Map = partners =>
                  from partner in partners
                  from  tag in partner.Tags
                  select new
                             {
                                 Tag = tag
                             };
            Reduce = results => from result in results
                                group result by new {result.Tag}
                                into gResult
                                select new Result
                                           {
                                               Tag = gResult.Key.Tag
                                           };


        }
    }
}
