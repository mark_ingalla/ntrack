using System;
using System.Collections.Generic;
using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{
    public class CorrectionRequestIndex : AbstractMultiMapIndexCreationTask<CorrectionRequestIndex.Result>
    {
        public class Result
        {
            public string Name { get; set; }
            public Guid CorrectionKey { get; set; }
            public string MovementStocktakeId { get; set; }
            public string DocketNumber { get; set; }
            public string SiteId { get; set; }
            public string SiteName { get; set; }
            public DateTime RequestedOn { get; set; }
            public KeyValuePair<string, string> RequestedBy { get; set; }
            public bool? IsApproved { get; set; }
            public DateTime UpdatedOn { get; set; }
        }

        public CorrectionRequestIndex()
        {
            AddMap<Movement>(movements =>
                             from movement in movements
                             where movement.PendingMovementCorrection != null
                             select new
                                 {
                                     Name = string.Format("Movement Correction ({0})", movement.MovementType),
                                     CorrectionKey = movement.PendingMovementCorrection.CorrectionKey,
                                     MovementStocktakeId = movement.IdString,
                                     DocketNumber = movement.DocketNumber,
                                     SiteId = movement.SiteId,
                                     SiteName = LoadDocument<Site>(movement.SiteId).CompanyName,
                                     RequestedOn = movement.PendingMovementCorrection.RequestedOn,
                                     RequestedBy = movement.PendingMovementCorrection.RequestedBy,
                                     IsApproved = movement.PendingMovementCorrection.IsApproved,
                                     UpdatedOn = movement.UpdatedOn
                                 });

            AddMap<Movement>(movements =>
                             from movement in movements
                             where
                                 movement.PendingMovementCancellation != null && movement.PendingMovementCancellation.IsApproved == null
                             select new
                                 {
                                     Name = string.Format("Movement Cancellation ({0})", movement.MovementType),
                                     CorrectionKey = movement.PendingMovementCancellation.CorrectionKey,
                                     MovementStocktakeId = movement.IdString,
                                     DocketNumber = movement.DocketNumber,
                                     SiteId = movement.SiteId,
                                     SiteName = LoadDocument<Site>(movement.SiteId),
                                     RequestedOn = movement.PendingMovementCancellation.RequestedOn,
                                     RequestedBy = movement.PendingMovementCancellation.RequestedBy,
                                     IsApproved = movement.PendingMovementCorrection.IsApproved,
                                     UpdatedOn = movement.UpdatedOn
                                 });

            AddMap<Stocktake>(stocktakes =>
                              from stocktake in stocktakes
                              from asset in stocktake.EquipmentCounts
                              where asset.Value.PendingCorrection != null
                              select new
                                  {
                                      Name = "Stocktake Correction (" + asset.Value.EquipmentName + ")",
                                      CorrectionKey = asset.Value.PendingCorrection.CorrectionKey,
                                      MovementStocktakeId = stocktake.IdString,
                                      DocketNumber = (string)null,
                                      SiteId = stocktake.SiteId,
                                      SiteName = stocktake.SiteName,
                                      RequestedOn = asset.Value.PendingCorrection.RequestedOn,
                                      RequestedBy = asset.Value.PendingCorrection.RequestedBy,
                                      IsApproved = asset.Value.PendingCorrection.IsApproved,
                                      UpdatedOn = stocktake.UpdatedOn
                                  });

            Reduce = results => from result in results
                                group result by new { result.MovementStocktakeId }
                                    into gResult
                                    select new Result
                                        {
                                            MovementStocktakeId = gResult.Key.MovementStocktakeId,
                                            Name = gResult.Select(x => x.Name).FirstOrDefault(),
                                            CorrectionKey = gResult.Select(x => x.CorrectionKey).FirstOrDefault(),
                                            DocketNumber = gResult.Select(x => x.DocketNumber).FirstOrDefault(),
                                            SiteId = gResult.Select(x => x.SiteId).FirstOrDefault(),
                                            SiteName = gResult.Select(x => x.SiteName).FirstOrDefault(),
                                            RequestedOn = gResult.Select(x => x.RequestedOn).FirstOrDefault(),
                                            RequestedBy = gResult.Select(x => x.RequestedBy).FirstOrDefault(),
                                            IsApproved = gResult.Select(x => x.IsApproved).FirstOrDefault(),
                                            UpdatedOn = gResult.Select(x => x.UpdatedOn).FirstOrDefault()
                                        };

            StoreAllFields(FieldStorage.Yes);

        }
    }
}