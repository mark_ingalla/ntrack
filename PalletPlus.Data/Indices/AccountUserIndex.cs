using Raven.Client.Indexes;
using System.Linq;

namespace PalletPlus.Data.Indices
{
    public class AccountUserIndex : AbstractIndexCreationTask<Account, AccountUserIndex.Result>
    {
        public class Result
        {
            public string AccountId { get; set; }
            public string UserId { get; set; }
            public string FullName { get; set; }
            public string Email { get; set; }
            public string Mobile { get; set; }
            public string Phone { get; set; }
        }

        public AccountUserIndex()
        {
            Map = accounts =>
                  from account in accounts
                  from user in account.Users
                  select new
                             {
                                 AccountId = account.IdString,
                                 UserId = user.Value.UserId,
                                 FullName = user.Value.FullName,
                                 Email = user.Value.Email,
                                 Mobile = user.Value.Mobile,
                                 Phone = user.Value.Phone
                             };


            Reduce = results => from result in results
                                group result by new { result.AccountId, result.UserId }
                                    into gResult
                                    select new Result
                                               {
                                                   AccountId = gResult.Key.AccountId,
                                                   UserId = gResult.Key.UserId,
                                                   FullName = gResult.Select(x => x.FullName).FirstOrDefault(),
                                                   Email = gResult.Select(x => x.Email).FirstOrDefault(),
                                                   Mobile = gResult.Select(x => x.Mobile).FirstOrDefault(),
                                                   Phone = gResult.Select(x => x.Phone).FirstOrDefault()
                                               };
        }
    }
}