﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{
    public class ExchangeDashboardMovementIndex : AbstractIndexCreationTask<Exchange,ExchangeDashboardMovementIndex.Result>
    {
        public class Result
        {
            
            
            
            public int Year { get; set; }
            public int Week { get; set; }
            public int Counts { get; set; }
            public string SiteId { get; set; }
            public int InCount { get; set; }
            public int OutCount { get; set; }
        }


        public ExchangeDashboardMovementIndex()
        {
            Map = exchanges =>
                  from exchange in exchanges
                  select new Result
                  {
                      
                      SiteId = exchange.SiteId,
                      Year = exchange.EffectiveDate.Year,
                      Week = (int)Math.Floor((decimal)exchange.EffectiveDate.Date.DayOfYear / 7),
                      Counts = 1,
                      InCount = exchange.EquipmentsIn.Count > 0 ? 1 : 0,
                      OutCount = exchange.EquipmentsOut.Count > 0 ? 1 : 0
                  };

            Reduce = results => from r in results
                                group r by new {  r.Week, r.Year, r.SiteId } into gr
                                select new Result
                                {
                                    Counts = gr.Sum(p => p.Counts),                                
                                    Week = gr.Key.Week,
                                    Year = gr.Key.Year,
                                    SiteId = gr.Key.SiteId
                                    ,InCount = gr.Sum(p=>p.InCount),
                                    OutCount = gr.Sum(p=>p.OutCount)
                                };

            StoreAllFields(FieldStorage.Yes);
        }


    }
}
