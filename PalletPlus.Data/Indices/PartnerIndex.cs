using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{
    public class PartnerIndex : AbstractIndexCreationTask<Partner>
    {
        public PartnerIndex()
        {
            Map = partners =>
                from partner in partners
                select new
                {
                    PartnerId = partner.IdString,
                    CompanyName = partner.CompanyName,
                    Suburb = partner.Suburb,
                    State = partner.State,
                    ContactEmail = partner.ContactEmail,
                    Sites = partner.Sites.ToList(),
                    IsDeleted = partner.IsDeleted
                };

            Index(p => p.CompanyName, FieldIndexing.Analyzed);
            Index(p => p.ContactEmail, FieldIndexing.Analyzed);
        }
    }
}