using System.Linq;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{
    public class SupplierIndex : AbstractIndexCreationTask<Supplier>
    {
        public SupplierIndex()
        {
            Map = suppliers =>
                from supplier in suppliers
                select new
                {
                    SupplierId = supplier.IdString,
                    CompanyName = supplier.CompanyName,
                    ContactEmail = supplier.ContactEmail,
                    IsDeleted = supplier.IsDeleted,
                    ProviderName = supplier.ProviderName
                };
        }
    }
}