﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{
    public class ExchangeIndex : AbstractIndexCreationTask<Exchange,ExchangeIndex.Result>
    {
        public class Result
        {
            public string ExchangeId { get; set; }
            public DateTime EffectiveDate { get; set; }
            public string SiteId { get; set; }
            
            public string SupplierId { get; set; }
            public string PartnerId { get; set; }
            public string PartnerName { get; set; }

            public string PartnerReference { get; set; }
        }

        public ExchangeIndex()
        {
            Map = exchanges =>
                  from exchange in exchanges
                  select new
                             {
                                 EffectiveDate = exchange.EffectiveDate,
                                 SiteId = exchange.SiteId,
                                 SupplierId = exchange.SupplierId,
                                 PartnerId = exchange.PartnerId,
                                 PartnerName = exchange.PartnerName,
                                 PartnerReference = exchange.PartnerReference,
                                 ExchangeId = exchange.Id
                             };

            Reduce = results =>
                     from result in results
                     group result by new {result.SupplierId,result.PartnerId,result.ExchangeId}
                     into gResult
                     select new Result
                                {
                                    EffectiveDate = gResult.Select(x=>x.EffectiveDate).FirstOrDefault(),
                                    SiteId = gResult.Select(x=>x.SiteId).FirstOrDefault(),
                                    SupplierId = gResult.Key.SupplierId,
                                    PartnerId = gResult.Key.PartnerId,
                                    PartnerName = gResult.Select(x=>x.PartnerName).FirstOrDefault(),
                                    PartnerReference = gResult.Select(x=>x.PartnerReference).FirstOrDefault(),
                                    ExchangeId = gResult.Key.ExchangeId
                                };

            StoreAllFields(FieldStorage.Yes);
        }
    }
}
