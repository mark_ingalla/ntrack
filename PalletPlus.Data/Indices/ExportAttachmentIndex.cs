using System.Linq;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{
    public class ExportAttachmentIndex : AbstractIndexCreationTask<ExportAttachment>
    {
        public ExportAttachmentIndex()
        {
            Map = attatchments =>
                from attatchment in attatchments
                select new
                {
                    AttachmentId = attatchment.Id,
                    MovementDate = attatchment.MovementDate,
                    SentOn = attatchment.SentOn,
                    FileName = attatchment.FileName,
                    SiteId = attatchment.SiteId,
                    SupplierId = attatchment.SupplierId,
                    TotalDownloads = attatchment.TotalDownloads,
                    TotalMovements = attatchment.TotalMovements,
                };

            TransformResults = (database, results) =>
                               from r in results
                               select new
                                   {
                                       AttachmentId = r.Id,
                                       MovementDate = r.MovementDate,
                                       SentOn = r.SentOn,
                                       FileName = r.FileName,
                                       SiteId = r.SiteId,
                                       SupplierId = r.SupplierId,
                                       TotalDownloads = r.TotalDownloads,
                                       TotalMovements = r.TotalMovements,
                                       SiteName = database.Load<Site>(r.SiteId).CompanyName,
                                       SupplierName = database.Load<Supplier>(r.SupplierId).CompanyName
                                   };

        }
    }
}