using Raven.Client.Indexes;
using System;
using System.Linq;

namespace PalletPlus.Data.Indices
{
    public class SiteUserIndex : AbstractIndexCreationTask<Site, SiteUserIndex.Result>
    {
        public class Result
        {
            public string SiteId { get; set; }
            public string SiteName { get; set; }
            public Guid UserKey { get; set; }
            public bool IsDeleted { get; set; }
        }

        public SiteUserIndex()
        {
            Map = sites =>
                  from site in sites
                  from user in site.Users
                  select new
                      {
                          SiteId = site.IdString,
                          SiteName = site.CompanyName,
                          UserKey = user,
                          IsDeleted = site.IsDeleted
                      };

            Reduce = results => from result in results
                                group result by new { result.SiteId, UserId = result.UserKey }
                                    into gResult
                                    select new Result
                                        {
                                            SiteId = gResult.Key.SiteId,
                                            SiteName = gResult.Select(x => x.SiteName).FirstOrDefault(),
                                            UserKey = gResult.Key.UserId,
                                            IsDeleted = gResult.Select(x => x.IsDeleted).FirstOrDefault()
                                        };
        }
    }
}