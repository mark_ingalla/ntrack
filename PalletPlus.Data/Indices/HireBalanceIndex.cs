﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{
    public class HireBalanceIndex : AbstractIndexCreationTask<Movement, HireBalanceIndex.Result>
    {
        public class Result : IRaportIndexResult
        {
            public int Year { get; set; }
            public int Month { get; set; }
            public int Issues { get; set; }
            public int Returns { get; set; }
            public int TransfersIn { get; set; }
            public int TransfersOut { get; set; }
            public int CorrectionsIn { get; set; }
            public int CorrectionsOut { get; set; }
            public string SupplierId { get; set; }
            public string SupplierName { get; set; }
            public string EquipmentName { get; set; }
            public string EquipmentCode { get; set; }
            public string SiteId { get; set; }
            public string SiteName { get; set; }
            public DateTime MovementDate { get; set; }
            public int ClosingBalance { get; set; }
        }

        public HireBalanceIndex()
        {
            Map = movements =>
                  from movement in movements
                  where !movement.IsCancelled
                  from eq in movement.Equipments
                  select new Result()
                             {
                                 SupplierId = movement.SupplierId,
                                 SupplierName = movement.SupplierName,
                                 EquipmentName = eq.Value.EquipmentName,
                                 EquipmentCode = eq.Key,
                                 SiteId = movement.SiteId,
                                 SiteName = LoadDocument<Site>(movement.SiteId).CompanyName,
                                 TransfersIn = movement.MovementType == "Transfer" && movement.Direction == "In" ? eq.Value.Quantity : 0,
                                 TransfersOut = movement.MovementType == "Transfer" && movement.Direction == "Out" ? eq.Value.Quantity : 0,
                                 Issues = movement.DestinationKind == "Supplier" && movement.Direction == "In" ? eq.Value.Quantity : 0,
                                 Returns = movement.DestinationKind == "Supplier" && movement.Direction == "Out" ? eq.Value.Quantity : 0,
                                 CorrectionsIn = movement.Direction == "In" ? movement.HistoricalCorrections.Count : 0,
                                 CorrectionsOut = movement.Direction == "Out" ? movement.HistoricalCorrections.Count : 0,
                                 ClosingBalance = 0,
                                 MovementDate = movement.MovementDate
                             };

            Reduce = results => from r in results
                                group r by new { r.SiteId, r.EquipmentCode, r.SupplierId, r.MovementDate } into gr
                                select new Result
                                           {
                                               SiteId = gr.Key.SiteId,
                                               SiteName = gr.Select(x => x.SiteName).FirstOrDefault(),
                                               EquipmentName = gr.Select(x => x.EquipmentName).FirstOrDefault(),
                                               EquipmentCode = gr.Key.EquipmentCode,
                                               SupplierId = gr.Key.SupplierId,
                                               SupplierName = gr.Select(x => x.SupplierName).FirstOrDefault(),
                                               TransfersIn = gr.Sum(x => x.TransfersIn),
                                               TransfersOut = gr.Sum(x => x.TransfersOut),
                                               Issues = gr.Sum(x => x.Issues),
                                               Returns = gr.Sum(x => x.Returns),
                                               CorrectionsIn = gr.Sum(x => x.CorrectionsIn),
                                               CorrectionsOut = gr.Sum(x => x.CorrectionsOut),
                                               MovementDate = gr.Key.MovementDate,
                                               ClosingBalance = gr.Sum(x => x.TransfersIn) + gr.Sum(x => x.Issues) - gr.Sum(x => x.TransfersOut) - gr.Sum(x => x.Returns)
                                           };

            StoreAllFields(FieldStorage.Yes);

        }
    }
}
