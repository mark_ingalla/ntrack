using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{
    public class EquipmentIndex : AbstractIndexCreationTask<Supplier, EquipmentIndex.Result>
    {
        public class Result
        {
            public string SupplierId { get; set; }
            public string EquipmentCode { get; set; }
            public string EquipmentName { get; set; }
        }

        public EquipmentIndex()
        {
            Map = suppliers =>
                  from supplier in suppliers
                  from assets in supplier.Equipments
                  select new
                      {
                          SupplierId = supplier.IdString,
                          EquipmentCode = assets.Value.EquipmentCode,
                          EquipmentName = assets.Value.EquipmentName
                      };

            Reduce = results => from result in results
                                group result by new { result.SupplierId, result.EquipmentCode }
                                    into gResult
                                    select new Result
                                        {
                                            SupplierId = gResult.Key.SupplierId,
                                            EquipmentCode = gResult.Key.EquipmentCode,
                                            EquipmentName = gResult.Select(x => x.EquipmentName).FirstOrDefault()
                                        };

            StoreAllFields(FieldStorage.Yes);
        }
    }
}