﻿using System;
using System.Linq;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{
    public interface IRaportIndexResult
    {
        DateTime MovementDate { get; set; }
    }

    public class ReportingMovementSummariesIndex : AbstractIndexCreationTask<Movement, ReportingMovementSummariesIndex.Result>
    {
        public class Result : IRaportIndexResult
        {
            public string SiteId { get; set; }
            public string SiteName { get; set; }

            public string SupplierId { get; set; }
            public string SupplierName { get; set; }

            public string PartnerId { get; set; }
            public string PartnerName { get; set; }

            public DateTime MovementDate { get; set; }
            public string MovementType { get; set; }
            public string MovementDirection { get; set; }

            public string EquipmentCode { get; set; }
            public string EquipmentName { get; set; }
            public int Quantity { get; set; }
        }

        public ReportingMovementSummariesIndex()
        {
            Map = movements => from m in movements
                               from eq in m.Equipments
                               select new Result()
                                          {
                                              SiteId = m.SiteId,
                                              SiteName = LoadDocument<Site>(m.SiteId).CompanyName,
                                              SupplierId = m.SupplierId,
                                              SupplierName = m.SupplierName,
                                              EquipmentCode = eq.Key,
                                              EquipmentName = eq.Value.EquipmentName,
                                              MovementDate = m.MovementDate,
                                              MovementType = m.MovementType,
                                              MovementDirection = m.Direction.ToLower() == "in" ? "Receive" : "Send",
                                              Quantity = eq.Value.Quantity,
                                              PartnerId = m.DestinationId,
                                              PartnerName = m.DestinationName
                                          };

            Reduce = results => from r in results
                                group r by new { r.SiteId, r.PartnerId, r.MovementType, r.MovementDirection, r.EquipmentCode, MovementDate = r.MovementDate.Date } into gr
                                //from g in gr
                                select new Result()
                                           {
                                               SiteId = gr.Key.SiteId,
                                               PartnerId = gr.Key.PartnerId,
                                               MovementDate = gr.Key.MovementDate,
                                               MovementDirection = gr.Key.MovementDirection,
                                               MovementType = gr.Key.MovementType,
                                               EquipmentCode = gr.Select(x => x.EquipmentCode).FirstOrDefault(x => x != null),
                                               EquipmentName = gr.Select(x => x.EquipmentName).FirstOrDefault(x => x != null),
                                               Quantity = gr.Sum(x => x.Quantity),
                                               PartnerName = gr.Select(x => x.PartnerName).FirstOrDefault(x => x != null),
                                               SiteName = gr.Select(x => x.SiteName).FirstOrDefault(x => x != null),
                                               SupplierId = gr.Select(x => x.SupplierId).FirstOrDefault(x => x != null),
                                               SupplierName = gr.Select(x => x.SupplierName).FirstOrDefault(x => x != null),
                                           };


        }
    }
}
