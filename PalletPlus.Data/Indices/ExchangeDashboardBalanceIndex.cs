﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{
    public class ExchangeDashboardBalanceIndex : AbstractIndexCreationTask<Exchange,ExchangeDashboardBalanceIndex.Result>
    {
        public class Result
        {
            public string SiteId { get; set; }
            public string SiteName { get; set; }
            public string EquipmentName { get; set; }
            public string EquipmentCode { get; set; }
            public int QuantityIn { get; set; }
            public int QuantityOut { get; set; }
        }


        public ExchangeDashboardBalanceIndex()
        {
            Map = exchanges =>
                  from exchange in exchanges
                  from eqIn in exchange.EquipmentsIn
                  from eqOut in exchange.EquipmentsOut
                  select new Result()
                  {
                      SiteId = exchange.SiteId,
                      SiteName = LoadDocument<Site>(exchange.SiteId).CompanyName,
                      EquipmentName = eqIn.Value.EquipmentName,
                      EquipmentCode = eqIn.Key,
                      QuantityIn = eqIn.Value.Quantity,
                      QuantityOut = eqOut.Value.Quantity
                      
                  };

            Reduce = results => from r in results
                                group r by new { r.SiteId,r.EquipmentCode } into gr
                                select new Result
                                {
                                    SiteId = gr.Key.SiteId,
                                    SiteName = gr.Select(x => x.SiteName).FirstOrDefault(),
                                    EquipmentName = gr.Select(x => x.EquipmentName).FirstOrDefault(),
                                    EquipmentCode = gr.Key.EquipmentCode,
                                    QuantityIn = gr.Sum(x => x.QuantityIn),
                                    QuantityOut = gr.Sum(x => x.QuantityOut)
                                };

            StoreAllFields(FieldStorage.Yes);
        }
            
    }
}
