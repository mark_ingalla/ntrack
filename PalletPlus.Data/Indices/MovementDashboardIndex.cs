﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{

    public class MovementDashboardIndex : AbstractIndexCreationTask<Movement, MovementDashboardIndex.Result>
    {
        public class Result
        {
            public string MovementType { get; set; }
            public string DestinationKind { get; set; }
            public string Direction { get; set; }
            public int Year { get; set; }
            public int Week { get; set; }
            public int Counts { get; set; }
            public string SiteId { get; set; }
        }

        public MovementDashboardIndex()
        {
            Map = movements =>
                  from movement in movements
                  where !movement.IsCancelled && !movement.IsPendingPTA
                  select new Result
                  {
                      MovementType = movement.MovementType,
                      DestinationKind = movement.DestinationKind,
                      SiteId = movement.SiteId,
                      Direction = movement.Direction,
                      Year = movement.MovementDate.Year,
                      Week = (int)Math.Floor((decimal)movement.MovementDate.Date.DayOfYear / 7),
                      Counts = 1,
                  };

            Reduce = results => from r in results
                                group r by new { r.MovementType, r.DestinationKind, r.Direction, r.Week, r.Year, r.SiteId } into gr
                                select new Result
                                {
                                    Counts = gr.Sum(p => p.Counts),
                                    MovementType = gr.Key.MovementType,
                                    DestinationKind = gr.Key.DestinationKind,
                                    Direction = gr.Key.Direction,
                                    Week = gr.Key.Week,
                                    Year = gr.Key.Year,
                                    SiteId =  gr.Key.SiteId
                                };

            StoreAllFields(FieldStorage.Yes);

        }
    }

}
