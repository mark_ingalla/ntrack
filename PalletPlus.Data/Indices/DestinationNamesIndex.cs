using System.Linq;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{
    /// <summary>
    /// This index is for names of either partner, site or a supplier
    /// (depends on what kind of movement do we create in Record Movement view)
    /// </summary>
    public class DestinationNamesIndex : AbstractMultiMapIndexCreationTask<DestinationNamesIndex.Result>
    {
        public class Result
        {
            public string CompanyId { get; set; }
            public string CompanyName { get; set; }
            public string Kind { get; set; }
        }

        public DestinationNamesIndex()
        {
            AddMap<Partner>(partners =>
                            from partner in partners
                            select new
                                       {
                                           CompanyId = partner.IdString,
                                           CompanyName = partner.CompanyName,
                                           Kind = "Partner"
                                       });

            AddMap<Site>(sites =>
                         from site in sites
                         select new
                                    {
                                        CompanyId = site.IdString,
                                        CompanyName = site.CompanyName,
                                        Kind = "Site"
                                    });

            AddMap<Supplier>(suppliers =>
                        from supplier in suppliers
                        select new
                                   {
                                       CompanyId = supplier.IdString,
                                       CompanyName = supplier.CompanyName,
                                       Kind = "Supplier"
                                   });

            Reduce = results => from result in results
                                group result by new { result.CompanyId }
                                    into gResult
                                    select new Result
                                               {
                                                   CompanyId = gResult.Key.CompanyId,
                                                   CompanyName = gResult.Select(x => x.CompanyName).FirstOrDefault(),
                                                   Kind = gResult.Select(x => x.Kind).FirstOrDefault()
                                               };
        }
    }
}