using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;
using System.Collections.Generic;
using System.Linq;

namespace PalletPlus.Data.Indices
{
    public class ReportingMovementIndex : AbstractIndexCreationTask<Movement, ReportingMovementIndexResult>
    {
        public ReportingMovementIndex()
        {
            Map = movements =>
                from movement in movements
                from eq in movement.Equipments
                select new ReportingMovementIndexResult
                {
                    Id = movement.IdString,
                    SiteId = movement.SiteId,
                    SiteName = LoadDocument<Site>(movement.SiteId).CompanyName,
                    SupplierId = movement.SupplierId,
                    SupplierName = movement.SupplierName,
                    PartnerId = movement.DestinationId,
                    DestinationName = movement.DestinationName,

                    MovementDate = movement.MovementDate,
                    MovementType = movement.MovementType,
                    MovementDirection = movement.Direction,
                    LocationKey = movement.LocationKey,
                    Equipment = eq.Value.EquipmentName,
                    EquipmentCode = eq.Key,
                    Quantity = eq.Value.Quantity,

                    PendingMovementCorrection = movement.PendingMovementCorrection,
                    PendingMovementCancellation = movement.PendingMovementCancellation,
                    IsCanceled = movement.IsCancelled,
                    IsReconciled = movement.IsReconciled,

                    ConsignmentNote = movement.ConsignmentNote,
                    DocketNumber = movement.DocketNumber,
                    DestinationReference = movement.DestinationReference,
                    MovementReference = movement.MovementReference,
                };

            Reduce = results => from result in results
                                group result by new { result.Id, result.EquipmentCode }
                                    into gResult
                                    select new ReportingMovementIndexResult
                                               {
                                                   Id = gResult.Select(x => x.Id).FirstOrDefault(),
                                                   SiteId = gResult.Select(x => x.SiteId).FirstOrDefault(),
                                                   SiteName = gResult.Select(x => x.SiteName).FirstOrDefault(),

                                                   SupplierId = gResult.Select(x => x.SupplierId).FirstOrDefault(),
                                                   SupplierName = gResult.Select(x => x.SupplierName).FirstOrDefault(),
                                                   PartnerId = gResult.Select(x => x.PartnerId).FirstOrDefault(),
                                                   DestinationName = gResult.Select(x => x.DestinationName).FirstOrDefault(),
                                                   MovementDate = gResult.Select(x => x.MovementDate).FirstOrDefault(),
                                                   MovementType = gResult.Select(x => x.MovementType).FirstOrDefault(),
                                                   MovementDirection = gResult.Select(x => x.MovementDirection).FirstOrDefault() != null && gResult.Select(x => x.MovementDirection).FirstOrDefault().ToLower() == "in" ? "Receive":"Send",
                                                   LocationKey = gResult.Select(x => x.LocationKey).FirstOrDefault(),
                                                   Equipment = gResult.Select(x => x.Equipment).FirstOrDefault(),
                                                   EquipmentCode = gResult.Select(x => x.EquipmentCode).FirstOrDefault(),
                                                   Quantity = gResult.Select(x => x.Quantity).FirstOrDefault(),

                                                   PendingMovementCorrection = gResult.Select(x => x.PendingMovementCorrection).FirstOrDefault(),
                                                   PendingMovementCancellation = gResult.Select(x => x.PendingMovementCancellation).FirstOrDefault(),
                                                   IsCanceled = gResult.Select(x => x.IsCanceled).FirstOrDefault(),
                                                   IsReconciled = gResult.Select(x => x.IsReconciled).FirstOrDefault(),

                                                   ConsignmentNote = gResult.Select(x => x.ConsignmentNote).FirstOrDefault(),
                                                   DocketNumber = gResult.Select(x => x.DocketNumber).FirstOrDefault(),
                                                   DestinationReference = gResult.Select(x => x.DestinationReference).FirstOrDefault(),
                                                   MovementReference = gResult.Select(x => x.MovementReference).FirstOrDefault(),
                                               };

            StoreAllFields(FieldStorage.Yes);
        }
    }
}