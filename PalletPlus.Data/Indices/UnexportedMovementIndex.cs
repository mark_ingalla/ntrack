﻿using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{
    public class UnexportedMovementIndex : AbstractIndexCreationTask<Movement>
    {
        public UnexportedMovementIndex()
        {
            Map = movements => from m in movements
                               where !m.IsExported && !m.IsCancelled && !m.IsDeleted &&
                                     m.PendingMovementCancellation == null &&
                                     m.PendingMovementCorrection == null &&
                                     m.MovementType == "Transfer" //&&
                                     //m.Direction == "Out"
                               orderby m.MovementDate
                               select new
                                   {
                                       m.SiteId,
                                       m.DocketNumber,
                                       m.IdString,
                                       m.SupplierId,
                                       m.MovementDate
                                   };
        }
    }
}
