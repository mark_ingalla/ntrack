using System;
using System.Collections.Generic;
using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{
    public class HistoricalCorrectionRequestIndex : AbstractMultiMapIndexCreationTask<HistoricalCorrectionRequestIndex.Result>
    {
        public class Result
        {
            public string Name { get; set; }
            public Guid CorrectionKey { get; set; }
            public string MovementStocktakeId { get; set; }
            public string DocketNumber { get; set; }
            public string SiteId { get; set; }
            public string SiteName { get; set; }
            public DateTime RequestedOn { get; set; }
            public KeyValuePair<string, string> RequestedBy { get; set; }
            public DateTime ClosedOn { get; set; }
            public KeyValuePair<string, string> ClosedBy { get; set; }
            public bool? IsApproved { get; set; }
            public DateTime UpdatedOn { get; set; }
        }

        public HistoricalCorrectionRequestIndex()
        {
            AddMap<Movement>(movements =>
                from movement in movements
                where movement.HistoricalCorrections != null && movement.HistoricalCorrections.Any()
                from correction in movement.HistoricalCorrections
                select new
                    {
                        Name = string.Format("Movement Correction ({0})", movement.MovementType),
                        CorrectionKey = correction.Value.CorrectionKey,
                        MovementStocktakeId = movement.IdString,
                        DocketNumber = movement.DocketNumber,
                        SiteId = movement.SiteId,
                        SiteName = LoadDocument<Site>(movement.SiteId).CompanyName,
                        RequestedOn = correction.Value.RequestedOn,
                        RequestedBy = correction.Value.RequestedBy,
                        ClosedOn = correction.Value.ClosedOn,
                        ClosedBy = correction.Value.ClosedBy,
                        IsApproved = correction.Value.IsApproved,
                        UpdatedOn = movement.UpdatedOn
                    });

            AddMap<Movement>(movements =>
                from movement in movements
                where movement.PendingMovementCancellation != null && movement.PendingMovementCancellation.IsApproved != null
                select new
                        {
                            Name = string.Format("Movement Cancellation ({0})", movement.MovementType),
                            CorrectionKey = movement.PendingMovementCancellation.CorrectionKey,
                            MovementStocktakeId = movement.IdString,
                            DocketNumber = movement.DocketNumber,
                            SiteId = movement.SiteId,
                            SiteName = LoadDocument<Site>(movement.SiteId).CompanyName,
                            RequestedOn = movement.PendingMovementCancellation.RequestedOn,
                            RequestedBy = movement.PendingMovementCancellation.RequestedBy,
                            ClosedOn = movement.PendingMovementCancellation.ClosedOn,
                            ClosedBy = movement.PendingMovementCancellation.ClosedBy,
                            IsApproved = movement.PendingMovementCancellation.IsApproved,
                            UpdatedOn = movement.UpdatedOn
                        });

            AddMap<Stocktake>(stocktakes =>
                              from stocktake in stocktakes
                              from asset in stocktake.EquipmentCounts
                              where asset.Value.HistoricalCorrections != null && asset.Value.HistoricalCorrections.Any()
                              from correction in asset.Value.HistoricalCorrections
                              select new
                              {
                                  Name = "Stocktake Correction (" + asset.Value.EquipmentName + ")",
                                  CorrectionKey = correction.CorrectionKey,
                                  MovementStocktakeId = stocktake.IdString,
                                  DocketNumber = (string)null,
                                  SiteId = stocktake.SiteId,
                                  SiteName = stocktake.SiteName,
                                  RequestedOn = correction.RequestedOn,
                                  RequestedBy = correction.RequestedBy,
                                  ClosedOn = correction.ClosedOn,
                                  ClosedBy = correction.ClosedBy,
                                  IsApproved = correction.IsApproved,
                                  UpdatedOn = stocktake.UpdatedOn
                              });


            Reduce = results => from result in results
                                group result by new { result.CorrectionKey }
                                    into gResult
                                    select new Result
                                           {
                                               MovementStocktakeId = gResult.Select(x => x.MovementStocktakeId).FirstOrDefault(),
                                               Name = gResult.Select(x => x.Name).FirstOrDefault(),
                                               CorrectionKey = gResult.Key.CorrectionKey,
                                               DocketNumber = gResult.Select(x => x.DocketNumber).FirstOrDefault(),
                                               SiteId = gResult.Select(x => x.SiteId).FirstOrDefault(),
                                               SiteName = gResult.Select(x => x.SiteName).FirstOrDefault(),
                                               RequestedOn = gResult.Select(x => x.RequestedOn).FirstOrDefault(),
                                               RequestedBy = gResult.Select(x => x.RequestedBy).FirstOrDefault(),
                                               ClosedOn = gResult.Select(x => x.ClosedOn).FirstOrDefault(),
                                               ClosedBy = gResult.Select(x => x.ClosedBy).FirstOrDefault(),
                                               IsApproved = gResult.Select(x => x.IsApproved).FirstOrDefault(),
                                               UpdatedOn = gResult.Select(x => x.UpdatedOn).FirstOrDefault()
                                           };

            StoreAllFields(FieldStorage.Yes);
        }
    }
}