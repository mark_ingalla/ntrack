using System;
using System.Collections.Generic;
using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{
    public class MovementIndex : AbstractIndexCreationTask<Movement, MovementIndex.Result>
    {
        public class Result
        {
            public string MovementId { get; set; }
            public string SiteId { get; set; }
            public string SiteAccountNumber { get; set; }
            public string SupplierId { get; set; }
            public string DestinationId { get; set; }
            public string DestinationName { get; set; }
            public string DestinationAccountNumber { get; set; }
            public string DestinationKind { get; set; }

            public DateTime MovementDate { get; set; }
            public DateTime EffectiveDate { get; set; }
            public string MovementType { get; set; }
            public string Direction { get; set; }
            public Guid? LocationKey { get; set; }

            public List<string> EquipmentKeys { get; set; }

            public Dictionary<string, Movement.EquipmentInfo> Equipment { get; set; }
            public Dictionary<string, ReconciliationData> ReconciledEquipment { get; set; }

            public MovementCorrection PendingMovementCorrection { get; set; }
            public MovementCancellation PendingMovementCancellation { get; set; }
            public bool IsReconciled { get; set; }
            public bool IsExported { get; set; }
            public bool IsPending { get; set; }

            public string ConsignmentNote { get; set; }
            public string DocketNumber { get; set; }
            public string DestinationReference { get; set; }
            public string MovementReference { get; set; }
        }

        public MovementIndex()
        {
            Map = movements =>
                from movement in movements
                where !movement.IsPendingPTA && !movement.IsCancelled
                select new Result
                {
                    MovementId = movement.IdString,
                    SiteId = movement.SiteId,
                    SiteAccountNumber = movement.SiteAccountNumber,
                    SupplierId = movement.SupplierId,
                    DestinationId = movement.DestinationId,
                    DestinationName = movement.DestinationName,
                    DestinationAccountNumber = movement.DestinationAccountNumber,
                    DestinationKind = movement.DestinationKind,

                    MovementDate = movement.MovementDate,
                    EffectiveDate = movement.EffectiveDate,
                    MovementType = movement.MovementType,
                    Direction = movement.Direction.ToLower() == "in" ? "Received" : "Sent",
                    LocationKey = movement.LocationKey,

                    EquipmentKeys = movement.Equipments.Select(x => x.Key).ToList(),

                    Equipment = movement.Equipments,
                    ReconciledEquipment = movement.ReconciledEquipment,

                    PendingMovementCorrection = movement.PendingMovementCorrection,
                    PendingMovementCancellation = movement.PendingMovementCancellation,
                    IsReconciled = movement.IsReconciled,
                    IsExported = movement.IsExported,
                    IsPending = movement.IsPending,

                    ConsignmentNote = movement.ConsignmentNote,
                    DocketNumber = movement.DocketNumber,
                    DestinationReference = movement.DestinationReference,
                    MovementReference = movement.MovementReference,
                };

            Reduce = results => from result in results
                                group result by new { result.MovementId }
                                    into gResult
                                    select new Result
                                               {
                                                   MovementId = gResult.Key.MovementId,
                                                   SiteId = gResult.Select(x => x.SiteId).FirstOrDefault(),
                                                   SiteAccountNumber = gResult.Select(x => x.SiteAccountNumber).FirstOrDefault(),

                                                   SupplierId = gResult.Select(x => x.SupplierId).FirstOrDefault(),

                                                   DestinationId = gResult.Select(x => x.DestinationId).FirstOrDefault(),
                                                   DestinationName = gResult.Select(x => x.DestinationName).FirstOrDefault(),
                                                   DestinationAccountNumber = gResult.Select(x => x.DestinationAccountNumber).FirstOrDefault(),
                                                   DestinationKind = gResult.Select(x => x.DestinationKind).FirstOrDefault(),

                                                   MovementDate = gResult.Select(x => x.MovementDate).FirstOrDefault(),
                                                   EffectiveDate = gResult.Select(x => x.EffectiveDate).FirstOrDefault(),
                                                   MovementType = gResult.Select(x => x.MovementType).FirstOrDefault(),
                                                   //  Direction = gResult.Select(x => x.Direction).FirstOrDefault().ToString() == "In" ? "Receive" : "Send",
                                                   Direction = gResult.Select(x => x.Direction).FirstOrDefault(),
                                                   LocationKey = gResult.Select(x => x.LocationKey).FirstOrDefault(),

                                                   EquipmentKeys = gResult.Select(x => x.EquipmentKeys).FirstOrDefault(),

                                                   Equipment = gResult.Select(x => x.Equipment).FirstOrDefault(),
                                                   ReconciledEquipment = gResult.Select(x => x.ReconciledEquipment).FirstOrDefault(),

                                                   PendingMovementCorrection = gResult.Select(x => x.PendingMovementCorrection).FirstOrDefault(),
                                                   PendingMovementCancellation = gResult.Select(x => x.PendingMovementCancellation).FirstOrDefault(),
                                                   IsReconciled = gResult.Select(x => x.IsReconciled).FirstOrDefault(),
                                                   IsExported = gResult.Select(x => x.IsExported).FirstOrDefault(),
                                                   IsPending = gResult.Select(x => x.IsPending).FirstOrDefault(),
                                                   ConsignmentNote = gResult.Select(x => x.ConsignmentNote).FirstOrDefault(),
                                                   DocketNumber = gResult.Select(x => x.DocketNumber).FirstOrDefault(),
                                                   DestinationReference = gResult.Select(x => x.DestinationReference).FirstOrDefault(),
                                                   MovementReference = gResult.Select(x => x.MovementReference).FirstOrDefault()
                                               };

            Index(p => p.DocketNumber, FieldIndexing.Analyzed);
            Index(p => p.DestinationName, FieldIndexing.Analyzed);
        }
    }
}