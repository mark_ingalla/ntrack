using PalletPlus.Data;
using Raven.Client.Indexes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PalletPlus.Data.Indices
{
    public class InvoiceAttachmentIndex : AbstractIndexCreationTask<InvoiceAttachment>
    {
        public class Result
        {
            public string InvoiceId { get; set; }
            public DateTime DateImported { get; set; }
            public string FileName { get; set; }
            public string SupplierId { get; set; }
            public string SupplierName { get; set; }
            public int ItemsCount { get; set; }
            public int UnreconciledItems { get; set; }
        }

        public InvoiceAttachmentIndex()
        {
            Map = invoices =>
                from invoice in invoices
                select new
                {
                    InvoiceId = invoice.IdString,
                    DateImported = invoice.DateImported,
                    FileName = invoice.FileName,
                    SupplierId = invoice.SupplierId,
                    SupplierName = (string)null,
                    ItemsCount = invoice.Details.Count(),
                    UnreconciledItems = invoice.UnreconciledItems
                };

            Reduce = results => from result in results
                                group result by new { result.InvoiceId }
                                    into gResult
                                    select new Result
                                    {
                                        InvoiceId = gResult.Key.InvoiceId,
                                        DateImported = gResult.Select(x => x.DateImported).FirstOrDefault(),
                                        FileName = gResult.Select(x => x.FileName).FirstOrDefault(),
                                        SupplierId = gResult.Select(x => x.SupplierId).FirstOrDefault(),
                                        SupplierName = (string)null,
                                        ItemsCount = gResult.Select(x => x.ItemsCount).FirstOrDefault(),
                                        UnreconciledItems = gResult.Select(x => x.UnreconciledItems).FirstOrDefault()
                                    };

            TransformResults =
               (database, results) => from result in results
                                    let alias = database.Load<Supplier>(result.SupplierId)
                                    select new Result
                                               {
                                                   InvoiceId = result.InvoiceId,
                                                   DateImported = result.DateImported,
                                                   FileName = result.FileName,
                                                   SupplierId = result.SupplierId,
                                                   SupplierName = alias.CompanyName,
                                                   ItemsCount = result.ItemsCount,
                                                   UnreconciledItems = result.UnreconciledItems
                                               };
        }
    }
}