using System;
using System.Collections.Generic;

namespace PalletPlus.Data.Indices
{
    public class ReportingMovementIndexResult : IRaportIndexResult
    {
        public ReportingMovementIndexResult()
        {

        }

        public string FakeIndexId { get; set; }

        public string Id { get; set; }
        public string SiteId { get; set; }
        public string SiteName { get; set; }

        public string SupplierId { get; set; }
        public string SupplierName { get; set; }

        public string DestinationName { get; set; }
        public string PartnerId { get; set; }

        public DateTime MovementDate { get; set; }
        public string MovementType { get; set; }
        public string MovementDirection { get; set; }
        public Guid? LocationKey { get; set; }

        public string Equipment { get; set; }
        public string EquipmentCode { get; set; }
        public int Quantity { get; set; }

        public Dictionary<string, ReconciliationData> ReconciledEquipment { get; set; }

        public MovementCorrection PendingMovementCorrection { get; set; }
        public MovementCancellation PendingMovementCancellation { get; set; }
        public bool IsCanceled { get; set; }
        public bool IsReconciled { get; set; }

        public string ConsignmentNote { get; set; }
        public string DocketNumber { get; set; }
        public string DestinationReference { get; set; }
        public string MovementReference { get; set; }
    }
}