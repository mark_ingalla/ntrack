﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{
    public class DayHireBalanceDashboardIndex : AbstractIndexCreationTask<Movement, DayHireBalanceDashboardIndex.Result>
    {
        public class Result
        {
            public int Issues { get; set; }
            public int Returns { get; set; }
            public int TransfersIn { get; set; }
            public int TransfersOut { get; set; }
            public string EquipmentName { get; set; }
            public string EquipmentCode { get; set; }
            public string SiteId { get; set; }
            public string SiteName { get; set; }
            public DateTime MovementDate { get; set; }
            public int IncomingQty { get; set; }            
            public int Day { get; set; }
            public int Week { get; set; }
            public int Month { get; set; }
            public int Year { get; set; }
            public int ClosingBalance { get; set; }
            
        }

        public DayHireBalanceDashboardIndex()
        {
            Map = movements =>
                  from movement in movements
                  where !movement.IsCancelled
                  from eq in movement.Equipments
                  select new Result()
                  {
                   
                      EquipmentName = eq.Value.EquipmentName,
                      EquipmentCode = eq.Key,
                      SiteId = movement.SiteId,
                      SiteName = LoadDocument<Site>(movement.SiteId).CompanyName,
                      TransfersIn = movement.MovementType == "Transfer" && movement.Direction == "In" ? eq.Value.Quantity : 0,
                      TransfersOut = movement.MovementType == "Transfer" && movement.Direction == "Out" ? eq.Value.Quantity : 0,
                      Issues = movement.DestinationKind == "Supplier" && movement.Direction == "In" ? eq.Value.Quantity : 0,
                      Returns = movement.DestinationKind == "Supplier" && movement.Direction == "Out" ? eq.Value.Quantity : 0,
                      IncomingQty = 0,
                      MovementDate = movement.MovementDate,
                 //     Day = movement.MovementDate.Day,
                      Week = (int)Math.Floor((decimal)movement.MovementDate.Date.DayOfYear / 7),
                //      Month = movement.MovementDate.Month,
                      Year = movement.MovementDate.Year,
                      ClosingBalance = 0
                  };

            Reduce = results => from r in results
                             //   group r by new { r.SiteId, r.EquipmentCode, r.MovementDate.Day, r.MovementDate.Month,r.MovementDate.Year } into gr
                                group r by new { r.SiteId, r.EquipmentCode, r.Week,r.Year } into gr
                                select new Result
                                {
                              SiteId = gr.Key.SiteId,
                              SiteName = gr.Select(x=>x.SiteName).FirstOrDefault(),
                                    EquipmentName = gr.Select(x => x.EquipmentName).FirstOrDefault(),
                                    EquipmentCode = gr.Key.EquipmentCode,
                                    TransfersIn = gr.Sum(x => x.TransfersIn),
                                    TransfersOut = gr.Sum(x => x.TransfersOut),
                                    Issues = gr.Sum(x => x.Issues),
                                    Returns = gr.Sum(x => x.Returns),
                                    MovementDate = gr.Select(x=>x.MovementDate).FirstOrDefault(),
                                    IncomingQty = gr.Sum(x => x.TransfersIn) + gr.Sum(x => x.Issues),
                                    Week = gr.Key.Week,
                                    //Day = gr.Key.Day,
                                    //Month = gr.Key.Month,
                                    Year = gr.Key.Year,
                                    ClosingBalance = gr.Sum(x => x.TransfersIn) + gr.Sum(x => x.Issues) - gr.Sum(x => x.TransfersOut) - gr.Sum(x => x.Returns)

                                };

            StoreAllFields(FieldStorage.Yes);

        }
    }
}
