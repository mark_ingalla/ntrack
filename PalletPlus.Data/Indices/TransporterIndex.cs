using Raven.Client.Indexes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PalletPlus.Data.Indices
{
    public class TransporterIndex : AbstractIndexCreationTask<Transporter, TransporterIndex.Result>
    {
        public class Result
        {
            public string TransporterId { get; set; }
            public string TransporterName { get; set; }
            public string Suburb { get; set; }
            public string State { get; set; }
            public string Postcode { get; set; }
            public string ContactEmail { get; set; }

            public bool IsDeleted { get; set; }
        }

        public TransporterIndex()
        {
            Map = transporters =>
                from transporter in transporters
                select new
                {
                    TransporterId = transporter.IdString,
                    TransporterName = transporter.CompanyName,
                    Suburb = transporter.Suburb,
                    State = transporter.State,
                    Postcode = transporter.Postcode,
                    ContactEmail = transporter.ContactEmail,
                    IsDeleted = transporter.IsDeleted
                };

            Reduce = results => from result in results
                                group result by new { result.TransporterId }
                                    into gResult
                                    select new Result
                                    {
                                        TransporterId = gResult.Key.TransporterId,
                                        TransporterName = gResult.Select(x => x.TransporterName).FirstOrDefault(),
                                        Suburb = gResult.Select(x => x.Suburb).FirstOrDefault(),
                                        State = gResult.Select(x => x.State).FirstOrDefault(),
                                        Postcode = gResult.Select(x => x.Postcode).FirstOrDefault(),
                                        ContactEmail = gResult.Select(x => x.ContactEmail).FirstOrDefault(),
                                        IsDeleted = gResult.Select(x => x.IsDeleted).FirstOrDefault()
                                    };
        }
    }
}