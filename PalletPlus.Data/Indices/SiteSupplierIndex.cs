using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{
    public class SiteSupplierIndex : AbstractMultiMapIndexCreationTask<SiteSupplierIndex.Result>
    {
        public class Result
        {
            public string SiteId { get; set; }
            public string SiteName { get; set; }
            public string SupplierId { get; set; }
            public string SupplierName { get; set; }
            public string SupplierAccountNumber { get; set; }
            public string Prefix { get; set; }
            public string Suffix { get; set; }
            public int SequenceStartNumber { get; set; }
            public bool IsDeleted { get; set; }
            public int Order { get; set; }
        }

        public SiteSupplierIndex()
        {
            AddMap<Site>(sites =>
                         from site in sites
                         from supplier in site.Suppliers
                         select new
                                    {
                                        SiteId = site.IdString,
                                        SiteName = site.CompanyName,
                                        SupplierId = supplier.Value.CompanyId,
                                        SupplierName = supplier.Value.CompanyName,
                                        SupplierAccountNumber = supplier.Value.AccountNumber,
                                        Prefix = supplier.Value.Prefix,
                                        Suffix = supplier.Value.Suffix,
                                        SequenceStartNumber = supplier.Value.SequenceNumber,
                                        IsDeleted = site.IsDeleted,
                                        Order = 0
                                    });

            AddMap<Account>(accounts =>
                            from account in accounts
                            from supplier in account.Suppliers
                            select new
                                             {
                                                 SiteId = account.IdString,
                                                 SiteName = account.CompanyName,
                                                 SupplierId = supplier.Value.CompanyId,
                                                 SupplierName = supplier.Value.CompanyName,
                                                 SupplierAccountNumber = supplier.Value.AccountNumber,
                                                 Prefix = supplier.Value.Prefix,
                                                 Suffix = supplier.Value.Suffix,
                                                 SequenceStartNumber = supplier.Value.SequenceNumber,
                                                 IsDeleted = account.IsDeleted,
                                                 Order = 1
                                             });

            Reduce = results => from result in results
                                group result by new { CompanyId = result.SiteId, result.SupplierId }
                                    into gResult
                                    select new Result
                                               {
                                                   SiteId = gResult.Key.CompanyId,
                                                   SiteName = gResult.Select(x => x.SiteName).FirstOrDefault(),
                                                   SupplierId = gResult.Key.SupplierId,
                                                   SupplierName = gResult.Select(x => x.SupplierName).FirstOrDefault(),
                                                   SupplierAccountNumber = gResult.Select(x => x.SupplierAccountNumber).FirstOrDefault(),
                                                   Prefix = gResult.Select(x => x.Prefix).FirstOrDefault(),
                                                   Suffix = gResult.Select(x => x.Suffix).FirstOrDefault(),
                                                   SequenceStartNumber = gResult.Select(x => x.SequenceStartNumber).FirstOrDefault(),
                                                   IsDeleted = gResult.Select(x => x.IsDeleted).FirstOrDefault(),
                                                   Order = gResult.Select(x => x.Order).OrderBy(x => x).FirstOrDefault()
                                               };

            StoreAllFields(FieldStorage.Yes);
        }
    }
}