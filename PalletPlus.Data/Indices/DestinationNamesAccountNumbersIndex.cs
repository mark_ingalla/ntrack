using System.Linq;
using Raven.Client.Indexes;

namespace PalletPlus.Data.Indices
{
    /// <summary>
    /// This index is for names of either partner, site or a supplier
    /// (depends on what kind of movement do we create in Record Movement view)
    /// </summary>
    public class DestinationNamesAccountNumbersIndex : AbstractMultiMapIndexCreationTask<DestinationNamesAccountNumbersIndex.Result>
    {
        public class Result
        {
            public string CompanyId { get; set; }
            public string CompanyName { get; set; }
            public string AccountNumber { get; set; }
            public string SupplierId { get; set; }
            public string AddressLine1 { get; set; }
            public string AddressLine2 { get; set; }
            public string Suburb { get; set; }
            public string State { get; set; }
            public string Postcode { get; set; }
            public string Kind { get; set; }
        }

        public DestinationNamesAccountNumbersIndex()
        {
            AddMap<Partner>(partners =>
                            from partner in partners
                            from supplier in partner.Suppliers
                            select new
                                       {
                                           CompanyId = partner.IdString,
                                           CompanyName = partner.CompanyName,
                                           AccountNumber = supplier.Value.AccountNumber,
                                           SupplierId = supplier.Value.SupplierId,
                                           AddressLine1 = partner.AddressLine1,
                                           AddressLine2 = partner.AddressLine2,
                                           Suburb = partner.Suburb,
                                           State = partner.State,
                                           Postcode = partner.Postcode,
                                           Kind = "Partner"
                                       });

            AddMap<Site>(sites =>
                         from site in sites
                         let account = LoadDocument<Account>(site.AccountId)
                         from supplier in site.Suppliers
                         select new
                                    {
                                        CompanyId = site.IdString,
                                        CompanyName = account.CompanyName + " - " + site.CompanyName,
                                        AccountNumber = supplier.Value.AccountNumber,
                                        SupplierId = supplier.Key,
                                        AddressLine1 = site.AddressLine1,
                                        AddressLine2 = site.AddressLine2,
                                        Suburb = site.Suburb,
                                        State = site.State,
                                        Postcode = site.Postcode,
                                        Kind = "Site"
                                    });

            AddMap<Site>(sites =>
                         from site in sites
                         let account = LoadDocument<Account>(site.AccountId)
                         from supplier in account.Suppliers
                         select new
                         {
                             CompanyId = site.IdString,
                             CompanyName = account.CompanyName + " - " + site.CompanyName,
                             AccountNumber = supplier.Value.AccountNumber,
                             SupplierId = supplier.Key,
                             AddressLine1 = site.AddressLine1,
                             AddressLine2 = site.AddressLine2,
                             Suburb = site.Suburb,
                             State = site.State,
                             Postcode = site.Postcode,
                             Kind = "Site"
                         });

            AddMap<Supplier>(suppliers =>
                        from supplier in suppliers
                        from location in supplier.Locations
                        select new
                                   {
                                       CompanyId = "suppliers/" + location.Key.ToString(),
                                       CompanyName = supplier.CompanyName + " - " + location.Value.CompanyName,
                                       AccountNumber = location.Value.AccountNumber,
                                       SupplierId = supplier.IdString,
                                       AddressLine1 = location.Value.AddressLine1,
                                       AddressLine2 = location.Value.AddressLine2,
                                       Suburb = location.Value.Suburb,
                                       State = location.Value.State,
                                       Postcode = location.Value.Postcode,
                                       Kind = "Supplier"
                                   });

            Reduce = results => from result in results
                                group result by new { result.CompanyId, result.SupplierId }
                                    into gResult
                                    select new Result
                                               {
                                                   CompanyId = gResult.Key.CompanyId,
                                                   AccountNumber = gResult.Select(x => x.AccountNumber).FirstOrDefault(x => x != null),
                                                   CompanyName = gResult.Select(x => x.CompanyName).FirstOrDefault(),
                                                   SupplierId = gResult.Key.SupplierId,
                                                   AddressLine1 = gResult.Select(x => x.AddressLine1).FirstOrDefault(),
                                                   AddressLine2 = gResult.Select(x => x.AddressLine2).FirstOrDefault(),
                                                   Suburb = gResult.Select(x => x.Suburb).FirstOrDefault(),
                                                   State = gResult.Select(x => x.State).FirstOrDefault(),
                                                   Postcode = gResult.Select(x => x.Postcode).FirstOrDefault(),
                                                   Kind = gResult.Select(x => x.Kind).FirstOrDefault()
                                               };
        }
    }
}