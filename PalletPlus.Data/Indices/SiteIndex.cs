using Raven.Client.Indexes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PalletPlus.Data.Indices
{
    public class SiteIndex : AbstractIndexCreationTask<Site, SiteIndex.Result>
    {
        public class Result
        {
            public string SiteId { get; set; }
            public string SiteName { get; set; }
            public string AddressLine1 { get; set; }
            public string AddressLine2 { get; set; }
            public string Suburb { get; set; }
            public string State { get; set; }
            public string Postcode { get; set; }
            public string ContactEmail { get; set; }
            public string AccountName { get; set; }

            public Dictionary<string, CompanySupplier> Suppliers { get; set; } 

            public bool IsDeleted { get; set; }
        }

        public SiteIndex()
        {
            Map = (sites) =>
                from site in sites
                select new
                {
                    SiteId = site.IdString,
                    SiteName = site.CompanyName,
                    AddressLine1 = site.AddressLine1,
                    AddressLine2 = site.AddressLine2,
                    Suburb = site.Suburb,
                    State = site.State,
                    Postcode = site.Postcode,
                    ContactEmail = site.ContactEmail,
                    Suppliers = site.Suppliers,
                    IsDeleted = site.IsDeleted,
                    AccountName = LoadDocument<Account>(site.AccountId).CompanyName
                };

            Reduce = results => from result in results
                                group result by new { result.SiteId }
                                    into gResult
                                    select new Result
                                    {
                                        SiteId = gResult.Key.SiteId,
                                        SiteName = gResult.Select(x => x.SiteName).FirstOrDefault(),
                                        AddressLine1 = gResult.Select(x => x.AddressLine1).FirstOrDefault(),
                                        AddressLine2 = gResult.Select(x => x.AddressLine2).FirstOrDefault(),
                                        Suburb = gResult.Select(x => x.Suburb).FirstOrDefault(),
                                        State = gResult.Select(x => x.State).FirstOrDefault(),
                                        Postcode = gResult.Select(x => x.Postcode).FirstOrDefault(),
                                        ContactEmail = gResult.Select(x => x.ContactEmail).FirstOrDefault(),
                                        Suppliers = gResult.Select(x => x.Suppliers).FirstOrDefault(),
                                        IsDeleted = gResult.Select(x => x.IsDeleted).FirstOrDefault(),
                                        AccountName = gResult.Select(x => x.AccountName).FirstOrDefault()
                                    };
        }
    }
}