﻿using System;
using System.Collections.Generic;
using nTrack.Data;

namespace PalletPlus.Data
{
    public class Partner : BaseModel
    {
        public string CompanyName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string CompanyPhone { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public bool IsPTAEnabled { get; set; }
        public List<string> Tags { get; set; }

        public Dictionary<string, PartnerSupplier> Suppliers { get; set; }
        public HashSet<string> Sites { get; set; }
        public HashSet<string> Transporters { get; set; }
        public Dictionary<Guid, PartnerDocument> Documents { get; set; }


        public Partner()
        {
            Suppliers = new Dictionary<string, PartnerSupplier>();
            Sites = new HashSet<string>();
            Transporters = new HashSet<string>();
            Documents = new Dictionary<Guid, PartnerDocument>();
            Tags = new List<string>();
        }
    }

    public class PartnerSupplier
    {
        public string SupplierId { get; set; }
        public string AccountNumber { get; set; }
        public bool IsExchangeEnabled { get; set; }

        public HashSet<string> Equipments { get; set; }

        public PartnerSupplier()
        {
            Equipments = new HashSet<string>();
        }
    }

    public class PartnerDocument
    {
        public string Name { get; set; }
        public string MIME { get; set; }
        public DateTime UploadDateUTC { get; set; }
        public double ByteSize { get; set; }
    }
}
