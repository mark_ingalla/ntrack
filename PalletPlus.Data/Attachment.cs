﻿using System;
using nTrack.Data;

namespace PalletPlus.Data
{
    public abstract class Attachment : BaseModel
    {
        public string FileName { get; set; }
    }

    /// <summary>
    /// Export Movement Attachment
    /// </summary>
    public class ExportAttachment : Attachment
    {
        public DateTime MovementDate { get; set; }
        public DateTime SentOn { get; set; }
        public string SupplierId { get; set; }
        public string SiteId { get; set; }
        public int TotalDownloads { get; set; }
        public int TotalMovements { get; set; }
    }

    /// <summary>
    /// This represents the attachment the user uploaded to the system
    /// </summary>
    public class InvoiceAttachment : Attachment
    {
        public string SupplierId { get; set; }
        public DateTime UploadedOn { get; set; }
    }

    public class ImportPartnersAttachment : Attachment
    {
        public DateTime UploadedOn { get; set; }
    }
}
