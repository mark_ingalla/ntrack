﻿using System;
using System.Collections.Generic;
using System.Linq;
using nTrack.Data;

namespace PalletPlus.Data
{
    public class Stocktake : BaseModel
    {
        public string SiteId { get; set; }
        public string SiteName { get; set; }

        public string SupplierId { get; set; }
        public string SupplierName { get; set; }

        public DateTime TakenOn { get; set; }
        public Dictionary<string, EquipmentCount> EquipmentCounts { get; set; }
        public bool GotPendingCorrections
        {
            get { return EquipmentCounts.Any(x => x.Value.PendingCorrection != null); }
        }

        public Stocktake()
        {
            EquipmentCounts = new Dictionary<string, EquipmentCount>();
        }
    }

    public class EquipmentCount
    {
        public string EquipmentName { get; set; }
        public int Quantity { get; set; }

        public StocktakeCorrection PendingCorrection { get; set; }

        public HashSet<StocktakeCorrection> HistoricalCorrections { get; set; }

        public EquipmentCount()
        {
            HistoricalCorrections = new HashSet<StocktakeCorrection>();
        }
    }
}
