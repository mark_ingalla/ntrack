﻿using System.Collections.Generic;

namespace PalletPlus.Data
{
    public abstract class LookupBase
    {
        public string Id { get; set; }
        public Dictionary<string, string> Lookup { get; set; }

        protected LookupBase()
        {
            Lookup = new Dictionary<string, string>();
        }
    }

    public class SupplierIntegrationLookup : LookupBase
    {
        public static string LookupId { get { return "Lookup/SupplierIntegration"; } }

        public SupplierIntegrationLookup()
        {
            Id = LookupId;
        }
    }
}