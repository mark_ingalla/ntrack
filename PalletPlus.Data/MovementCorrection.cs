﻿using System;
using System.Collections.Generic;

namespace PalletPlus.Data
{
    public class MovementCorrection
    {
        public Guid CorrectionKey { get; set; }

        public DateTime RequestedOn { get; set; }
        public DateTime? ClosedOn { get; set; }

        public KeyValuePair<Guid, string> RequestedBy { get; set; }
        public KeyValuePair<Guid, string> ClosedBy { get; set; }

        public DateTime OldMovementDate { get; set; }
        public DateTime NewMovementDate { get; set; }

        public DateTime OldEffectiveDate { get; set; }
        public DateTime NewEffectiveDate { get; set; }

        public Dictionary<string, QuantityInfo> AssetCodeQuantity { get; set; }
        public string Reason { get; set; }

        public bool? IsApproved { get; set; } //true accepted, false declined

        public MovementCorrection()
        { }

        public MovementCorrection(Guid key, KeyValuePair<Guid, string> requestedBy, DateTime requestedOn, Dictionary<string, int> assetCodeNewQuantities,
            Dictionary<string, int> assetCodeOldQuantities, DateTime oldMovementDate, DateTime newMovementDate, DateTime oldEffectiveDate, DateTime newEffectiveDate, string reason)
        {
            AssetCodeQuantity = new Dictionary<string, QuantityInfo>();

            CorrectionKey = key;
            RequestedBy = requestedBy;
            RequestedOn = requestedOn;

            foreach (var q in assetCodeNewQuantities)
            {
                AssetCodeQuantity.Add(q.Key, new QuantityInfo(q.Value, assetCodeOldQuantities[q.Key]));
            }

            OldMovementDate = oldMovementDate;
            NewMovementDate = newMovementDate;
            OldEffectiveDate = oldEffectiveDate;
            NewEffectiveDate = newEffectiveDate;
            Reason = reason;
        }

        public void ChangeCorrectionStatus(bool accepted, KeyValuePair<Guid, string> closedBy)
        {
            ClosedOn = DateTime.UtcNow;

            IsApproved = accepted;
            ClosedBy = closedBy;
        }
    }
}
