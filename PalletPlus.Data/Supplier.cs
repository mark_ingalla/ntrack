﻿using System;
using System.Collections.Generic;
using nTrack.Data;

namespace PalletPlus.Data
{
    public class Supplier : BaseModel
    {
        public string CompanyName { get; set; }
        public string ContactEmail { get; set; }
        public string ProviderName { get; set; }
        public bool IsPTAEnabled { get; set; }

        public Dictionary<Guid, SupplierLocation> Locations { get; set; }
        public Dictionary<string, Equipment> Equipments { get; set; }

        public Supplier()
        {
            Equipments = new Dictionary<string, Equipment>();
            Locations = new Dictionary<Guid, SupplierLocation>();
        }
    }

    public class SupplierLocation
    {
        public Guid LocationKey { get; set; }
        public string CompanyName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string CompanyPhone { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }

        //This is the supplier account number for this location
        public string AccountNumber { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class Equipment
    {
        public string EquipmentCode { get; set; }
        public string EquipmentName { get; set; }
        public bool IsDeleted { get; set; }
    }
}
