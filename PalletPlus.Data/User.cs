﻿using System;

namespace PalletPlus.Data
{
    public class User
    {
        public string UserId { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Role { get; set; }
    }
}
