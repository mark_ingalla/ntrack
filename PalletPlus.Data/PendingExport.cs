﻿using System;
using nTrack.Data.Extenstions;
using nTrack.Extension;

namespace PalletPlus.Data
{
    /// <summary>
    /// This is an entity to lock any further import process from happening while there is a pending one
    /// </summary>
    public class PendingExport
    {
        public string Id { get; set; }
        public Guid SiteId { get; set; }
        public Guid SupplierId { get; set; }
        public DateTime StartedOn { get; set; }
        public int TotalExported { get; set; }

        public static string GenerateId(Guid siteId, Guid supplierId)
        {
            return "{0}{1}/{2}".Fill(DocHelpers.CollectionName<PendingExport>(), siteId, supplierId);
        }
    }
}