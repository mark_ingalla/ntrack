﻿using System;
using System.Collections.Generic;
using nTrack.Data;

namespace PalletPlus.Data
{
    public class Site : BaseModel
    {
        public string AccountId { get; set; }
        public string CompanyName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string CompanyPhone { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }

        public Dictionary<string, CompanySupplier> Suppliers { get; set; }
        public HashSet<Guid> Users { get; set; }


        public int PTASequenceNumber { get; set; }
        public string PTASuffix { get; set; }
        public bool IsPTAEnabled { get; set; }

        public int ExchangeSequenceNumber { get; set; }


        public Site()
        {
            Suppliers = new Dictionary<string, CompanySupplier>();
            Users = new HashSet<Guid>();
        }
    }

    public class CompanySupplier
    {
        public string CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string AccountNumber { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public int SequenceNumber { get; set; }
    }
}
