﻿
using System;
using System.Collections.Generic;
using System.Linq;
using nTrack.Data;

namespace PalletPlus.Data
{
    public class SupplierInvoice : BaseModel
    {
        public string SupplierId { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string InvoiceNumber { get; set; }
        public int TotalItems { get { return Details.Count; } }
        public List<InvoiceDetail> Details { get; set; }
        public bool IsClosed { get; set; }
        public bool IsReconciled { get { return Details.All(x => x.IsReconciled); } }

        public SupplierInvoice()
        {
            Details = new List<InvoiceDetail>();
        }


        public class InvoiceDetail
        {
            public Guid Key { get; set; }
            public string TransactionCode { get; set; }
            public string DocketNumber { get; set; }
            public string EquipmentCode { get; set; }
            public int Qty { get; set; }
            public int DaysHire { get; set; }
            public string AccountNumber { get; set; }
            public string AccountRef { get; set; }
            public string DestinationAccountNumber { get; set; }
            public string DestinationRef { get; set; }
            public string SupplierRef { get; set; }
            public string ConsignmentRef { get; set; }
            public DateTime MovementDate { get; set; }
            public DateTime EffectiveDate { get; set; }
            public DateTime DehireDate { get; set; }

            public bool IsReconciled { get; set; }
        }
    }
}
