﻿using System;

namespace PalletPlus.Data
{

    public class PartnersImport
    {
        public Guid AccountId { get; set; }
        public string FileName { get; set; }
        public int TotalItems { get; set; }
        public DateTime ImportDateUTC { get; set; }
        public string Log { get; set; }

        public PartnersImport(Guid accountId, string fileName, int totalItems, DateTime importDateUTC, string log)
        {
            AccountId = accountId;
            FileName = fileName;
            TotalItems = totalItems;
            ImportDateUTC = importDateUTC;
            Log = log;
        }
    }
}
