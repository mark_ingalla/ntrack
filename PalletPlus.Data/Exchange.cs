﻿using System;
using System.Collections.Generic;
using nTrack.Data;

namespace PalletPlus.Data
{
    public class Exchange : BaseModel
    {
        public string SiteId { get; set; }
        //public string SiteName { get; set; }
        //public string SiteAccountNumber { get; set; }
        public string SiteAddress { get; set; }

        public string SupplierId { get; set; }
        //public string SupplierName { get; set; }

        public string PartnerId { get; set; }
        public string PartnerName { get; set; }
        //public string PartnerAccountNumber { get; set; }
        public string PartnerAddress { get; set; }

        public string DocketNumber { get; set; }
        public Dictionary<string, EquipmentInfo> EquipmentsIn { get; set; }
        public Dictionary<string, EquipmentInfo> EquipmentsOut { get; set; }

        public DateTime EffectiveDate { get; set; }
        public string PartnerReference { get; set; }
        public string ConsignmentReference { get; set; }


        public class EquipmentInfo
        {
            public string EquipmentName { get; set; }
            public int Quantity { get; set; }
        }
    }

    
}
