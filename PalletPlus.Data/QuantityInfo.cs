﻿
namespace PalletPlus.Data
{
    public class QuantityInfo
    {
        public int OldQuantity { get; set; } //they're null at the beginning (the state before any approval decision) cause we can check them in movement data model
        public int NewQuantity { get; set; }

        public QuantityInfo(int newQuantity, int oldQuantity)
        {
            OldQuantity = oldQuantity;
            NewQuantity = newQuantity;
        }
    }
}
