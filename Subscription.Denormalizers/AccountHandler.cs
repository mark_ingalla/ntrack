﻿using System;
using System.Linq;
using NServiceBus;
using nTrack.Core.Utilities;
using nTrack.Data;
using nTrack.Data.Extenstions;
using Raven.Client;
using StructureMap.Attributes;
using Subscription.Data;
using Subscription.Messages.Events.Account;
using Subscription.Messages.Events.Account.User;
using Account = Subscription.Data.Account;

namespace Subscription.Denormalizers
{
    public class AccountHandler :
        IHandleMessages<AccountCreated>,
        IHandleMessages<AccountUpdated>,
        IHandleMessages<AccountActivated>,
        IHandleMessages<AccountDeactivated>,

        //User Management
        IHandleMessages<UserAdded>,
        IHandleMessages<UserRemoved>,
        IHandleMessages<UserDetailUpdated>,
        IHandleMessages<UserLocked>,
        IHandleMessages<UserUnlocked>,
        IHandleMessages<UserPasswordChanged>,
        IHandleMessages<UserRoleChanged>,
        IHandleMessages<UserRetrieved>,

        //Billing
        IHandleMessages<SubscriptionCreated>,
        IHandleMessages<SubscriptionTerminated>,
        IHandleMessages<InvoiceRaised>,
        IHandleMessages<InvoicePaid>
    {
        private string CreateActivityLog(string message)
        {
            return string.Format("[{0}]: {1}", DateTime.UtcNow, message);
        }

        [SetterProperty]
        public IDocumentSession Session { get; set; }


        public void Handle(AccountCreated message)
        {
            //make sure the account accountId is unique
            var existing = Session.Advanced.DocumentStore.DatabaseCommands.Head(message.AccountId.ToStringId<Account>());
            if (existing != null)
                throw new Exception("Account already exists!");

            
            Session.Store(new Account
                {
                    Id = message.AccountId,
                    CompanyName = message.CompanyName,
                    CompanyPhone = message.CompanyPhone,
                    Country = message.Country,
                    Ip = message.AddressIp
                });
        }

        public void Handle(AccountUpdated message)
        {
            var account = Session.Load<Account>(message.AccountId);

            account.CompanyName = message.CompanyName;
            account.CompanyPhone = message.CompanyPhone;
            account.AddressLine1 = message.AddressLine1;
            account.AddressLine2 = message.AddressLine2;
            account.Suburb = message.Suburb;
            account.State = message.State;
            account.CompanyPhone = message.CompanyPhone;
            account.Postcode = message.Postcode;
            account.Country = message.Country;

        }
        public void Handle(AccountActivated message)
        {
            var account = Session.Load<Account>(message.AccountId);
            account.IsActive = true;
        }
        public void Handle(AccountDeactivated message)
        {
            var account = Session.Load<Account>(message.AccountId);
            account.IsActive = false;
        }

        public void Handle(UserAdded message)
        {
            var account = Session.Load<Account>(message.AccountId);

            //NOTE: moved hasing password here
            var cryptoService = new CryptographyService();
            var user = new User
                           {
                               UserKey = message.UserKey,

                               Email = message.Email,
                               Password = cryptoService.GetMD5Hash(message.Password),

                               FirstName = message.FirstName,
                               LastName = message.LastName,
                               Role = message.Role.ToString()
                           };

            user.ActivityLog.Add(CreateActivityLog("User was created."));

            account.Users.Add(user);
        }
        public void Handle(UserRemoved message)
        {
            var account = Session.Load<Account>(message.AccountId);
            account.Users.Single(x => x.UserKey == message.UserKey).IsDeleted = true;
        }

        public void Handle(UserRetrieved message)
        {
            var account = Session.Load<Account>(message.AccountId);
            account.Users.Single(x => x.UserKey == message.UserKey).IsDeleted = false;
        }

        public void Handle(UserDetailUpdated message)
        {
            var account = Session.Load<Account>(message.AccountId);
            var user = account.Users.Single(x => x.UserKey == message.UserKey);

            user.FirstName = message.FirstName;
            user.LastName = message.LastName;
            user.Phone = message.Phone;
            user.Mobile = message.Mobile;
            user.Email = message.Email;

            user.ActivityLog.Add(CreateActivityLog("User was updated."));
        }
        public void Handle(UserLocked message)
        {
            var account = Session.Load<Account>(message.AccountId);
            var user = account.Users.Single(x => x.UserKey == message.UserKey);
            user.IsLocked = true;

            user.ActivityLog.Add(CreateActivityLog(string.Format("User was locked: {0}.", message.Reason)));
        }
        public void Handle(UserUnlocked message)
        {
            var account = Session.Load<Account>(message.AccountId);
            var user = account.Users.Single(x => x.UserKey == message.UserKey);
            user.IsLocked = false;

            user.ActivityLog.Add(CreateActivityLog(string.Format("User was unlocked: {0}.", message.Reason)));
        }
        public void Handle(UserPasswordChanged message)
        {
            var account = Session.Load<Account>(message.AccountId);
            var user = account.Users.Single(x => x.UserKey == message.UserKey);
            user.Password = message.Password;
            user.ActivityLog.Add(CreateActivityLog("User password was changed."));
        }
        public void Handle(UserRoleChanged message)
        {
            var account = Session.Load<Account>(message.AccountId);
            var user = account.Users.Single(x => x.UserKey == message.UserKey);
            user.Role = message.Role.ToString();
        }

        public void Handle(SubscriptionCreated message)
        {
            var account = Session.Load<Account>(message.AccountId);
            account.Subscriptions.Add(new AccountSubscription
                                          {
                                              Key = message.SubscriptionKey,
                                              ProductId = message.ProductId.ToStringId<Product>(),
                                              ProductCode = message.ProductCode,
                                              OfferKey = message.OfferKey,
                                              SubscribedOnUTC = message.SubscribedOnUTC,
                                              Amount = message.Amount,
                                              OfferQTY = message.OfferQTY
                                          });
        }

        public void Handle(SubscriptionTerminated message)
        {
            var account = Session.Load<Account>(message.AccountId);
            var subscription = account.Subscriptions.Single(x => x.Key == message.SubscriptionKey && !x.TerminatedOnUTC.HasValue);

            subscription.TerminatedOnUTC = message.TerminatedOnUTC;

        }
        public void Handle(InvoiceRaised message)
        {
            var invoice = new Invoice
                {
                    Id = message.InvoiceId,
                    AccountId = message.AccountId.ToStringId<Account>(),
                    InvoiceNumber = message.InvoiceNumber,
                    IssuedOnUTC = message.InvoiceDateUTC,
                    Details = message.Details.Select(x => new Data.InvoiceDetail
                        {
                            ProductId = x.ProductId.ToStringId<Product>(),
                            ProductCode = x.ProductCode,
                            OfferKey = x.OfferKey,
                            LineDescription = "TBA: Product Name - Offer Name",
                            LineTotal = x.Amount
                        }).ToArray()
                };

            Session.Store(invoice);
        }
        public void Handle(InvoicePaid message)
        {
            var inovice = Session.Load<Invoice>(message.InvoiceId);
            inovice.Payments.Add(new InvoicePayment { Amount = message.Amount, PaidOnUTC = message.PaidOnUTC });
        }


        
    }
}
