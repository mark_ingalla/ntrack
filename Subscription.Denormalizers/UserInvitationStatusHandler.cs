﻿using NServiceBus;
using Raven.Client;
using Subscription.Data;
using Subscription.Messages.Events;

namespace Subscription.Denormalizers
{
    public class UserInvitationStatusHandler : IHandleMessages<UserInvitationStatus>
    {
        private readonly IDocumentSession _dbSession;

        public UserInvitationStatusHandler(IDocumentSession dbSession)
        {
            _dbSession = dbSession;
        }


        public void Handle(UserInvitationStatus message)
        {
            if (message.Status == "Pending")
            {
                _dbSession.Store(new UserInvitation
                {
                    InvitationToken = message.VerificationToken,
                    Email = message.UserEmail
                });
            }
            else if (message.Status == "Completed" || message.Status == "Timeouted" || message.Status == "BouncedBack")
            {
                UserInvitation userInvitation = _dbSession.Load<UserInvitation>(message.VerificationToken);
                _dbSession.Delete(userInvitation);
            }
        }
    }
}