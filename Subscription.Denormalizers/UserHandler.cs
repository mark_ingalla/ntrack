﻿using System;
using System.Collections.Generic;
using System.Linq;
using NServiceBus;
using nTrack.Core.Utilities;
using Raven.Client;
using Subscription.Data;
using Subscription.Data.Indices;
using Subscription.Messages.Events.User;

namespace Subscription.Denormalizers
{
    public class UserHandler : 
        IHandleMessages<UserCreated>,
        IHandleMessages<UserAssignedToAccount>,
        IHandleMessages<UserAssignmentToAccountRemoved>,
        IHandleMessages<PasswordReseted>,
        IHandleMessages<PasswordChanged>,
        IHandleMessages<UserVerified>,
        IHandleMessages<UserDetailUpdated>,
        IHandleMessages<UserPermissionAdded>,
        IHandleMessages<UserPermissionRevoked>,
        IHandleMessages<UserPromotedToAdmin>,
        IHandleMessages<AdminDemotedToUser>,
        IHandleMessages<UserLocked>,
        IHandleMessages<UserUnlocked>,
        IHandleMessages<UserProductRemoved>
    {
        readonly IDocumentSession _session;
        readonly ICryptographyService _cryptographyService;

        public UserHandler(IDocumentSession session, ICryptographyService cryptographyService)
        {
            _session = session;
            _cryptographyService = cryptographyService;
        }

        public void Handle(UserCreated message)
        {
            var existing = _session.Advanced.DocumentStore.DatabaseCommands.Head("users/" + message.UserId);
            if(existing!=null)
                throw new Exception("User already exists!");

            _session.Store(new User
            {
                Id = message.UserId,
                Email = message.Email,
                PasswordHash = _cryptographyService.GetMD5Hash(message.Password),
                FirstName = message.FirstName,
                LastName = message.LastName,
                IsVerified = false
                
            });
        }
        public void Handle(UserAssignedToAccount message)
        {
            var account = _session.Load<Account>(message.AccountId);
            var user = account.Users.Single(x=>x.UserId==message.UserId);
            //user.RoleName = message.RoleName;

            //if the role doesn't exists, create it
            if (user.Roles.Any(x => x.Name == message.Role)) return;
            user.Roles.Add(new UserRole { Name = message.Role });
        }
        public void Handle(UserAssignmentToAccountRemoved message)
        {
            var account = _session.Load<Account>(message.AccountId);
            var user = account.Users.Single(x => x.UserId == message.UserId);
            user.Roles.Clear();
        }
        public void Handle(PasswordReseted message)
        {
            var user = _session.Load<User>(message.UserId);
            user.PasswordHash = _cryptographyService.GetMD5Hash(message.Password);
        }
        public void Handle(PasswordChanged message)
        {
            var user = _session.Load<User>(message.UserId);
            user.PasswordHash = _cryptographyService.GetMD5Hash(message.NewPassword);
        }
        public void Handle(UserVerified message)
        {
            var user = _session.Load<User>(message.UserId);
            user.IsVerified = true;
        }
        public void Handle(UserDetailUpdated message)
        {
            var user = _session.Load<User>(message.UserId);
            user.FirstName = message.FirstName;
            user.LastName = message.LastName;
            user.Phone = message.Phone;
            user.Mobile = message.Mobile;
            user.Email = message.Email;

            //get all accounts this user is linked to and update the name
            var accountIs = _session.Query<AccountUserIndex.Result, AccountUserIndex>()
                .Where(x => x.UserId == message.UserId)
                .ToList();

            var accounts = _session.Load<Account>(accountIs.Select(x => x.AccountId));
            var users = accounts.SelectMany(x => x.Users).Where(x => x.UserId == message.UserId).ToList();
            users.ForEach(x => x.FullName = user.FullName);
        }
        public void Handle(UserPermissionAdded message)
        {
            var account = _session.Load<Account>(message.AccountId);
            var user = account.Users.Single(x => x.UserId == message.UserId);

            UserRole role;
            if (user.Roles.Any(x => x.Name != message.Role))
            {
                role = new UserRole { Name = message.Role };
                user.Roles.Add(role);
            }
            else
                role = user.Roles.Single(x => x.Name == message.Role);

            List<string> permissions;
            if (!role.Permissions.ContainsKey(message.ProductId))
            {
                permissions = new List<string>();
                role.Permissions.Add(message.ProductId, permissions);
            }
            else
            {
                permissions = role.Permissions[message.ProductId];
            }

            if (permissions.Any(x => x != message.PermissionCode))
                permissions.Add(message.PermissionCode);

        }
        public void Handle(UserPermissionRevoked message)
        {
            var account = _session.Load<Account>(message.AccountId);
            var user = account.Users.Single(x => x.UserId == message.UserId);

            UserRole role;
            if (user.Roles.Any(x => x.Name != message.Role))
            {
                role = new UserRole { Name = message.Role };
                user.Roles.Add(role);
            }
            else
                role = user.Roles.Single(x => x.Name == message.Role);

            List<string> permissions;
            if (!role.Permissions.ContainsKey(message.ProductId))
            {
                permissions = new List<string>();
                role.Permissions.Add(message.ProductId, permissions);
            }
            else
            {
                permissions = role.Permissions[message.ProductId];
            }

            if (permissions.Any(x => x == message.PermissionCode))
                permissions.RemoveAll(x => x == message.PermissionCode);
        }
        public void Handle(UserPromotedToAdmin message)
        {
            var account = _session.Load<Account>(message.AccountId);
            var user = account.Users.Single(x => x.UserId == message.UserId);
            user.IsAdmin = true;
        }
        public void Handle(AdminDemotedToUser message)
        {
            var account = _session.Load<Account>(message.AccountId);
            var user = account.Users.Single(x => x.UserId == message.UserId);
            user.IsAdmin = false;
        }
        public void Handle(UserLocked message)
        {
            var user = _session.Load<User>(message.UserId);
            user.IsLocked = true;
            user.ActivityLog.Add(string.Format("User was Locked: {0}", message.Reason));
        }
        public void Handle(UserUnlocked message)
        {
            var user = _session.Load<User>(message.UserId);
            user.IsLocked = false;
            user.ActivityLog.Add(string.Format("User was Unlocked: {0}", message.Reason));
        }
        public void Handle(UserProductRemoved message)
        {
            var account = _session.Load<Account>(message.AccountId);
            var user = account.Users.Single(x => x.UserId == message.UserId);
            foreach (var userRole in user.Roles)
            {
                userRole.Permissions.Remove(message.ProductId);
            }
        }

        
    }
}
