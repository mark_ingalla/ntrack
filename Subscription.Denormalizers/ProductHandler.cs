﻿using System;
using System.Collections.Generic;
using System.Linq;
using NServiceBus;
using Raven.Client;
using StructureMap.Attributes;
using Subscription.Data;
using Subscription.Messages.Events.Product;

namespace Subscription.Denormalizers
{
    public class ProductHandler :
        IHandleMessages<ProductCreated>,
        IHandleMessages<ProductUpdated>,
        IHandleMessages<ProductActivated>,
        IHandleMessages<ProductDiscontinued>,

        //Features
        IHandleMessages<FeatureAdded>,
        IHandleMessages<FeatureUpdated>,
        IHandleMessages<FeatureRemoved>,
        IHandleMessages<FeatureActivated>,
        IHandleMessages<FeatureDeactivated>,

        //Offers
        IHandleMessages<OfferAdded>,
        IHandleMessages<OfferUpdated>,
        IHandleMessages<OfferRemoved>,
        IHandleMessages<OfferLocked>,
        IHandleMessages<OfferActivated>,
        IHandleMessages<OfferDeactivated>,
        IHandleMessages<OfferFeaturesAdded_V2>,
        IHandleMessages<OfferFeaturesRemoved>,
        IHandleMessages<OfferFeaturesUpdated>
    {
        [SetterProperty]
        public IDocumentSession Session { get; set; }


        public void Handle(ProductCreated message)
        {
            var existing = Session.Advanced.DocumentStore.DatabaseCommands.Head("products/" + message.ProductId);
            if (existing != null)
                throw new Exception("Product already exists!");

            Session.Store(new Product
                {
                    Id = message.ProductId,
                    Name = message.Name,
                    Code = message.Code,
                    Description = message.Description,
                    BasePrice = message.BasePrice,
                    AccessUrl = message.AccessUrl
                });
        }
        public void Handle(ProductUpdated message)
        {
            var product = Session.Load<Product>(message.ProductId);
            product.Name = message.Name;
            product.Code = message.Code;
            product.Description = message.Description;
            product.BasePrice = message.BasePrice;
            product.AccessUrl = message.AccessUrl;
        }
        public void Handle(ProductActivated message)
        {
            var product = Session.Load<Product>(message.ProductId);
            product.IsActive = true;
        }
        public void Handle(ProductDiscontinued message)
        {
            var product = Session.Load<Product>(message.ProductId);
            product.IsActive = false;
            product.DiscontinueReason = message.Reason;
        }

        //Features
        public void Handle(FeatureAdded message)
        {
            var product = Session.Load<Product>(message.ProductId);
            product.Features.Add(new ProductFeature(message.Code, message.Name, message.Price, message.Limit));
        }
        public void Handle(FeatureUpdated message)
        {
            var product = Session.Load<Product>(message.ProductId);
            var feature = product.Features.Single(x => x.Code == message.Code);
            feature.Name = message.Name;
            feature.Price = message.Price;
            feature.Limit = message.Limit;
        }
        public void Handle(FeatureRemoved message)
        {
            var product = Session.Load<Product>(message.ProductId);
            product.Features.RemoveAll(x => x.Code == message.FeatureCode);
        }
        public void Handle(FeatureActivated message)
        {
            var product = Session.Load<Product>(message.ProductId);
            product.Features.Single(x => x.Code == message.FeatureCode).IsActive = true;
        }
        public void Handle(FeatureDeactivated message)
        {
            var product = Session.Load<Product>(message.ProductId);
            product.Features.Single(x => x.Code == message.FeatureCode).IsActive = false;
        }

        //Offers
        public void Handle(OfferAdded message)
        {
            var product = Session.Load<Product>(message.ProductId);
            product.Offers.Add(new ProductOffer(message.OfferKey, message.BasePrice, message.Name));
        }
        public void Handle(OfferUpdated message)
        {
            var product = Session.Load<Product>(message.ProductId);
            product.Offers.Single(x => x.Key == message.OfferKey).Name = message.OfferName;
        }
        public void Handle(OfferRemoved message)
        {
            var product = Session.Load<Product>(message.ProductId);
            product.Offers.RemoveAll(x => x.Key == message.OfferKey);
        }
        public void Handle(OfferLocked message)
        {
            var product = Session.Load<Product>(message.ProductId);
            product.Offers.Single(x => x.Key == message.OfferKey).IsLocked = true;
        }
        public void Handle(OfferActivated message)
        {
            var product = Session.Load<Product>(message.ProductId);
            product.Offers.Single(x => x.Key == message.OfferKey).IsActive = true;
        }
        public void Handle(OfferDeactivated message)
        {
            var product = Session.Load<Product>(message.ProductId);
            product.Offers.Single(x => x.Key == message.OfferKey).IsActive = false;
        }
        public void Handle(OfferFeaturesAdded_V2 message)
        {
            var product = Session.Load<Product>(message.ProductId);
            var offer = product.Offers.Single(x => x.Key == message.OfferKey);

        
            var features = from p in product.Features
                           join q in message.Features on p.Code equals q.Code
                               into joinTbl
                           from q in joinTbl.DefaultIfEmpty()
                           where message.Features.Any(x => x.Code == p.Code)
                           select new ProductFeatureSummary
                                      {
                                          Code = p.Code,
                                          Limit = q != null ? q.Limit : 0,
                                          Name = p.Name,
                                          Price = p.Price,
                                          IsIncluded = true
                                      };

            offer.Features.Clear();
            offer.Features.AddRange(features.Select(x => new ProductFeatureSummary { Code = x.Code, Name = x.Name, Price = x.Price, Limit = x.Limit }));

        }
        public void Handle(OfferFeaturesRemoved message)
        {
            var product = Session.Load<Product>(message.ProductId);
            var offer = product.Offers.Single(x => x.Key == message.OfferKey);

            
            foreach (var code in message.FeatureCodes)
            {
            
                offer.Features.RemoveAll(x => x.Code == code);
            }
        }


        public void Handle(OfferFeaturesUpdated message)
        {
            var product = Session.Load<Product>(message.ProductId);
            var offer = product.Offers.Single(x => x.Key == message.OfferKey);
       //     offer.Features = new List<ProductFeatureSummary>();

            if (offer.Features == null)
                offer.Features = new List<ProductFeatureSummary>();

            foreach (var offerFeature in offer.Features)
            {
                offerFeature.IsIncluded = false;
            }

            foreach (var feature in message.Features)
            {
                var productFeature = product.Features.Single(p => p.Code == feature.Code);

                if (productFeature != null)
                {
                    var offerFeature = offer.Features.SingleOrDefault(p => p.Code == productFeature.Code);

                    if (offerFeature != null)
                    {
                        offerFeature.IsIncluded = true;
                    }
                    else
                    {
                        offer.Features.Add(new ProductFeatureSummary() { Code = feature.Code, Limit = feature.Limit, Name = productFeature.Name, Price = productFeature.Price, IsIncluded = true});
                    }

                }
            }
        }
    }
}
