﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Subscription.Data;
using Subscription.Messages.Events.Account;

namespace Subscription.Denormalizers.Profiles
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            Mapper.CreateMap<AccountUpdated, Account>();
        }
    }
}
