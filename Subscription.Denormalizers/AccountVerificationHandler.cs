﻿using System;
using NServiceBus;
using Raven.Client;
using StructureMap.Attributes;
using Subscription.Data;
using Subscription.Messages.Events;

namespace Subscription.Denormalizers
{
    public class AccountVerificationHandler : IHandleMessages<UserVerificationStatus>
    {
        [SetterProperty]
        public IDocumentSession Session { get; set; }

        public void Handle(UserVerificationStatus message)
        {
            var account = Session.Load<Account>(message.AccountId);

            switch (message.Status)
            {
                case "Pending":
                    account.PendingVerificationToken = message.VerificationToken;
                    break;

                case "Completed":
                case "TimedOut":
                case "BouncedBack":
                    account.PendingVerificationToken = Guid.Empty;
                    break;
            }
        }
    }
}