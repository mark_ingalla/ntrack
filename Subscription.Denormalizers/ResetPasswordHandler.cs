﻿using System.Linq;
using NServiceBus;
using Raven.Client;
using Subscription.Data;
using Subscription.Messages.Events;

namespace Subscription.Denormalizers
{
    public class ResetPasswordHandler : IHandleMessages<UserPasswordResetStatus>
    {
        private readonly IDocumentSession _session;

        public ResetPasswordHandler(IDocumentSession session)
        {
            _session = session;
        }

        public void Handle(UserPasswordResetStatus message)
        {
            var account = _session.Load<Account>(message.AccountId);
            var user = account.Users.Single(x => x.UserKey == message.UserKey);

            switch (message.Status)
            {
                case "Pending":
                    if (user.PendingResetPasswordTokens.All(x => x != message.ResetToken))
                        user.PendingResetPasswordTokens.Add(message.ResetToken);
                    break;
                case "TimedOut":
                case "Completed":
                    user.PendingResetPasswordTokens.RemoveAll(x => x == message.ResetToken);
                    break;
            }
        }
    }
}