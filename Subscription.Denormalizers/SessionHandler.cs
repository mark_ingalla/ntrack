﻿using NServiceBus;
using nTrack.Data;
using Raven.Client;
using Subscription.Messages.Events;
using Account = Subscription.Data.Account;

namespace Subscription.Denormalizers
{
    public class SessionHandler :
        IHandleMessages<LoggedIn>,
        IHandleMessages<LoggedOut>
    {
        private readonly IDocumentSession _session;

        public SessionHandler(IDocumentSession session)
        {
            _session = session;
        }

        public void Handle(LoggedIn message)
        {
            // If the user was already logged in, then remove his old session.
            var account = _session.Load<Account>(message.AccountId);

            // Add the new session.
            account.ActiveSessions.Add(new UserSession
                                        {
                                            SessionToken = message.SessionToken,
                                            StartedOnUTC = message.LoggedInOnUTC,
                                            UserKey = message.UserKey
                                        });
        }
        public void Handle(LoggedOut message)
        {
            var account = _session.Load<Account>(message.AccountId);
            account.ActiveSessions.RemoveAll(x => x.SessionToken == message.SessionToken);
        }
    }
}