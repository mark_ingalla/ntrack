﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Linq;
using System.Xml.XPath;
using Dapper;
using NServiceBus;
using nTrack.Data;
using PalletPlus.Data;
using Raven.Client;
using Raven.Client.Document;
using Subscription.Messages.Commands.Product;

namespace Migration
{
    class Program
    {
        IBus _bus;
        IDocumentStore _store;
        readonly Guid _productId = Guid.Parse("4a826164-19a2-417b-9e3a-aa301b03421b");
        Guid _userKey;
        Guid _accountId;

        static void Main(string[] args)
        {
            var p = new Program();
            p.Start();
        }

        void Start()
        {
            _bus = Configure.With()
                            .DefineEndpointName("ntrack.debugger")
                            .DefaultBuilder()
                            .DefiningCommandsAs(
                                type =>
                                type != null && type.Namespace != null && type.Namespace.Contains("Messages.Commands"))
                            .DefiningEventsAs(
                                type =>
                                type != null && type.Namespace != null && type.Namespace.Contains("Messages.Events"))
                            .XmlSerializer()
                            .MsmqTransport()
                            .UnicastBus()
                            .SendOnly();

            _store = new DocumentStore { ConnectionStringName = "ntrack" };
            _store.Initialize();

            //InitilizeCountries();
            SeedProducts();

            // EcoFramsMigration();
        }

        void EcoFramsMigration()
        {
            _accountId = Guid.Parse("7e8c2246-225e-4c53-8731-bdb09475fee7");
            _userKey = Guid.Parse("9a0339c8-f665-4ffc-8226-e27378357414");

            var cnn = new SqlConnection("Server=202.182.159.37, 1078;Database=esm_ecofarms_v2;User Id=sa;Password=SarmaadAmin2;");
            cnn.Open();

            Subscription.Data.Account account;
            using (var session = _store.OpenSession("ntrack"))
            {
                account = session.Load<Subscription.Data.Account>(_accountId);
            }

            var clientDb = account.Databases["PP"].Name;

            MigrateSuppliers(cnn, clientDb);
            Thread.Sleep(200);
            MigrateSupplierEquipments(cnn, clientDb);
            MigrateSupplierLocations(cnn, clientDb);


            if (MigratePartners(cnn, clientDb))
            {
                Thread.Sleep(200);
                MigratePartnerEquipments(cnn, clientDb);
            }

            if (MigrateSites(cnn, clientDb))
            {
                Thread.Sleep(200);
                MigrateSitePartners(cnn, clientDb);
                MigrateSiteSuppliers(cnn, clientDb);
            }
            MigratePartnerMovements(cnn, clientDb);
            MigrateSupplierMovements(cnn, clientDb);

            cnn.Dispose();
        }
        void MigrateSuppliers(SqlConnection cnn, string clientDb)
        {
            using (var session = _store.OpenSession(clientDb))
            {
                if (session.Load<MigrationResult>("Migration/Suppliers") != null) return;

                var supplierResult = new MigrationResult { Id = "Migration/Suppliers" };
                foreach (var supplier in cnn.Query("SELECT * FROM esm_Supplier").Take(1).ToList())
                {
                    var cmd = new PalletPlus.Messages.Commands.Supplier.CreateSupplier
                        {
                            SupplierId = Guid.NewGuid(),
                            Name = supplier.SUPPLIER_NAME.ToString().Split(' ')[0],
                            //AddressLine1 = supplier.ADDRESS_1,
                            //AddressLine2 = supplier.ADDRESS_2,
                            //Suburb = supplier.ADDRESS_SUBURB,
                            //Postcode = supplier.ADDRESS_POSTCODE,
                            //State = supplier.ADDRESS_STATE,
                            //Country = "Australia",
                            //LocationName = supplier.SUPPLIER_NAME,
                            EmailAddress = supplier.CONTACT_EMAIL,
                            //ContactName = supplier.CONTACT_NAME,

                        };

                    SetHeaers(cmd);
                    _bus.Send(cmd);

                    supplierResult.IdMap.Add(supplier.SUPPLIER_ID, cmd.SupplierId.ToString());
                }

                session.Store(supplierResult);
                session.SaveChanges();
            }
        }
        void MigrateSupplierEquipments(SqlConnection cnn, string clientDb)
        {
            using (var session = _store.OpenSession(clientDb))
            {
                if (session.Load<MigrationResult>("Migration/Suppliers/Equipments") != null) return;

                var supplierResult = session.Load<MigrationResult>("Migration/Suppliers");
                var equipmentResult = new MigrationResult { Id = "Migration/Suppliers/Equipments" };
                foreach (var equipment in cnn.Query("SELECT * FROM esm_Equipment").ToList())
                {
                    var supplierId = supplierResult.IdMap.ContainsKey(equipment.SUPPLIER_ID)
                                         ? supplierResult.IdMap[equipment.SUPPLIER_ID]
                                         : null;

                    if (supplierId == null) continue;

                    Supplier supplierEntity = session.Load<Supplier>(Guid.Parse(supplierId));

                    if (supplierEntity.Equipments.ContainsKey(equipment.EQUIPMENT_CODE))
                    {
                        continue;
                    }

                    var cmd = new PalletPlus.Messages.Commands.Supplier.AddEquipmentToSupplier
                        {
                            SupplierId = Guid.Parse(supplierId),
                            EquipmentCode = equipment.EQUIPMENT_CODE,
                            EquipmentName = equipment.EQUIPMENT_NAME
                        };
                    SetHeaers(cmd);
                    _bus.Send(cmd);

                    equipmentResult.IdMap.Add(equipment.EQUIPMENT_ID, cmd.EquipmentCode);
                }

                session.Store(equipmentResult);
                session.SaveChanges();

            }
        }
        void MigrateSupplierLocations(SqlConnection cnn, string clientDb)
        {
            using (var session = _store.OpenSession(clientDb))
            {
                if (session.Load<MigrationResult>("Migration/Suppliers/Locations") != null) return;

                var locationResult = new MigrationResult { Id = "Migration/Suppliers/Locations" };
                var supplierResult = session.Load<MigrationResult>("Migration/Suppliers");
                var supplierId = supplierResult.IdMap.First().Value;
                var chepSupplier = session.Load<Supplier>(Guid.Parse(supplierId));

                foreach (var location in cnn.Query("SELECT esm_Partner.*,esm_PartnerSuppliers.SUPPLIER_ACCOUNT FROM esm_Partner INNER JOIN esm_PartnerSuppliers on esm_PartnerSuppliers.PARTNER_ID = esm_Partner.PARTNER_ID WHERE esm_PartnerSuppliers.IS_EQUIPMENT_CENTER='true'").ToList())
                {
                    if (chepSupplier.Locations.Any(x => x.Value.AccountNumber == location.SUPPLIER_ACCOUNT))
                        continue;

                    var cmd = new PalletPlus.Messages.Commands.Supplier.AddLocationToSupplier
                        {
                            SupplierId = chepSupplier.Id,
                            LocationKey = Guid.NewGuid(),
                            Name = location.PARTNER_NAME,
                            AddressLine1 = location.ADDRESS_1,
                            AddressLine2 = location.ADDRESS_2,
                            Suburb = location.ADDRESS_SUBURB,
                            Postcode = location.ADDRESS_POSTCODE,
                            State = location.ADDRESS_STATE,
                            Country = "Australia",
                            ContactEmail = location.CONTACT_EMAIL,
                            ContactName = location.CONTACT_NAME,
                            AccountNumber = location.SUPPLIER_ACCOUNT
                        };
                    SetHeaers(cmd);
                    _bus.Send(cmd);

                    if (!locationResult.IdMap.ContainsKey(location.PARTNER_ID))
                        locationResult.IdMap.Add(location.PARTNER_ID, cmd.LocationKey.ToString());
                    else
                    {
                        locationResult.IdMap[location.PARTNER_ID] = cmd.LocationKey.ToString();
                    }
                }

                session.Store(locationResult);
                session.SaveChanges();
            }
        }

        bool MigratePartners(SqlConnection cnn, string clientDb)
        {
            using (var session = _store.OpenSession(clientDb))
            {
                if (session.Load<MigrationResult>("Migration/Partners") != null) return false;

                var partnerResult = new MigrationResult { Id = "Migration/Partners" };
                var supplierResult = session.Load<MigrationResult>("Migration/Suppliers");
                var supplierId = supplierResult.IdMap.First().Value;

                foreach (var partner in cnn.Query("SELECT esm_Partner.*,esm_PartnerSuppliers.SUPPLIER_ACCOUNT FROM esm_Partner INNER JOIN esm_PartnerSuppliers on esm_PartnerSuppliers.PARTNER_ID = esm_Partner.PARTNER_ID WHERE esm_PartnerSuppliers.IS_EQUIPMENT_CENTER='false' and esm_PartnerSuppliers.SUPPLIER_ID=1 and esm_Partner.PARTNER_NAME not like 'ECO%' and (len(esm_PartnerSuppliers.SUPPLIER_ACCOUNT) > 5 or len(esm_PartnerSuppliers.SUPPLIER_ACCOUNT)<=0)").ToList())
                {
                    if (partnerResult.IdMap.ContainsKey(partner.PARTNER_ID)) continue;

                    var partnerId = Guid.NewGuid();
                    var cmds = new List<object>
                                       {
                                           new PalletPlus.Messages.Commands.Partner.CreatePartner
                                               {
                                                   PartnerId = partnerId,
                                                   Name = partner.PARTNER_NAME,
                                                   AddressLine1 = partner.ADDRESS_1,
                                                   AddressLine2 = partner.ADDRESS_2,
                                                   Suburb = partner.ADDRESS_SUBURB,
                                                   Postcode = partner.ADDRESS_POSTCODE,
                                                   State = partner.ADDRESS_STATE,
                                                   Country = "Australia",
                                                   ContactEmail = partner.CONTACT_EMAIL,
                                                   ContactName = partner.CONTACT_NAME
                                               },
                                           new PalletPlus.Messages.Commands.Partner.AddSupplierToPartner
                                               {
                                                   SupplierId = Guid.Parse(supplierId),
                                                   PartnerId = partnerId,
                                                   AccountNumber = partner.SUPPLIER_ACCOUNT,
                                               }
                                       };

                    cmds.ForEach(cmd =>
                    {
                        SetHeaers(cmd);
                        _bus.Send(cmd);
                    });

                    partnerResult.IdMap.Add(partner.PARTNER_ID, partnerId.ToString());
                }

                session.Store(partnerResult);
                session.SaveChanges();
            }

            return true;
        }
        void MigratePartnerEquipments(SqlConnection cnn, string clientDb)
        {
            using (var session = _store.OpenSession(clientDb))
            {
                var supplierResult = session.Load<MigrationResult>("Migration/Suppliers");
                var supplierId = supplierResult.IdMap.First().Value;

                var sEquipments = session.Load<MigrationResult>("Migration/Suppliers/Equipments");
                var partners = session.Load<MigrationResult>("Migration/Partners");
                var equipments = cnn.Query("SELECT * FROM esm_PartnerEquipmentTT").ToList();
                foreach (var equipment in equipments.GroupBy(x => x.PARTNER_ID).ToList())
                {
                    if (!partners.IdMap.ContainsKey(equipment.Key))
                        continue;

                    var partnerId = partners.IdMap[equipment.Key];

                    var cmd = new PalletPlus.Messages.Commands.Partner.AllowEquipmentToPartner
                        {
                            PartnerId = Guid.Parse(partnerId),
                            SupplierId = Guid.Parse(supplierId),
                            EquipmentCodes = equipment.Select(x => (string)sEquipments.IdMap[x.EQUIPMENT_ID]).ToArray()
                        };

                    SetHeaers(cmd);
                    _bus.Send(cmd);
                }
            }
        }


        bool MigrateSites(SqlConnection cnn, string clientDatabase)
        {
            using (var session = _store.OpenSession(clientDatabase))
            {
                if (session.Load<MigrationResult>("Migration/Sites") != null) return false;

                var migrationResult = new MigrationResult { Id = "Migration/Sites" };
                foreach (var site in cnn.Query("SELECT * FROM esm_Site").ToList())
                {
                    var cmd = new PalletPlus.Messages.Commands.Site.CreateSite
                    {
                        SiteId = Guid.NewGuid(),
                        Name = site.SITE_NAME,
                        AddressLine1 = site.ADDRESS_1,
                        AddressLine2 = site.ADDRESS_2,
                        Suburb = site.ADDRESS_SUBURB,
                        Postcode = site.ADDRESS_POSTCODE,
                        State = site.ADDRESS_STATE,
                        Country = "Australia"
                    };

                    SetHeaers(cmd);
                    _bus.Send(cmd);

                    migrationResult.IdMap.Add(site.SITE_ID, cmd.SiteId.ToString());
                }

                session.Store(migrationResult);
                session.SaveChanges();
            }

            return true;
        }
        void MigrateSiteSuppliers(SqlConnection cnn, string clientDb)
        {
            using (var session = _store.OpenSession(clientDb))
            {
                var siteResults = session.Load<MigrationResult>("Migration/Sites");
                var supplierResults = session.Load<MigrationResult>("Migration/Suppliers");

                foreach (var sp in cnn.Query("SELECT * FROM esm_SiteSuppliers").ToList())
                {
                    var siteId = siteResults.IdMap.ContainsKey(sp.SITE_ID) ? siteResults.IdMap[sp.SITE_ID] : string.Empty;
                    var supplierId = supplierResults.IdMap.ContainsKey(sp.SUPPLIER_ID)
                                        ? supplierResults.IdMap[sp.SUPPLIER_ID]
                                        : string.Empty;

                    if (string.IsNullOrEmpty(siteId) || string.IsNullOrEmpty(supplierId)) continue;

                    Supplier supplier = session.Load<Supplier>(Guid.Parse(supplierId));

                    var cmd = new PalletPlus.Messages.Commands.Site.AddSupplierToSite
                    {
                        SiteId = Guid.Parse(siteId),
                        SupplierId = supplier.Id,
                        SupplierName = supplier.CompanyName,
                        SupplierAccountNumber = sp.SUPPLIER_ACCOUNT,
                        Prefix = sp.DOCKET_PREFIX,
                        Suffix = sp.DOCKET_SUFFIX,
                        SequenceNumber = sp.DOCKET_NUMBER
                    };

                    SetHeaers(cmd);
                    _bus.Send(cmd);
                }
            }

        }
        void MigrateSitePartners(SqlConnection cnn, string clientDb)
        {
            using (var session = _store.OpenSession(clientDb))
            {
                var siteResults = session.Load<MigrationResult>("Migration/Sites");
                var partnerResults = session.Load<MigrationResult>("Migration/Partners");

                foreach (var sp in cnn.Query("SELECT * FROM esm_SitePartners").ToList())
                {
                    var siteId = siteResults.IdMap.ContainsKey(sp.SITE_ID) ? siteResults.IdMap[sp.SITE_ID] : string.Empty;
                    var partnerId = partnerResults.IdMap.ContainsKey(sp.PARTNER_ID)
                                        ? partnerResults.IdMap[sp.PARTNER_ID]
                                        : string.Empty;

                    if (string.IsNullOrEmpty(siteId) || string.IsNullOrEmpty(partnerId)) continue;

                    var cmd = new PalletPlus.Messages.Commands.Site.AddPartnerToSite
                    {
                        PartnerId = Guid.Parse(partnerId),
                        SiteId = Guid.Parse(siteId)
                    };

                    SetHeaers(cmd);
                    _bus.Send(cmd);
                }
            }
        }

        void MigratePartnerMovements(SqlConnection cnn, string clientDb)
        {
            using (var session = _store.OpenSession(clientDb))
            {
                if (session.Load<MigrationResult>("Migration/Movements/Partners") != null) return;

                var movementResult = new MigrationResult { Id = "Migration/Movements/Partners" };

                var sites = session.Load<MigrationResult>("Migration/Sites");
                var partners = session.Load<MigrationResult>("Migration/Partners");
                var supplierResult = session.Load<MigrationResult>("Migration/Suppliers");
                var supplierId = supplierResult.IdMap.First().Value;


                var transferMovements =
                    cnn.Query(
                        "SELECT esm_Movement.*, esm_Equipment.EQUIPMENT_CODE FROM esm_Movement INNER JOIN esm_Equipment on esm_Equipment.EQUIPMENT_ID = esm_Movement.EQUIPMENT_ID WHERE MOVEMENT_TYPE='TRANSFER' and IS_DELETED='false' and DATE_CREATE> '2012-06-30 23:23:59' ORDER BY DATE_CREATE")
                       .ToList();

                var transferReduce = (from m in transferMovements
                                      group m by ((string)m.MOVEMENT_DOCKET).ToLower()
                                          into g
                                          select new
                                          {
                                              MOVEMENT_DOCKET = g.Key,
                                              SITE_ID = g.Select(x => x.SITE_ID).FirstOrDefault(),
                                              PARTNER_ID = g.Select(x => x.PARTNER_ID).FirstOrDefault(),
                                              MOVEMENT_DIRECTION = g.Select(x => x.MOVEMENT_DIRECTION).FirstOrDefault(),
                                              MOVEMENT_DATE = g.Select(x => x.MOVEMENT_DATE).FirstOrDefault(),
                                              EFFECTIVE_DATE = g.Select(x => x.EFFECTIVE_DATE).FirstOrDefault(),
                                              MOVEMENT_REFERENCE = g.Select(x => x.MOVEMENT_REFERENCE).FirstOrDefault(),
                                              PARTNER_REFERENCE = g.Select(x => x.PARTNER_REFERENCE).FirstOrDefault(),
                                              EQUIPMENTS = g.Select(x => new EquipmentItem
                                              {
                                                  MOVEMENT_ID = x.MOVEMENT_ID,
                                                  EQUIPMENT_CODE = x.EQUIPMENT_CODE,
                                                  QUANTITY = x.QUANTITY
                                              }).ToList(),

                                              DATE_CREATE = g.Select(x => x.DATE_CREATE).FirstOrDefault(),
                                              TRANSPORTER_NOTE = g.Select(x => x.TRANSPORTER_NOTE).FirstOrDefault()

                                          }).ToList();



                foreach (var movement in transferReduce)
                {
                    var siteId = sites.IdMap.ContainsKey(movement.SITE_ID)
                                     ? sites.IdMap[movement.SITE_ID]
                                     : string.Empty;

                    var partnerId = partners.IdMap.ContainsKey(movement.PARTNER_ID)
                                        ? partners.IdMap[movement.PARTNER_ID]
                                        : string.Empty;

                    if (string.IsNullOrEmpty(siteId) || string.IsNullOrEmpty(partnerId)) continue;

                    string direction = string.Empty;
                    if (movement.MOVEMENT_DIRECTION == "OUT") direction = "Out";
                    if (movement.MOVEMENT_DIRECTION == "IN") direction = "In";


                    var cmd = new PalletPlus.Messages.Commands.Movement.CreateMovement
                    {
                        MovementId = Guid.NewGuid(),
                        SiteId = Guid.Parse(siteId),
                        DestinationId = Guid.Parse(partnerId),
                        SupplierId = Guid.Parse(supplierId),
                        Direction = direction,
                        MovementDate = movement.DATE_CREATE,
                        EffectiveDate = movement.EFFECTIVE_DATE,
                        Equipments = GetEquipments(movement.EQUIPMENTS),
                        DocketNumber = movement.MOVEMENT_DOCKET,
                        MovementReference = movement.MOVEMENT_REFERENCE,
                        DestinationReference = movement.PARTNER_REFERENCE,
                        ConsignmentNote = movement.TRANSPORTER_NOTE
                    };

                    SetHeaers(cmd);
                    _bus.Send(cmd);

                    foreach (var mId in movement.EQUIPMENTS.Where(mId => !movementResult.IdMap.ContainsKey(mId.MOVEMENT_ID)))
                    {
                        movementResult.IdMap.Add(mId.MOVEMENT_ID, cmd.MovementId.ToString());
                    }

                }

                session.Store(movementResult);
                session.SaveChanges();
            }
        }
        void MigrateSupplierMovements(SqlConnection cnn, string clientDb)
        {
            using (var session = _store.OpenSession(clientDb))
            {
                if (session.Load<MigrationResult>("Migration/Movements/Suppliers") != null) return;

                var movementResult = new MigrationResult { Id = "Migration/Movements/Suppliers" };

                var sites = session.Load<MigrationResult>("Migration/Sites");
                var supplierLocation = session.Load<MigrationResult>("Migration/Suppliers/Locations");
                var supplierResult = session.Load<MigrationResult>("Migration/Suppliers");
                var supplierId = supplierResult.IdMap.First().Value;


                var transferMovements =
                    cnn.Query(
                        "SELECT esm_Movement.*, esm_Equipment.EQUIPMENT_CODE FROM esm_Movement INNER JOIN esm_Equipment on esm_Equipment.EQUIPMENT_ID = esm_Movement.EQUIPMENT_ID WHERE (MOVEMENT_TYPE='ISSUE' OR MOVEMENT_TYPE='RETURN') and IS_DELETED='false' and DATE_CREATE> '2012-06-30 23:23:59' ORDER BY DATE_CREATE")
                       .ToList();

                var transferReduce = (from m in transferMovements
                                      group m by ((string)m.MOVEMENT_DOCKET).ToLower()
                                          into g
                                          select new
                                          {
                                              MOVEMENT_DOCKET = g.Key,
                                              SITE_ID = g.Select(x => x.SITE_ID).FirstOrDefault(),
                                              PARTNER_ID = g.Select(x => x.PARTNER_ID).FirstOrDefault(),
                                              MOVEMENT_DIRECTION = g.Select(x => x.MOVEMENT_DIRECTION).FirstOrDefault(),
                                              MOVEMENT_DATE = g.Select(x => x.MOVEMENT_DATE).FirstOrDefault(),
                                              EFFECTIVE_DATE = g.Select(x => x.EFFECTIVE_DATE).FirstOrDefault(),
                                              MOVEMENT_REFERENCE = g.Select(x => x.MOVEMENT_REFERENCE).FirstOrDefault(),
                                              PARTNER_REFERENCE = g.Select(x => x.PARTNER_REFERENCE).FirstOrDefault(),
                                              EQUIPMENTS = g.Select(x => new EquipmentItem
                                              {
                                                  MOVEMENT_ID = x.MOVEMENT_ID,
                                                  EQUIPMENT_CODE = x.EQUIPMENT_CODE,
                                                  QUANTITY = x.QUANTITY
                                              }).ToList(),

                                              DATE_CREATE = g.Select(x => x.DATE_CREATE).FirstOrDefault(),
                                              TRANSPORTER_NOTE = g.Select(x => x.TRANSPORTER_NOTE).FirstOrDefault()

                                          }).ToList();



                foreach (var movement in transferReduce)
                {
                    var siteId = sites.IdMap.ContainsKey(movement.SITE_ID)
                                     ? sites.IdMap[movement.SITE_ID]
                                     : string.Empty;

                    var partnerId = supplierLocation.IdMap.ContainsKey(movement.PARTNER_ID)
                                        ? supplierLocation.IdMap[movement.PARTNER_ID]
                                        : string.Empty;

                    if (string.IsNullOrEmpty(siteId) || string.IsNullOrEmpty(partnerId)) continue;

                    string direction = string.Empty;
                    if (movement.MOVEMENT_DIRECTION == "OUT") direction = "Out";
                    if (movement.MOVEMENT_DIRECTION == "IN") direction = "In";


                    var cmd = new PalletPlus.Messages.Commands.Movement.CreateMovement
                    {
                        MovementId = Guid.NewGuid(),
                        SiteId = Guid.Parse(siteId),
                        DestinationId = Guid.Parse(supplierId),
                        SupplierId = Guid.Parse(supplierId),
                        LocationId = Guid.Parse(partnerId),
                        Direction = direction,
                        MovementDate = movement.DATE_CREATE,
                        EffectiveDate = movement.EFFECTIVE_DATE,
                        Equipments = GetEquipments(movement.EQUIPMENTS),
                        DocketNumber = movement.MOVEMENT_DOCKET,
                        MovementReference = movement.MOVEMENT_REFERENCE,
                        DestinationReference = movement.PARTNER_REFERENCE,
                        ConsignmentNote = movement.TRANSPORTER_NOTE
                    };

                    SetHeaers(cmd);
                    _bus.Send(cmd);

                    foreach (var mId in movement.EQUIPMENTS.Where(mId => !movementResult.IdMap.ContainsKey(mId.MOVEMENT_ID)))
                    {
                        movementResult.IdMap.Add(mId.MOVEMENT_ID, cmd.MovementId.ToString());
                    }

                }

                session.Store(movementResult);
                session.SaveChanges();
            }
        }


        Dictionary<string, int> GetEquipments(List<EquipmentItem> equipments)
        {
            equipments.Reverse();
            var result = new Dictionary<string, int>();

            foreach (var item in equipments)
            {
                if (!result.ContainsKey(item.EQUIPMENT_CODE))
                    result.Add(item.EQUIPMENT_CODE, item.QUANTITY);
            }

            return result;
        }

        void SetHeaers(object message)
        {
            message.SetHeader("nTrack.AccountId", _accountId.ToString());
            message.SetHeader("nTrack.UserId", _userKey.ToString());
            message.SetHeader("nTrack.ApplicationName", "Migrator");
            message.SetHeader("nTrack.Version", "13.02.13");
        }
        void InitilizeCountries()
        {
            using (var session = _store.OpenSession("ntrack"))
            {
                if (session.Load<Countries>(Countries.EntityId) != null) return;

                var xml = XDocument.Load(@"country_names_and_code_elements.xml");
                var countries = new Countries();
                countries.CountryNames = xml.XPathSelectElements("//ISO_3166-1_Entry/ISO_3166-1_Country_name")
                                            .Select(x => ToCamelCase(x.Value.ToLower())).ToArray();

                session.Store(countries);
                session.SaveChanges();
            }

        }
        string ToCamelCase(string input)
        {
            string[] words = input.Split(' ');
            StringBuilder sb = new StringBuilder();
            foreach (string s in words)
            {
                string firstLetter = s.Substring(0, 1);
                string rest = s.Substring(1, s.Length - 1);
                sb.Append(firstLetter.ToUpper() + rest);
                sb.Append(' ');

            }
            return sb.ToString().Substring(0, sb.ToString().Length - 1);
        }
        void SeedProducts()
        {
            using (var session = _store.OpenSession("ntrack"))
            {
                if (session.Load<Subscription.Data.Product>(_productId) != null)
                    return;
            }

            var liteOffer = Guid.NewGuid();
            var bronzeOffer = Guid.NewGuid();
            var silverOffer = Guid.NewGuid();
            var goldOffer = Guid.NewGuid();

            var cmds = new List<object>
                           {
                               new Subscription.Messages.Commands.Product.CreateProduct
                                   {
                                       ProductId = _productId,
                                       BasePrice = 0,
                                       Code = "PP",
                                       Name = "Pallet Plus"
                                   },
                                new Subscription.Messages.Commands.Product.ActivateProduct{ProductId = _productId},
                                new Subscription.Messages.Commands.Product.AddFeature
                                    {
                                        Code = "LITE",
                                        Name = "Lite",
                                        ProductId = _productId,
                                        Limit = 100,
                                        Price = 0
                                    },
                                new Subscription.Messages.Commands.Product.ActivateFeature{ProductId = _productId,FeatureCode = "LITE"},
                                new Subscription.Messages.Commands.Product.AddFeature
                                    {
                                        Code = "BRONZE",
                                        Name = "Bronze",
                                        ProductId = _productId,
                                        Limit = 250,
                                        Price = 49
                                    },
                                new Subscription.Messages.Commands.Product.ActivateFeature{ProductId = _productId,FeatureCode = "BRONZE"},
                                new Subscription.Messages.Commands.Product.AddFeature
                                    {
                                        Code = "SILVER",
                                        Name = "Silver",
                                        ProductId = _productId,
                                        Limit = 500,
                                        Price = 79
                                    },
                                new Subscription.Messages.Commands.Product.ActivateFeature{ProductId = _productId,FeatureCode = "SILVER"},
                                new Subscription.Messages.Commands.Product.AddFeature
                                    {
                                        Code = "GOLD",
                                        Name = "Gold",
                                        ProductId = _productId,
                                        Limit = 2000,
                                        Price = 129
                                    },
                                new Subscription.Messages.Commands.Product.ActivateFeature{ProductId = _productId,FeatureCode = "GOLD"},
                                
                                new Subscription.Messages.Commands.Product.AddOffer
                                    {
                                        Name = "Lite Edition",
                                        ProductId = _productId,
                                        OfferKey = liteOffer
                                    },
                                new Subscription.Messages.Commands.Product.ActivateOffer{ProductId = _productId,OfferKey = liteOffer},
                                //new Subscription.Messages.Commands.Product.AddOfferFeatures{FeatureCodes = new []{"LITE"},ProductId = _productId,OfferKey = liteOffer},
                                new Subscription.Messages.Commands.Product.UpdateOfferFeatures(){Features = new List<OfferFeature>{new OfferFeature{Code = "LITE"}},ProductId = _productId,OfferKey = liteOffer},


                                new Subscription.Messages.Commands.Product.AddOffer
                                    {
                                        Name = "Bronze Edition",
                                        ProductId = _productId,
                                        OfferKey = bronzeOffer
                                    },
                                new Subscription.Messages.Commands.Product.ActivateOffer{ProductId = _productId,OfferKey = bronzeOffer},
                                //new Subscription.Messages.Commands.Product.AddOfferFeatures{FeatureCodes = new []{"BRONZE"},ProductId = _productId,OfferKey = bronzeOffer},
                                new Subscription.Messages.Commands.Product.UpdateOfferFeatures{Features = new List<OfferFeature>{new OfferFeature{Code = "BRONZE"}},ProductId = _productId,OfferKey = bronzeOffer},

                                new Subscription.Messages.Commands.Product.AddOffer
                                    {
                                        Name = "Silver Edition",
                                        ProductId = _productId,
                                        OfferKey = silverOffer
                                    },
                                new Subscription.Messages.Commands.Product.ActivateOffer{ProductId = _productId,OfferKey = silverOffer},
                                //new Subscription.Messages.Commands.Product.AddOfferFeatures{FeatureCodes = new []{"SILVER"},ProductId = _productId,OfferKey = silverOffer},
                                new Subscription.Messages.Commands.Product.UpdateOfferFeatures{Features = new List<OfferFeature>{new OfferFeature{Code = "SILVER"}},ProductId = _productId,OfferKey = silverOffer},

                                new Subscription.Messages.Commands.Product.AddOffer
                                    {
                                        Name = "Gold Edition",
                                        ProductId = _productId,
                                        OfferKey = goldOffer
                                    },
                                new Subscription.Messages.Commands.Product.ActivateOffer{ProductId = _productId,OfferKey = goldOffer},
                                //new Subscription.Messages.Commands.Product.AddOfferFeatures{FeatureCodes = new []{"GOLD"},ProductId = _productId,OfferKey = goldOffer},
                                new Subscription.Messages.Commands.Product.UpdateOfferFeatures{Features = new List<OfferFeature>{new OfferFeature{Code = "GOLD"}},ProductId = _productId,OfferKey = goldOffer},
                           };



            cmds.ForEach(cmd => _bus.Send(cmd));
        }
    }

    class EquipmentItem
    {
        public int MOVEMENT_ID { get; set; }
        public string EQUIPMENT_CODE { get; set; }
        public int QUANTITY { get; set; }
    }

    public class MigrationResult
    {
        public string Id { get; set; }
        public Dictionary<int, string> IdMap { get; set; }

        public MigrationResult()
        {
            IdMap = new Dictionary<int, string>();
        }
    }
}
