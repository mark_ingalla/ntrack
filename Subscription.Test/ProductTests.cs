﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonDomain.Persistence;
using Machine.Specifications;
using Subscription.Domain.Models.Product;
using Subscription.Messages.Events.Account;
using Subscription.Messages.Events.Product;

namespace Subscription.Test
{
    public class ProductTests : TestBase
    {
        protected static string ProductName = "ProductName";
        protected static Guid ProductId;

        //feature VO
        protected static string FeatureName = "FeatureName";
        protected static string FeatureCode = "FeatureCode";
        protected static string FeatureDescription = "FeatureDescription";

        //permission VO
        protected static string PermissionName = "PermissionName";
        protected static string PermissionCode = "PermissionCode";

        protected static Product CreateAndSaveProduct(ref Guid productId)
        {
            productId = Guid.NewGuid();
            var product = new Product(productId, ProductName);
            Repository.Save(product, Guid.NewGuid(), null);
            return product;
        }

        [Subject("Creating product, should raise event")]
        public class CreatingProduct
        {
            protected static ProductCreated _event;
            Establish context = () =>
            {
                Setup();
                ProductId = Guid.NewGuid();
            };
            Because of = () =>
            {
                var product = new Product(ProductId, ProductName);
                Repository.Save(product, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<ProductCreated>().First();
            };
            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It productIdShouldBeEqual = () => _event.ProductId.ShouldEqual(ProductId);
            It productNameShouldEqual = () => _event.Name.ShouldEqual(ProductName);
        }

        [Subject("Creating product with empty id, should raise exception")]
        public class CreatingProductWithEmptyId
        {
            static Exception _exception;
            Establish context = () =>
            {
                Setup();
                ProductId = Guid.NewGuid();

            };
            Because of = () =>
            {
                _exception = Catch.Exception(() => new Product(Guid.Empty, ProductName));
            };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Adding feature to product, should raise event")]
        public class AddingFeatureToProduct
        {
            static Product product;
            protected static FeatureAdded _event;
            Establish context = () =>
            {
                Setup();
                product = CreateAndSaveProduct(ref ProductId);
            };
            Because of = () =>
            {
                product.AddFeature(FeatureCode, FeatureName, FeatureDescription);
                Repository.Save(product, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<FeatureAdded>().First();
            };
            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It productIdShouldBeEqual = () => _event.ProductId.ShouldEqual(ProductId);
            It featureCodeShouldEqual = () => _event.FeatureCode.ShouldEqual(FeatureCode);
            It featureNameShouldEqual = () => _event.Name.ShouldEqual(FeatureName);
            It featureDescShouldEqual = () => _event.Description.ShouldEqual(FeatureDescription);
        }

        [Subject("Adding feature twice to product, should raise exception")]
        public class AddingFeatureTwiceToProduct
        {
            static Product product;
            protected static Exception _exception;
            Establish context = () =>
            {
                Setup();
                product = CreateAndSaveProduct(ref ProductId);
                product.AddFeature(FeatureCode, FeatureName, FeatureDescription);
                Repository.Save(product, Guid.NewGuid(), null);
            };
            Because of = () =>
                             {
                                 _exception = Catch.Exception(() => product.AddFeature(FeatureCode, FeatureName, FeatureDescription));
                             };
            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Discontinuing feature from product, should raise event")]
        public class DiscontinuingFeatureForProduct
        {
            static Product product;
            protected static FeatureDiscontinued _event;
            Establish context = () =>
            {
                Setup();
                product = CreateAndSaveProduct(ref ProductId);
                product.AddFeature(FeatureCode, FeatureName, FeatureDescription);
                Repository.Save(product, Guid.NewGuid(), null);
            };
            Because of = () =>
            {
                product.DiscontinueFeature(FeatureCode);
                Repository.Save(product, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<FeatureDiscontinued>().First();
            };
            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It productIdShouldBeEqual = () => _event.ProductId.ShouldEqual(ProductId);
            It featureCodeShouldEqual = () => _event.FeatureCode.ShouldEqual(FeatureCode);
        }

        [Subject("Discontinuing non existing feature from product, should raise exception")]
        public class DiscontinuingNonExistingFeatureFromProduct
        {
            static Product product;
            static Exception _exception;
            Establish context = () =>
            {
                Setup();
                product = CreateAndSaveProduct(ref ProductId);
                product.AddFeature(FeatureCode, FeatureName, FeatureDescription);
                Repository.Save(product, Guid.NewGuid(), null);
            };
            Because of = () =>
            {
                _exception = Catch.Exception(() => product.DiscontinueFeature("UnexisitngFeatureCode"));
            };
            It shouldRaiseEvent = () => _exception.ShouldNotBeNull();
        }

        [Subject("Adding permission for product, should raise event")]
        public class AddingPermissionForProduct
        {
            static Product product;
            protected static ProductPermissionAdded _event;
            Establish context = () =>
            {
                Setup();
                product = CreateAndSaveProduct(ref ProductId);
            };
            Because of = () =>
            {
                product.AddPermission(PermissionCode, PermissionName);
                Repository.Save(product, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<ProductPermissionAdded>().First();
            };
            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It productIdShouldBeEqual = () => _event.ProductId.ShouldEqual(ProductId);
            It permissionNameShouldEqual = () => _event.Name.ShouldEqual(PermissionName);
            It permissionCodeShouldEqual = () => _event.Code.ShouldEqual(PermissionCode);
        }

        [Subject("Adding permission twice to product, should raise exception")]
        public class AddingPermissionTwiceToProduct
        {
            static Product product;
            protected static Exception _exception;
            Establish context = () =>
            {
                Setup();
                product = CreateAndSaveProduct(ref ProductId);
                product.AddPermission(PermissionCode, PermissionName);
                Repository.Save(product, Guid.NewGuid(), null);
            };
            Because of = () =>
                             {
                                 _exception = Catch.Exception(() => product.AddPermission(PermissionCode, PermissionName));
                             };
            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Revoking added permission from product, should raise event")]
        public class RevokingAddedPermissionFromProduct
        {
            static Product product;
            protected static ProductPermissionRemoved _event;
            Establish context = () =>
            {
                Setup();
                product = CreateAndSaveProduct(ref ProductId);
                product.AddPermission(PermissionCode, PermissionName);
                Repository.Save(product, Guid.NewGuid(), null);
            };
            Because of = () =>
            {
                product.RemovePermission(PermissionCode);
                Repository.Save(product, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<ProductPermissionRemoved>().First();
            };
            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It productIdShouldBeEqual = () => _event.ProductId.ShouldEqual(ProductId);
            It permissionCodeShouldEqual = () => _event.Code.ShouldEqual(PermissionCode);
        }

        [Subject("Revoking non-added permission from product, should raise exception")]
        public class RevokingNonAddedPermissionFromProduct
        {
            static Product product;
            static Exception _exception;
            Establish context = () =>
            {
                Setup();
                product = CreateAndSaveProduct(ref ProductId);
                product.AddPermission(PermissionCode, PermissionName);
                Repository.Save(product, Guid.NewGuid(), null);
            };
            Because of = () =>
            {
                _exception = Catch.Exception(() => product.RemovePermission("NonExistingPermissionCode"));
            };
            It shouldRaiseEvent = () => _exception.ShouldNotBeNull();
        }

    }
}
