using System;
using Machine.Specifications;
using Subscription.Domain.Models.User;
using nTrack.Core.Exceptions;

namespace Subscription.Test.UserTests
{
    [Subject("Assigning user to existing account twice, should raise exception")]
    public class AssigningUserToExistingAccountTwice : UserTestBase
    {
        static Exception _exception;
        static User _user;
        Establish context = () =>
                                {
                                    Setup();
                                    UserId = Guid.NewGuid();
                                    _user = CreateAndSaveUser(ref UserId);
                                    _user.VerifyUser();
                                    Repository.Save(_user, Guid.NewGuid(), null);
                                };
        Because of = () =>
                         {
                             _exception = Catch.Exception(() => _user.AssignUserToAccount(Guid.NewGuid(), Role));
                         };
        It shouldRaiseException = () => _exception.ShouldNotBeNull();
        It shouldBeOfType = () => _exception.ShouldBeOfType(typeof(AggregateRootNotFoundException));
    }
}