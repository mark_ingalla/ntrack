using System;
using System.Linq;
using Machine.Specifications;
using Subscription.Domain.Models.User;
using Subscription.Messages.Events.User;

namespace Subscription.Test.UserTests
{
    [Subject("Verifying user, should raise event")]
    public class VeryfyingUser : UserTestBase
    {
        protected static UserVerified _event;
        static User _user;
        Establish context = () =>
                                {
                                    Setup();
                                    _user = CreateAndSaveUser(ref UserId);
                                };
        Because of = () =>
                         {
                             _user.VerifyUser();
                             Repository.Save(_user, Guid.NewGuid(), null);
                             _event = Commit.Events.Select(x => x.Body).OfType<UserVerified>().First();
                         };
        It shouldRaiseEvent = () => _event.ShouldNotBeNull();
        It userIdShouldEqual = () => _event.UserId.ShouldEqual(UserId);
        It shouldUpdateProperty = () => _user.EmailVerfied.ShouldBeTrue();
    }
}