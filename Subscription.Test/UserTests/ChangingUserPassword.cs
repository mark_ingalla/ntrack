using System;
using System.Linq;
using Machine.Specifications;
using Subscription.Domain.Models.User;
using Subscription.Messages.Events.User;

namespace Subscription.Test.UserTests
{
    [Subject("Changing user password, should raise event")]
    public class ChangingUserPassword : UserTestBase
    {
        protected static PasswordChanged _event;
        static User _user;
        static string newPassword = "NewPassword";
        Establish context = () =>
                                {
                                    Setup();
                                    _user = CreateAndSaveUser(ref UserId);
                                    _user.VerifyUser();
                                };
        Because of = () =>
                         {
                             _user.ChangePassword(UserPassword, newPassword);
                             Repository.Save(_user, Guid.NewGuid(), null);
                             _event = Commit.Events.Select(x => x.Body).OfType<PasswordChanged>().First();
                         };
        It shouldRaiseEvent = () => _event.ShouldNotBeNull();
        It userIdShouldEqual = () => _event.UserId.ShouldEqual(UserId);
        It oldPasswordShouldEqual = () => _event.OldPassword.ShouldEqual(UserPassword);
        It newPasswordShouldEqual = () => _event.NewPassword.ShouldEqual(newPassword);
    }
}