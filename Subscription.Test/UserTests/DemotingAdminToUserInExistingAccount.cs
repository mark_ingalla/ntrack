using System;
using System.Linq;
using Machine.Specifications;
using Subscription.Domain.Models.Account;
using Subscription.Domain.Models.User;
using Subscription.Messages.Events.User;

namespace Subscription.Test.UserTests
{
    [Subject("Demoting admin to user for existing account, should raise event")]
    public class DemotingAdminToUserInExistingAccount : UserTestBase
    {
        protected static AdminDemotedToUser _event;
        static User _user;
        static Account account;
        static Guid accountId;
        Establish context = () =>
                                {
                                    Setup();
                                    accountId = Guid.NewGuid();
                                    account = CreateAndSaveAccount(ref accountId);
                                    UserId = Guid.NewGuid();
                                    _user = CreateAndSaveUser(ref UserId);

                                    account.AuthorizeUser(UserId);
                                    Repository.Save(account, Guid.NewGuid(), null);
                                    _user.VerifyUser();
                                    _user.AssignUserToAccount(accountId, Role);
                                    _user.PromoteToAdmin(accountId);
                                    Repository.Save(_user, Guid.NewGuid(), null);
                                };
        Because of = () =>
                         {
                             _user.DemoteToUser(accountId);
                             Repository.Save(_user, Guid.NewGuid(), null);
                             _event = Commit.Events.Select(x => x.Body).OfType<AdminDemotedToUser>().First();
                         };
        It shouldRaiseEvent = () => _event.ShouldNotBeNull();
        It userIdShouldEqual = () => _event.UserId.ShouldEqual(UserId);
        It accountIdShouldEqual = () => _event.AccountId.ShouldEqual(accountId);
        It shouldUpdateRoles = () => _user.AccountRoles[accountId].TypeName.ToString().ShouldEqual(Domain.Models.User.Role.RoleType.User.ToString());
    }
}