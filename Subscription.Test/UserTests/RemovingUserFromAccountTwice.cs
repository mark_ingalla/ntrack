using System;
using Machine.Specifications;
using Subscription.Domain.Models.Account;
using Subscription.Domain.Models.User;

namespace Subscription.Test.UserTests
{
    [Subject("Removing user from existing account twice, should throw exception")]
    public class RemovingUserFromAccountTwice : UserTestBase
    {
        static User _user;
        static Account account;
        static Guid accountId;
        static Exception _exception;
        Establish context = () =>
                                {
                                    Setup();
                                    accountId = Guid.NewGuid();
                                    account = CreateAndSaveAccount(ref accountId);
                                    UserId = Guid.NewGuid();
                                    _user = CreateAndSaveUser(ref UserId);
                                    _user.VerifyUser();
                                    _user.AssignUserToAccount(accountId, Role);
                                    _user.RemoveAssignmentToAccount(accountId);
                                    Repository.Save(_user, Guid.NewGuid(), null);
                                };
        Because of = () =>
                         {
                             _exception = Catch.Exception(() => _user.RemoveAssignmentToAccount(accountId));
                         };
        It shouldThrowException = () => _exception.ShouldNotBeNull();
    }
}