using System;
using Machine.Specifications;
using Subscription.Domain.Models.User;

namespace Subscription.Test.UserTests
{
    [Subject("Resetting password of unverified user, should raise exception")]
    public class ResettingPasswordOfUnverifiedUser : UserTestBase
    {
        static User _user;
        static string newPassword = "NewPassword";
        static Exception _exception;
        Establish context = () =>
                                {
                                    Setup();
                                    _user = CreateAndSaveUser(ref UserId);
                                };
        Because of = () =>
                         {
                             _exception = Catch.Exception(() => _user.ResetPassword(newPassword));
                         };
        It shouldRaiseException = () => _exception.ShouldNotBeNull();
    }
}