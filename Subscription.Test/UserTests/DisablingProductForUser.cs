using System;
using System.Linq;
using Machine.Specifications;
using Subscription.Domain.Models.Account;
using Subscription.Domain.Models.User;
using Subscription.Messages.Events.User;

namespace Subscription.Test.UserTests
{
    [Subject("Disabling product for user, should raise event")]
    public class DisablingProductForUser : UserTestBase
    {
        protected static UserProductRemoved _event;
        static User _user;
        static Account account;
        static Guid accountId;
        static Guid productId;
        Establish context = () =>
                                {
                                    Setup();
                                    var product = CreateAndSaveProductWithPermissionsAndFeatures(ref productId);
                                    accountId = Guid.NewGuid();
                                    account = CreateAndSaveAccount(ref accountId);
                                    UserId = Guid.NewGuid();
                                    _user = CreateAndSaveUser(ref UserId);
                                    account.AuthorizeUser(UserId);
                                    account.Subscribe(productId);
                                    Repository.Save(account, Guid.NewGuid(), null);
                                    _user.VerifyUser();
                                    _user.AssignUserToAccount(accountId, Role);
                                    _user.AddPermission(accountId, Role, productId, PermissionCode);
                                    Repository.Save(_user, Guid.NewGuid(), null);
                                };
        Because of = () =>
                         {
                             _user.RemoveProduct(productId, accountId);
                             Repository.Save(_user, Guid.NewGuid(), null);
                             _event = Commit.Events.Select(x => x.Body).OfType<UserProductRemoved>().First();
                         };
        It shouldRaiseEvent = () => _event.ShouldNotBeNull();
        It userIdShouldEqual = () => _event.UserId.ShouldEqual(UserId);
        It accountIdShouldEqual = () => _event.AccountId.ShouldEqual(accountId);
        It productIdShouldEqual = () => _event.ProductId.ShouldEqual(productId);
    }
}