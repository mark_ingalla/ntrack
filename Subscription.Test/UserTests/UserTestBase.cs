using System;
using Subscription.Domain.Models.Account;
using Subscription.Domain.Models.Product;
using Subscription.Domain.Models.User;

namespace Subscription.Test.UserTests
{
    public class UserTestBase : TestBase
    {
        protected static Guid UserId;
        protected static string UserFirstName = "FirstName";
        protected static string UserMobile = "Mobile";
        protected static string UserLastName = "LastName";
        protected static string UserEmail = "email@email.com";
        protected static string UserPassword = "password";
        protected static string UserPhone = "phone";

        protected static string Role = "RoleName";

        protected static User CreateAndSaveUser(ref Guid userId)
        {
            userId = Guid.NewGuid();
            var user = new User(userId, UserFirstName, UserLastName, UserEmail, UserPassword);
            Repository.Save(user, Guid.NewGuid(), null);
            return user;
        }

        //needed for association user with account
        protected static Guid _accountId;
        protected static Guid _adminId;
        protected static string _companyName;
        protected static string AddressLine1 = "AddLine1";
        protected static string AddressLine2 = "AddLine2";
        protected static string AddressLine3 = "AddLine3";
        protected static string Phone = "1234567";
        protected static string Country = "Australia";
        protected static string Suburb = "Suburb";
        protected static string State = "NSW";
        protected static string PCode = "1234";

        protected static ContactDetail ContactDetail = new ContactDetail(AddressLine1, AddressLine2, AddressLine3,
                                                                         Suburb, State, PCode, Country, Phone);

        protected static Account CreateAndSaveAccount(ref Guid accountId)
        {
            _adminId = Guid.NewGuid();
            var admin = CreateAndSaveUser(ref _adminId);
            admin.VerifyUser();
            accountId = Guid.NewGuid();
            var account = new Account(accountId, _companyName, ContactDetail.Phone,ContactDetail.Country);
            account.AuthorizeUser(_adminId);
            Repository.Save(account, Guid.NewGuid(), null);
            return account;
        }

        protected static string ProductName = "ProductName";
        protected static Guid ProductId;

        //feature VO
        protected static string FeatureName = "FeatureName";
        protected static string FeatureCode = "FeatureCode";
        protected static string FeatureDescription = "FeatureDescription";
        //permission VO
        protected static string PermissionName = "PermissionName";
        protected static string PermissionCode = "PermissionCode";

        protected static Product CreateAndSaveProductWithPermissionsAndFeatures(ref Guid productId)
        {
            productId = Guid.NewGuid();
            var product = new Product(productId, ProductName);
            product.AddFeature(FeatureCode, FeatureName, FeatureDescription);
            product.AddPermission(PermissionCode, PermissionName);
            Repository.Save(product, Guid.NewGuid(), null);
            return product;
        }
    }
}