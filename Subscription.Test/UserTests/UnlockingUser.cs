using System;
using System.Linq;
using Machine.Specifications;
using Subscription.Domain.Models.User;
using Subscription.Messages.Events.User;

namespace Subscription.Test.UserTests
{
    [Subject("Unlocking user, should raise event")]
    public class UnlockingUser : UserTestBase
    {
        static string lockingReason = "Reason";
        static string unlockingReason = "ReasonReason";
        protected static UserUnlocked _event;
        static User _user;

        Establish context = () =>
                                {
                                    Setup();
                                    _user = CreateAndSaveUser(ref UserId);
                                    _user.VerifyUser();
                                    _user.LockUser(lockingReason);
                                };
        Because of = () =>
                         {
                             _user.UnlockUser(unlockingReason);
                             Repository.Save(_user, Guid.NewGuid(), null);
                             _event = Commit.Events.Select(x => x.Body).OfType<UserUnlocked>().First();
                         };

        It shouldRaiseEvent = () => _event.ShouldNotBeNull();
        It userIdShouldEqual = () => _event.UserId.ShouldEqual(UserId);
        It reasonShouldEqual = () => _event.Reason.ShouldEqual(unlockingReason);
        It shouldPublishDate = () => _event.UnlockDate.ShouldBeGreaterThan(DateTime.MinValue);
        It shouldUpdateProperty = () => _user.IsLocked.ShouldBeFalse();
    }
}