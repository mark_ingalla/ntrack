using System;
using System.Linq;
using Machine.Specifications;
using Subscription.Messages.Events.User;

namespace Subscription.Test.UserTests
{
    [Subject("Creating user, should raise event")]
    public class CreatingUser : UserTestBase
    {
        protected static UserCreated _event;
        Establish context = () =>
                                {
                                    TestBase.Setup();
                                    UserId = Guid.NewGuid();
                                };
        Because of = () =>
                         {
                             var user = new Domain.Models.User.User(UserId, UserFirstName, UserLastName, UserEmail, UserPassword);
                             TestBase.Repository.Save(user, Guid.NewGuid(), null);
                             _event = TestBase.Commit.Events.Select(x => x.Body).OfType<UserCreated>().First();
                         };
        It shouldRaiseEvent = () => _event.ShouldNotBeNull();
        It userIdShouldEqual = () => _event.UserId.ShouldEqual(UserId);
        It userFirstNameShouldEqual = () => _event.FirstName.ShouldEqual(UserFirstName);
        It userLastNameShouldEqual = () => _event.LastName.ShouldEqual(UserLastName);
        It userEmailShouldEqual = () => _event.Email.ShouldEqual(UserEmail);
        It userPasswordShouldEqual = () => _event.Password.ShouldEqual(UserPassword);
    }
}