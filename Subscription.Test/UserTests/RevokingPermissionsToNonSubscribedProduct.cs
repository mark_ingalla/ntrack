using System;
using Machine.Specifications;
using Subscription.Domain.Models.Account;
using Subscription.Domain.Models.User;

namespace Subscription.Test.UserTests
{
    [Subject("Revoking permissions from non subscribed product, should raise exception")]
    public class RevokingPermissionsToNonSubscribedProduct : UserTestBase
    {
        static User _user;
        static Account account;
        static Guid accountId;
        static Guid productId;
        static Exception _exception;
        Establish context = () =>
                                {
                                    Setup();
                                    CreateAndSaveProductWithPermissionsAndFeatures(ref productId);
                                    accountId = Guid.NewGuid();
                                    account = CreateAndSaveAccount(ref accountId);
                                    UserId = Guid.NewGuid();
                                    _user = CreateAndSaveUser(ref UserId);
                                    account.AuthorizeUser(UserId);
                                    Repository.Save(account, Guid.NewGuid(), null);
                                    _user.VerifyUser();
                                    _user.AssignUserToAccount(accountId, Role);
                                    Repository.Save(_user, Guid.NewGuid(), null);
                                };
        Because of = () =>
                         {
                             _exception = Catch.Exception(() => _user.RevokePermission(accountId, Role, productId, PermissionCode));

                         };
        It shouldRaiseException = () => _exception.ShouldNotBeNull();
    }
}