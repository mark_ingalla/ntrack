using System;
using Machine.Specifications;
using Subscription.Domain.Models.User;

namespace Subscription.Test.UserTests
{
    [Subject("Unlocking unlocked user, should raise exception")]
    public class UnlockingUserTwice : UserTestBase
    {
        static string lockingReason = "Reason";
        static Exception _exception;
        static User _user;

        Establish context = () =>
                                {
                                    Setup();
                                    _user = CreateAndSaveUser(ref UserId);
                                    _user.VerifyUser();
                                    Repository.Save(_user, Guid.NewGuid(), null);
                                };
        Because of = () =>
                         {
                             _exception = Catch.Exception(() => _user.UnlockUser(lockingReason));
                         };

        It shouldThrowException = () => _exception.ShouldNotBeNull();
    }
}