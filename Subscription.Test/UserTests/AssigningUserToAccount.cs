using System;
using System.Linq;
using Machine.Specifications;
using Subscription.Domain.Models.Account;
using Subscription.Domain.Models.User;
using Subscription.Messages.Events.User;

namespace Subscription.Test.UserTests
{
    [Subject("Assigning user to existing account, should raise event")]
    public class AssigningUserToAccount : UserTestBase
    {
        protected static UserAssignedToAccount _event;
        static User _user;
        static Account account;
        static Guid accountId;
        Establish context = () =>
                                {
                                    Setup();
                                    accountId = Guid.NewGuid();
                                    account = CreateAndSaveAccount(ref accountId);
                                    UserId = Guid.NewGuid();
                                    _user = CreateAndSaveUser(ref UserId);
                                    _user.VerifyUser();
                                    Repository.Save(_user, Guid.NewGuid(), null);
                                };
        Because of = () =>
                         {
                             _user.AssignUserToAccount(accountId, Role);
                             Repository.Save(_user, Guid.NewGuid(), null);
                             _event = Commit.Events.Select(x => x.Body).OfType<UserAssignedToAccount>().First();
                         };
        It shouldRaiseEvent = () => _event.ShouldNotBeNull();
        It userIdShouldEqual = () => _event.UserId.ShouldEqual(UserId);
        It accountIdShouldEqual = () => _event.AccountId.ShouldEqual(accountId);
        It roleShouldEqual = () => _event.Role.ShouldEqual(Role);
    }
}