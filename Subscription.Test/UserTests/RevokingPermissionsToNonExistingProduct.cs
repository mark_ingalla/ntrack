using System;
using Machine.Specifications;
using Subscription.Domain.Models.Account;
using Subscription.Domain.Models.User;

namespace Subscription.Test.UserTests
{
    [Subject("Revoking permissions to non existing product, should raise exception")]
    public class RevokingPermissionsToNonExistingProduct : UserTestBase
    {
        static User _user;
        static Account account;
        static Guid accountId;
        static Guid productId;
        static Exception _exception;
        Establish context = () =>
                                {
                                    Setup();
                                    CreateAndSaveProductWithPermissionsAndFeatures(ref productId);
                                    var p = Guid.NewGuid();
                                    CreateAndSaveProductWithPermissionsAndFeatures(ref p);
                                    accountId = Guid.NewGuid();
                                    account = CreateAndSaveAccount(ref accountId);
                                    UserId = Guid.NewGuid();
                                    _user = CreateAndSaveUser(ref UserId);
                                    account.AuthorizeUser(UserId);
                                    account.Subscribe(p);
                                    Repository.Save(account, Guid.NewGuid(), null);
                                    _user.VerifyUser();
                                    _user.AssignUserToAccount(accountId, Role);
                                    Repository.Save(_user, Guid.NewGuid(), null);
                                };
        Because of = () =>
                         {
                             _exception = Catch.Exception(() => _user.AddPermission(accountId, Role, Guid.NewGuid(), PermissionCode));

                         };
        It shouldRaiseException = () => _exception.ShouldNotBeNull();
    }
}