using System;
using Machine.Specifications;
using Subscription.Domain.Models.Account;
using Subscription.Domain.Models.User;

namespace Subscription.Test.UserTests
{
    [Subject("Revoking permissions to non authorized account, should raise exception")]
    public class RevokingPermissionsToNotAuthorizedAccount : UserTestBase
    {
        static Exception _exception;
        static User _user;
        static Account account;
        static Guid accountId;
        static Guid productId;
        Establish context = () =>
                                {
                                    Setup();
                                    var product = CreateAndSaveProductWithPermissionsAndFeatures(ref productId);
                                    accountId = Guid.NewGuid();
                                    account = CreateAndSaveAccount(ref accountId);
                                    UserId = Guid.NewGuid();
                                    _user = CreateAndSaveUser(ref UserId);
                                    account.AuthorizeUser(UserId);
                                    account.Subscribe(productId);
                                    var id = Guid.NewGuid();
                                    Repository.Save(account, id, null);
                                    _user.VerifyUser();
                                    //_user.AssignUserToAccount(accountId, RoleName);
                                    Repository.Save(_user, id, null);
                                };
        Because of = () =>
                         {
                             _exception = Catch.Exception(() => _user.RevokePermission(accountId, Role, productId, PermissionCode));
                         };
        It shouldRaiseException = () => _exception.ShouldNotBeNull();
    }
}