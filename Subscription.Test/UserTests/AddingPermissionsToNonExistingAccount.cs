using System;
using Machine.Specifications;
using Subscription.Domain.Models.User;

namespace Subscription.Test.UserTests
{
    [Subject("Adding permissions to non existing account, should raise exception")]
    public class AddingPermissionsToNonExistingAccount : UserTestBase
    {
        static Exception _exception;
        static User _user;
        static Guid productId;
        Establish context = () =>
                                {
                                    Setup();
                                    CreateAndSaveProductWithPermissionsAndFeatures(ref productId);
                                    UserId = Guid.NewGuid();
                                    _user = CreateAndSaveUser(ref UserId);
                                    _user.VerifyUser();
                                    Repository.Save(_user, Guid.NewGuid(), null);
                                };
        Because of = () =>
                         {
                             _exception = Catch.Exception(() => _user.AddPermission(Guid.NewGuid(), Role, productId, PermissionCode));
                         };
        It shouldRaiseException = () => _exception.ShouldNotBeNull();
    }
}