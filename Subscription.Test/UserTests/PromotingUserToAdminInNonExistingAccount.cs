using System;
using Machine.Specifications;
using Subscription.Domain.Models.User;

namespace Subscription.Test.UserTests
{
    [Subject("Promoting user to admin for non existing account, should raise event")]
    public class PromotingUserToAdminInNonExistingAccount : UserTestBase
    {
        static Exception _exception;
        static User _user;
        Establish context = () =>
                                {
                                    Setup();
                                    UserId = Guid.NewGuid();
                                    _user = CreateAndSaveUser(ref UserId);
                                    _user.VerifyUser();
                                    Repository.Save(_user, Guid.NewGuid(), null);
                                };
        Because of = () =>
                         {
                             _exception = Catch.Exception(() => _user.PromoteToAdmin(Guid.NewGuid()));
                         };

        It shouldRaiseException = () => _exception.ShouldNotBeNull();
    }
}