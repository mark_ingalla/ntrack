using System;
using System.Linq;
using Machine.Specifications;
using Subscription.Domain.Models.Account;
using Subscription.Domain.Models.User;
using Subscription.Messages.Events.User;

namespace Subscription.Test.UserTests
{
    [Subject("Adding permissions, should raise event")]
    public class AddingPermissions : UserTestBase
    {
        protected static UserPermissionAdded _event;
        static User _user;
        static Account account;
        static Guid accountId;
        static Guid productId;
        Establish context = () =>
                                {
                                    Setup();
                                    var product = CreateAndSaveProductWithPermissionsAndFeatures(ref productId);
                                    accountId = Guid.NewGuid();
                                    account = CreateAndSaveAccount(ref accountId);
                                    UserId = Guid.NewGuid();
                                    _user = CreateAndSaveUser(ref UserId);
                                    account.Subscribe(productId);
                                    account.AuthorizeUser(UserId);

                                    Repository.Save(account, Guid.NewGuid(), null);
                                    _user.VerifyUser();
                                    _user.AssignUserToAccount(accountId, Role);
                                    Repository.Save(_user, Guid.NewGuid(), null);
                                };
        Because of = () =>
                         {
                             _user.AddPermission(accountId, Role, productId, PermissionCode);
                             Repository.Save(_user, Guid.NewGuid(), null);
                             _event = Commit.Events.Select(x => x.Body).OfType<UserPermissionAdded>().First();
                         };
        It shouldRaiseEvent = () => _event.ShouldNotBeNull();
        It userIdShouldEqual = () => _event.UserId.ShouldEqual(UserId);
        It accountIdShouldEqual = () => _event.AccountId.ShouldEqual(accountId);
        It productIdShouldEqual = () => _event.ProductId.ShouldEqual(productId);
        It roleShouldEqual = () => _event.Role.ShouldEqual(Role);
        It permissionCodeShouldEqual = () => _event.PermissionCode.ShouldEqual(PermissionCode);
    }
}