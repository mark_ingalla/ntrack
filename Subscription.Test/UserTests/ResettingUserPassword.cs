using System;
using System.Linq;
using Machine.Specifications;
using Subscription.Domain.Models.User;
using Subscription.Messages.Events.User;

namespace Subscription.Test.UserTests
{
    [Subject("Resetting user password, should raise event")]
    public class ResettingUserPassword : UserTestBase
    {
        protected static PasswordReseted _event;
        static User _user;
        static string newPassword = "NewPassword";
        Establish context = () =>
                                {
                                    Setup();
                                    _user = CreateAndSaveUser(ref UserId);
                                    _user.VerifyUser();
                                };
        Because of = () =>
                         {
                             _user.ResetPassword(newPassword);
                             Repository.Save(_user, Guid.NewGuid(), null);
                             _event = Commit.Events.Select(x => x.Body).OfType<PasswordReseted>().First();
                         };
        It shouldRaiseEvent = () => _event.ShouldNotBeNull();
        It userIdShouldEqual = () => _event.UserId.ShouldEqual(UserId);
        It newPasswordShouldEqual = () => _event.Password.ShouldEqual(newPassword);
    }
}