using System;
using System.Linq;
using Machine.Specifications;
using Subscription.Domain.Models.User;
using Subscription.Messages.Events.User;

namespace Subscription.Test.UserTests
{
    [Subject("Locking user, should raise event")]
    public class LockingUser : UserTestBase
    {
        static string lockingReason = "Reason";
        protected static UserLocked _event;
        static User _user;

        Establish context = () =>
                                {
                                    Setup();
                                    _user = CreateAndSaveUser(ref UserId);
                                    _user.VerifyUser();
                                };
        Because of = () =>
                         {
                             _user.LockUser(lockingReason);
                             Repository.Save(_user, Guid.NewGuid(), null);
                             _event = Commit.Events.Select(x => x.Body).OfType<UserLocked>().First();
                         };

        It shouldRaiseEvent = () => _event.ShouldNotBeNull();
        It userIdShouldEqual = () => _event.UserId.ShouldEqual(UserId);
        It reasonShouldEqual = () => _event.Reason.ShouldEqual(lockingReason);
        It shouldPublishDate = () => _event.LockDate.ShouldBeGreaterThan(DateTime.MinValue);
        It shouldUpdateProperty = () => _user.IsLocked.ShouldBeTrue();
    }
}