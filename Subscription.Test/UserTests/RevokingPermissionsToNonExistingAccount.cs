using System;
using Machine.Specifications;
using Subscription.Domain.Models.User;

namespace Subscription.Test.UserTests
{
    [Subject("Revoking permissions to non existing account, should raise exception")]
    public class RevokingPermissionsToNonExistingAccount : UserTestBase
    {
        static Exception _exception;
        static User _user;
        static Guid productId;
        Establish context = () =>
                                {
                                    Setup();
                                    UserId = Guid.NewGuid();
                                    _user = CreateAndSaveUser(ref UserId);
                                    _user.VerifyUser();
                                    Repository.Save(_user, Guid.NewGuid(), null);
                                    var product = CreateAndSaveProductWithPermissionsAndFeatures(ref productId);
                                };
        Because of = () =>
                         {
                             _exception = Catch.Exception(() => _user.RevokePermission(Guid.NewGuid(), Role, productId, PermissionCode));
                         };
        It shouldRaiseException = () => _exception.ShouldNotBeNull();
    }
}