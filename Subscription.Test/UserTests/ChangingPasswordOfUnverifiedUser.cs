using System;
using Machine.Specifications;
using Subscription.Domain.Models.User;

namespace Subscription.Test.UserTests
{
    [Subject("Changing password of unverified user, should raise exception")]
    public class ChangingPasswordOfUnverifiedUser : UserTestBase
    {
        static User _user;
        static string newPassword = "NewPassword";
        static Exception _exception;
        Establish context = () =>
                                {
                                    Setup();
                                    _user = CreateAndSaveUser(ref UserId);
                                };
        Because of = () =>
                         {
                             _exception = Catch.Exception(() => _user.ChangePassword(UserPassword, newPassword));
                         };
        It shouldRaiseException = () => _exception.ShouldNotBeNull();
    }
}