using System;
using Machine.Specifications;
using Subscription.Domain.Models.Account;
using Subscription.Domain.Models.User;

namespace Subscription.Test.UserTests
{
    [Subject("Assigning user to non existing account, should raise exception")]
    public class AssigningUserToNonExistentAccount : UserTestBase
    {
        static User _user;
        static Account account;
        static Guid accountId;
        static Exception _exception;
        Establish context = () =>
                                {
                                    Setup();
                                    accountId = Guid.NewGuid();
                                    account = CreateAndSaveAccount(ref accountId);
                                    UserId = Guid.NewGuid();
                                    _user = CreateAndSaveUser(ref UserId);
                                    _user.VerifyUser();
                                    Repository.Save(_user, Guid.NewGuid(), null);
                                    _user.AssignUserToAccount(accountId, Role);
                                };
        Because of = () =>
                         {
                             _exception = Catch.Exception(() => _user.AssignUserToAccount(accountId, Role));
                         };
        It shouldRaiseException = () => _exception.ShouldNotBeNull();
    }
}