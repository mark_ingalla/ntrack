using System;
using Machine.Specifications;
using Subscription.Domain.Models.User;

namespace Subscription.Test.UserTests
{
    [Subject("Demoting admin to user for non existing account, should raise exception")]
    public class DemotingUserToAdminInNonExistingAccount : UserTestBase
    {
        static Exception _exception;
        static User _user;
        Establish context = () =>
                                {
                                    Setup();
                                    UserId = Guid.NewGuid();
                                    _user = CreateAndSaveUser(ref UserId);
                                    _user.VerifyUser();
                                    Repository.Save(_user, Guid.NewGuid(), null);
                                };
        Because of = () =>
                         {
                             _exception = Catch.Exception(() => _user.DemoteToUser(Guid.NewGuid()));
                         };

        It shouldRaiseException = () => _exception.ShouldNotBeNull();
    }
}