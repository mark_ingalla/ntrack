using System;
using Machine.Specifications;
using Subscription.Domain.Models.User;

namespace Subscription.Test.UserTests
{
    [Subject("Changing user password passing incorrect old password, should raise exception")]
    public class ChangingUserPasswordPassingIncorrectOldPassword : UserTestBase
    {
        static User _user;
        static string newPassword = "NewPassword";
        static Exception _exception;
        Establish context = () =>
                                {
                                    Setup();
                                    _user = CreateAndSaveUser(ref UserId);
                                };
        Because of = () =>
                         {
                             _exception = Catch.Exception(() => _user.ChangePassword("IncorrectOldPassword", newPassword));
                         };
        It shouldRaiseException = () => _exception.ShouldNotBeNull();
    }
}