using System;
using Machine.Specifications;
using Subscription.Domain.Models.Account;
using Subscription.Domain.Models.User;

namespace Subscription.Test.UserTests
{
    [Subject("Promoting user to admin for non assigned account, should raise event")]
    public class PromotingUserToAdminInNonAssignedAccount : UserTestBase
    {
        static Exception _exception;
        static User _user;
        static Account account;
        static Guid accountId;
        Establish context = () =>
                                {
                                    Setup();
                                    accountId = Guid.NewGuid();
                                    account = CreateAndSaveAccount(ref accountId);
                                    UserId = Guid.NewGuid();
                                    _user = CreateAndSaveUser(ref UserId);
                                    _user.VerifyUser();
                                    _user.AssignUserToAccount(accountId, Role);
                                    Repository.Save(_user, Guid.NewGuid(), null);
                                };
        Because of = () =>
                         {
                             _exception = Catch.Exception(() => _user.PromoteToAdmin(accountId));
                         };

        It shouldRaiseException = () => _exception.ShouldNotBeNull();
    }
}