using System;
using Machine.Specifications;
using Subscription.Domain.Models.Account;
using Subscription.Domain.Models.User;

namespace Subscription.Test.UserTests
{
    [Subject("Revoking permissions to not existing role, should raise exception")]
    public class RevokingPermissionsToNotExistingRole : UserTestBase
    {
        static Exception _exception;
        static User _user;
        static Account account;
        static Guid accountId;
        static Guid productId;
        Establish context = () =>
                                {
                                    Setup();
                                    var product = CreateAndSaveProductWithPermissionsAndFeatures(ref productId);
                                    accountId = Guid.NewGuid();
                                    account = CreateAndSaveAccount(ref accountId);
                                    UserId = Guid.NewGuid();
                                    _user = CreateAndSaveUser(ref UserId);
                                    account.AuthorizeUser(UserId);
                                    account.Subscribe(productId);
                                    Repository.Save(account, Guid.NewGuid(), null);
                                    _user.VerifyUser();
                                    _user.AssignUserToAccount(accountId, Role);
                                    _user.AddPermission(accountId, Role, productId, PermissionCode);
                                    Repository.Save(_user, Guid.NewGuid(), null);
                                };
        Because of = () =>
                         {
                             _exception = Catch.Exception(() => _user.RevokePermission(accountId, "DiffRoleName", productId, PermissionCode));
                         };
        It shouldRaiseException = () => _exception.ShouldNotBeNull();
    }
}