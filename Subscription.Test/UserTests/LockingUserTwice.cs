using System;
using Machine.Specifications;
using Subscription.Domain.Models.User;

namespace Subscription.Test.UserTests
{
    [Subject("Locking user twice, should raise exception")]
    public class LockingUserTwice : UserTestBase
    {
        static string lockingReason = "Reason";
        static Exception _exception;
        static User _user;

        Establish context = () =>
                                {
                                    Setup();
                                    _user = CreateAndSaveUser(ref UserId);
                                    _user.VerifyUser();
                                    _user.LockUser(lockingReason);
                                    Repository.Save(_user, Guid.NewGuid(), null);
                                };
        Because of = () =>
                         {
                             _exception = Catch.Exception(() => _user.LockUser(lockingReason));
                         };

        It shouldThrowException = () => _exception.ShouldNotBeNull();
    }
}