﻿using CommonDomain;
using CommonDomain.Core;
using CommonDomain.Persistence;
using CommonDomain.Persistence.EventStore;
using EventStore;
using EventStore.Dispatcher;
using Raven.Client;
using Raven.Client.Embedded;
using StructureMap;
using Subscription.AppHost.Infrastructure;

namespace Subscription.Test
{
    public class TestBase
    {
        public static EmbeddableDocumentStore DocumentStore { get; set; }
        public static IStoreEvents Store { get; set; }
        public static IRepository Repository { get; set; }
        public static Commit Commit { get; set; }

        public static void Setup()
        {
            Store = Wireup.Init()
                .UsingInMemoryPersistence()
                .InitializeStorageEngine()
                .UsingSynchronousDispatchScheduler()
                    .DispatchTo(new DelegateMessageDispatcher(Dispatch))
                .Build();

            DocumentStore = new EmbeddableDocumentStore() { RunInMemory = true };

            ObjectFactory.Configure(
                x =>
                {
                    x.ForSingletonOf<IStoreEvents>().Use(Store);
                    x.ForSingletonOf<IConstructAggregates>().Use<AggregateFactory>();
                    x.ForSingletonOf<IRepository>().Use<EventStoreRepository>();
                    x.ForSingletonOf<IDetectConflicts>().Use<ConflictDetector>();
                    x.ForSingletonOf<EmbeddableDocumentStore>().Use(DocumentStore);
                    x.ForSingletonOf<IDocumentStore>().Use(DocumentStore);
                    x.ForSingletonOf<IDocumentSession>().Use(ct => ct.GetInstance<IDocumentStore>().Initialize().OpenSession());
                });

            Repository = ObjectFactory.GetInstance<IRepository>();
        }

        private static void Dispatch(Commit commit)
        {
            Commit = commit;
        }
    }
}
