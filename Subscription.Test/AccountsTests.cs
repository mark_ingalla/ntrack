﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonDomain.Persistence;
using Machine.Specifications;
using Subscription.Domain.Models.Account;
using Subscription.Domain.Models.Product;
using Subscription.Domain.Models.User;
using Subscription.Messages.Events.Account;

namespace Subscription.Test
{
    public class AccountsTests : TestBase
    {
        protected static Guid _accountId;
        protected static Account _account;
        protected static Guid _adminId;
        protected static string _companyName;
        protected static string AddressLine1 = "AddLine1";
        protected static string AddressLine2 = "AddLine2";
        protected static string AddressLine3 = "AddLine3";
        protected static string Phone = "1234567";
        protected static string Country = "Australia";
        protected static string Suburb = "Suburb";
        protected static string State = "NSW";
        protected static string PCode = "1234";
        protected static ContactDetail ContactDetail = new ContactDetail(AddressLine1, AddressLine2, AddressLine3, Suburb, State, PCode, Country, Phone);

        //additional user
        protected static User _user;
        protected static string _firstName = "UserFirstName";
        protected static string _lastName = "UserLastName";
        protected static string _mobile = "UserMobile";
        protected static string _phone = "UserPhone";
        protected static string _email = "User@email.com";
        protected static string _password = "password";

        //product to test
        protected static Guid _productId;
        protected static string _productName = "ProductName";

        //features
        protected static string _featureCode1 = "FeatureCode1";
        protected static string _featureName1 = "FeatureName1";
        protected static string _featureDesc1 = "FeatureDesc1";

        protected static string _featureCode2 = "FeatureCode1";

        private static void CreateUser(ref Guid userId)
        {
            userId = Guid.NewGuid();
            _user = new User(userId, _firstName, _lastName, _email, _password);
            Repository.Save(_user, Guid.NewGuid(), null);
        }

        private static void CreateAccount()
        {
            CreateUser(ref _adminId);
            _accountId = Guid.NewGuid();
            _account = new Account(_accountId, _companyName, ContactDetail.Phone,ContactDetail.Country);
            Repository.Save(_account, Guid.NewGuid(), null);
        }

        private static void CreateProduct(ref Guid productId)
        {
            productId = Guid.NewGuid();
            Product p = new Product(productId, _productName);
            p.AddFeature(_featureCode1, _featureName1, _featureDesc1);
            Repository.Save(p, Guid.NewGuid(), null);
        }

        [Subject("Creating account, should raise event")]
        public class CreatingAccount
        {
            protected static AccountCreated _event;

            Establish context = () =>
            {
                Setup();
                CreateUser(ref _adminId);
                _accountId = Guid.NewGuid();
            };
            Because of = () =>
            {
                _account = new Account(_accountId, _companyName, ContactDetail.Phone,ContactDetail.Country);
                Repository.Save(_account, Guid.NewGuid());
                _event = Commit.Events.Select(x => x.Body).OfType<AccountCreated>().First();
            };

            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It accountIdShouldEqual = () => _event.AccountId.ShouldEqual(_accountId);
            It companyNameShouldEqual = () => _event.CompanyName.ShouldEqual(_companyName);
            It countryNameShouldEqual = () => _event.Country.ShouldEqual(Country);
            It phoneShouldEqual = () => _event.Phone.ShouldEqual(Phone);
        }

        [Subject("Creating account with empty account id, should raise exception")]
        public class CreatingAccountWithEmptyAccountId
        {
            static Exception _exception;
            Establish context = () => Setup();
            Because of = () =>
            {
                _exception = Catch.Exception(() => _account = new Account(Guid.Empty, _companyName, ContactDetail.Phone,ContactDetail.Country));
            };
            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Authorize existing user to account, should raise event")]
        public class AuthorizeExistingUserToAccount
        {
            static Guid _additionalUserId;
            static UserAuthorized _event;
            Establish context = () =>
                                    {
                                        Setup();
                                        CreateAccount();
                                        _additionalUserId = Guid.NewGuid();
                                        CreateUser(ref _additionalUserId);
                                    };
            Because of = () =>
            {
                _account.AuthorizeUser(_additionalUserId);
                Repository.Save(_account, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<UserAuthorized>().First();
            };
            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It userIdShouldEqual = () => _event.UserId.ShouldEqual(_additionalUserId);
            It accountIdShouldEqual = () => _event.AccountId.ShouldEqual(_accountId);
            It shouldUpdateAuthorizedUsers = () => _account.AuthorizedUsers.Contains(_additionalUserId).ShouldBeTrue();
        }

        [Subject("Authorize non existent user to account, should raise exception")]
        public class AuthorizeNotExistingUserToAccount
        {
            static Exception _exception;
            Establish context = () =>
            {
                Setup();
                CreateAccount();
            };
            Because of = () =>
            {
                _exception = Catch.Exception(() => _account.AuthorizeUser(Guid.NewGuid()));
            };
            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Revoking existing user from account, should raise event")]
        public class RevokingExistingUserFromAccount
        {
            static Guid _additionalUserId;
            static UserRevoked _event;
            Establish context = () =>
            {
                Setup();
                CreateAccount();
                _additionalUserId = Guid.NewGuid();
                CreateUser(ref _additionalUserId);
                _account.AuthorizeUser(_additionalUserId);
                Repository.Save(_account, Guid.NewGuid(), null);
            };
            Because of = () =>
            {
                _account.RevokeUser(_additionalUserId);
                Repository.Save(_account, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<UserRevoked>().First();
            };
            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It userIdShouldEqual = () => _event.UserId.ShouldEqual(_additionalUserId);
            It accountIdShouldEqual = () => _event.AccountId.ShouldEqual(_accountId);
            It shouldUpdateAuthorizedUsers = () => _account.AuthorizedUsers.Contains(_additionalUserId).ShouldBeFalse();
        }

        [Subject("Revoking non authorized user from account, should raise exception")]
        public class RevokingNotAuthorizedUserFromAccount
        {
            static Exception _exception;
            Establish context = () =>
            {
                Setup();
                CreateAccount();
            };
            Because of = () =>
            {
                _exception = Catch.Exception(() => _account.RevokeUser(Guid.NewGuid()));
            };
            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Inviting user for first time, should raise event")]
        public class InvitingUserForFirstTime
        {
            static string _invitedUserEmail = "email@email.com";
            static UserInvited _event;
            Establish context = () =>
            {
                Setup();
                CreateAccount();
            };
            Because of = () =>
            {
                _account.InviteUser(_invitedUserEmail);
                Repository.Save(_account, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<UserInvited>().First();
            };
            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It emailShouldEqual = () => _event.Email.ShouldEqual(_invitedUserEmail);
            It accountIdShouldEqual = () => _event.AccountId.ShouldEqual(_accountId);
            It shouldAddInvitationToList = () => _account.Invitations.InvitationExists(_invitedUserEmail).ShouldBeTrue();
            It shouldUpdateInvStatus = () => _account.Invitations.GetInvitationStatus(_invitedUserEmail).ToString().ShouldEqual(Account.InvitationStatus.Pending.ToString());
        }

        [Subject("Reinviting user after timeout, should raise event")]
        public class ReinvitingUserAfterTimeout
        {
            static string _invitedUserEmail = "email@email.com";
            static UserInvited _event;
            Establish context = () =>
            {
                Setup();
                CreateAccount();
                _account.InviteUser(_invitedUserEmail);
                _account.ChangeInvitationStatus(_invitedUserEmail, Account.InvitationStatus.Timeouted);
                Repository.Save(_account, Guid.NewGuid(), null);
            };
            Because of = () =>
            {
                _account.InviteUser(_invitedUserEmail);
                Repository.Save(_account, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<UserInvited>().First();
            };
            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It emailShouldEqual = () => _event.Email.ShouldEqual(_invitedUserEmail);
            It accountIdShouldEqual = () => _event.AccountId.ShouldEqual(_accountId);
            It shouldAddInvitationToList = () => _account.Invitations.InvitationExists(_invitedUserEmail).ShouldBeTrue();
            It shouldUpdateInvStatus = () => _account.Invitations.GetInvitationStatus(_invitedUserEmail).ToString().ShouldEqual(Account.InvitationStatus.Pending.ToString());
        }

        [Subject("Inviting user twice, should raise exception")]
        public class InvitingUserTwice
        {
            static string _invitedUserEmail = "email@email.com";
            static Exception _exception;
            Establish context = () =>
            {
                Setup();
                CreateAccount();
                _account.InviteUser(_invitedUserEmail);
                Repository.Save(_account, Guid.NewGuid(), null);
            };
            Because of = () =>
            {
                _exception = Catch.Exception(() => _account.InviteUser(_invitedUserEmail));
            };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Changing invitation status, should raise event")]
        public class ChangingInvitationStatus
        {
            static string _invitedUserEmail = "email@email.com";
            static InvitationStatusChanged _event;
            static Account.InvitationStatus status = Account.InvitationStatus.BouncedBack;
            Establish context = () =>
            {
                Setup();
                CreateAccount();
                _account.InviteUser(_invitedUserEmail);
                Repository.Save(_account, Guid.NewGuid(), null);
            };
            Because of = () =>
            {
                _account.ChangeInvitationStatus(_invitedUserEmail, status);
                Repository.Save(_account, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<InvitationStatusChanged>().First();
            };
            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It emailShouldEqual = () => _event.UserEmail.ShouldEqual(_invitedUserEmail);
            It statusShouldEqual = () => _event.InvitationStatus.ShouldEqual(Account.InvitationStatus.BouncedBack.ToString());
            It accountIdShouldEqual = () => _event.AccountId.ShouldEqual(_accountId);
            It invitationShouldExist = () => _account.Invitations.InvitationExists(_invitedUserEmail).ShouldBeTrue();
            It shouldUpdateInvStatus = () => _account.Invitations.GetInvitationStatus(_invitedUserEmail).ToString().ShouldEqual(status.ToString());
        }


        [Subject("Changing status of non-existent invitation, should raise exception")]
        public class ChangingStatusOfNonExistingInvitation
        {
            static string _invitedUserEmail = "email@email.com";
            static Exception _exception;
            Establish context = () =>
            {
                Setup();
                CreateAccount();
                _account.InviteUser(_invitedUserEmail);
                Repository.Save(_account, Guid.NewGuid(), null);
            };
            Because of = () =>
            {
                _exception = Catch.Exception(() => _account.ChangeInvitationStatus("notInvitedEmail@email.com", Account.InvitationStatus.Completed));
            };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Subscribing for product, should raise event")]
        public class SubscribingForExistingProduct
        {
            static SubscriptionCreated _event;
            Establish context = () =>
            {
                Setup();
                CreateAccount();
                CreateProduct(ref _productId);
            };
            Because of = () =>
            {
                _account.Subscribe(_productId);
                Repository.Save(_account, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<SubscriptionCreated>().First();
            };

            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It accountIdShouldEqual = () => _event.AccountId.ShouldEqual(_accountId);
            It productIdShouldEqual = () => _event.ProductId.ShouldEqual(_productId);
            It createdOnShoulntBeEmpty = () => _event.SubscribedOnUTC.ShouldNotEqual(DateTime.MinValue);
            It shouldSetCreationDate = () => _event.SubscribedOnUTC.ShouldBeLessThanOrEqualTo(DateTime.UtcNow);
            It shouldUpdateSubscriptionsList = () => _account.SubscribedProductFeatures.ContainsKey(_productId).ShouldBeTrue();
        }

        [Subject("Subscribing for non existent product, should raise exception")]
        public class SubscribingForNonExistentProduct
        {
            static Exception _exception;
            Establish context = () =>
            {
                Setup();
                CreateAccount();
                CreateProduct(ref _productId);
            };
            Because of = () =>
            {
                _exception = Catch.Exception(() => _account.Subscribe(Guid.NewGuid()));
            };
            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Subscribing twice for existing product, should raise exception")]
        public class SubscribingTwiceForExistingProduct
        {
            static Exception _exception;
            Establish context = () =>
            {
                Setup();
                CreateAccount();
                CreateProduct(ref _productId);
                _account.Subscribe(_productId);
            };
            Because of = () =>
            {
                _exception = Catch.Exception(() => _account.Subscribe(_productId));
            };
            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Unsubscribing from product, should raise event")]
        public class UnsubscribingFromProduct
        {
            static SubscriptionTerminated _event;
            Establish context = () =>
            {
                Setup();
                CreateAccount();
                CreateProduct(ref _productId);
                _account.Subscribe(_productId);
                Repository.Save(_account, Guid.NewGuid(), null);
            };
            Because of = () =>
            {
                _account.Unsubscribe(_productId);
                Repository.Save(_account, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<SubscriptionTerminated>().First();
            };

            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It accountIdShouldEqual = () => _event.AccountId.ShouldEqual(_accountId);
            It productIdShouldEqual = () => _event.ProductId.ShouldEqual(_productId);
            It terminatedOnShoulntBeEmpty = () => _event.TerminatedOn.ShouldNotEqual(DateTime.MinValue);
            It shouldSetTerminatedDate = () => _event.TerminatedOn.ShouldBeLessThanOrEqualTo(DateTime.UtcNow);
            It shouldUpdateSubscriptionsList = () => _account.SubscribedProductFeatures.ContainsKey(_productId).ShouldBeFalse();
        }

        [Subject("Unsubscribing from not subscribed product, should raise exception")]
        public class UnsubscribingFromNotSubscribedProduct
        {
            static Exception _exception;
            static Guid anotherProductId;
            Establish context = () =>
            {
                Setup();
                CreateAccount();
                CreateProduct(ref _productId);
                CreateProduct(ref anotherProductId);
            };
            Because of = () =>
            {
                _exception = Catch.Exception(() => _account.Unsubscribe(anotherProductId));
            };
            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Adding feature for product, should raise event")]
        public class AddingFeatureForSubscribedProduct
        {
            static AddedFeaturesToSubscription _event;
            Establish context = () =>
            {
                Setup();
                CreateProduct(ref _productId);
                CreateAccount();
                _account.Subscribe(_productId);
                Repository.Save(_account, Guid.NewGuid(), null);
            };
            Because of = () =>
            {
                _account.AddFeaturesToSubsctiption(_productId, new List<string>() { _featureCode1 });
                Repository.Save(_account, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<AddedFeaturesToSubscription>().First();
            };
            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It accountIdShouldEqual = () => _event.AccountId.ShouldEqual(_accountId);
            It productIdShouldEqual = () => _event.ProductId.ShouldEqual(_productId);
            It featureCodeShouldEqual = () => _event.Features.First().ShouldEqual(_featureCode1);
            It shouldUpdateSubscList = () => _account.SubscribedProductFeatures.ContainsKey(_productId).ShouldBeTrue();
            It featureShouldBeUpdated = () => _account.SubscribedProductFeatures[_productId].Contains(_featureCode1);
        }

        [Subject("Adding feature for non existing product, should throw exception")]
        public class AddingFeatureForNonExistingProduct
        {
            static Exception _exception;
            Establish context = () =>
            {
                Setup();
                CreateProduct(ref _productId);
                CreateAccount();
                _account.Subscribe(_productId);
                Repository.Save(_account, Guid.NewGuid(), null);
            };
            Because of = () =>
            {
                _exception = Catch.Exception(() => _account.AddFeaturesToSubsctiption(Guid.NewGuid(), new List<string>() { _featureCode1 }));
            };
            It shouldThrowException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Adding non existing feature for existing product, should throw exception")]
        public class AddingNonExistingFeatureExistingProduct
        {
            static Exception _exception;
            Establish context = () =>
            {
                Setup();
                CreateProduct(ref _productId);
                CreateAccount();
                _account.Subscribe(_productId);
                Repository.Save(_account, Guid.NewGuid(), null);
            };
            Because of = () =>
            {
                _exception = Catch.Exception(() => _account.AddFeaturesToSubsctiption(_productId, new List<string>() { "NonExistingFeature" }));
            };
            It shouldThrowException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Adding non existing feature for existing product, should throw exception")]
        public class AddingExistingFeatureUnsubscribedProduct
        {
            static Exception _exception;
            Establish context = () =>
            {
                Setup();
                CreateProduct(ref _productId);
                CreateAccount();
                Repository.Save(_account, Guid.NewGuid(), null);
            };
            Because of = () =>
            {
                _exception = Catch.Exception(() => _account.AddFeaturesToSubsctiption(_productId, new List<string>() { _featureCode1 }));
            };
            It shouldThrowException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Adding already subscribed feature for existing product, should throw exception")]
        public class AddingAlreadySubscribedFeatureProduct
        {
            static Exception _exception;
            Establish context = () =>
            {
                Setup();
                CreateProduct(ref _productId);
                CreateAccount();
                _account.Subscribe(_productId);
                _account.AddFeaturesToSubsctiption(_productId, new List<string>() { _featureCode1 });
                Repository.Save(_account, Guid.NewGuid(), null);
            };
            Because of = () =>
            {
                _exception = Catch.Exception(() => _account.AddFeaturesToSubsctiption(_productId, new List<string>() { _featureCode1 }));
            };
            It shouldThrowException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Removing feature for product, should raise event")]
        public class RemovingFeatureForSubscribedProduct
        {
            static RemovedFeaturesFromSubscription _event;
            Establish context = () =>
            {
                Setup();
                CreateProduct(ref _productId);
                CreateAccount();
                _account.Subscribe(_productId);
                _account.AddFeaturesToSubsctiption(_productId, new List<string>() { _featureCode1 });
                Repository.Save(_account, Guid.NewGuid(), null);
            };
            Because of = () =>
            {
                _account.RemoveFeaturesFromSubsctiption(_productId, new List<string>() { _featureCode1 });
                Repository.Save(_account, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<RemovedFeaturesFromSubscription>().First();
            };
            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It accountIdShouldEqual = () => _event.AccountId.ShouldEqual(_accountId);
            It productIdShouldEqual = () => _event.ProductId.ShouldEqual(_productId);
            It featureCodeShouldEqual = () => _event.Features.First().ShouldEqual(_featureCode1);
            It productShouldBeStillSubscribed = () => _account.SubscribedProductFeatures.ContainsKey(_productId).ShouldBeTrue();
            It featureShouldBeUnsubscribed = () => _account.SubscribedProductFeatures[_productId].Contains(_featureCode1).ShouldBeFalse();
        }

        [Subject("Removing feature for non existing product, should throw exception")]
        public class RemovingFeatureForNonExistingProduct
        {
            static Exception _exception;
            Establish context = () =>
            {
                Setup();
                CreateProduct(ref _productId);
                CreateAccount();
                _account.Subscribe(_productId);
                Repository.Save(_account, Guid.NewGuid(), null);
            };
            Because of = () =>
            {
                _exception = Catch.Exception(() => _account.RemoveFeaturesFromSubsctiption(Guid.NewGuid(), new List<string>() { _featureCode1 }));
            };
            It shouldThrowException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Removing non existing feature for existing product, should throw exception")]
        public class RemovingNonExistingFeatureSubscribedExistingProduct
        {
            static Exception _exception;
            Establish context = () =>
            {
                Setup();
                CreateProduct(ref _productId);
                CreateAccount();
                _account.Subscribe(_productId);
                Repository.Save(_account, Guid.NewGuid(), null);
            };
            Because of = () =>
            {
                _exception = Catch.Exception(() => _account.RemoveFeaturesFromSubsctiption(_productId, new List<string>() { "NonExistingFeature" }));
            };
            It shouldThrowException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Removing non existing feature for existing product, should throw exception")]
        public class RemovingExistingFeatureUnsubscribedProduct
        {
            static Exception _exception;
            Establish context = () =>
            {
                Setup();
                CreateProduct(ref _productId);
                CreateAccount();
                Repository.Save(_account, Guid.NewGuid(), null);
            };
            Because of = () =>
            {
                _exception = Catch.Exception(() => _account.RemoveFeaturesFromSubsctiption(_productId, new List<string>() { _featureCode1 }));
            };
            It shouldThrowException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Removing unsubscribed feature for existing product, should throw exception")]
        public class RemovingUnsubscribedFeatureProduct
        {
            static Exception _exception;
            Establish context = () =>
            {
                Setup();
                CreateProduct(ref _productId);
                CreateAccount();
                _account.Subscribe(_productId);
                Repository.Save(_account, Guid.NewGuid(), null);
            };
            Because of = () =>
            {
                _exception = Catch.Exception(() => _account.RemoveFeaturesFromSubsctiption(_productId, new List<string>() { _featureCode1 }));
            };
            It shouldThrowException = () => _exception.ShouldNotBeNull();
        }

    }
}
