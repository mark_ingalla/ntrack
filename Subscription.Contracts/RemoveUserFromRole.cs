﻿using System;

namespace Subscription.Contracts
{
    public class RemoveUserFromRole
    {
        public Guid UserID { get; set; }
        public Guid RoleID { get; set; }
    }
}
