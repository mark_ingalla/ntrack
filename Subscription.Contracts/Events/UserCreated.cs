using System;

namespace Subscription.Contracts.Events
{
    public class UserCreated
    {
        public Guid SagaId { get; set; }
        public Guid UserId { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
    }
}