using System;

namespace Subscription.Contracts.Events
{
    public class RoleCreated
    {
        public Guid SagaId { get; set; }
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public Guid UserId { get; set; }
    }
}