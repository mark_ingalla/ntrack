﻿using System;

namespace Subscription.Contracts.Events
{
    public class Registered
    {
        public Guid AccountID { get; set; }
        public Guid AdminId { get; set; }

        public string CompanyName { get; set; }

        public string CompanyPhone { get; set; }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }

        public string CompanySubrub { get; set; }

        public string CompanyState { get; set; }

        public string CompanyPostcode { get; set; }

        public string CompanyCountry { get; set; }

        //string AdminFirstName { get; set; }        
        //string AdminLastName { get; set; }        
        //string AdminEmail { get; set; }
        //string AdminPhone { get; set; }
        //string AdminMobile { get; set; }        
        //string AdminPassword { get; set; }

        public string IPAddress { get; set; }
        public string EmailVerificationPageUrl { get; set; }
    }
}
