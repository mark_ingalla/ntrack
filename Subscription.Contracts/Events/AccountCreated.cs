using System;

namespace Subscription.Contracts.Events
{
    public class AccountCreated
    {
        public Guid SagaId { get; set; }
    }
}