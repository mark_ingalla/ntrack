﻿using System;

namespace Subscription.Contracts.Commands
{
    public class PrepareResetPassword
    {
        public Guid UserID { get; set; }
        public String UserEmail { get; set; }
        public Guid ResetPasswordToken { get; set; }
        public String ResetPasswordLink { get; set; }
    }
}
