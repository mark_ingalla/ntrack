﻿using System;

namespace Subscription.Contracts.Commands
{
    public class AddRolePermission
    {
        public Guid RoleID { get; set; }
        public Guid ProductID { get; set; }
        //public PermissionModel Permission { get; set; }
    }
}
