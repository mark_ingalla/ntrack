﻿using System;

namespace Subscription.Contracts.Commands
{
    public class LoginUser
    {
        public Guid SessionToken { get; set; }
        public Guid AccountId { get; set; }
    }
}
