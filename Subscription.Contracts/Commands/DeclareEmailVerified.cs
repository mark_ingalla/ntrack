﻿using System;

namespace Subscription.Contracts.Commands
{
    public class DeclareEmailVerified
    {
        public Guid EmailVerficationToken { get; set; }
    }
}
