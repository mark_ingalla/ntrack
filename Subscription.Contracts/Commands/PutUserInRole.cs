﻿using System;

namespace Subscription.Contracts.Commands
{
    public class PutUserInRole
    {
        public Guid UserID { get; set; }
        public Guid RoleID { get; set; }
    }
}
