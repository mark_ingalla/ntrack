using System;
using nTrack.Core.Messages;

namespace Subscription.Contracts.Commands
{
    public class CreateUser : MessageBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
        public Guid SagaId { get; set; }
    }
}