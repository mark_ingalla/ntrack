﻿using System;

namespace Subscription.Contracts.Commands
{
    public class AddFeatureToSubscription
    {
        public Guid SubscriptionID { get; set; }
        public Guid FeatureID { get; set; }
    }
}
