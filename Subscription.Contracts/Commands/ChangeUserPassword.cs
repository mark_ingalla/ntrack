﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Subscription.Contracts.Commands
{
    public class ChangeUserPassword
    {
        public Guid UserID { get; set; }
        [Required]
        public String NewPassword { get; set; }
    }
}
