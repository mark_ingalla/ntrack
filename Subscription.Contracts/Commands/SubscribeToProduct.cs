﻿using System;

namespace Subscription.Contracts.Commands
{
    public class SubscribeToProduct
    {
        public Guid SubscriptionID { get; set; }
        public Guid AccountID { get; set; }
        public Guid ProductID { get; set; }
        //public List<FeatureModel> Features { get; set; }
    }
}
