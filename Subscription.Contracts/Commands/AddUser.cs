﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Subscription.Contracts.Commands
{
    public class AddUser
    {
        public Guid AccountID { get; set; }

        [Required]
        public String FirstName { get; set; }
        [Required]
        public String LastName { get; set; }
        [Required]
        public String Email { get; set; }

        public String Phone { get; set; }
        public String Mobile { get; set; }
        [Required]
        public String Password { get; set; }

        [Required]
        public List<Guid> RoleIDs { get; set; }
    }
}
