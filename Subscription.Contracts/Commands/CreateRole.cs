using System;
using nTrack.Core.Messages;

namespace Subscription.Contracts.Commands
{
    public class CreateRole : MessageBase
    {
        public string RoleName { get; set; }
        public Guid SagaId { get; set; }
        public Guid UserId { get; set; }
    }
}