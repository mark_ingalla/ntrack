﻿using System;

namespace Subscription.Contracts.Commands
{
    public class AddRole
    {
        public Guid RoleID { get; set; }
        public string Name { get; set; }
        //public List<PermissionModel> Permissions { get; set; }
    }
}
