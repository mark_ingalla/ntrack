﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Subscription.Contracts.Commands
{
    public class Register
    {
        [Required]
        public Guid AccountID { get; set; }
        [Required]
        public string CompanyName { get; set; }
        [Required]
        public string CompanyPhone { get; set; }
        [Required]
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        [Required]
        public string CompanySubrub { get; set; }
        [Required]
        public string CompanyState { get; set; }
        [Required]
        public string CompanyPostcode { get; set; }
        [Required]
        public string CompanyCountry { get; set; }
        [Required]
        public string AdminFirstName { get; set; }
        [Required]
        public string AdminLastName { get; set; }
        [Required]
        public string AdminEmail { get; set; }
        public string AdminPhone { get; set; }
        public string AdminMobile { get; set; }
        [Required]
        public string AdminPassword { get; set; }
        [Required]
        public string IPAddress { get; set; }
        public string EmailVerificationPageUrl { get; set; }
    }
}
