﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Subscription.Contracts.Commands
{
    public class DoResetPassword
    {
        public Guid ResetPasswordToken { get; set; }

        [Required]
        public String NewPassword { get; set; }
    }
}
