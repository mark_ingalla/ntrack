﻿using System;

namespace Subscription.Contracts.Commands
{
    public class RemoveFeatureFromSubscription
    {
        public Guid SubscriptionID { get; set; }
        public Guid FeatureID { get; set; }
    }
}
