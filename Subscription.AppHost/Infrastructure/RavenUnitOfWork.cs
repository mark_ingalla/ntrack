﻿using System;
using log4net;
using NServiceBus.UnitOfWork;
using Raven.Client;
using Raven.Client.Document;

namespace Subscription.AppHost.Infrastructure
{
    public class RavenUnitOfWork : IManageUnitsOfWork
    {
        readonly ILog _log = LogManager.GetLogger(typeof(RavenUnitOfWork));
        readonly IDocumentSession _session;

        public RavenUnitOfWork(IDocumentSession session)
        {
            this._session = session;
        }

        public void Begin()
        {
            LogMessage("Begin - Noop ");//since we're leaning on the func support of StructureMap to create the session for us
        }


        public void End(Exception ex)
        {
            if (ex == null)
            {
                LogMessage("Commit - Saving changes to raven");
                _session.SaveChanges();
            }
            else
                LogMessage("Rollback - We don't need to do anything here");

        }


        void LogMessage(string message)
        {
            _log.Debug(string.Format("SId({0}) UoW - {1}", ((DocumentSession)_session).Id, message));
        }

    }
}
