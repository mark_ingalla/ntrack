﻿using System;
using System.Runtime.InteropServices;
using log4net;
using NServiceBus;
using nTrack.Extension;
using Raven.Abstractions.Exceptions;
using Raven.Client;

namespace Subscription.AppHost.Infrastructure
{
    [ComVisible(false)]
    public class RavenSessionFactory : IDisposable
    {
        static readonly ILog Logger = LogManager.GetLogger(typeof(RavenSessionFactory));

        public static Func<IMessageContext, string> GetDatabaseName = (Func<IMessageContext, string>)(context => "");
        [ThreadStatic]
        private static IDocumentSession session;

        public IDocumentStore Store { get; private set; }

        public IBus Bus { get; set; }

        public IDocumentSession Session
        {
            get
            {
                return session ?? (session = this.OpenSession());
            }
        }

        static RavenSessionFactory()
        {
        }

        public RavenSessionFactory(IDocumentStore store)
        {
            this.Store = store;
        }

        public void Dispose()
        {
            if (session == null)
                return;
            session.Dispose();
            session = (IDocumentSession)null;
        }

        private IDocumentSession OpenSession()
        {
            IMessageContext messageContext = (IMessageContext)null;
            if (this.Bus != null)
                messageContext = this.Bus.CurrentMessageContext;
            string database = GetDatabaseName(messageContext);
            IDocumentSession documentSession = !string.IsNullOrEmpty(database) ? this.Store.OpenSession(database) : this.Store.OpenSession();
            documentSession.Advanced.AllowNonAuthoritativeInformation = false;
            documentSession.Advanced.UseOptimisticConcurrency = true;

            Logger.Debug("Session opened: {0} for Database: {1}".Fill(documentSession.GetHashCode(), database));

            return documentSession;
        }

        public void SaveChanges()
        {
            if (session == null)
                return;
            try
            {
                session.SaveChanges();
            }
            catch (ConcurrencyException ex)
            {
                throw new ConcurrencyException("A saga with the same Unique property already existed in the storage. See the inner exception for further details", (Exception)ex);
            }
        }
    }
}