﻿using NServiceBus;
using NServiceBus.MessageMutator;
using NServiceBus.Unicast.Transport;
using System.Linq;

namespace Subscription.AppHost.Infrastructure
{
    public class OutgoingMessageMutator : IMutateOutgoingTransportMessages
    {
        readonly IBus _bus;
        public OutgoingMessageMutator(IBus bus)
        {
            _bus = bus;
        }

        public void MutateOutgoing(object[] messages, TransportMessage transportMessage)
        {
            if (_bus.CurrentMessageContext == null) return;

            var headers = _bus.CurrentMessageContext.Headers.Where(x => x.Key.StartsWith("nTrack"))
                              .ToDictionary(x => x.Key, y => y.Value);
            //also check of the message has AccountId property but not set on the header.

            if (!headers.ContainsKey("nTrack.AccountId"))
                foreach (var message in messages)
                {
                    var prop = message.GetType().GetProperties().SingleOrDefault(x => x.Name == "AccountId");
                    if (prop == null) continue;

                    var accountId = prop.GetValue(message, null);
                    if (accountId == null) continue;

                    headers.Add("nTrack.AccountId", accountId.ToString());
                    break;
                }

            foreach (var header in headers)
            {
                if (!transportMessage.Headers.ContainsKey(header.Key))
                    transportMessage.Headers.Add(header.Key, header.Value);
            }

        }
    }
}
