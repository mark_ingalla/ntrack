using System;
using System.Linq;
using EventStore.RavenDb;
using NServiceBus;

namespace Subscription.AppHost.Infrastructure
{
    public sealed class NSBMessageDispatcher : IDispatchCommits
    {
        readonly IBus bus;

        public NSBMessageDispatcher(IBus bus)
        {
            if (bus == null) throw new ArgumentNullException("bus");
            this.bus = bus;
        }

        public void Dispatch(Commit commit)
        {
            var messages = commit.Events.Select(e => e.Body).ToArray();

            foreach (var message in messages)
            {
                bus.Publish(message);
            }
        }

        public void Dispose()
        {

        }
    }
}