using Raven.Client;
using Raven.Client.Listeners;

namespace Subscription.AppHost.Infrastructure
{
    public class NoStaleQueriesListener : IDocumentQueryListener
    {
        public void BeforeQueryExecuted(IDocumentQueryCustomization queryCustomization)
        {
            queryCustomization.WaitForNonStaleResults();
        }
    }
}