﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.Configuration;
using System.Linq;
using System.Reflection;
using EventStore.RavenDb;
using EventStore.RavenDb.Persistence;
using NServiceBus;
using NServiceBus.MessageMutator;
using NServiceBus.UnitOfWork;
using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Indexes;
using StructureMap;
using Subscription.AppHost.Infrastructure;
using Subscription.Data;

namespace Subscription.AppHost
{
    public class EndpointConfig : IConfigureThisEndpoint, AsA_Publisher, IWantCustomInitialization
    {
        public void Init()
        {
            ObjectFactory.Configure(
                x =>
                {
                    x.Scan(
                        scan =>
                        {
                            scan.AssembliesFromApplicationBaseDirectory();
                            scan.TheCallingAssembly();
                            scan.LookForRegistries();
                            scan.WithDefaultConventions();
                        });

                    x.ForSingletonOf<IDocumentStore>().Use(SetupRavenDB);
                    x.ForSingletonOf<EventSource>()
                     .Use(ctx =>
                     {
                         return new EventSource(ctx.GetInstance<IDocumentStore>())
                             .UsingAsynchronousDispatchScheduler(new NSBMessageDispatcher(ctx.GetInstance<IBus>()))
                             .Initialize();
                     });

                    x.ForSingletonOf<RavenSessionFactory>()
                     .Use(ctx =>
                     {
                         var factory = new RavenSessionFactory(ctx.GetInstance<IDocumentStore>())
                         {
                             Bus = ctx.GetInstance<IBus>()
                         };
                         return factory;
                     });

                    x.ForSingletonOf<IMutateOutgoingTransportMessages>().Use<OutgoingMessageMutator>();

                    x.For<IRepository>().Use<RavenDbStoreRepository>();
                    x.For<IManageUnitsOfWork>().Use<RavenUnitOfWork>();
                    x.For<IDocumentSession>().Use(ctx => ctx.GetInstance<RavenSessionFactory>().Session);
                    x.For<IEventStoreSession>().Use(ctx => ctx.GetInstance<EventSource>().OpenSession());

                });

            Configure.With()
                .DefineEndpointName(ConfigurationManager.AppSettings["EndpointName"])
                .StructureMapBuilder(ObjectFactory.Container)
                .DefiningCommandsAs(type => type != null && type.Namespace != null && type.Namespace.Contains("Messages.Commands"))
                .DefiningEventsAs(type => type != null && type.Namespace != null && type.Namespace.Contains("Messages.Events"))
                .XmlSerializer()
                .MsmqTransport()
                .RunTimeoutManager()
                .UnicastBus()
                .CreateBus()
                .Start();

        }

        IDocumentStore SetupRavenDB()
        {
            var store = new DocumentStore { ConnectionStringName = "RavenDB" };
            store.RegisterListener(new UpdateDateListener());
            store.RegisterListener(new UpdateStringIdListener());
            store.ResourceManagerId = Guid.NewGuid();
            store.Initialize();

            CreateIndex(store);

            return store;
        }

        void CreateIndex(IDocumentStore store)
        {
            var indices = typeof(Account).Assembly.GetTypes().Where(x => x.Namespace != null && x.Name.EndsWith("Index"));

            IndexCreation.CreateIndexes(new CompositionContainer(new TypeCatalog(indices)), store);
            IndexCreation.CreateIndexes(Assembly.GetAssembly(typeof(nTrack.Data.Account)), store);
        }

    }
}
