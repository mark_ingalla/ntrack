﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using EventStore.RavenDb;
using EventStore.RavenDb.Persistence;
using log4net;
using NServiceBus;
using NServiceBus.MessageMutator;
using NServiceBus.UnitOfWork;
using nTrack.Core;
using nTrack.Data;
using PalletPlus.AppHost.Infrastructure;
using PalletPlus.Domain.Services;
using PalletPlus.Services.Suppliers;
using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Extensions;
using Raven.Client.Indexes;
using StructureMap;
using Account = PalletPlus.Data.Account;

namespace PalletPlus.AppHost
{
    public class EndpointConfig : IConfigureThisEndpoint, AsA_Publisher, IWantCustomInitialization
    {
        static readonly ILog Logger = LogManager.GetLogger(typeof(EndpointConfig));

        public void Init()
        {
            ObjectFactory.Configure(
                x =>
                {
                    x.Scan(
                        scan =>
                        {
                            scan.AssembliesFromApplicationBaseDirectory();
                            scan.TheCallingAssembly();
                            scan.LookForRegistries();
                            scan.WithDefaultConventions();
                            scan.AddAllTypesOf<ISupplierIntegration>();
                        });

                    x.ForSingletonOf<IDocumentStore>().Use(ClientDatabases);
                    x.ForSingletonOf<EventSource>()
                     .Use(ctx =>
                         {
                             EventSource.DatabaseName = () =>
                                 {
                                     return GetClientDatabase(ctx);
                                 };
                             return new EventSource(ctx.GetInstance<IDocumentStore>())
                                 .UsingAsynchronousDispatchScheduler(new NSBMessageDispatcher(ctx.GetInstance<IBus>()))
                                 .WithConvertersFrom(Assembly.GetAssembly(typeof(Domain.Converters.SupplierCreatedConverter)))
                                 .Initialize();
                         });

                    x.ForSingletonOf<RavenSessionFactory>()
                     .Use(ctx =>
                     {
                         RavenSessionFactory.GetDatabaseName = context => context.Headers[nTrackConstants.DatabaseHeaderKey];

                         var factory = new RavenSessionFactory(ctx.GetInstance<IDocumentStore>())
                         {
                             Bus = ctx.GetInstance<IBus>()
                         };
                         return factory;
                     });

                    x.ForSingletonOf<IMutateOutgoingTransportMessages>().Use<OutgoingMessageMutator>();
                    x.ForSingletonOf<IMutateIncomingTransportMessages>().Use<IncommingMessageMutator>();
                    x.ForSingletonOf<ISystemDb>().Use<SystemDb>();
                    x.ForSingletonOf<DocketNumberService>().Use<DocketNumberService>();

                    x.For<IRepository>().Use<RavenDbStoreRepository>();
                    x.For<IManageUnitsOfWork>().Use<RavenUnitOfWork>();
                    x.For<IDocumentSession>().Use(ctx => ctx.GetInstance<RavenSessionFactory>().Session);
                    x.For<IEventStoreSession>().Use(ctx => ctx.GetInstance<EventSource>().OpenSession());

                });

            Configure.With()
                .DefineEndpointName(ConfigurationManager.AppSettings["EndpointName"])
                .StructureMapBuilder(ObjectFactory.Container)
                .DefiningCommandsAs(type => type != null && type.Namespace != null && type.Namespace.Contains("Messages.Commands"))
                .DefiningEventsAs(type => type != null && type.Namespace != null && type.Namespace.Contains("Messages.Events"))
                .DefiningMessagesAs(type => type != null && type.Namespace != null && type.Namespace.StartsWith("PalletPlus.Messages"))
                .XmlSerializer()
                .MsmqTransport()
                .RunTimeoutManager()
                .UnicastBus()
                .CreateBus()
                .Start();
        }

        string GetClientDatabase(IContext context)
        {
            var bus = context.GetInstance<IBus>();

            if (bus.CurrentMessageContext.Headers.ContainsKey(nTrackConstants.DatabaseHeaderKey))
                return bus.CurrentMessageContext.Headers[nTrackConstants.DatabaseHeaderKey];

            var systemDb = context.GetInstance<ISystemDb>();

            var accountId = bus.CurrentMessageContext.Headers.ContainsKey("nTrack.AccountId")
                                ? Guid.Parse(bus.CurrentMessageContext.Headers["nTrack.AccountId"])
                                : Guid.Empty;

            //TODO: if bus is null or no account id throw exception if its not a control message
            if (accountId == Guid.Empty)
            {
                throw new Exception("Received a message without an accountId");
            }

            var databaseName = systemDb.GetClientDatabaseName(accountId);
            return databaseName;
        }

        IDocumentStore ClientDatabases()
        {
            var store = new DocumentStore { ConnectionStringName = "ClientDB" };

            store.RegisterListener(new UpdateDateListener());
            store.RegisterListener(new UpdateStringIdListener());

            store.Initialize();

            store.ResourceManagerId = Guid.NewGuid();

            return store;
        }

        public static void CreateClientIndcies(string databaseName)
        {
            var clientStore = ObjectFactory.GetInstance<IDocumentStore>();
            var clientData = typeof(Account).Assembly.GetTypes()
                                                .Where(x =>
                                                       x.Namespace != null &&
                                                       x.Namespace.StartsWith("PalletPlus.Data.Indices") &&
                                                       x.Name.EndsWith("Index"))
                                                .ToList();

            var clientDomain = typeof(Domain.Models.Site).Assembly.GetTypes()
                                                .Where(x =>
                                                       x.Namespace != null &&
                                                       x.Namespace.StartsWith("PalletPlus.Domain.Indices") &&
                                                       x.Name.EndsWith("Index"))
                                                .ToList();

            var clientIndices = clientData.Union(clientDomain);

            var databaseCommands = clientStore.DatabaseCommands.ForDatabase(databaseName);
            Task.Factory.StartNew(
                () =>
                {
                    databaseCommands.EnsureDatabaseExists(databaseName);
                    IndexCreation.CreateIndexes(new CompositionContainer(new TypeCatalog(clientIndices)),
                                                databaseCommands, clientStore.Conventions);
                });
        }
    }
}
