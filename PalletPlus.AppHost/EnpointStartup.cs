﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NServiceBus;
using Raven.Client.Extensions;
using nTrack.Data;
using nTrack.Data.Indices;
using PalletPlus.Data;
using PalletPlus.Services.Suppliers;
using Raven.Client;
using StructureMap;

namespace PalletPlus.AppHost
{
    public class EnpointStartup : IWantToRunAtStartup
    {
        public void Run()
        {
            var supplierIntegrations = ObjectFactory.GetAllInstances<ISupplierIntegration>();
            var integrationNames = supplierIntegrations.Select(x => x.Name).ToArray();

            var clientStore = ObjectFactory.GetInstance<IDocumentStore>();
            var systemDb = ObjectFactory.GetInstance<ISystemDb>();

            foreach (var clientDatabase in GetClientDatabases(systemDb))
            {
                clientStore.DatabaseCommands.EnsureDatabaseExists(clientDatabase);
                EndpointConfig.CreateClientIndcies(clientDatabase);
                UpdateClientLookup(clientStore, clientDatabase, integrationNames);
            }

        }

        public void Stop()
        {
            var clientStore = ObjectFactory.GetInstance<IDocumentStore>();
            if (clientStore != null)
                clientStore.Dispose();

            var systemDb = ObjectFactory.GetInstance<ISystemDb>();
            if (systemDb.Store != null)
                systemDb.Store.Dispose();


        }

        string[] GetClientDatabases(ISystemDb systemDb)
        {
            using (var session = systemDb.Store.OpenSession())
            {
                var productCode = ConfigurationManager.AppSettings["ProductCode"];
                return session.Query<AccountDatabaseIndex.Result, AccountDatabaseIndex>()
                              .Where(x => x.ProductCode == productCode)
                              .Select(x => x.DatabaseName)
                              .ToArray();
            }
        }

        void UpdateClientLookup(IDocumentStore clientStore, string clientDatabase, IEnumerable<string> integrationNames)
        {
            using (var session = clientStore.OpenSession(clientDatabase))
            {
                var supplierLookup = session.Load<SupplierIntegrationLookup>(SupplierIntegrationLookup.LookupId);
                if (supplierLookup == null)
                {
                    supplierLookup = new SupplierIntegrationLookup();
                    session.Store(supplierLookup);
                }

                foreach (var name in integrationNames)
                {
                    if (!supplierLookup.Lookup.ContainsKey(name))
                        supplierLookup.Lookup.Add(name, name);
                }

                session.SaveChanges();
            }
        }
    }
}