﻿using System;
using System.Linq;
using NServiceBus;
using NServiceBus.Saga;
using PalletPlus.AppHost.Infrastructure;
using StructureMap;
using PalletPlus.Messages.Commands.Account;
using nTrack.Data;
using nTrack.Data.Indices;
using Subscription.Messages.Events;
using Subscription.Messages.Events.Account;

namespace PalletPlus.AppHost.Receptors
{
    public class NewSubscriptionController : Saga<NewSubscriotionData>,
        IAmStartedByMessages<SubscriptionCreated>,
        IAmStartedByMessages<DatabaseReady>
    {
        public override void ConfigureHowToFindSaga()
        {
            base.ConfigureMapping<SubscriptionCreated>(s => s.AccountId, m => m.AccountId);
            base.ConfigureMapping<DatabaseReady>(s => s.AccountId, m => m.AccountId);
        }

        public void Handle(DatabaseReady message)
        {
            Data.AccountId = message.AccountId;
            Data.IsDatabaseReady = true;

            EndpointConfig.CreateClientIndcies(message.DatabaseName);

            SagaCompleted();
        }
        public void Handle(SubscriptionCreated message)
        {
            Data.AccountId = message.AccountId;

            if (!Data.IsDatabaseReady) return;

            SagaCompleted();
        }

        void SagaCompleted()
        {
            var systemDb = ObjectFactory.GetInstance<ISystemDb>();
            using (var session = systemDb.Store.OpenSession())
            {
                var accountId = "accounts/" + Data.AccountId;
                var account = session.Query<AccountDatabaseIndex.Result, AccountDatabaseIndex>()
                                     .Single(x => x.AccountId == accountId);

                var cmd = new CreateAccount
                              {
                                  AccountId = Data.AccountId,
                                  CompanyName = account.CompanyName
                              };
                
                Bus.Send(cmd);
            }

            MarkAsComplete();
        }
    }

    public class NewSubscriotionData : ISagaEntity
    {
        public Guid Id { get; set; }
        public string Originator { get; set; }
        public string OriginalMessageId { get; set; }

        [Unique]
        public Guid AccountId { get; set; }

        public bool IsDatabaseReady { get; set; }
    }
}
