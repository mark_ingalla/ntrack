﻿using NServiceBus;
using Subscription.Messages.Events.Account;
using Subscription.Messages.Events.Account.User;

namespace PalletPlus.AppHost.Receptors
{
    public class SubscriptionReceptor :

        IHandleMessages<SubscriptionTerminated>,
        IHandleMessages<UserAdded>,
        IHandleMessages<UserDetailUpdated>,
        IHandleMessages<UserRemoved>,
        IHandleMessages<UserLocked>,
        IHandleMessages<UserUnlocked>,
        IHandleMessages<UserPasswordChanged>
    {
        public IBus Bus { get; set; }


        public SubscriptionReceptor()
        {
        }


        public void Handle(SubscriptionTerminated message)
        {

        }
        public void Handle(UserAdded message)
        {

        }
        public void Handle(UserDetailUpdated message)
        {

        }
        public void Handle(UserRemoved message)
        {

        }
        public void Handle(UserLocked message)
        {

        }
        public void Handle(UserUnlocked message)
        {
        }
        public void Handle(UserPasswordChanged message)
        {

        }


    }
}
