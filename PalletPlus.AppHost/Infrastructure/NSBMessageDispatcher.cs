using System;
using System.Collections.Generic;
using EventStore.RavenDb;
using NServiceBus;

namespace PalletPlus.AppHost.Infrastructure
{
    public sealed class NSBMessageDispatcher : IDispatchCommits
    {
        const string AggregateIdKey = "nTrack.AggregateId";
        const string CommitVersionKey = "nTrack.CommitVersion";
        readonly IBus _bus;

        public NSBMessageDispatcher(IBus bus)
        {
            if (bus == null) throw new ArgumentNullException("bus");
            _bus = bus;
        }

        public void Dispatch(Commit commit)
        {
            for (var i = 0; i < commit.Events.Count; i++)
            {
                var eventMessage = commit.Events[i];
                var busMessage = eventMessage.Body;
                
                AppendHeaders(busMessage, eventMessage.Headers);
                AppendVersion(commit, i);
                _bus.Publish(busMessage);
            }
        }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }


        static void AppendHeaders(object message, IEnumerable<KeyValuePair<string, object>> headers)
        {
            foreach (var header in headers)
            {
                var key = header.Key;
                var value = (header.Value ?? string.Empty).ToString();
                message.SetHeader(key, value);
            }
        }
        static void AppendVersion(Commit commit, int index)
        {
            var busMessage = commit.Events[index].Body;
            busMessage.SetHeader(AggregateIdKey, commit.AggregateId.ToString());
            busMessage.SetHeader(CommitVersionKey, commit.CommitSequence.ToString());
        }
    }
}