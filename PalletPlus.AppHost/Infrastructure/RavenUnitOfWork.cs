﻿using System;
using log4net;
using NServiceBus.UnitOfWork;
using nTrack.Extension;

namespace PalletPlus.AppHost.Infrastructure
{
    public class RavenUnitOfWork : IManageUnitsOfWork, IDisposable
    {
        readonly RavenSessionFactory _factory;
        static readonly ILog Logger = LogManager.GetLogger(typeof(RavenUnitOfWork));

        public RavenUnitOfWork(RavenSessionFactory factory)
        {
            _factory = factory;
        }

        public void Begin()
        {
            Logger.Debug("unit of work started: {0}".Fill(GetHashCode()));
        }
        public void End(Exception ex)
        {
            if (ex == null)
            {
                Logger.Debug("unit of work saving changes");
                _factory.SaveChanges();
            }

            _factory.Dispose();

            Logger.Debug("unit of work ended: {0}".Fill(GetHashCode()));
        }
        public void Dispose()
        {
            if (_factory != null)
                _factory.Dispose();
        }
    }
}
