﻿using System;
using log4net;
using NServiceBus.MessageMutator;
using NServiceBus.Unicast.Transport;
using nTrack.Data;

namespace PalletPlus.AppHost.Infrastructure
{
    public class IncommingMessageMutator : IMutateIncomingTransportMessages
    {
        static readonly ILog Logger = LogManager.GetLogger(typeof(IncommingMessageMutator));

        readonly ISystemDb _systemDb;

        public IncommingMessageMutator(ISystemDb systemDb)
        {
            _systemDb = systemDb;
        }

        public void MutateIncoming(TransportMessage transportMessage)
        {
            Logger.Debug("Incoming Mutator called!");

            if (transportMessage.Headers.ContainsKey("NServiceBus.ControlMessage") &&
                bool.Parse(transportMessage.Headers["NServiceBus.ControlMessage"]))
                return;

            if (transportMessage.Headers.ContainsKey("nTrack.DatabaseName"))
                return;

            var accountId = transportMessage.Headers.ContainsKey("nTrack.AccountId")
                                ? Guid.Parse(transportMessage.Headers["nTrack.AccountId"])
                                : Guid.Empty;

            if (accountId == Guid.Empty)
            {
                throw new Exception("Received a message without an accountId");
            }

            var databaseName = _systemDb.GetClientDatabaseName(accountId);
            transportMessage.Headers.Add("nTrack.DatabaseName", databaseName);
        }

    }
}