﻿using System.Collections.Generic;
using System.Linq;
using log4net;
using NServiceBus;
using NServiceBus.MessageMutator;
using NServiceBus.Unicast.Transport;

namespace PalletPlus.AppHost.Infrastructure
{
    public class OutgoingMessageMutator : IMutateOutgoingTransportMessages
    {
        readonly IBus _bus;
        static readonly ILog Logger = LogManager.GetLogger(typeof(OutgoingMessageMutator));

        public OutgoingMessageMutator(IBus bus)
        {
            _bus = bus;
        }

        public void MutateOutgoing(object[] messages, TransportMessage transportMessage)
        {
            Logger.Debug("Outgoing Mutator called.");

            if (_bus.CurrentMessageContext == null) return;

            var headers = _bus.CurrentMessageContext.Headers.Where(x => x.Key.StartsWith("nTrack"))
                              .ToDictionary(x => x.Key, y => y.Value);

            foreach (var header in headers)
            {
                if (!transportMessage.Headers.ContainsKey(header.Key))
                    transportMessage.Headers.Add(header.Key, header.Value);
            }

        }
    }
}
