﻿using System;
using System.Configuration;
using System.Linq;
using nTrack.Data;
using nTrack.Data.Extenstions;
using nTrack.Data.Indices;
using Raven.Client;
using Raven.Client.Document;

namespace PalletPlus.AppHost.Infrastructure
{
    public class SystemDb : ISystemDb
    {
        public SystemDb()
        {
            Store = SystemDatabase();
        }

        IDocumentStore SystemDatabase()
        {
            var store = new DocumentStore { ConnectionStringName = "SystemDB" };

            store.RegisterListener(new UpdateDateListener());
            store.RegisterListener(new UpdateStringIdListener());

            store.Initialize();

            store.ResourceManagerId = Guid.NewGuid();

            return store;
        }

        public IDocumentStore Store { get; private set; }
        public string GetClientDatabaseName(Guid accountId)
        {
            using (var session = Store.OpenSession())
            {
                var productCode = ConfigurationManager.AppSettings["ProductCode"];
                var accountIdString = accountId.ToStringId<Account>();
                var db = session.Query<AccountDatabaseIndex.Result, AccountDatabaseIndex>()
                                   .SingleOrDefault(x => x.AccountId == accountIdString && x.ProductCode == productCode);

                if (db == null)
                    throw new Exception("Unable to find database for this account!");

                return db.DatabaseName;
            }
        }
    }
}
