using System;
using PalletPlus.Data;
using Raven.Client.Listeners;
using Raven.Json.Linq;
using nTrack.Data;

namespace PalletPlus.AppHost.Infrastructure
{
    public class UpdateDateListener:IDocumentStoreListener
    {
        public bool BeforeStore(string key, object entityInstance, RavenJObject metadata, RavenJObject original)
        {
            var entity = entityInstance as BaseModel;
            if(entity!=null)
            {
                entity.UpdatedOn = DateTime.UtcNow;
            }

            return true;
        }

        public void AfterStore(string key, object entityInstance, RavenJObject metadata)
        {
            
        }
    }
}