﻿namespace Subscription.Reports
{
    public class SummaryVM
    {
        public decimal Subtotal { get; set; }
        public decimal GST { get { return Subtotal * 0.1M; } }
        public decimal Total { get { return Subtotal + GST; } }
        public decimal Paid { get; set; }
        public decimal BalanceDue { get { return Total - Paid; } }
    }
}