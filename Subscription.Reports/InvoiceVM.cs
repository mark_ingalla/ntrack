﻿using System.Collections.Generic;

namespace Subscription.Reports
{
    public class InvoiceVM
    {
        public CustomerVM Customer { get; set; }
        public List<InvoiceItemVM> InvoiceItems { get; set; }
        public string NTrackAddress { get; set; }
        public InfoVM Info { get; set; }
        public string Comments { get; set; }
        public string Footer { get; set; }
        public SummaryVM Summary { get; set; }

        public InvoiceVM()
        {
            NTrackAddress = "nTrack Pty Ltd\n" +
                            "Level 2, 608 Harris Street\n" +
                            "Ultimo NSW 2007\n";
        }
    }
}
