﻿namespace Subscription.Reports
{
    public class InvoiceItemVM
    {
        public string Name { get; set; }
        public int Quantity { get; set; }
        public decimal Rate { get; set; }
        public decimal Amount { get { return Quantity * Rate; } }
    }
}