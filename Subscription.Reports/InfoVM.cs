﻿namespace Subscription.Reports
{
    public class InfoVM
    {
        public int InvoiceNumber { get; set; }
        public string Date { get; set; }
        public string Terms { get; set; }
        public string DueDate { get; set; }
        public string NTrackABN { get; set; }
    }
}