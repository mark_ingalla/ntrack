﻿namespace Subscription.Reports
{
    public class CustomerVM
    {
        public string FullName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
    }
}