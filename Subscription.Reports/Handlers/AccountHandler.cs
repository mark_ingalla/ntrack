﻿using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using NServiceBus;
using Raven.Client;
using Subscription.Data;
using Subscription.Messages.Events.Account;
using System.IO;
using Telerik.Reporting;
using Telerik.Reporting.Processing;
using Report = Telerik.Reporting.Report;

namespace Subscription.Reports.Handlers
{
    public class AccountHandler :
        IHandleMessages<InvoiceRaised>
    {
        private IDocumentSession DbSession;

        public AccountHandler(IDocumentSession dbSession)
        {
            DbSession = dbSession;
        }

        public void Handle(InvoiceRaised message)
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-AU");
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-AU");

            Report report = new Invoice();
            var account = DbSession.Load<Account>(message.AccountId);
            var customer = new CustomerVM()
                               {
                                   AddressLine1 = account.AddressLine1 ?? "",
                                   AddressLine2 = account.AddressLine2 ?? "",
                                   FullName = account.CompanyName ?? "",
                                   Postcode = account.Postcode ?? "",
                                   State = account.State ?? ""
                               };

            var invoice = DbSession.Load<Data.Invoice>(message.InvoiceId);

            List<InvoiceItemVM> invoiceItemVms = new List<InvoiceItemVM>();

            foreach (var detail in invoice.Details)
            {
                invoiceItemVms.Add(new InvoiceItemVM()
                                       {
                                           Name = detail.LineDescription,
                                           Quantity = 1,
                                           Rate = detail.LineTotal
                                       });
            }

            SummaryVM summary = new SummaryVM()
                                    {
                                        Subtotal = invoiceItemVms.Sum(p => p.Amount),
                                        Paid = 0
                                    };

            InfoVM info = new InfoVM()
                              {
                                  Date = message.InvoiceDateUTC.ToShortDateString(),
                                  DueDate = message.InvoiceDateUTC.AddDays(30).ToShortDateString(),
                                  InvoiceNumber = message.InvoiceNumber,
                                  NTrackABN = "69 137 638 521",
                                  Terms = "Net 00",
                              };

            var invoiceVM = new InvoiceVM()
                                {
                                    Info = info,
                                    Customer = customer,
                                    InvoiceItems = invoiceItemVms,
                                    Comments = "Some comment",
                                    Footer = "http://www.ntrack.com.au/",
                                    Summary = summary
                                };

            report.DataSource = invoiceVM;
            var fileName = "Invoice_" + message.InvoiceNumber + ".pdf";
            CreateAndSaveInvoice(report, fileName);
            invoice.FileName = fileName;
            DbSession.SaveChanges();
        }

        private byte[] CreateAndSaveInvoice(Report report, string fileName)
        {
            var reportProcessor = new ReportProcessor();
            var instanceReportSource = new InstanceReportSource();

            instanceReportSource.ReportDocument = report;

            var result = reportProcessor.RenderReport("PDF", instanceReportSource, null).DocumentBytes;

            using (var ms = new MemoryStream(result))
            {
                //TODO: store invoices to RavenDb nTrack Database

                //string path = ConfigurationManager.AppSettings["InvoiceFilesPath"];

                //if (!Directory.Exists(path))
                //    Directory.CreateDirectory(path);

                
                //path += "/" + fileName;
                //FileStream file = new FileStream(path, FileMode.Create, System.IO.FileAccess.Write);
                //byte[] bytes = new byte[ms.Length];
                //ms.Read(bytes, 0, (int)ms.Length);
                //file.Write(bytes, 0, bytes.Length);
                //file.Close();
                //ms.Close();
            }



            return result;
        }
    }
}
