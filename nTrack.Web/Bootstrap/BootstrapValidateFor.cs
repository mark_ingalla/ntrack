﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace nTrack.Web.Bootstrap
{
    public enum AlertType
    {
        Success,
        Error,
        Info,
        None
    }

    public static partial class Bootstrap
    {
        public static MvcHtmlString ValidationMessageFor<TModel, TValue>(HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            var ret = html.ValidationMessageFor(expression);

            return ret;
        }


        public static MvcHtmlString Alert(object alerts)
        {
            List<Alert> alertsList = alerts as List<Alert>;

            if (alertsList == null) return null;

            TagBuilder html = new TagBuilder("div");

            html.AddCssClass("alerts");

            foreach (var alert in alertsList)
            {
                if (alert != null)
                {

                    TagBuilder tag = new TagBuilder("div");

                    switch (alert.Type)
                    {
                        case AlertType.Success:
                            tag.AddCssClass("alert alert-success");
                            break;
                        case AlertType.Error:
                            tag.AddCssClass("alert alert-error");
                            break;
                        case AlertType.Info:
                            tag.AddCssClass("alert alert-info");
                            break;
                    }

                    tag.SetInnerText(alert.Message);

                    html.InnerHtml += tag.ToString(TagRenderMode.Normal);
                }
            }

            return MvcHtmlString.Create(html.ToString(TagRenderMode.Normal));
        }


        public static MvcHtmlString Alert(object innerText, object type)
        {
            if (type == null) return null;

            TagBuilder tag = new TagBuilder("div");
            
            switch((AlertType)type)
            {
                case AlertType.Success:
                    tag.AddCssClass("alert alert-success");
                    break;
                case AlertType.Error:
                    tag.AddCssClass("alert alert-error");
                    break;
                case AlertType.Info:
                    tag.AddCssClass("alert alert-info");
                    break;
            }
            
            tag.SetInnerText((string)innerText);

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }
    }
}