﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace nTrack.Web.Bootstrap
{
    public static partial class Bootstrap
    {
        public static MvcHtmlString LabelFor<TModel, TValue>(HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, object htmlAttributes = null)
        {
            return LabelFor(html, expression, new RouteValueDictionary(htmlAttributes));
        }

        public static MvcHtmlString LabelFor<TModel, TValue>(HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, IDictionary<string, object> htmlAttributes)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            string htmlFieldName = ExpressionHelper.GetExpressionText(expression);
            string labelText = metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();
            if (String.IsNullOrEmpty(labelText))
            {
                return MvcHtmlString.Empty;
            }

            TagBuilder tag = new TagBuilder("label");
            tag.MergeAttributes(htmlAttributes);
            tag.Attributes.Add("for", html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(htmlFieldName));
            tag.SetInnerText(labelText);
            tag.AddCssClass("control-label");

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString Label(string labelText, IDictionary<string, object> htmlAttributes=null)
        {
            TagBuilder tag = new TagBuilder("label");
            tag.MergeAttributes(htmlAttributes);
            tag.SetInnerText(labelText);
            tag.AddCssClass("control-label");

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString CheckBoxFor<TModel>(HtmlHelper<TModel> html, Expression<Func<TModel, bool>> expression, string id, string checkBoxText)
        {
            TagBuilder tag = new TagBuilder("label");
            tag.AddCssClass("checkbox");
            tag.Attributes.Add("id", id);
            tag.InnerHtml = String.Format("{0} {1}", html.CheckBoxFor(expression).ToHtmlString(), checkBoxText);

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString CheckBoxFor<TModel>(HtmlHelper<TModel> html, Expression<Func<TModel, bool>> expression, string id, string checkBoxText, object htmlAttributes)
        {
            TagBuilder tag = new TagBuilder("label");
            tag.AddCssClass("checkbox");
            tag.Attributes.Add("id", id);
            tag.InnerHtml = String.Format("{0} {1}", html.CheckBoxFor(expression, new RouteValueDictionary(htmlAttributes)).ToHtmlString(), checkBoxText);

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }
    }
}