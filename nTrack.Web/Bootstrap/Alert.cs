﻿namespace nTrack.Web.Bootstrap
{
    public class Alert
    {
        public AlertType Type { get; set; }
        public string Message { get; set; }
        public string TabValue { get; set; }

        public Alert(AlertType type, string message, string tabValue)
        {
            Type = type;
            Message = message;
            TabValue = tabValue;
        }
    }
}