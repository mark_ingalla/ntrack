﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace nTrack.Web.Bootstrap
{
    public static partial class Bootstrap
    {
        public enum PopoverPlacement
        {
            Right, 
            Left,
            Top, 
            Bottom
        }
        public static MvcHtmlString InfoPopover(PopoverPlacement placement, string title, string content)
        {
            TagBuilder tag = new TagBuilder("a");
            tag.Attributes["data-title"] = title;
            tag.Attributes["data-placement"] = placement.ToString().ToLower();
            tag.Attributes["data-toggle"] = "popover";
            tag.Attributes["data-content"] = content;
            tag.Attributes["data-trigger"] = "hover";
            tag.Attributes["href"] = "#";
            tag.AddCssClass("icon-info-sign");
            return MvcHtmlString.Create(tag.ToString());
        }
    }
}
