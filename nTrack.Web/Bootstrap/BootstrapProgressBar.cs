﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace nTrack.Web.Bootstrap
{
    public partial class Bootstrap
    {
        public static MvcHtmlString ProgressBar(int percentage)
        {
            TagBuilder tag = new TagBuilder("div");
            tag.AddCssClass("progress");

            TagBuilder bar = new TagBuilder("div");
            bar.AddCssClass("bar bar-success");
            bar.Attributes.Add("style", string.Format("width: {0}%;", percentage));

            tag.InnerHtml = bar.ToString();

            return MvcHtmlString.Create(tag.ToString());
        }
    }
}
