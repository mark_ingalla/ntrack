﻿using System.Web.Mvc;

namespace nTrack.Web.Bootstrap
{
    public static partial class Bootstrap
    {
        public static MvcHtmlString SubmitButton(string innerText, string id)
        {
            TagBuilder tag = new TagBuilder("button");
            tag.Attributes["type"] = "submit";
            tag.Attributes["id"] = id;
            tag.AddCssClass("btn");
            tag.SetInnerText(innerText);

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString SaveButton(string innerText, string id)
        {
            TagBuilder tag = new TagBuilder("button");
            tag.Attributes["type"] = "submit";
            tag.Attributes["id"] = id;
            tag.AddCssClass("btn btn-success");
            tag.SetInnerText(innerText);

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString BackButton(string innerText, string id, string href)
        {
            TagBuilder tag = new TagBuilder("a");
            tag.Attributes["id"] = id;
            tag.AddCssClass("btn btn-inverse");
            tag.Attributes["href"] = href;
            tag.SetInnerText(innerText);

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }


        #region Jacob Wizard Helper

        public static MvcHtmlString NextAction(string href)
        {
            TagBuilder tag = new TagBuilder("a");
            tag.AddCssClass("btn btn-primary pull-right");
            if (href == null)
                tag.Attributes["disabled"] = "disabled";
            else
                tag.Attributes["href"] = href;

            tag.InnerHtml = "Next  <i class=\"icon-arrow-right icon-white\"></i>";

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString PreviousAction(string href)
        {
            TagBuilder tag = new TagBuilder("a");
            tag.AddCssClass("btn btn-inverse pull-left");
            if (href == null)
                tag.Attributes["disabled"] = "disabled";
            else
                tag.Attributes["href"] = href;
            tag.InnerHtml = "<i class=\"icon-arrow-left icon-white\"></i>  Previous";

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString NextSubmit()
        {
            TagBuilder tag = new TagBuilder("button");
            tag.AddCssClass("btn btn-primary pull-right");
            tag.Attributes["type"] = "submit";
            tag.InnerHtml = "Next  <i class=\"icon-arrow-right icon-white\"></i>";

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }
        #endregion

        public static MvcHtmlString NextButton(string innerText, string id, bool disabled = false)
        {
            TagBuilder tag = new TagBuilder("button");
            tag.Attributes["id"] = id;
            tag.Attributes["type"] = "submit";
            tag.AddCssClass("btn btn-primary");
            tag.SetInnerText(innerText);

            var ret = tag.ToString();

            if (disabled)
                ret = ret.Insert(8, " disabled ");

            return MvcHtmlString.Create(ret);
        }

        public static MvcHtmlString ResetButton(string innerText, string id, bool disabled = false)
        {
            TagBuilder tag = new TagBuilder("button");
            tag.Attributes["id"] = id;
            tag.Attributes["type"] = "button";
            tag.AddCssClass("btn btn-warning");
            tag.SetInnerText(innerText);

            return MvcHtmlString.Create(tag.ToString());
        }
    }
}