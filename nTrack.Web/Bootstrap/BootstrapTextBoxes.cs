﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace nTrack.Web.Bootstrap
{
    public static partial class Bootstrap
    {
        public static MvcHtmlString TextBoxFor<TModel, TValue>(HtmlHelper<TModel> html,
                                                               Expression<Func<TModel, TValue>> expression,
                                                               object htmlAttributes = null, string @class = null)
        {
            var metaData = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            string displayName = metaData.DisplayName ??
                                 (metaData.PropertyName ?? ExpressionHelper.GetExpressionText(expression));

            var ret = html.TextBoxFor(expression, htmlAttributes)
                .Attribute("placeholder", displayName);

            if (!string.IsNullOrEmpty(@class))
            {
                ret = ret.Attribute("class", @class);
            }
            return ret;
        }

        public static MvcHtmlString TextAreaFor<TModel, TValue>(HtmlHelper<TModel> html,
                                                               Expression<Func<TModel, TValue>> expression,
                                                               object htmlAttributes = null, string @class = null)
        {
            var metaData = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            string displayName = metaData.DisplayName ??
                                 (metaData.PropertyName ?? ExpressionHelper.GetExpressionText(expression));

            var ret = html.TextAreaFor(expression)
                .Attribute("placeholder", displayName);

            if (!string.IsNullOrEmpty(@class))
            {
                ret = ret.Attribute("class", @class);
            }
            return ret;
        }

        public static MvcHtmlString PasswordFor<TModel, TValue>(HtmlHelper<TModel> html,
                                                                Expression<Func<TModel, TValue>> expression,
                                                                object htmlAttributes = null, string @class = null)
        {
            var metaData = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            string displayName = metaData.DisplayName ??
                                 (metaData.PropertyName ?? ExpressionHelper.GetExpressionText(expression));

            var ret = html.PasswordFor(expression)
                .Attribute("placeholder", displayName);

            if (!string.IsNullOrEmpty(@class))
            {
                ret = ret.Attribute("class", @class);
            }
            return ret;
        }

        public static MvcHtmlString DisabledTextBoxFor<TModel, TValue>(HtmlHelper<TModel> html,
                                                                       Expression<Func<TModel, TValue>> expression,
                                                                       string innerText)
        {
            var metaData = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            string displayName = metaData.DisplayName ??
                                 (metaData.PropertyName ?? ExpressionHelper.GetExpressionText(expression));

            var ret = new TagBuilder("input");

            ret.Attributes.Add("class", "input-xlarge");
            ret.Attributes.Add("name", metaData.PropertyName);
            ret.Attributes.Add("type", "text");
            ret.Attributes.Add("placeholder", innerText);

            var txtbox = ret.ToString();
            txtbox = txtbox.Insert(7, " disabled ");

            return MvcHtmlString.Create(txtbox);
        }
    }
}