﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace nTrack.Web.Bootstrap
{
    public static partial class Bootstrap
    {
        public static MvcHtmlString ComboboxFor<TModel, TValue>(HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, 
            Dictionary<Guid, string> options, object htmlAttributes = null)
        {
            return ComboboxFor(html, expression, options, new RouteValueDictionary(htmlAttributes));
        }

        public static MvcHtmlString ComboboxFor<TModel, TValue>(HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression,
            Dictionary<string, string> options, object htmlAttributes = null)
        {
            return ComboboxFor(html, expression, options, new RouteValueDictionary(htmlAttributes));
        }

        public static MvcHtmlString ComboboxFor<TModel, TValue>(HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, 
            Dictionary<Guid, KeyValuePair<string, string>> options, object htmlAttributes = null)
        {
            return ComboboxFor(html, expression, options, new RouteValueDictionary(htmlAttributes));
        }

        public static MvcHtmlString ComboboxFor<TModel, TValue>(HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, 
            Dictionary<Guid, string> options, IDictionary<string, object> htmlAttributes)
        {
            var tag = SelectBuilder(html, expression, htmlAttributes);

            tag.InnerHtml = OptionsBuilder(options);

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString ComboboxFor<TModel, TValue>(HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression,
            Dictionary<string, string> options, IDictionary<string, object> htmlAttributes)
        {
            var tag = SelectBuilder(html, expression, htmlAttributes);

            tag.InnerHtml = OptionsBuilder(options);

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString ComboboxFor<TModel, TValue>(HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, 
            Dictionary<Guid, KeyValuePair<string, string>> options, IDictionary<string, object> htmlAttributes)
        {
            var tag = SelectBuilder(html, expression, htmlAttributes);

            tag.InnerHtml = OptionsBuilder(options);

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }

        private static TagBuilder SelectBuilder<TModel, TValue>(HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, IDictionary<string, object> htmlAttributes)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            string htmlFieldName = ExpressionHelper.GetExpressionText(expression);
            string labelText = metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();
            if (String.IsNullOrEmpty(labelText))
            {
                return null;
            }

            TagBuilder tag = new TagBuilder("select");
            tag.MergeAttributes(htmlAttributes);
            tag.Attributes.Add("id", htmlFieldName);
            tag.Attributes.Add("name", htmlFieldName);
            tag.Attributes.Add("for", html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(htmlFieldName));
            tag.AddCssClass("combobox");

            return tag;
        }

        private static string OptionsBuilder(Dictionary<Guid, string> options)
        {
            StringBuilder optionstringBuilder = new StringBuilder();
            TagBuilder op = new TagBuilder("option");
            optionstringBuilder.Append(op);

            if (options != null)
            {
                foreach (var o in options)
                {
                    TagBuilder option = new TagBuilder("option");
                    option.Attributes.Add("value", o.Key.ToString());
                    option.InnerHtml = o.Value;
                    optionstringBuilder.Append(option);
                }
            }

            return optionstringBuilder.ToString();
        }

        private static string OptionsBuilder(Dictionary<string, string> options)
        {
            StringBuilder optionstringBuilder = new StringBuilder();
            TagBuilder op = new TagBuilder("option");
            optionstringBuilder.Append(op);

            if (options != null)
            {
                foreach (var o in options)
                {
                    TagBuilder option = new TagBuilder("option");
                    option.Attributes.Add("value", o.Key);
                    option.InnerHtml = o.Value;
                    optionstringBuilder.Append(option);
                }
            }

            return optionstringBuilder.ToString();
        }

        private static string OptionsBuilder(Dictionary<Guid, KeyValuePair<string, string>> options)
        {
            StringBuilder optionstringBuilder = new StringBuilder();
            TagBuilder op = new TagBuilder("option");
            optionstringBuilder.Append(op);

            if (options != null)
            {
                foreach (var o in options)
                {
                    TagBuilder option = new TagBuilder("option");
                    option.Attributes.Add("value", string.Format("{0}/{1}", o.Key.ToString(), o.Value.Value));
                    option.InnerHtml = o.Value.Key;
                    optionstringBuilder.Append(option);
                }
            }

            return optionstringBuilder.ToString();
        }
    }
}