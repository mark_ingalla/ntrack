﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Machine.Specifications;
using PalletPlus.Domain.Models;
using PalletPlus.Messages.Commands.Account;
using PalletPlus.Messages.Commands.Transporter;
using Raven.Client.Document;
using Raven.Client.Embedded;

namespace PalletPlusTests
{
    [Subject("Embedded Raven DB test")]
    public class RavenDBTest
    {
        public static EmbeddableDocumentStore DocumentStore { get; set; }
        private static Site site;

        Establish context = () =>
            {
                DocumentStore = TestBase.SetupDocumentStore();
            };

        Because of = () =>
        {
            using (var session = DocumentStore.OpenSession())
            {
                site = new Site(Guid.NewGuid(), Guid.NewGuid(), "", "", "", "", "", "", "");
                session.Store(site);
                session.SaveChanges();
            }
        };

        It movementTypeShouldInternal = () =>
            {
                using (var session = DocumentStore.OpenSession())
                {
                    Site siteReloaded = session.Load<Site>(site.Id);
                    site.ShouldNotBeNull();
                }
            };
    }
}
