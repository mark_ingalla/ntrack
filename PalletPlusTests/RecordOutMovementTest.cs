﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Machine.Specifications;
using NServiceBus.Testing;
using PalletPlus.Domain.DomainServices.DocketNumberService;
using PalletPlus.Messages.Events.Movement;
using PalletPlus.Domain.Models;

namespace PalletPlusTests
{
    class RecordOutMovementTest : TestBase
    {
        [Subject("Transfering between 2 sites in an internal transfert")]
        public class CreatingAccount
        {
            static Guid accountId;
            static Guid movementId;
            static Guid companyId;
            static Guid siteId;
            static Guid partnerId;
            static TransferDirection direction;
            static DateTime movementDate;
            static Guid supplierId;
            static string conNote;
            static string providerDocketNo;
            static string partnerReference;
            static string reference;
            static bool receivedViaTransporter;
            static Guid transporterId;
            static Dictionary<string, int> assets;
            static string movementType;
            static bool isPta = false;

            static Movement _movement;

            static MovementCreated _event;

            Establish context = () =>
            {
                Setup();

                //Supplier
                supplierId = Guid.NewGuid();
                Supplier supplier = new Supplier(supplierId, "", Guid.NewGuid(), "", "", "", "", "", "", "", "", "", "", "");
                //Assets
                supplier.AddAsset("123", "tyle");
                Repository.Save(supplier, Guid.NewGuid());

                //Account
                accountId = Guid.NewGuid();
                Account account = new Account(accountId, "");
                Repository.Save(account, Guid.NewGuid());

                //Partner
                partnerId = Guid.NewGuid();
                Partner partner = new Partner(partnerId, "", "", "", "", "", "", "", "", "");
                partner.AddSupplier(supplierId, "999", true);
                partner.AllowEquipment("123", supplierId);
                Repository.Save(partner, Guid.NewGuid());

                //Site
                siteId = Guid.NewGuid();
                Site site = new Site(accountId, siteId, "", "", "", "", "", "", "");
                //Link Site & Partner
                site.AddPartner(partnerId);
                site.AddSupplierToSite(supplierId, "supplierName", "999", "pre", "suff", 1);
                Repository.Save(site, Guid.NewGuid());

                Thread.Sleep(10000);
            };

            Because of = () =>
                                    {
                                        var assets = new Dictionary<string, int>();
                                        assets.Add("123", 27);

                                        //Create the movement
                                        movementId = Guid.NewGuid();
                                        _movement = new Movement(movementId, accountId, siteId, partnerId, supplierId, assets,
                                            direction, movementDate, conNote, providerDocketNo, partnerReference, reference, receivedViaTransporter,
                                            transporterId);

                                        Repository.Save(_movement, Guid.NewGuid());
                                        _event = Commit.Events.Select(x => x.Body).OfType<MovementCreated>().First();
                                    };

            It movementTypeShouldInternal = () => _event.MovementType.ShouldEqual("Internal");
        }
    }
}
