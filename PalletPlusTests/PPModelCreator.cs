﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NServiceBus;
using PalletPlus.Domain.Models;
using PalletPlus.Messages.Commands.Movement;
using PalletPlus.Messages.Commands.Partner;
using PalletPlus.Messages.Commands.Site;
using PalletPlus.Messages.Commands.Supplier;

namespace PalletPlusTests
{
    public class PPModelCreator
    {
        public static Guid AddSupplier(IBus bus, string supplierName, string accountNumber, Dictionary<string, string> equipmentsCodeName)
        {
            var supplierId = Guid.NewGuid();
            bus.Send(new CreateSupplier { SupplierId = supplierId, Name = supplierName, AccountNumber = accountNumber });

            foreach (KeyValuePair<string, string> kvp in equipmentsCodeName)
            {
                bus.Send(new AddEquipmentToSupplier
                {
                    SupplierId = supplierId,
                    EquipmentCode = kvp.Key,
                    EquipmentName = kvp.Value
                });
            }

            return supplierId;
        }

        public static Guid CreatePartner(IBus bus, Guid supplierId, string accountNumber, string[] allowedEquipmentsCode, bool allowIOU)
        {
            var partnerId = Guid.NewGuid();
            bus.Send(new CreatePartner { PartnerId = partnerId });
            //Add Supplier to Partner
            bus.Send(new AddSupplierToPartner
            {
                PartnerId = partnerId,
                SupplierId = supplierId,
                AccountNumber = accountNumber,
                AllowIOU = true
            });
            //Allow equipment to partner
            bus.Send(new AllowEquipmentToPartner
            {
                PartnerId = partnerId,
                SupplierId = supplierId,
                Equipments = allowedEquipmentsCode
            });
            return partnerId;
        }

        public static Guid CreateSite(IBus bus, string name, string addressLine1)
        {
            var siteId = Guid.NewGuid();
            bus.Send(new CreateSite { SiteId = siteId, Name = name, AddressLine1 = addressLine1 });
            return siteId;
        }

        public static Guid CreateMovement(IBus bus, Guid siteId, Guid destinationId, Guid supplierId, Dictionary<string, int> equipments,
            Guid transporterId, TransferDirection transportDirection)
        {
            var movementId = Guid.NewGuid();
            bus.Send(new CreateMovement
            {
                MovementId = movementId,
                SiteId = siteId,
                DestinationId = destinationId,
                SupplierId = supplierId,
                Equipments = equipments,
                TransporterId = transporterId,
                Direction = transportDirection.ToString()
            });
            return movementId;
        }

        /// <summary>
        /// Link Site & Supplier
        /// </summary>
        /// <param name="bus"></param>
        /// <param name="siteId"></param>
        /// <param name="supplierId"></param>
        public static void AddSupplierToSite(IBus bus, Guid siteId, Guid supplierId, string accountNumber)
        {
            bus.Send(new AddSupplierToSite()
            {
                SiteId = siteId,
                SupplierId = supplierId,
                SupplierAccountNumber = accountNumber,
                Prefix = "P",
                Suffix = "S",
                SequenceStartNumber = 23
            });
        }

        public static void AddPartnerToSite(IBus bus, Guid siteId, Guid partnerId)
        {
            bus.Send(new AddPartnerToSite()
            {
                SiteId = siteId,
                PartnerId = partnerId
            });
        }
    }
}
