﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Machine.Specifications;
using PalletPlus.Domain;
using PalletPlus.Domain.DomainServices.AssetBalance;
using PalletPlus.Domain.Models;
using PalletPlus.DomainServices.AssetBalance;
using PalletPlus.DomainServices.DocketNumberService;
using PalletPlus.Messages.Events.Movement.TransferMovement;
using PalletPlus.Messages.Events.Site;
using Raven.Client;
using StructureMap;

namespace PalletPlusTests
{
    // ReSharper disable InconsistentNaming
    [Subject(typeof(TransferMovement), "Transfer movement tests")]
    public class TransferMovementTests : MovementTestsBase
    {
        [Subject("Creating simple transfer movement")]
        public class CreatingNonPtaMovement
        {
            static TransferMovementCreated _event;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
            };
            Because _of = () =>
                             {
                                 var movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets);
                                 Repository.Save(movement, Guid.NewGuid(), null);
                                 _event = Commit.Events.Select(x => x.Body).OfType<TransferMovementCreated>().First();
                             };


            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It supplierIdShouldEqual = () => _event.SupplierId.ShouldEqual(_supplierId);
            It siteIdShouldEqual = () => _event.SiteId.ShouldEqual(_siteId);
            It partnerIdShouldEqual = () => _event.PartnerId.ShouldEqual(_partnerId);
            It movementIdShouldEqual = () => _event.MovementId.ShouldEqual(_movementId);
            It directionShouldBeTheSame = () => _event.Direction.ToString(CultureInfo.InvariantCulture).ShouldEqual(TransferDirection.Out.ToString());
            It docketNoShouldBeInitialized = () => _event.DocketNumber.ShouldNotBeEmpty();
            It movementDateShouldEqual = () => _event.MovementDate.ShouldEqual(_movementDate);
            It carriedAssetsShouldBeTheSame = () => _event.Assets.ToList().ForEach(x => x.Value.ShouldEqual(_assets[x.Key]));
            It shouldNotBePte = () => _event.IsPta.ShouldBeFalse();
        }

        [Subject("Creating out transfer with balance checking")]
        public class CreatingPtaMovementWithBalanceChecking
        {
            static TransferMovementCreated _event;
            static AssetBalanceService _assetBalanceService;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                var movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets);
                Repository.Save(movement, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<TransferMovementCreated>().First();
            };
            Because _of = () =>
            {
                _assetBalanceHandler.Handle(_event);
                _assetBalanceService = new AssetBalanceService();
            };
            It assetABalanceShouldEqual = () => _assetBalanceService.Balance(_siteId, _supplierId, _assetA).ShouldEqual(-_movedAssetAQty);
            It assetBBalanceShouldEqual = () => _assetBalanceService.Balance(_siteId, _supplierId, _assetB).ShouldEqual(-_movedAssetBQty);
        }

        [Subject("Creating out transfer with asset not allowed for supplier, should fail")]
        public class CreatingMovementWithIncorrectAssetCodeForSupplier
        {
            static Exception _excecption;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                _assets.Add("C", 1);
            };
            Because _of = () =>
            {
                _excecption = Catch.Exception(() => new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets));
            };
            It shouldRaiseException = () => _excecption.ShouldNotBeNull();
        }

        [Subject("Creating out transfer with nonexisting site, should fail")]
        public class CreatingMovementWithNotExistingSite
        {
            static Exception _excecption;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
            };
            Because _of = () =>
            {
                _excecption = Catch.Exception(() => new TransferMovement(_movementId, Guid.NewGuid(), _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets));
            };
            It shouldRaiseException = () => _excecption.ShouldNotBeNull();
        }

        [Subject("Creating out transfer with nonexisting partner, should fail")]
        public class CreatingMovementWithNotExistingPartner
        {
            static Exception _excecption;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
            };
            Because _of = () =>
            {
                _excecption = Catch.Exception(() => new TransferMovement(_movementId, _siteId, Guid.NewGuid(), TransferDirection.Out, _movementDate, _supplierId, _assets));
            };
            It shouldRaiseException = () => _excecption.ShouldNotBeNull();
        }

        [Subject("Creating out transfer with not enabled partner, should fail")]
        public class CreatingMovementWithNotEnabledPartner
        {
            static Exception _excecption;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                partner.RemoveSupplier(_supplierId);
                Repository.Save(partner, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                _excecption = Catch.Exception(() => new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets));
            };
            It shouldRaiseException = () => _excecption.ShouldNotBeNull();
        }

        [Subject("Creating out transfer with not enabled asset code, should fail")]
        public class CreatingMovementWithNotEnabledAssetCode
        {
            static Exception _excecption;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                partner.DisallowAssetType(_assetA, _supplierId);
                Repository.Save(partner, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                _excecption = Catch.Exception(() => new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets));
            };
            It shouldRaiseException = () => _excecption.ShouldNotBeNull();
        }

        [Subject("Creating out transfer with nonexisting supplier, should fail")]
        public class CreatingMovementWithNonExistingSupplier
        {
            static Exception _excecption;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
            };
            Because _of = () =>
            {
                _excecption = Catch.Exception(() => new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, Guid.NewGuid(), _assets));
            };
            It shouldRaiseException = () => _excecption.ShouldNotBeNull();
        }

        [Subject("Creating out transfer without assets, should fail")]
        public class CreatingNonPtaMovementWithoutAssets
        {
            static Exception _excecption;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
            };

            Because _of = () =>
            {
                _assets = new Dictionary<string, int>();
                _excecption = Catch.Exception(() => new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets));
            };

            It shouldRaiseException = () => _excecption.ShouldNotBeNull();
        }

        [Subject("Creating simple pta transfer movement, should raise event")]
        public class CreatingPtaMovement
        {
            static TransferMovementCreated _event;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
            };
            Because _of = () =>
            {
                var movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<TransferMovementCreated>().First();
            };

            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It siteIdShouldEqual = () => _event.SiteId.ShouldEqual(_siteId);
            It partnerIdShouldEqual = () => _event.PartnerId.ShouldEqual(_partnerId);
            It movementIdShouldEqual = () => _event.MovementId.ShouldEqual(_movementId);
            It directionShouldBeTheSame = () => _event.Direction.ToString(CultureInfo.InvariantCulture).ShouldEqual(TransferDirection.Out.ToString());
            It docketNoShouldBeInitialized = () => _event.DocketNumber.ShouldNotBeNull();
            It movementDateShouldEqual = () => _event.MovementDate.ShouldEqual(_movementDate);
            It carriedAssetsShouldBeTheSame = () => _event.Assets.ToList().ForEach(x => x.Value.ShouldEqual(_assets[x.Key]));
            It shouldNotBePte = () => _event.IsPta.ShouldBeTrue();
        }

        [Subject("Creating out pta transfer shouldn't change balance")]
        public class CreatingPtaMovementShouldntChangeBalance
        {
            static TransferMovementCreated _event;
            static AssetBalanceService _assetBalanceService;

            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();

                var movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<TransferMovementCreated>().First();
            };

            Because _of = () =>
            {
                _assetBalanceHandler.Handle(_event);
                _assetBalanceService = new AssetBalanceService();
            };

            It assetABalanceShouldEqual = () => _assetBalanceService.Balance(_siteId, _supplierId, _assetA).ShouldEqual(0);
            It assetBBalanceShouldEqual = () => _assetBalanceService.Balance(_siteId, _supplierId, _assetB).ShouldEqual(0);
        }

        [Subject("Creating out pta transfer, should raise event")]
        public class PtaMovementApproval
        {
            static PtaApproved _event;
            static AssetBalanceService _assetBalanceService;

            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                var movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _assetBalanceHandler.Handle(Commit.Events.Select(x => x.Body).OfType<TransferMovementCreated>().First());
                _assetBalanceService = new AssetBalanceService();
                movement.ApprovePtaMovement("ApprovalNumber", _assets);
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
                              {
                                  _event = Commit.Events.Select(x => x.Body).OfType<PtaApproved>().First();
                              };

            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It assetsShouldNotBeNull = () => _event.Assets.ShouldNotBeNull();
            It movementIdShouldEqual = () => _event.MovementId.ShouldEqual(_movementId);
            It praNumShouldNotBeEmpty = () => _event.PtaApprovalNum.ShouldNotBeEmpty();
        }

        [Subject("Creating out pta transfer, should change balance")]
        public class PtaMovementApprovalShouldChangeBalance
        {
            static TransferMovementCreated _event;
            static AssetBalanceService _assetBalanceService;

            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();

                var movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<TransferMovementCreated>().First();

                _assetBalanceHandler.Handle(_event);
                _assetBalanceService = new AssetBalanceService();

                movement.ApprovePtaMovement("ApprovalNumber", _assets);
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                _assetBalanceHandler.Handle(Commit.Events.Select(x => x.Body).OfType<PtaApproved>().First());
            };
            It assetABalanceShouldEqual = () => _assetBalanceService.Balance(_siteId, _supplierId, _assetA).ShouldEqual(-_movedAssetAQty);
            It assetBBalanceShouldEqual = () => _assetBalanceService.Balance(_siteId, _supplierId, _assetB).ShouldEqual(-_movedAssetBQty);
        }

        [Subject("Creating out pta transfer approval with correction should change balance")]
        public class PtaMovementApprovalWithCorrectionShouldChangeBalance
        {
            static TransferMovementCreated _event;
            static AssetBalanceService _assetBalanceService;
            static int _assetAQtyCorrection = 10;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();

                var movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<TransferMovementCreated>().First();

                _assetBalanceHandler.Handle(_event);
                _assetBalanceService = new AssetBalanceService();

                _assets[_assetA] = _assetAQtyCorrection;
                movement.ApprovePtaMovement("ApprovalNumber", _assets);
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                _assetBalanceHandler.Handle(Commit.Events.Select(x => x.Body).OfType<PtaApproved>().First());
            };

            It assetABalanceShouldEqual = () => _assetBalanceService.Balance(_siteId, _supplierId, _assetA).ShouldEqual(-_assetAQtyCorrection);
            It assetBBalanceShouldEqual = () => _assetBalanceService.Balance(_siteId, _supplierId, _assetB).ShouldEqual(-_movedAssetBQty);
        }

        [Subject("Creating out pta transfer approval with correction and not allowed asset type, should fail")]
        public class PtaMovementApprovalWithCorrectionAndNotAllowedAsset
        {
            static TransferMovement movement;
            static Exception _exception;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();

                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _assets.Add("C", 1);

            };
            Because _of = () =>
                              {
                                  _exception = Catch.Exception(() => movement.ApprovePtaMovement("ApprovalNumber", _assets));
                              };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Creating out pta transfer approval with empty assets, should fail")]
        public class PtaMovementApprovalWithCorrectionAndEmptyAssets
        {
            static TransferMovement movement;
            static Exception _exception;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();

                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _assets.Clear();
            };
            Because _of = () =>
            {
                _exception = Catch.Exception(() => movement.ApprovePtaMovement("ApprovalNumber", _assets));
            };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Approving non pta movement, should fail")]
        public class ApprovingNonPtaMovement
        {
            static TransferMovement movement;
            static Exception _exception;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();

                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, false);
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                _exception = Catch.Exception(() => movement.ApprovePtaMovement("ApprovalNumber", _assets));
            };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Declining pta movement, should raise event")]
        public class DecliningPtaMovement
        {
            static TransferMovement movement;
            static PtaDeclined _event;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();

                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
                              {
                                  movement.DeclinePtaMovement();
                                  Repository.Save(movement, Guid.NewGuid(), null);
                                  _event = Commit.Events.Select(x => x.Body).OfType<PtaDeclined>().First();
                              };

            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It movementIdShouldEqual = () => _event.MovementId.ShouldEqual(_movementId);
        }

        [Subject("Declining non pta movement, should raise exception")]
        public class DecliningNonPtaMovement
        {
            static TransferMovement movement;
            static Exception _exception;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();

                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, false);
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                _exception = Catch.Exception(() => movement.DeclinePtaMovement());
            };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Updating movement references, should raise event")]
        public class UpdatingMovementReferences
        {
            static TransferMovement movement;
            static ReferencesUpdated _event;
            static string _movementReference;
            static string _partnerReference;
            static string _transporterReference;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                _movementReference = "MovementReference";
                _partnerReference = "PartnerReference";
                _transporterReference = "TransporterReference";
                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
                              {
                                  movement.UpdateReferences(_movementReference, _partnerReference, _transporterReference);
                                  Repository.Save(movement, Guid.NewGuid(), null);
                                  _event = Commit.Events.Select(x => x.Body).OfType<ReferencesUpdated>().First();
                              };

            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It movementIdShouldEqual = () => _event.MovementId.ShouldEqual(_movementId);
            It movementReferenceShouldEqual = () => _event.MovementReference.ShouldEqual(_movementReference);
            It partnerReferenceShouldEqual = () => _event.PartnerReference.ShouldEqual(_partnerReference);
            It transporterReferenceShouldEqual = () => _event.TransporterReference.ShouldEqual(_transporterReference);
        }

        [Subject("Issuing movement correction, should raise event")]
        public class IssuingMovementCorrection
        {
            static TransferMovement movement;
            static CorrectionRequestIssued _event;
            static Guid _movementCorrectionKey;
            static Dictionary<string, int> _assetsToCorrect;
            static int _correctedQuantityForAssetA;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _movementCorrectionKey = Guid.NewGuid();
                _correctedQuantityForAssetA = 10;
                _assetsToCorrect = new Dictionary<string, int>();
                _assetsToCorrect.Add(_assetA, _correctedQuantityForAssetA);
            };
            Because _of = () =>
            {
                movement.IssueQuantityCorrection(_movementCorrectionKey, _assetsToCorrect);
                Repository.Save(movement, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<CorrectionRequestIssued>().First();
            };

            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It movementIdShouldEqual = () => _event.MovementId.ShouldEqual(_movementId);
            It correctionKeyShouldEqual = () => _event.CorrectionKey.ShouldEqual(_movementCorrectionKey);
            It correctedAssetsShouldNotBeNull = () => _event.AssetCodeQuantity.ShouldNotBeNull();
            It correctedAssetsShouldHave1Item = () => _event.AssetCodeQuantity.Count.ShouldEqual(1);
            It assetCodeShouldEqual = () => _event.AssetCodeQuantity.First().Key.ShouldEqual(_assetA);
            It assetQuantityShouldEqual = () => _event.AssetCodeQuantity.First().Value.ShouldEqual(_correctedQuantityForAssetA);
        }

        [Subject("Issuing movement correction with incorrect asset code, should fail")]
        public class IssuingMovementCorrectionWithIncorrectAssetCode
        {
            static TransferMovement movement;
            static Exception _exception;
            static Guid _movementCorrectionKey;
            static Dictionary<string, int> _assetsToCorrect;
            static int _correctedQuantityForAssetC;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _movementCorrectionKey = Guid.NewGuid();
                _correctedQuantityForAssetC = 10;
                _assetsToCorrect = new Dictionary<string, int>();
                _assetsToCorrect.Add("C", _correctedQuantityForAssetC);
            };
            Because _of = () =>
                              {
                                  _exception = Catch.Exception(() => movement.IssueQuantityCorrection(_movementCorrectionKey, _assetsToCorrect));
                              };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Issuing movement correction with negative quantity, should fail")]
        public class IssuingMovementCorrectionWithNegativeQuantity
        {
            static TransferMovement movement;
            static Exception _exception;
            static Guid _movementCorrectionKey;
            static Dictionary<string, int> _assetsToCorrect;
            static int _correctedQuantityForAssetA;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _movementCorrectionKey = Guid.NewGuid();
                _correctedQuantityForAssetA = -1;
                _assetsToCorrect = new Dictionary<string, int>();
                _assetsToCorrect.Add(_assetA, _correctedQuantityForAssetA);
            };
            Because _of = () =>
            {
                _exception = Catch.Exception(() => movement.IssueQuantityCorrection(_movementCorrectionKey, _assetsToCorrect));
            };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Issuing movement correction with empty assets, should fail")]
        public class IssuingMovementCorrectionWithEmptyAssets
        {
            static TransferMovement movement;
            static Exception _exception;
            static Guid _movementCorrectionKey;
            static Dictionary<string, int> _assetsToCorrect;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _movementCorrectionKey = Guid.NewGuid();
                _assetsToCorrect = new Dictionary<string, int>();
            };
            Because _of = () =>
            {
                _exception = Catch.Exception(() => movement.IssueQuantityCorrection(_movementCorrectionKey, _assetsToCorrect));
            };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Issuing movement correction of cancelled movement, should fail")]
        public class IssuingMovementCorrectionOfCancelledMovement
        {
            static TransferMovement movement;
            static Exception _exception;
            static Guid _movementCorrectionKey;
            static Dictionary<string, int> _assetsToCorrect;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _movementCorrectionKey = Guid.NewGuid();
                _assetsToCorrect = new Dictionary<string, int>();
                var cancellationey = Guid.NewGuid();
                movement.IssueMovementCancellation(cancellationey, "Reason");
                movement.AcceptMovementCancellation(cancellationey);
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                _exception = Catch.Exception(() => movement.IssueQuantityCorrection(_movementCorrectionKey, _assetsToCorrect));
            };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Approving movement correction, should raise event")]
        public class ApprovingMovementCorrection
        {
            static TransferMovement movement;
            static CorrectionApproved _event;
            static Guid _movementCorrectionKey;
            static Dictionary<string, int> _assetsToCorrect;
            static int _correctedQuantityForAssetA;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _movementCorrectionKey = Guid.NewGuid();
                _correctedQuantityForAssetA = 10;
                _assetsToCorrect = new Dictionary<string, int>();
                _assetsToCorrect.Add(_assetA, _correctedQuantityForAssetA);
                movement.IssueQuantityCorrection(_movementCorrectionKey, _assetsToCorrect);
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                movement.ApproveCorrection(_movementCorrectionKey);
                Repository.Save(movement, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<CorrectionApproved>().First();
            };

            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It movementIdShouldEqual = () => _event.MovementId.ShouldEqual(_movementId);
            It correctionKeyShouldEqual = () => _event.CorrectionKey.ShouldEqual(_movementCorrectionKey);
            It correctedAssetsShouldNotBeNull = () => _event.AssetCodeQuantity.ShouldNotBeNull();
            It correctedAssetsShouldHave1Item = () => _event.AssetCodeQuantity.Count.ShouldEqual(1);
            It assetCodeShouldEqual = () => _event.AssetCodeQuantity.First().Key.ShouldEqual(_assetA);
            It assetQuantityShouldEqual = () => _event.AssetCodeQuantity.First().Value.ShouldEqual(_correctedQuantityForAssetA);
        }

        [Subject("Declining movement correction, should raise event")]
        public class DecliningMovementCorrection
        {
            static TransferMovement movement;
            static CorrectionDeclined _event;
            static Guid _movementCorrectionKey;
            static Dictionary<string, int> _assetsToCorrect;
            static int _correctedQuantityForAssetA;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _movementCorrectionKey = Guid.NewGuid();
                _correctedQuantityForAssetA = 10;
                _assetsToCorrect = new Dictionary<string, int>();
                _assetsToCorrect.Add(_assetA, _correctedQuantityForAssetA);
                movement.IssueQuantityCorrection(_movementCorrectionKey, _assetsToCorrect);
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                movement.DeclineCorrection(_movementCorrectionKey);
                Repository.Save(movement, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<CorrectionDeclined>().First();
            };

            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It movementIdShouldEqual = () => _event.MovementId.ShouldEqual(_movementId);
            It correctionKeyShouldEqual = () => _event.CorrectionKey.ShouldEqual(_movementCorrectionKey);
        }

        [Subject("Approving correction with incorrect key, should fail")]
        public class ApprovingMovementCorrectionWithIncorrectKey
        {
            static TransferMovement movement;
            static Exception _exception;
            static Guid _movementCorrectionKey;
            static Dictionary<string, int> _assetsToCorrect;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _movementCorrectionKey = Guid.NewGuid();
                _assetsToCorrect = new Dictionary<string, int>();
                _assetsToCorrect.Add(_assetA, 10);
                movement.IssueQuantityCorrection(_movementCorrectionKey, _assetsToCorrect);
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                _exception = Catch.Exception(() => movement.ApproveCorrection(Guid.NewGuid()));
            };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Declining correction with incorrect key, should fail")]
        public class DecliningMovementCorrectionWithIncorrectKey
        {
            static TransferMovement movement;
            static Exception _exception;
            static Guid _movementCorrectionKey;
            static Dictionary<string, int> _assetsToCorrect;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _movementCorrectionKey = Guid.NewGuid();
                _assetsToCorrect = new Dictionary<string, int>();
                _assetsToCorrect.Add(_assetA, 10);
                movement.IssueQuantityCorrection(_movementCorrectionKey, _assetsToCorrect);
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                _exception = Catch.Exception(() => movement.DeclineCorrection(Guid.NewGuid()));
            };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Issuing movement correction for 2nd time should overwrite previous one")]
        public class IssuingMovementCorrection2NdTimeShouldOverwritePrevious
        {
            static TransferMovement movement;
            static CorrectionApproved _event;
            static Guid _movementCorrectionKey;
            static Guid _movementCorrectionKey2;
            static Dictionary<string, int> _assetsToCorrect;
            static int _correctedQuantityForAssetA;
            static int _correctedQuantityForAssetA2;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _movementCorrectionKey = Guid.NewGuid();
                _correctedQuantityForAssetA = 10;
                _correctedQuantityForAssetA2 = 30;
                _assetsToCorrect = new Dictionary<string, int>();
                _assetsToCorrect.Add(_assetA, _correctedQuantityForAssetA);
                movement.IssueQuantityCorrection(_movementCorrectionKey, _assetsToCorrect);
                Repository.Save(movement, Guid.NewGuid(), null);

                _assetsToCorrect = new Dictionary<string, int>();
                _assetsToCorrect.Add(_assetA, _correctedQuantityForAssetA2);
                _movementCorrectionKey2 = Guid.NewGuid();
                movement.IssueQuantityCorrection(_movementCorrectionKey2, _assetsToCorrect);
                Repository.Save(movement, Guid.NewGuid(), null);
            };

            Because _of = () =>
            {
                movement.ApproveCorrection(_movementCorrectionKey2);
                Repository.Save(movement, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<CorrectionApproved>().First();
            };

            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It movementIdShouldEqual = () => _event.MovementId.ShouldEqual(_movementId);
            It correctionKeyShouldEqual = () => _event.CorrectionKey.ShouldEqual(_movementCorrectionKey2);
            It correctedAssetsShouldNotBeNull = () => _event.AssetCodeQuantity.ShouldNotBeNull();
            It correctedAssetsShouldHave1Item = () => _event.AssetCodeQuantity.Count.ShouldEqual(1);
            It assetCodeShouldEqual = () => _event.AssetCodeQuantity.First().Key.ShouldEqual(_assetA);
            It assetQuantityShouldEqual = () => _event.AssetCodeQuantity.First().Value.ShouldEqual(_correctedQuantityForAssetA2);
            It shouldChangePendingCorrectionId = () => movement.PendingCorrection.CorrectionKey.ShouldEqual(_movementCorrectionKey2);
            It shouldChangePendingCorrectionAssets = () => movement.PendingCorrection.AssetCodeQuantity.First().Value.ShouldEqual(_correctedQuantityForAssetA2);
        }

        [Subject("Issuing movement cancellation, should raise event")]
        public class IssuingMovementCancellationShouldRaiseEvent
        {
            static TransferMovement movement;
            static MovementCancellationIssued _event;
            static Guid _movementCancellationKey;
            static string _reason;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _movementCancellationKey = Guid.NewGuid();
                _reason = "Reason";
                movement.IssueMovementCancellation(_movementCancellationKey, _reason);
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                _event = Commit.Events.Select(x => x.Body).OfType<MovementCancellationIssued>().First();
            };

            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It movementIdShouldEqual = () => _event.MovementId.ShouldEqual(_movementId);
            It cancellationKeyShouldEqual = () => _event.CancellationKey.ShouldEqual(_movementCancellationKey);
            It reasonShouldEqual = () => _event.Reason.ShouldEqual(_reason);
        }

        [Subject("Issuing movement cancellation of cancelled movement, should fail")]
        public class IssuingMovementCancellationOfCancelledMovement
        {
            static TransferMovement movement;
            static Guid _movementCancellationKey;
            static Exception _exception;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _movementCancellationKey = Guid.NewGuid();

                movement.IssueMovementCancellation(_movementCancellationKey, "Reason");
                Repository.Save(movement, Guid.NewGuid(), null);
                movement.AcceptMovementCancellation(_movementCancellationKey);
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                _exception = Catch.Exception(() => movement.IssueMovementCancellation(Guid.NewGuid(), "Reason2"));
            };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Accepting movement cancellation, should raise event")]
        public class AcceptingMovementCancellation
        {
            static TransferMovement movement;
            static MovementCancellationAccepted _event;
            static Guid _movementCancellationKey;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _movementCancellationKey = Guid.NewGuid();

                movement.IssueMovementCancellation(_movementCancellationKey, "Reason");
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                movement.AcceptMovementCancellation(_movementCancellationKey);
                Repository.Save(movement, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<MovementCancellationAccepted>().First();
            };

            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It siteIdShouldEqual = () => _event.SiteId.ShouldEqual(_siteId);
            It movementIdShouldEqual = () => _event.MovementId.ShouldEqual(_movementId);
            It correctionKeyShouldEqual = () => _event.CancellationKey.ShouldEqual(_movementCancellationKey);
        }

        [Subject("Declining movement cancellation, should raise event")]
        public class DecliningMovementCancellation
        {
            static TransferMovement movement;
            static MovementCancellationDeclined _event;
            static Guid _movementCancellationKey;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _movementCancellationKey = Guid.NewGuid();

                movement.IssueMovementCancellation(_movementCancellationKey, "Reason");
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                movement.DeclineMovementCancellation(_movementCancellationKey);
                Repository.Save(movement, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<MovementCancellationDeclined>().First();
            };

            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It movementIdShouldEqual = () => _event.MovementId.ShouldEqual(_movementId);
            It correctionKeyShouldEqual = () => _event.CancellationKey.ShouldEqual(_movementCancellationKey);
        }

        [Subject("Approving cancellation with incorrect key, should fail")]
        public class ApprovingMovementCancellationWithIncorrectKey
        {
            static TransferMovement movement;
            static Exception _exception;
            static Guid _movementCancellationKey;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _movementCancellationKey = Guid.NewGuid();
                movement.IssueMovementCancellation(_movementCancellationKey, "Reason");
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                _exception = Catch.Exception(() => movement.AcceptMovementCancellation(Guid.NewGuid()));
            };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Declining cancellation with incorrect key, should fail")]
        public class DecliningMovementCancellationWithIncorrect
        {
            static TransferMovement movement;
            static Exception _exception;
            static Guid _movementCancellationKey;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _movementCancellationKey = Guid.NewGuid();
                //movement.IssueMovementCancellation(_movementCancellationKey, "Reason");
                //Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                _exception = Catch.Exception(() => movement.DeclineMovementCancellation(Guid.NewGuid()));
            };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Issuing movement cancellation for 2nd time should overwrite previous one")]
        public class IssuingMovementCancellation2NdTimeShouldOverwritePrevious
        {
            static TransferMovement movement;
            static MovementCancellationAccepted _event;
            static Guid _movementCancellationKey;
            static Guid _movementCancellationKey2;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
                _movementCancellationKey = Guid.NewGuid();
                movement.IssueMovementCancellation(_movementCancellationKey, "Reason1");
                Repository.Save(movement, Guid.NewGuid(), null);

                _movementCancellationKey2 = Guid.NewGuid();
                movement.IssueMovementCancellation(_movementCancellationKey2, "Reason2");
                Repository.Save(movement, Guid.NewGuid(), null);
            };

            Because _of = () =>
            {
                movement.AcceptMovementCancellation(_movementCancellationKey2);
                Repository.Save(movement, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<MovementCancellationAccepted>().First();
            };

            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It movementIdShouldEqual = () => _event.MovementId.ShouldEqual(_movementId);
            It cancellationKeyShouldEqual = () => _event.CancellationKey.ShouldEqual(_movementCancellationKey2);
            It siteIdShouldEqual = () => _event.SiteId.ShouldEqual(_siteId);
            It shouldChangePendingCorrectionId = () => movement.PendingCancellation.CancellationKey.ShouldEqual(_movementCancellationKey2);
        }

        [Subject("Using transporter, should raise event")]
        public class UsingTransporter
        {
            static TransferMovement movement;
            static TransporterAdded _event;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                movement.UseTransporter(_transporterId);
                Repository.Save(movement, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<TransporterAdded>().First();
            };
            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It movementIdShouldEqual = () => _event.MovementId.ShouldEqual(_movementId);
            It transporterIdShouldEqual = () => _event.TransporterId.ShouldEqual(_transporterId);
        }

        [Subject("Using not existing transporter, should fail")]
        public class UsingNotExistingTransporter
        {
            static TransferMovement movement;
            static Exception _exception;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets, true);
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () => _exception = Catch.Exception(() => movement.UseTransporter(Guid.NewGuid()));
            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }
    }
    // ReSharper restore InconsistentNaming
}
