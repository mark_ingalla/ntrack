﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Machine.Specifications;
using PalletPlus.Domain;
using PalletPlus.Messages.Events.Supplier;

namespace PalletPlusTests
{
    [Subject(typeof(Supplier), "Supplier tests")]
    public class SupplierTests : TestBase
    {
        public class CreatingSupplier
        {
            static Guid _supplierId;
            static string _companyName;
            static string _companyAccountNo;
            static Supplier _supplier;
            static SupplierCreated _event;

            Establish context = () =>
            {
                Setup();
                _supplierId = Guid.NewGuid();
                _companyName = "CompanyA";
                _companyAccountNo = "1234";
            };

            Because of = () =>
            {
                _supplier = new Supplier(_supplierId, _companyName, _companyAccountNo);
                Repository.Save(_supplier, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<SupplierCreated>().First();
            };

            It eventShouldBeRaised = () => _event.ShouldNotBeNull();
            It supplierIdShouldEqual = () => _event.SupplierId.ShouldEqual(_supplierId);
            It companyNameShouldEqual = () => _event.Name.ShouldEqual(_companyName);
            It companyAccountShouldEqual = () => _event.AccountNumber.ShouldEqual(_companyAccountNo);
        }

        [Subject("Adding asset type for supplier")]
        public class AddingAssetTypeForSupplier
        {
            static Guid _supplierId;
            static string _companyName;
            static string _companyAccountNo;
            static Supplier _supplier;
            static AssetSupplierAdded _event;
            static string _allowedAssetCode;
            static string _allowedAssetName;
            Establish context = () =>
            {
                Setup();
                _supplierId = Guid.NewGuid();
                _companyName = "CompanyA";
                _companyAccountNo = "1234";
                _allowedAssetCode = "Asset";
                _allowedAssetName = "AssetName";
                _supplier = new Supplier(_supplierId, _companyName, _companyAccountNo);
                Repository.Save(_supplier, Guid.NewGuid(), null);
            };

            Because of = () =>
            {
                _supplier.AddAsset(_allowedAssetCode, _allowedAssetName);
                Repository.Save(_supplier, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<AssetSupplierAdded>().First();
            };

            It eventShouldBeRaised = () => _event.ShouldNotBeNull();
            It assetListShouldBeUpdated = () => _supplier.AssetCodes.Count.ShouldBeGreaterThan(0);
            It assetTypeInListShouldEqual = () => _supplier.AssetCodes.Last().ShouldEqual(_allowedAssetCode);
            It supplierIdShouldEqual = () => _event.SupplierId.ShouldEqual(_supplierId);
            It assetTypeShouldBeEqual = () => _event.AssetCode.ShouldEqual(_allowedAssetCode);
            It assetNameShouldEqual = () => _event.AssetName.ShouldEqual(_allowedAssetName);
        }

        public class AllowingAssetTypeForSupplier2NdTimeShouldFail
        {
            static Guid _supplierId;
            static string _companyName;
            static string _companyAccountNo;
            static Supplier _supplier;
            static string _allowedAsset;
            static Exception _exception;
            Establish context = () =>
            {
                Setup();
                _supplierId = Guid.NewGuid();
                _companyName = "CompanyA";
                _companyAccountNo = "1234";
                _allowedAsset = "Asset";
                _supplier = new Supplier(_supplierId, _companyName, _companyAccountNo);
                _supplier.AddAsset(_allowedAsset,"AssetName");
                Repository.Save(_supplier, Guid.NewGuid(), null);
            };

            Because of = () =>
                             {
                                 _exception = Catch.Exception(() => _supplier.AddAsset(_allowedAsset, "AssetName"));
                             };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }
    }
}
