﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Machine.Specifications;
using PalletPlus.Data;
using PalletPlus.Messages.Commands.Account;
using PalletPlus.Messages.Commands.Transporter;
using Raven.Client;

namespace PalletPlusTests
{
    public class SimpleTest : TestBase
    {
        [Subject("Simple test")]
        public class VerySimpleTest
        {
            private static Guid siteId;

            Establish context = () =>
            {
                Setup();

                siteId = PPModelCreator.CreateSite(Bus, "Simple Test City", "123 Simple Test Street");
            };

            Because of = () =>
            {

            };

            It siteShouldExist = () =>
                {
                    Site site = null;
                    while (site == null)
                    {
                        using (IDocumentSession session = DocumentStore.OpenSession())
                        {
                            site = session.Load<Site>(siteId);
                        }
                        Thread.Sleep(1000);
                    }

                    site.ShouldNotBeNull();
                };
        }
    }
}
