﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Machine.Specifications;
using PalletPlus.Domain;
using PalletPlus.Messages.Events.Partner;
using PalletPlus.Messages.Events.Site;

namespace PalletPlusTests
{
    public class SiteTests : TestBase
    {
        [Subject("Creating site, should raise event")]
        public class CreatingSite
        {
            static Guid _accountId;
            static Guid _siteId;
            static string _companyAccountNo;
            static Site _site;
            static SiteCreated _event;

            Establish context = () =>
            {
                Setup();
                _siteId = Guid.NewGuid();
                _companyAccountNo = "1234";
                _siteId = Guid.NewGuid();
                _accountId = Guid.NewGuid();
                Account a = new Account(_accountId, "CompanyName", "CompanyAccountNo");
                Repository.Save(a, Guid.NewGuid(), null);
            };

            Because of = () =>
            {
                _site = new Site(_accountId, _siteId, _companyAccountNo);
                Repository.Save(_site, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<SiteCreated>().First();
            };

            It eventShouldBeRaised = () => _event.ShouldNotBeNull();
            It siteIdShouldBeEqual = () => _event.SiteId.ShouldEqual(_siteId);
            It accountIdShouldBeEqual = () => _event.AccountId.ShouldEqual(_accountId);
            It companyAccountShouldEqual = () => _event.SiteAccountNo.ShouldEqual(_companyAccountNo);
        }

        public class AddingSupplierForSite
        {
            static Guid _siteId;
            static string _companyAccountNo;
            static Site _site;
            static SupplierToSiteAdded _event;
            static Guid _accountId;
            static Guid _supplierId;
            static Supplier _supplier;
            static String _supplierAccount;
            static string _prefix;
            static string _suffix;
            static int _startingSequenceNo;
            Establish context = () =>
            {
                Setup();
                _siteId = Guid.NewGuid();
                _supplierId = Guid.NewGuid();

                _companyAccountNo = "1234";

                _accountId = Guid.NewGuid();
                Account a = new Account(_accountId, "CompanyName", "CompanyAccountNo");
                Repository.Save(a, Guid.NewGuid(), null);

                _prefix = "A";
                _suffix = "B";
                _startingSequenceNo = 1;
                _supplierAccount = "5678";
                _site = new Site(_accountId, _siteId, _companyAccountNo);
                _supplier = new Supplier(_supplierId, "SuppName", _supplierAccount);
                Repository.Save(_supplier, Guid.NewGuid(), null);
                Repository.Save(_site, Guid.NewGuid(), null);
            };

            private Because of = () =>
            {
                _site.AddSupplierToSite(_supplierId, _supplierAccount, _prefix, _suffix, _startingSequenceNo);
                Repository.Save(_site, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<SupplierToSiteAdded>().First();
            };

            It eventShouldBeRaised = () => _event.ShouldNotBeNull();
            It siteIdShouldequal = () => _event.SiteId.ShouldEqual(_siteId);
            It supplierIdShouldequal = () => _event.SupplierId.ShouldEqual(_supplierId);
            It supplierAccountNoShouldEqual = () => _event.SupplierAccountNo.ShouldEqual(_supplierAccount);
            It prefixShouldEqual = () => _event.Prefix.ShouldEqual(_prefix);
            It suffixShouldEqual = () => _event.Suffix.ShouldEqual(_suffix);
            It startingSequenceNoShouldEqual = () => _event.SequenceStartNumber.ShouldEqual(_startingSequenceNo);

        }

        public class AddingNonExistingSupplierForSite
        {
            static Guid _siteId;
            static string _companyAccountNo;
            static Site _site;
            static Exception _exception;
            static Guid _supplierId;
            static Guid _accountId;

            Establish context = () =>
            {
                Setup();
                _siteId = Guid.NewGuid();
                _supplierId = Guid.NewGuid();

                _companyAccountNo = "1234";

                _accountId = Guid.NewGuid();
                Account a = new Account(_accountId, "CompanyName", "CompanyAccountNo");
                Repository.Save(a, Guid.NewGuid(), null);

                _site = new Site(_accountId, _siteId, _companyAccountNo);

                Repository.Save(_site, Guid.NewGuid(), null);
            };

            private Because of = () => _exception = Catch.Exception(() => _site.AddSupplierToSite(_supplierId, "1234", "A", "B", 1));

            It shouldFail = () => _exception.ShouldNotBeNull();
        }

        public class AddingSupplier2NdTimeForSite
        {
            static Guid _siteId;
            static string _companyAccountNo;
            static Site _site;

            static Guid _accountId;

            static Guid _supplierId;
            static String _supplierAccount;
            static Exception _exception;
            Establish context = () =>
            {
                Setup();
                _siteId = Guid.NewGuid();
                _supplierId = Guid.NewGuid();

                _companyAccountNo = "1234";
                _supplierAccount = "5678";
                _accountId = Guid.NewGuid();
                Account a = new Account(_accountId, "CompanyName", "CompanyAccountNo");
                Repository.Save(a, Guid.NewGuid(), null);

                _site = new Site(_accountId, _siteId, _companyAccountNo);
                var supplier = new Supplier(_supplierId, "SuppName", _supplierAccount);
                Repository.Save(supplier, Guid.NewGuid(), null);
                _site.AddSupplierToSite(_supplierId, _supplierAccount, "A", "B", 1);
                Repository.Save(_site, Guid.NewGuid(), null);
            };

            Because of = () => _exception = Catch.Exception(() => _site.AddSupplierToSite(_supplierId, _supplierAccount, "A", "B", 1));

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        public class RemovingSupplierFromSite
        {
            static Guid _siteId;
            static Site _site;
            static SupplierFromSiteRemoved _event;
            static Guid _accountId;
            static Guid _supplierId;
            static string _supplierAccount;
            Establish context = () =>
            {
                Setup();
                _siteId = Guid.NewGuid();
                _supplierId = Guid.NewGuid();

                _accountId = Guid.NewGuid();
                Account a = new Account(_accountId, "CompanyName", "CompanyAccountNo");
                Repository.Save(a, Guid.NewGuid(), null);

                _site = new Site(_accountId, _siteId, "1234");
                _supplierAccount = "12345678";
                var supplier = new Supplier(_supplierId, "SuppName", _supplierAccount);

                Repository.Save(supplier, Guid.NewGuid(), null);
                _site.AddSupplierToSite(_supplierId, _supplierAccount, "A", "B", 1);
                Repository.Save(_site, Guid.NewGuid(), null);
            };

            Because of = () =>
                             {
                                 _site.RemoveSupplierFromSite(_supplierId);
                                 Repository.Save(_site, Guid.NewGuid(), null);
                                 _event = Commit.Events.Select(x => x.Body).OfType<SupplierFromSiteRemoved>().First();
                             };

            It eventShouldBeRaised = () => _event.ShouldNotBeNull();
            It siteIdShouldEqual = () => _event.SiteId.ShouldEqual(_siteId);
            It supplierIdShouldEqual = () => _event.SupplierId.ShouldEqual(_supplierId);

        }

        public class RemovingNotAddedOrExisitngSupplierFromSite
        {
            static Guid _partnerId;
            static string _companyAccountNo;
            static Site _site;
            static Exception _exception;
            static Guid _supplierId;
            static Guid _accountId;
            Establish context = () =>
            {
                Setup();
                _partnerId = Guid.NewGuid();
                _supplierId = Guid.NewGuid();

                _companyAccountNo = "1234";

                _accountId = Guid.NewGuid();
                Account a = new Account(_accountId, "AccountCompanyName", "CompanyAccountNo");
                Repository.Save(a, Guid.NewGuid(), null);

                _site = new Site(_accountId, _partnerId, _companyAccountNo);
                Repository.Save(_site, Guid.NewGuid(), null);
            };

            private Because of = () =>
            {
                _exception = Catch.Exception(() => _site.RemoveSupplierFromSite(_supplierId));
            };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();

        }
    }
}
