﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Machine.Specifications;
using PalletPlus.Domain;
using PalletPlus.Messages.Events.Partner;

namespace PalletPlusTests
{
    public class PartnerTests : TestBase
    {
        [Subject("Creating partner, should raise event")]
        public class CreatingPartner
        {
            static Guid _partnerId;
            static string _companyName;
            static string _companyAccountNo;
            static Partner _partner;
            static PartnerCreated _event;
            Establish context = () =>
            {
                Setup();
                _partnerId = Guid.NewGuid();
                _companyName = "CompanyA";
                _companyAccountNo = "1234";
            };
            Because of = () =>
           {
               _partner = new Partner(_partnerId, _companyName, _companyAccountNo);
               Repository.Save(_partner, Guid.NewGuid(), null);
               _event = Commit.Events.Select(x => x.Body).OfType<PartnerCreated>().First();
           };
            It eventShouldBeRaised = () => _event.ShouldNotBeNull();
            It companyNameShouldEqual = () => _event.Name.ShouldEqual(_companyName);
            It companyAccountShouldEqual = () => _event.AccountNo.ShouldEqual(_companyAccountNo);
        }

        [Subject("Adding supplier for partner, should raise event")]
        public class AddingSupplierForPartner
        {
            static Guid _partnerId;
            static string _companyName;
            static string _companyAccountNo;
            static Partner _partner;
            static SupplierToPartnerAdded _event;
            static Guid _supplierId;
            static Supplier _supplier;
            static String _supplierAccount;
            Establish context = () =>
            {
                Setup();
                _partnerId = Guid.NewGuid();
                _supplierId = Guid.NewGuid();
                _companyName = "CompanyA";
                _companyAccountNo = "1234";
                _supplierAccount = "5678";
                _partner = new Partner(_partnerId, _companyName, _companyAccountNo);
                _supplier = new Supplier(_supplierId, "SuppName", _supplierAccount);

                Repository.Save(_supplier, Guid.NewGuid(), null);
                Repository.Save(_partner, Guid.NewGuid(), null);
            };

            private Because of = () =>
            {
                _partner.AddSupplier(_supplierId, _supplierAccount);
                Repository.Save(_partner, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<SupplierToPartnerAdded>().First();
            };

            It eventShouldBeRaised = () => _event.ShouldNotBeNull();
            It partnerIdShouldEqual = () => _event.ParnterId.ShouldEqual(_partnerId);
            It supplierIdShouldEqual = () => _event.SupplierId.ShouldEqual(_supplierId);
            It supplierAccountShouldEqual = () => _event.SupplierAccountNumber.ShouldEqual(_supplierAccount);
        }

        [Subject("Adding non-existent supplier for partner, should fail")]
        public class AddingNonExistentSupplierForPartner
        {
            static Guid _partnerId;
            static string _companyName;
            static string _companyAccountNo;
            static Partner _partner;
            static Exception _exception;
            static Guid _supplierId;
            Establish context = () =>
            {
                Setup();
                _partnerId = Guid.NewGuid();
                _supplierId = Guid.NewGuid();

                _companyName = "CompanyA";
                _companyAccountNo = "1234";

                _partner = new Partner(_partnerId, _companyName, _companyAccountNo);

                Repository.Save(_partner, Guid.NewGuid(), null);
            };

            private Because of = () => _exception = Catch.Exception(() => _partner.AddSupplier(_supplierId, "1234"));

            It shouldFail = () => _exception.ShouldNotBeNull();
        }

        [Subject("Adding the same supplier twice for partner, should fail")]
        public class AddingSupplier2NdTimeForPartner
        {
            static Guid _partnerId;
            static string _companyName;
            static string _companyAccountNo;
            static Partner _partner;

            static Guid _supplierId;
            static String _supplierAccount;
            static Exception _exception;
            Establish context = () =>
            {
                Setup();
                _partnerId = Guid.NewGuid();
                _supplierId = Guid.NewGuid();

                _companyName = "CompanyA";
                _companyAccountNo = "1234";
                _supplierAccount = "5678";
                _partner = new Partner(_partnerId, _companyName, _companyAccountNo);
                var supplier = new Supplier(_supplierId, "SuppName", _supplierAccount);
                Repository.Save(supplier, Guid.NewGuid(), null);
                _partner.AddSupplier(_supplierId, _supplierAccount);
                Repository.Save(_partner, Guid.NewGuid(), null);
            };

            Because of = () => _exception = Catch.Exception(() => _partner.AddSupplier(_supplierId, _supplierAccount));

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Removing supplier from partner, should raise event")]
        public class RemovingSupplierFromPartner
        {
            static Guid _partnerId;
            static Partner _partner;
            static SupplierFromPartnerRemoved _event;
            static Guid _supplierId;
            Establish context = () =>
            {
                Setup();
                _partnerId = Guid.NewGuid();
                _supplierId = Guid.NewGuid();

                _partner = new Partner(_partnerId, "CompanyA", "1234");
                const string supplierAccount = "12345678";
                var supplier = new Supplier(_supplierId, "SuppName", supplierAccount);

                Repository.Save(supplier, Guid.NewGuid(), null);
                _partner.AddSupplier(_supplierId, supplierAccount);
                Repository.Save(_partner, Guid.NewGuid(), null);
            };

            private Because of = () =>
            {
                _partner.RemoveSupplier(_supplierId);
                Repository.Save(_partner, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<SupplierFromPartnerRemoved>().First();
            };

            It eventShouldBeRaised = () => _event.ShouldNotBeNull();
            It partnerIdShouldEqual = () => _event.PartnerId.ShouldEqual(_partnerId);
            It supplierIdShouldEqual = () => _event.SupplierId.ShouldEqual(_supplierId);
        }

        [Subject("Removing non-existent or not added supplier from partner, should fail")]
        public class RemovingNotAddedOrExisitngSupplierFromPartner
        {
            static Guid _partnerId;
            static string _companyName;
            static string _companyAccountNo;
            static Partner _partner;
            static Exception _exception;
            static Guid _supplierId;
            Establish context = () =>
            {
                Setup();
                _partnerId = Guid.NewGuid();
                _supplierId = Guid.NewGuid();
                _companyName = "CompanyA";
                _companyAccountNo = "1234";
                _partner = new Partner(_partnerId, _companyName, _companyAccountNo);
                Repository.Save(_partner, Guid.NewGuid(), null);
            };
            private Because of = () =>
            {
                _exception = Catch.Exception(() => _partner.RemoveSupplier(_supplierId));
            };
            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Allowing asset type for partner, should raise event")]
        public class AllowingAssetForPartner
        {
            static Guid _partnerId;
            static Partner _partner;
            static AssetTypeToPartnerAllowed _event;
            static Guid _supplierId;

            static string _assetCodeA = "ACode";
            static string _assetNameA = "AName";

            Establish context = () =>
            {
                Setup();
                _partnerId = Guid.NewGuid();
                _supplierId = Guid.NewGuid();

                _partner = new Partner(_partnerId, "CompanyA", "1234");
                const string supplierAccount = "12345678";
                var supplier = new Supplier(_supplierId, "SuppName", supplierAccount);

                Repository.Save(supplier, Guid.NewGuid(), null);

                supplier.AddAsset(_assetCodeA, _assetNameA);
                Repository.Save(supplier, Guid.NewGuid(), null);
                _partner.AddSupplier(_supplierId, supplierAccount);
                Repository.Save(_partner, Guid.NewGuid(), null);
            };

            private Because of = () =>
            {
                _partner.AllowAssetType(_assetCodeA, _supplierId);
                Repository.Save(_partner, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<AssetTypeToPartnerAllowed>().First();
            };

            It eventShouldBeRaised = () => _event.ShouldNotBeNull();
            It partnerIdShouldEqual = () => _event.PartnerId.ShouldEqual(_partnerId);
            It supplierIdShouldEqual = () => _event.SupplierId.ShouldEqual(_supplierId);
            It assetCodeShouldEqual = () => _event.AssetCode.ShouldEqual(_assetCodeA);
            It allowedAssetShouldAddSupplierId = () => _partner.AllowedAssets.ContainsKey(_supplierId).ShouldBeTrue();
            It allowedAssetShouldAddCode = () => _partner.AllowedAssets[_supplierId].Contains(_assetCodeA).ShouldBeTrue();
        }

        [Subject("Allowing not allowed asset type for partner, should raise exception")]
        public class AllowingNotEnabledAssetForPartner
        {
            static Guid _partnerId;
            static Partner _partner;
            static Guid _supplierId;
            static Exception _exception;
            static string _assetCodeA = "ACode";
            static string _assetNameA = "AName";

            Establish context = () =>
            {
                Setup();
                _partnerId = Guid.NewGuid();
                _supplierId = Guid.NewGuid();

                _partner = new Partner(_partnerId, "CompanyA", "1234");
                const string supplierAccount = "12345678";
                var supplier = new Supplier(_supplierId, "SuppName", supplierAccount);

                Repository.Save(supplier, Guid.NewGuid(), null);

                supplier.AddAsset(_assetCodeA, _assetNameA);
                Repository.Save(supplier, Guid.NewGuid(), null);
                _partner.AddSupplier(_supplierId, supplierAccount);
                Repository.Save(_partner, Guid.NewGuid(), null);
            };

            private Because of = () =>
            {
                _exception = Catch.Exception(() => _partner.AllowAssetType("NotAllowedCode", _supplierId));
            };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Allowing in partner asset of non existing supplier, should raise exception")]
        public class AllowingAssetOfNonExistingSupplierForPartner
        {
            static Guid _partnerId;
            static Partner _partner;
            static Guid _supplierId;
            static Exception _exception;
            static string _assetCodeA = "ACode";
            static string _assetNameA = "AName";

            Establish context = () =>
            {
                Setup();
                _partnerId = Guid.NewGuid();
                _supplierId = Guid.NewGuid();

                _partner = new Partner(_partnerId, "CompanyA", "1234");
                const string supplierAccount = "12345678";
                var supplier = new Supplier(_supplierId, "SuppName", supplierAccount);

                Repository.Save(supplier, Guid.NewGuid(), null);

                supplier.AddAsset(_assetCodeA, _assetNameA);
                Repository.Save(supplier, Guid.NewGuid(), null);
                _partner.AddSupplier(_supplierId, supplierAccount);
                Repository.Save(_partner, Guid.NewGuid(), null);
            };

            private Because of = () =>
            {
                _exception = Catch.Exception(() => _partner.AllowAssetType(_assetCodeA, Guid.NewGuid()));
            };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Allowing in partner asset of not allowed supplier, should raise exception")]
        public class AllowingAssetOfNotAllowedSupplierForPartner
        {
            static Guid _partnerId;
            static Partner _partner;
            static Guid _supplierId;
            static Guid _supplierId2;
            static Exception _exception;
            static string _assetCodeA = "ACode";
            static string _assetNameA = "AName";

            Establish context = () =>
            {
                Setup();
                _partnerId = Guid.NewGuid();
                _supplierId = Guid.NewGuid();

                _partner = new Partner(_partnerId, "CompanyA", "1234");
                const string supplierAccount = "12345678";
                var supplier = new Supplier(_supplierId, "SuppName", supplierAccount);
                Repository.Save(supplier, Guid.NewGuid(), null);

                _supplierId2 = Guid.NewGuid();
                var supplier2 = new Supplier(_supplierId2, "Supp2", "Supp2Account");
                Repository.Save(supplier2, Guid.NewGuid(), null);
                supplier2.AddAsset(_assetCodeA, _assetNameA);
                Repository.Save(supplier2, Guid.NewGuid(), null);

                supplier.AddAsset(_assetCodeA, _assetNameA);
                Repository.Save(supplier, Guid.NewGuid(), null);
                _partner.AddSupplier(_supplierId, supplierAccount);
                Repository.Save(_partner, Guid.NewGuid(), null);
            };

            private Because of = () =>
            {
                _exception = Catch.Exception(() => _partner.AllowAssetType(_assetCodeA, _supplierId2));
            };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }


        [Subject("Allowing the same asset twice, should raise exception")]
        public class AllowingAssetTwiceForPartner
        {
            static Guid _partnerId;
            static Partner _partner;
            static Guid _supplierId;
            static Exception _exception;
            static string _assetCodeA = "ACode";
            static string _assetNameA = "AName";

            Establish context = () =>
            {
                Setup();
                _partnerId = Guid.NewGuid();
                _supplierId = Guid.NewGuid();

                _partner = new Partner(_partnerId, "CompanyA", "1234");
                const string supplierAccount = "12345678";
                var supplier = new Supplier(_supplierId, "SuppName", supplierAccount);

                Repository.Save(supplier, Guid.NewGuid(), null);

                supplier.AddAsset(_assetCodeA, _assetNameA);
                Repository.Save(supplier, Guid.NewGuid(), null);
                _partner.AddSupplier(_supplierId, supplierAccount);
                Repository.Save(_partner, Guid.NewGuid(), null);
                _partner.AllowAssetType(_assetCodeA, _supplierId);
                Repository.Save(_partner, Guid.NewGuid(), null);
            };

            private Because of = () =>
            {
                _exception = Catch.Exception(() => _partner.AllowAssetType(_assetCodeA, _supplierId));
            };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Disallowing asset type for partner, should raise event")]
        public class DisallowingAssetForPartner
        {
            static Guid _partnerId;
            static Partner _partner;
            static AssetTypeToPartnerDisallowed _event;
            static Guid _supplierId;

            static string _assetCodeA = "ACode";
            static string _assetNameA = "AName";

            Establish context = () =>
            {
                Setup();
                _partnerId = Guid.NewGuid();
                _supplierId = Guid.NewGuid();

                _partner = new Partner(_partnerId, "CompanyA", "1234");
                const string supplierAccount = "12345678";
                var supplier = new Supplier(_supplierId, "SuppName", supplierAccount);

                Repository.Save(supplier, Guid.NewGuid(), null);

                supplier.AddAsset(_assetCodeA, _assetNameA);
                Repository.Save(supplier, Guid.NewGuid(), null);
                _partner.AddSupplier(_supplierId, supplierAccount);
                Repository.Save(_partner, Guid.NewGuid(), null);
                _partner.AllowAssetType(_assetCodeA, _supplierId);
                Repository.Save(_partner, Guid.NewGuid(), null);
            };

            private Because of = () =>
            {
                _partner.DisallowAssetType(_assetCodeA, _supplierId);
                Repository.Save(_partner, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<AssetTypeToPartnerDisallowed>().First();
            };

            It eventShouldBeRaised = () => _event.ShouldNotBeNull();
            It partnerIdShouldEqual = () => _event.PartnerId.ShouldEqual(_partnerId);
            It supplierIdShouldEqual = () => _event.SupplierId.ShouldEqual(_supplierId);
            It assetCodeShouldEqual = () => _event.AssetCode.ShouldEqual(_assetCodeA);
            It allowedAssetShouldAddSupplierId = () => _partner.AllowedAssets.ContainsKey(_supplierId).ShouldBeTrue();
            It allowedAssetShouldRemoveCode = () => _partner.AllowedAssets[_supplierId].Contains(_assetCodeA).ShouldBeFalse();
        }

        [Subject("Disallowing not allowed asset type for partner, should raise exception")]
        public class DisallowingNotAllowedAssetForPartner
        {
            static Guid _partnerId;
            static Partner _partner;
            static Exception _exception;
            static Guid _supplierId;

            static string _assetCodeA = "ACode";
            static string _assetNameA = "AName";

            Establish context = () =>
                                    {
                                        Setup();
                                        _partnerId = Guid.NewGuid();
                                        _supplierId = Guid.NewGuid();

                                        _partner = new Partner(_partnerId, "CompanyA", "1234");
                                        const string supplierAccount = "12345678";
                                        var supplier = new Supplier(_supplierId, "SuppName", supplierAccount);

                                        Repository.Save(supplier, Guid.NewGuid(), null);

                                        supplier.AddAsset(_assetCodeA, _assetNameA);
                                        Repository.Save(supplier, Guid.NewGuid(), null);
                                        _partner.AddSupplier(_supplierId, supplierAccount);
                                        Repository.Save(_partner, Guid.NewGuid(), null);
                                    };

            Because of = () =>
                             {
                                 _exception = Catch.Exception(() => _partner.DisallowAssetType(_assetCodeA, _supplierId));
                             };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Disallowing partner asset of not allowed supplier, should raise exception")]
        public class DisallowingAllowedAssetOfNotEnabledSupplierForPartner
        {
            static Guid _partnerId;
            static Partner _partner;
            static Exception _exception;
            static Guid _supplierId;

            static string _assetCodeA = "ACode";
            static string _assetNameA = "AName";

            Establish context = () =>
            {
                Setup();
                _partnerId = Guid.NewGuid();
                _supplierId = Guid.NewGuid();

                _partner = new Partner(_partnerId, "CompanyA", "1234");
                const string supplierAccount = "12345678";
                var supplier = new Supplier(_supplierId, "SuppName", supplierAccount);

                Repository.Save(supplier, Guid.NewGuid(), null);

                supplier.AddAsset(_assetCodeA, _assetNameA);
                Repository.Save(supplier, Guid.NewGuid(), null);
                _partner.AddSupplier(_supplierId, supplierAccount);
                Repository.Save(_partner, Guid.NewGuid(), null);
            };

            private Because of = () =>
            {
                _exception = Catch.Exception(() => _partner.DisallowAssetType(_assetCodeA, Guid.NewGuid()));
            };

            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }
    }
}
