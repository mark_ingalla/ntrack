﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonDomain.Persistence;
using Machine.Specifications;
using PalletPlus.Domain;
using PalletPlus.DomainServices.Asset;
using PalletPlus.Messages.Events.Partner;
using PalletPlus.Messages.Events.Supplier;
using Raven.Client;
using StructureMap;

namespace PalletPlusTests
{
    public class AssetTest : TestBase
    {
        static Guid _supplierId;
        static string _companyName;
        static string _companyAccountNo;
        static Supplier _supplier;

        static Guid _partnerId;
        static Partner _partner;

        static string assetType = "AssetType";
        static string assetName = "AssetName";
        static AssetsHandler handler = new AssetsHandler();

        static void SetEnvironment()
        {
            handler = new AssetsHandler();
            handler.Repository = ObjectFactory.GetInstance<IRepository>();
            handler.Session = ObjectFactory.GetInstance<IDocumentSession>();

            _partnerId = Guid.NewGuid();
            _companyName = "CompanyA";
            _companyAccountNo = "1234";
            _partner = new Partner(_partnerId, _companyName, _companyAccountNo);
            Repository.Save(_partner, Guid.NewGuid(), null);

            _supplierId = Guid.NewGuid();
            _supplier = new Supplier(_supplierId, _companyName, _companyAccountNo);
            _supplier.AddAsset(assetType, assetName);
            Repository.Save(_supplier, Guid.NewGuid(), null);

            _partner.AddSupplier(_supplierId, "account");
            Repository.Save(_partner, Guid.NewGuid(), null);
        }

        [Subject("Discontinuing asset, should raise event")]
        public class DiscontinuingAsset
        {
            static AssetSupplierDiscontinued _event;

            Establish context = () =>
            {
                Setup();
                SetEnvironment();
                _partner.AllowAssetType(assetType, _supplierId);
                Repository.Save(_partner, Guid.NewGuid(), null);
                var evt = Commit.Events.Select(x => x.Body).OfType<AssetTypeToPartnerAllowed>().First();
                handler.Handle(evt);
            };
            Because of = () =>
            {
                _supplier.DiscontinueAsset(assetType);
                Repository.Save(_supplier, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<AssetSupplierDiscontinued>().First();
                handler.Handle(_event);
                _partner = Repository.GetById<Partner>(_partnerId);
            };
            It eventShouldBeRaised = () => _event.ShouldNotBeNull();
            It assetCodeShouldEqual = () => _event.AssetCode.ShouldEqual(assetType);
            It supplierIdShouldEqual = () => _event.SupplierId.ShouldEqual(_supplierId);
            It shouldUpdatePartner = () => _partner.AllowedAssets[_supplierId].Count.ShouldEqual(0);
        }

        [Subject("Discontinuing asset not supported by partner, should not change partner data")]
        public class DiscontinuingAssetNotSupportedByPartner
        {
            static AssetSupplierDiscontinued _event;

            Establish context = () =>
            {
                Setup();
                SetEnvironment();
            };
            Because of = () =>
            {
                _supplier.DiscontinueAsset(assetType);
                Repository.Save(_supplier, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<AssetSupplierDiscontinued>().First();
                handler.Handle(_event);
                _partner = Repository.GetById<Partner>(_partnerId);
            };

            It shouldntChangePartnerData = () => _partner.AllowedAssets[_supplierId].Count.ShouldEqual(0);
        }

        [Subject("Discontinuing not existing asset, should throw exception")]
        public class DiscontinuingNotExistingAsset
        {
            static Exception _exception;
            Establish context = () =>
            {
                Setup();
                SetEnvironment();
                _partner.AllowAssetType(assetType, _supplierId);
                Repository.Save(_partner, Guid.NewGuid(), null);
                var evt = Commit.Events.Select(x => x.Body).OfType<AssetTypeToPartnerAllowed>().First();
                handler.Handle(evt);
            };
            Because of = () =>
            {
                _exception = Catch.Exception(() => _supplier.DiscontinueAsset("AssetAssetAsset"));
            };

            It shouldThrowException = () => _exception.ShouldNotBeNull();
        }
    }
}
