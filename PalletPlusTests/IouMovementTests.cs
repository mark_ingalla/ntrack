﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Machine.Specifications;
using PalletPlus.Domain.DomainServices.AssetBalance;
using PalletPlus.Domain.Models;
using PalletPlus.Messages.Events.Movement;
using Raven.Client;
using StructureMap;
using IouMovement = PalletPlus.Domain.DomainServices.IouAssetsBalance.IouMovement;

namespace PalletPlusTests
{
    public class IouMovementTests : MovementTestsBase
    {
        [Subject("Creating IOU movement, should raise event")]
        public class CreatingIouMovement
        {
            static MovementCreated _event;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
            };
            Because _of = () =>
            {
                var movement = new IouMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets);
                Repository.Save(movement, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<IouMovementCreated>().First();
            };
            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It supplierIdShouldEqual = () => _event.SupplierId.ShouldEqual(_supplierId);
            It siteIdShouldEqual = () => _event.SiteId.ShouldEqual(_siteId);
            It partnerIdShouldEqual = () => _event.PartnerId.ShouldEqual(_partnerId);
            It movementIdShouldEqual = () => _event.MovementId.ShouldEqual(_movementId);
            It directionShouldBeTheSame = () => _event.Direction.ToString(CultureInfo.InvariantCulture).ShouldEqual(TransferDirection.Out.ToString());
            It movementDateShouldEqual = () => _event.MovementDate.ShouldEqual(_movementDate);
            It carriedAssetsShouldBeTheSame = () => _event.Assets.ToList().ForEach(x => x.Value.ShouldEqual(_assets[x.Key]));
        }

        [Subject("Creating IOU movement should change IOU balanace")]
        public class CreatingIouMovementShouldChangeIouBalance
        {
            static MovementCreated _event;
            static IouAssetBalanceHandler iouAssetBalanceHandler;
            static IouAssetBalanceService service;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                var movement = new IouMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets);
                Repository.Save(movement, Guid.NewGuid(), null);
                service = new IouAssetBalanceService();
            };
            Because _of = () =>
            {
                _event = Commit.Events.Select(x => x.Body).OfType<IouMovementCreated>().First();
                iouAssetBalanceHandler = new IouAssetBalanceHandler { Session = ObjectFactory.GetInstance<IDocumentSession>() };
                iouAssetBalanceHandler.Handle(_siteCreatedEvent);
                iouAssetBalanceHandler.Handle(_event);
            };
            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It iouAssetBalanceShouldBeUpdated = () => _assets.ToList().ForEach(x => service.Balance(_siteId, _supplierId, x.Key).ShouldEqual(-x.Value));
        }

        [Subject("Cancelled IOU movement should not change IOU balanace")]
        public class CancelledIouMovementShouldntChangeIouBalance
        {
            static IouMovementCreated _event;
            static IouAssetBalanceHandler _iouAssetBalanceHandler;
            static IouAssetBalanceService _service;
            static IouMovement _movement;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                _movement = new IouMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets);
                Repository.Save(_movement, Guid.NewGuid(), null);
                _service = new IouAssetBalanceService();
                _event = Commit.Events.Select(x => x.Body).OfType<IouMovementCreated>().First();
                _iouAssetBalanceHandler = new IouAssetBalanceHandler { Session = ObjectFactory.GetInstance<IDocumentSession>() };
                _iouAssetBalanceHandler.Handle(_siteCreatedEvent);
                _iouAssetBalanceHandler.Handle(_event);
            };
            Because _of = () =>
            {
                _movement.Cancel();
                Repository.Save(_movement, Guid.NewGuid(), null);
                _iouAssetBalanceHandler.Handle(Commit.Events.Select(x => x.Body).OfType<IouMovementCanceled>().First());
            };
            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It iouAssetBalanceShouldBeUpdated = () => _assets.Keys.ToList().ForEach(x => _service.Balance(_siteId, _supplierId, x).ShouldEqual(0));
        }

        [Subject("Creating IOU movement with asset not allowed for supplier, should fail")]
        public class CreatingIouMovementWithIncorrectAssetCodeForSupplier
        {
            static Exception _excecption;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                _assets.Add("C", 1);
            };
            Because _of = () => { _excecption = Catch.Exception(() => new IouMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets)); };
            It shouldRaiseException = () => _excecption.ShouldNotBeNull();
        }

        [Subject("Creating IOU movement with nonexisting site, should fail")]
        public class CreatingIouMovementWithNotExistingSite
        {
            static Exception _excecption;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
            };
            Because _of = () => _excecption = Catch.Exception(() => new IouMovement(_movementId, Guid.NewGuid(), _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets));
            It shouldRaiseException = () => _excecption.ShouldNotBeNull();
        }

        [Subject("Creating IOU movement with nonexisting partner, should fail")]
        public class CreatingMovementWithNotExistingPartner
        {
            static Exception _excecption;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
            };
            Because _of = () => _excecption = Catch.Exception(() => new IouMovement(_movementId, _siteId, Guid.NewGuid(), TransferDirection.Out, _movementDate, _supplierId, _assets));
            It shouldRaiseException = () => _excecption.ShouldNotBeNull();
        }

        [Subject("Creating Iou movement with not enabled partner, should fail")]
        public class CreatingIouMovementWithNotEnabledPartner
        {
            static Exception _excecption;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                partner.RemoveSupplier(_supplierId);
                Repository.Save(partner, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                _excecption = Catch.Exception(() => new TransferMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets));
            };
            It shouldRaiseException = () => _excecption.ShouldNotBeNull();
        }

        [Subject("Creating IOU movement with not enabled asset code, should fail")]
        public class CreatingIouMovementWithNotEnabledAssetCode
        {
            static Exception _excecption;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                partner.DisallowAssetType(_assetA, _supplierId);
                Repository.Save(partner, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                _excecption = Catch.Exception(() => new IouMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets));
            };
            It shouldRaiseException = () => _excecption.ShouldNotBeNull();
        }

        [Subject("Creating IOU movement with nonexisting supplier, should fail")]
        public class CreatingMovementWithNonExistingSupplier
        {
            static Exception _excecption;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
            };
            Because _of = () =>
            {
                _excecption = Catch.Exception(() => new IouMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, Guid.NewGuid(), _assets));
            };
            It shouldRaiseException = () => _excecption.ShouldNotBeNull();
        }

        [Subject("Creating IOU movement without assets, should fail")]
        public class CreatingNonPtaMovementWithoutAssets
        {
            static Exception _excecption;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
            };
            Because _of = () =>
            {
                _assets = new Dictionary<string, int>();
                _excecption = Catch.Exception(() => new IouMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets));
            };
            It shouldRaiseException = () => _excecption.ShouldNotBeNull();
        }

        [Subject("Updating IOU movement references, should raise event")]
        public class UpdatingIouReferences
        {
            static IouReferencesUpdated _event;
            static IouMovement movement;
            static string _movRef = "MovementRef";
            static string _partRef = "PartnerRef";
            static string _transpRef = "TransporterRef";
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new IouMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets);
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
                              {
                                  movement.UpdateReferences(_movRef, _partRef, _transpRef);
                                  Repository.Save(movement, Guid.NewGuid(), null);
                                  _event = Commit.Events.Select(x => x.Body).OfType<IouReferencesUpdated>().First();
                              };
            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It movementIdShouldEqual = () => _event.MovementId.ShouldEqual(_movementId);
            It movementReferenceShouldEqual = () => _event.MovementReference.ShouldEqual(_movRef);
            It partnerReferenceShouldEqual = () => _event.PartnerReference.ShouldEqual(_partRef);
            It transporterReferenceShouldEqual = () => _event.TransporterReference.ShouldEqual(_transpRef);
        }

        [Subject("Cancelling IOU movement, should raise event")]
        public class CancellingIouMovement
        {
            static IouMovementCanceled _event;
            static IouMovement movement;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new IouMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets);
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                movement.Cancel();
                Repository.Save(movement, Guid.NewGuid(), null);
                _event = Commit.Events.Select(x => x.Body).OfType<IouMovementCanceled>().First();
            };
            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It movementIdShouldEqual = () => _event.MovementId.ShouldEqual(_movementId);
            It siteIdShouldEqual = () => _event.SiteId.ShouldEqual(_siteId);
        }

        [Subject("Cancelling cancelled IOU movement, should fail")]
        public class CancellingCancelledIouMovement
        {
            static IouMovement movement;
            static Exception _exception;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new IouMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets);
                Repository.Save(movement, Guid.NewGuid(), null);
                movement.Cancel();
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                _exception = Catch.Exception(() => movement.Cancel());
            };
            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }

        [Subject("Updating cancelled IOU movement, should fail")]
        public class UpdaingCancelledIouMovement
        {
            static IouMovement movement;
            static Exception _exception;
            Establish _context = () =>
            {
                Setup();
                CreateEnvironment();
                movement = new IouMovement(_movementId, _siteId, _partnerId, TransferDirection.Out, _movementDate, _supplierId, _assets);
                Repository.Save(movement, Guid.NewGuid(), null);
                movement.Cancel();
                Repository.Save(movement, Guid.NewGuid(), null);
            };
            Because _of = () =>
            {
                _exception = Catch.Exception(() => movement.UpdateReferences("A", "B", "C"));
            };
            It shouldRaiseException = () => _exception.ShouldNotBeNull();
        }
    }
}
