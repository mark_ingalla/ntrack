﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Machine.Specifications;
using PalletPlus.Domain.Models;
using PalletPlus.Messages.Commands.Account;
using PalletPlus.Messages.Commands.Transporter;
using Raven.Client;
using Movement = PalletPlus.Data.Movement;
using Site = PalletPlus.Data.Site;

namespace PalletPlusTests
{
    public class MovementTest : TestBase
    {
        [Subject("Transfering between 2 sites and test Internal, Transfert or IOU movement")]
        public class OutMovementTest
        {
            private static Guid _siteId1;
            private static Dictionary<string, string> _equipments;
            private static Guid _supplierId1;
            private static Guid _transporterId;

            private static Guid _movementId1;
            private static Guid _movementId2;
            private static Guid _movementId3;
            private static Guid _movementId4;

            Establish context = () =>
            {
                Setup();

                _siteId1 = PPModelCreator.CreateSite(Bus, "Sydney", "123 Some Street");

                _equipments = new Dictionary<string, string>();
                _equipments.Add("TyleCode", "TyleName");

                _supplierId1 = PPModelCreator.AddSupplier(Bus, "Supplier1", "AccountNo1", _equipments);
                PPModelCreator.AddSupplierToSite(Bus, _siteId1, _supplierId1, "AccountNo1");

                //Transporter
                _transporterId = Guid.NewGuid();
                Bus.Send(new CreateTransporter()
                    {
                        Id = _transporterId,
                        Name = "DHL"
                    });
            };

            Because of = () =>
            {
                //Internal Movement
                {
                    var siteId2 = PPModelCreator.CreateSite(Bus, "Melbourne", "2 Site Street");
                    PPModelCreator.AddSupplierToSite(Bus, siteId2, _supplierId1, "AccountNo1");

                    Dictionary<string, int> equipementsToMove = new Dictionary<string, int>();
                    equipementsToMove.Add(_equipments.First().Key, 247);
                    _movementId1 = PPModelCreator.CreateMovement(Bus, _siteId1, siteId2, _supplierId1, equipementsToMove,
                                                    _transporterId, TransferDirection.Out);
                }

                //External Movement
                {
                    var partnerId1 = PPModelCreator.CreatePartner(Bus, _supplierId1, "AccountNumberPartner1",
                                                                    _equipments.Keys.ToArray(), false);

                    Dictionary<string, int> equipementsToMove = new Dictionary<string, int>();
                    equipementsToMove.Add(_equipments.First().Key, 247);
                    _movementId2 = PPModelCreator.CreateMovement(Bus, _siteId1, partnerId1, _supplierId1, equipementsToMove,
                                                    _transporterId, TransferDirection.Out);
                }

                //External Movement (partner is IOU but PartnerSupplierInfo have a AccountNumber)
                {
                    Guid partnerId2 = PPModelCreator.CreatePartner(Bus, _supplierId1, "AccountNumberPartner2",
                                                                    _equipments.Keys.ToArray(), true);

                    Dictionary<string, int> equipementsToMove = new Dictionary<string, int>();
                    equipementsToMove.Add(_equipments.First().Key, 247);
                    _movementId3 = PPModelCreator.CreateMovement(Bus, _siteId1, partnerId2, _supplierId1, equipementsToMove,
                                                    _transporterId, TransferDirection.Out);
                }

                //IOU Movement
                {
                    Guid partnerId3 = PPModelCreator.CreatePartner(Bus, _supplierId1, "", _equipments.Keys.ToArray(),
                                                                    true);

                    Dictionary<string, int> equipementsToMove = new Dictionary<string, int>();
                    equipementsToMove.Add(_equipments.First().Key, 247);
                    _movementId4 = PPModelCreator.CreateMovement(Bus, _siteId1, partnerId3, _supplierId1, equipementsToMove,
                                                    _transporterId, TransferDirection.Out);
                }
            };

            It movement1ShouldBeInternal = () =>
            {
                Movement movement = null;
                while (movement == null)
                {
                    using (IDocumentSession session = DocumentStore.OpenSession())
                    {
                        movement = session.Load<Movement>(_movementId1);
                    }
                    Thread.Sleep(1000);
                }

                movement.MovementType.ShouldEqual(MovementTransferType.Internal.ToString());
            };

            It movement2ShouldBeATransfer = () =>
            {
                Movement movement = null;
                while (movement == null)
                {
                    using (IDocumentSession session = DocumentStore.OpenSession())
                    {
                        movement = session.Load<Movement>(_movementId2);
                    }
                    Thread.Sleep(1000);
                }

                movement.MovementType.ShouldEqual(MovementTransferType.Transfer.ToString());
            };

            It movement3ShouldBeATransfer = () =>
            {
                Movement movement = null;
                while (movement == null)
                {
                    using (IDocumentSession session = DocumentStore.OpenSession())
                    {
                        movement = session.Load<Movement>(_movementId3);
                    }
                    Thread.Sleep(1000);
                }

                movement.MovementType.ShouldEqual(MovementTransferType.Transfer.ToString());
            };

            It movement4ShouldBeIOU = () =>
            {
                Movement movement = null;
                while (movement == null)
                {
                    using (IDocumentSession session = DocumentStore.OpenSession())
                    {
                        movement = session.Load<Movement>(_movementId4);
                    }
                    Thread.Sleep(1000);
                }

                movement.MovementType.ShouldEqual(MovementTransferType.IOU.ToString());
            };
        }
    }
}
