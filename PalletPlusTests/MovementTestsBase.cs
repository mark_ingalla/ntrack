using System;
using System.Collections.Generic;
using System.Linq;
using PalletPlus.Domain.DomainServices.AssetBalance;
using PalletPlus.Domain.DomainServices.DocketNumberService;
using PalletPlus.Domain.Models;
using PalletPlus.Messages.Events.Site;
using Raven.Client;
using StructureMap;

namespace PalletPlusTests
{
    public class MovementTestsBase : TestBase
    {
        protected static Guid _siteId;
        protected static Guid _partnerId;
        protected static Guid _supplierId;
        protected static Dictionary<string, int> _assets;
        protected static Guid _movementId;
        protected static DateTime _movementDate;
        protected static string _assetA = "A";
        protected static string _assetB = "B";
        protected static int _movedAssetAQty = 3;
        protected static int _movedAssetBQty = 7;
        protected static AssetBalanceHandler _assetBalanceHandler;
        protected static DocketNumberHandler _docketNumberHandler;
        protected static Supplier _supplier;
        protected static Guid _transporterId;
        protected static SiteCreated _siteCreatedEvent;
        protected static Partner partner;

        protected static void CreateEnvironment()
        {
            var accountId = Guid.NewGuid();
            _movementId = Guid.NewGuid();
            _siteId = Guid.NewGuid();
            _partnerId = Guid.NewGuid();
            _supplierId = Guid.NewGuid();
            _transporterId = Guid.NewGuid();
            _movementDate = DateTime.Now;
            var account = new Account(accountId, "CompanyName", "CompanyAccount");
            Repository.Save(account, Guid.NewGuid(), null);
            var site = new Site(accountId, _siteId, "SiteAccountNo");
            partner = new Partner(_partnerId, "PartnerName", "PartnerAccountNo");
            _supplier = new Supplier(_supplierId, "SupplierName", "SupplierAccountNo");
            Repository.Save(account, Guid.NewGuid(), null);

            Repository.Save(new Transporter(_transporterId, "TransporterCode", "TransporterName"), Guid.NewGuid(), null);

            _supplier.AddAsset(_assetA, "AName");
            _supplier.AddAsset(_assetB, "BName");
            Repository.Save(_supplier, Guid.NewGuid(), null);
            partner.AddSupplier(_supplierId,"SupplierAccountNo");
            Repository.Save(partner, Guid.NewGuid(), null);

            partner.AllowAssetType(_assetA,_supplierId);
            partner.AllowAssetType(_assetB, _supplierId);
            Repository.Save(partner, Guid.NewGuid(), null);

            account.AddPartner(_partnerId, "PartnerAccountNo");
            Repository.Save(account, Guid.NewGuid(), null);

            site.AddSupplierToSite(_supplierId, "SupplierAccountNo", "Prefix", "Suffix", 0);
            site.AddPartner(_partnerId, "PartnerAccountNo");

            Repository.Save(site, Guid.NewGuid(), null);

            _siteCreatedEvent = Commit.Events.Select(x => x.Body).OfType<SiteCreated>().First();

            _assetBalanceHandler = new AssetBalanceHandler { Session = ObjectFactory.GetInstance<IDocumentSession>() };
            _assetBalanceHandler.Handle(_siteCreatedEvent);

            _docketNumberHandler = new DocketNumberHandler { Session = ObjectFactory.GetInstance<IDocumentSession>() };
            _docketNumberHandler.Handle(Commit.Events.Select(x => x.Body).OfType<SupplierToSiteAdded>().First());

            account.AddSite(_siteId);
            Repository.Save(account, Guid.NewGuid(), null);

            _assets = new Dictionary<string, int> { { _assetA, _movedAssetAQty }, { _assetB, _movedAssetBQty } };
        }
    }
}