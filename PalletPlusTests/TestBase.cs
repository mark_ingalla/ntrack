﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonDomain;
using CommonDomain.Core;
using CommonDomain.Persistence;
using CommonDomain.Persistence.EventStore;
using EventStore;
using EventStore.Dispatcher;
using NServiceBus;
using Raven.Client.Document;
using nTrack.Core;
using PalletPlus.AppHost.Infrastructure;
using Raven.Client;
using Raven.Client.Embedded;
using StructureMap;
using nTrack.Data;

namespace PalletPlusTests
{
    public class TestBase
    {
        public static Guid accountId = new Guid("12345678-513f-4580-a9c8-07fe61a411ab");
        public static string QueueName = "palletplus.service";

        public static DocumentStore DocumentStore { get; set; }
        public static IStoreEvents Store { get; set; }
        public static ICommonRepository Repository { get; set; }
        public static Commit Commit { get; set; }
        public static IBus Bus { get; set; }


        public static void Setup()
        {
            SetupBus();

            //SetupEventStore();

            SetupDocumentStore();

            //SetupStructureMap();
        }

        public static void SetupBus()
        {
            Bus = Configure.With()
                .DefineEndpointName("ntrack.test")
                .DefaultBuilder()
                .DefiningCommandsAs(type => type != null && type.Namespace != null && type.Namespace.Contains("Messages.Commands"))
                .XmlSerializer()
                .Log4Net()
                .MsmqTransport()
                .UnicastBus()
                .SendOnly();

            Bus.OutgoingHeaders.Add("nTrack.AccountId", accountId.ToString());
        }

        public static void SetupEventStore()
        {
            Store = Wireup.Init()
                          .UsingInMemoryPersistence()
                          .InitializeStorageEngine()
                          .UsingSynchronousDispatchScheduler()
                          .DispatchTo(new DelegateMessageDispatcher(Dispatch))
                          .Build();
        }

        public static void SetupDocumentStore()
        {
            //
            DocumentStore nTrackCentral = new DocumentStore();
            nTrackCentral.ConnectionStringName = "SystemDBTest";
            nTrackCentral.Initialize();
            string databaseName;
            using (IDocumentSession session = nTrackCentral.OpenSession())
            {
                AccountDatabase ad = session.Load<AccountDatabase>(accountId);
                databaseName = ad.DatabaseName;
            }

            //
            DocumentStore = new DocumentStore();
            DocumentStore.ConnectionStringName = "ClientDBTest";
            DocumentStore.DefaultDatabase = databaseName;
            DocumentStore.Initialize();
        }

        private static void SetupStructureMap()
        {
            ObjectFactory.Configure(
                x => x.Scan(
                    scan =>
                    {
                        scan.WithDefaultConventions();
                        scan.TheCallingAssembly();
                        scan.LookForRegistries();
                        scan.AddAllTypesOf(typeof(IHandleMessages<>));
                        scan.ConnectImplementationsToTypesClosing(typeof(IHandleMessages<>));
                    }));

            Bus = Configure.With()
                .DefineEndpointName(QueueName)
                .StructureMapBuilder(ObjectFactory.Container)
                .DefiningCommandsAs(type => type != null && type.Namespace != null && type.Namespace.Contains("Messages.Commands"))
                .DefiningEventsAs(type => type != null && type.Namespace != null && type.Namespace.Contains("Messages.Events"))
                .DefiningMessagesAs(type => type != null && type.Namespace != null && type.Namespace.StartsWith("PalletPlus.Messages"))
                .XmlSerializer()
                .MsmqTransport()
                .InMemorySubscriptionStorage()
                .RunTimeoutManagerWithInMemoryPersistence()
                .UnicastBus()
                .CreateBus()
                .Start();

            var accountId = Guid.Parse("77c2263a-8d9c-4959-a13a-e9b86b4138c0");
            var userId = Guid.Parse("ad2ca212-d8be-4624-ab51-aa3e5104e877");
            Bus.OutgoingHeaders.Add("nTrack.AccountId", accountId.ToString());
            Bus.OutgoingHeaders.Add("nTrack.UserId", userId.ToString());

            ObjectFactory.Configure(
                x =>
                    {
                        x.ForSingletonOf<IStoreEvents>().Use(Store);
                        x.ForSingletonOf<IConstructAggregates>().Use<AggregateFactory>();
                        x.ForSingletonOf<IRepository>().Use<EventStoreRepository>();
                        x.ForSingletonOf<IDetectConflicts>().Use<ConflictDetector>();
                        x.ForSingletonOf<DocumentStore>().Use(DocumentStore);
                        x.ForSingletonOf<IDocumentStore>().Use(DocumentStore);
                        x.ForSingletonOf<ICommonRepository>().Use<CommonRepository>();
                        x.ForSingletonOf<IDocumentSession>()
                         .Use(ct => ct.GetInstance<IDocumentStore>().Initialize().OpenSession());
                    });

            Repository = ObjectFactory.GetInstance<ICommonRepository>();

            /*Bus = Configure.With()
                .DefineEndpointName(QueueName)
                .StructureMapBuilder(ObjectFactory.Container)
                //.DefiningCommandsAs(type => type != null && type.Namespace != null && type.Namespace.Contains("Messages.Commands"))
                .DefiningCommandsAs(type => type != null && type.Namespace != null && type.Namespace.Contains("Messages.Events"))
                .XmlSerializer()
                .MsmqTransport()
                .InMemorySubscriptionStorage()
                .RunTimeoutManagerWithInMemoryPersistence()
                .UnicastBus()
                    .LoadMessageHandlers()
                .CreateBus()
                .Start();*/
            
        }

        public static void ReplyEvents()
        {
            var estore = Wireup.Init()
                .UsingInMemoryPersistence()
                .InitializeStorageEngine()
                .UsingSynchronousDispatchScheduler()
                    .DispatchTo(new DelegateMessageDispatcher(Dispatch))
                .Build();

            var commits = estore.Advanced.GetFrom(DateTime.MinValue).ToList();

            foreach (var commit in commits)
            {
                for (var i = 0; i < commit.Events.Count; i++)
                {
                    var eventMessage = commit.Events[i];
                    var busMessage = eventMessage.Body;
                    AppendHeaders(busMessage, commit.Headers); // optional
                    AppendHeaders(busMessage, eventMessage.Headers); // optional
                    AppendVersion(commit, i); // optional
                    Bus.Send(QueueName, busMessage);
                }
            }

            estore.Dispose();
        }

        #region Publishing Events

        const string AggregateIdKey = "AggregateId";
        const string CommitVersionKey = "CommitVersion";
        const string EventVersionKey = "EventVersion";

        static void AppendHeaders(object message, IEnumerable<KeyValuePair<string, object>> headers)
        {
            //headers = headers.Where(x => x.Key.StartsWith(BusPrefixKey));
            foreach (var header in headers)
            {
                var key = header.Key;
                var value = (header.Value ?? string.Empty).ToString();
                message.SetHeader(key, value);
            }
        }
        static void AppendVersion(Commit commit, int index)
        {
            var busMessage = commit.Events[index].Body;
            busMessage.SetHeader(AggregateIdKey, commit.StreamId.ToString());
            busMessage.SetHeader(CommitVersionKey, commit.StreamRevision.ToString());
            busMessage.SetHeader(EventVersionKey, GetSpecificEventVersion(commit, index).ToString());
        }
        static int GetSpecificEventVersion(Commit commit, int index)
        {
            // e.g. (StreamRevision: 120) - (5 events) + 1 + (index @ 4: the last index) = event version: 120
            return commit.StreamRevision - commit.Events.Count + 1 + index;
        }

        #endregion

        private static void Dispatch(Commit commit)
        {
            foreach (var eventMessage in commit.Events)
            {
                Bus.Send(QueueName, eventMessage.Body);
            }
            Commit = commit;
        }
    }
}
