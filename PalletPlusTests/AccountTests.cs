﻿using System;
using System.Linq;
using CommonDomain.Persistence;
using Machine.Specifications;
using PalletPlus.Domain;
using PalletPlus.Messages.Events.Account;
using It = Machine.Specifications.It;

namespace PalletPlusTests
{
    public class AccountTest : TestBase
    {
        [Subject("Creating account, should raise event")]
        public class CreatingAccount
        {
            static Guid _accountId;
            static string _companyName;
            static string _companyAccountNo;
            static Account _account;
            static AccountCreated _event;
            Establish context = () =>
            {
                Setup();
                _accountId = Guid.NewGuid();
                _companyName = "CompanyA";
                _companyAccountNo = "1234";

            };
            Because of = () =>
                                    {
                                        _account = new Account(_accountId, _companyName, _companyAccountNo);
                                        Repository.Save(_account, Guid.NewGuid());
                                        _event = Commit.Events.Select(x => x.Body).OfType<AccountCreated>().First();
                                    };

            It shouldRaiseEvent = () => _event.ShouldNotBeNull();
            It companyNameShouldEqual = () => _event.CompanyName.ShouldEqual(_companyName);
            It companyAccountShouldEqual = () => _event.CompanyAccountNo.ShouldEqual(_companyAccountNo);
        }

        [Subject("Adding partner to account, should raise event")]
        public class AddingPartnerToAccount
        {
            static Partner _partner;
            static Account _account;
            static Guid _accountId;
            static Guid _partnerId;
            static string _partnerName;
            static string _partnerAccountNumber;
            static PartnerToAccountAdded _evt;
            Establish context = () =>
            {
                Setup();
                _accountId = Guid.NewGuid();
                _partnerId = Guid.NewGuid();

                _account = new Account(_accountId, "Test", "Test");

                _partnerAccountNumber = "1234";
                _partnerName = "Partner";

                _partner = new Partner(_partnerId, _partnerName, _partnerAccountNumber);

                Repository.Save(_account, Guid.NewGuid());
                Repository.Save(_partner, Guid.NewGuid());
            };

            Because of = () =>
                             {
                                 _account.AddPartner(_partnerId, _partnerAccountNumber);
                                 Repository.Save(_account, Guid.NewGuid());
                                 _evt = Commit.Events.Select(x => x.Body).OfType<PartnerToAccountAdded>().First();
                             };

            It shouldRaiseEvent = () => _evt.ShouldNotBeNull();
            It accountIdShouldBeEqual = () => _evt.AccountId.ShouldEqual(_accountId);
            It partnerIdShouldBeEqual = () => _evt.PartnerId.ShouldEqual(_partnerId);
            It partnerAccountShouldBeEqual = () => _evt.PartnerAccountNo.ShouldEqual(_partnerAccountNumber);

            It shouldAddNewPartner = () => _account.AccountPartners.Count.ShouldEqual(1);
            It addedPartnerIdShouldBeEqual = () => _account.AccountPartners.Last().ShouldEqual(_partnerId);
        }

        [Subject("Adding to account the same partner twice, should fail")]
        public class AddingPartnerFor2NdTime
        {
            static Exception _exception;
            static Partner _partner;
            static Account _account;
            static Guid _accountId;
            static Guid _partnerId;
            static string _partnerName;
            static string _partnerAccountNumber;
            Establish context = () =>
            {
                Setup();
                _accountId = Guid.NewGuid();
                _partnerId = Guid.NewGuid();

                _account = new Account(_accountId, "Test", "Test");
                _partnerAccountNumber = "1234";
                _partnerName = "Partner";
                _partner = new Partner(_partnerId, _partnerName, _partnerAccountNumber);
                Repository.Save(_account, Guid.NewGuid());
                Repository.Save(_partner, Guid.NewGuid());
            };

            Because of = () =>
            {
                _account.AddPartner(_partnerId, _partnerAccountNumber);
                Repository.Save(_account, Guid.NewGuid());
                _exception = Catch.Exception(() => _account.AddPartner(_partnerId, _partnerAccountNumber));
            };

            It should_fail = () => _exception.ShouldNotBeNull();
        }

        [Subject("Adding to account non-existent partner, should fail")]
        public class AddingNonExistentPartnerToAccount
        {
            static Account _account;
            static Guid _accountId;
            static Guid _partnerId;
            protected static Exception Exception { get; set; }
            Establish context = () =>
            {
                Setup();
                _accountId = Guid.NewGuid();
                _partnerId = Guid.NewGuid();
                _account = new Account(_accountId, "Test", "Test");                
                Repository.Save(_account, Guid.NewGuid());              
            };
            Because of = () =>
            {
                Exception = Catch.Exception(() => _account.AddPartner(_partnerId, "1234"));
            };
            It shouldFail = () => Exception.ShouldNotBeNull();
        }

        [Subject("Disabling partner in account, should raise event")]
        public class DisablingPartnerInAccount
        {
            static Partner _partner;
            static Account _account;
            static Guid _accountId;
            static Guid _partnerId;
            static PartnerInAccountDisabled _evt;
            Establish context = () =>
            {
                Setup();
                _accountId = Guid.NewGuid();
                _partnerId = Guid.NewGuid();
                _account = new Account(_accountId, "Test", "Test");
                const string partnerAccountNumber = "1234";
                _partner = new Partner(_partnerId, "Partner", partnerAccountNumber);
                Repository.Save(_partner, Guid.NewGuid());
                _account.AddPartner(_partnerId, partnerAccountNumber);
                Repository.Save(_account, Guid.NewGuid());
            };
            Because of = () =>
            {
                _account.DisablePartner(_partnerId);
                Repository.Save(_account, Guid.NewGuid());
                _evt = Commit.Events.Select(x => x.Body).OfType<PartnerInAccountDisabled>().First();
            };
            It shouldRaiseEvent = () => _evt.ShouldNotBeNull();
            It accountIdShouldBeEqual = () => _evt.AccountId.ShouldEqual(_accountId);
            It partnerIdShouldBeEqual = () => _evt.PartnerId.ShouldEqual(_partnerId);
            It shouldDisablePartner = () => _account.AccountPartners.Count.ShouldEqual(0);
        }

        [Subject("Disabling not enabled partner in account, should fail")]
        public class DisablingNotEnabledPartner
        {
            static Exception Exception;
            static Account _account;
            static Guid _accountId;
            static Guid _partnerId;
            Establish context = () =>
            {
                Setup();
                _accountId = Guid.NewGuid();
                _partnerId = Guid.NewGuid();
                _account = new Account(_accountId, "Test", "Test");
                Repository.Save(_account, Guid.NewGuid());
            };
            Because of = () =>
            {
                Exception = Catch.Exception(() => _account.DisablePartner(_partnerId));
                Repository.Save(_account, Guid.NewGuid());
            };
            It should_fail = () => Exception.ShouldNotBeNull();
        }

        [Subject("Adding site to account, should raise event")]
        public class AddingSiteToAccount
        {
            static Site _site;
            static Account _account;
            static Guid _accountId;
            static Guid _siteId;
            static SiteToAccountAdded _evt;
            Establish context = () =>
            {
                Setup();
                _accountId = Guid.NewGuid();
                _siteId = Guid.NewGuid();
                _account = new Account(_accountId, "Test", "Test");
                Repository.Save(_account, Guid.NewGuid());
                _site = new Site(_accountId, _siteId, "1234");                
                Repository.Save(_site, Guid.NewGuid());
            };
            Because of = () =>
            {
                _account.AddSite(_siteId);
                Repository.Save(_account, Guid.NewGuid());
                _evt = Commit.Events.Select(x => x.Body).OfType<SiteToAccountAdded>().First();
            };
            It shouldRaiseEvent = () => _evt.ShouldNotBeNull();
            It accountIdShouldBeEqual = () => _evt.AccountId.ShouldEqual(_accountId);
            It siteIdShouldBeEqual = () => _evt.SiteId.ShouldEqual(_siteId);
            It shouldAddNewSite = () => _account.Sites.Count.ShouldEqual(1);
            It addedSiteIdShouldBeEqual = () => _account.Sites.Last().ShouldEqual(_siteId);
        }

        [Subject("Adding to account the same site twice, should fail")]
        public class AddingSiteFor2NdTime
        {
            static Exception Exception;
            static Site _site;
            static Account _account;
            static Guid _accountId;
            static Guid _siteId;
            static string _siteAccountNumber;
            Establish context = () =>
            {
                Setup();
                _accountId = Guid.NewGuid();
                _siteId = Guid.NewGuid();

                _account = new Account(_accountId, "Test", "Test");
                Repository.Save(_account, Guid.NewGuid());
                _siteAccountNumber = "1234";

                _site = new Site(_accountId, _siteId, _siteAccountNumber);
                
                Repository.Save(_site, Guid.NewGuid());
            };

            Because of = () =>
            {
                _account.AddSite(_siteId);
                Repository.Save(_account, Guid.NewGuid());
                
                Exception = Catch.Exception(() => _account.AddSite(_siteId));
            };

            It should_fail = () => Exception.ShouldNotBeNull();
        }

        [Subject("Adding to account non-existent site, should fail")]
        public class AddingNonExistingSiteToAccount
        {
            static Account _account;
            static Guid _accountId;
            static Guid _siteId;
            protected static Exception Exception { get; set; }
            Establish context = () =>
            {
                Setup();
                _accountId = Guid.NewGuid();
                _siteId = Guid.NewGuid();
                _account = new Account(_accountId, "Test", "Test");
                Repository.Save(_account, Guid.NewGuid());
            };
            Because of = () =>
            {
                Exception = Catch.Exception(() => _account.AddSite(_siteId));
            };
            It shouldFail = () => Exception.ShouldNotBeNull();
        }

        [Subject("Closing site in account, should raise event")]
        public class ClosingSiteInAccount
        {
            static Site _site;
            static Account _account;
            static Guid _accountId;
            static Guid _siteId;
            static SiteInAccountClosed _evt;
            Establish context = () =>
            {
                Setup();
                _accountId = Guid.NewGuid();
                _siteId = Guid.NewGuid();
                _account = new Account(_accountId, "Test", "Test");
                Repository.Save(_account, Guid.NewGuid());
                _site = new Site(_accountId, _siteId, "1234");
                Repository.Save(_site, Guid.NewGuid());
                _account.AddSite(_siteId);
                Repository.Save(_account, Guid.NewGuid());                
            };

            Because of = () =>
            {                
                _account.CloseSite(_siteId);
                Repository.Save(_account, Guid.NewGuid());
                _evt = Commit.Events.Select(x => x.Body).OfType<SiteInAccountClosed>().First();
            };

            It shouldRaiseEvent = () => _evt.ShouldNotBeNull();
            It accountIdShouldBeEqual = () => _evt.AccountId.ShouldEqual(_accountId);
            It siteIdShouldBeEqual = () => _evt.SiteId.ShouldEqual(_siteId);
            It shouldDeleteSiteIdFromList = () => _account.Sites.Count.ShouldEqual(0);
        }

        [Subject("Closing the same site twice in account, should fail")]
        public class ClosingSite2NdTime
        {
            static Exception _exception;
            static Site _site;
            static Account _account;
            static Guid _accountId;
            static Guid _siteId;
            static string _siteAccountNumber;
            Establish context = () =>
            {
                Setup();
                _accountId = Guid.NewGuid();
                _siteId = Guid.NewGuid();
                _account = new Account(_accountId, "Test", "Test");
                Repository.Save(_account,Guid.NewGuid());
                _siteAccountNumber = "1234";
                _site = new Site(_accountId, _siteId, _siteAccountNumber);
                Repository.Save(_site, Guid.NewGuid());
                _account.AddSite(_siteId);
                Repository.Save(_account, Guid.NewGuid());                
            };
            Because of = () =>
            {
                _account.CloseSite(_siteId);
                Repository.Save(_account, Guid.NewGuid());
                _exception = Catch.Exception(() => _account.CloseSite(_siteId));
            };
            It should_fail = () => _exception.ShouldNotBeNull();
        }

    }
}
