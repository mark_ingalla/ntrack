﻿using System;


namespace nTrack.Extension
{
    public static class GuidExtensions
    {
        public static bool IsEmpty(this Guid g)
        {
            return g == Guid.Empty;
        }
    }
}
