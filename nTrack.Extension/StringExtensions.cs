﻿using System;
using System.Text.RegularExpressions;

namespace nTrack.Extension
{
    public static class StringExtensions
    {
        public static string ToSeparateWords(this string value)
        {
            if (value != null)
                return Regex.Replace(value, "([A-Z][a-z]?)", " $1").Trim();
            return value;
        }

        public static DateTime JsonDateToDateTime(this string value)
        {
            if (value != null)
            {
                var date = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(long.Parse(value)).ToLocalTime();
                return date;
            }
            return DateTime.Now;
        }

        public static string Fill(this string value, params object[] args)
        {
            return string.Format(value, args);
        }

        public static bool IsGuid(this string expression)
        {
            if (expression != null)
            {
                Regex guidRegEx = new Regex(@"[({]?(0x)?[0-9a-fA-F]{8}([-,]?(0x)?[0-9a-fA-F]{4}){2}((-?[0-9a-fA-F]{4}-?[0-9a-fA-F]{12})|(,\{0x[0-9a-fA-F]{2}(,0x[0-9a-fA-F]{2}){7}\}))[)}]?");
                return guidRegEx.IsMatch(expression);
            }
            return false;
        }
    }
}