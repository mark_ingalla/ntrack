﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nTrack.Extension
{
    public static class ColorExtensions
    {
        public static String ToHexConverter(this System.Drawing.Color c)
        {
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }
    }
}
