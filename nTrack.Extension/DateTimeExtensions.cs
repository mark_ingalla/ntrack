﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nTrack.Extension
{
    public static class DateTimeJavaScript
    {
        public static long ToJavaScriptMilliseconds(this DateTime dt)
        {
            return (long)dt.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
        }
    }
}
