namespace nTrack.Extension
{
    public static class IntegerExtensions
    {
        public static int ToInt(this string value)
        {
            int pValue;
            if (!int.TryParse(value, out pValue))
                pValue = 0;

            return pValue;
        }
    }
}