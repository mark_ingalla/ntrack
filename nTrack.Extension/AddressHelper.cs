﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nTrack.Extension
{
    public static class AddressHelper
    {
        public static string GetFormatedAddress(string address1, string address2, string suburb, string state, string postcode)
        {
            if (!string.IsNullOrEmpty(address1))
                return !string.IsNullOrEmpty(address2)
                           ? string.Format("{0} {1}, {2}, {3} {4}", address1, address2, suburb, state, postcode)
                           : string.Format("{0}, {1}, {2} {3}", address1, suburb, state, postcode);

            return !string.IsNullOrEmpty(address2)
                       ? string.Format("{0}, {1}, {2} {3}", address2, suburb, state, postcode)
                       : string.Format("{0}, {1} {2}", suburb, state, postcode);
        }
    }
}
