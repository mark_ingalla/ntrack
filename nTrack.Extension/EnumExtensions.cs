﻿using System;

namespace nTrack.Extension
{
    public static class EnumHelpers
    {
        public static T FromString<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value);
        }
    }
}
