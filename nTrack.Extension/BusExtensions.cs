﻿using System;
using System.Threading;
using NServiceBus;
using nTrack.Core;

namespace nTrack.Extension
{
    public static class BusExtensions
    {
        public static string GetDatabaseName(this IBus bus)
        {
            string database;

            if (bus.CurrentMessageContext != null && bus.CurrentMessageContext.Headers.ContainsKey(nTrackConstants.DatabaseHeaderKey))
            {
                database = bus.CurrentMessageContext.Headers[nTrackConstants.DatabaseHeaderKey];
                //logger.DebugFormat("Found In Bus CurrentMessageContext UserId {0}", userId);
            }
            else if (Thread.GetData(Thread.GetNamedDataSlot(nTrackConstants.DatabaseHeaderKey)) != null)
            {
                database = Thread.GetData(Thread.GetNamedDataSlot(nTrackConstants.DatabaseHeaderKey)).ToString();
                //logger.DebugFormat("Found In Thread Data UserId {0}", userId);
            }
            else
            {
                database = string.Empty;
                //logger.Debug("Found no UserId");
            }

            if (!string.IsNullOrWhiteSpace(database)) return database;

            throw new Exception("Unable to process message due to unknown database name");

        }
        public static Guid GetAccountId(this IBus bus)
        {
            string accountId;

            if (bus.CurrentMessageContext != null && bus.CurrentMessageContext.Headers.ContainsKey(nTrackConstants.AccountId))
            {
                accountId = bus.CurrentMessageContext.Headers[nTrackConstants.AccountId];
            }
            else if (Thread.GetData(Thread.GetNamedDataSlot(nTrackConstants.AccountId)) != null)
            {
                accountId = Thread.GetData(Thread.GetNamedDataSlot(nTrackConstants.AccountId)).ToString();
            }
            else
            {
                accountId = string.Empty;
            }

            if (!string.IsNullOrWhiteSpace(accountId)) return Guid.Parse(accountId);

            throw new Exception("Unable to process message due to unknown accountId");
        }
        public static Guid GetUserId(this IBus bus)
        {
            string userId;

            if (bus.CurrentMessageContext != null && bus.CurrentMessageContext.Headers.ContainsKey(nTrackConstants.UserId))
            {
                userId = bus.CurrentMessageContext.Headers[nTrackConstants.UserId];
            }
            else if (Thread.GetData(Thread.GetNamedDataSlot(nTrackConstants.UserId)) != null)
            {
                userId = Thread.GetData(Thread.GetNamedDataSlot(nTrackConstants.UserId)).ToString();
            }
            else
            {
                userId = string.Empty;
            }

            if (!string.IsNullOrWhiteSpace(userId)) return Guid.Parse(userId);

            throw new Exception("Unable to process message due to unknown userId");
        }
    }
}
