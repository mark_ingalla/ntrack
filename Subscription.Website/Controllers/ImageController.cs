﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using Raven.Client;
using Subscription.Website.Services;
using nTrack.Core.Utilities;

namespace Subscription.Website.Controllers
{
    public class ImageController : Controller
    {
        private readonly IAttachmentService _attachmentService;

        public ImageController(IAttachmentService attachmentService)
        {
            _attachmentService = attachmentService;
        }

        [Authorize]
        [GET("GetImage")]
        public ActionResult GetImage(Guid id, string entity)
        {

            Raven.Abstractions.Data.Attachment attachment = _attachmentService.GetAttachment(id, entity);

            if (attachment == null)
                return null;

            return File(attachment.Data(), "image/png");
        }

        [Authorize]
        [POST("PutImage")]
        public ActionResult PutImage(Guid id, string entity, string returnUrl, IEnumerable<HttpPostedFileBase> files)
        {
            if (files != null)
            {
                var file = files.First();

                if (file.ContentLength > 0)
                {
                    _attachmentService.PutAttachment(id, entity, file.InputStream);
                }
            }

            return Redirect(returnUrl);
        }
    }
}
