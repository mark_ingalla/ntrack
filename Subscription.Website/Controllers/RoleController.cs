﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using Subscription.Data;

using Subscription.Website.Authorization;
using Subscription.Website.ViewModels.Role;
using nTrack.Core.Utilities;
using Subscription.Website.Services;
using Subscription.Website.Infrastructure;
//using Raven.Client.Linq;

namespace Subscription.Website.Controllers
{
    [CheckIfCanManageAccount]
    public abstract class RoleController : BaseController
    {

        readonly IThreadingUtilities _threadingUtilities;
        private readonly IAccountService _accountService;

        public RoleController(IThreadingUtilities threadingUtilities, IAccountService accountService)
        {
            _threadingUtilities = threadingUtilities;
            _accountService = accountService;
        }
        //
        // GET: /Manager/Role/

        protected virtual ActionResult ReturnToIndexAction(Guid accountId)
        {
            return RedirectToAction("Index");
        }

        protected virtual string ReturnUrl(Guid accountId)
        {
            return Url.Action("Index");
        }

        private Dictionary<Guid, List<string>> GetSelectedPermissionsDictionary(RoleAddEditVM vm)
        {
            Dictionary<Guid, List<string>> dictionary = new Dictionary<Guid, List<string>>();

            foreach (var product in vm.SubscribedProductWithPermissions)
            {
                List<string> list = new List<string>();
                foreach (var permission in product.Permissions)
                {
                    if (permission.IsSelected)
                        list.Add(permission.Code);
                }

                if (list.Count > 0)
                    dictionary.Add(product.Id, list);
            }

            return dictionary;
        }

        [GET("Role/Add/{accountId?}")]
        public ActionResult Add(Guid? accountId)
        {

            var account = DbSession.Load<Account>(accountId ?? ContextAccountID);

            if (account == null)
                return HttpNotFound();

            var subscribedProducts = _accountService.GetSubscribedProducts(DbSession, "accounts/"+account.Id);

            var vm = new RoleAddEditVM(subscribedProducts, account.Id);
            vm.ReturnUrl = ReturnUrl(account.Id);

            return View(vm);
        }

        [POST("Role/Add/{accountId?}")]
        public ActionResult Add(RoleAddEditVM vm)
        {
            DateTime sentTime = DateTime.UtcNow;

            var dictionary = GetSelectedPermissionsDictionary(vm);

            Guid roleId = Guid.NewGuid();

            AddRole cmd = new AddRole()
                              {
                                  Name = vm.RoleName,
                                  RoleId = roleId,
                                  AccountId = vm.AccountId,
                                  Permissions = dictionary
                              };

            Bus.Send(cmd);

            bool found;

            found = _threadingUtilities
                .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                 () =>
                                 DbSession.Query<Account>().Any(x => x.Id == vm.AccountId && x.Roles.Any(p => p.Key == roleId && p.UpdatedOn > sentTime)));

            if (found)
            {
                Success("", "Role has been added!");
                return ReturnToIndexAction(vm.AccountId);
            }

            Info("", "The server is taking to long to confirm your request.");

            vm.AllowPost = false;
            return View(vm);

        }


        [GET("Role/Edit/{accountId?}")]
        public ActionResult Edit(Guid key, Guid? accountId)
        {
            var account = DbSession.Load<Account>(accountId ?? ContextAccountID);

            if (account == null)
                return HttpNotFound();

            var subscribedProducts = _accountService.GetSubscribedProducts(DbSession, "accounts/"+account.Id);

            var role = DbSession.Load<Account>(account.Id).Roles.SingleOrDefault(p => p.Key == key);

            if (role == null)
                return HttpNotFound();

            var vm = new RoleAddEditVM(subscribedProducts, role, account.Id);
            vm.ReturnUrl = ReturnUrl(account.Id);

            return View(vm);
        }

        [POST("Role/Edit/{accountId?}")]
        public ActionResult Edit(RoleAddEditVM vm)
        {
            DateTime sentTime = DateTime.UtcNow;

            var dictionary = GetSelectedPermissionsDictionary(vm);

            UpdateRole cmd = new UpdateRole()
            {
                Name = vm.RoleName,
                RoleId = vm.Key,
                AccountId = vm.AccountId,
                Permissions = dictionary
            };

            Bus.Send(cmd);

            bool found;

            found = _threadingUtilities
                .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                 () =>
                                 DbSession.Query<Account>().Any(x => x.Id == vm.AccountId && x.Roles.Any(p => p.Key == vm.Key && p.UpdatedOn > sentTime)));

            if (found)
            {
                Success("", "Role has been changed!");
                return ReturnToIndexAction(vm.AccountId);
            }

            Info("", "The server is taking to long to confirm your request.");

            vm.AllowPost = false;
            return View(vm);

        }

        [GET("Role/Remove/{accountId?}")]
        public ActionResult Remove(Guid key, Guid? accountId)
        {
            var account = DbSession.Load<Account>(accountId ?? ContextAccountID);

            if (account == null)
                return HttpNotFound();

            var role = DbSession.Load<Account>(account.Id).Roles.SingleOrDefault(p => p.Key == key);

            if (role == null)
                return HttpNotFound();

            var vm = new RoleRemoveVM()
                         {
                             AccountId = account.Id,
                             Key = key,
                             RoleName = role.Name,
                             ReturnUrl = ReturnUrl(account.Id)
                         };

            return View(vm);
        }

        [POST("Role/Remove/{accountId?}")]
        public ActionResult RemovePost(RoleRemoveVM vm)
        {

            Bus.Send(new RemoveRole()
                         {
                             AccountId = vm.AccountId,
                             RoleId = vm.Key
                         });

            bool found=true;

            ////TODO: don't know why, DBSession still have removed Role, even if physically it doesn't exists 
            found = _threadingUtilities
                .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                 () => !DbSession.Load<Account>(vm.AccountId).Roles.Any(p => p.Key == vm.Key));

            if (found)
            {
                Success("", "Role has been removed!");
                return ReturnToIndexAction(vm.AccountId);
            }

            Info("", "The server is taking to long to confirm your request.");

            vm.AllowPost = false;
            return View("Remove",vm);
        }


    }
}
