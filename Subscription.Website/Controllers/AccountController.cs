﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using nTrack.Core.Utilities;
using nTrack.Data;
using nTrack.Data.Extenstions;
using nTrack.Data.Indices;
using Raven.Client;
using Recaptcha;
using Subscription.Data;
using Subscription.Data.Indices;
using Subscription.Messages.Commands;
using Subscription.Messages.Commands.Account.User;
using Subscription.Website.Infrastructure;
using Subscription.Website.ViewModels;
using Subscription.Website.ViewModels.Account;
using System;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using Account = Subscription.Data.Account;

namespace Subscription.Website.Controllers
{
    [RoutePrefix("Account")]
    public class AccountController : BaseController
    {
        readonly ICryptographyService _cryptographyService;
        private readonly IThreadingUtilities _threadingUtilities;
        private readonly IDocumentStore _store;

        public AccountController(ICryptographyService cryptographyService, IThreadingUtilities threadingUtilities, IDocumentStore store)
        {
            _cryptographyService = cryptographyService;
            _threadingUtilities = threadingUtilities;
            _store = store;
        }

        [GET("Register")]
        public ActionResult Register(Guid? offerKey)
        {
            var countries = DbSession.Load<Countries>(Countries.EntityId);
            var products = DbSession.Query<ProductOfferIndex.Result, ProductOfferIndex>()
                                    .Where(x => x.IsProductActive && x.IsOfferActive).ToList();

            var productModels = products.GroupBy(x => x.ProductId)
                                        .ToDictionary(x => x.Key.ToGuidId(), y => y.Select(x => x.ProductName).FirstOrDefault());

            var vm = new RegisterVM(countries != null ? countries.CountryNames : new string[] { }, productModels);

            if (offerKey.HasValue)
            {
                //we have an offer.. lets get the product details
                var offer = products.SingleOrDefault(x => x.OfferKey == offerKey.Value);
                if (offer != null)
                {
                    vm.SelectedOfferKey = offerKey.Value;
                    vm.SelectProductId = offer.ProductId.ToGuidId();
                }
            }


            return View(vm);
        }


        [RecaptchaControlMvc.CaptchaValidatorAttribute]
        [POST("Register")]
        public ActionResult Register(RegisterVM vm, bool captchaValid)
        {
            if (!captchaValid)
            {
                ModelState.AddModelError("captcha", "You did not type the verification word correctly. Please try again.");
            }

            if (!CheckEmailsDomain(vm.Email))
            {
                ModelState.AddModelError("Email", "Pallet Plus is a B2B product for company use. Please enter a valid company email address to sign-up");
            }

            if (!IsUniqueEmail(vm.Email))
            {
                ModelState.AddModelError("Email", "Email address is already registered!");
            }

            if (!vm.SelectedOfferKey.HasValue)
            {
                ModelState.AddModelError("SelectedOffer", "Offer Selection is Required!");
            }

            if (!ModelState.IsValid)
            {
                var countries = DbSession.Load<Countries>(Countries.EntityId);
                var products = DbSession.Query<ProductOfferIndex.Result, ProductOfferIndex>()
                                        .Where(x => x.IsProductActive && x.IsOfferActive).ToList();

                var productModels = products.GroupBy(x => x.ProductId)
                                        .ToDictionary(x => x.Key.ToGuidId(), y => y.Select(x => x.ProductName).FirstOrDefault());

                vm = new RegisterVM(countries != null ? countries.CountryNames : new string[] { }, productModels);
                return View(vm);
            }

            var product = DbSession.Load<Product>(vm.SelectProductId.Value);
            var verificationToken = Guid.NewGuid();
            var offerName = product.Offers.Single(p => p.Key == vm.SelectedOfferKey.Value)
                    .Name;
            var veritifactionPageUrl = Url.Action("EmailVerification", "Account", new { token = verificationToken }, "http");

            Bus.Send(CommandsFactory.Subscription.Account.RegisterAccount(vm, Guid.NewGuid(), Guid.NewGuid(),
                                                                          verificationToken,
                                                                          veritifactionPageUrl, product.Name, offerName,
                                                                          Request.UserHostAddress));

            //var cmd = Mapper.Map<RegisterAccount>(vm);
            //cmd.OfferName =
            //    product.Offers.Single(p => p.Key == vm.SelectedOfferKey.Value)
            //        .Name;
            //cmd.ProductName = product.Name;
            //cmd.ProductId = vm.SelectProductId.Value;
            //cmd.OfferKey = vm.SelectedOfferKey.Value;
            //cmd.AccountId = Guid.NewGuid();
            //cmd.UserKey = Guid.NewGuid();
            //cmd.IPAddress = Request.UserHostAddress;
            //cmd.EmailVerificationToken = Guid.NewGuid();
            //cmd.EmailVerificationPageUrl = Url.Action("EmailVerification", "Account", new { token = cmd.EmailVerificationToken }, "http");

            //Bus.Send(cmd);

#if DEBUG
            ViewBag.EmailVerificationUrl = veritifactionPageUrl;
#endif

            return View("RegistrationSuccess");
        }

        [GET("Product/Offers/{productId}")]
        public JsonResult ProductOffers(Guid productId)
        {
            var product = DbSession.Load<Product>(productId);
            return Json(product.Offers.Where(x => x.IsActive).Select(x => new { x.Name, x.Key, Price = x.Price.ToString("c"), IsSingle = x.Features.Any(p => p.Code == "SINGLE") }),
                        JsonRequestBehavior.AllowGet);
        }

        [GET("LimitQuantity")]
        public JsonResult LimitQuantity(int offerQTY, Guid selectedOfferKey, Guid selectProductId)
        {
            var product = DbSession.Load<Product>(selectProductId);

            var offer = product.Offers.SingleOrDefault(x => x.Key == selectedOfferKey);

            if (offer != null)
            {
                var feature = offer.Features.FirstOrDefault(x => x.Code == "SINGLE");

                if (feature != null && feature.Limit > 0)
                {

                    return Json(offerQTY <= feature.Limit, JsonRequestBehavior.AllowGet);

                }
            }

            return Json(true, JsonRequestBehavior.AllowGet);


        }

        [GET("EmailVerification/{token}")]
        public ActionResult EmailVerification(Guid token)
        {
            var account = DbSession.Query<Account>()
                                   .Customize(x => x.WaitForNonStaleResultsAsOfNow())
                                   .SingleOrDefault(x => x.PendingVerificationToken == token);

            if (account == null)
                return View("Error", new ErrorVM("Email verification failed. Invalid or expired token."));

            Bus.Send<VerifyEmail>(cmd => { cmd.EmailVerificationToken = token; });

            return View("EmailVerified");
        }

        [GET("ResetPasswordVerification/{token}")]
        public ActionResult ResetPasswordVerification(Guid token)
        {
            var result =
                DbSession.Query<UserIndex.Result, UserIndex>().SingleOrDefault(p => p.ResetPasswordTokens.Any(x => x == token));

            if (result == null)
                return View("Error", new ErrorVM("Password reset verification failed. Invalid or expired token."));

            var newPassword = System.Web.Security.Membership.GeneratePassword(20, 10);

            Bus.Send<ResetPassword>(
                cmd =>
                {
                    cmd.ResetPasswordToken = token;
                    cmd.NewPassword = newPassword;
                });

            var vm = new PasswordRestedVM()
                         {
                         };
#if DEBUG
            ViewBag.NewPassword = newPassword;
#endif
            return View("PasswordRested", vm);
        }

        [POST("IsUniqueEmail")]
        public bool IsUniqueEmail(string email)
        {
            if (string.IsNullOrEmpty(email)) return false;
            var exists = DbSession.Query<UserIndex.Result, UserIndex>().Any(c => c.Email == email);
            return !exists;
        }

        [GET("JSONIsUniqueEmail")]
        public JsonResult JSONIsUniqueEmail(string email)
        {
            var result = IsUniqueEmail(email);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [GET("ForgotPassword")]
        public ActionResult ForgotPassword()
        {
            return View("ForgotPassword", new ForgotPasswordPageVM());
        }

        [POST("ForgotPassword")]
        public ActionResult ForgotPassword(ForgotPasswordPageVM vm)
        {
            var u = DbSession.Query<UserIndex.Result, UserIndex>().SingleOrDefault(p => p.Email == vm.Email);


            if (u == null)
                return HttpNotFound();


            if (ModelState.IsValid)
            {

                var token = Guid.NewGuid();

                Bus.Send(new PrepareResetPassword()

                             {
                                 AccountId = u.AccountId.ToGuidId(),
                                 UserEmail = vm.Email,
                                 UserKey = u.UserKey,
                                 ResetPasswordToken = token,
                                 ResetPasswordLink =
                                     Url.Action("ResetPasswordVerification", "Account", new { token = token, area = "" },
                                                "http"),
                                 UserFullname = u.UserFullName
                             });

#if DEBUG
                ViewBag.ResetPasswordLink = Url.Action("ResetPasswordVerification", "Account",
                                                       new { token = token, area = "" }, "http");
#endif
                return View("ResetPasswordSuccess");

            }

            return View("ForgotPassword", vm);
        }

        [GET("Login")]
        public ActionResult Login()
        {
            return View("Login", new LoginFormVM());
        }

        [POST("Login")]
        public ActionResult Login(LoginFormVM vm)
        {
            if (string.IsNullOrEmpty(vm.Email))
            {
                ModelState.AddModelError("Email", "Email address is required");
                vm.Password = string.Empty;
                return View("Login", vm);
            }
            var user = DbSession.Query<UserIndex.Result, UserIndex>()
                                   .SingleOrDefault(
                                       x =>
                                       x.Email == vm.Email &&
                                       x.Password == _cryptographyService.GetMD5Hash(vm.Password));

            if (user == null)
            {
                ModelState.AddModelError(string.Empty, "Invalid username or password.");
                return View("Login", vm);
            }

            FormsAuthentication.SetAuthCookie(user.UserKey.ToString(), vm.Remember);

            return RedirectToAction("Index", "Home", new { area = "Manager" });

            //FormsAuthentication.RedirectFromLoginPage(user.UserKey.ToString(), vm.Remember);
            //return null;
        }

        [GET("Logout")]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        [GET("Profile")]
        public ActionResult UserProfile(string returnUrl)
        {
            var account = DbSession.Load<Account>(ContextAccountId);
            var user = account.Users.SingleOrDefault(x => x.UserKey == ContextUserKey);

            var userVM = new UserVM(user);
            userVM.ReturnUrl = returnUrl;
            return View(userVM);

        }

        [Authorize]
        [GET("AccountProfile")]
        public ActionResult AccountProfile(string returnUrl)
        {

            var account = DbSession.Load<Account>(ContextAccountId);
            if (account == null)
                return HttpNotFound();

            var accountVM = new AccountVM(account);
            accountVM.ReturnUrl = returnUrl;

            return View(accountVM);
        }

        [Authorize]
        [GET("HaveNoRights")]
        public ActionResult HaveNoRights()
        {
            return View();
        }

        [Authorize]
        [POST("Profile")]
        public ActionResult UserProfile(UserVM vm)
        {
            bool passwordChanged = false;
            var account = DbSession.Load<Account>(ContextAccountId);
            var ntUser = account.Users.Single(x => x.UserKey == ContextUserKey);

            if (vm.OldPassword != null)
            {
                if (_cryptographyService.GetMD5Hash(vm.OldPassword) != ntUser.Password)
                {
                    ModelState.AddModelError("OldPassword", "Current password is not valid");
                }

                if (string.IsNullOrWhiteSpace(vm.Password))
                {
                    ModelState.AddModelError("Password", "You must type new password and repeat to change current");
                }

                passwordChanged = true;
            }

            if (!IsUniqueEmail(vm.Email) && vm.Email != ntUser.Email)
            {
                ModelState.AddModelError("Email", "This email is already registered!");
            }
            if (ModelState.IsValid)
            {
                Bus.Send(CommandsFactory.Subscription.Account.User.UpdateUserDetail(account.Id, vm.Email, vm.FirstName,
                                                                                    vm.LastName, vm.Mobile, vm.Phone,
                                                                                    vm.UserKey));

                //TODO: send the admin command

                Success("", "User profile has been changed!");

                if (passwordChanged)
                {
                    Bus.Send(new ChangeUserPassword()
                                 {
                                     AccountId = account.Id,
                                     UserKey = ntUser.UserKey,
                                     NewPassword = vm.Password
                                 });

                    Success(null, "Password has been changed!");
                }
            }

            vm.OldPassword = null;
            vm.RepeatPassword = null;
            vm.Password = null;

            return View(vm);
        }


        private bool CheckEmailsDomain(string email)
        {
            var emailsString = ConfigurationManager.AppSettings["ForbiddenEmails"];
            var emails = emailsString.Split(',');

            foreach (var e in emails)
            {
                if (email.Contains(e))
                {
                    return false;
                }
            }

            return true;
        }

    }
}
