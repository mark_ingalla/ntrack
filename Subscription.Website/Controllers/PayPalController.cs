﻿using System;
using System.Linq;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using nTrack.Data.Extenstions;
using Subscription.Data;
using Subscription.Messages.Commands.Account;
using Subscription.Website.Infrastructure;
using Subscription.Website.Services;
using Subscription.Website.ViewModels;

namespace Subscription.Website.Controllers
{
    public class PayPalController : BaseController
    {
        readonly IPayPalService _payPalService;

        public PayPalController(IPayPalService payPalService)
        {
            _payPalService = payPalService;
        }

        //
        // GET: /paypal/start-payment/23
        [Authorize]
        [POST("PayPal/StartPayment/{invoiceId}")]
        public ActionResult StartPayment(Guid invoiceId)
        {
            ActionResult actionResult;

            var invoice = DbSession.Load<Invoice>(invoiceId);

            if (invoice == null || invoice.IsPaid)
            {
                ErrorVM errorVM = new ErrorVM(
                       string.Format("You may not pay for this invoice #{0}", invoice == null ? 0 : invoice.InvoiceNumber));
                actionResult = View("Error", errorVM);
            }
            else
            {

                IPayPalService paypalService = _payPalService;
                string token;
                string returnUrl = Url.Action("EndPayment", "PayPal", new { invoice.InvoiceNumber }, "http");
                string cancelUrl = Url.Action("Index", "Billing", new { area = "Manager" }, "http");
                string payPalDescription = string.Format("NTrack Subscription Invoice #{0}", invoice.InvoiceNumber);
                string logoImageUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
                                      Url.Content("~/content/images/paypal-header-logo.png");
                bool setExpressCheckoutSuccess = paypalService.SetExpressCheckout(DbSession,
                                                                                  invoice.InvoiceNumber, payPalDescription,
                                                                                  invoice.Total, "AUD", "AU", false,
                                                                                  logoImageUrl, returnUrl, cancelUrl,
                                                                                  out token);
                if (!setExpressCheckoutSuccess)
                {
                    ErrorVM errorVM = new ErrorVM(
                        "PayPal reported an error. Please click the back button of your browser and try again.");
                    actionResult = View("Error", errorVM);
                }
                else
                {
                    string redirectUrl = paypalService.GetExpressCheckoutRedirectUrl(token);
                    actionResult = Redirect(redirectUrl);
                }
            }


            return actionResult;
        }

        //
        // GET: /paypal/end-payment/23
        [Authorize]
        [GET("Paypal/EndPayment/{invoiceNumber}")]
        public ActionResult EndPayment(int invoiceNumber, string token, string payerID)
        {
            ActionResult actionResult;

            var invoice = DbSession.Query<Invoice>().SingleOrDefault(p => p.InvoiceNumber == invoiceNumber);
            if (invoice == null || invoice.IsPaid)
            {
                ErrorVM errorVM = new ErrorVM(
                        string.Format("You may not pay for this invoice #{0}", invoiceNumber));
                actionResult = View("Error", errorVM);
            }
            else
            {
                bool doExpressCheckoutSuccess = _payPalService.DoExpressCheckout(DbSession,
                    token, payerID, invoiceNumber, invoice.Total, "AUD");
                if (!doExpressCheckoutSuccess)
                {
                    ErrorVM errorVM = new ErrorVM("Could not process the PayPal transaction.");
                    actionResult = View("Error", errorVM);
                }
                else
                {
                    //var account = DbSession.Load<Account>(invoice.AccountId);
                    //if (account == null)
                    //{
                    //    ErrorVM errorVM = new ErrorVM("Could not process the PayPal transaction.");
                    //    actionResult = View("Error", errorVM);
                    //}
                    //else
                    //{
                    //    var subscription = account.Subscriptions.SingleOrDefault(p => p.Key == invoice.AccountSubscriptionKey);
                    //    if (subscription == null)
                    //    {
                    //        ErrorVM errorVM = new ErrorVM("Could not process the PayPal transaction.");
                    //        actionResult = View("Error", errorVM);
                    //    }
                    //    else
                    //    {
                    Bus.Send(new PayInvoice()
                                 {
                                     AccountId = invoice.AccountId.ToGuidId(),
                                     InvoiceId = invoice.Id,
                                     Amount = invoice.Total
                                 });
                    // Redirect the user to the confirmation page.
                    actionResult = RedirectToAction("Index", "Billing", new { area = "Manager" });
                    //    }
                    //}
                }
            }

            return actionResult;
        }
    }
}