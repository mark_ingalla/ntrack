﻿using System;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AttributeRouting;
using AttributeRouting.Web.Mvc;
using nTrack.Core.Utilities;
using nTrack.Data;
using nTrack.Data.Indices;
using Subscription.Data.Indices;
using Subscription.Website.Infrastructure;
using Subscription.Website.Services;
using Subscription.Website.ViewModels;
using Account = Subscription.Data.Account;

namespace Subscription.Website.Controllers
{
    [RoutePrefix("Authentication")]
    public class AuthenticationController : BaseController
    {
        readonly ICryptographyService _cryptographyService;
        readonly IThreadingUtilities _threadingUtilities;

        public AuthenticationController(ICryptographyService cryptographyService, IThreadingUtilities threadingUtilities)
        {
            _cryptographyService = cryptographyService;
            _threadingUtilities = threadingUtilities;
        }

        

        //string AppendSessionTokenParameter(string callbackUrl, Guid sessionToken)
        //{
        //    var callbackUrlBuilder = new UriBuilder(callbackUrl);
        //    var encryptionSecret = _authenticationServerEncryptionSecret;
        //    var encryptedSessionToken = _cryptographyService.Encrypt(sessionToken.ToString(), encryptionSecret);
        //    var newQueryString = "sessionToken=" + HttpUtility.UrlEncode(encryptedSessionToken);

        //    if (!string.IsNullOrEmpty(callbackUrlBuilder.Query))
        //        newQueryString = callbackUrlBuilder.Query.Substring(1) + "&" + newQueryString;

        //    callbackUrlBuilder.Query = newQueryString;

        //    return callbackUrlBuilder.Uri.AbsoluteUri;
        //}
        //string AppendSessionTokenAndUserParameter(string callbackUrl, string userEmail, Guid sessionToken)
        //{
        //    var callbackUrlBuilder = new UriBuilder(callbackUrl);
        //    var encryptionSecret = _authenticationServerEncryptionSecret;
        //    var encryptedSessionToken = _cryptographyService.Encrypt(sessionToken.ToString(), encryptionSecret);

        //    var emailQueryString = "userEmail=" + HttpUtility.UrlEncode(userEmail);
        //    var tokenQueryString = "sessionToken=" + HttpUtility.UrlEncode(encryptedSessionToken);

        //    string newQueryString = emailQueryString + "&" + tokenQueryString;
        //    if (!string.IsNullOrEmpty(callbackUrlBuilder.Query))
        //        newQueryString = callbackUrlBuilder.Query.Substring(1) + "&" + newQueryString;

        //    callbackUrlBuilder.Query = newQueryString;

        //    return callbackUrlBuilder.Uri.AbsoluteUri;
        //}
    }
}
