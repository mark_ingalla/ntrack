﻿using AttributeRouting.Web.Mvc;
using AutoMapper;
using nTrack.Core.Messages;
using nTrack.Core.Utilities;
using nTrack.Data.Extenstions;
using nTrack.Data.Indices;
using Subscription.Messages.Commands;
using Subscription.Messages.Commands.Account.User;
using Subscription.Website.Infrastructure;
using Subscription.Website.Services;
using Subscription.Website.ViewModels.User;
using System;
using System.Linq;
using System.Web.Mvc;
using Account = Subscription.Data.Account;

namespace Subscription.Website.Controllers
{
    [CheckIfCanManageUser]
    public abstract class UserController : BaseController
    {
        private readonly IThreadingUtilities _threadingUtilities;
        private readonly IAccountService _accountService;

        protected virtual ActionResult ReturnToIndexAction(Guid accountId)
        {
            return RedirectToAction("Index");
        }

        protected virtual string ReturnUrl(Guid accountId)
        {
            return Url.Action("Index");
        }

        public UserController(IThreadingUtilities threadingUtilities, IAccountService accountService)
        {
            _threadingUtilities = threadingUtilities;
            _accountService = accountService;
        }

        [GET("Users/{accountId?}")]
        public ActionResult Index(Guid? accountId)
        {
            //get all users for this account
            if (accountId == null)
                accountId = ContextAccountId;

            var account = DbSession.Load<Account>(accountId);
            if (account == null)
                return HttpNotFound();

            var model = new UsersVM(account.Users.Where(x => !x.IsDeleted).ToList());

            return View(model);
        }

        [GET("User/Add/{accountId?}")]
        public ActionResult Add(Guid? accountId)
        {
            if (accountId == null)
                accountId = ContextAccountId;
            var account = DbSession.Load<Account>(accountId);
            if (account == null)
                return HttpNotFound();
            string[] roles = Enum.GetNames(typeof(UserRole));

            return View(new UserVM(roles) { AccountId = account.Id, ReturnUrl = ReturnUrl(account.Id) });
        }

        [POST("User/Add/{accountId?}")]
        public ActionResult Add(UserVM vm)
        {
            DateTime sentTime = DateTime.UtcNow;
            var userKey = Guid.NewGuid();
            var accountId = vm.AccountId ?? ContextAccountId;
            var account = DbSession.Load<Account>(accountId);
            if (account == null)
                return HttpNotFound();

            if (vm.Password == null || vm.Password.Count() < 8)
            {
                ModelState.AddModelError("Password", "Password is required! It has to be minimum 8 char string.");
            }

            if (!IsUniqueEmail(vm.Email, Guid.Empty))
            {
                ModelState.AddModelError("Email", "Email address is already registered!");
            }

            string[] roles = Enum.GetNames(typeof(UserRole));
            vm.Roles = new SelectList(roles);

            if (!ModelState.IsValid)
            {
                return View(vm);
            }

            var cmd = Mapper.Map<UserVM, AddUser>(vm);
            cmd.Role = (UserRole)Enum.Parse(typeof(UserRole), vm.Role);

            cmd.UserKey = userKey;
            cmd.AccountId = account.Id;

            Bus.Send(cmd);

            bool found;

            found = _threadingUtilities
                .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                 () =>
                                 DbSession.Query<UserIndex.Result, UserIndex>().Any(p => p.UserKey == userKey && p.AccountUpdateOn > sentTime));

            if (found)
            {
                Success(null, "User has been added!");
                return ReturnToIndexAction(account.Id);
            }

            Info("", "The server is taking to long to confirm your request.");

            vm.AllowPost = false;

            return View(vm);
        }

        [GET("User/{userKey}/Edit")]
        public ActionResult Edit(Guid userKey)
        {
            var result = DbSession.Query<UserIndex.Result, UserIndex>()
                .SingleOrDefault(p => p.UserKey == userKey);

            if (result == null)
                return HttpNotFound();

            var user = DbSession.Load<Account>(result.AccountId).Users.SingleOrDefault(p => p.UserKey == userKey);

            if (user == null)
                return HttpNotFound();

            string[] roles = Enum.GetNames(typeof(UserRole));

            var vm = new UserVM(roles);

            Mapper.Map(user, vm);
            vm.ReturnUrl = ReturnUrl(result.AccountId.ToGuidId());

            return View(vm);
        }

        [POST("User/{userKey}/Edit")]
        public ActionResult Edit(UserVM vm)
        {
            DateTime sentTime = DateTime.UtcNow;

            if (!IsUniqueEmail(vm.Email, vm.UserKey))
            {
                ModelState.AddModelError("Email", "Email address is already registered!");
            }

            string[] roles = Enum.GetNames(typeof(UserRole));
            vm.Roles = new SelectList(roles);

            if (!ModelState.IsValid)
            {
                return View(vm);
            }

            var result = DbSession.Query<UserIndex.Result, UserIndex>().SingleOrDefault(p => p.UserKey == vm.UserKey);

            if (result == null)
                return HttpNotFound();

            UpdateUserDetail updateUserDetail = new UpdateUserDetail();
            updateUserDetail.AccountId = result.AccountId.ToGuidId();
            Mapper.Map(vm, updateUserDetail);
            Bus.Send(updateUserDetail);

            if (result.Role != vm.Role)
            {
                UserRole userRole = (UserRole)Enum.Parse(typeof(UserRole), vm.Role);
                Bus.Send(new ChangeUserRole() { AccountId = result.AccountId.ToGuidId(), Role = userRole, UserKey = result.UserKey });
            }

            bool found;

            found = _threadingUtilities
                .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                 () =>
                                      DbSession.Query<UserIndex.Result, UserIndex>().Any(
                                     x =>
                                     x.UserKey == vm.UserKey &&
                                     x.AccountUpdateOn > sentTime));

            if (found)
            {
                Success("", "User has been updated!");
                return ReturnToIndexAction(result.AccountId.ToGuidId());
            }

            Info("", "The server is taking to long to confirm your request.");

            vm.AllowPost = false;
            return View(vm);
        }

        [GET("User/{userKey}/Lock")]
        public ActionResult Lock(Guid userKey)
        {
            var vm = GetLockingVM(userKey, true);
            if (vm == null)
                return HttpNotFound();
            return View("Locking", vm);
        }

        [GET("User/{userKey}/Unlock")]
        public ActionResult Unlock(Guid userKey)
        {
            var vm = GetLockingVM(userKey, false);
            if (vm == null)
                return HttpNotFound();

            return View("Locking", vm);
        }

        [POST("User/{userKey}/Lock")]
        public ActionResult Lock(UserLockingVM vm)
        {
            DateTime sentTime = DateTime.UtcNow;

            if (!ModelState.IsValid)
            {
                return View("Locking", vm);
            }

            var result = DbSession.Query<UserIndex.Result, UserIndex>().SingleOrDefault(p => p.UserKey == vm.UserKey);

            if (result == null)
                return HttpNotFound();
            Bus.Send(new LockUser()
                             {
                                 AccountId = result.AccountId.ToGuidId(),
                                 Reason = vm.Reason,
                                 UserKey = vm.UserKey
                             });

            bool found;

            found = _threadingUtilities
                .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                 () =>
                                 DbSession.Query<UserIndex.Result, UserIndex>().Any(
                                     x =>
                                     x.UserKey == vm.UserKey && x.UserLocked &&
                                     x.AccountUpdateOn > sentTime));

            if (found)
            {
                Success("", "User has been locked!");
                return ReturnToIndexAction(result.AccountId.ToGuidId());
            }

            Info("", "The server is taking to long to confirm your request.");

            vm.AllowPost = false;

            return View("Locking", vm);
        }

        [POST("User/{userKey}/Unlock")]
        public ActionResult Unlock(UserLockingVM vm)
        {
            DateTime sentTime = DateTime.UtcNow;

            if (!ModelState.IsValid)
            {
                return View("Locking", vm);
            }

            var result = DbSession.Query<UserIndex.Result, UserIndex>().SingleOrDefault(p => p.UserKey == vm.UserKey);

            if (result == null)
                return HttpNotFound();

            Bus.Send(new UnlockUser()
                             {
                                 AccountId = result.AccountId.ToGuidId(),
                                 Reason = vm.Reason,
                                 UserKey = vm.UserKey
                             });


            bool found;

            found = _threadingUtilities
                .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                 () =>
                                     DbSession.Query<UserIndex.Result, UserIndex>().Any(
                                     x =>
                                     x.UserKey == vm.UserKey && !x.UserLocked &&
                                     x.AccountUpdateOn > sentTime));

            if (found)
            {
                Success("", "User has been unlocked!");
                return ReturnToIndexAction(result.AccountId.ToGuidId());
            }

            Info("", "The server is taking to long to confirm your request.");

            vm.AllowPost = false;

            return View("Locking", vm);
        }

        [GET("User/{userKey}/RemoveUser")]
        public ActionResult RemoveUser(Guid userKey)
        {
            var vm = GetRemovingVM(userKey);
            if (vm == null)
                return HttpNotFound();
            return View("Removing", vm);
        }

        [POST("User/{userKey}/RemoveUser")]
        public ActionResult RemoveUser(UserRemovingVM vm)
        {
            DateTime sentTime = DateTime.UtcNow;
            Guid accountIdGuid = Guid.Empty;
            var databaseUpdated = false;
            var databaseError = false;

            if (ModelState.IsValid)
            {
                var userResult = DbSession.Query<UserIndex.Result, UserIndex>()
                    .FirstOrDefault(p => p.UserKey == vm.UserKey);

                if (userResult == null)
                {
                    databaseError = true;
                }
                else
                {
                    accountIdGuid = userResult.AccountId.ToGuidId();

                    Bus.Send(new RemoveUser()
                                 {
                                     AccountId = accountIdGuid,
                                     UserKey = vm.UserKey
                                 });

                    databaseUpdated = _threadingUtilities
                        .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                         () =>
                                         DbSession.Query<UserIndex.Result, UserIndex>().Any(
                                             x =>
                                             x.UserKey == vm.UserKey && x.UserLocked &&
                                             x.AccountUpdateOn > sentTime));
                }
            }

            if (databaseError)
                return HttpNotFound();

            if (databaseUpdated)
            {
                Success("", "User has been removed!");
                return ReturnToIndexAction(accountIdGuid);
            }

            Info("", "The server is taking to long to confirm your request.");
            vm.AllowPost = false;

            return View("Removing", vm);
        }

        [GET("User/{userKey}/ResetPassword")]
        public ActionResult ResetPassword(Guid userKey)
        {
            var result = DbSession.Query<UserIndex.Result, UserIndex>().SingleOrDefault(p => p.UserKey == userKey);

            if (result == null)
                return HttpNotFound();

            var vm = new UserResetPasswordVM()
                         {
                             UserKey = userKey,
                             UserFullName = result.UserFullName
                         };
            vm.ReturnUrl = ReturnUrl(result.AccountId.ToGuidId());

            return View(vm);
        }

        [POST("User/{userKey}/ResetPassword")]
        public ActionResult ResetPasswordPost(UserResetPasswordVM vm)
        {
            var user = DbSession.Query<UserIndex.Result, UserIndex>().SingleOrDefault(p => p.UserKey == vm.UserKey);

            if (user == null)
                return HttpNotFound();

            if (ModelState.IsValid)
            {

                var token = Guid.NewGuid();

                Bus.Send(new PrepareResetPassword()

                             {
                                 AccountId = user.AccountId.ToGuidId(),
                                 UserEmail = user.Email,
                                 UserKey = user.UserKey,
                                 ResetPasswordToken = token,
                                 ResetPasswordLink = Url.Action("ResetPasswordVerification", "Account", new { token = token, area = "" }, "http"),
                                 UserFullname = user.UserFullName
                             });

                //#if DEBUG
                //                ViewBag.ResetPasswordLink = Url.Action("ResetPasswordVerification", "Account", new { token = token, area = "" }, "http");
                //#endif
                return View("ResetPasswordSuccess");

            }

            return View("ResetPassword", vm);
        }

        private bool IsUniqueEmail(string email, Guid ownerKey)
        {
            if (string.IsNullOrEmpty(email)) return false;
            var exists = DbSession.Query<UserIndex.Result, UserIndex>().Any(c => c.Email == email && c.UserKey != ownerKey);
            return !exists;
        }

        private UserLockingVM GetLockingVM(Guid userKey, bool locking)
        {
            var result = DbSession.Query<UserIndex.Result, UserIndex>().SingleOrDefault(p => p.UserKey == userKey);

            if (result == null)
                return null;

            var vm = new UserLockingVM()
            {
                UserKey = result.UserKey,
                FullName = result.UserFullName,
                Locking = locking,
                ReturnUrl = ReturnUrl(result.AccountId.ToGuidId())
            };

            return vm;
        }

        private UserRemovingVM GetRemovingVM(Guid userKey)
        {
            var userResult = DbSession.Query<UserIndex.Result, UserIndex>().SingleOrDefault(p => p.UserKey == userKey);

            if (userResult == null)
                return null;

            var vm = new UserRemovingVM()
            {
                UserKey = userResult.UserKey,
                FullName = userResult.UserFullName,
                ReturnUrl = ReturnUrl(userResult.AccountId.ToGuidId())
            };

            return vm;
        }
    }
}
