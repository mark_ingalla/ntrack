﻿using System.Web.Mvc;
using System.Web.Routing;
using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Subscription.Website.Infrastructure;
using Subscription.Website.ViewModels;

namespace Subscription.Website.Controllers
{
    [RoutePrefix("home")]
    public class HomeController : BaseController
    {
        [GET("", IsAbsoluteUrl = true)]
        public ActionResult Index()
        {
            if (User.IsInRole("Admin"))
                return RedirectToAction("Index", "Home", new {area = "Manager"});
            else if (User.IsInRole("Administrator"))
            {
                return RedirectToAction("Index", "Home", new {area = "Admin"});
            }

            return View(new LoginFormVM());
        }
    }
}
