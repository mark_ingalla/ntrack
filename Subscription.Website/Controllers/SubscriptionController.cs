﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using AutoMapper;
using nTrack.Core.Utilities;
using Subscription.Data;
using Subscription.Data.Indices;
using Subscription.Messages.Commands.Account;
using Subscription.Website.Authorization;
using Subscription.Website.Infrastructure;
using Subscription.Website.ViewModels.Subscription;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Subscription.Website.Controllers
{
    [RoutePrefix("Subscription")]
    [CheckIfCanManageAccount]
    public abstract class SubscriptionController : BaseController
    {
        private readonly IThreadingUtilities _threadingUtilities;

        protected SubscriptionController(IThreadingUtilities threadingUtilities)
        {
            _threadingUtilities = threadingUtilities;
        }

        protected virtual ActionResult ReturnToIndexAction(Guid accountId)
        {
            return RedirectToAction("Index");
        }

        protected virtual string ReturnUrl(Guid accountId)
        {
            return Url.Action("Index");
        }

        [GET("")]
        public ActionResult Index(Guid? accountId)
        {
            var account = DbSession.Load<Account>(accountId ?? ContextAccountId);
            if (account == null)
                return HttpNotFound();

            var accountSubscriptions = account.Subscriptions.Where(p => p.TerminatedOnUTC == null);

            var offersResult =
                DbSession.Query<OfferIndex.Result, OfferIndex>().Where(p => p.IsProductActive && p.IsOfferActive).ToList();


            var subscribedOffers = new List<OfferIndex.Result>();

            foreach (var s in accountSubscriptions)
            {
                var offer = offersResult.SingleOrDefault(p => p.OfferKey == s.OfferKey);
                subscribedOffers.Add(offer);
                offersResult.Remove(offer);
            }

            foreach (var s in subscribedOffers)
            {
                offersResult.RemoveAll(p => p.ProductId == s.ProductId);
            }

            var offersVM = Mapper.Map<List<OfferIndex.Result>, List<SubscriptionProductVM>>(offersResult);
            var subscribedOffersVM = Mapper.Map<List<OfferIndex.Result>, List<SubscriptionProductVM>>(subscribedOffers);

            var indexVM = new SubscriptionIndexVM(subscribedOffersVM, offersVM);

            return View(indexVM);
        }

        [GET("View/{accountId?}")]
        public ActionResult View(Guid? accountId, Guid productId, Guid offerKey)
        {
            var subscriptionCreateVM = CreateSubscriptionViewVM(accountId ?? ContextAccountId, productId, offerKey);
            if (subscriptionCreateVM == null)
                return HttpNotFound();
           
            return View(subscriptionCreateVM);
        }


        [POST("Cancel/{accountId?}")]
        public ActionResult Cancel(Guid? accountId, Guid productId, Guid offerKey)
        {
            var sentTime = DateTime.UtcNow;

            var account = DbSession.Load<Account>(accountId ?? ContextAccountId);
            if (account == null)
                return HttpNotFound();

            if (productId == Guid.Empty)
                return HttpNotFound();

            bool found;

            var accountIdString = "accounts/" + account.Id.ToString();

            var subscription =
                account.Subscriptions.SingleOrDefault(p => p.ProductId == productId && p.TerminatedOnUTC == null);

            Bus.Send(new TerminateSubscription { AccountId = account.Id, SubscriptionKey = subscription.Key});
            found = _threadingUtilities
                .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                 () =>
                                 DbSession.Query<SubscriptionIndex.Result, SubscriptionIndex>().Any(
                                     p =>
                                     p.AccountId == accountIdString && p.ProductId == productId &&
                                     p.UnsubscribedOnUTC > sentTime));

            if (found)
            {
                Success("", "Subscription has been cancelled!");
                return ReturnToIndexAction(account.Id);
            }

            var subscriptionEditVM = CreateSubscriptionViewVM(account.Id,productId, offerKey);

            if (subscriptionEditVM == null)
                return HttpNotFound();

            Info("", "The server is taking to long to confirm your request.");
            subscriptionEditVM.AllowPost = false;
            return View("View", subscriptionEditVM);
        }


        [POST("Create/{accountId?}")]
        public ActionResult Create(Guid? accountId, SubscriptionCreateVM subscription)
        {
            var sentTime = DateTime.UtcNow;

            //var account = DbSession.Load<Account>(accountId ?? ContextAccountID);
            //if (account == null)
            //    return HttpNotFound();

            //if (subscription.ProductId == Guid.Empty)
            //    return HttpNotFound();

            accountId = accountId ?? ContextAccountId;

            CreateSubscription createCommand = new CreateSubscription()
                                                   {
                                                       AccountId = (Guid)accountId,
                                                       OfferKey = subscription.OfferKey,
                                                       ProductId = subscription.ProductId,
                                                       SubscriptionKey = Guid.NewGuid()
                                                   };

            Bus.Send(createCommand);

            RaiseInvoice invoiceCommand = new RaiseInvoice()
                                              {
                                                  AccountId = (Guid) accountId,
                                                  InvoiceId = Guid.NewGuid()
                                              };

            Bus.Send(invoiceCommand);


            var accountIdString = "accounts/" + accountId.ToString();

            bool found;

            found = _threadingUtilities
                .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                 () =>
                                 DbSession.Query<SubscriptionIndex.Result, SubscriptionIndex>().Any(
                                     p =>
                                     p.AccountId == accountIdString && p.ProductId == subscription.ProductId &&
                                     p.SubscribedOnUTC > sentTime));

            if (found)
            {
                Success("", "Product has been subscribed!");
                return ReturnToIndexAction((Guid)accountId);
            }

            Info("", "The server is taking to long to confirm your request.");

            var createVM = CreateSubscriptionViewVM((Guid)accountId, subscription.ProductId, subscription.OfferKey);

            createVM.AllowPost = false;

            return View("View", createVM);
        }

        private SubscriptionCreateVM CreateSubscriptionViewVM(Guid accountId, Guid productId, Guid offerKey)
        {
            var account = DbSession.Load<Account>(accountId);
            if (account == null)
                return null;

            var productIdString = "products/" + productId;

            var offer =
                DbSession.Query<OfferIndex.Result, OfferIndex>().SingleOrDefault(
                    p => p.OfferKey == offerKey && p.ProductId == productIdString && p.IsProductActive && p.IsOfferActive);

            if (offer == null)
                return null;

            var subscriptionCreateVM = new SubscriptionCreateVM();

            subscriptionCreateVM.Subscribed =
                account.Subscriptions.Any(p => p.OfferKey == offerKey && p.ProductId == productId && p.TerminatedOnUTC == null);

            subscriptionCreateVM.ReturnUrl = ReturnUrl(account.Id);

            Mapper.Map(offer, subscriptionCreateVM);

            return subscriptionCreateVM;
        }
    }
}
