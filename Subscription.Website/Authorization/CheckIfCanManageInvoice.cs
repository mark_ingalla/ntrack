﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Subscription.Data;
using Subscription.Website.Controllers;
using Subscription.Website.Infrastructure;

namespace Subscription.Website.Authorization
{
    public class CheckIfCanManageInvoice : ActionFilterAttribute
    {

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controllerUsingThisAttribute = ((BaseController) filterContext.Controller);

            var user = controllerUsingThisAttribute.NTrackUser;
            var account = controllerUsingThisAttribute.NTrackAccount;

            Guid? invoiceId = null;

            if (filterContext.ActionParameters.ContainsKey("invoiceId"))
                invoiceId = (Guid?)filterContext.ActionParameters["invoiceId"];

            if (invoiceId == null)
                invoiceId = Guid.Empty;

            var invoice = controllerUsingThisAttribute.DbSession.Load<Invoice>(invoiceId);



            //if currently logged user is not administrator and passed invoiceId isn't belong to his account
            //we should notify him about not having rights!
            if (!user.IsAdministrator && invoice != null && account != null && invoice.AccountId != account.Id)
                filterContext
                    .HttpContext
                    .Response
                    .RedirectToRoute(new {controller = "Account", action = "HaveNoRights", area = ""});

            base.OnActionExecuting(filterContext);
        }
    }
}