﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Subscription.Data;
using Subscription.Website.Controllers;
using Subscription.Website.Infrastructure;

namespace Subscription.Website.Authorization
{
    public class CheckIfCanManageAccount : ActionFilterAttribute
    {

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controllerUsingThisAttribute = ((BaseController) filterContext.Controller);

            var user = controllerUsingThisAttribute.NTrackUser;
            var account = controllerUsingThisAttribute.NTrackAccount;

            Guid? accountId = null;

            if (filterContext.ActionParameters.ContainsKey("accountId"))
               accountId =  (Guid?) filterContext.ActionParameters["accountId"];

            if (accountId == null)
                accountId = Guid.Empty;

            //if empty, it is manager, we should take his AccountId
            //if not, it is calling by admin, we should leave accountId as it is. 
            if (accountId == Guid.Empty)
                accountId = controllerUsingThisAttribute.NTrackAccount.Id;

            //if currently logged user is not administrator and passed accountId is different than his accountId
            //we should notify him about not having rights!
            if (!user.IsAdministrator && account.Id != accountId)
                filterContext
                    .HttpContext
                    .Response
                    .RedirectToRoute(new {controller = "Account", action = "HaveNoRights", area = ""});

            base.OnActionExecuting(filterContext);
        }
    }
}