﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Subscription.Data;
using Subscription.Website.Controllers;
using Subscription.Website.Infrastructure;

namespace Subscription.Website.Authorization
{
    public class CheckIfCanManageUser : ActionFilterAttribute
    {

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controllerUsingThisAttribute = ((BaseController) filterContext.Controller);

            var account = controllerUsingThisAttribute.NTrackAccount;
            var user = controllerUsingThisAttribute.NTrackUser;

            Guid? userKey = null;

            if (filterContext.ActionParameters.ContainsKey("userKey"))
                userKey = (Guid?)filterContext.ActionParameters["userKey"];

            if (userKey == null)
                userKey = Guid.Empty;

            //if currently logged user is not administrator and passed userId isn't belong to his account
            //we should notify him about not having rights!
            if (!user.IsAdministrator && userKey != Guid.Empty && account.Users.All(p => p.UserKey != userKey))
                filterContext
                    .HttpContext
                    .Response
                    .RedirectToRoute(new {controller = "Account", action = "HaveNoRights", area = ""});

            base.OnActionExecuting(filterContext);
        }
    }
}