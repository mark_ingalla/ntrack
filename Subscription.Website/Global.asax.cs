﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Security;
using NServiceBus;
using nTrack.Data.Extenstions;
using nTrack.Data.Indices;
using Raven.Client;
using Subscription.Website.App_Start;
using Subscription.Website.Infrastructure;

namespace Subscription.Website
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static IBus Bus { get; set; }
        public static string Version { get; set; }
        protected void Application_Start()
        {
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            BundleConfig.ConfigureBundles(BundleTable.Bundles);
            AssemblyVersion.LoadAssemblyVersion();
        }

        protected void Application_AuthenticateRequest()
        {
            if (Request.Url.AbsoluteUri.Contains("/Content") || Request.Url.AbsoluteUri.Contains("/Scripts"))
                return;

            var session = DependencyResolver.Current.GetService<IDocumentSession>();
            try
            {
                var cookie = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value);
                var user = session.Query<UserIndex.Result, UserIndex>()
                    .FirstOrDefault(p => p.UserKey == Guid.Parse(cookie.Name) && !p.IsDeleted && !p.IsLocked);

                if (user == null)
                {
                    FormsAuthentication.SignOut();
                    return;
                }
                var ip = Request.UserHostAddress;

                var identity = new CustomIdentity(user.AccountId.ToGuidId(), user.UserKey, user.UserFullName, user.CompanyName, ip);

                var roles = new List<string> { user.Role };
                if (user.IsAdministrator) roles.Add("Administrator");

                Context.User = new CustomPrincipal(identity, roles.ToArray());
            }
            catch (Exception exception)
            {

            }

        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            // If the exception is caused by an HTTP 401 (Unauthorized) and the user is not authenticated, then redirect to the login page.
            if (Context.Response.StatusCode != 401 ||
                (Context.User != null && Context.User.Identity != null && Context.User.Identity.IsAuthenticated))
                return;

            FormsAuthentication.RedirectToLoginPage();
        }
    }
}