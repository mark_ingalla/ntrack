﻿using AutoMapper;
using Subscription.Data;
using System.Linq;
using Subscription.Website.Areas.Admin.Models;
using Subscription.Website.Areas.Admin.ViewModels;
using Subscription.Website.Areas.Admin.ViewModels.Product;

namespace Subscription.Website.Profiles
{
    public class ProductProfile:Profile
    {
        public ProductProfile()
        {
            Mapper.CreateMap<Product, ProductSummary>()
                .ForMember(d => d.ProductId, opt => opt.MapFrom(s => s.Id))
                .AfterMap((x,y) =>
                              {
                                  y.TotalPrice = x.Features.Sum(p => p.Price) + x.BasePrice;
                              })
                ;
            Mapper.CreateMap<Product, ProductEditViewModel>()
                .ForMember(d => d.ProductId, opt => opt.MapFrom(s => s.Id))
                ;
        }
    }
}