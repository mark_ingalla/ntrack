﻿using AutoMapper;
using nTrack.Data.Extenstions;
using Subscription.Data.Indices;
using Subscription.Website.ViewModels.Subscription;

namespace Subscription.Website.Profiles
{
    public class SubscriptionProfile : Profile
    {
        public SubscriptionProfile()
        {
            Mapper.CreateMap<ProductOfferIndex.Result, SubscriptionProductVM>().ForMember(p => p.ProductId,
                                                                                   src =>
                                                                                   src.MapFrom(p => p.ProductId.ToGuidId()));



            Mapper.CreateMap<ProductOfferIndex.Result, SubscriptionCreateVM>()
                .ForMember(p => p.ProductId, src => src.MapFrom(p => p.ProductId.ToGuidId()));

            Mapper.CreateMap<SubscriptionIndex.Result, SubscriptionProductVM>()
                .ForMember(d => d.ProductId, opt => opt.MapFrom(s => s.ProductId.ToGuidId()))
                ;
        }
    }
}