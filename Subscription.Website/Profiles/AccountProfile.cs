﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Subscription.Data;
using Subscription.Website.ViewModels.Account;

namespace Subscription.Website.Profiles
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            Mapper.CreateMap<Account, AccountVM>()
                .ForMember(d => d.AccountId, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.CompanyName, opt => opt.MapFrom(s => s.CompanyName))
                .ForMember(d => d.Phone, opt => opt.MapFrom(s => s.CompanyPhone));
        }
    }
}