﻿using AutoMapper;
using nTrack.Data;
using Subscription.Messages.Commands.Account.User;
using Subscription.Website.ViewModels.User;

namespace Subscription.Website.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            Mapper.CreateMap<UserVM, UpdateUserDetail>()
                ;

            Mapper.CreateMap<ViewModels.Account.UserVM, UpdateUserDetail>()
               ;

            Mapper.CreateMap<UserVM, AddUser>()
               ;

            Mapper.CreateMap<User, UserVM>()
                ;

            Mapper.CreateMap<User, ViewModels.Account.UserVM>();
        }
    }
}