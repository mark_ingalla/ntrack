﻿using AutoMapper;
using Subscription.Messages.Commands;
using Subscription.Website.ViewModels;
using Subscription.Website.ViewModels.Account;

namespace Subscription.Website.Profiles
{
    public class RegisterProfile:Profile
    {
        public RegisterProfile()
        {
            Mapper.CreateMap<RegisterVM, RegisterAccount>()
                .ForMember(p => p.OfferKey, src => src.MapFrom(p => p.SelectedOfferKey))
                //.ForMember(p => p.ProductId, src => src.MapFrom(p => p.SelectProductId))
                ;
        }
    }
}