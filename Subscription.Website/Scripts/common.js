﻿function resetNav() {
    $('.bs-docs-sidenav').each(function () {
        $(this).find('li').each(function () {
            $(this).removeClass("active");
        });
    });
}
