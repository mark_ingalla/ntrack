﻿using System.ComponentModel.DataAnnotations;

namespace Subscription.Website.ViewModels
{
    public class LoginFormVM
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
        public bool Remember { get; set; }
    }
}