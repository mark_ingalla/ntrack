﻿using System;

namespace Subscription.Website.ViewModels.User
{
    public class UserRemovingVM : ReturnUrlVM
    {
        public Guid UserKey { get; set; }
        public Guid AccountId { get; set; }

        public bool AllowPost { get; set; }

        public string FullName { get; set; }

        public UserRemovingVM()
        {
            AllowPost = true;
        }
    }
}