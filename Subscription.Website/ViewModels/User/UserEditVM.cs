using System;
using System.ComponentModel.DataAnnotations;
using Subscription.Messages;

namespace Subscription.Website.Areas.Manager.ViewModels.User
{
    public class UserEditVM
    {
        public Guid UserId { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        public string Phone { get; set; }

        public string Mobile { get; set; }

        public bool IsAdmin { get; set; }

        [Required]
        [RegularExpression(ValidationPatterns.EMAIL, ErrorMessage = "Email is invalid.")]
        public string Email { get; set; }

        public string FullName { get { return string.Format("{0} {1}", FirstName, LastName).Trim(); } }

        public bool AllowPost { get; set; }

        public UserEditVM()
        {
            AllowPost = true;
        }
    }
}