﻿using System;

namespace Subscription.Website.ViewModels.User
{
    public class UserResetPasswordVM : ReturnUrlVM
    {
        public Guid UserKey { get; set; }
        public string UserFullName { get; set; }
        public Guid AccountId { get; set; }
    }
}