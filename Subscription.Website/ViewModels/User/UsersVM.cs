﻿using System.Collections.Generic;

namespace Subscription.Website.ViewModels.User
{
    public class UsersVM
    {
        public List<nTrack.Data.User> Users { get; set; }

        public UsersVM(List<nTrack.Data.User> users)
        {
            Users = users;
        }
    }
}