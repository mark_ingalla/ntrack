﻿using System;
using System.Collections.Generic;
using Subscription.Data;
using Subscription.Website.Areas.Manager.ViewModels.Common;

namespace Subscription.Website.ViewModels.User
{
    public class UserEditPermissionsVM : ReturnUrlVM
    {
        public List<ProductRoleVM> SubscribedProductWithPermissions { get; set; }

        public bool AllowPost { get; set; }

        public string UserFullName { get; set; }

        public Guid UserKey { get; set; }

        public UserEditPermissionsVM()
        {
            SubscribedProductWithPermissions = new List<ProductRoleVM>();
            AllowPost = true;
        }

        public UserEditPermissionsVM(List<Product> products)
            : this()
        {

            foreach (var product in products)
            {
                var vm = new ProductRoleVM()
                             {
                                 Id = product.Id,
                                 Name = product.Name

                             };

                //foreach (var permission in product.Permissions)
                //{
                //    vm.Permissions.Add(new ProductRolePermissionVM()
                //               {
                //                   Code = permission.Code,
                //                   IsSelected = false,
                //                   Name = permission.Name
                //               });
                //}

                SubscribedProductWithPermissions.Add(vm);
            }
        }

        public UserEditPermissionsVM(List<Product> products, nTrack.Data.User user)
            : this(products)
        {
            UserFullName = user.FullName;
            UserKey = user.UserKey;

            //foreach (var product in user.Permissions)
            //{
            //    var p = SubscribedProductWithPermissions.SingleOrDefault(x => x.Id == product.Key);
            //    if (p != null)
            //    {
            //        foreach (var code in product.Value)
            //        {
            //            var permission = p.Permissions.SingleOrDefault(x => x.Code == code);
            //            if (permission != null)
            //                permission.IsSelected = true;
            //        }
            //    }
            //}
        }
    }
}