﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Subscription.Website.ViewModels.User
{
    public class UserLockingVM : ReturnUrlVM
    {
        public Guid UserKey { get; set; }

        public Guid AccountId { get; set; }

        [Required]
        public string Reason { get; set; }

        public bool AllowPost { get; set; }

        public string FullName { get; set; }

        public bool Locking { get; set; }

        public UserLockingVM()
        {
            AllowPost = true;
        }
    }
}