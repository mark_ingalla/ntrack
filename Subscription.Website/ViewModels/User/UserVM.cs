using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Subscription.Messages;
using Subscription.Messages.Commands;

namespace Subscription.Website.ViewModels.User
{
    public class UserVM : ReturnUrlVM
    {
        public Guid AccountId { get; set; }

        public Guid UserKey { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [RegularExpression(ValidationPatterns.EMAIL, ErrorMessage = "Email is invalid.")]
        public string Email { get; set; }

        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Repeat password")]
        [Compare("Password", ErrorMessage = "Password and repeat password do not match.")]
        public string RepeatPassword { get; set; }

        public string Phone { get; set; }

        public string Mobile { get; set; }

        public bool AllowPost { get; set; }

        public SelectList Roles { get; set; }

        public string Role { get; set; }

        public string FullName { get { return string.Format("{0} {1}", FirstName, LastName).Trim(); } }

        public UserVM(string[] roles, Guid accountId, nTrack.Data.User user) : this(roles, accountId)
        {
            UserKey = user.UserKey;
            FirstName = user.FirstName;
            LastName = user.LastName;
            Email = user.Email;
            Password = user.Password;
            Phone = user.Phone;
            Mobile = user.Mobile;
            Role = user.Role;
        }

        public UserVM(string[] roles, Guid accountId)
        {
            var list = new List<SelectListItem>();
            foreach (var role in roles)
            {
                list.Add(new SelectListItem()
                             {
                                 Text = role,
                                 Value = role
                             });
            }

            Roles = new SelectList(roles);

            AccountId = accountId;

            AllowPost = true;
        }

        public UserVM()
        {}
    }
}