﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Subscription.Data;

namespace Subscription.Website.ViewModels.User
{
    
    public class UserSelectRoleVM : ReturnUrlVM
    {
        public SelectList Roles { get; set; }
        public Guid UserId { get; set; }
        public string UserFullName { get; set; }

        [Required]
      //  [Display(Name = "Role")] Why when we set this, we have error on post in view? (about SelectRole must be SelectList not Guid o.O)
        public Guid SelectedRole { get; set; }

        public bool AllowPost { get; set; }

        public UserSelectRoleVM()
        {
            AllowPost = true;
        }

        public UserSelectRoleVM(List<AccountRole> roles, Data.User user)
        {
            Roles = new SelectList(roles, "Key", "Name");
            UserId = user.Id;
 
            UserFullName = user.FullName;
        }
    }
}