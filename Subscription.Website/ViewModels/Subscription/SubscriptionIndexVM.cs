﻿
using System.Collections.Generic;
using System.Linq;
using Subscription.Data.Indices;

namespace Subscription.Website.ViewModels.Subscription
{
    public class SubscriptionIndexVM
    {
        public SubscriptionProductVM[] Subscriptions { get; set; }

        public SubscriptionIndexVM()
        {}

        public SubscriptionIndexVM(List<SubscriptionIndex.Result> subscriptions)
        {
            Subscriptions = subscriptions.Select(p => new SubscriptionProductVM(p)).ToArray();
        }
    }
}