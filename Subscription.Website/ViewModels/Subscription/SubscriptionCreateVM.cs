﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Subscription.Data;
using Subscription.Data.Indices;
using nTrack.Data.Extenstions;

namespace Subscription.Website.ViewModels.Subscription
{
    public class SubscriptionCreateVM : ReturnUrlVM
    {
        public Guid ProductId { get; set; }
        public Guid AccountId { get; set; }
        public Guid OfferKey { get; set; }

        [Display(Name = "Product Name")]
        public string OfferName { get; set; }

        public string ImageUrl { get; set; }
        public bool AllowPost { get; set; }
        public bool Subscribed { get; set; }
        public List<ProductFeatureSummary> Features { get; set; }

        public SubscriptionCreateVM()
        {
            AllowPost = true;
            Features = new List<ProductFeatureSummary>();
        }

        public SubscriptionCreateVM(ProductOfferIndex.Result result, bool subscribed)
            : this()
        {
            Subscribed = subscribed;
            ProductId = result.ProductId.ToGuidId();
            OfferKey = result.OfferKey;
            OfferName = result.OfferName;
            ImageUrl = result.ImageUrl;
        }
    }

}