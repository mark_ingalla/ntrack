﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Subscription.Website.ViewModels.Subscription
{
    public class SubscriptionEditVM : ReturnUrlVM
    {
        public Guid SubscriptionKey { get; set; }
        public Guid ProductId { get; set; }
        public Guid OfferKey { get; set; }

        [Display(Name = "Product Name")]
        public string OfferName { get; set; }

        public string ImageUrl { get; set; }
        public bool AllowPost { get; set; }

        public SubscriptionEditVM()
        {
            AllowPost = true;
        }

    }
}