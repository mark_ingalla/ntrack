﻿using System;
using Subscription.Data.Indices;
using nTrack.Data.Extenstions;

namespace Subscription.Website.ViewModels.Subscription
{
    public class SubscriptionProductVM
    {
        public Guid SubscriptionKey { get; set; }

        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }

        public Guid OfferKey { get; set; }
        public string OfferName { get; set; }

        public DateTime SubscribedOnUTC { get; set; }
        public DateTime? TerminatedOnUTC { get; set; }

        public decimal Amount { get; set; }

        public SubscriptionProductVM()
        {}

        public SubscriptionProductVM(SubscriptionIndex.Result subscriptionResult)
        {
            SubscriptionKey = subscriptionResult.SubscriptionKey;
            ProductId = subscriptionResult.ProductId.ToGuidId();
            ProductName = subscriptionResult.ProductName;
            ProductCode = subscriptionResult.ProductCode;
            OfferKey = subscriptionResult.OfferKey;
            OfferName = subscriptionResult.OfferName;
            SubscribedOnUTC = subscriptionResult.SubscribedOnUTC;
            TerminatedOnUTC = subscriptionResult.TerminatedOnUTC;
            Amount = subscriptionResult.Amount;
        }
    }
}