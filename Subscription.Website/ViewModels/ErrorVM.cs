﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Subscription.Website.ViewModels
{
    public class ErrorVM
    {
        public string ErrorMessage { get; set; }

        public ErrorVM(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }
    }
}