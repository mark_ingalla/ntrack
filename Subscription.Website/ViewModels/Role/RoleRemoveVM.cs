﻿using System;

namespace Subscription.Website.ViewModels.Role
{
    public class RoleRemoveVM : ReturnUrlVM
    {
        public Guid Key { get; set; }
        public Guid AccountId { get; set; }
        public string RoleName { get; set; }

        public bool AllowPost { get; set; }

        public RoleRemoveVM()
        {
            AllowPost = true;
        }
    }
}