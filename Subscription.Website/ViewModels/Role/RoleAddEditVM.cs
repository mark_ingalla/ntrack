using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Subscription.Data;
using Subscription.Website.Areas.Manager.ViewModels.Common;

namespace Subscription.Website.ViewModels.Role
{
    

    public class RoleAddEditVM : ReturnUrlVM
    {
        public List<ProductRoleVM> SubscribedProductWithPermissions { get; set; }

        public bool AllowPost { get; set; }

        [Display(Name="Role name")]
        [Required]
        public string RoleName { get; set; }

        public Guid AccountId { get; set; }
        public Guid Key { get; set; }

        public RoleAddEditVM()
        {
            SubscribedProductWithPermissions = new List<ProductRoleVM>();
            AllowPost = true;
        }

        public RoleAddEditVM(List<Product> products, Guid accountId) : this()
        {
            AccountId = accountId;

            foreach (var product in products)
            {
                var vm = new ProductRoleVM()
                             {
                                 Id = product.Id,
                                 Name = product.Name
                                 
                             };

                foreach (var permission in product.Permissions)
                {
                    vm.Permissions.Add(new ProductRolePermissionVM()
                               {
                                   Code = permission.Code,
                                   IsSelected = false,
                                   Name = permission.Name
                               });
                }

                SubscribedProductWithPermissions.Add(vm);
            }
        }

        public RoleAddEditVM(List<Product> products, AccountRole role, Guid accountId)
            : this(products, accountId)
        {
            RoleName = role.Name;
            Key = role.Key;

            foreach (var product in role.Permissions)
            {
                var p = SubscribedProductWithPermissions.SingleOrDefault(x => x.Id == product.Key);
                if (p != null)
                {
                    foreach (var code in product.Value)
                    {
                        var permission = p.Permissions.SingleOrDefault(x => x.Code == code);
                        if (permission != null)
                            permission.IsSelected = true;
                    }
                }
            }
        }
    }
}