﻿using System.Collections.Generic;
using Subscription.Data;

namespace Subscription.Website.ViewModels.Role
{
    public class RolesVM
    {
        public List<AccountRole> Roles { get; set; }


        public RolesVM(List<AccountRole> roles)
        {
            Roles = roles;
        }
    }
}