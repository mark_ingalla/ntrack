﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Subscription.Website.ViewModels
{
    public class PasswordRestedVM
    {
        public string NewPassword { get; set; }
    }
}