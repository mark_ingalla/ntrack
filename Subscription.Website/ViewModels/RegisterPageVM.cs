﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using nTrack.Core.Utilities;
using Subscription.Messages;

namespace Subscription.Website.ViewModels
{
    public class RegisterPageVM
    {
        [Required]
        [StringLength(50, MinimumLength = 3)]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Required]
        [Display(Name = "Company Phone")]
        public string CompanyPhone { get; set; }

        [Required]
        [Display(Name = "Country")]
        public string Country { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 2)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 2)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [RegularExpression(ValidationPatterns.EMAIL, ErrorMessage = "Email is invalid.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 8)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Repeat password")]
        [Compare("Password", ErrorMessage = "Password and repeat password do not match.")]
        public string RepeatPassword { get; set; }

        public readonly IEnumerable<SelectListItem> CountryOptions;
        public RegisterPageVM(IEnumerable<string> countryOptions)
        {
            CountryOptions = countryOptions.Select(x => new SelectListItem {Text = x, Value = x});
        }

        public RegisterPageVM(){}
    }

    
    public static class ModelStateExtensions
    {
        /// <summary>
        /// Adds an error for the property if the property does not already have an error associated with it. If the error is not
        /// associated with a certain property (propertyName is null or empty), then the error is added anyway.
        /// </summary>
        public static void AddErrorIfPropertyHasNoError(this ModelStateDictionary modelState, string propertyName, string errorMessage)
        {
            if (string.IsNullOrEmpty(propertyName) || !modelState.Keys.Any(key => key == propertyName && modelState[key].Errors.Count > 0))
                modelState.AddModelError(propertyName ?? string.Empty, errorMessage);
        }

        /// <summary>
        /// Adds the model errors for properties that do not already have an error in the ModelStateDictionary. Errors that are not associated
        /// with a certain property are always added.
        /// </summary>
        public static void AddModelErrors(this ModelStateDictionary modelState, object model)
        {
            AddModelErrors(modelState, model, null);
        }

        /// <summary>
        /// Adds the model errors for properties that do not already have an error in the ModelStateDictionary. Errors that are not associated
        /// with a certain property are always added. The propertyToDisplayName dictionary is used to replace property names by display names
        /// in the error messages.
        /// </summary>
        public static void AddModelErrors(this ModelStateDictionary modelState, object model, Dictionary<string, string> propertyToDisplayName)
        {
            ICollection<ValidationResult> validationErrors = new ValidationUtilities().GetValidationErrors(model);
            if (propertyToDisplayName != null && propertyToDisplayName.Count > 0)
            {
                string propertyName;
                foreach (ValidationResult error in validationErrors)
                {
                    propertyName = error.MemberNames.FirstOrDefault();
                    if (!string.IsNullOrEmpty(propertyName) && propertyToDisplayName.ContainsKey(propertyName))
                        error.ErrorMessage = error.ErrorMessage.Replace(propertyName, propertyToDisplayName[propertyName]);
                }
            }

            foreach (ValidationResult error in validationErrors)
                modelState.AddErrorIfPropertyHasNoError(error.MemberNames.FirstOrDefault(), error.ErrorMessage);
        }
    }
}