﻿using System.ComponentModel.DataAnnotations;

namespace Subscription.Website.ViewModels
{
    public class ForgotPasswordPageVM
    {
        [Required]
        public string Email { get; set; }
    }
}