﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Subscription.Website.ViewModels.Image;

namespace Subscription.Website.ViewModels.Account
{
    public class AccountVM
    {
        public Guid? AccountId { get; set; }

        [Required]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        public string Phone { get; set; }
        [Display(Name = "Address Line 1")]
        public string AddressLine1 { get; set; }
        [Display(Name = "Address Line 2")]
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string ReturnUrl { get; set; }
        public bool AllowPost { get; set; }

        public ImageVM ImageVM { get; set; }

        public IEnumerable<SelectListItem> CountryOptions;

        public AccountVM(IEnumerable<string> countryOptions, Data.Account account) : this(account)
        {
            CountryOptions = countryOptions.Select(x => new SelectListItem {Text = x, Value = x});
        }

        public AccountVM(Data.Account account) : this()
        {
            AccountId = account.Id;
            CompanyName = account.CompanyName;
            Phone = account.CompanyPhone;
            AddressLine1 = account.AddressLine1;
            AddressLine2 = account.AddressLine2;
            Suburb = account.Suburb;
            State = account.State;
            Postcode = account.Postcode;
            Country = account.Country;
        }

        public AccountVM()
        {
            AllowPost = true;
        }

    }
}