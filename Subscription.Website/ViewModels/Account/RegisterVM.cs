﻿
using Subscription.Messages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Subscription.Messages.Commands;
using Subscription.Website.Infrastructure;

namespace Subscription.Website.ViewModels.Account
{
    public class RegisterVM
    {
        [Required]
        [StringLength(50, MinimumLength = 3)]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Required]
        [Display(Name = "Company Phone")]
        public string CompanyPhone { get; set; }

        [Required]
        [Display(Name = "Country")]
        public string Country { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 2)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 2)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [RegularExpression(ValidationPatterns.EMAIL, ErrorMessage = "Email is invalid.")]
        [Display(Name = "Email")]
        [Remote("JSONIsUniqueEmail", "Account", ErrorMessage = "Email is already registered.")]
        public string Email { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 8)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Repeat password")]
        [Compare("Password", ErrorMessage = "Password and repeat password do not match.")]
        public string RepeatPassword { get; set; }

        [Display(Name = "Countries")]
        public SelectListItem[] CountryOptions { get; private set; }

        [Display(Name = "Products")]
        public SelectListItem[] ProductOptions { get; private set; }

        public Guid? SelectProductId { get; set; }
        public Guid? SelectedOfferKey { get; set; }


        [Display(Name = "Number of sites")]
        [Remote("LimitQuantity", "Account", AdditionalFields = "SelectedOfferKey,SelectProductId", ErrorMessage = "You can't specify more sites for this offer.")]
        //[Range(1, int.MaxValue)]        
        public int OfferQTY { get; set; }

        public RegisterVM(string[] contries, Dictionary<Guid, string> products) : this()
        {
            ProductOptions =
                products.Select(x => new SelectListItem { Text = x.Value, Value = x.Key.ToString() }).ToArray();
            CountryOptions = contries.Select(x => new SelectListItem { Text = x, Value = x }).ToArray();
        }

        public RegisterVM()
        {
            OfferQTY = 1;
        }
    }
}