using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Subscription.Messages;
using Subscription.Messages.Commands;

namespace Subscription.Website.ViewModels.Account
{
    public class UserVM
    {
        public Guid UserKey { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [RegularExpression(ValidationPatterns.EMAIL, ErrorMessage = "Email is invalid.")]
        public string Email { get; set; }

        [StringLength(30, MinimumLength = 8)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [StringLength(30, MinimumLength = 8)]
        [Display(Name = "New password")]
        public string Password { get; set; }

        [Display(Name = "Repeat password")]
        [Compare("Password", ErrorMessage = "Password and repeat password do not match.")]
        public string RepeatPassword { get; set; }

        public string Phone { get; set; }

        public string Mobile { get; set; }

        public bool AllowPost { get; set; }

        public string ReturnUrl { get; set; }

        public string FullName { get { return string.Format("{0} {1}", FirstName, LastName).Trim(); } }

        public UserVM()
        {
            AllowPost = true;
        }

        public UserVM(nTrack.Data.User user) : this()
        {
            UserKey = user.UserKey;
            FirstName = user.FirstName;
            LastName = user.LastName;
            Email = user.Email;
            Phone = user.Phone;
            Mobile = user.Mobile;
        }
    }
}