﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Subscription.Website.ViewModels.Image
{
    public class ImageVM
    {
        public string Entity { get; set; }
        public Guid Id { get; set; }
        public bool Avaible { get; set; }
    }
}