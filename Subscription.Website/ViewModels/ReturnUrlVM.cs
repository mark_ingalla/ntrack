﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Subscription.Website.ViewModels
{
    public class ReturnUrlVM
    {
        public string ReturnUrl { get; set; }
    }
}