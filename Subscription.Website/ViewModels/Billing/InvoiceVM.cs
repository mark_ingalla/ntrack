﻿using Subscription.Data;
using System;

namespace Subscription.Website.ViewModels.Billing
{
    public class InvoiceVM
    {
        public Guid InvoiceId { get; set; }
        public int InvoiceNumber { get; set; }
        public string OfferName { get; set; }
        public Guid OfferKey { get; set; }
        public DateTime InvoiceCreateDate { get; set; }
        public decimal Amount { get; set; }
        public bool Paid { get; set; }
        public string FileUrl { get; set; }

        public InvoiceVM(Invoice invoice, ProductOffer offer, string path)
        {
            InvoiceId = invoice.Id;
            InvoiceNumber = invoice.InvoiceNumber;
            OfferName = offer.Name;
            OfferKey = offer.Key;
            InvoiceCreateDate = invoice.IssuedOnUTC;
            Amount = invoice.Total;
            Paid = invoice.IsPaid;
            if (!string.IsNullOrWhiteSpace(invoice.FileName))
                FileUrl = path + invoice.FileName;
        }
    }
}