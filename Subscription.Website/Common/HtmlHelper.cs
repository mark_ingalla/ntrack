﻿using System.Web.Mvc;

namespace Subscription.Website.Common
{
    public static class MvcHelpers
    {

        public static string GetActiveMenuClass(this HtmlHelper helper, string areaName, string controllerName, string actionName, ViewContext view)
        {
           
            var currentController = view.Controller.ValueProvider.GetValue("controller").RawValue;
            var currentAction = view.Controller.ValueProvider.GetValue("action").RawValue;
            var currentArea = view.RouteData.DataTokens["area"];

            if (currentArea == null)
                currentArea = "";

            
            if (actionName == null)
            {
                if (controllerName == null)
                {
                    if (areaName == null)
                        return "active";

                    if (currentArea.ToString() == areaName)
                        return "active";
                    return "";
                }

                if (currentController.ToString() == controllerName)
                    return "active";
                return "";
            }


            if (currentController.ToString() == controllerName && actionName == currentAction.ToString() && areaName == currentArea.ToString())
                return "active";
            return "";
        }

    }
}