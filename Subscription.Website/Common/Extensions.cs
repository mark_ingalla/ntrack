﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace Subscription.Website.Common
{
    public static class Extensions
    {
        public static string ToQueryString(this NameValueCollection pairs)
        {
            return string.Join("&", Array.ConvertAll(
                pairs.AllKeys, key => string.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(pairs[key]))));
        }
    }
}