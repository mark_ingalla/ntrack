﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Subscription.Website.Services
{
    public enum SettingName
    {
        PayPalNvpServerUrl,
        PayPalUserLoginUrl,
        PayPalApiUsername,
        PayPalApiPassword,
        PayPalApiSignature
    }
}