﻿using System.Collections.Generic;
using System.Linq;
using Raven.Client;
using Raven.Client.Linq;
using Subscription.Data.Indices;
using Subscription.Data;


namespace Subscription.Website.Services
{
    public interface IAccountService
    {
        List<Product> GetSubscribedProducts(IDocumentSession dbSession, string contextAccountRavenId);
    }

    public class AccountService : BaseService, IAccountService
    {
        public List<Product> GetSubscribedProducts(IDocumentSession dbSession, string contextAccountRavenId)
        {
            var results = dbSession.Query<AccountProductIndex.Result, AccountProductIndex>()
                .Where(x => x.AccountId == contextAccountRavenId && x.TerminatedOnUTC == null).ToList();

            List<Product> subscribedProducts = new List<Product>();

            if (results.Any())
                subscribedProducts =
                    dbSession.Load<Product>(results.Select(p => "products/" + p.ProductId.ToString())).ToList();

            return subscribedProducts;
        }

    }
}