﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Subscription.Website.Services
{
    public class ConfigurationService : BaseService
    {
            public string GetSettingValue(SettingName settingName)
            {
                return ConfigurationManager.AppSettings[settingName.ToString()];
            }
    }
}