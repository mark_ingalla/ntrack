﻿using System;
using System.Web;

namespace Subscription.Website.Services
{
    public class ClientSideStoreServices : IClientSideStoreServices
    {
        private const string AUTHENTICATION_COOKIE_NAME = "nTrack";
        private const string CONTEXT_COOKIE_NAME = "nTrackContext";
        private const string SESSION_TOKEN_KEY = "SessionToken";
        private const string CONTEXT_ACCOUNT_KEY = "Account";


        public Guid? GetSessionToken(HttpCookieCollection requestCookies)
        {
            Guid? sessionToken = null;

            HttpCookie authenticationCookie = requestCookies[AUTHENTICATION_COOKIE_NAME];
            if (authenticationCookie != null && authenticationCookie.Values[SESSION_TOKEN_KEY] != null)
                sessionToken = Guid.Parse(authenticationCookie.Values[SESSION_TOKEN_KEY]);

            return sessionToken;
        }

        public void SetSessionToken(HttpCookieCollection responseCookies, Guid token, int daysToKeepCookie)
        {
            var authCookie = new HttpCookie(AUTHENTICATION_COOKIE_NAME);
            authCookie[SESSION_TOKEN_KEY] = token.ToString();
            if (daysToKeepCookie > 0)
                authCookie.Expires = DateTime.Now.AddDays(daysToKeepCookie);
            responseCookies.Add(authCookie);
        }

        public void DeleteSessionToken(HttpCookieCollection responseCookies)
        {
            var expiredCookie = new HttpCookie(AUTHENTICATION_COOKIE_NAME);
            expiredCookie.Expires = DateTime.Now.AddDays(-1d);
            responseCookies.Add(expiredCookie);
        }

        //public Guid? GetContextAccountID(HttpCookieCollection requestCookies)
        //{
        //    Guid? contextAccountID = null;

        //    HttpCookie contextCookie = requestCookies[CONTEXT_COOKIE_NAME];
        //    if (contextCookie != null && contextCookie.Values[CONTEXT_ACCOUNT_KEY] != null)
        //        contextAccountID = Guid.Parse(contextCookie.Values[CONTEXT_ACCOUNT_KEY]);

        //    return contextAccountID;
        //}

        //public void SetContextAccountID(HttpCookieCollection responseCookies, Guid contextAccountID)
        //{
        //    HttpCookie contextCookie = new HttpCookie(CONTEXT_COOKIE_NAME);
        //    contextCookie[CONTEXT_ACCOUNT_KEY] = contextAccountID.ToString();
        //    responseCookies.Add(contextCookie);
        //}
    }
}