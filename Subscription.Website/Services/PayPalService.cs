﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System;
using Raven.Client;
using Subscription.Data;
using Subscription.Website.Common;

namespace Subscription.Website.Services
{
    public interface  IPayPalService
    {
        bool SetExpressCheckout(IDocumentSession dbSession, int invoiceNumber, string description, decimal totalAmount,
                                string currencyCode, string localeCode,
                                bool displayShippingAddress, string logoImageUrl, string returnUrl, string cancelUrl,
                                out string token);

        string GetExpressCheckoutRedirectUrl(string token);

        bool DoExpressCheckout(IDocumentSession dbSession, string token, string payerID, int invoiceNumber,
                               decimal totalAmount, string currencyCode);


    }
    public class PayPalService : IBaseService, IPayPalService
    {
        private const string API_VERSION = "84.0";


        public bool SetExpressCheckout(IDocumentSession dbSession, int invoiceNumber, string description, decimal totalAmount, string currencyCode, string localeCode,
            bool displayShippingAddress, string logoImageUrl, string returnUrl, string cancelUrl, out string token)
        {
            ConfigurationService configurationService = new ConfigurationService();

            NameValueCollection requestNvps = new NameValueCollection
            {
                { "USER", configurationService.GetSettingValue(SettingName.PayPalApiUsername) },
                { "PWD", configurationService.GetSettingValue(SettingName.PayPalApiPassword) },
                { "SIGNATURE", configurationService.GetSettingValue(SettingName.PayPalApiSignature) },
                { "VERSION", API_VERSION },

                { "METHOD", "SetExpressCheckout" },
                
                { "PAYMENTREQUEST_0_PAYMENTACTION", "Sale" },
                { "PAYMENTREQUEST_0_AMT", totalAmount.ToString("0.00") },
                { "PAYMENTREQUEST_0_CURRENCYCODE", currencyCode },
                { "PAYMENTREQUEST_0_DESC", description },
                { "LOCALECODE", localeCode },
                { "NOSHIPPING", displayShippingAddress ? "0" : "1" },

                { "HDRIMG", logoImageUrl },
                
                { "RETURNURL", returnUrl },
                { "CANCELURL", cancelUrl }
            };

            NameValueCollection responseNvps = ExecuteApiOperation(requestNvps);
            LogPayPalMessage(dbSession, invoiceNumber, HttpMessageDirection.Request, requestNvps);
            LogPayPalMessage(dbSession, invoiceNumber, HttpMessageDirection.Response, responseNvps);

            bool success;
            if (responseNvps["ACK"] == "Success" || responseNvps["ACK"] == "SuccessWithWarning")
            {
                success = true;
                token = responseNvps["TOKEN"];
            }
            else
            {
                success = false;
                token = null;
            }

            return success;
        }

        public string GetExpressCheckoutRedirectUrl(string token)
        {
            ConfigurationService configurationService = new ConfigurationService();

            NameValueCollection urlParameters = new NameValueCollection
            {
                { "cmd", "_express-checkout" },
                { "useraction", "commit" },
                { "token", token }
            };

            string userLoginUrl = configurationService.GetSettingValue(SettingName.PayPalUserLoginUrl);
            return string.Format("{0}?{1}", userLoginUrl, urlParameters.ToQueryString());
        }

        public bool DoExpressCheckout(IDocumentSession dbSession, string token, string payerID, int invoiceNumber, decimal totalAmount, string currencyCode)
        {
            ConfigurationService configurationService = new ConfigurationService();

            NameValueCollection requestNvps = new NameValueCollection
            {
                { "USER", configurationService.GetSettingValue(SettingName.PayPalApiUsername) },
                { "PWD", configurationService.GetSettingValue(SettingName.PayPalApiPassword) },
                { "SIGNATURE", configurationService.GetSettingValue(SettingName.PayPalApiSignature) },
                { "VERSION", API_VERSION },

                { "METHOD", "DoExpressCheckoutPayment" },

                { "TOKEN", token },
                { "PAYERID", payerID },

                { "PAYMENTREQUEST_0_AMT", totalAmount.ToString("0.00") },
                { "PAYMENTREQUEST_0_CURRENCYCODE", currencyCode },
                { "PAYMENTREQUEST_0_PAYMENTACTION", "Sale" }
            };

            NameValueCollection responseNvps = ExecuteApiOperation(requestNvps);
            LogPayPalMessage(dbSession, invoiceNumber, HttpMessageDirection.Request, requestNvps);
            LogPayPalMessage(dbSession, invoiceNumber, HttpMessageDirection.Response, responseNvps);

            return (responseNvps["ACK"] == "Success" || responseNvps["ACK"] == "SuccessWithWarning");
        }


        private NameValueCollection ExecuteApiOperation(NameValueCollection requestNvps)
        {
            ConfigurationService configurationService = new ConfigurationService();
            string requestBodyText = requestNvps.ToQueryString();
            byte[] requestBodyBytes = Encoding.UTF8.GetBytes(requestBodyText);
            WebRequest apiRequest = WebRequest.Create(configurationService.GetSettingValue(SettingName.PayPalNvpServerUrl));
            apiRequest.Method = "POST";
            apiRequest.ContentType = "application/x-www-form-urlencoded";
            apiRequest.ContentLength = requestBodyBytes.Length;
            Stream apiRequestStream = apiRequest.GetRequestStream();
            apiRequestStream.Write(requestBodyBytes, 0, requestBodyBytes.Length);
            apiRequestStream.Close();

            WebResponse apiResponse = apiRequest.GetResponse();
            Stream responseStream = apiResponse.GetResponseStream();
            string responseBodyText = new StreamReader(responseStream).ReadToEnd();
            NameValueCollection responseNvps = HttpUtility.ParseQueryString(responseBodyText);
            return responseNvps;
        }

        private void LogPayPalMessage(IDocumentSession dbSession, int invoiceNumber, HttpMessageDirection messageDirection, NameValueCollection nvps)
        {
            string nvpsStr = nvps.ToQueryString().Replace("&", "\r\n").Replace("=", ": ");
            DateTime webServerTime = DateTime.UtcNow;

            PayPalMessageLog messageLog = new PayPalMessageLog
            {
                InvoiceNumber = invoiceNumber,
                Time = webServerTime,
                MessageDirection = messageDirection.ToString(),
                NameValuePairs = nvpsStr
            };

            dbSession.Store(messageLog);
            dbSession.SaveChanges();
        }

        public List<ServiceError> ErrorMessages { get; private set; }
        public bool HasErrors { get; private set; }
    }
}