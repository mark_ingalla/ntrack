﻿using System;
using System.IO;
using Raven.Abstractions.Data;
using Raven.Client;

namespace Subscription.Website.Services
{
    public interface IAttachmentService
    {
        Attachment GetAttachment(Guid id, string entity);
        void PutAttachment(Guid id, string entity, Stream data);
    }

    public class AttachmentService : IAttachmentService
    {
        private readonly IDocumentStore _store;

        public AttachmentService(IDocumentStore store)
        {
            _store = store;
        }

        public Attachment GetAttachment(Guid id, string entity)
        {
            var attachment = _store.DatabaseCommands.GetAttachment(entity + "/" + id.ToString());
            return attachment;
        }

        public void PutAttachment(Guid id, string entity, Stream data)
        {
            _store.DatabaseCommands.PutAttachment(entity + "/" + id.ToString(), null,
                                                         data,
                                                         null);
        }

    }
}
