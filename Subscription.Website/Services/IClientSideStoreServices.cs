﻿using System;
using System.Web;

namespace Subscription.Website.Services
{
    public interface IClientSideStoreServices
    {
        Guid? GetSessionToken(HttpCookieCollection requestCookies);
        void SetSessionToken(HttpCookieCollection responseCookies, Guid token, int daysToKeepCookie);
        void DeleteSessionToken(HttpCookieCollection responseCookies);
        //Guid? GetContextAccountID(HttpCookieCollection requestCookies);
        //void SetContextAccountID(HttpCookieCollection responseCookies, Guid contextAccountID);
    }
}