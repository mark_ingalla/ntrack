﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Raven.Client;

namespace Subscription.Website.Services
{
    public interface IBaseService
    {
        List<ServiceError> ErrorMessages { get; }

        bool HasErrors { get; }

    }
}