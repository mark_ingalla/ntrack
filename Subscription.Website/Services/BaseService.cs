﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Raven.Client;

namespace Subscription.Website.Services
{
    public class ServiceError
    {
        public string Key { get; set; }
        public string Message { get; set; }

        public ServiceError()
        {}

        public ServiceError(string key, string message)
        {
            Key = key;
            Message = message;
        }
    }

    public abstract class BaseService : IBaseService
    {
        protected List<ServiceError> _errorMessages;

        public List<ServiceError> ErrorMessages { get { return _errorMessages; } }

        public bool HasErrors { get { return ErrorMessages.Count > 0; } }

        protected BaseService()
        {
            _errorMessages = new List<ServiceError>();
        
        }

        protected void AddError(string key, string message)
        {
            ErrorMessages.Add(new ServiceError(key, message));
        }
    }
}