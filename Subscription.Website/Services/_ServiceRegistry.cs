﻿using nTrack.Core.Utilities;
using StructureMap.Configuration.DSL;

namespace Subscription.Website.Services
{
    public class ServiceRegistry:Registry
    {
        public ServiceRegistry()
        {
            For<ICryptographyService>().Use<CryptographyService>();
            For<IThreadingUtilities>().Use<ThreadingUtilities>();
            For<IAccountService>().Use<AccountService>();
            For<IPayPalService>().Use<PayPalService>();
        }
    }
}