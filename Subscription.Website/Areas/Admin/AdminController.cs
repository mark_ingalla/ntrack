﻿using AttributeRouting;
using Subscription.Website.Infrastructure;
using System.Web.Mvc;

namespace Subscription.Website.Areas.Admin
{
    [RouteArea("Admin")]
    [Authorize(Roles = "Administrator")]
    public abstract class AdminController : BaseController
    {

    }
}