﻿using AutoMapper;
using Subscription.Data;
using Subscription.Website.Areas.Admin.ViewModels.Account;
using System.Linq;

namespace Subscription.Website.Areas.Admin.Profiles
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            Mapper.CreateMap<Account, IndexAccountVM>()
                .AfterMap((x, y) =>
                              {
                                  y.AccountId = x.Id;
                                  y.CompanyName = x.CompanyName;
                                  y.ProductSubscriptionsCount = x.Subscriptions.Count(s => !s.TerminatedOnUTC.HasValue);
                                  y.UsersCount = x.Users.Count(s => !s.IsDeleted);
                              });
        }
    }
}