﻿using System.Web.Mvc;
using System.Web.Routing;
using AttributeRouting;
using Subscription.Website.Authorization;
using nTrack.Core.Utilities;
using Subscription.Website.Services;
//using Raven.Client.Linq;

namespace Subscription.Website.Areas.Admin.Controllers
{
    [RouteArea("Admin")]
    [CheckIfCanManageAccount]
    public class RoleController : Subscription.Website.Controllers.RoleController
    {
        public RoleController(IThreadingUtilities threadingUtilities, IAccountService accountService) : base(threadingUtilities, accountService)
        {
        }

        protected override void Authorize()
        {
            if (!NTrackUser.IsAdministrator)
            {
                Response.Redirect(Url.Action("HaveNoRights", "Account", new { area = "" }), true);
                return;
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
        }

        protected override System.Web.Mvc.ActionResult ReturnToIndexAction(System.Guid accountId)
        {
            return RedirectToAction("Edit", "Account", new {accountId = accountId});
        }

        protected override string ReturnUrl(System.Guid accountId)
        {
            return Url.Action("Edit", "Account", new { accountId = accountId, area = "Admin" });
        }
    }
}
