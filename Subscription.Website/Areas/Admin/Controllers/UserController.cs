﻿using AttributeRouting.Web.Mvc;
using Subscription.Website.Infrastructure;
using nTrack.Core.Messages;
using nTrack.Core.Utilities;
using nTrack.Data.Extenstions;
using nTrack.Data.Indices;
using Subscription.Data;
using Subscription.Messages.Commands;
using Subscription.Messages.Commands.Account.User;
using Subscription.Website.Services;
using Subscription.Website.ViewModels.User;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Subscription.Website.Areas.Admin.Controllers
{
    public class UserController : AdminController
    {
        readonly IThreadingUtilities _threadingUtilities;
        private readonly IAccountService _accountService;

        ActionResult ReturnToIndexAction(Guid accountId)
        {
            return RedirectToAction("Edit", "Account", new { accountId = accountId, area = "Admin" });
        }

        string ReturnUrl(Guid accountId)
        {
            return Url.Action("Edit", "Account", new { accountId = accountId, area = "Admin" });
        }

        public UserController(IThreadingUtilities threadingUtilities, IAccountService accountService)
        {
            _threadingUtilities = threadingUtilities;
            _accountService = accountService;
        }

        [GET("Users/{accountId}")]
        public ActionResult Index(Guid accountId)
        {

            var account = DbSession.Load<Account>(accountId);
            if (account == null)
                return HttpNotFound();

            var model = new UsersVM(account.Users.Where(x => !x.IsDeleted).ToList());

            return View(model);
        }

        [GET("User/Add/{accountId}")]
        public ActionResult Add(Guid accountId)
        {
            var account = DbSession.Load<Account>(accountId);
            if (account == null)
                return HttpNotFound();
            string[] roles = Enum.GetNames(typeof(UserRole));

            return View(new UserVM(roles, accountId) { ReturnUrl = ReturnUrl(account.Id) });
        }

        [POST("User/Add/{accountId}")]
        public ActionResult Add(UserVM vm)
        {
            DateTime sentTime = DateTime.UtcNow;
            var userKey = Guid.NewGuid();


            if (vm.Password == null || vm.Password.Count() < 8)
            {
                ModelState.AddModelError("Password", "Password is required! It has to be minimum 8 char string.");
            }

            if (!IsUniqueEmail(vm.Email, Guid.Empty))
            {
                ModelState.AddModelError("Email", "Email address is already registered!");
            }

            string[] roles = Enum.GetNames(typeof(UserRole));
            vm.Roles = new SelectList(roles);

            if (!ModelState.IsValid)
            {
                return View(vm);
            }

            Bus.Send(CommandsFactory.Subscription.Account.User.AddUser(vm, vm.AccountId, userKey));

            //var cmd = Mapper.Map<UserVM, AddUser>(vm);
            //cmd.Role = (UserRole)Enum.Parse(typeof(UserRole), vm.Role);

            //cmd.UserKey = userKey;
            //cmd.AccountId = vm.AccountId;

            //Bus.Send(cmd);

            bool found;

            found = _threadingUtilities
                .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                 () =>
                                 DbSession.Query<UserIndex.Result, UserIndex>().Any(p => p.UserKey == userKey && p.AccountId == vm.AccountId.ToStringId<Account>()
                                     && p.AccountUpdateOn > sentTime));

            if (found)
            {
                Success(null, "User has been added!");
                return ReturnToIndexAction(vm.AccountId);
            }

            Info("", "The server is taking to long to confirm your request.");

            vm.AllowPost = false;

            return View(vm);
        }

        [GET("User/{accountId}/{userKey}/Edit")]
        public ActionResult Edit(Guid accountId, Guid userKey)
        {
            var user = DbSession.Load<Account>(accountId).Users.Single(p => p.UserKey == userKey);

            string[] roles = Enum.GetNames(typeof(UserRole));

            var vm = new UserVM(roles, accountId, user);
            

         //   Mapper.Map(user, vm);
            vm.ReturnUrl = ReturnUrl(accountId);

            return View(vm);
        }

        [POST("User/{accountId}/{userKey}/Edit")]
        public ActionResult Edit(UserVM vm)
        {
            DateTime sentTime = DateTime.UtcNow;

            if (!IsUniqueEmail(vm.Email, vm.UserKey))
            {
                ModelState.AddModelError("Email", "Email address is already registered!");
            }

            string[] roles = Enum.GetNames(typeof(UserRole));
            vm.Roles = new SelectList(roles);

            if (!ModelState.IsValid)
            {
                return View(vm);
            }

            var user = DbSession.Load<Account>(vm.AccountId).Users.Single(p => p.UserKey == vm.UserKey);

            Bus.Send(CommandsFactory.Subscription.Account.User.UpdateUserDetail(vm.AccountId, vm.Email, vm.FirstName, vm.LastName, vm.Mobile, vm.Phone, vm.UserKey));

            if (user.Role != vm.Role)
            {
                UserRole userRole = (UserRole)Enum.Parse(typeof(UserRole), vm.Role);
                Bus.Send(new ChangeUserRole() { AccountId = vm.AccountId, Role = userRole, UserKey = user.UserKey });
            }

            bool found;

            found = _threadingUtilities
                .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                 () =>
                                      DbSession.Query<UserIndex.Result, UserIndex>().Any(
                                     x =>
                                     x.UserKey == vm.UserKey && x.AccountId == vm.AccountId.ToStringId<Account>() &&
                                     x.AccountUpdateOn > sentTime));

            if (found)
            {
                Success("", "User has been updated!");
                return ReturnToIndexAction(vm.AccountId);
            }

            Info("", "The server is taking to long to confirm your request.");

            vm.AllowPost = false;
            return View(vm);
        }

        [GET("User/{accountId}/{userKey}/Lock")]
        public ActionResult Lock(Guid accountId, Guid userKey)
        {
            var vm = GetLockingVM(accountId, userKey, true);
            if (vm == null)
                return HttpNotFound();
            return View("Locking", vm);
        }

        [GET("User/{accountId}/{userKey}/Unlock")]
        public ActionResult Unlock(Guid accountId, Guid userKey)
        {
            var vm = GetLockingVM(accountId, userKey, false);
            if (vm == null)
                return HttpNotFound();

            return View("Locking", vm);
        }

        [POST("User/{accountId}/{userKey}/Lock")]
        public ActionResult Lock(UserLockingVM vm)
        {
            DateTime sentTime = DateTime.UtcNow;

            if (!ModelState.IsValid)
            {
                return View("Locking", vm);
            }


            Bus.Send(new LockUser()
            {
                AccountId = vm.AccountId,
                Reason = vm.Reason,
                UserKey = vm.UserKey
            });

            bool found;

            found = _threadingUtilities
                .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                 () =>
                                 DbSession.Query<UserIndex.Result, UserIndex>().Any(
                                     x =>
                                     x.UserKey == vm.UserKey && x.IsLocked && x.AccountId == vm.AccountId.ToStringId<Account>() &&
                                     x.AccountUpdateOn > sentTime));

            if (found)
            {
                Success("", "User has been locked!");
                return ReturnToIndexAction(vm.AccountId);
            }

            Info("", "The server is taking to long to confirm your request.");

            vm.AllowPost = false;

            return View("Locking", vm);
        }

        [POST("User/{accountId}/{userKey}/Unlock")]
        public ActionResult Unlock(UserLockingVM vm)
        {
            DateTime sentTime = DateTime.UtcNow;

            if (!ModelState.IsValid)
            {
                return View("Locking", vm);
            }



            Bus.Send(new UnlockUser()
            {
                AccountId = vm.AccountId,
                Reason = vm.Reason,
                UserKey = vm.UserKey
            });


            bool found;

            found = _threadingUtilities
                .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                 () =>
                                     DbSession.Query<UserIndex.Result, UserIndex>().Any(
                                     x =>
                                     x.UserKey == vm.UserKey && !x.IsLocked && x.AccountId == vm.AccountId.ToStringId<Account>() &&
                                     x.AccountUpdateOn > sentTime));

            if (found)
            {
                Success("", "User has been unlocked!");
                return ReturnToIndexAction(vm.AccountId);
            }

            Info("", "The server is taking to long to confirm your request.");

            vm.AllowPost = false;

            return View("Locking", vm);
        }

        [GET("User/{accountId}/{userKey}/RemoveUser")]
        public ActionResult RemoveUser(Guid accountId, Guid userKey)
        {
            var vm = GetRemovingVM(accountId, userKey);
            if (vm == null)
                return HttpNotFound();
            return View("Removing", vm);
        }

        [POST("User/{accountId}/{userKey}/RemoveUser")]
        public ActionResult RemoveUser(UserRemovingVM vm)
        {
            DateTime sentTime = DateTime.UtcNow;
            Guid accountIdGuid = Guid.Empty;
            var databaseUpdated = false;

            if (ModelState.IsValid)
            {
                accountIdGuid = vm.AccountId;

                    Bus.Send(new RemoveUser()
                    {
                        AccountId = accountIdGuid,
                        UserKey = vm.UserKey
                    });

                    databaseUpdated = _threadingUtilities
                        .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                         () =>
                                         DbSession.Query<UserIndex.Result, UserIndex>().Any(
                                             x =>
                                             x.UserKey == vm.UserKey && x.IsLocked && x.AccountId == vm.AccountId.ToStringId<Account>() &&
                                             x.AccountUpdateOn > sentTime));
                
            }

            if (databaseUpdated)
            {
                Success("", "User has been removed!");
                return ReturnToIndexAction(accountIdGuid);
            }

            Info("", "The server is taking to long to confirm your request.");
            vm.AllowPost = false;

            return View("Removing", vm);
        }

        [GET("User/{accountId}/{userKey}/ResetPassword")]
        public ActionResult ResetPassword(Guid accountId, Guid userKey)
        {
            var user = DbSession.Load<Account>(accountId).Users.Single(p => p.UserKey == userKey);

            var vm = new UserResetPasswordVM()
            {
                UserKey = userKey,
                UserFullName = user.FullName,
                AccountId = accountId
            };
            vm.ReturnUrl = ReturnUrl(accountId);

            return View(vm);
        }

        [POST("User/{accountId}/{userKey}/ResetPassword")]
        public ActionResult ResetPasswordPost(UserResetPasswordVM vm)
        {
            var user = DbSession.Load<Account>(vm.AccountId).Users.Single(p => p.UserKey == vm.UserKey);


            if (ModelState.IsValid)
            {

                var token = Guid.NewGuid();

                Bus.Send(new PrepareResetPassword()

                {
                    AccountId = vm.AccountId,
                    UserEmail = user.Email,
                    UserKey = user.UserKey,
                    ResetPasswordToken = token,
                    ResetPasswordLink = Url.Action("ResetPasswordVerification", "Account", new { token = token, area = "" }, "http"),
                    UserFullname = user.FullName
                });

                //#if DEBUG
                //                ViewBag.ResetPasswordLink = Url.Action("ResetPasswordVerification", "Account", new { token = token, area = "" }, "http");
                //#endif
                return View("ResetPasswordSuccess");

            }

            return View("ResetPassword", vm);
        }

        bool IsUniqueEmail(string email, Guid ownerKey)
        {
            if (string.IsNullOrEmpty(email)) return false;
            var exists = DbSession.Query<UserIndex.Result, UserIndex>().Any(c => c.Email == email && c.UserKey != ownerKey);
            return !exists;
        }
        UserLockingVM GetLockingVM(Guid accountId, Guid userKey, bool locking)
        {
            var user = DbSession.Load<Account>(accountId).Users.Single(p => p.UserKey == userKey);

            var vm = new UserLockingVM()
            {
                AccountId = accountId,
                UserKey = user.UserKey,
                FullName = user.FullName,
                Locking = locking,
                ReturnUrl = ReturnUrl(accountId)
            };

            return vm;
        }

        UserRemovingVM GetRemovingVM(Guid accountId, Guid userKey)
        {
            var user = DbSession.Load<Account>(accountId).Users.Single(p => p.UserKey == userKey);

            var vm = new UserRemovingVM()
            {
                UserKey = user.UserKey,
                FullName = user.FullName,
                ReturnUrl = ReturnUrl(accountId),
                AccountId = accountId         
            };

            return vm;
        }

    }
}
