﻿using AttributeRouting.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Raven.Client;
using Subscription.Website.Areas.Admin.ViewModels.Product;
using Subscription.Website.Services;
using Subscription.Website.ViewModels.Image;
using nTrack.Core.Enums;
using nTrack.Core.Utilities;
using Subscription.Data;
using Subscription.Messages.Commands.Product;
using Subscription.Website.Areas.Admin.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Subscription.Website.Areas.Admin.Controllers
{
    public class ProductController : AdminController
    {
        readonly IThreadingUtilities _threadingUtilities;
        private readonly IAttachmentService _attachmentService;


        public const string ModuleName = "Product";
        public ProductController(IThreadingUtilities threadingUtilities, IAttachmentService attachmentService)
        {
            _threadingUtilities = threadingUtilities;
            _attachmentService = attachmentService;
        }

        #region Product CRUD

        [GET("Products")]
        public ActionResult Index()
        {
            var products = DbSession.Query<Product>();
            Session[ModuleName + "ActiveTabId"] = null;
            Session[ModuleName + "ActiveTabPaneId"] = null;
            return View(new ProductsViewModel(products.ToList()));
        }

        [GET("Product/Create")]
        public ActionResult Create()
        {
            return View(new ProductViewModel { AllowPost = true });
        }

        [POST("Product/Create")]
        public ActionResult Create(ProductViewModel vm, IEnumerable<HttpPostedFileBase> files)
        {
            var sentTime = DateTime.UtcNow;
            var productId = Guid.NewGuid();

            Bus.Send(new CreateProduct
            {
                ProductId = productId,
                Name = vm.Name,
                Code = vm.Code,
                Description = vm.Description,
                BasePrice = vm.BasePrice,
                AccessUrl = vm.AccessUrl
            });

            if (vm.IsActive)
                Bus.Send(new ActivateProduct { ProductId = productId });

            var found = _threadingUtilities
                .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                 () => DbSession.Query<Product>().Any(x => x.UpdatedOn > sentTime && x.Id == productId));
            if (found)
            {
                Success("", "Product has been created!");
                return RedirectToAction("Edit", new { id = productId });
            }

            Info("", "The server is taking to long to confirm your request.");
            vm.AllowPost = false;
            return View(vm);
        }

        [GET("Product/{id}")]
        public ActionResult Edit(Guid id)
        {
            var product = DbSession.Load<Product>(id);
         
            Raven.Abstractions.Data.Attachment attachment = _attachmentService.GetAttachment(product.Id,AttachmentEntities.ProductLogos.ToString());

            var vm = new ProductEditViewModel(product, attachment != null);
            ViewBag.Module = ModuleName;

            return View(vm);
        }

        [POST("Product/{id}")]
        public ActionResult Edit(ProductEditViewModel vm, IEnumerable<HttpPostedFileBase> files)
        {
            var sentTime = DateTime.UtcNow;

            var product = DbSession.Load<Product>(vm.ProductId);
            if (product == null)
            {
                return HttpNotFound();
            }

            if (product.IsActive && !vm.IsActive)
            {
                Bus.Send(new DiscontinueProduct { ProductId = vm.ProductId, Reason = vm.DiscontinueReason });
            }

            if (!product.IsActive && vm.IsActive)
            {
                Bus.Send(new ActivateProduct { ProductId = vm.ProductId });
            }

            if (ModelState.IsValid)
            {
                Bus.Send(new UpdateProduct
                {
                    ProductId = vm.ProductId,
                    Name = vm.Name,
                    Code = vm.Code,
                    Description = vm.Description,
                    BasePrice = vm.BasePrice,
                    AccessUrl = vm.AccessUrl
                });
            }

            var found = _threadingUtilities
               .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                () =>
                                DbSession.Query<Product>().Any(x => x.UpdatedOn > sentTime && x.Id == product.Id));
            if (found)
            {
                Success("", "Product has been updated!");
                return RedirectToAction("Edit", new { id = product.Id });
            }

            Info("", "The server is taking to long to confirm your request.");
            vm.AllowPost = false;
            return View(vm);
        }

        [POST("Product/{id}/Discontinue")]
        public void Discontinue(Guid id)
        {
            Bus.Send(new DiscontinueProduct { ProductId = id });
        }

        #endregion

        #region Offer CRUD

        [GET("Product/{productId}/CreateOffer")]
        public ActionResult CreateOffer(Guid productId)
        {
            var product = DbSession.Load<Product>(productId);

            if (product == null)
                return HttpNotFound();

            var vm = new OfferViewModel(product, null);
            return View(vm);
        }

        [POST("Product/{productId}/CreateOffer")]
        public ActionResult CreateOffer(OfferViewModel vm, Guid productId)
        {
            var sentTime = DateTime.UtcNow;
            var offerKey = Guid.NewGuid();

            Bus.Send(new AddOffer
            {
                ProductId = productId,
                Name = vm.Name,
                OfferKey = offerKey
            });

            if (vm.IsActive)
                Bus.Send(new ActivateOffer() {ProductId = productId, OfferKey = offerKey});

            Bus.Send(new UpdateOfferFeatures()
            {
                Features = vm.SelectedFeatures.Where(x => x.IsIncluded).Select(x => new Subscription.Messages.Commands.Product.OfferFeature
                                                                                          {
                                                                                              Code = x.Code,IsIncluded = x.IsIncluded,Limit = x.Limit
                                                                                          }).ToList(), OfferKey = offerKey, ProductId = productId });

            
            var found = _threadingUtilities
                .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                 () => DbSession.Query<Product>().Any(x => x.UpdatedOn > sentTime && x.Id == productId));

            if (found)
            {
                Success("", "Offer has been created!");
            }
            else
            {
                Info("", "The server is taking to long to confirm your request.");
            }

            return RedirectToAction("Edit", new { id = productId });
        }

        [GET("Product/{productId}/EditOffer/{offerKey}")]
        public ActionResult EditOffer(Guid productId, Guid offerKey)
        {
            var product = DbSession.Load<Product>(productId);

            if (product == null)
                return HttpNotFound();

            var offer = product.Offers.SingleOrDefault(p => p.Key == offerKey);

            if (offer == null)
                return HttpNotFound();

            var vm = new OfferViewModel(product, offer);

            return View(vm);
        }

        [POST("Product/{productId}/EditOffer/{offerKey}")]
        public ActionResult EditOffer(OfferViewModel vm)
        {
            DateTime sentTime = DateTime.UtcNow;

            var product = DbSession.Load<Product>(vm.ProductId);

            if (product == null)
                return HttpNotFound();

            var offer = product.Offers.SingleOrDefault(p => p.Key == vm.OfferKey);

            if (offer == null)
                return HttpNotFound();

            if (offer.Name != vm.Name)
                Bus.Send(new UpdateOffer()
                {
                    Name = vm.Name,
                    OfferKey = vm.OfferKey,
                    ProductId = vm.ProductId
                });


            if (vm.IsActive && !offer.IsActive)
                Bus.Send(new ActivateOffer() { ProductId = vm.ProductId, OfferKey = vm.OfferKey });
            else if (!vm.IsActive && offer.IsActive)
            {
                Bus.Send(new DeactivateOffer
                {
                    ProductId = vm.ProductId,
                    OfferKey = vm.OfferKey
                });
            }


            Bus.Send(new UpdateOfferFeatures()
                         {
                             Features = vm.SelectedFeatures.Where(p => p.IsIncluded).Select(p => new OfferFeature
                                                                                                     {
                                                                                                         Code = p.Code,
                                                                                                         Limit = p.Limit,
                                                                                                     }).ToList(),
                             OfferKey = vm.OfferKey,
                             ProductId = vm.ProductId
                         });

            var found = _threadingUtilities
                .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                 () => DbSession.Query<Product>().Any(x => x.UpdatedOn > sentTime && x.Id == vm.ProductId));
            if (found)
            {
                Success("", "Offer has been updated!");
                return RedirectToAction("Edit", new { id = vm.ProductId });
            }

            Info("", "The server is taking to long to confirm your request.");
            vm = new OfferViewModel(product, offer);
            vm.AllowPost = false;

            return View("EditOffer", vm);
        }

        [GET("Product/{productId}/RemoveOffer/{offerKey}")]
        public ActionResult RemoveOffer(Guid productId, Guid offerKey)
        {
            var product = DbSession.Load<Product>(productId);

            if (product == null)
                return HttpNotFound();

            var offer = product.Offers.SingleOrDefault(p => p.Key == offerKey);

            if (offer == null)
                return HttpNotFound();


            var vm = new RemoveOfferViewModel(product, offer);

            return View(vm);
        }

        [POST("Product/{productId}/RemoveOffer/{offerKey}")]
        public ActionResult RemoveOffer(RemoveOfferViewModel viewModel)
        {
            DateTime sentTimeUTC = DateTime.UtcNow;

            var product = DbSession.Load<Product>(viewModel.ProductId);

            if (product == null)
                return HttpNotFound();

            var offer = product.Offers.SingleOrDefault(p => p.Key == viewModel.OfferKey);

            if (offer == null)
                return HttpNotFound();

            if (offer.IsLocked)
            {
                Failure("", "Offer is locked! Can't be removed.");
                return RedirectToAction("Edit", new { id = viewModel.ProductId });
            }

            Bus.Send(new RemoveOffer()
                         {
                             OfferKey = viewModel.OfferKey,
                             ProductId = viewModel.ProductId,
                             Reason = viewModel.Reason
                         });

            var found = _threadingUtilities
               .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                () => DbSession.Query<Product>().Any(x => x.UpdatedOn > sentTimeUTC && x.Id == viewModel.ProductId));

            if (found)
            {
                Success("", "Offer has been removed!");

                return RedirectToAction("Edit", new { id = viewModel.ProductId });
            }

            Info("", "The server is taking to long to confirm your request.");
            viewModel.AllowPost = false;

            return View("RemoveOffer", viewModel);
        }

        #endregion

        #region Feature CRUD

        [GET("Product/{productId}/CreateFeature")]
        public ActionResult CreateFeature(Guid productId)
        {
            var product = DbSession.Load<Product>(productId);

            if (product == null)
                return HttpNotFound();

            var vm = new FeatureViewModel(product, null);

            return View(vm);
        }

        [POST("Product/{productId}/CreateFeature")]
        public ActionResult CreateFeature(Guid productId, FeatureViewModel feature)
        {
            var product = DbSession.Load<Product>(productId);
            
            if (product.Features.SingleOrDefault(p => p.Code == feature.Code) != null)
            {
                ModelState.AddModelError("feature", "Feature with typed code exists. ");
            }
            if (ModelState.IsValid)
            {
                Bus.Send(new AddFeature()
                             {
                                 Code = feature.Code,
                                 Name = feature.Name,
                                 ProductId = productId,
                                 Price = feature.Price,
                                 Limit = feature.Limit
                             });

                if (feature.IsActive)
                    Bus.Send(new ActivateFeature()
                                 {
                                     FeatureCode = feature.Code,
                                     ProductId = productId
                                 });
            }

            //The model is invalid - render the current view to show any validation errors
            return RedirectToAction("Edit", "Product", new {id = productId});
        }

        [GET("Product/{productId}/EditFeature")]
        public ActionResult EditFeature(Guid productId, string featureCode)
        {
            var product = DbSession.Load<Product>(productId);

            if (product == null)
                return HttpNotFound();

            var feature = product.Features.SingleOrDefault(p => p.Code == featureCode);

            if (feature == null)
                return HttpNotFound();

            var vm = new FeatureViewModel(product, feature);

            return View(vm);
        }

        [POST("Product/{productId}/EditFeature")]
        public ActionResult EditFeature(FeatureViewModel feature, Guid productId)
        {
            var product = DbSession.Load<Product>(productId);
            var currentFeature = product.Features.SingleOrDefault(p => p.Code == feature.Code);
            if (currentFeature == null)
            {
                ModelState.AddModelError("feature", "Feature with typed code do not exists. ");
            }
            if (ModelState.IsValid)
            {
                Bus.Send(new UpdateFeature
                {
                    Code = feature.Code,
                    Name = feature.Name,
                    ProductId = productId,
                    Price = feature.Price,
                    Limit = feature.Limit
                });

                if (feature.IsActive && !currentFeature.IsActive)
                {
                    Bus.Send(new ActivateFeature
                                 {
                                     FeatureCode = feature.Code,
                                     ProductId = productId
                                 });
                }
                else if (!feature.IsActive && currentFeature.IsActive)
                {
                    Bus.Send(new DeactivateFeature
                                 {
                                     FeatureCode = feature.Code,
                                     ProductId = productId
                                 });
                }
            }
            //The model is invalid - render the current view to show any validation errors

            return RedirectToAction("Edit", new { id = productId });
        }

        [GET("Product/{productId}/DeleteFeature")]
        public ActionResult DeleteFeature(Guid productId, string featureCode)
        {
            var product = DbSession.Load<Product>(productId);

            if (product == null)
                return HttpNotFound();

            var feature = product.Features.SingleOrDefault(p => p.Code == featureCode);

            if (feature == null)
                return HttpNotFound();


            var vm = new RemoveFeatureViewModel(product, feature);

            return View(vm);
        }

        [POST("Product/{productId}/DeleteFeature")]
        public ActionResult DeleteFeature(FeatureViewModel feature)
        {
            var sentTime = DateTime.UtcNow;

            var product = DbSession.Load<Product>(feature.ProductId);

            if (product.Features.SingleOrDefault(p => p.Code == feature.Code) == null)
            {
                ModelState.AddModelError("feature", "Feature with typed code do not exists. ");
            }

            if (feature.Code != null)
            {
                Bus.Send(new RemoveFeature()
                {
                    Code = feature.Code,
                    ProductId = feature.ProductId
                });
            }

            _threadingUtilities.WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                 () => DbSession.Query<Product>().Any(x => x.UpdatedOn > sentTime && x.Id == feature.ProductId));

            //The model is invalid - render the current view to show any validation errors
            return RedirectToAction("Edit", new { id = feature.ProductId });
        }

        #endregion
    }
}
