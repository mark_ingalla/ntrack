﻿using AttributeRouting.Web.Mvc;
using nTrack.Core.Utilities;
using nTrack.Data.Extenstions;
using Subscription.Data;
using Subscription.Data.Indices;
using Subscription.Messages.Commands.Account;
using Subscription.Website.ViewModels.Subscription;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Subscription.Website.Areas.Admin.Controllers
{
    public class SubscriptionController : AdminController
    {
        readonly IThreadingUtilities _threadingUtilities;

        public SubscriptionController(IThreadingUtilities threadingUtilities)
        {
            _threadingUtilities = threadingUtilities;
        }


        [GET("Subscriptions")]
        public ActionResult Index(Guid accountId)
        {
            var subscriptions = DbSession.Query<SubscriptionIndex.Result, SubscriptionIndex>()
                                         .Where(x =>
                                                x.AccountId == accountId.ToStringId<Account>() &&
                                                !x.TerminatedOnUTC.HasValue)
                                         .ToList();

            var indexVM = new SubscriptionIndexVM(subscriptions);

            return View(indexVM);
        }

        [GET("Subscription/View/{accountId}")]
        public ActionResult View(Guid accountId, Guid productId, Guid offerKey)
        {
            var subscriptionCreateVM = CreateSubscriptionViewVM(accountId, productId, offerKey);
            if (subscriptionCreateVM == null)
                return HttpNotFound();

            return View(subscriptionCreateVM);
        }

        [POST("Subscription/Cancel/{accountId}")]
        public ActionResult Cancel(Guid accountId, Guid productId, Guid offerKey)
        {
            var sentTime = DateTime.UtcNow;

            var account = DbSession.Load<Account>(accountId);
            if (account == null)
                return HttpNotFound();


            bool found;

            var accountIdString = account.IdString;

            var subscription =
                account.Subscriptions.Single(p => p.ProductId == productId.ToStringId<Product>() && p.TerminatedOnUTC == null);

            Bus.Send(new TerminateSubscription { AccountId = account.Id, SubscriptionKey = subscription.Key });
            found = _threadingUtilities
                .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                 () =>
                                 DbSession.Query<SubscriptionIndex.Result, SubscriptionIndex>().Any(
                                     p =>
                                     p.AccountId == accountIdString && p.ProductId == productId.ToStringId<Product>() &&
                                     p.TerminatedOnUTC > sentTime));

            if (found)
            {
                Success("", "Subscription has been cancelled!");
                return RedirectToAction("Edit", "Account", new { accountId = account.Id, area = "Admin" });
            }

            var subscriptionEditVM = CreateSubscriptionViewVM(account.Id, productId, offerKey);

            if (subscriptionEditVM == null)
                return HttpNotFound();

            Info("", "The server is taking to long to confirm your request.");
            subscriptionEditVM.AllowPost = false;
            return View("View", subscriptionEditVM);
        }

        [POST("Subscription/Create/{accountId}")]
        public ActionResult Create(Guid accountId, SubscriptionCreateVM subscription)
        {
            var sentTime = DateTime.UtcNow;


            var createCommand = new CreateSubscription()
            {
                AccountId = accountId,
                OfferKey = subscription.OfferKey,
                ProductId = subscription.ProductId,
                SubscriptionKey = Guid.NewGuid()
            };

            Bus.Send(createCommand);

            //NOTE: why the logic of raising an invoice is here?
            //var invoiceCommand = new RaiseInvoice()
            //{
            //    AccountId = accountId,
            //    InvoiceId = Guid.NewGuid()
            //};

            //Bus.Send(invoiceCommand);


            var accountIdString = accountId.ToStringId<Account>();

            bool found;

            found = _threadingUtilities
                .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                 () =>
                                 DbSession.Query<SubscriptionIndex.Result, SubscriptionIndex>().Any(
                                     p =>
                                     p.AccountId == accountIdString && p.ProductId == subscription.ProductId.ToStringId<Product>() &&
                                     p.SubscribedOnUTC > sentTime));

            if (found)
            {
                Success("", "Product has been subscribed!");
                return RedirectToAction("Edit", "Account", new { accountId = accountId, area = "Admin" });
            }

            Info("", "The server is taking to long to confirm your request.");

            var createVM = CreateSubscriptionViewVM((Guid)accountId, subscription.ProductId, subscription.OfferKey);

            createVM.AllowPost = false;

            return View("View", createVM);
        }

        SubscriptionCreateVM CreateSubscriptionViewVM(Guid accountId, Guid productId, Guid offerKey)
        {
            var account = DbSession.Load<Account>(accountId);
            if (account == null)
                return null;

            var productIdString = productId.ToStringId<Product>();

            var offer =
                DbSession.Query<ProductOfferIndex.Result, ProductOfferIndex>().SingleOrDefault(
                    p => p.OfferKey == offerKey && p.ProductId == productIdString && p.IsProductActive && p.IsOfferActive);

            if (offer == null)
                return null;

            var subscriptionCreateVM = new SubscriptionCreateVM(offer, account.Subscriptions
                                                                           .Any(
                                                                               p =>
                                                                               p.OfferKey == offerKey &&
                                                                               p.ProductId ==
                                                                               productId.ToStringId<Product>() &&
                                                                               p.TerminatedOnUTC == null));

            subscriptionCreateVM.ReturnUrl = Url.Action("Edit", "Account", new { accountId = account.Id, area = "Admin" });


            subscriptionCreateVM.AccountId = accountId;

            return subscriptionCreateVM;
        }




    }
}
