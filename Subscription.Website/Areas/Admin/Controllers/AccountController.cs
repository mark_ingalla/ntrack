﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using Subscription.Website.Services;
using Subscription.Website.ViewModels.Image;
using nTrack.Core.Enums;
using nTrack.Core.Utilities;
using nTrack.Data;
using nTrack.Data.Extenstions;
using Subscription.Data.Indices;
using Subscription.Messages.Commands.Account;
using Subscription.Website.Areas.Admin.ViewModels.Account;
using Subscription.Website.ViewModels.Account;
using Subscription.Website.ViewModels.Subscription;
using Account = Subscription.Data.Account;


namespace Subscription.Website.Areas.Admin.Controllers
{
    public class AccountController : AdminController
    {
        public const string ModuleName = "Account";

        private readonly IThreadingUtilities _threadingUtilities;
        private readonly IAttachmentService _attachmentService;

        public AccountController(IThreadingUtilities threadingUtilities, IAttachmentService attachmentService)
        {
            _threadingUtilities = threadingUtilities;
            _attachmentService = attachmentService;
        }

        [GET("Accounts")]
        public ActionResult Index()
        {
            var accounts = DbSession.Query<Account>().ToList();



            var indexVM = new IndexVM(accounts);

            Session[ModuleName + "ActiveTabId"] = null;
            Session[ModuleName + "ActiveTabPaneId"] = null;

            return View(indexVM);
        }

        [GET("Account/{accountId}")]
        public ActionResult Edit(Guid accountId)
        {
            var account = DbSession.Load<Account>(accountId);
            if (account == null)
                return HttpNotFound();

            var users = account.Users.Where(x => !x.IsDeleted).ToList();

            var countries = DbSession.Load<Countries>(Countries.EntityId);

            var accountVM = new AccountVM(countries != null ? countries.CountryNames : new string[0], account);


            var subscriptions = DbSession.Query<SubscriptionIndex.Result, SubscriptionIndex>()
                                         .Where(x =>
                                                x.AccountId == accountId.ToStringId<Account>() &&
                                                x.TerminatedOnUTC == null)
                                         .ToList();


            var indexVM = new SubscriptionIndexVM(subscriptions);

            var editVM = new EditVM
                             {
                                 AccountId = account.Id,
                                 Users = users,
                                 AccountInfo = accountVM,
                                 SubscriptionsInfo = indexVM
                             };

            Raven.Abstractions.Data.Attachment attachment = _attachmentService.GetAttachment(account.Id,AttachmentEntities.AccountLogos.ToString());

            editVM.ImageVM = new ImageVM()
                                 {
                                     Avaible = attachment != null,
                                     Entity = AttachmentEntities.AccountLogos.ToString(),
                                     Id = account.Id
                                 };

            ViewBag.Module = ModuleName;

            return View(editVM);
        }


        [Authorize]
        [POST("Account/{accountId}")]
        public ActionResult Edit(AccountVM vm, IEnumerable<HttpPostedFileBase> files)
        {
            DateTime sentTime = DateTime.UtcNow;

            var account = DbSession.Load<Account>(vm.AccountId);

            if (account == null)
                return HttpNotFound();

            if (ModelState.IsValid)
            {
                Bus.Send(new UpdateAccount
                {
                    AccountId = account.Id,
                    AddressLine1 = vm.AddressLine1,
                    AddressLine2 = vm.AddressLine2,
                    CompanyName = vm.CompanyName,
                    Country = vm.Country,
                    CompanyPhone = vm.Phone,
                    Postcode = vm.Postcode,
                    State = vm.State,
                    Suburb = vm.Suburb,
                });
            }

            bool found = _threadingUtilities
               .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                () =>
                                DbSession.Query<Account>().Any(x => x.Id == account.Id && x.UpdatedOn > sentTime));

            if (found)
            {
                Success("", "Account data has been changed!");
                return RedirectToAction("Edit");
            }

            Info("", "The server is taking to long to confirm your request.");

            vm.AllowPost = false;
            var countries = DbSession.Load<Countries>(Countries.EntityId);
            vm.CountryOptions = countries.CountryNames
                .Select(x => new SelectListItem { Text = x, Value = x });

            var subscriptions = DbSession.Query<SubscriptionIndex.Result, SubscriptionIndex>()
                                         .Where(x =>
                                                x.AccountId == account.IdString &&
                                                x.TerminatedOnUTC == null)
                                         .ToList();

            var users = account.Users.Where(x => !x.IsDeleted).ToList();

            var indexVM = new SubscriptionIndexVM(subscriptions);

            var newVM = new EditVM()
                            {
                                AccountId = account.Id,
                                AccountInfo = vm,
                                SubscriptionsInfo = indexVM,
                                Users = users
                            };

            return View(newVM);
        }

        [GET("Account/{accountId}/Activate")]
        public ActionResult Activate(Guid accountId)
        {
            if (accountId != ContextAccountId)
            {
                var vm = GetActivationVM(accountId, true);
                if (vm == null)
                    return HttpNotFound();

                return View("Activation", vm);
            }

            return RedirectToAction("Index");
        }

        [POST("Account/{accountId}/Activate")]
        public ActionResult Activate(AccountActivationVM vm)
        {
            if (vm.AccountId == Guid.Empty)
            {
                Failure(string.Empty, "Couldn't activate account. Database error.");
            }
            else
            {
                Bus.Send<ActivateAccount>(
                    cmd => { cmd.AccountId = vm.AccountId; });

                Success(string.Empty, "Account activated successfully");
            }

            return RedirectToAction("Index");
        }

        [GET("Account/{accountId}/Deactivate")]
        public ActionResult Deactivate(Guid accountId)
        {
            if (accountId == ContextAccountId)
            {
                return RedirectToAction("Index");
            }

            var vm = GetActivationVM(accountId, false);
            if (vm == null)
                return HttpNotFound();

            return View("Activation", vm);
        }

        [POST("Account/{accountId}/Deactivate")]
        public ActionResult Deactivate(AccountActivationVM vm)
        {
            if (vm.AccountId == Guid.Empty)
            {
                Failure(string.Empty, "Couldn't deactivate account. Database error.");
            }
            else
            {
                Bus.Send<DeactivateAccount>(
                    cmd =>
                    {
                        cmd.AccountId = vm.AccountId;
                        cmd.Reason = vm.Reason;
                    });

                Success(string.Empty, "Account deactivated successfully");
            }

            return RedirectToAction("Index");
        }

        AccountActivationVM GetActivationVM(Guid accountId, bool activating)
        {
            var accountModel = DbSession.Load<Account>(accountId);

            if (accountModel == null)
                return null;

            var vm = new AccountActivationVM()
            {
                CompanyName = accountModel.CompanyName,
                AccountId = accountModel.Id,
                Activating = activating,
                ReturnUrl = Url.Action("Index")
            };

            return vm;
        }
    }
}
