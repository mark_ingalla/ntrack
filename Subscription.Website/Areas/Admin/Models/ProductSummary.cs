﻿
using System;
using System.Linq;

namespace Subscription.Website.Areas.Admin.Models
{
    public class ProductSummary
    {
        public Guid ProductId { get; set; }
        public string Name { get; set; }
        public int FeaturesCount { get; set; }
        public int OffersCount { get; set; }
        public bool IsActive { get; set; }
        public decimal TotalPrice { get; set; }

        public ProductSummary()
        {}

        public ProductSummary(Data.Product product)
        {
            ProductId = product.Id;
            Name = product.Name;
            FeaturesCount = product.Features.Count;
            OffersCount = product.Offers.Count;
            IsActive = product.IsActive;
            TotalPrice = product.Features.Sum(p => p.Price) + product.BasePrice;
        }
    }
}