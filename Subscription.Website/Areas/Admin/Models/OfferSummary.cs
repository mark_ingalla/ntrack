﻿
using System;

namespace Subscription.Website.Areas.Admin.Models
{
    public class OfferSummary
    {
        public Guid ProductId { get; set; }
        public Guid ProductName { get; set; }
        public Guid OfferKey { get; set; }
        public string Name { get; set; }
        public int FeaturesCount { get; set; }
        public bool IsActive { get; set; }
        public decimal TotalPrice { get; set; }
    }
}