﻿using System;
using System.ComponentModel.DataAnnotations;
using Subscription.Website.ViewModels;

namespace Subscription.Website.Areas.Admin.ViewModels.Account
{
    public class AccountActivationVM : ReturnUrlVM
    {
        public Guid AccountId { get; set; }

        [Required]
        public string Reason { get; set; }

        public bool AllowPost { get; set; }

        public string CompanyName { get; set; }

        public bool Activating { get; set; }

        public AccountActivationVM()
        {
            AllowPost = true;
        }
    }
}