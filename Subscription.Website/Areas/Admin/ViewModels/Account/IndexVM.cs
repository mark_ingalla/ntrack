﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Subscription.Website.Areas.Admin.ViewModels.Account
{
    public class IndexVM
    {
        public List<IndexAccountVM> Accounts { get; set; }
 
        public IndexVM()
        {}

        public IndexVM(List<Data.Account> accounts)
        {
            Accounts = accounts.Select(p => new IndexAccountVM(p)).ToList();
        }

    }
}