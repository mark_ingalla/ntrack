﻿using System;
using System.Linq;

namespace Subscription.Website.Areas.Admin.ViewModels.Account
{
    public class IndexAccountVM
    {
        public Guid AccountId { get; set; }
        public string CompanyName { get; set; }
        public bool IsActive { get; set; }
        public int UsersCount { get; set; }
        public int ProductSubscriptionsCount { get; set; }

        public IndexAccountVM()
        {}

        public IndexAccountVM(Data.Account account)
        {
            AccountId = account.Id;
            CompanyName = account.CompanyName;
            IsActive = account.IsActive;
            UsersCount = account.Users.Count;
            ProductSubscriptionsCount = account.Subscriptions.Count(s => !s.TerminatedOnUTC.HasValue);
        }
    }
}