﻿using System;
using System.Collections.Generic;
using Subscription.Website.ViewModels.Image;
using nTrack.Data;

namespace Subscription.Website.Areas.Admin.ViewModels.Account
{
    public class EditVM
    {
        public Guid AccountId { get; set; }
        public List<User> Users { get; set; }
        public Website.ViewModels.Account.AccountVM AccountInfo { get; set; }
        public Website.ViewModels.Subscription.SubscriptionIndexVM SubscriptionsInfo { get; set; }
        public ImageVM ImageVM { get; set; }

    }
}