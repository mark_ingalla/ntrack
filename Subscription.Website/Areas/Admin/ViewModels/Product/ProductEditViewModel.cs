using System.Linq;
using Subscription.Data;
using System;
using Subscription.Website.ViewModels.Image;
using nTrack.Core.Enums;

namespace Subscription.Website.Areas.Admin.ViewModels.Product
{
    public class ProductEditViewModel : ProductViewModel
    {
        public Guid ProductId { get; set; }
        public ProductFeature[] Features { get; set; }
        public ProductOffer[] Offers { get; set; }
        public string LargeImageUrl { get; set; }
        public string DiscontinueReason { get; set; }
        public ImageVM ImageVM { get; set; }

        public ProductEditViewModel()
        {}

        public ProductEditViewModel(Data.Product product, bool avaibleAttachment) : base(product)
        {
            ProductId = product.Id;
            Features = product.Features.ToArray();
            Offers = product.Offers.ToArray();
            LargeImageUrl = product.LargeImageUrl;
            DiscontinueReason = product.DiscontinueReason;
            ImageVM = new ImageVM()
            {
                Avaible = avaibleAttachment,
                Entity = AttachmentEntities.ProductLogos.ToString(),
                Id = product.Id
            };
            
        }
    }
}