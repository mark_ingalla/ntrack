﻿using Subscription.Data;
using System;

namespace Subscription.Website.Areas.Admin.ViewModels.Product
{
    public class RemoveFeatureViewModel
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }

        //feature properties
        public string Name { get; set; }
        public string Code { get; set; }
        public string Reason { get; set; }
        public bool AllowPost { get; set; }

        public RemoveFeatureViewModel()
        {
            AllowPost = true;
        }

        public RemoveFeatureViewModel(Data.Product product, ProductFeature feature)
            : this()
        {
            ProductId = product.Id;
            ProductName = product.Name;

            Name = feature.Name;
            Code = feature.Code;
        }
    }
}