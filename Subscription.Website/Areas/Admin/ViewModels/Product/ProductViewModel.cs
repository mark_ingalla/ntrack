using System.ComponentModel.DataAnnotations;

namespace Subscription.Website.Areas.Admin.ViewModels.Product
{
    public class ProductViewModel
    {
        [Required]
        [Display(Name = "Product Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Product Code")]
        public string Code { get; set; }

        [Required]
        [Display(Name = "Product Amount")]
        public decimal BasePrice { get; set; }

        [Display(Name = "Product Description")]
        public string Description { get; set; }

        [Display(Name = "Product Access Url")]
        public string AccessUrl { get; set; }   

        public bool IsActive { get; set; }
        public bool AllowPost { get; set; }

        public ProductViewModel()
        {}

        public ProductViewModel(Data.Product product)
        {
            Name = product.Name;
            Code = product.Code;
            BasePrice = product.BasePrice;
            Description = product.Description;
            AccessUrl = product.AccessUrl;
            IsActive = product.IsActive;
            AllowPost = true;
        }
    }
}