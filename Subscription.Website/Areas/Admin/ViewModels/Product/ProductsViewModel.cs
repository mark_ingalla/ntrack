﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Subscription.Website.Areas.Admin.Models;

namespace Subscription.Website.Areas.Admin.ViewModels.Product
{
    public class ProductsViewModel
    {
        public ProductsViewModel()
        {}

        public ProductsViewModel(List<Data.Product> products)
        {
            Products = products.Select(p => new ProductSummary(p)).ToList();
        }

        public List<ProductSummary> Products { get; set; }
    }
}