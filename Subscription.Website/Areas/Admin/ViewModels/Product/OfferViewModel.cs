﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Subscription.Data;

namespace Subscription.Website.Areas.Admin.ViewModels.Product
{
    public class OfferViewModel 
    {
        public Guid OfferKey { get; set; }
        public string Name { get; set; }
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public List<ProductFeature> Features { get; set; }
        [Display(Name = "Selected Features")]
        public List<ProductFeatureSummary> SelectedFeatures { get; set; }
        public bool IsActive { get; set; }
         public bool AllowPost { get; set; }

        public OfferViewModel(Data.Product product, ProductOffer offer)
        {
            if (product == null)
                return;

            ProductId = product.Id;
            ProductName = product.Name;
            Features = product.Features.Where(p => p.IsActive).ToList();
            AllowPost = true;
            
            if (offer != null)
            {
                Name = offer.Name;
                var query = from p in Features
                            join q in offer.Features on p.Code equals q.Code
                                into joinTbl
                            from q in joinTbl.DefaultIfEmpty()
                            select new ProductFeatureSummary
                                       {
                                           Code = p.Code,
                                           Limit = q != null ? q.Limit : 0,
                                           IsIncluded = q != null && q.IsIncluded,
                                           Name = p.Name
                                       };
                SelectedFeatures = query.ToList();
               // SelectedFeatures = offer.Features.Select(p => p.Code).ToList();
                IsActive = offer.IsActive;
            }
            else
            {
                SelectedFeatures = product.Features.Where(x => x.IsActive).Select(x => new ProductFeatureSummary
                                                                                           {
                                                                                               Code = x.Code,
                                                                                               IsIncluded = false,
                                                                                               Name = x.Name
                                                                                           }).ToList();
            }
        }

        public OfferViewModel()
        {
            AllowPost = true;
        }
 
    }
}