﻿using Subscription.Data;
using System;

namespace Subscription.Website.Areas.Admin.ViewModels.Product
{
    public class RemoveOfferViewModel
    {
        public Guid OfferKey { get; set; }
        public Guid ProductId { get; set; }
        public string Reason { get; set; }
        public bool AllowPost { get; set; }
        public string Name { get; set; }
        public string ProductName { get; set; }

        public RemoveOfferViewModel()
        {
            AllowPost = true;
        }

        public RemoveOfferViewModel(Data.Product product, ProductOffer offer) : this()
        {
            OfferKey = offer.Key;
            ProductId = product.Id;
            Name = offer.Name;
            ProductName = product.Name;
        }
    }
}