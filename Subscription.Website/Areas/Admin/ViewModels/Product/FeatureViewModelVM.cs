﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Subscription.Data;

namespace Subscription.Website.Areas.Admin.ViewModels.Product
{
    public class FeatureViewModel
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }

        [Required]
        public string Code { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public decimal Price { get; set; }

        public int Limit { get; set; }
        
        public bool IsActive { get; set; }
        public bool AllowPost { get; set; }

        public FeatureViewModel(Data.Product product, ProductFeature feature)
        {
            if (product == null)
                return;

            ProductId = product.Id;
            ProductName = product.Name;

            if(feature != null)
            {
                Code = feature.Code;
                Name = feature.Name;
                Price = feature.Price;
                Limit = feature.Limit;
                IsActive = feature.IsActive;
            }

            AllowPost = true;
        }

        public FeatureViewModel()
        {
            AllowPost = true;
        }
    }
}