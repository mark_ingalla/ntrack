using AttributeRouting;
using Subscription.Website.Infrastructure;
using System.Web.Mvc;

namespace Subscription.Website.Areas.Manager
{
    [RouteArea("Manager")]
    [Authorize(Roles = "Admin")]
    public abstract class ManagerController : BaseController
    {

    }
}