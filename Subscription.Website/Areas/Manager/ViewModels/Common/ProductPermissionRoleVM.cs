﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Subscription.Website.Areas.Manager.ViewModels.Common
{
    public class ProductRolePermissionVM
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }
}