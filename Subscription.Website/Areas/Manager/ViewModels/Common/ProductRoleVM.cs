﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Subscription.Website.Areas.Manager.ViewModels.Common
{
    public class ProductRoleVM
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<ProductRolePermissionVM> Permissions { get; set; }

        public ProductRoleVM()
        {
            Permissions = new List<ProductRolePermissionVM>();
        }
    }
}