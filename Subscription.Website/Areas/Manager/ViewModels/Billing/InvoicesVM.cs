﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Subscription.Data;
using Subscription.Website.ViewModels.Billing;

namespace Subscription.Website.Areas.Manager.ViewModels.Billing
{
    public class InvoicesVM
    {
        public List<InvoiceVM> Invoices { get; set; }


        public InvoicesVM(List<InvoiceVM> invoices)
        {
            Invoices = invoices;
        }
    }
}