﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Subscription.Website.ViewModels;

namespace Subscription.Website.Areas.Manager.ViewModels.Billing
{
    public class StartPaymentVM : ReturnUrlVM
    {
        public Guid InvoiceId { get; set; }
        public bool AllowPost { get; set; }
        public StartPaymentVM()
        {
            AllowPost = true;
        }   
    }
}