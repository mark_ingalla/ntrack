﻿using System;
using System.Linq;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using nTrack.Core.Utilities;
using nTrack.Data.Extenstions;
using Subscription.Data;
using Subscription.Data.Indices;
using Subscription.Messages.Commands.Account;
using Subscription.Website.ViewModels.Subscription;


namespace Subscription.Website.Areas.Manager.Controllers
{
    public class SubscriptionController : ManagerController
    {
        readonly IThreadingUtilities _threadingUtilities;

        public SubscriptionController(IThreadingUtilities threadingUtilities)
        {
            _threadingUtilities = threadingUtilities;
        }

        [GET("Subscriptions")]
        public ActionResult Index()
        {
            var subscriptions = DbSession.Query<SubscriptionIndex.Result, SubscriptionIndex>()
                                         .Where(x =>
                                                x.AccountId == ContextAccountId.ToStringId<Account>() &&
                                                x.TerminatedOnUTC == null)
                                         .ToList();

            var indexVM = new SubscriptionIndexVM(subscriptions);

            return View(indexVM);
        }

        [GET("Subscription/{offerKey}")]
        public ActionResult View(Guid offerKey)
        {
            return null;
            //var subscriptionCreateVM = CreateSubscriptionViewVM(ContextAccountId, productId, offerKey);
            //if (subscriptionCreateVM == null)
            //    return HttpNotFound();

            //return View(subscriptionCreateVM);
        }

        [POST("Subscription/{offerkey}/Cancel")]
        public ActionResult Cancel(Guid offerKey)
        {
            return null;
            //var sentTime = DateTime.UtcNow;

            //var account = DbSession.Load<Account>(ContextAccountId);
            //if (account == null)
            //    return HttpNotFound();

            //if (productId == Guid.Empty)
            //    return HttpNotFound();

            //bool found;

            //var subscription =
            //    account.Subscriptions.Single(p => p.ProductId == productId.ToStringId<Product>() && p.TerminatedOnUTC == null);

            //Bus.Send(new TerminateSubscription { AccountId = account.Id, SubscriptionKey = subscription.Key });
            //found = _threadingUtilities
            //    .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
            //                     () =>
            //                     DbSession.Query<SubscriptionIndex.Result, SubscriptionIndex>().Any(
            //                         p =>
            //                         p.AccountId == ContextAccountId.ToStringId<Account>() && p.ProductId == productId.ToStringId<Product>() &&
            //                         p.TerminatedOnUTC > sentTime));

            //if (found)
            //{
            //    Success("", "Subscription has been cancelled!");
            //    return RedirectToAction("Index");
            //}

            //var subscriptionEditVM = CreateSubscriptionViewVM(account.Id, productId, offerKey);

            //if (subscriptionEditVM == null)
            //    return HttpNotFound();

            //Info("", "The server is taking to long to confirm your request.");
            //subscriptionEditVM.AllowPost = false;
            //return View("View", subscriptionEditVM);
        }

        [POST("Subscription")]
        public ActionResult Create(SubscriptionCreateVM subscription)
        {
            var sentTime = DateTime.UtcNow;

            var createCommand = new CreateSubscription()
                {
                    AccountId = ContextAccountId,
                    OfferKey = subscription.OfferKey,
                    ProductId = subscription.ProductId,
                    SubscriptionKey = Guid.NewGuid()
                };

            Bus.Send(createCommand);

            //NOTE: its not the job of the UI to raise an invoice!!
            //RaiseInvoice invoiceCommand = new RaiseInvoice()
            //{
            //    AccountId = ContextAccountId,
            //    InvoiceId = Guid.NewGuid()
            //};

            //Bus.Send(invoiceCommand);



            bool found;

            found = _threadingUtilities
                .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                 () =>
                                 DbSession.Query<SubscriptionIndex.Result, SubscriptionIndex>().Any(
                                     p =>
                                     p.AccountId == ContextAccountId.ToStringId<Account>() && p.ProductId == subscription.ProductId.ToStringId<Product>() &&
                                     p.SubscribedOnUTC > sentTime));

            if (found)
            {
                Success("", "Product has been subscribed!");
                return RedirectToAction("Index");
            }

            Info("", "The server is taking to long to confirm your request.");

            var createVM = CreateSubscriptionViewVM(ContextAccountId, subscription.ProductId, subscription.OfferKey);

            createVM.AllowPost = false;

            return View("View", createVM);
        }

        SubscriptionCreateVM CreateSubscriptionViewVM(Guid accountId, Guid productId, Guid offerKey)
        {
            var account = DbSession.Load<Account>(accountId);
            if (account == null)
                return null;

            var offer =
                DbSession.Query<ProductOfferIndex.Result, ProductOfferIndex>().Single(
                    p => p.OfferKey == offerKey && p.ProductId == productId.ToStringId<Product>() && p.IsProductActive && p.IsOfferActive);


            var subscriptionCreateVM = new SubscriptionCreateVM(offer,
                                                                account.Subscriptions.Any(
                                                                    p =>
                                                                    p.OfferKey == offerKey &&
                                                                    p.ProductId == productId.ToStringId<Product>() &&
                                                                    p.TerminatedOnUTC == null));

            subscriptionCreateVM.ReturnUrl = Url.Action("Index");

            return subscriptionCreateVM;
        }
    }
}
