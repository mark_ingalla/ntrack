﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Hosting;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using nTrack.Data.Extenstions;
using Subscription.Data;
using Subscription.Website.Areas.Manager.ViewModels.Billing;
using Subscription.Website.ViewModels.Billing;

namespace Subscription.Website.Areas.Manager.Controllers
{
    public class BillingController : ManagerController
    {
        [GET("Billing/{accountId?}")]
        public ActionResult Index(Guid? accountId)
        {
            var account = DbSession.Load<Account>(accountId ?? ContextAccountId);
            if (account == null)
                return HttpNotFound();

            List<InvoiceVM> invoiceVms = new List<InvoiceVM>();


            var invoices =
                DbSession.Query<Invoice>().Where(
                    p => p.AccountId == account.IdString);

            foreach (var invoice in invoices)
            {
                var product = DbSession.Load<Product>(invoice.Details.First().ProductId);
                if (product != null)
                {
                    var offer = product.Offers.SingleOrDefault(p => p.Key == invoice.Details.First().OfferKey);

                    if (offer != null)
                    {

                        string path = Url.Content("~/Files/Invoices/");
                        invoiceVms.Add(new InvoiceVM(invoice, offer, path));
                    }
                }
            }

            var model = new InvoicesVM(invoiceVms);

            return View(model);
        }

        [GET("Billing/Invoice/{invoiceId}")]
        public FileResult Invoice(Guid invoiceId)
        {
            var invoice = DbSession.Load<Invoice>(invoiceId);

            if (invoice == null)
                return null;

            if (invoice.AccountId != ContextAccountId.ToStringId<Account>())
                return null;

            if (string.IsNullOrWhiteSpace(invoice.FileName))
                return null;

            var path = HostingEnvironment.MapPath("~/Files/Invoices");

            path += "/" + invoice.FileName;

            string contentType = "application/pdf";
            //Parameters to file are
            //1. The File Path on the File Server
            //2. The content type MIME type
            //3. The parameter for the file save by the browser
            return File(path, contentType, invoice.FileName);
        }

        [GET("Billing/StartPayment/{invoiceId}")]
        public ActionResult StartPayment(Guid invoiceId)
        {
            var invoice = DbSession.Load<Invoice>(invoiceId);
            if (invoice == null)
                return HttpNotFound();

            var model = new StartPaymentVM() { InvoiceId = invoice.Id };

            return View(model);
        }

        [POST("Billing/StartPayment/{invoiceId}")]
        public ActionResult StartPaymentPost(Guid invoiceId)
        {
            var invoice = DbSession.Load<Invoice>(invoiceId);
            if (invoice == null)
                return HttpNotFound();

            return RedirectToAction("StartPayment", "PayPal", new { invoiceNumber = invoice.InvoiceNumber });

        }

    }
}
