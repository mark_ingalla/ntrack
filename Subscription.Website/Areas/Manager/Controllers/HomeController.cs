﻿using System;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;

namespace Subscription.Website.Areas.Manager.Controllers
{
    public class HomeController : ManagerController
    {
        [GET("")]
        public ActionResult Index()
        {
            return View();
        }

        [POST("SetActiveTabInSession")]
        public void SetActiveTabInSession(string module, string tabId)
        {
            if (!string.IsNullOrWhiteSpace(module) && !string.IsNullOrWhiteSpace(tabId))
            {
                string tabPane = tabId.Replace("Tab", "");
                Session.Add(module + "ActiveTabId", "#" + tabId);
                Session.Add(module + "ActiveTabPaneId", "#" + tabPane);
            }
        }

        //[GET("SetContextAccount/{accountID}")]
        //public ActionResult SetContextAccount(Guid accountID)
        //{
        //    ClientSideStoreServices.SetContextAccountID(Response.Cookies, accountID);
        //    return RedirectToAction("Index", "Home");
        //}

    }
}
