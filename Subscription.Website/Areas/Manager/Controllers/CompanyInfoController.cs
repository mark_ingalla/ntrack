﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using Subscription.Messages.Commands.Account;
using Subscription.Website.Services;
using Subscription.Website.ViewModels.Account;
using Subscription.Website.ViewModels.Image;
using nTrack.Core.Enums;
using nTrack.Core.Utilities;
using nTrack.Data;
using Account = Subscription.Data.Account;

namespace Subscription.Website.Areas.Manager.Controllers
{
    public class CompanyInfoController : ManagerController
    {
        public const string ModuleName = "CompanyInfo";

        private readonly IThreadingUtilities _threadingUtilities;
        private readonly IAttachmentService _attachmentService;

        public CompanyInfoController(IThreadingUtilities threadingUtilities, IAttachmentService attachmentService)
        {
            _threadingUtilities = threadingUtilities;
            _attachmentService = attachmentService;
        }

        [Authorize]
        [GET("CompanyInfo/Edit/")]
        public ActionResult Edit(string returnUrl)
        {
            var account = DbSession.Load<Account>(ContextAccountId);

            if (account == null)
                return HttpNotFound();

            var countries = DbSession.Load<Countries>(Countries.EntityId);

            var accountVM = new AccountVM(countries != null ? countries.CountryNames : new string[0], account);

            accountVM.ReturnUrl = returnUrl;

            Raven.Abstractions.Data.Attachment attachment = _attachmentService.GetAttachment(account.Id,AttachmentEntities.AccountLogos.ToString());

            accountVM.ImageVM = new ImageVM()
                                    {
                                        Entity = AttachmentEntities.AccountLogos.ToString(),
                                        Id = account.Id,
                                        Avaible = attachment != null
                                    };

            ViewBag.Module = ModuleName;

            return View(accountVM);
        }

        [Authorize]
        [POST("CompanyInfo/Edit/")]
        public ActionResult Edit(AccountVM vm, IEnumerable<HttpPostedFileBase> files)
        {
            DateTime sentTime = DateTime.UtcNow;

            var account = DbSession.Load<Account>(ContextAccountId);

            if (account == null)
                return HttpNotFound();

            if (ModelState.IsValid)
            {
                Bus.Send(new UpdateAccount
                {
                    AccountId = account.Id,
                    AddressLine1 = vm.AddressLine1,
                    AddressLine2 = vm.AddressLine2,
                    CompanyName = vm.CompanyName,
                    Country = vm.Country,
                    CompanyPhone = vm.Phone,
                    Postcode = vm.Postcode,
                    State = vm.State,
                    Suburb = vm.Suburb,
                });
            }

            bool found = _threadingUtilities
               .WaitOnPredicate(DELAY_WAIT_MILLISECONDS, TIMEOUT_WAIT_SECONDS,
                                () =>
                                DbSession.Query<Account>().Any(x => x.Id == account.Id && x.UpdatedOn > sentTime));

            if (found)
            {
                Success("", "Account data has been changed!");
                if (!string.IsNullOrWhiteSpace(vm.ReturnUrl))
                    return Redirect(vm.ReturnUrl);
                return RedirectToAction("Edit");
            }

            Info("", "The server is taking to long to confirm your request.");

            vm.AllowPost = false;
            vm.AccountId = account.Id;
            var countries = DbSession.Load<Countries>(Countries.EntityId);
            vm.CountryOptions = countries.CountryNames
                .Select(x => new SelectListItem { Text = x, Value = x });

            return View(vm);
        }

    }
}
