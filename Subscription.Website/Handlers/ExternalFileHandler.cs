﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Routing;

namespace Subscription.Website.Handlers
{
    public class ExternalFileHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var response = context.Response;
            var request = context.Request;
            var server = context.Server;
            var uniqueFileIdentifier = HttpContext.Current.Request.RequestContext.RouteData.Values["uniqueidentifier"].ToString();
            
            FileInfo fileInfo =
                new FileInfo(Path.Combine(HttpContext.Current.Server.MapPath("~/Files/Invoices"), uniqueFileIdentifier));

        }

        public bool IsReusable { get; private set; }
    }


    public class ExternalFileRouteHandler : IRouteHandler
    {
        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return new ExternalFileHandler();
        }
    }
}