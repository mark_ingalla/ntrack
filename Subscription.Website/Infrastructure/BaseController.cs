﻿using NServiceBus;
using nTrack.Data;
using nTrack.Web.Bootstrap;
using Raven.Client;
using StructureMap.Attributes;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Subscription.Website.Infrastructure
{
    public abstract class BaseController : Controller
    {
        public const int DELAY_WAIT_MILLISECONDS = 300;
        public const int TIMEOUT_WAIT_SECONDS = 5;

        [SetterProperty]
        public IDocumentSession DbSession { get; set; }
        protected IBus Bus { get { return MvcApplication.Bus; } }

        protected Guid ContextAccountId { get { return ((CustomIdentity)(User.Identity)).AccountId; } }
        public Guid ContextUserKey { get { return ((CustomIdentity)(User.Identity)).UserKey; } }

        private List<Alert> AlertsCreator()
        {
            if (TempData["alert"] == null)
                TempData["alert"] = new List<Alert>();

            List<Alert> alerts = TempData["alert"] as List<Alert>;

            return alerts;

        }

        protected void Alert(AlertType type, string tabRouteValue, string message)
        {
            var alerts = AlertsCreator();

            if (alerts != null)
                alerts.Add(new Alert(type, message, tabRouteValue));
        }
        protected void RecordCreated(string tabRouteValue)
        {
            Alert(AlertType.Success, tabRouteValue, "Your record has been created.");
        }
        protected void RecordUpdated(string tabRouteValue)
        {
            Alert(AlertType.Success, tabRouteValue, "Your record has been updated.");
        }
        protected void Success(string tabRouteValue, string message)
        {
            Alert(AlertType.Success, tabRouteValue, message);
        }
        protected void Failure(string tabRouteValue, string message)
        {
            Alert(AlertType.Error, tabRouteValue, message);
        }
        protected void Info(string tabRouteValue, string message)
        {
            Alert(AlertType.Info, tabRouteValue, message);
        }
    }
}