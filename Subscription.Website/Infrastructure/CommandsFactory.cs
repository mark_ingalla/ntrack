﻿using System;
using Subscription.Messages.Commands;
using Subscription.Messages.Commands.Account.User;
using Subscription.Website.ViewModels.Account;
using nTrack.Core.Messages;
using UserVM = Subscription.Website.ViewModels.User.UserVM;

namespace Subscription.Website.Infrastructure
{
    public static class CommandsFactory
    {
        public static class Subscription
        {
            public static class Account
            {
                public static class User
                {
                    public static AddUser AddUser(UserVM userVM, Guid accountId, Guid userKey)
                    {
                        return new AddUser()
                                   {
                                       AccountId = accountId,
                                       Email = userVM.Email,
                                       FirstName = userVM.FirstName,
                                       LastName = userVM.LastName,
                                       Password = userVM.Password,
                                       Role = (UserRole) Enum.Parse(typeof (UserRole), userVM.Role),
                                       UserKey = userKey
                                   };
                    }

                    public static UpdateUserDetail UpdateUserDetail(Guid accountId, string email, string firstName, string lastName,
                        string mobile, string phone, Guid userKey)
                    {
                        return new UpdateUserDetail()
                        {
                            AccountId = accountId,
                            Email = email,
                            FirstName = firstName,
                            LastName = lastName,
                            Mobile = mobile,
                            Phone = phone,
                            UserKey = userKey
                        };
                    }
                }

                public static RegisterAccount RegisterAccount(RegisterVM registerVM, Guid accountId, Guid userId,
                    Guid verificationToken, string verificationPageUrl, string productName, string offerName, string IpAddress)
                {
                    return new RegisterAccount()
                               {
                                   AccountId = accountId,
                                   CompanyName = registerVM.CompanyName,
                                   CompanyPhone = registerVM.CompanyPhone,
                                   Country = registerVM.Country,
                                   Email = registerVM.Email,
                                   EmailVerificationPageUrl = verificationPageUrl,
                                   EmailVerificationToken = verificationToken,
                                   FirstName = registerVM.FirstName,
                                   LastName = registerVM.LastName,
                                   ProductId = registerVM.SelectProductId.Value,
                                   OfferKey = registerVM.SelectedOfferKey.Value,
                                   Password = registerVM.Password,
                                   UserKey = userId,
                                   OfferQTY = registerVM.OfferQTY,
                                   OfferName = offerName,
                                   ProductName = productName,
                                   IPAddress = IpAddress

                               };
                }

            }
        }

       
    }
}