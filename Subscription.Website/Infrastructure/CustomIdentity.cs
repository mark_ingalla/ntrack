﻿using System;
using System.Security.Principal;

namespace Subscription.Website.Infrastructure
{
    public class CustomIdentity : IIdentity
    {
        public string AuthenticationType { get { return "Forms"; } }
        public bool IsAuthenticated { get; private set; }
        public string Name { get; private set; }

        public Guid AccountId { get; private set; }
        public Guid UserKey { get; private set; }


        public string UserFullName { get; private set; }
        public string CompanyName { get; private set; }
        public string Ip { get; private set; }


        public CustomIdentity(Guid accountId, Guid userKey, string userFullName, string companyName, string ip)
        {
            AccountId = accountId;
            UserKey = userKey;
            UserFullName = userFullName;
            CompanyName = companyName;
            IsAuthenticated = true;
            Name = userKey.ToString();
            Ip = ip;
        }
    }
}