﻿using Raven.Client;
using Raven.Client.Document;
using StructureMap.Configuration.DSL;

namespace Subscription.Website.Infrastructure
{
    public class RavenDBRegistry : Registry
    {
        public RavenDBRegistry()
        {
            ForSingletonOf<IDocumentStore>().Use(
                () =>
                {
                    var store = new DocumentStore { ConnectionStringName = "RavenDB" };
                    store.RegisterListener(new NoStaleQueriesListener());
                    store.Initialize();
                    return store;
                });

            For<IDocumentSession>()
                .HybridHttpOrThreadLocalScoped()
                .Use(context =>
                         {
                             var session = context.GetInstance<IDocumentStore>().OpenSession();
                             session.Advanced.UseOptimisticConcurrency = true;
                             return session;
                         });
        }
    }
}