﻿using System.Web.Optimization;

namespace Subscription.Website.App_Start
{
    public static class BundleConfig
    {
        public static void ConfigureBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-1.8.*"));

            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                        "~/Scripts/Kendo/jquery.min*",
                        "~/Scripts/Kendo/kendo.web*",
                        "~/Scripts/Kendo/kendo.aspnetmvc*"));
                        

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate.min.*",
                        "~/Scripts/jquery.validate.unobtrusive.min.*"));

            bundles.Add(new ScriptBundle("~/bundles/ntrack").Include(
                        "~/Scripts/common.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.min.*",
                "~/Content/bootstrap-responsive.min.*",
                "~/Content/bootstrap-combobox*",
                "~/Content/validation*",
                "~/Content/custom*"));

            bundles.Add(new StyleBundle("~/Content/Kendo/css").Include(
                        "~/Content/kendo/kendo.common*",
                        "~/Content/kendo/kendo.default*",
                        "~/Content/kendo/kendo.dataviz*"));

            bundles.IgnoreList.Ignore("*.intellisense.js");
            bundles.IgnoreList.Ignore("*-vsdoc.js");
            bundles.IgnoreList.Ignore("*.debug.js", OptimizationMode.WhenEnabled);
        }
    }
}