﻿using System.Web.Mvc;
using StructureMap;
using Subscription.Website.App_Start;
using Subscription.Website.Infrastructure;

[assembly: WebActivator.PreApplicationStartMethod(typeof(StructuremapStart), "Start")]
namespace Subscription.Website.App_Start
{
    public static class StructuremapStart
    {
        public static void Start()
        {
            ObjectFactory.Initialize(x => x.Scan(
                scan =>
                    {
                        scan.AssembliesFromApplicationBaseDirectory();
                        scan.TheCallingAssembly();
                        scan.LookForRegistries();
                        scan.WithDefaultConventions();
                      //  scan.AddAllTypesOf<Profile>();
                    }));

            DependencyResolver.SetResolver(new SmDependencyResolver(ObjectFactory.Container));
        }
    }
}