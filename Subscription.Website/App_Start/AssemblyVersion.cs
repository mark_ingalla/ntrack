﻿using System.Diagnostics;
using System.Reflection;

namespace Subscription.Website.App_Start
{
    public class AssemblyVersion
    {
        public static void LoadAssemblyVersion()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);

            MvcApplication.Version = fvi.FileVersion;
        }
    }
}