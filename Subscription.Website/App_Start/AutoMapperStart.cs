using System.Linq;
using AutoMapper;
using StructureMap;
using Subscription.Website.App_Start;

[assembly: WebActivator.PostApplicationStartMethod(typeof(AutoMapperStart), "Start")]
namespace Subscription.Website.App_Start
{
    public static class AutoMapperStart
    {
        public static void Start()
        {
            var profiles = ObjectFactory.GetAllInstances<Profile>().ToList();
            profiles.ForEach(Mapper.AddProfile);
        }
    }
}