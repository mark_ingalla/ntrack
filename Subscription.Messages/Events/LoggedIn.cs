﻿using System;

namespace Subscription.Messages.Events
{
    public class LoggedIn
    {
        public Guid AccountId { get; set; }
        public Guid UserKey { get; set; }
        public DateTime LoggedInOnUTC { get; set; }
        public Guid SessionToken { get; set; }

        public LoggedIn(Guid accountId, Guid userId, DateTime loggedInOnUtc, Guid sessionToken)
        {
            AccountId = accountId;
            UserKey = userId;
            LoggedInOnUTC = loggedInOnUtc;
            SessionToken = sessionToken;
        }
    }
}
