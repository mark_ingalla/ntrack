﻿using System;

namespace Subscription.Messages.Events
{
    public class UserInvitationStatus
    {
        public Guid VerificationToken { get; set; }
        public string UserEmail { get; set; }
        public string Status { get; set; }

        public UserInvitationStatus(Guid verificationToken, string userEmail, string status)
        {
            VerificationToken = verificationToken;
            UserEmail = userEmail;
            Status = status;
        }
    }
}
