﻿using System;

namespace Subscription.Messages.Events
{
    public class UserVerificationStatus
    {
        public Guid AccountId { get; set; }
        public Guid UserKey { get; set; }
        public Guid VerificationToken { get; set; }
        public string Status { get; set; }

        public UserVerificationStatus(Guid accountId, Guid userKey, Guid verificationToken, string status)
        {
            AccountId = accountId;
            UserKey = userKey;
            VerificationToken = verificationToken;
            Status = status;
        }
    }
}
