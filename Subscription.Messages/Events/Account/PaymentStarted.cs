﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Subscription.Messages.Events.Account
{
    public class PaymentStarted
    {
        public Guid AccountId { get; set; }
        public Guid ProductId { get; set; }
        public Guid InvoiceId { get; set; }
        public bool First { get; set; }

        public PaymentStarted(Guid accountId, Guid productId, Guid invoiceKey)
        {
            AccountId = accountId;
            ProductId = productId;
            InvoiceId = invoiceKey;
        }
    }
}
