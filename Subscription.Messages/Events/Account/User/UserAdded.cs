﻿using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Events.Account.User
{
    public class UserAdded
    {
        public Guid AccountId { get; set; }
        public Guid UserKey { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public UserRole Role { get; set; }

        public UserAdded(Guid accountId, Guid userKey, string email, string password, string firstName, string lastName, UserRole role)
        {
            AccountId = accountId;
            UserKey = userKey;
            Email = email;
            Password = password;
            FirstName = firstName;
            LastName = lastName;
            Role = role;
        }

    }
}
