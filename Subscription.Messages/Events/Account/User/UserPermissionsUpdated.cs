﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Subscription.Messages.Events.Account.User
{
    public class UserPermissionsUpdated
    {
        public Guid AccountId { get; set; }
        public Guid UserId { get; set; }
        public Dictionary<Guid, List<string>> Permissions { get; set; }

        public UserPermissionsUpdated(Guid accountId, Guid userId, Dictionary<Guid, List<string>> permissions)
        {
            AccountId = accountId;
            UserId = userId;
            Permissions = permissions;
        }
    }
}
