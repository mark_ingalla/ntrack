using System;

namespace Subscription.Messages.Events.Account.User
{
    public class UserDetailUpdated
    {
        public Guid AccountId { get; set; }
        public Guid UserKey { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }

        public UserDetailUpdated(Guid accountId, Guid userKey, string firstName, string lastName, string phone, string mobile, string email)
        {
            AccountId = accountId;
            UserKey = userKey;
            FirstName = firstName;
            LastName = lastName;
            Phone = phone;
            Mobile = mobile;
            Email = email;
        }
    }
}