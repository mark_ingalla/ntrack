using System;

namespace Subscription.Messages.Events.Account.User
{
    public class UserLocked
    {
        public Guid AccountId { get; set; }
        public Guid UserKey { get; set; }
        public string Reason { get; set; }

        public UserLocked(Guid accountId, Guid userId, string reason)
        {
            AccountId = accountId;
            UserKey = userId;
            Reason = reason;
        }
    }
}