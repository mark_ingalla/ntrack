﻿using System;

namespace Subscription.Messages.Events.Account
{
    public class InvoicePaid
    {
        public Guid AccountId { get; set; }
        public Guid InvoiceId { get; set; }
        public decimal Amount { get; set; }
        public DateTime PaidOnUTC { get; set; }

        public InvoicePaid(Guid accountId, Guid invoiceId, decimal amount, DateTime paidOnUTC)
        {
            AccountId = accountId;
            InvoiceId = invoiceId;

            Amount = amount;
            PaidOnUTC = paidOnUTC;
        }
    }
}
