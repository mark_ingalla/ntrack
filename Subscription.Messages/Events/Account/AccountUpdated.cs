﻿using System;

namespace Subscription.Messages.Events.Account
{
    public class AccountUpdated
    {
        public Guid AccountId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyPhone { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }

        public AccountUpdated(Guid accountId, string companyName, string companyPhone, string addressLine1, string addressLine2, string suburb, string state, string postcode, string country)
        {
            AccountId = accountId;
            CompanyName = companyName;
            CompanyPhone = companyPhone;
            AddressLine1 = addressLine1;
            AddressLine2 = addressLine2;
            Suburb = suburb;
            State = state;
            Postcode = postcode;
            Country = country;
        }
    }
}
