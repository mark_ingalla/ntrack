using System;

namespace Subscription.Messages.Events.Account
{
    public sealed class UserRevoked
    {
        public Guid AccountId { get; set; }
        public Guid UserId { get; set; }

        public UserRevoked(Guid accountId, Guid userId)
        {
            AccountId = accountId;
            UserId = userId;
        }
    }
}