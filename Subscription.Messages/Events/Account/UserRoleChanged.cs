﻿using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Events.Account
{
    public class UserRoleChanged
    {
        public Guid AccountId { get; set; }
        public Guid UserKey { get; set; }
        public UserRole Role { get; set; }

        public UserRoleChanged(Guid accountId, Guid userKey, UserRole role)
        {
            AccountId = accountId;
            UserKey = userKey;
            Role = role;
        }
    }
}