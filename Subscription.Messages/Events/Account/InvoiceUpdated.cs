﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Subscription.Messages.Events.Account
{
    public class InvoiceUpdated
    {
        public Guid AccountId { get; set; }
        public Guid ProductId { get; set; }
        public Guid InvoiceId { get; set; }

        public InvoiceUpdated(Guid accountId, Guid productId, Guid invoiceId)
        {
            AccountId = accountId;
            ProductId = productId;
            InvoiceId = invoiceId;
        }
    }
}
