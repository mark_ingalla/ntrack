﻿using System;

namespace Subscription.Messages.Events.Account
{
    public class AccountDeactivated
    {
        public Guid AccountId { get; set; }
        public string Reason { get; set; }

        public AccountDeactivated(Guid accountId, string reason)
        {
            AccountId = accountId;
            Reason = reason;
        }
    }
}