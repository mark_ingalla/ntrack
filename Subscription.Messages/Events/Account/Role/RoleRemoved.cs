﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Subscription.Messages.Events.Account.Role
{
    public class RoleRemoved
    {
        public Guid AccountId { get; set; }
        public Guid RoleId { get; set; }

        public RoleRemoved(Guid accountId, Guid id)
        {
            AccountId = accountId;
            RoleId = id;
        }
    }
}
