﻿using System;

namespace Subscription.Messages.Events.Account
{
    public class InvoiceRaised
    {
        public Guid AccountId { get; set; }
        public Guid InvoiceId { get; set; }
        public int InvoiceNumber { get; set; }
        public decimal Amount { get; set; }
        public DateTime InvoiceDateUTC { get; set; }
        public InvoiceDetail[] Details { get; set; }

        public InvoiceRaised(Guid accountId, Guid invoiceId, int invoiceNumber, decimal amount, DateTime invoiceDateUTC, InvoiceDetail[] details)
        {
            AccountId = accountId;
            InvoiceId = invoiceId;
            InvoiceNumber = invoiceNumber;
            Amount = amount;
            InvoiceDateUTC = invoiceDateUTC;
            Details = details;
        }
    }

    public class InvoiceDetail
    {
        public Guid ProductId { get; set; }
        public string ProductCode { get; set; }
        public Guid OfferKey { get; set; }
        public decimal Amount { get; set; }

        public InvoiceDetail(Guid productId, string productCode, Guid offerKey, decimal amount)
        {
            ProductId = productId;
            ProductCode = productCode;
            OfferKey = offerKey;
            Amount = amount;
        }
    }
}
