using System;

namespace Subscription.Messages.Events.User
{
    public class UserPermissionAdded
    {
        public Guid UserId { get; set; }
        public Guid AccountId { get; set; }
        public string Role { get; set; }
        public Guid ProductId { get; set; }
        public string PermissionCode { get; set; }

        public UserPermissionAdded(Guid userId, Guid accountId, string role, Guid productId, string permissionCode)
        {
            UserId = userId;
            AccountId = accountId;
            Role = role;
            ProductId = productId;
            PermissionCode = permissionCode;
        }
    }
}