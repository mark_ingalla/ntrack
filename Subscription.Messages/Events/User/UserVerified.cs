﻿using System;

namespace Subscription.Messages.Events.User
{
    public class UserVerified
    {
        public Guid UserId { get; set; }

        public UserVerified(Guid userId)
        {
            UserId = userId;
        }
    }
}
