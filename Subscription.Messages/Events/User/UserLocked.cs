using System;

namespace Subscription.Messages.Events.User
{
    public class UserLocked
    {
        public Guid UserId { get; set; }
        public string Reason { get; set; }
        public DateTime LockDate { get; set; }

        public UserLocked(Guid userId, string reason, DateTime lockDate)
        {
            UserId = userId;
            Reason = reason;
            LockDate = lockDate;
        }
    }
}