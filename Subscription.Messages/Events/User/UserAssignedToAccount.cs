using System;

namespace Subscription.Messages.Events.User
{
    public class UserAssignedToAccount
    {
        public Guid UserId { get; set; }
        public Guid AccountId { get; set; }
        public string Role { get; set; }

        public UserAssignedToAccount(Guid userId, Guid accountId, string role)
        {
            UserId = userId;
            AccountId = accountId;
            Role = role;
        }
    }
}