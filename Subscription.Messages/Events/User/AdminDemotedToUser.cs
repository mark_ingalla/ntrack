using System;

namespace Subscription.Messages.Events.User
{
    public class AdminDemotedToUser
    {
        public Guid UserId { get; set; }
        public Guid AccountId { get; set; }

        public AdminDemotedToUser(Guid userId, Guid accountId)
        {
            UserId = userId;
            AccountId = accountId;
        }
    }
}