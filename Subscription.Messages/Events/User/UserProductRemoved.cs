using System;

namespace Subscription.Messages.Events.User
{
    public class UserProductRemoved
    {
        public Guid UserId { get; set; }
        public Guid AccountId { get; set; }
        public Guid ProductId { get; set; }

        public UserProductRemoved(Guid userId, Guid accountId, Guid productId)
        {
            UserId = userId;
            AccountId = accountId;
            ProductId = productId;
        }
    }
}