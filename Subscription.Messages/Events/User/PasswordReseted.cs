﻿using System;

namespace Subscription.Messages.Events.User
{
    public class PasswordReseted
    {
        public string Password { get; set; }
        public Guid UserId { get; set; }

        public PasswordReseted(Guid userId, string password)
        {
            UserId = userId;
            Password = password;
        }
    }
}
