using System;

namespace Subscription.Messages.Events.User
{
    public class UserUnlocked
    {
        public Guid UserId { get; set; }
        public string Reason { get; set; }
        public DateTime UnlockDate { get; set; }

        public UserUnlocked(Guid userId, string reason, DateTime unlockDate)
        {
            UserId = userId;
            Reason = reason;
            UnlockDate = unlockDate;
        }
    }
}