using System;

namespace Subscription.Messages.Events.User
{
    public class UserAssignmentToAccountRemoved
    {
        public Guid UserId { get; set; }
        public Guid AccountId { get; set; }

        public UserAssignmentToAccountRemoved(Guid userId, Guid accountId)
        {
            UserId = userId;
            AccountId = accountId;
        }
    }
}