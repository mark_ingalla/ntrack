﻿using System;

namespace Subscription.Messages.Events.User
{
    public class UserPromotedToAdmin
    {
        public Guid UserId { get; set; }
        public Guid AccountId { get; set; }

        public UserPromotedToAdmin(Guid userId, Guid accountId)
        {
            UserId = userId;
            AccountId = accountId;
        }
    }
}
