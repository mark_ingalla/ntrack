using System;

namespace Subscription.Messages.Events.User
{
    public class UserDetailUpdated
    {
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }

        public UserDetailUpdated(Guid userId, string firstName, string lastName, string phone, string mobile, string email)
        {
            UserId = userId;
            FirstName = firstName;
            LastName = lastName;
            Phone = phone;
            Mobile = mobile;
            Email = email;
        }
    }
}