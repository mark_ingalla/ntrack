﻿using System;

namespace Subscription.Messages.Events
{
    public class LoggedOut
    {
        public Guid AccountId { get; set; }
        public Guid UserKey { get; set; }
        public Guid SessionToken { get; set; }
        public DateTime LoggedOutOn { get; set; }
    }
}
