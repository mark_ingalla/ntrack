﻿using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Events
{
    public class RemovedRolePermission
    {
        public Guid RoleId { get; set; }
        public String PermissionName { get; set; }
    }
}
