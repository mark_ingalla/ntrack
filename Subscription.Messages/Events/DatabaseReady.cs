﻿using System;

namespace Subscription.Messages.Events
{
    public class DatabaseReady
    {
        public Guid AccountId { get; set; }
        public string DatabaseName { get; set; }
        public Guid ProductId { get; set; }
        public string ProductCode { get; set; }

    }
}
