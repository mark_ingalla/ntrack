using System;

namespace Subscription.Messages.Events
{
    public class RoleCreated
    {
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
    }
}