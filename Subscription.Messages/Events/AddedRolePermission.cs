﻿using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Events
{
    public class AddedRolePermission
    {
        public Guid RoleId { get; set; }
        public String PermissionName { get; set; }

        public Guid ProductId { get; set; }
    }
}
