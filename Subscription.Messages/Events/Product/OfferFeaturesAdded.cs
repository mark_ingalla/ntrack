using System;

namespace Subscription.Messages.Events.Product
{
    public class OfferFeaturesAdded
    {
        public Guid ProductId { get; set; }
        public Guid OfferKey { get; set; }
        public string[] FeatureCodes { get; set; }

        public OfferFeaturesAdded(Guid productId, Guid offerKey, string[] featureCodes)
        {
            ProductId = productId;
            OfferKey = offerKey;
            FeatureCodes = featureCodes;
        }
    }
}