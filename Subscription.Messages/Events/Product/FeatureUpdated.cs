using System;

namespace Subscription.Messages.Events.Product
{
    public class FeatureUpdated
    {
        public Guid ProductId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Limit { get; set; }

        public FeatureUpdated(Guid productId, string code, string name, decimal price, int limit = 0)
        {
            ProductId = productId;
            Code = code;
            Name = name;
            Price = price;
            Limit = limit;
        }
    }
}