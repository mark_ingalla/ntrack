using System;

namespace Subscription.Messages.Events.Product
{
    public class ProductPermissionUpdated
    {
        public Guid ProductId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        public ProductPermissionUpdated(Guid productId, string code, string name)
        {
            ProductId = productId;
            Code = code;
            Name = name;
        }
    }
}