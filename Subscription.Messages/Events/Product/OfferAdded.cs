using System;

namespace Subscription.Messages.Events.Product
{
    public class OfferAdded
    {
        public Guid ProductId { get; set; }
        public Guid OfferKey { get; set; }
        public string Name { get; set; }
        public decimal BasePrice { get; set; }

        public OfferAdded(Guid productId, Guid offerKey, string name, decimal basePrice)
        {
            ProductId = productId;
            OfferKey = offerKey;
            Name = name;
            BasePrice = basePrice;
        }
    }
}