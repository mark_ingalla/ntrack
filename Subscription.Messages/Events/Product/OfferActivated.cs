using System;

namespace Subscription.Messages.Events.Product
{
    public class OfferActivated
    {
        public Guid ProductId { get; set; }
        public Guid OfferKey { get; set; }

        public OfferActivated(Guid productId, Guid offerKey)
        {
            ProductId = productId;
            OfferKey = offerKey;
        }
    }
}