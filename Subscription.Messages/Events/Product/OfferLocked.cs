using System;

namespace Subscription.Messages.Events.Product
{
    public class OfferLocked
    {
        public Guid ProductId { get; set; }
        public Guid OfferKey { get; set; }

        public OfferLocked(Guid productId, Guid offerKey)
        {
            ProductId = productId;
            OfferKey = offerKey;
        }
    }
}