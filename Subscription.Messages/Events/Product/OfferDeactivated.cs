using System;

namespace Subscription.Messages.Events.Product
{
    public class OfferDeactivated
    {
        public Guid ProductId { get; set; }
        public Guid OfferKey { get; set; }
        public string Reason { get; set; }

        public OfferDeactivated(Guid productId, Guid offerKey, string reason)
        {
            ProductId = productId;
            OfferKey = offerKey;
            Reason = reason;
        }
    }
}