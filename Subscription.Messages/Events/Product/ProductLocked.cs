using System;

namespace Subscription.Messages.Events.Product
{
    public class ProductLocked
    {
        public Guid ProductId { get; set; }

        public ProductLocked(Guid productId)
        {
            ProductId = productId;
        }
    }
}