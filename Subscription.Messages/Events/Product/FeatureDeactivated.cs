using System;

namespace Subscription.Messages.Events.Product
{
    public class FeatureDeactivated
    {
        public Guid ProductId { get; set; }
        public string FeatureCode { get; set; }

        public FeatureDeactivated(Guid productId, string featureCode)
        {
            ProductId = productId;
            FeatureCode = featureCode;
        }
    }
}