using System;

namespace Subscription.Messages.Events.Product
{
    public class ProductPermissionRemoved
    {
        public Guid ProductId { get; set; }
        public string Code { get; set; }

        public ProductPermissionRemoved(Guid productId, string code)
        {
            ProductId = productId;
            Code = code;
        }
    }
}