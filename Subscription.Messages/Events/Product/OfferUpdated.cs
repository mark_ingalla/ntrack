using System;

namespace Subscription.Messages.Events.Product
{
    public class OfferUpdated
    {
        public Guid ProductId { get; set; }
        public Guid OfferKey { get; set; }
        public string OfferName { get; set; }

        public OfferUpdated(Guid productId, Guid offerKey, string offerName)
        {
            ProductId = productId;
            OfferKey = offerKey;
            OfferName = offerName;
        }
    }
}