﻿using System;

namespace Subscription.Messages.Events
{
    public class TokenSeen
    {
        public Guid Token { get; set; }
        public DateTime SeenOn { get; set; }
    }
}
