﻿using System;

namespace Subscription.Messages.Events
{
    public class UserPasswordResetStatus
    {
        public Guid AccountId { get; set; }
        public Guid UserKey { get; set; }
        public Guid ResetToken { get; set; }
        public string UserEmail { get; set; }
        public string Status { get; set; }

        public UserPasswordResetStatus(Guid accountId, Guid userKey, Guid resetToken, string userEmail, string status)
        {
            AccountId = accountId;
            UserKey = userKey;
            ResetToken = resetToken;
            UserEmail = userEmail;
            Status = status;
        }
    }
}
