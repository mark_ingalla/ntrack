﻿using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Account.User
{
    public class UpdateUserDetail : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid UserKey { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
    }
}
