﻿using nTrack.Core.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Subscription.Messages.Commands.Account.User
{
    public class UpdateUserPermissions : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid UserId { get; set; }
        public Dictionary<Guid, List<string>> Permissions { get; set; } 
    }
}
