﻿using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Account.User
{
    public class AddUser : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid UserKey { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public UserRole Role { get; set; }
    }
}
