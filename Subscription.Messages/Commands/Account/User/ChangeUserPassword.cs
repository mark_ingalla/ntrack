﻿using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Account.User
{
    public class ChangeUserPassword : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid UserKey { get; set; }
        public string NewPassword { get; set; }
    }
}
