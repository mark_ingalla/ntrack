﻿using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Account
{
    public class ActivateAccount : MessageBase
    {
        public Guid AccountId { get; set; }
    }
}
