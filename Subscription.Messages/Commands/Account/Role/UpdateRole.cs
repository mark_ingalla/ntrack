﻿using nTrack.Core.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Subscription.Messages.Commands.Account.Role
{
    public class UpdateRole : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid RoleId { get; set; }
        public string Name { get; set; }
        public Dictionary<Guid, List<string>> Permissions { get; set; }
    }
}
