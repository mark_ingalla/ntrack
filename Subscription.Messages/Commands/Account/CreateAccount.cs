﻿using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Account
{
    public class CreateAccount : MessageBase
    {
        public Guid AccountId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyPhone { get; set; }
        public string Country { get; set; }
    }
}
