﻿using System;
using System.Collections.Generic;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Account
{
    public class AddFeaturesToSubscription : MessageBase
    {
        public Guid AccountId { get; set; }
        //productId - list of feature codes
        public Dictionary<Guid, List<string>> ProductsFeatures { get; set; }
    }
}
