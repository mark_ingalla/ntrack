using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.User
{
    public class LockUser : MessageBase
    {
        public string Reason { get; set; }
        public Guid UserId { get; set; }

    }
}