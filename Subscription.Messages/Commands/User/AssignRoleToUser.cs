﻿using System;
using System.Collections.Generic;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.User
{
    public class AssignRoleToUser : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid UserId { get; set; }
        public string RoleName { get; set; }
        public Dictionary<Guid, List<string>> RoleProductsPermissions { get; set; }
    }
}
