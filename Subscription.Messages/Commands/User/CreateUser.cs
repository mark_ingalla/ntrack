using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.User
{
    public class CreateUser : MessageBase
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
    }
}