using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.User
{
    public class PromoteUserToAdmin : MessageBase
    {
        public Guid UserId { get; set; }
        public Guid AccountId { get; set; }
    }
}