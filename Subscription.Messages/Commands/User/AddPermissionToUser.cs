﻿using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.User
{
    public class AddPermissionToUser : MessageBase
    {
        public Guid UserId { get; set; }
        public Guid AccountId { get; set; }
        public Guid ProductId { get; set; }
        public string Role { get; set; }
        public string Permission { get; set; }

    }
}
