﻿using System;

namespace Subscription.Messages.Commands.User
{
    public class SeeToken
    {
        public Guid Token { get; set; }
        public DateTime SeenOn { get; set; }
    }
}
