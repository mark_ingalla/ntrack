using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.User
{
    public class RevokePermissionFromUser : MessageBase
    {
        
        public Guid UserId { get; set; }
        public Guid AccountId { get; set; }
        public string RoleName { get; set; }
        public Guid ProductId { get; set; }
        public string Permission { get; set; }

    }
}