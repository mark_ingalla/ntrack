using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.User
{
    public class UnlockUser : MessageBase
    {
        public string Reason { get; set; }
        public Guid UserId { get; set; }

    }
}