﻿using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.User
{
    public class PrepareResetPassword : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid UserId { get; set; }
        public string UserEmail { get; set; }
        public Guid ResetPasswordToken { get; set; }
        public string ResetPasswordLink { get; set; }
    }
}
