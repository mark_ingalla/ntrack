﻿using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.User
{
    public class LogOut : MessageBase
    {
        public Guid SessionToken { get; set; }
    }
}
