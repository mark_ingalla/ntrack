﻿using System;
using System.ComponentModel.DataAnnotations;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.User
{
    public class DoResetPassword : MessageBase
    {
        public Guid ResetPasswordToken { get; set; }
        
        [Required]
        public string NewPassword { get; set; }
    }
}
