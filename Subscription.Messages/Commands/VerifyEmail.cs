﻿using System;

namespace Subscription.Messages.Commands
{
    public class VerifyEmail
    {
        public Guid EmailVerificationToken { get; set; }
    }
}