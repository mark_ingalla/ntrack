
namespace Subscription.Messages.Commands.Email
{
    public class SendResetPasswordEmail : EmailMessageBase
    {
        public string ResetPasswordUrl { get; set; }

    }
}