﻿using System;

namespace Subscription.Messages.Commands.Email
{
    public class SendInvitationEmail : EmailMessageBase
    {
        public Guid InvitationToken { get; set; }
    }
}
