﻿using System;

namespace Subscription.Messages.Commands.Email
{
    public class SendInvoicePaidEmail : EmailMessageBase
    {
        public DateTime CreateDate { get; set; }
    }
}
