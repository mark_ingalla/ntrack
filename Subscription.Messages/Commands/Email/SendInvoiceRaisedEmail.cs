﻿using System;

namespace Subscription.Messages.Commands.Email
{
    public class SendInvoiceRaisedEmail : EmailMessageBase
    {
        public DateTime CreateOnUTC { get; set; }

    }
}
