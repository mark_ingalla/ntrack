﻿
namespace Subscription.Messages.Commands.Email
{
    public class SendVerificationEmail : EmailMessageBase
    {
        public string VerificationUrl { get; set; }
    }
}
