﻿using System.Collections.Generic;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Email
{
    public abstract class EmailMessageBase : MessageBase
    {
        public Dictionary<string, string> ToEmails { get; set; }
    }
}
