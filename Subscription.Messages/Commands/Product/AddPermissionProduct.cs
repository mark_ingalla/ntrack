using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Product
{
    public class AddPermissionProduct : MessageBase
    {
        public Guid ProductId { get; set; }
        public string PermissionCode { get; set; }
        public string PermissionName { get; set; }
    }
}