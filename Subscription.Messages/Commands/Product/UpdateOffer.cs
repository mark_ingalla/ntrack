using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Product
{
    public class UpdateOffer : MessageBase
    {
        public Guid ProductId { get; set; }
        public Guid OfferKey { get; set; }
        public string Name { get; set; }
    }
}