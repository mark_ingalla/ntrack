using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Product
{
    public class ActivateProduct:MessageBase
    {
        public Guid ProductId { get; set; }
    }
}