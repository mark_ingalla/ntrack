using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Product
{
    public class AddOfferFeatures : MessageBase
    {
        public Guid ProductId { get; set; }
        public Guid OfferKey { get; set; }
        public string[] FeatureCodes { get; set; }
    }
}