using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Commands.Product
{
    public class RemoveFeature : MessageBase
    {
        public Guid ProductId { get; set; }
        public string Code { get; set; }
        public string Reason { get; set; }
    }
}