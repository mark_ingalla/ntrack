﻿using nTrack.Core.Messages;
using System;
using System.ComponentModel.DataAnnotations;

namespace Subscription.Messages.Commands
{
    public class RegisterAccount : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid UserKey { get; set; }
        public Guid OfferKey { get; set; }
        public Guid ProductId { get; set; }

        public int OfferQTY { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string CompanyName { get; set; }

        [Required]
        public string CompanyPhone { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 2)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 2)]
        public string LastName { get; set; }

        [Required]
        [RegularExpression(ValidationPatterns.EMAIL)]
        public string Email { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 8)]
        public string Password { get; set; }

        [Required]
        public string IPAddress { get; set; }

        [Required]
        public string OfferName { get; set; }

        [Required]
        public string ProductName { get; set; }

        public Guid EmailVerificationToken { get; set; }
        public string EmailVerificationPageUrl { get; set; }
    }
}