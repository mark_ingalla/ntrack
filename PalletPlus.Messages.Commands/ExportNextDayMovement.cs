﻿using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands
{
    public class ExportNextDayMovement : MessageBase
    {
        public Guid SagaId { get; set; }
    }
}