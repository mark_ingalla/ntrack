using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Invoice
{
    public class RejectMovementRecord : MessageBase
    {
        public DateTime RejectedOn { get; set; }
        public Guid RejectedBy { get; set; }

        public string MovementId { get; set; }

        public Guid InvoiceId { get; set; }
        public Guid DetailKey { get; set; }
        public DateTime MovementDate { get; set; }
        public string DocketNumber { get; set; }
        public string DestinationAccountNumber { get; set; }
        public string MovementType { get; set; }
        public string MovementDirection { get; set; }
        public string EquipmentCode { get; set; }
        public int Quantity { get; set; }
    }
}