using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Invoice
{
    public class MatchMovementRecord : MessageBase
    {
        public DateTime MatchedOn { get; set; }
        public Guid MatchedBy { get; set; }

        public string MovementId { get; set; }
        public string MovementEquipmentCode { get; set; }

        public Guid InvoiceId { get; set; }
        public Guid DetailKey { get; set; }
        public DateTime MatchedMovementDate { get; set; }
        public string MatchedDocketNumber { get; set; }
        public string MatchedDestinationId { get; set; }
        public string MatchedEquipmentCode { get; set; }
        public string MatchedMovementType { get; set; }
        public string MatchedMovementDirection { get; set; }
        public int MatchedQuantity { get; set; }
    }
}