using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Transporter
{
    public class UndeleteTransporter : MessageBase
    {
        public Guid TransporterId { get; set; }
    }
}