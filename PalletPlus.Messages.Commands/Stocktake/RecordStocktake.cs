using nTrack.Core.Messages;
using System;
using System.Collections.Generic;

namespace PalletPlus.Messages.Commands.Stocktake
{
    public class RecordStocktake : MessageBase
    {
        public Guid StocktakeId { get; set; }
        public Guid SiteId { get; set; }
        public Guid SupplierId { get; set; }
        public DateTime StocktakeDate { get; set; }
        public Dictionary<string, int> Equipments { get; set; }

    }
}