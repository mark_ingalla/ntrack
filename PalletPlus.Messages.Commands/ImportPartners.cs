﻿using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands
{
    public class ImportPartners : MessageBase
    {
        public Guid AttachmentId { get; set; }
    }
}
