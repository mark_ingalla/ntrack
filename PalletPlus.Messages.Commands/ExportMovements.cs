﻿using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands
{
    public class ExportMovements : MessageBase
    {
        public Guid SiteId { get; set; }
        public Guid SupplierId { get; set; }
    }
}
