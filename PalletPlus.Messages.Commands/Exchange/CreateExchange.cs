﻿using System;
using System.Collections.Generic;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Exchange
{
    public class CreateExchange : MessageBase
    {
        public Guid ExchangeId { get; set; }

        public Guid SiteId { get; set; }
        public Guid SupplierId { get; set; }
        public Guid PartnerId { get; set; }

        public Dictionary<string, int> EquipmentsIn { get; set; }
        public Dictionary<string, int> EquipmentsOut { get; set; }

        public DateTime EffectiveDate { get; set; }
        public string PartnerReference { get; set; }
        public string ConsignmentReference { get; set; }


    }
}
