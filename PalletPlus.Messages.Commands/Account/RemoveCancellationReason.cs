﻿using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Account
{
    public class RemoveCancellationReason : MessageBase
    {
        public Guid ReasonKey { get; set; }
        public Guid AccountId { get; set; }
    }
}
