using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Account
{
    public class RemoveSupplierFromAccount : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid SupplierId { get; set; }

    }
}