﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Account
{
    public class EditRequirements : MessageBase
    {
        public Guid AccountId { get; set; }
        public HashSet<string> RequiredFields { get; set; }
    }
}
