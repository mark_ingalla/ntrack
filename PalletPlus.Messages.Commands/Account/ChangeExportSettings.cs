﻿using nTrack.Core.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PalletPlus.Messages.Commands.Account
{
    public class ChangeExportSettings : MessageBase
    {
        public Guid AccountId { get; set; }
        public bool ExportMovementIn { get; set; }
        public bool ExportMovementOut { get; set; }
    }
}
