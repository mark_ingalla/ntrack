﻿using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Account
{
    public class AddCancellationReason : MessageBase
    {
        public Guid ReasonKey { get; set; }
        public Guid AccountId { get; set; }
        public string Reason { get; set; }
    }
}
