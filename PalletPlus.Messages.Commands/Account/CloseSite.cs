using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Account
{
    public class CloseSite : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid SiteId { get; set; }

        public CloseSite(Guid accountId, Guid siteId)
        {
            AccountId = accountId;
            SiteId = siteId;
        }
    }
}