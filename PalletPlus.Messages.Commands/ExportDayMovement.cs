﻿using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands
{
    public class ExportDayMovement : MessageBase
    {
        public Guid SagaId { get; set; }
        public Guid SiteId { get; set; }
        public Guid SupplierId { get; set; }
        public DateTime MovementDate { get; set; }
    }
}