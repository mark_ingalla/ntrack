using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Partner
{
    public class RemoveSupplierFromPartner : MessageBase
    {
        public Guid SupplierId { get; set; }
        public Guid PartnerId { get; set; }
    }
}