using System.Collections.Generic;
using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Partner
{
    public class UpdatePartner : MessageBase
    {
        public Guid PartnerId { get; set; }
        public string Name { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public bool IsPTAEnabled { get; set; }
        public List<string> Tags { get; set; }
    }
}