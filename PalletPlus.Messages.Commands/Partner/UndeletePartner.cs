using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Partner
{
    public class UndeletePartner : MessageBase
    {
        public Guid PartnerId { get; set; }
    }
}