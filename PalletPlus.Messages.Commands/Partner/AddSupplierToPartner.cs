using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Partner
{
    public class AddSupplierToPartner : MessageBase
    {
        public Guid PartnerId { get; set; }
        public Guid SupplierId { get; set; }
        public string AccountNumber { get; set; }
       // public bool EnableExchange { get; set; }
    }
}