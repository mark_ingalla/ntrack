using nTrack.Core.Messages;
using System;
using System.Collections.Generic;

namespace PalletPlus.Messages.Commands.Partner
{
    public class DisallowEquipmentToPartner : MessageBase
    {
        public Guid PartnerId { get; set; }
        public Guid SupplierId { get; set; }
        public List<string> Equipments { get; set; }
    }
}