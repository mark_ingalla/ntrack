﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Partner
{
    public class EditEnableExchange : MessageBase
    {
        public Guid SupplierId { get; set; }
        public Guid PartnerId { get; set; }
        public bool EnabledExchange { get; set; }
    }
}
