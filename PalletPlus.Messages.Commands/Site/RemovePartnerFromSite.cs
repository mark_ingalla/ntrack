using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Site
{
    public class RemovePartnerFromSite : MessageBase
    {
        public Guid SiteId { get; set; }
        public Guid PartnerId { get; set; }
    }
}