﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Site
{
    public class UpdatePTAInfo : MessageBase
    {
        public Guid SiteId { get; set; }
        public bool IsEnabled { get; set; }
        public string Suffix { get; set; }
        public int SequenceNumber { get; set; }
    }
}
