using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Site
{
    public class DeleteSite : MessageBase
    {
        public Guid SiteId { get; set; }
    }
}