using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Site
{
    public class UndeleteSite : MessageBase
    {
        public Guid SiteId { get; set; }
    }
}