using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Site
{
    public class RemoveSupplierFromSite : MessageBase
    {
        public Guid SiteId { get; set; }
        public Guid SupplierId { get; set; }

    }
}