using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Site
{
    public class UpdateSiteSupplier : MessageBase
    {
        public Guid SiteId { get; set; }
        public Guid SupplierId { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public int SequenceStartNumber { get; set; }
    }
}