using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Supplier
{
    public class UpdateSupplier : MessageBase
    {
        public Guid SupplierId { get; set; }
        public string Name { get; set; }
        public string ProviderName { get; set; }
        public string EmailAddress { get; set; }
        public bool IsPTAEnabled { get; set; }
    }
}