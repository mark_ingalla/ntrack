using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Supplier
{
    public class RemoveSupplierLocation : MessageBase
    {
        public Guid SupplierId { get; set; }
        public Guid LocationKey { get; set; }
    }
}