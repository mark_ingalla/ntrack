using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Supplier
{
    public class UndiscontinueEquipment : MessageBase
    {
        public Guid SupplierId { get; set; }
        public string EquipmentCode { get; set; }
    }
}