using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Supplier
{
    public class CreateSupplier : MessageBase
    {
        public Guid SupplierId { get; set; }
        public string Name { get; set; }
        public string ProviderName { get; set; }
        public string EmailAddress { get; set; }
        public bool IsPTAEnabled { get; set; }
    }
}