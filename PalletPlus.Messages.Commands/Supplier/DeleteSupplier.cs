using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Supplier
{
    public class DeleteSupplier : MessageBase
    {
        public Guid SupplierId { get; set; }
    }
}