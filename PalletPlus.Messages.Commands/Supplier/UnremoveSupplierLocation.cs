using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Supplier
{
    public class UnremoveSupplierLocation : MessageBase
    {
        public Guid SupplierId { get; set; }
        public Guid LocationKey { get; set; }
        public string AccountNumber { get; set; }
    }
}