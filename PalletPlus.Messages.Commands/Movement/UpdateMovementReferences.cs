using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Movement
{
    public class UpdateMovementReferences : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid MovementId { get; set; }
        public string MovementReference { get; set; }
        public string PartnerReference { get; set; }
        public string TransporterReference { get; set; }
    }
}