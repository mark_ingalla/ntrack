﻿using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Movement
{
    public class ChangeCorrectionAcceptanceStatus : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid MovementId { get; set; }
        public Guid CorrectionKey { get; set; }
        public bool Accept { get; set; }
        public Guid ClosedBy { get; set; }
    }
}
