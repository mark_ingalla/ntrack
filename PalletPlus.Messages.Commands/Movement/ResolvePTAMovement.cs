﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Movement
{
    public class ResolvePTAMovement : MessageBase
    {
        public Guid MovementId { get; set; }
        public DateTime EffectiveDateUTC { get; set; }
        public Dictionary<string, int> Equipments { get; set; }
        public string DocketNumber { get; set; }
    }
}
