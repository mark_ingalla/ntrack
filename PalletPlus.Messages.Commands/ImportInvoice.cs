using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands
{
    public class ImportInvoice : MessageBase
    {
        public Guid SupplierId { get; set; }
        public string ProviderName { get; set; }
        public Guid AttachmentId { get; set; }
    }
}