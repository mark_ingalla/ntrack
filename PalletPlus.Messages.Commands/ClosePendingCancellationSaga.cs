using System;

namespace PalletPlus.Messages.Commands
{
    public class ClosePendingCancellationSaga
    {
        public Guid MovementId { get; set; }
        public Guid CancellationKeyToDecline { get; set; }
        public ClosePendingCancellationSaga(Guid movementId, Guid cancellationKeyToDecline)
        {
            MovementId = movementId;
            CancellationKeyToDecline = cancellationKeyToDecline;
        }
    }
}