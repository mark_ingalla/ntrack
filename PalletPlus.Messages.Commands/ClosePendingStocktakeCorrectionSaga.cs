using System;

namespace PalletPlus.Messages.Commands
{
    public class ClosePendingStocktakeCorrectionSaga
    {
        public Guid StocktakeId { get; set; }
        public Guid CorrectionKeyToDecline { get; set; }

        public ClosePendingStocktakeCorrectionSaga(Guid stocktakeId, Guid correctionKey)
        {
            StocktakeId = stocktakeId;
            CorrectionKeyToDecline = correctionKey;
        }
    }
}