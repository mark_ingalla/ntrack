﻿using NServiceBus;
using PalletPlus.Messages.Commands.Invoice;

namespace PalletPlus.Domain.Handlers
{
    public class SupplierInvoiceHandler : HandlerBase,
        IHandleMessages<ReconcileMovements>,
        IHandleMessages<MatchMovementRecord>,
        IHandleMessages<RejectMovementRecord>,
        IHandleMessages<CreateTransaction>
    {

        public void Handle(ReconcileMovements message)
        {
            var import = Repository.GetById<Models.SupplierInvoice>(message.InvoiceId);
            import.ReconcileMovements(message.InvoiceDetailsToReconcile, message.MovementDetailsToReconcile);

            Save(import, message.Id);
        }

        public void Handle(MatchMovementRecord message)
        {
            var import = Repository.GetById<Models.SupplierInvoice>(message.InvoiceId);
            import.MatchRecords(message.MatchedOn, message.MatchedBy, message.MovementId, message.MovementEquipmentCode, message.DetailKey,
                message.MatchedMovementDate, message.MatchedDocketNumber, message.MatchedDestinationId,
                message.MatchedMovementType, message.MatchedMovementDirection, message.MatchedEquipmentCode, message.MatchedQuantity);

            Save(import, message.Id);
        }

        public void Handle(RejectMovementRecord message)
        {
            var import = Repository.GetById<Models.SupplierInvoice>(message.InvoiceId);
            import.RejectRecords(message.RejectedOn, message.RejectedBy, message.DetailKey);

            Save(import, message.Id);
        }

        public void Handle(CreateTransaction message)
        {
            var import = Repository.GetById<Models.SupplierInvoice>(message.InvoiceId);
            import.CreateTransaction(message.MovementId, message.DetailKey, message.CreatedOn, message.CreatedBy);
            Save(import, message.Id);
        }
    }
}
