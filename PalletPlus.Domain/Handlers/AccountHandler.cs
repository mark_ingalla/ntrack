﻿using NServiceBus;
using PalletPlus.Messages.Commands.Account;

namespace PalletPlus.Domain.Handlers
{
    public class AccountHandler : HandlerBase,
        IHandleMessages<CreateAccount>,

        IHandleMessages<AddSupplierToAccount>,
        IHandleMessages<UpdateAccountSupplier>,
        IHandleMessages<RemoveSupplierFromAccount>,
        IHandleMessages<AddCorrectionReason>,
        IHandleMessages<RemoveCorrectionReason>,
        IHandleMessages<AddCancellationReason>,
        IHandleMessages<RemoveCancellationReason>,
        IHandleMessages<SetDashboardEquipmentColor>,
        IHandleMessages<ChangeExportSettings>,
        IHandleMessages<EditRequirements>
    {
        public void Handle(CreateAccount message)
        {
            var account = new Models.Account(message.AccountId, message.CompanyName);
            Save(account, message.Id);
        }
        public void Handle(AddSupplierToAccount message)
        {
            var company = Repository.GetById<Models.Account>(message.AccountId);
            company.AddSupplierToSite(message.SupplierId, message.SupplierName, message.AccountNumber, message.Prefix, message.Suffix, message.SequenceStartNumber);
            Save(company, message.Id);
        }
        public void Handle(UpdateAccountSupplier message)
        {
            var company = Repository.GetById<Models.Account>(message.AccountId);
            company.UpdateSupplier(message.SupplierId, message.Prefix, message.Suffix, message.SequenceStartNumber);
            Save(company, message.Id);
        }
        public void Handle(RemoveSupplierFromAccount message)
        {
            var company = Repository.GetById<Models.Account>(message.AccountId);
            company.RemoveSupplierFromSite(message.SupplierId);
            Save(company, message.Id);
        }
        public void Handle(AddCorrectionReason message)
        {
            var company = Repository.GetById<Models.Account>(message.AccountId);
            company.AddCorrectionReason(message.ReasonKey, message.Reason);
            Save(company, message.Id);
        }
        public void Handle(RemoveCorrectionReason message)
        {
            var company = Repository.GetById<Models.Account>(message.AccountId);
            company.RemoveCorrectionReason(message.ReasonKey);
            Save(company, message.Id);
        }
        public void Handle(AddCancellationReason message)
        {
            var company = Repository.GetById<Models.Account>(message.AccountId);
            company.AddCancellationReason(message.ReasonKey, message.Reason);
            Save(company, message.Id);
        }
        public void Handle(RemoveCancellationReason message)
        {
            var company = Repository.GetById<Models.Account>(message.AccountId);
            company.RemoveCancellationReason(message.ReasonKey);
            Save(company, message.Id);
        }
        public void Handle(SetDashboardEquipmentColor message)
        {
            var company = Repository.GetById<Models.Account>(message.AccountId);
            company.SetEquipemntDashboardColor(message.EquipmentCode, message.EquipmentName, message.R, message.G, message.B);
            Save(company, message.Id);
        }
        public void Handle(ChangeExportSettings message)
        {
            var company = Repository.GetById<Models.Account>(message.AccountId);
            company.ChangeExportSettings(message.ExportMovementIn, message.ExportMovementOut);
            Save(company, message.Id);
        }
        public void Handle(EditRequirements message)
        {
            var company = Repository.GetById<Models.Account>(message.AccountId);
            company.EditRequirements(message.RequiredFields);
            Save(company, message.Id);
        }
    }
}