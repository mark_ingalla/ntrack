﻿using NServiceBus;
using PalletPlus.Domain.Models;
using PalletPlus.Messages.Commands.Exchange;

namespace PalletPlus.Domain.Handlers
{
    public class ExchangeHandler : HandlerBase,
        IHandleMessages<CreateExchange>
    {
        public void Handle(CreateExchange message)
        {
            var exchange = new Exchange(message.ExchangeId, message.SiteId, message.SupplierId, message.PartnerId, message.EffectiveDate,
                                        message.PartnerReference, message.ConsignmentReference,
                                        message.EquipmentsIn, message.EquipmentsOut);

            Save(exchange, exchange.Id);
        }


    }
}
