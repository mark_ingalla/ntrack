﻿using NServiceBus;
using PalletPlus.Messages.Commands.Stocktake;

namespace PalletPlus.Domain.Handlers
{
    public class StocktakeHandler : HandlerBase,
        IHandleMessages<RecordStocktake>,
        IHandleMessages<CountStock>,
        IHandleMessages<RequestStockCountCorrection>
    {
        public void Handle(RecordStocktake message)
        {
            var stocktake = new Models.Stocktake(message.StocktakeId, message.SiteId, message.SupplierId, message.StocktakeDate);
            Save(stocktake, message.Id);
        }

        public void Handle(CountStock message)
        {
            var stocktake = Repository.GetById<Models.Stocktake>(message.StocktakeId);

            stocktake.AddCount(message.CountKey, message.SiteId, message.EquipmentCode, message.Quantity);

            Save(stocktake, message.Id);
        }

        public void Handle(RequestStockCountCorrection message)
        {
            var stocktake = Repository.GetById<Models.Stocktake>(message.StocktakeId);

            stocktake.CorrectQTY(message.CorrectionKey, message.EquipmentCode, message.Quantity, message.Reason, message.UserKey);

            Save(stocktake, message.Id);
        }
    }
}