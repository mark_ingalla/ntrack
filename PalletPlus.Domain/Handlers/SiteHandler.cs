﻿using NServiceBus;
using PalletPlus.Messages.Commands.Site;
using System;

namespace PalletPlus.Domain.Handlers
{
    public class SiteHandler : HandlerBase,
        IHandleMessages<CreateSite>,
        IHandleMessages<UpdateSite>,
        IHandleMessages<DeleteSite>,
        IHandleMessages<UndeleteSite>,
        IHandleMessages<AddSupplierToSite>,
        IHandleMessages<UpdateSiteSupplier>,
        IHandleMessages<RemoveSupplierFromSite>,
        IHandleMessages<AddPartnerToSite>,
        IHandleMessages<RemovePartnerFromSite>,
        IHandleMessages<AddUserToSite>,
        IHandleMessages<RemoveUserFromSite>,
        IHandleMessages<UpdatePTAInfo>
    {
        public void Handle(CreateSite message)
        {
            var accountId = message.GetHeader("nTrack.AccountId") != null
                                ? Guid.Parse(message.GetHeader("nTrack.AccountId"))
                                : Guid.Empty;

            var site = new Models.Site(accountId, message.SiteId, message.Name,
                message.AddressLine1, message.AddressLine2, message.Suburb, message.State, message.Postcode, message.Country);

            Save(site, message.Id);
        }

        public void Handle(UpdateSite message)
        {
            var site = Repository.GetById<Models.Site>(message.SiteId);
            site.Update(message.Name,
                        message.AddressLine1, message.AddressLine2, message.Suburb, message.State, message.Postcode, message.Country);

            Save(site, message.Id);
        }

        public void Handle(DeleteSite message)
        {
            var site = Repository.GetById<Models.Site>(message.SiteId);
            site.Delete();

            Save(site, message.Id);
        }

        public void Handle(UndeleteSite message)
        {
            var site = Repository.GetById<Models.Site>(message.SiteId);
            site.Undelete();

            Save(site, message.Id);
        }

        public void Handle(AddSupplierToSite message)
        {
            var site = Repository.GetById<Models.Site>(message.SiteId);
            site.AddSupplierToSite(message.SupplierId, message.SupplierName, message.SupplierAccountNumber, message.Prefix, message.Suffix, message.SequenceNumber);
            Save(site, message.Id);
        }

        public void Handle(UpdateSiteSupplier message)
        {
            var site = Repository.GetById<Models.Site>(message.SiteId);
            site.UpdateSupplier(message.SupplierId, message.Prefix, message.Suffix, message.SequenceStartNumber);
            Save(site, message.Id);
        }

        public void Handle(RemoveSupplierFromSite message)
        {
            var site = Repository.GetById<Models.Site>(message.SiteId);
            site.RemoveSupplierFromSite(message.SupplierId);
            Save(site, message.Id);
        }

        public void Handle(AddPartnerToSite message)
        {
            var site = Repository.GetById<Models.Site>(message.SiteId);
            site.AddPartner(message.PartnerId);
            Save(site, message.Id);
        }

        public void Handle(RemovePartnerFromSite message)
        {
            var site = Repository.GetById<Models.Site>(message.SiteId);
            site.RemovePartner(message.PartnerId);
            Save(site, message.Id);
        }

        public void Handle(AddUserToSite message)
        {
            var site = Repository.GetById<Models.Site>(message.SiteId);
            site.AddUserToSite(message.UserId, message.Email, message.FullName);
            Save(site, message.Id);
        }

        public void Handle(RemoveUserFromSite message)
        {
            var site = Repository.GetById<Models.Site>(message.SiteId);
            site.RemoveUserFromSite(message.UserId);
            Save(site, message.Id);
        }

        public void Handle(UpdatePTAInfo message)
        {
            var site = Repository.GetById<Models.Site>(message.SiteId);
            site.UpdatePTAInfo(message.IsEnabled, message.Suffix, message.SequenceNumber);
            Save(site, message.Id);
        }
    }
}
