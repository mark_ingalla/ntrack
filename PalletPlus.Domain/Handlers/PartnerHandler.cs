﻿using NServiceBus;
using PalletPlus.Messages.Commands.Partner;

namespace PalletPlus.Domain.Handlers
{
    public class PartnerHandler : HandlerBase,
        IHandleMessages<CreatePartner>,
        IHandleMessages<UpdatePartner>,
        IHandleMessages<DeletePartner>,
        IHandleMessages<UndeletePartner>,
        IHandleMessages<AddSupplierToPartner>,
        IHandleMessages<RemoveSupplierFromPartner>,
        IHandleMessages<AllowEquipmentToPartner>,
        IHandleMessages<DisallowEquipmentToPartner>,
        IHandleMessages<EditEnableExchange>
    {
        public void Handle(CreatePartner message)
        {
            var partner = new Models.Partner(message.PartnerId, message.Name,
                message.AddressLine1, message.AddressLine2, message.Suburb, message.State, message.Postcode, message.Country,
                message.ContactName, message.ContactEmail, message.IsPTAEnabled, message.Tags);

            Save(partner, message.Id);
        }

        public void Handle(UpdatePartner message)
        {
            var partner = Repository.GetById<Models.Partner>(message.PartnerId);

            partner.Update(message.Name,
                message.AddressLine1, message.AddressLine2, message.Suburb, message.State, message.Postcode, message.Country,
                message.ContactName, message.ContactEmail, message.IsPTAEnabled, message.Tags);

            Save(partner, message.Id);
        }

        public void Handle(DeletePartner message)
        {
            var partner = Repository.GetById<Models.Partner>(message.PartnerId);
            partner.Delete();

            Save(partner, message.Id);
        }

        public void Handle(UndeletePartner message)
        {
            var partner = Repository.GetById<Models.Partner>(message.PartnerId);
            partner.Undelete();

            Save(partner, message.Id);
        }

        public void Handle(AddSupplierToPartner message)
        {
            var partner = Repository.GetById<Models.Partner>(message.PartnerId);
            partner.AddSupplier(message.SupplierId, message.AccountNumber);
            Save(partner, message.Id);
        }

        public void Handle(RemoveSupplierFromPartner message)
        {
            var partner = Repository.GetById<Models.Partner>(message.PartnerId);
            partner.RemoveSupplier(message.SupplierId);
            Save(partner, message.Id);
        }

        public void Handle(AllowEquipmentToPartner message)
        {
            var partner = Repository.GetById<Models.Partner>(message.PartnerId);
            foreach (var assetType in message.EquipmentCodes)
                partner.AllowEquipment(assetType, message.SupplierId);

            Save(partner, message.Id);
        }

        public void Handle(DisallowEquipmentToPartner message)
        {
            var partner = Repository.GetById<Models.Partner>(message.PartnerId);
            foreach (var assetType in message.Equipments)
                partner.DisallowEquipment(assetType, message.SupplierId);
            Save(partner, message.Id);
        }

        public void Handle(EditEnableExchange message)
        {
            var partner = Repository.GetById<Models.Partner>(message.PartnerId);
            partner.UpdateEnableExchange(message.SupplierId,message.PartnerId,message.EnabledExchange);
            Save(partner,message.Id);
        }
    }
}
