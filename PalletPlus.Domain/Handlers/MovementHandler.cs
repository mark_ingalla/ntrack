﻿using System;
using NServiceBus;
using PalletPlus.Domain.Models;
using PalletPlus.Messages.Commands.Movement;

namespace PalletPlus.Domain.Handlers
{
    public class MovementHandler : HandlerBase,
        IHandleMessages<CreateMovement>,
        IHandleMessages<UpdateMovementReferences>,
        IHandleMessages<UseTransporterMovement>,
        IHandleMessages<ResolvePTAMovement>,
        IHandleMessages<CompleteMovement>
    {
        public void Handle(CreateMovement message)
        {
            var accountId = message.GetHeader("nTrack.AccountId") != null
                                ? Guid.Parse(message.GetHeader("nTrack.AccountId"))
                                : Guid.Empty;

            TransferDirection direction;
            if (!Enum.TryParse(message.Direction, true, out direction))
                throw new Exception("Unrecognized Direction Value!");

            var movement = new Movement(message.MovementId, accountId, message.SiteId, message.DestinationId, message.SupplierId,
                message.LocationId, message.Equipments, direction, message.MovementDate, message.EffectiveDate,
                message.DocketNumber, message.DestinationReference, message.MovementReference, message.ConsignmentNote);

            Save(movement, message.Id);
        }
        public void Handle(UpdateMovementReferences message)
        {
            var movement = Repository.GetById<Models.Movement>(message.MovementId);
            movement.UpdateReferences(message.MovementReference, message.PartnerReference, message.TransporterReference);
            Save(movement, message.Id);
        }
        public void Handle(UseTransporterMovement message)
        {
            var movement = Repository.GetById<Models.Movement>(message.MovementId);
            movement.UseTransporter(message.TransporterId);
            Save(movement, message.Id);
        }
        public void Handle(ResolvePTAMovement message)
        {
            var movement = Repository.GetById<Models.Movement>(message.MovementId);
            movement.ResolvePTA(message.EffectiveDateUTC, message.Equipments, message.DocketNumber);
            Save(movement, message.Id);
        }
        public void Handle(CompleteMovement message)
        {
            var movement = Repository.GetById<Models.Movement>(message.MovementId);
            movement.CompleteMovement(message.EffectiveDateUTC, message.DocketNumber);
            Save(movement, message.Id);
        }
    }
}
