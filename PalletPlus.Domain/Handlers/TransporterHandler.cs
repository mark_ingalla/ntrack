﻿using NServiceBus;
using PalletPlus.Messages.Commands.Transporter;

namespace PalletPlus.Domain.Handlers
{
    public class TransporterHandler : HandlerBase,
        IHandleMessages<CreateTransporter>,
        IHandleMessages<UpdateTransporter>,
        IHandleMessages<DeleteTransporter>,
        IHandleMessages<UndeleteTransporter>
    {
        public void Handle(CreateTransporter message)
        {
            var transporter = new Models.Transporter(message.TransporterId, message.Name, message.AddressLine1, message.AddressLine2,
                message.Suburb, message.State, message.Postcode, message.Country, message.ContactName, message.ContactEmail);

            Save(transporter, message.Id);
        }
        public void Handle(UpdateTransporter message)
        {
            var transporter = Repository.GetById<Models.Transporter>(message.TransporterId);
            transporter.Update(message.Name, message.AddressLine1, message.AddressLine2,
                               message.Suburb, message.State, message.Postcode, message.Country, message.ContactName,
                               message.ContactEmail);

            Save(transporter, message.Id);
        }
        public void Handle(DeleteTransporter message)
        {
            var transporter = Repository.GetById<Models.Transporter>(message.TransporterId);
            transporter.Delete();

            Save(transporter, message.Id);
        }
        public void Handle(UndeleteTransporter message)
        {
            var transporter = Repository.GetById<Models.Transporter>(message.TransporterId);
            transporter.Undelete();

            Save(transporter, message.Id);
        }
    }
}
