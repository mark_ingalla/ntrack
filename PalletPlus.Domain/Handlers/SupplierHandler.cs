﻿using NServiceBus;
using PalletPlus.Messages.Commands.Supplier;

namespace PalletPlus.Domain.Handlers
{
    public class SupplierHandler : HandlerBase,
        IHandleMessages<CreateSupplier>,
        IHandleMessages<UpdateSupplier>,
        IHandleMessages<DeleteSupplier>,
        IHandleMessages<UndeleteSupplier>,
        IHandleMessages<AddEquipmentToSupplier>,
        IHandleMessages<DiscontinueEquipment>,
        IHandleMessages<UndiscontinueEquipment>,
        IHandleMessages<AddLocationToSupplier>,
        IHandleMessages<UpdateSupplierLocation>,
        IHandleMessages<RemoveSupplierLocation>,
        IHandleMessages<UnremoveSupplierLocation>
    {
        public void Handle(CreateSupplier message)
        {
            var supplier = new Models.Supplier(message.SupplierId, message.Name, message.ProviderName, message.IsPTAEnabled, message.EmailAddress);
            Save(supplier, message.Id);
        }
        public void Handle(UpdateSupplier message)
        {
            var supplier = Repository.GetById<Models.Supplier>(message.SupplierId);
            supplier.Update(message.Name, message.ProviderName, message.IsPTAEnabled, message.EmailAddress);
            Save(supplier, message.Id);
        }
        public void Handle(DeleteSupplier message)
        {
            var supplier = Repository.GetById<Models.Supplier>(message.SupplierId);
            supplier.Delete();

            Save(supplier, message.Id);
        }
        public void Handle(UndeleteSupplier message)
        {
            var supplier = Repository.GetById<Models.Supplier>(message.SupplierId);
            supplier.Undelete();

            Save(supplier, message.Id);
        }
        public void Handle(AddEquipmentToSupplier message)
        {
            var supplier = Repository.GetById<Models.Supplier>(message.SupplierId);
            supplier.AddAsset(message.EquipmentCode, message.EquipmentName);

            Save(supplier, message.Id);
        }
        public void Handle(DiscontinueEquipment message)
        {
            var supplier = Repository.GetById<Models.Supplier>(message.SupplierId);
            supplier.DiscontinueAsset(message.EquipmentCode);

            Save(supplier, message.Id);
        }

        public void Handle(UndiscontinueEquipment message)
        {
            var supplier = Repository.GetById<Models.Supplier>(message.SupplierId);
            supplier.UndiscontinueAsset(message.EquipmentCode);

            Save(supplier, message.Id);
        }

        public void Handle(AddLocationToSupplier message)
        {
            var supplier = Repository.GetById<Models.Supplier>(message.SupplierId);

            supplier.AddLocation(message.LocationKey, message.AccountNumber, message.Name, message.AddressLine1, message.AddressLine2, message.Suburb, message.State,
                message.Postcode, message.Country, message.ContactName, message.ContactEmail);

            Save(supplier, message.Id);
        }

        public void Handle(UpdateSupplierLocation message)
        {
            var supplier = Repository.GetById<Models.Supplier>(message.SupplierId);

            supplier.UpdateLocation(message.LocationKey, message.AccountNumber, message.Name, message.AddressLine1, message.AddressLine2, message.Suburb,
                message.State, message.Postcode, message.Country, message.ContactName, message.ContactEmail);

            Save(supplier, message.Id);
        }

        public void Handle(RemoveSupplierLocation message)
        {
            var supplier = Repository.GetById<Models.Supplier>(message.SupplierId);

            supplier.RemoveLocation(message.LocationKey);

            Save(supplier, message.Id);
        }

        public void Handle(UnremoveSupplierLocation message)
        {
            var supplier = Repository.GetById<Models.Supplier>(message.SupplierId);

            supplier.UnremoveLocation(message.LocationKey, message.AccountNumber);

            Save(supplier, message.Id);
        }
    }
}
