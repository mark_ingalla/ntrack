﻿using System;
using System.Collections.Generic;

namespace PalletPlus.Domain.DomainServices.Asset
{
    public class PartnerAssets
    {
        //partner id
        public Guid Id { get; set; }
        public List<Asset> Assets { get; set; }

        public PartnerAssets(Guid partnerId)
        {
            Id = partnerId;
            Assets = new List<Asset>();
        }
    }
}
