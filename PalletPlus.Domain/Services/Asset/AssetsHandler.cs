﻿using System;
using System.Linq;
using NServiceBus;
using nTrack.Core;
using PalletPlus.Messages.Events.Partner;
using PalletPlus.Messages.Events.Supplier;
using Raven.Client;
using Raven.Client.Linq;
using StructureMap.Attributes;

namespace PalletPlus.Domain.DomainServices.Asset
{
    public class AssetsHandler :
        IHandleMessages<EquipmentSupplierDiscontinued>,
        IHandleMessages<EquipmentToPartnerAllowed>,
        IHandleMessages<EquipmentToPartnerDisallowed>
    {
        [SetterProperty]
        public ICommonRepository Repository { get; set; }
        [SetterProperty]
        public IDocumentSession Session { get; set; }

        public void Handle(EquipmentSupplierDiscontinued message)
        {
            var partnersToUpdate =
                Session.Query<PartnerAssets>()
                .Where(x => x.Assets.Any(z => z.AssetCode == message.EquipmentCode && z.SupplierId == message.SupplierId)).
                    ToList();

            partnersToUpdate.ForEach(z => z.Assets.RemoveAll(x => x.AssetCode == message.EquipmentCode && x.SupplierId == message.SupplierId));

            var commitId = Guid.NewGuid();
            foreach (var partners in partnersToUpdate)
            {
                var partner = Repository.GetById<Models.Partner>(partners.Id);
                partner.DisallowEquipment(message.EquipmentCode, message.SupplierId);
                Repository.Save(partner, commitId);
            }

            Session.SaveChanges();
        }

        public void Handle(EquipmentToPartnerAllowed message)
        {
            var partner = Session.Load<PartnerAssets>(message.PartnerId);

            if (partner == null)
            {
                partner = new PartnerAssets(message.PartnerId);
                partner.Assets.Add(new Asset() { AssetCode = message.EquipmentCode, SupplierId = message.SupplierId });
                Session.Store(partner);

            }
            else
            {
                if (!partner.Assets.Any(x => x.SupplierId == message.SupplierId && x.AssetCode == message.EquipmentCode))
                    partner.Assets.Add(new Asset() { AssetCode = message.EquipmentCode, SupplierId = message.SupplierId });
            }

            Session.SaveChanges();
        }

        public void Handle(EquipmentToPartnerDisallowed message)
        {
            var partner = Session.Load<PartnerAssets>(message.PartnerId);
            partner.Assets.RemoveAll(x => x.AssetCode == message.EquipmentCode && x.SupplierId == message.SupplierId);

            Session.SaveChanges();
        }
    }
}
