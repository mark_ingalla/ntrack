using System;

namespace PalletPlus.Domain.DomainServices.Asset
{
    public class Asset
    {
        public string AssetCode { get; set; }
        public Guid SupplierId { get; set; }
    }
}