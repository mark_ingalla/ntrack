﻿using System;
using System.Collections.Generic;
using System.Linq;
using NServiceBus;
using PalletPlus.Domain.Handlers;
using PalletPlus.Domain.Indices;
using PalletPlus.Domain.Models;
using PalletPlus.Messages.Commands;
using PalletPlus.Messages.Events;
using Raven.Client;
using Raven.Client.Linq;
using StructureMap.Attributes;

namespace PalletPlus.Domain.Services
{
    public class ExportService : HandlerBase,
        IHandleMessages<ExportDayMovement>
    {
        [SetterProperty]
        public IDocumentSession Session { get; set; }


        public void Handle(ExportDayMovement message)
        {
            //get all the movement Ids for the date
            var movementIds = new List<Guid>();

            var count = 0;
            while (true)
            {
                var results = Session.Query<UnexportedMovementIndex.Result, UnexportedMovementIndex>()
                                     .Customize(x => x.WaitForNonStaleResultsAsOfNow(TimeSpan.FromSeconds(10)))
                                     .Where(x =>
                                            x.SiteId == message.SiteId &&
                                            x.SupplierId == message.SupplierId &&
                                            x.MovementDate >=
                                            new DateTime(message.MovementDate.Year, message.MovementDate.Month, message.MovementDate.Day, 0, 0, 0) &&
                                            x.MovementDate <=
                                            new DateTime(message.MovementDate.Year, message.MovementDate.Month, message.MovementDate.Day, 23, 59, 59))

                                     .OrderBy(x => x.MovementDate)
                                     .Skip(count)
                                     .Take(1024)
                                     .ToList();

                if (results.Count <= 0)
                    break;

                movementIds.AddRange(results.Select(x => x.MovementId));
                count += results.Count;
            }

            //process all domain models
            foreach (var id in movementIds)
            {
                var movement = Repository.GetById<Movement>(id);
                movement.ExportMovement();
                Save(movement, message.Id);
            }

            Bus.Publish(new DayMovementExported(message.SagaId, message.SiteId, message.SupplierId, message.MovementDate, movementIds.ToArray()));
        }
    }
}
