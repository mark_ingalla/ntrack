﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using NServiceBus;
using nTrack.Core;
using nTrack.Data.Extenstions;
using nTrack.Extension;
using PalletPlus.Data;
using Raven.Abstractions.Data;
using Raven.Client;

namespace PalletPlus.Domain.Services
{
    public class DocketNumberService
    {
        static readonly object Lock = new object();
        readonly IBus _bus;
        readonly IDocumentStore _store;

        readonly ConcurrentDictionary<string, Sequential> _sequences;

        public DocketNumberService(IBus bus, IDocumentStore store)
        {
            _bus = bus;
            _store = store;

            _sequences = new ConcurrentDictionary<string, Sequential>();
        }

        string DatabaseName()
        {
            return _bus.CurrentMessageContext.Headers[nTrackConstants.DatabaseHeaderKey];
        }

        //public string FuturePTANumber()
        //{
        //var sequenceId = "{0}/Sequences".Fill(siteId.ToStringId<Site>());

        //lock (Lock)
        //{
        //    var sequence = _sequences.GetOrAdd(sequenceId, s =>
        //        {
        //            //i don't have it cached, try load it from database
        //            using (var session = _store.OpenSession(DatabaseName()))
        //            {
        //                var _sequence = session.Load<Sequential>(sequenceId);
        //                if (_sequence == null)
        //                {
        //                    var site = session.Load<Site>(siteId);
        //                    _sequence = new Sequential {Id = sequenceId};

        //                    //how do I calculate the last number?
        //                    _sequence.LastPTA = site.PTASequenceNumber;
        //                    _sequence.PTASuffix = site.PTASuffix

        //                    session.Store(_sequence);
        //                }

        //                session.SaveChanges();
        //                return _sequence;
        //            }
        //        });


        //        sequence.LastPTA++;
        //        return "PTA{0}{1}".Fill(sequence.LastPTA.ToString("0#######"), sequence.PTASuffix);

        //}
        //}


        public string NextPTADocketNumber(Guid siteId)
        {
            Site site;
            var databseName = DatabaseName();
            using (var session = _store.OpenSession(databseName))
                site = session.Load<Site>(siteId);

            Task.Factory.StartNew(
                () =>
                _store.DatabaseCommands.ForDatabase(databseName)
                .Patch(site.IdString, new ScriptedPatchRequest { Script = @"this.PTASequenceNumber++" })).Wait();

            site.PTASequenceNumber++;
            return "PTA{0}{1}".Fill(site.PTASequenceNumber.ToString("0#######"), site.PTASuffix);






            //var site = _session.Load<Site>(siteId);
            //_session.Advanced.Evict(site);

            //string ptaDocketNumber;

            //if (site.IsPTAEnabled)
            //{
            //    ptaDocketNumber = "PTA" + site.PTASequenceNumber.ToString("0#######") + site.PTASuffix;
            //    Task.Factory.StartNew(() => _store.Patch(site.IdString, new ScriptedPatchRequest
            //    {
            //        Script = @"this.PTASequenceNumber++",
            //        Values = new Dictionary<string, object>
            //                {
            //                    {"siteId", siteId.ToStringId<Site>()}
            //                }
            //    })).Wait();
            //}
            //else
            //{
            //    throw new Exception("Site doesn't have enabled PTA process!");
            //}

            //return ptaDocketNumber;
        }
        public string GetTransferDocketNumber(Guid siteId, Guid supplierId)
        {
            Site site;
            Account account;

            var databseName = DatabaseName();
            using (var session = _store.OpenSession(databseName))
            {
                site = session.Include<Site>(x => x.AccountId).Load<Site>(siteId);
                account = session.Load<Account>(site.AccountId);
            }


            int nextNumber;
            string prefix;
            string suffix;

            if (site.Suppliers.ContainsKey(supplierId.ToStringId<Supplier>()))
            {
                var sequence = site.Suppliers[supplierId.ToStringId<Supplier>()];

                nextNumber = sequence.SequenceNumber + 1;
                prefix = sequence.Prefix;
                suffix = sequence.Suffix;

                Task.Factory.StartNew(() => _store.DatabaseCommands.ForDatabase(databseName)
                    .Patch(site.IdString, new ScriptedPatchRequest
                    {
                        Script = @"this.Suppliers[supplierId].SequenceNumber++",
                        Values = new Dictionary<string, object>
                            {
                                {"supplierId", supplierId.ToStringId<Supplier>()}
                            }
                    })).Wait();
            }
            else if (account.Suppliers.ContainsKey(supplierId.ToStringId<Supplier>()))
            {
                var sequence = account.Suppliers[supplierId.ToStringId<Supplier>()];

                nextNumber = sequence.SequenceNumber + 1;
                prefix = sequence.Prefix;
                suffix = sequence.Suffix;

                Task.Factory.StartNew(() => _store.DatabaseCommands.ForDatabase(databseName)
                    .Patch(account.IdString, new ScriptedPatchRequest
                    {
                        Script = @"this.Suppliers[supplierId].SequenceNumber++",
                        Values = new Dictionary<string, object>
                            {
                                {"supplierId", supplierId.ToStringId<Supplier>()}
                            }
                    })).Wait();
            }
            else
            {
                throw new Exception("Supplier doesn't belong to site nor company.");
            }


            var docketNumber = prefix + nextNumber.ToString("0#######") + suffix;
            return docketNumber;
        }
        public string GetNextSequentialNumber(Guid accountId)
        {
            Account account;

            var databseName = DatabaseName();
            using (var session = _store.OpenSession(databseName))
                account = session.Load<Account>(accountId);


            Task.Factory.StartNew(
                () =>
                _store.DatabaseCommands.ForDatabase(databseName)
                      .Patch(account.IdString, new ScriptedPatchRequest { Script = @"this.GenericSequenceNumber++" }))
                .Wait();

            account.GenericSequenceNumber++;
            return account.GenericSequenceNumber.ToString("0#######");
        }
        public string GetExchangeSequenceNumber(Guid siteId)
        {
            Site site;
            var databseName = DatabaseName();
            using (var session = _store.OpenSession(databseName))
                site = session.Load<Site>(siteId);


            Task.Factory.StartNew(
                () =>
                _store.DatabaseCommands.ForDatabase(databseName)
                .Patch(site.IdString, new ScriptedPatchRequest { Script = @"this.ExchangeSequenceNumber++" })).Wait();

            site.ExchangeSequenceNumber++;
            return "T{0}".Fill(site.ExchangeSequenceNumber.ToString("0#######"));
        }


    }

    internal sealed class Sequential
    {
        public string Id { get; set; }
        public int LastPTA { get; set; }
        public int LastExchange { get; set; }
        public int LastTransfer { get; set; }
        public int LastSequence { get; set; }

        public string PTASuffix { get; set; }
    }
}
