﻿
using System;
using System.IO;
using System.Linq;
using NServiceBus;
using nTrack.Extension;
using PalletPlus.Data;
using PalletPlus.Domain.Handlers;
using PalletPlus.Messages.Commands;
using PalletPlus.Services.ImportPartners;
using PalletPlus.Services.ImportPartners.Models;
using PalletPlus.Services.Suppliers;
using Raven.Client;

namespace PalletPlus.Domain.Services
{
    public class ImportService : HandlerBase,
        IHandleMessages<ImportInvoice>,
        IHandleMessages<ImportPartners>
    {
        readonly IDocumentStore _store;
        readonly IDocumentSession _session;
        readonly ISupplierIntegration[] _supplierIntegrations;

        public ImportService(IDocumentStore store, IDocumentSession session, ISupplierIntegration[] supplierIntegrations)
        {
            _store = store;
            _session = session;
            _supplierIntegrations = supplierIntegrations;
        }

        public void Handle(ImportInvoice message)
        {
            //go through and process the invoice
            var integration = _supplierIntegrations.SingleOrDefault(x => x.Name == message.ProviderName);
            if (integration == null)
                throw new Exception("Unable to find Supplier Integration Provider. {0}".Fill(message.ProviderName));


            var invoiceAttachment = _session.Load<InvoiceAttachment>(message.AttachmentId);
            var attachment = _store.DatabaseCommands.ForDatabase(Bus.GetDatabaseName()).GetAttachment(invoiceAttachment.IdString);

            byte[] fileBytes;
            using (var stream = attachment.Data.Invoke())
            using (var memStream = new MemoryStream())
            {
                stream.CopyTo(memStream);
                fileBytes = memStream.ToArray();
            }

            var result = integration.ImportInvoice(invoiceAttachment.FileName, fileBytes);

            var invoice = new Models.SupplierInvoice(message.AttachmentId, message.SupplierId, result.InvoiceNumber, result.InvoiceDate);
            foreach (var detail in result.Details)
            {
                invoice.AddDetail(detail.TransactionCode, detail.DocketNumber, detail.EquipmentCode, detail.Qty, detail.DaysHire, detail.AccountNumber,
                                  detail.AccountRef, detail.PartnerAccountNumber, detail.PartnerRef, detail.SupplierRef, detail.ConsignmentRef,
                                  detail.MovementDate, detail.EffectiveDate, detail.DehireDate);
            }

            invoice.CloseInvoice();
            Save(invoice, message.Id);
        }

        public void Handle(ImportPartners message)
        {
            var importAttachment = _session.Load<ImportPartnersAttachment>(message.AttachmentId);
            var attachment = _store.DatabaseCommands.ForDatabase(Bus.GetDatabaseName()).GetAttachment(importAttachment.IdString);

            byte[] fileBytes;
            using (var stream = attachment.Data.Invoke())
            using (var memStream = new MemoryStream())
            {
                stream.CopyTo(memStream);
                fileBytes = memStream.ToArray();
            }

            var service = new ImportPartnerService();
            ErrorLogModel errorLog;
            var results = service.ImportPartners(fileBytes, importAttachment.FileName, out errorLog);

            foreach (var result in results)
            {
                //TODO: complete the process to create partners
            }
        }
    }
}
