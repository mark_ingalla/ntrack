﻿using NServiceBus;
using PalletPlus.Messages.Events.Movement;
using PalletPlus.Messages.Events.Site;
using Raven.Client;
using StructureMap.Attributes;

namespace PalletPlus.Domain.DomainServices.MovementExportService
{
    public class MovementExportHandler :
        IHandleMessages<SiteCreated>,
        //IHandleMessages<MovementCreated>,
        IHandleMessages<MovementCancellationAccepted>,
        IHandleMessages<ExportMovementDataBuilt>
    {
        [SetterProperty]
        public IDocumentSession Session { get; set; }

        public void Handle(SiteCreated message)
        {
            var movementsToExport = new MovementsToExport(message.SiteId);
            Session.Store(movementsToExport);
            Session.SaveChanges();
        }

        public void Handle(MovementCreated message)
        {
            //NOTE: what happen to none exportable movements?
            if (message.MovementType == "IOU" || message.MovementType == "Internal")
                return;

            var movementsToExport = Session.Load<MovementsToExport>(message.SiteId);
            movementsToExport.AddMovement(message.SupplierId, message.MovementId);

            Session.Store(movementsToExport);
            Session.SaveChanges();
        }

        public void Handle(MovementCancellationAccepted message)
        {
            if (message.MovementType == "IOU" || message.MovementType == "Internal")
                return;

            var movementsToExport = Session.Load<MovementsToExport>(message.SiteId);
            movementsToExport.RemoveMovement(message.MovementId);

            Session.Store(movementsToExport);
            Session.SaveChanges();
        }

        public void Handle(ExportMovementDataBuilt message)
        {
            var movementsToExport = Session.Load<MovementsToExport>(message.SiteId);
            movementsToExport.NotExportedMovements[message.SupplierId].MovementIds.Clear();

            Session.Store(movementsToExport);
            Session.SaveChanges();
        }
    }
}
