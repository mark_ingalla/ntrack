﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PalletPlus.Domain.DomainServices.MovementExportService
{
    public class ExportDetails
    {
        public int Counter { get; set; }
        public List<Guid> MovementIds { get; set; }

        public ExportDetails()
        {
            Counter = 0;
            MovementIds = new List<Guid>();
        }
    }
}
