using System;
using System.Collections.Generic;

namespace PalletPlus.Domain.DomainServices.MovementExportService
{
    public class MovementsToExport
    {
        public MovementsToExport(Guid id)
        {
            Id = id;

            NotExportedMovements = new Dictionary<Guid, ExportDetails>();
        }

        public Guid Id { get; set; } // siteId

        public Dictionary<Guid, ExportDetails> NotExportedMovements { get; set; } //supplierId | List of movement ids

        public void AddMovement(Guid supplierId, Guid movementId)
        {
            if(!NotExportedMovements.ContainsKey(supplierId))
            {
                NotExportedMovements.Add(supplierId, new ExportDetails());
            }
            NotExportedMovements[supplierId].MovementIds.Add(movementId);
        }

        public void RemoveMovement(Guid movementId)
        {
            foreach(var supplier in NotExportedMovements.Keys)
            {
                NotExportedMovements[supplier].MovementIds.Remove(movementId);
            }
        }
    }
}