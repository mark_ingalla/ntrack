﻿using System;
using Raven.Client;
using StructureMap;
using StructureMap.Attributes;

namespace PalletPlus.Domain.DomainServices.AssetBalance
{
    public class AssetBalanceService
    {
        [SetterProperty]
        public IDocumentSession Session { get; set; }

        public AssetBalanceService()
        {
            Session = ObjectFactory.GetInstance<IDocumentSession>();
        }

        public int Balance(Guid siteId, string assetCode)
        {
            var siteAssets = Session.Load<SiteAsset>(siteId);
            return siteAssets.Balance(assetCode);
        }
    }
}