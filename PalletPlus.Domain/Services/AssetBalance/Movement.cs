using System;
using System.Collections.Generic;

namespace PalletPlus.Domain.DomainServices.AssetBalance
{
    public class Movement
    {
        public Guid MovementId { get; set; }
        public DateTime MovementDate { get; set; }
        public Models.TransferDirection Direction { get; set; }

        public Movement(Guid mId, DateTime movementDate, Models.TransferDirection direction)
        {
            MovementId = mId;
            MovementDate = movementDate;
            Direction = direction;
        }
    }
}