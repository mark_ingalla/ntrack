﻿using System;
using System.Collections.Generic;

namespace PalletPlus.Domain.DomainServices.AssetBalance
{
    public class Stocktake
    {
        public List<Asset> Assets { get; set; }
        public DateTime StocktakeDate { get; set; }

        public Stocktake(List<Asset> assets, DateTime stocktakeDate)
        {
            Assets = assets;
            StocktakeDate = stocktakeDate;
        }
    }
}
