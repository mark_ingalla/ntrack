﻿using System;
using PalletPlus.Domain.DomainServices.AssetBalance;
using Raven.Client;
using StructureMap;
using StructureMap.Attributes;

namespace PalletPlus.Domain.DomainServices.AssetBalance
{
    public class IouAssetBalanceService
    {
        [SetterProperty]
        public IDocumentSession Session { get; set; }

        public IouAssetBalanceService()
        {
            Session = ObjectFactory.GetInstance<IDocumentSession>();
        }

        public int Balance(Guid siteId, Guid supplierId, string assetCode)
        {
            var siteAssets = Session.Load<IouSiteAsset>(siteId);
            return siteAssets.Balance(assetCode);
        }
    }
}
