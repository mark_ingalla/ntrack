using NServiceBus;
using PalletPlus.Messages.Events.Movement;
using PalletPlus.Messages.Events.Site;
using PalletPlus.Messages.Events.Stocktake;
using Raven.Client;
using StructureMap.Attributes;
using System;
using System.Linq;

namespace PalletPlus.Domain.DomainServices.AssetBalance
{
    public class AssetBalanceHandler :
        //IHandleMessages<MovementCreated>,
        IHandleMessages<MovementCancellationAccepted>,
        IHandleMessages<SiteCreated>,
        IHandleMessages<CorrectionApproved>,
        IHandleMessages<PtaApproved>,
        IHandleMessages<StockCounted>,
        IHandleMessages<StocktakeCorrectionApproved>
    {
        [SetterProperty]
        public IDocumentSession Session { get; set; }

        [SetterProperty]
        public IBus Bus { get; set; }

        public AssetBalanceHandler(IDocumentSession session)
        {
            Session = session;
        }

        public void Handle(MovementCreated message)
        {
            switch (message.MovementType)
            {
                case "Iou":
                    HandleIOUMovementCreated(message);
                    break;
                case "Internal":
                    HandleInternalMovementCreated(message);
                    break;
                default:
                    HandleTransferMovementCreated(message);
                    break;
            }
        }

        private void HandleTransferMovementCreated(MovementCreated message)
        {
            var siteAsset = Session.Load<SiteAsset>(message.SiteId);

            var direction = (Models.TransferDirection)Enum.Parse(typeof(Models.TransferDirection), message.Direction);
            siteAsset.AddMovement(message.MovementId, message.MovementDate, direction);


            //TODO: take PTA in consideration
            if (direction == Models.TransferDirection.In)
                message.Equipments.ToList()
                        .ForEach(x => siteAsset.Add(Bus, message.MovementId, message.SupplierId, x.Key, x.Value));
            else
                message.Equipments.ToList()
                        .ForEach(x => siteAsset.Subtract(Bus, message.MovementId, message.SupplierId, x.Key, x.Value));


            Session.SaveChanges();

        }
        private void HandleIOUMovementCreated(MovementCreated message)
        {
            var siteAsset = Session.Load<IouSiteAsset>(message.SiteId);

            var direction = (Models.TransferDirection)Enum.Parse(typeof(Models.TransferDirection), message.Direction);
            siteAsset.AddMovement(message.MovementId, message.MovementDate, direction);

            if (direction == Models.TransferDirection.In)
                message.Equipments.ToList().ForEach(x => siteAsset.Add(Bus, message.MovementId, message.SupplierId, x.Key, x.Value));
            else
                message.Equipments.ToList().ForEach(x => siteAsset.Subtract(Bus, message.MovementId, message.SupplierId, x.Key, x.Value));

            Session.SaveChanges();
        }
        private void HandleInternalMovementCreated(MovementCreated message)
        {
            var siteA = Session.Load<SiteAsset>(message.SiteId);
            var siteB = Session.Load<SiteAsset>(message.DestinationId);

            var direction = (Models.TransferDirection)Enum.Parse(typeof(Models.TransferDirection), message.Direction);
            siteA.AddMovement(message.MovementId, message.MovementDate, direction);
            siteB.AddMovement(message.MovementId, message.MovementDate, direction);

            if (direction == Models.TransferDirection.In)
            {
                message.Equipments.ToList().ForEach(x => siteA.Add(Bus, message.MovementId, message.SupplierId, x.Key, x.Value));
                message.Equipments.ToList().ForEach(x => siteB.Subtract(Bus, message.MovementId, message.DestinationId, x.Key, x.Value));
            }
            else
            {
                message.Equipments.ToList().ForEach(x => siteB.Add(Bus, message.MovementId, message.DestinationId, x.Key, x.Value));
                message.Equipments.ToList().ForEach(x => siteA.Subtract(Bus, message.MovementId, message.SupplierId, x.Key, x.Value));
            }

            Session.SaveChanges();
        }

        public void Handle(SiteCreated message)
        {
            Session.Store(new SiteAsset(message.SiteId));
            Session.Store(new IouSiteAsset(message.SiteId));

            Session.SaveChanges();
        }
        public void Handle(MovementCancellationAccepted message)
        {
            switch (message.MovementType)
            {
                case "Iou":
                    var iouSiteAsset = Session.Load<IouSiteAsset>(message.SiteId);
                    iouSiteAsset.CancelMovement(Bus, message.MovementId);
                    Session.SaveChanges();
                    break;
                default:
                    var siteAsset = Session.Load<SiteAsset>(message.SiteId);
                    siteAsset.CancelMovement(Bus, message.MovementId);
                    Session.SaveChanges();
                    break;
            }
        }
        public void Handle(CorrectionApproved message)
        {
            switch (message.MovementType)
            {
                case "Iou":
                    var iouSiteAsset = Session.Load<IouSiteAsset>(message.SiteId);
                    iouSiteAsset.Update(Bus, message.MovementId, message.SupplierId, message.AssetCodeQuantity);
                    Session.SaveChanges();
                    break;
                default:
                    var siteAsset = Session.Load<SiteAsset>(message.SiteId);
                    siteAsset.Update(Bus, message.MovementId, message.SupplierId, message.AssetCodeQuantity);
                    Session.SaveChanges();
                    break;
            }
        }
        public void Handle(PtaApproved message)
        {
            var siteAsset = Session.Load<SiteAsset>(message.SiteId);
            siteAsset.Update(Bus, message.MovementId, message.SupplierId, message.Assets);
            Session.SaveChanges();
        }

        //added
        public void Handle(StocktakeRecorded message)
        {
            var siteAsset = Session.Load<SiteAsset>(message.SiteId);
            siteAsset.RecordStocktake(message.TakenOn);
            Session.SaveChanges();
        }
        public void Handle(StockCounted message)
        {
            var siteAsset = Session.Load<SiteAsset>(message.SiteId);
            siteAsset.RecordStocktake(Bus, message.SupplierId, message.EquipmentCode, message.Quantity);
            Session.SaveChanges();
        }
        public void Handle(StocktakeCorrectionApproved message)
        {
            var siteAsset = Session.Load<SiteAsset>(message.SiteId);
            siteAsset.RecordStocktake(Bus, message.SupplierId, message.AssetCode, message.Quantity);
            Session.SaveChanges();
        }
    }
}