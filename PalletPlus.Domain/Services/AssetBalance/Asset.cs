using System;

namespace PalletPlus.Domain.DomainServices.AssetBalance
{
    public class Asset
    {
        public Asset(Guid supplierId, string code, int qty)
        {
            SupplierId = supplierId;
            Quantity = qty;
            AssetCode = code;
        }
        public Guid SupplierId { get; set; }
        public string AssetCode { get; set; }
        public int Quantity { get; set; }
    }
}