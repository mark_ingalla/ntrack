using System;
using System.Collections.Generic;
using System.Linq;
using NServiceBus;
using PalletPlus.Domain.DomainServices.IouAssetsBalance;
using PalletPlus.Messages.Events.AssetBalance;

namespace PalletPlus.Domain.DomainServices.AssetBalance
{
    public class IouSiteAsset
    {
        public IouSiteAsset(Guid id)
        {
            Id = id;

            MovAssets = new Dictionary<Guid, List<Asset>>();
            Movements = new List<IouMovement>();
        }

        //site id
        public Guid Id { get; set; }

        public Dictionary<Guid, List<Asset>> MovAssets { get; set; }
        public List<IouMovement> Movements { get; set; }

        public void AddMovement(Guid movementId, DateTime movementDate, Models.TransferDirection direction)
        {
            MovAssets.Add(movementId, new List<Asset>());
            Movements.Add(new IouMovement(movementId, movementDate, direction));
        }

        public void Update(IBus bus, Guid movementId, Guid supplierId, Dictionary<string, int> assets)
        {
            MovAssets[movementId].Clear();

            var direction = Movements.First(x => x.MovementId == movementId).Direction;

            if (direction == Models.TransferDirection.In)
                assets.ToList().ForEach(x =>
                {
                    MovAssets[movementId].Add(new Asset(supplierId, x.Key, x.Value));
                    bus.Publish(new IouAssetBalanceUpdated(Id, x.Key, Balance(x.Key)));
                });
            else
                assets.ToList().ForEach(x =>
                {
                    MovAssets[movementId].Add(new Asset(supplierId, x.Key, x.Value * -1));
                    bus.Publish(new IouAssetBalanceUpdated(Id, x.Key, Balance(x.Key)));
                });
        }

        public void CancelMovement(IBus bus, Guid movementId)
        {
            MovAssets[movementId].ForEach(x =>
            {
                x.Quantity = 0;
                bus.Publish(new IouAssetBalanceUpdated(Id, x.AssetCode, Balance(x.AssetCode)));
            });

            MovAssets.Remove(movementId);
            Movements.RemoveAll(x => x.MovementId == movementId);
        }

        public void Add(IBus bus, Guid movementId, Guid supplierId, string code, int qty)
        {
            MovAssets[movementId].Add(new Asset(supplierId, code, qty));

            bus.Publish(new IouAssetBalanceUpdated(Id, code, Balance(code)));
        }

        public void Subtract(IBus bus, Guid movementId, Guid supplierId, string code, int qty)
        {
            MovAssets[movementId].Add(new Asset(supplierId, code, qty * -1));

            bus.Publish(new IouAssetBalanceUpdated(Id, code, Balance(code)));
        }

        public int Balance(string code)
        {
            var assets = MovAssets.SelectMany(x => x.Value).ToList();
            var assetsCount = assets.Where(x => x.AssetCode == code).Sum(x => x.Quantity);

            return assetsCount;

            //return MovAssets.Values.SelectMany(x => x).Where(x => x.EquipmentCode == code && x.SupplierId == supplierId).Sum(x => x.Quantity);
        }
    }
}