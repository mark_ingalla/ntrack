using System;

namespace PalletPlus.Domain.DomainServices.IouAssetsBalance
{
    public class IouMovement
    {
        public Guid MovementId { get; set; }
        public DateTime MovementDate { get; set; }
        public Models.TransferDirection Direction { get; set; }

        public IouMovement(Guid mId, DateTime movementDate, Models.TransferDirection direction)
        {
            MovementId = mId;
            MovementDate = movementDate;
            Direction = direction;
        }
    }
}