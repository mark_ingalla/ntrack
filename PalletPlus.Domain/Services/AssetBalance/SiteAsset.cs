using System;
using System.Collections.Generic;
using System.Linq;
using CommonDomain.Core;
using NServiceBus;
using PalletPlus.Messages.Events.AssetBalance;
using PalletPlus.Messages.Events.Stocktake;
using Raven.Client.Linq;
using Raven.Imports.Newtonsoft.Json;
using StructureMap.Attributes;

namespace PalletPlus.Domain.DomainServices.AssetBalance
{
    public class SiteAsset
    {
        public SiteAsset(Guid id)
        {
            Id = id;

            MovAssets = new Dictionary<Guid, List<Asset>>();
            Movements = new List<Movement>();
            LastStocktake = new Stocktake(new List<Asset>(), DateTime.Now.Date);
        }

        //site id
        public Guid Id { get; set; }

        public Dictionary<Guid, List<Asset>> MovAssets { get; set; }
        public List<Movement> Movements { get; set; }
        public Stocktake LastStocktake { get; set; }

        public void AddMovement(Guid movementId, DateTime movementDate, Models.TransferDirection direction)
        {
            MovAssets.Add(movementId, new List<Asset>());
            Movements.Add(new Movement(movementId, movementDate, direction));
        }

        public void CancelMovement(IBus bus, Guid movementId)
        {
            MovAssets[movementId].ForEach(x =>
                                              {
                                                  x.Quantity = 0;
                                                  bus.Publish(new TransferAssetBalanceUpdated(Id, x.AssetCode, Balance(x.AssetCode)));
                                              });
                                              
            MovAssets.Remove(movementId);
            Movements.RemoveAll(x => x.MovementId == movementId);
        }

        public void Add(IBus bus, Guid movementId, Guid supplierId, string code, int qty)
        {
            MovAssets[movementId].Add(new Asset(supplierId, code, qty));

            bus.Publish(new TransferAssetBalanceUpdated(Id, code, Balance(code)));
        }

        public void Subtract(IBus bus, Guid movementId, Guid supplierId, string code, int qty)
        {
            MovAssets[movementId].Add(new Asset(supplierId, code, qty * -1));

            bus.Publish(new TransferAssetBalanceUpdated(Id, code, Balance(code)));
        }

        public void Update(IBus bus, Guid movementId, Guid supplierId, Dictionary<string, int> assets)
        {
            MovAssets[movementId].Clear();

            var direction = Movements.First(x => x.MovementId == movementId).Direction;

            if (direction == Models.TransferDirection.In)
                assets.ToList().ForEach(x =>
                                            {
                                                MovAssets[movementId].Add(new Asset(supplierId, x.Key, x.Value));
                                                bus.Publish(new TransferAssetBalanceUpdated(Id, x.Key, Balance(x.Key)));
                                            });
            else
                assets.ToList().ForEach(x =>
                                            {
                                                MovAssets[movementId].Add(new Asset(supplierId, x.Key, x.Value*-1));
                                                bus.Publish(new TransferAssetBalanceUpdated(Id, x.Key, Balance(x.Key)));
                                            });
        }

        public void RecordStocktake(DateTime stocktakeDate)
        {
            LastStocktake.StocktakeDate = stocktakeDate;
        }

        public void RecordStocktake(IBus bus, Guid supplierId, string assetCode, int qty)
        {
            var asset = LastStocktake.Assets.FirstOrDefault(x => x.AssetCode == assetCode && x.SupplierId == supplierId);
            if (asset == null)
            {
                LastStocktake.Assets.Add(new Asset(supplierId, assetCode, qty));
            }
            else
            {
                asset.Quantity = qty;
            }

            bus.Publish(new TransferAssetBalanceUpdated(Id, assetCode, Balance(assetCode)));
        }

        public int Balance(string code)
        {
            //int stockValue = 0;
            //var stock = LastStocktake.Assets.FirstOrDefault(x => x.EquipmentCode == code);
            //if (stock != null)
            //    stockValue = stock.Quantity;

            //var movementsAfterStocktake = Movements.Where(x => x.MovementDate > LastStocktake.StocktakeDate).Select(x => x.MovementId).ToList();
            //var movementsWithAssets = MovAssets.Where(x => x.Key.In(movementsAfterStocktake)).SelectMany(x => x.Value).ToList();
            //var assetsCount = movementsWithAssets.Where(x => x.EquipmentCode == code).Sum(x => x.Quantity);

            //return assetsCount + stockValue;

            var assets = MovAssets.SelectMany(x => x.Value).ToList();
            var assetsCount = assets.Where(x => x.AssetCode == code).Sum(x => x.Quantity);

            return assetsCount;
        }
    }
}