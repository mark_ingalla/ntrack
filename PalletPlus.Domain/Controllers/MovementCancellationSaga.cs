﻿using EventStore.RavenDb;
using NServiceBus;
using NServiceBus.Saga;
using PalletPlus.Domain.infrastructure;
using PalletPlus.Messages.Commands;
using PalletPlus.Messages.Commands.Movement;
using PalletPlus.Messages.Events;
using StructureMap.Attributes;
using System;

namespace PalletPlus.Domain.Controllers
{
    public class MovementCancellationSaga : Saga<MovementCancellationSagaData>,
        IAmStartedByMessages<RequestMovementCancellation>,
        IHandleMessages<ChangeCancellationAcceptanceStatus>,
        IHandleMessages<WithdrawCancellation>,
        IHandleMessages<ClosePendingCancellationSaga>,
        IHandleMessages<ClosedPendingCancellationSaga>,
        IHandleTimeouts<CancellationTimeout>
    {
        static readonly TimeSpan DecissionTimeSpan = TimeSpan.FromHours(36);

        public override void ConfigureHowToFindSaga()
        {
            ConfigureMapping<ChangeCancellationAcceptanceStatus>(s => s.CancellationKey, m => m.CancellationKey);
            ConfigureMapping<WithdrawCancellation>(s => s.CancellationKey, m => m.CancellationKey);
            ConfigureMapping<ClosePendingCancellationSaga>(s => s.CancellationKey, m => m.CancellationKeyToDecline);
            ConfigureMapping<ClosedPendingCancellationSaga>(s => s.CancellationKey, m => m.DeclinedCancellationKey);
        }

        [SetterProperty]
        public IRepository Repository { get; set; }

        public void Handle(RequestMovementCancellation message)
        {
            Data.CancellationKey = Guid.NewGuid();

            var movement = Repository.GetById<Models.Movement>(message.MovementId);
            Data.RequestedBy = message.RequestedBy;
            Data.RequestedOn = message.RequestedOn;
            Data.Reason = message.Reason;
            Data.MovementId = message.MovementId;

            var pendingCancellation = movement.PendingCancellation;

            if (pendingCancellation != null)
                Bus.Send(new ClosePendingCancellationSaga(message.MovementId, pendingCancellation.CancellationKey));
            else
            {
                movement.IssueMovementCancellation(Data.CancellationKey, message.RequestedBy, message.RequestedOn, message.Reason);
                Repository.Save(movement, message.Id, Bus);
                RequestUtcTimeout<CancellationTimeout>(DecissionTimeSpan);
            }
        }
        public void Handle(ChangeCancellationAcceptanceStatus message)
        {
            var movement = Repository.GetById<Models.Movement>(message.MovementId);

            if (message.Accept)
                movement.AcceptMovementCancellation(message.CancellationKey, message.ClosedBy);
            else
                movement.DeclineMovementCancellation(message.CancellationKey, message.ClosedBy);

            Repository.Save(movement, message.Id, Bus);
            MarkAsComplete();
        }
        public void Handle(WithdrawCancellation message)
        {
            var movement = Repository.GetById<Models.Movement>(message.MovementId);

            movement.WithdrawCancellation(message.MovementId, Data.CancellationKey);
            Repository.Save(movement, Guid.NewGuid(), Bus);
            MarkAsComplete();
        }
        public void Handle(ClosePendingCancellationSaga message)
        {
            var movement = Repository.GetById<Models.Movement>(message.MovementId);

            movement.DeclineMovementCancellation(message.CancellationKeyToDecline, Guid.Empty);
            Repository.Save(movement, Guid.NewGuid(), Bus);

            Bus.Publish(new ClosedPendingCancellationSaga(message.MovementId, message.CancellationKeyToDecline));
            MarkAsComplete();
        }
        public void Handle(ClosedPendingCancellationSaga message)
        {
            var movement = Repository.GetById<Models.Movement>(message.MovementId);

            movement.IssueMovementCancellation(Data.CancellationKey, Data.RequestedBy, Data.RequestedOn, Data.Reason);
            Repository.Save(movement, Guid.NewGuid(), Bus);

            RequestUtcTimeout<CancellationTimeout>(DecissionTimeSpan);
        }

        public void Timeout(CancellationTimeout state)
        {
            var movement = Repository.GetById<Models.Movement>(Data.MovementId);
            movement.DeclineMovementCancellation(Data.CancellationKey, Guid.Empty);

            Repository.Save(movement, state.Id, Bus);
            MarkAsComplete();
        }
    }

    public class MovementCancellationSagaData : ISagaEntity
    {
        public Guid Id { get; set; }
        public string Originator { get; set; }
        public string OriginalMessageId { get; set; }

        [Unique]
        public Guid CancellationKey { get; set; }
        public Guid MovementId { get; set; }
        public string Reason { get; set; }

        public Guid RequestedBy { get; set; }
        public DateTime RequestedOn { get; set; }
    }
}
