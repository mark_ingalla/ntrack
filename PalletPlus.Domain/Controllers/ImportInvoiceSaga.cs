﻿using System;
using NServiceBus.Saga;
using PalletPlus.Messages.Commands;
using Raven.Client;
using StructureMap.Attributes;

namespace PalletPlus.Domain.Controllers
{
    public class ImportInvoiceSaga : Saga<ImportInvoiceSagaData>,
        IAmStartedByMessages<ImportInvoice>,
        IHandleTimeouts<ImportInvoiceTimeout>
    {
        [SetterProperty]
        public IDocumentStore Store { get; set; }

        public void Handle(ImportInvoice message)
        {
            Data.SupplierId = message.SupplierId;
            Data.AttachmentId = message.InvoiceAttachmentId;



            RequestUtcTimeout<ImportInvoiceTimeout>(TimeSpan.FromHours(2));
        }

        public void Timeout(ImportInvoiceTimeout state)
        {
            //TODO: tell someone something failed

            MarkAsComplete();
        }
    }

    public class ImportInvoiceSagaData : ISagaEntity
    {
        public Guid Id { get; set; }
        public string Originator { get; set; }
        public string OriginalMessageId { get; set; }

        public Guid SupplierId { get; set; }
        public Guid AttachmentId { get; set; }
    }

    public class ImportInvoiceTimeout { }
}
