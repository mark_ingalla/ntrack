﻿using EventStore.RavenDb;
using NServiceBus;
using NServiceBus.Saga;
using PalletPlus.Domain.infrastructure;
using PalletPlus.Messages.Commands;
using PalletPlus.Messages.Commands.Stocktake;
using PalletPlus.Messages.Events;
using StructureMap.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PalletPlus.Domain.Controllers
{
    public class StocktakeCorrectionSaga : Saga<StocktakeCorrectionSagaData>,
        IAmStartedByMessages<RequestStockCountCorrection>,
        IHandleMessages<ChangeStocktakeCorrectionAcceptance>,
        IHandleMessages<ClosePendingStocktakeCorrectionSaga>,
        IHandleMessages<ClosedPendingStocktakeCorrectionSaga>,
        IHandleTimeouts<CorrectionTimeout>
    {
        static readonly TimeSpan DecissionTimeSpan = TimeSpan.FromHours(36);

        [SetterProperty]
        public IRepository Repository { get; set; }

        public override void ConfigureHowToFindSaga()
        {
            ConfigureMapping<ChangeStocktakeCorrectionAcceptance>(s => s.CorrectionKey, m => m.CorrectionKey);
            ConfigureMapping<ClosePendingStocktakeCorrectionSaga>(s => s.CorrectionKey, m => m.CorrectionKeyToDecline);
            ConfigureMapping<ClosedPendingStocktakeCorrectionSaga>(s => s.CorrectionKey, m => m.DeclinedCorrectionKey);
        }

        public void Handle(RequestStockCountCorrection message)
        {
            Data.CorrectionKey = message.CorrectionKey;
            Data.Id = Guid.NewGuid();

            var stocktake = Repository.GetById<Models.Stocktake>(message.StocktakeId);

            Data.Asset = new KeyValuePair<string, int>(message.EquipmentCode, message.Quantity);
            Data.Reason = message.Reason;
            Data.StocktakeId = message.StocktakeId;
            Data.UserId = message.UserKey;

            var pendingCorrection =
                stocktake.Counts.FirstOrDefault(x => x.AssetCode == message.EquipmentCode).PendingCorrection;

            if (pendingCorrection != null)
                Bus.Send(new ClosePendingStocktakeCorrectionSaga(message.StocktakeId, pendingCorrection.CorrectionKey));
            else
            {
                stocktake.CorrectQTY(Data.CorrectionKey, message.EquipmentCode, message.Quantity, message.Reason, message.UserKey);
                Repository.Save(stocktake, message.Id, Bus);
                RequestUtcTimeout<CorrectionTimeout>(DecissionTimeSpan);
            }
        }
        public void Handle(ChangeStocktakeCorrectionAcceptance message)
        {
            var stocktake = Repository.GetById<Models.Stocktake>(Data.StocktakeId);

            if (message.Accept)
                stocktake.ApproveCorrection(message.CorrectionKey, message.ClosedBy);
            else
                stocktake.DeclineCorrection(message.CorrectionKey, message.ClosedBy);

            Repository.Save(stocktake, message.Id, Bus);
            MarkAsComplete();
        }
        public void Handle(ClosePendingStocktakeCorrectionSaga message)
        {
            var stocktake = Repository.GetById<Models.Stocktake>(Data.StocktakeId);

            stocktake.DeclineCorrection(Data.CorrectionKey, Guid.Empty);
            Repository.Save(stocktake, Guid.NewGuid(), Bus);
            Bus.Send(new ClosePendingStocktakeCorrectionSaga(message.StocktakeId, message.CorrectionKeyToDecline));
            MarkAsComplete();
        }
        public void Handle(ClosedPendingStocktakeCorrectionSaga message)
        {
            var stocktake = Repository.GetById<Models.Stocktake>(Data.StocktakeId);

            stocktake.CorrectQTY(Data.CorrectionKey, Data.Asset.Key, Data.Asset.Value, Data.Reason, Data.UserId);
            Repository.Save(stocktake, Guid.NewGuid(), Bus);
            RequestUtcTimeout<CorrectionTimeout>(DecissionTimeSpan);
        }

        public void Timeout(CorrectionTimeout state)
        {
            var stocktake = Repository.GetById<Models.Stocktake>(Data.StocktakeId);

            stocktake.DeclineCorrection(Data.CorrectionKey, Guid.Empty);
            Repository.Save(stocktake, state.Id, Bus);
            MarkAsComplete();
        }
    }

    public class StocktakeCorrectionSagaData : ISagaEntity
    {
        public Guid Id { get; set; }
        public string Originator { get; set; }
        public string OriginalMessageId { get; set; }

        [Unique]
        public Guid CorrectionKey { get; set; }

        public Guid StocktakeId { get; set; }

        public KeyValuePair<string, int> Asset { get; set; }

        public Guid UserId { get; set; }

        public string Reason { get; set; }
        //public Guid StocktakeRecordKey { get; set; }
        //public int Quantity { get; set; }
    }
}
