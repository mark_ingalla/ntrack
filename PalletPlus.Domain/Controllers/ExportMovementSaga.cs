﻿
using System;
using System.Linq;
using System.Transactions;
using NServiceBus;
using NServiceBus.Saga;
using nTrack.Core;
using PalletPlus.Data;
using PalletPlus.Domain.Indices;
using PalletPlus.Messages.Commands;
using PalletPlus.Messages.Events;
using Raven.Abstractions.Data;
using Raven.Client;
using Raven.Json.Linq;
using StructureMap.Attributes;

namespace PalletPlus.Domain.Controllers
{
    public class ExportMovementSaga : Saga<ExportMovementSagaData>,
        IAmStartedByMessages<ExportMovements>,
        IHandleMessages<ExportNextDayMovement>,
        IHandleTimeouts<ExportMovementTimeout>,
        IHandleMessages<DayMovementExported>
    {
        [SetterProperty]
        public IDocumentStore Store { get; set; }

        public override void ConfigureHowToFindSaga()
        {
            ConfigureMapping<ExportNextDayMovement>(x => x.Id, m => m.SagaId);
            ConfigureMapping<DayMovementExported>(x => x.Id, m => m.SagaId);
        }

        public void Handle(ExportMovements message)
        {
            Data.SiteId = message.SiteId;
            Data.SupplierId = message.SupplierId;
            Data.ClientDatabase = Bus.CurrentMessageContext.Headers[nTrackConstants.DatabaseHeaderKey];


            //check there is no pending export for this site and supplier
            if (Store.DatabaseCommands.ForDatabase(Data.ClientDatabase).Head(PendingExport.GenerateId(Data.SiteId, Data.SupplierId)) != null)
            {
                //there is an existing Saga
                MarkAsComplete();
                return;
            }

            var pendingExport = new PendingExport
                {
                    Id = PendingExport.GenerateId(Data.SiteId, Data.SupplierId),
                    SiteId = Data.SiteId,
                    SupplierId = Data.SupplierId,
                    StartedOn = DateTime.Now
                };

            using (var session = Store.OpenSession(Data.ClientDatabase))
            {
                session.Store(pendingExport);
                session.SaveChanges();
            }


            //send to itself the first command to start processing the movements
            Bus.Send<ExportNextDayMovement>(x => x.SagaId = Data.Id);

            //request a timeout callback to send error report if process took too long (2 hours)
            RequestUtcTimeout<ExportMovementTimeout>(TimeSpan.FromHours(2));
        }
        public void Handle(ExportNextDayMovement message)
        {
            //get the earliest day
            using (var session = Store.OpenSession(Data.ClientDatabase))
            {
                var result = session.Query<UnexportedMovementIndex.Result, UnexportedMovementIndex>()
                                    .Customize(x => x.WaitForNonStaleResultsAsOfNow(TimeSpan.FromSeconds(10)))
                                    .Where(x => x.SiteId == Data.SiteId && x.SupplierId == Data.SupplierId)
                                    .Take(1)
                                    .OrderBy(x => x.MovementDate)
                                    .FirstOrDefault();

                if (result != null)
                {
                    Bus.Send<ExportDayMovement>(x =>
                        {
                            x.SagaId = Data.Id;
                            x.SiteId = Data.SiteId;
                            x.SupplierId = Data.SupplierId;
                            x.MovementDate = result.MovementDate;
                        });
                }
                else
                {
                    //TODO: email someone that the export is completed
                    using (var t = new TransactionScope(TransactionScopeOption.Suppress))
                    {
                        Store.DatabaseCommands.ForDatabase(Data.ClientDatabase).Delete(PendingExport.GenerateId(Data.SiteId, Data.SupplierId), null);
                        t.Complete();
                    }
                    MarkAsComplete();
                }
            }
        }
        public void Handle(DayMovementExported message)
        {
            using (var t = new TransactionScope(TransactionScopeOption.Suppress))
            {
                Store.DatabaseCommands.ForDatabase(Data.ClientDatabase)
                     .Patch(PendingExport.GenerateId(Data.SiteId, Data.SupplierId),
                            new[]
                                {
                                    new PatchRequest
                                        {
                                            Type = PatchCommandType.Inc,
                                            Name = "TotalExported",
                                            Value = new RavenJValue(message.TotalMovements)
                                        }
                                });
                t.Complete();
            }

            //send to itself the first command to start processing the movements
            Bus.Send<ExportNextDayMovement>(x => x.SagaId = Data.Id);
        }

        public void Timeout(ExportMovementTimeout state)
        {
            //TODO: send email to support with details of the saga to check why this happened

            Store.DatabaseCommands.ForDatabase(Data.ClientDatabase).Delete(PendingExport.GenerateId(Data.SiteId, Data.SupplierId), null);
            MarkAsComplete();
        }
    }

    public class ExportMovementSagaData : ISagaEntity
    {
        public Guid Id { get; set; }
        public string Originator { get; set; }
        public string OriginalMessageId { get; set; }

        public Guid SiteId { get; set; }
        public Guid SupplierId { get; set; }
        public string ClientDatabase { get; set; }
    }

    public class ExportMovementTimeout { }
}
