﻿using System;
using System.Collections.Generic;
using EventStore.RavenDb;
using NServiceBus;
using NServiceBus.Saga;
using PalletPlus.Domain.infrastructure;
using PalletPlus.Messages.Commands;
using PalletPlus.Messages.Commands.Movement;
using PalletPlus.Messages.Events;
using StructureMap.Attributes;

namespace PalletPlus.Domain.Controllers
{
    public class MovementCorrectionSaga : Saga<MovementCorrectionSagaData>,
        IAmStartedByMessages<IssueQuantityCorrection>,
        IHandleMessages<ChangeCorrectionAcceptanceStatus>,
        IHandleMessages<WithdrawQuantityCorrection>,
        IHandleMessages<ClosePendingCorrectionSaga>,
        IHandleMessages<ClosedPendingCorrectionSaga>,
        IHandleTimeouts<CorrectionTimeout>
    {
        static readonly TimeSpan DecissionTimeSpan = TimeSpan.FromHours(36);

        [SetterProperty]
        public IRepository Repository { get; set; }

        public override void ConfigureHowToFindSaga()
        {
            ConfigureMapping<ChangeCorrectionAcceptanceStatus>(s => s.CorrectionKey, m => m.CorrectionKey);
            ConfigureMapping<WithdrawQuantityCorrection>(s => s.CorrectionKey, m => m.CorrectionKey);
            ConfigureMapping<ClosedPendingCorrectionSaga>(s => s.CorrectionKey, m => m.DeclinedCorrectionKey);
            ConfigureMapping<ClosePendingCorrectionSaga>(s => s.CorrectionKey, m => m.CorrectionKeyToDecline);
        }

        public void Handle(IssueQuantityCorrection message)
        {
            Data.CorrectionKey = Guid.NewGuid();

            var movement = Repository.GetById<Models.Movement>(message.MovementId);
            Data.Assets = message.EquipmentQuantity;
            Data.RequestedBy = message.RequestedBy;
            Data.RequestedOn = message.RequestedOn;
            Data.Reason = message.Reason;
            Data.MovementId = message.MovementId;
            Data.MovementDate = message.NewMovementDate;
            Data.EffectiveDate = message.NewEffectiveDate;

            var pendingCorrection = movement.PendingCorrection;

            if (pendingCorrection != null)
                Bus.Send(new ClosePendingCorrectionSaga(message.MovementId, pendingCorrection.CorrectionKey));
            else
            {
                movement.IssueQuantityCorrection(Data.CorrectionKey, message.RequestedBy, message.RequestedOn, message.EquipmentQuantity,
                    message.NewMovementDate, message.NewEffectiveDate, message.Reason);
                Repository.Save(movement, message.Id, Bus);
                RequestUtcTimeout<CorrectionTimeout>(DecissionTimeSpan);
            }
        }
        public void Handle(ChangeCorrectionAcceptanceStatus message)
        {
            var movement = Repository.GetById<Models.Movement>(message.MovementId);

            if (message.Accept)
                movement.ApproveCorrection(message.CorrectionKey, message.ClosedBy);
            else
                movement.DeclineCorrection(message.CorrectionKey, message.ClosedBy);

            Repository.Save(movement, message.Id, Bus);
            MarkAsComplete();
        }
        public void Handle(WithdrawQuantityCorrection message)
        {
            var movement = Repository.GetById<Models.Movement>(message.MovementId);

            movement.WithdrawCorrection(message.MovementId, Data.CorrectionKey);
            Repository.Save(movement, Guid.NewGuid(), Bus);
            MarkAsComplete();
        }
        public void Handle(ClosedPendingCorrectionSaga message)
        {
            var movement = Repository.GetById<Models.Movement>(message.MovementId);

            movement.IssueQuantityCorrection(Data.CorrectionKey, Data.RequestedBy, Data.RequestedOn, Data.Assets, Data.MovementDate, Data.EffectiveDate, Data.Reason);
            Repository.Save(movement, Guid.NewGuid(), Bus);
            RequestUtcTimeout<CorrectionTimeout>(DecissionTimeSpan);
        }
        public void Handle(ClosePendingCorrectionSaga message)
        {
            var movement = Repository.GetById<Models.Movement>(message.MovementId);

            movement.DeclineCorrection(message.CorrectionKeyToDecline, Guid.Empty);
            Repository.Save(movement, Guid.NewGuid(), Bus);
            Bus.Publish(new ClosedPendingCorrectionSaga(message.MovementId, message.CorrectionKeyToDecline));
            MarkAsComplete();
        }

        public void Timeout(CorrectionTimeout state)
        {
            var movement = Repository.GetById<Models.Movement>(Data.MovementId);

            movement.DeclineCorrection(Data.CorrectionKey, Guid.Empty);
            Repository.Save(movement, state.Id, Bus);
            MarkAsComplete();
        }
    }

    public class MovementCorrectionSagaData : ISagaEntity
    {
        public Guid Id { get; set; }
        public string Originator { get; set; }
        public string OriginalMessageId { get; set; }
        public Guid RequestedBy { get; set; }
        public DateTime RequestedOn { get; set; }
        public string Reason { get; set; }

        [Unique]
        public Guid CorrectionKey { get; set; }

        public Guid MovementId { get; set; }

        public Dictionary<string, int> Assets { get; set; }

        public DateTime MovementDate { get; set; }

        public DateTime EffectiveDate { get; set; }
    }
}