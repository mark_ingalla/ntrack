﻿using EventStore.RavenDb;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;
using System;
using System.Linq;

namespace PalletPlus.Domain.Indices
{
    public class UnexportedMovementIndex : AbstractIndexCreationTask<EventStream, UnexportedMovementIndex.Result>
    {
        public class Result
        {
            public Guid MovementId { get; set; }
            public Guid SiteId { get; set; }
            public Guid SupplierId { get; set; }
            public string MovementType { get; set; }
            public string Direction { get; set; }
            public bool IsPending { get; set; }
            public DateTime MovementDate { get; set; }
            public int Day { get; set; }
            public int Month { get; set; }
            public int Year { get; set; }

        }

        public UnexportedMovementIndex()
        {
            Map = streams => from stream in streams.Where(x => x.AggregateType == "PalletPlus.Domain.Models.Movement")
                             let snapshot = (Models.Movement)stream.Snapshot
                             where !snapshot.IsExported
                             select new
                                        {
                                            MovementId = snapshot.Id,
                                            SiteId = snapshot.SiteId,
                                            SupplierId = snapshot.SupplierId,
                                            MovementType = snapshot.MovementTypeValue,
                                            Direction = snapshot.Direction,
                                            IsPending = snapshot.IsPending,
                                            MovementDate = snapshot.MovementDate,
                                            Day = snapshot.MovementDate.Day,
                                            Month = snapshot.MovementDate.Month,
                                            Year = snapshot.MovementDate.Year
                                        };


            Reduce = results => from r in results
                                group r by r.MovementId
                                    into g
                                    select new Result
                                               {
                                                   MovementId = g.Key,
                                                   SiteId = g.Select(x => x.SiteId).FirstOrDefault(),
                                                   SupplierId = g.Select(x => x.SupplierId).FirstOrDefault(),
                                                   MovementType = g.Select(x => x.MovementType).FirstOrDefault(),
                                                   Direction = g.Select(x => x.Direction).FirstOrDefault(),
                                                   IsPending = g.Select(x => x.IsPending).FirstOrDefault(),
                                                   MovementDate = g.Select(x => x.MovementDate).FirstOrDefault(),
                                                   Day = g.Select(x => x.Day).FirstOrDefault(),
                                                   Month = g.Select(x => x.Month).FirstOrDefault(),
                                                   Year = g.Select(x => x.Year).FirstOrDefault(),
                                               };

            StoreAllFields(FieldStorage.Yes);
        }
    }
}
