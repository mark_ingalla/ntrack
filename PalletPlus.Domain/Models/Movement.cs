﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonDomain.Core;
using PalletPlus.Domain.infrastructure;
using PalletPlus.Domain.Services;
using PalletPlus.Domain.Validation;
using PalletPlus.Messages.Events.Movement;
using StructureMap;

namespace PalletPlus.Domain.Models
{
    public class Movement : AggregateBase
    {
        public Guid SupplierId { get; set; }
        public Guid SiteId { get; set; }
        public Dictionary<string, int> Equipment;
        public Cancellation PendingCancellation { get; set; }
        public List<Cancellation> HistoryOfCancellations { get; set; }

        //true - accepted, false - declined, null - waiting for ack
        public Correction PendingCorrection { get; private set; }
        public List<Correction> HistoryOfCorrections { get; private set; }

        public bool IsPendingPta { get; set; }
        public bool IsPending { get; set; }
        public bool IsExported { get; set; }
        public string MovementTypeValue { get; set; }
        public string Direction { get; set; }
        public DateTime MovementDate { get; set; }
        public string DocketNumber { get; set; }


        public Movement() { }

        public Movement(Guid movementId, Guid accountId, Guid siteId, Guid destinationId, Guid supplierId, Guid? locationId,
                        Dictionary<string, int> equipments, TransferDirection direction, DateTime movementDate, DateTime effectiveDate,
                        string docketNumber, string destinationReference, string movementReference, string consignmentNote)
        {


            #region Code


            var isInternal = new IsInternalMovement(accountId, siteId, destinationId, supplierId);
            MovementType movementType = isInternal.IsSatisfiedBy(this) ? MovementType.Internal : MovementType.Transfer;
            var destinationIsASupplier = new SupplierExists(destinationId);
            var destinationIsAPartner = new PartnerExists(destinationId);
            var destinationIsASite = new SiteExists(destinationId);

            var isSupplierEnabledInSite = new IsSupplierEnabledInSite(siteId, accountId, supplierId);
            if (!isSupplierEnabledInSite.IsSatisfiedBy(this))
                throw new Exception("Equipment supplier is not associated with site.");

            string destinationKind;

            bool isPTAMovement = false;

            //Movement Site <=> Site
            if (movementType == MovementType.Internal || (movementType == MovementType.Transfer && destinationIsASite.IsSatisfiedBy(this)))
            {
                destinationKind = "Site";
            }
            //Movement Site <=> Supplier
            else if (movementType == MovementType.Transfer && destinationIsASupplier.IsSatisfiedBy(this))
            {
                ISpecification<Movement> specs = new AreAssetsEnabledForSupplier(supplierId, equipments.Keys.ToList());
                specs.And(new SupplierLocationExists(supplierId, locationId ?? Guid.Empty));

                if (!specs.IsSatisfiedBy(this))
                    throw new Exception("Impossible to create movement");

                ISpecification<Movement> isPTA = new IsSupplierEnabledPTA(siteId, supplierId);

                if (isPTA.IsSatisfiedBy(this))
                    isPTAMovement = true;

                destinationKind = "Supplier";
            }
            //Movement Site <=> Partner
            else if (movementType == MovementType.Transfer && destinationIsAPartner.IsSatisfiedBy(this))
            {
                ISpecification<Movement> specs = new AreAssetsEnabledForPartner(destinationId, equipments.Keys.ToList(), supplierId);
                specs.And(new IsMovementWithPartnerPossible(siteId, destinationId));
                if (!specs.IsSatisfiedBy(this))
                    throw new Exception("Impossible to create movement");

                ISpecification<Movement> isPTA = new IsPartnerEnabledPTA(siteId, destinationId);

                if (isPTA.IsSatisfiedBy(this))
                    isPTAMovement = true;

                destinationKind = "Partner";
            }
            else
            {
                throw new Exception("Impossible to create movement");
            }


            #endregion

            if (string.IsNullOrEmpty(docketNumber) && direction != TransferDirection.In)
            {
                //TODO: determine if the destination require PTA support or not before issuing a docket number
                var service = ObjectFactory.GetInstance<DocketNumberService>();

                if (isPTAMovement)
                {
                    docketNumber = service.NextPTADocketNumber(siteId);
                }
                else
                {
                    switch (movementType)
                    {
                        case MovementType.Transfer:
                            docketNumber = service.GetTransferDocketNumber(siteId, supplierId);
                            break;
                        case MovementType.Internal:
                            docketNumber = service.GetNextSequentialNumber(accountId);
                            break;
                    }
                }
            }
            else
            {
                isPTAMovement = false;
            }

            docketNumber = !string.IsNullOrEmpty(docketNumber) ? docketNumber.ToUpper() : docketNumber;

            bool isNotComplete = string.IsNullOrEmpty(docketNumber) && direction == TransferDirection.In;

            RaiseEvent(new MovementCreated(movementId, siteId, destinationId, supplierId, locationId,
                                           destinationKind, movementType.ToString(), direction.ToString(), movementDate,
                                           effectiveDate,
                                           equipments, docketNumber, movementReference, destinationReference,
                                           consignmentNote, isPTAMovement, isNotComplete));
        }

        public void IssueQuantityCorrection(Guid correctionKey, Guid requestedBy, DateTime requestedOn, Dictionary<string, int> assetCodeQuantity,
            DateTime newMovementDate, DateTime newEffectiveDate, string reason)
        {
            //asset must exists
            //qty > 0
            //issue correction request
            //each correction request will have a key

            if (assetCodeQuantity == null || assetCodeQuantity.Keys.Count == 0)
                throw new Exception("No changes detected");

            if (PendingCancellation != null && PendingCancellation.Status != null && PendingCancellation.Status.Value)
                throw new Exception("Movement cancelled");

            if (PendingCancellation != null && PendingCancellation.Status == null)
                throw new Exception("Movement has pending cancellation");

            assetCodeQuantity.Keys.ToList().ForEach(assetCode =>
                                                        {
                                                            if (!Equipment.ContainsKey(assetCode))
                                                                throw new Exception("There's no asset with this code in the movement");
                                                        });

            assetCodeQuantity.Values.ToList().ForEach(quantity =>
                                                          {
                                                              if (quantity <= 0)
                                                                  throw new Exception("Quantity should be positive");
                                                          });

            RaiseEvent(new CorrectionRequestIssued(Id, correctionKey, requestedBy, requestedOn, assetCodeQuantity, newMovementDate, newEffectiveDate, reason));
        }
        public void WithdrawCorrection(Guid movementId, Guid correctionKey)
        {
            if (PendingCorrection == null)
                throw new Exception("Correction already withdrawn or processed.");

            RaiseEvent(new QuantityCorrectionWithdrawn(Id, correctionKey));
        }
        public void ApproveCorrection(Guid correctionKey, Guid closedBy)
        {
            if (PendingCancellation != null && PendingCancellation.Status.HasValue && PendingCancellation.Status.Value)
                throw new Exception("Movement already cancelled");

            if (PendingCorrection == null)
                throw new Exception("No pending correction");

            if (PendingCorrection.CorrectionKey != correctionKey)
                throw new Exception("No correction with this key");

            //there is a saga that will manage the approval/reject
            RaiseEvent(new CorrectionApproved(SiteId, Id, correctionKey, SupplierId, closedBy, MovementTypeValue, PendingCorrection.AssetCodeQuantity,
                PendingCorrection.Date, PendingCorrection.EffectiveDate));
        }
        public void DeclineCorrection(Guid correctionKey, Guid closedBy)
        {
            if (PendingCancellation != null && PendingCancellation.Status.HasValue && PendingCancellation.Status.Value)
                throw new Exception("Movement already cancelled");

            if (PendingCorrection == null)
                throw new Exception("No pending correction");

            if (PendingCorrection.CorrectionKey != correctionKey)
                throw new Exception("No correction with this key");

            RaiseEvent(new CorrectionDeclined(Id, correctionKey, closedBy));
        }
        public void UpdateReferences(string movementReference, string partnerReference, string transporterReference)
        {
            RaiseEvent(new ReferencesUpdated(Id, movementReference, partnerReference, transporterReference));
        }
        public void IssueMovementCancellation(Guid cancellationKey, Guid requestedBy, DateTime requestedOn, string reason)
        {
            if (PendingCancellation != null && PendingCancellation.Status.HasValue && PendingCancellation.Status.Value)
                throw new Exception("Movement already cancelled");

            //canceling movement
            RaiseEvent(new MovementCancellationIssued(Id, cancellationKey, requestedOn, requestedBy, reason));
        }
        public void WithdrawCancellation(Guid movementId, Guid correctionKey)
        {
            if (PendingCancellation == null)
                throw new Exception("Cancellation already withdrawn or processed.");

            RaiseEvent(new CancellationWithdrawn(Id, correctionKey));
        }
        public void AcceptMovementCancellation(Guid cancellationKey, Guid closedBy)
        {
            if (PendingCancellation == null)
                throw new Exception("No pending cancellation");

            if (PendingCancellation != null && PendingCancellation.Status.HasValue && PendingCancellation.Status.Value)
                throw new Exception("Movement already cancelled");

            if (PendingCancellation.CancellationKey != cancellationKey)
                throw new Exception("No cancellation with this key");

            //canceling movement
            RaiseEvent(new MovementCancellationAccepted(SiteId, Id, cancellationKey, closedBy, MovementTypeValue, PendingCancellation.Reason));
        }
        public void DeclineMovementCancellation(Guid cancellationKey, Guid closedBy)
        {
            if (PendingCancellation == null)
                throw new Exception("No pending cancellation");

            if (PendingCancellation != null && PendingCancellation.Status.HasValue && PendingCancellation.Status.Value)
                throw new Exception("Movement already cancelled");

            if (PendingCancellation.CancellationKey != cancellationKey)
                throw new Exception("No cancellation with this key");

            RaiseEvent(new MovementCancellationDeclined(Id, cancellationKey, closedBy));
        }
        public void UseTransporter(Guid transporterId)
        {
            if (PendingCancellation != null && PendingCancellation.Status.HasValue && PendingCancellation.Status.Value)
                throw new Exception("Movement already cancelled");

            ISpecification<Movement> specs = new TransporterExists(transporterId);
            if (!specs.IsSatisfiedBy(this))
                throw new Exception("No transporter with this Id");

            RaiseEvent(new TransporterAdded(Id, transporterId));
        }
        public void ExportMovement()
        {
            RaiseEvent(new MovementExported(Id));
        }
        public void ReconcileMovement()
        {
        }

        public void ResolvePTA(DateTime effectiveDateUTC, Dictionary<string, int> equipments, string docketNumber)
        {
            if (!IsPendingPta)
                throw new Exception("Movement is not pending PTA process!");

            if (equipments.Count != Equipment.Count)
            {
                throw new Exception("Provided equpiments are different than already existing in movement");
            }
            foreach (var equipment in equipments)
            {
                if (!Equipment.ContainsKey(equipment.Key))
                    throw new Exception("Movement don't contain equipment with provided key.");
            }

            RaiseEvent(new PTAResolved(Id, effectiveDateUTC, equipments, docketNumber));
        }

        public void CompleteMovement(DateTime effectiveDateUTC, string docketNumber)
        {
            if (!IsPending)
                throw new Exception("Movement is already completed!");

            RaiseEvent(new MovementCompleted(Id, effectiveDateUTC, docketNumber));
        }


        void Apply(MovementCreated evt)
        {
            Id = evt.MovementId;
            SupplierId = evt.SupplierId;
            Equipment = evt.Equipments;
            PendingCorrection = null;
            PendingCancellation = null;
            HistoryOfCancellations = new List<Cancellation>();
            HistoryOfCorrections = new List<Correction>();
            SiteId = evt.SiteId;
            IsPendingPta = evt.IsPTA;
            IsPending = evt.IsPending;
            MovementTypeValue = evt.MovementType;
            Direction = evt.Direction;
            MovementDate = evt.MovementDate;
            DocketNumber = evt.DocketNumber;
        }
        void Apply(MovementCancellationIssued evt)
        {
            PendingCancellation = new Cancellation(evt.CancellationKey, evt.Reason);
        }
        void Apply(MovementCancellationAccepted evt)
        {
            PendingCancellation.ChangeCancellationStatus(true);
            HistoryOfCancellations.Add(PendingCancellation);
            PendingCancellation = null;
        }
        void Apply(MovementCancellationDeclined evt)
        {
            PendingCancellation.ChangeCancellationStatus(false);
            HistoryOfCancellations.Add(PendingCancellation);
            PendingCancellation = null;
        }
        void Apply(CorrectionRequestIssued evt)
        {
            PendingCorrection = new Correction(evt.CorrectionKey, evt.AssetCodeQuantity, evt.NewDate, evt.NewEffectiveDate);
        }
        void Apply(QuantityCorrectionWithdrawn evt)
        {
            PendingCorrection = null;
        }
        void Apply(CancellationWithdrawn evt)
        {
            PendingCancellation = null;
        }
        void Apply(CorrectionApproved evt)
        {
            PendingCorrection.ChangeCorrectionStatus(true);
            HistoryOfCorrections.Add(PendingCorrection);
            PendingCorrection = null;
        }
        void Apply(CorrectionDeclined evt)
        {
            PendingCorrection.ChangeCorrectionStatus(false);
            HistoryOfCorrections.Add(PendingCorrection);
            PendingCorrection = null;
        }
        void Apply(TransporterAdded evt)
        {
        }
        void Apply(MovementExported evt)
        {
            IsExported = true;
        }

        void Apply(ReferencesUpdated evt)
        {

        }
        void Apply(PTAResolved evt)
        {
            Equipment = evt.Equipments;
            IsPendingPta = false;
            DocketNumber = evt.DocketNumber;
        }
        void Apply(MovementCompleted evt)
        {
            //NOTE: @Jakub, what's this? logic in apply method!!
            //if (!string.IsNullOrEmpty(evt.DocketNumber))
            //{
            //    IsPending = false;
            //}

            DocketNumber = evt.DocketNumber;
            IsPending = false;
        }
    }

    public class Correction
    {
        public Guid CorrectionKey { get; set; }
        public Dictionary<string, int> AssetCodeQuantity { get; set; }
        public DateTime Date { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string DestinationId { get; set; }

        //true accepted, false declined
        public bool? Status { get; set; }

        public Correction(Guid key, Dictionary<string, int> assetCodeQuantity, DateTime newDate, DateTime newEffectiveDate)
        {
            CorrectionKey = key;
            AssetCodeQuantity = assetCodeQuantity;
            Date = newDate;
            EffectiveDate = newEffectiveDate;
        }
        public void ChangeCorrectionStatus(bool accepted)
        {
            Status = accepted;
        }
    }

    public class Cancellation
    {
        public Guid CancellationKey { get; set; }
        public string Reason { get; set; }

        //true accepted, false declined
        public bool? Status { get; set; }

        public Cancellation(Guid key, string reason)
        {
            CancellationKey = key;
            Reason = reason;
        }

        public void ChangeCancellationStatus(bool accepted)
        {
            Status = accepted;
        }
    }

    public enum MovementType
    {
        Unknown,
        Internal,
        Transfer
    }

    public enum TransferDirection
    {
        In,
        Out
    }
}
