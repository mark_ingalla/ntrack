﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonDomain.Core;
using PalletPlus.Messages.Events.Stocktake;

namespace PalletPlus.Domain.Models
{
    public class Stocktake : AggregateBase
    {
        public List<AssetCount> Counts { get; set; }
        public Guid SiteId { get; set; }
        public Guid SupplierId;

        public Stocktake() { }

        public Stocktake(Guid stocktakeId, Guid siteId, Guid supplierId, DateTime takenOn)
        {
            RaiseEvent(new StocktakeRecorded(stocktakeId, siteId, supplierId, takenOn));
        }

        public void AddCount(Guid countKey, Guid siteId, string code, int quantity)
        {
            RaiseEvent(new StockCounted(Id, SupplierId, siteId, countKey, code, quantity));
        }

        public void CorrectQTY(Guid correctionKey, string assetCode, int quantity, string reason, Guid userId)
        {
            //asset must exists
            //qty > 0
            //issue correction request
            //each correction request will have a key

            if (correctionKey == Guid.Empty || quantity == 0)
                throw new Exception("No changes detected");

            var stocktakeItem = Counts.FirstOrDefault(x => x.AssetCode == assetCode);

            if (stocktakeItem != null)
            {
                if (stocktakeItem.PendingCancellation != null && stocktakeItem.PendingCancellation.Status != null &&
                    stocktakeItem.PendingCancellation.Status.Value)
                    throw new Exception("Movement cancelled");

                if (stocktakeItem.PendingCancellation != null && stocktakeItem.PendingCancellation.Status == null)
                    throw new Exception("Movement has pending cancellation");
            }

            if (quantity <= 0)
                throw new Exception("Quantity should be positive");

            RaiseEvent(new StockCountCorrectionRequested(Id, correctionKey, assetCode, quantity, reason, userId));
        }

        public void ApproveCorrection(Guid correctionKey, Guid closedBy)
        {
            var countItem =
                Counts.FirstOrDefault(
                    x => x.PendingCorrection != null && x.PendingCorrection.CorrectionKey == correctionKey);

            if (countItem == null)
                throw new Exception("No item with that correction key.");

            if (countItem.PendingCorrection == null)
                throw new Exception("No pending correction");

            if (countItem.PendingCorrection.CorrectionKey != correctionKey)
                throw new Exception("No correction with this key");

            //there is a saga that will manage the approval/reject
            RaiseEvent(new StocktakeCorrectionApproved(SiteId, Id, correctionKey, SupplierId, closedBy, countItem.AssetCode, countItem.PendingCorrection.Quantity));
        }
        public void DeclineCorrection(Guid correctionKey, Guid closedBy)
        {
            var PendingCorrection = Counts.FirstOrDefault(x => x.PendingCorrection.CorrectionKey == correctionKey).PendingCorrection;

            if (PendingCorrection == null)
                throw new Exception("No pending correction");

            if (PendingCorrection.CorrectionKey != correctionKey)
                throw new Exception("No correction with this key");

            RaiseEvent(new StocktakeCorrectionDeclined(Id, correctionKey, closedBy));
        }

        void Apply(StocktakeRecorded evt)
        {
            Id = evt.StocktakeId;
            SupplierId = evt.SupplierId;
            SiteId = evt.SiteId;

            Counts = new List<AssetCount>();
        }

        void Apply(StockCounted evt)
        {
            Counts.Add(new AssetCount
                           {
                               AssetCode = evt.EquipmentCode,
                               Quantity = evt.Quantity
                           });
        }

        void Apply(StockCountCorrectionRequested evt)
        {
            var stocktakeItem = Counts.FirstOrDefault(x => x.AssetCode == evt.AssetCode);
            stocktakeItem.PendingCorrection = new StocktakeCorrection(evt.CorrectionKey, evt.Quantity, evt.Reason);
        }

        void Apply(StocktakeCorrectionApproved evt)
        {
            var PendingCorrection =
                Counts.FirstOrDefault(x => x.PendingCorrection != null && x.PendingCorrection.CorrectionKey == evt.CorrectionKey).PendingCorrection;

            PendingCorrection.ChangeCorrectionStatus(true);
        }
        void Apply(StocktakeCorrectionDeclined evt)
        {
            var PendingCorrection =
                Counts.FirstOrDefault(x => x.PendingCorrection != null && x.PendingCorrection.CorrectionKey == evt.CorrectionKey).PendingCorrection;

            PendingCorrection.ChangeCorrectionStatus(false);
        }
    }

    public class AssetCount
    {
        public StocktakeCancellation PendingCancellation { get; set; }

        //true - accepted, false - declined, null - waiting for ack
        public StocktakeCorrection PendingCorrection { get; set; }

        public string AssetCode { get; set; }
        public int Quantity { get; set; }

        public AssetCount()
        {
            PendingCorrection = null;
            PendingCancellation = null;
        }
    }

    public class StocktakeCancellation
    {
        public Guid CancellationKey { get; set; }
        public string Reason { get; set; }

        //true accepted, false declined
        public bool? Status { get; set; }

        public StocktakeCancellation(Guid key, string reason)
        {
            CancellationKey = key;
            Reason = reason;
        }

        public void ChangeCancellationStatus(bool accepted)
        {
            Status = accepted;
        }
    }

    public class StocktakeCorrection
    {
        public Guid CorrectionKey { get; set; }
        public int Quantity { get; set; }
        public string Reason { get; set; }

        //true accepted, false declined
        public bool? Status { get; set; }

        public StocktakeCorrection(Guid correctionKey, int assetQuantity, string reason)
        {
            CorrectionKey = correctionKey;
            Quantity = assetQuantity;
            Reason = reason;
        }

        public void ChangeCorrectionStatus(bool accepted)
        {
            Status = accepted;
        }
    }
}
