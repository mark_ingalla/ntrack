﻿using System;
using CommonDomain.Core;
using PalletPlus.Messages.Events.Transporter;

namespace PalletPlus.Domain.Models
{
    public class Transporter : AggregateBase
    {
        public Transporter() { }

        public Transporter(Guid transporterId, string name, string addressLine1, string addressLine2,
            string suburb, string state, string postcode, string country, string contactName, string contactEmail)
        {
            RaiseEvent(new TransporterCreated(transporterId, name, addressLine1, addressLine2, suburb, state, postcode,
                                              country,
                                              contactName, contactEmail));
        }

        public void Update(string name, string addressLine1, string addressLine2,
            string suburb, string state, string postcode, string country, string contactName, string contactEmail)
        {
            RaiseEvent(new TransporterUpdated(Id, name, addressLine1,
                                              addressLine2, suburb, state, postcode, country, contactName, contactEmail));
        }
        public void Delete()
        {
            RaiseEvent(new TransporterDeleted(Id));
        }
        public void Undelete()
        {
            RaiseEvent(new TransporterUndeleted(Id));
        }


        void Apply(TransporterCreated evt)
        {
            Id = evt.TransporterId;
        }
        void Apply(TransporterUpdated evt)
        {
        }
        void Apply(TransporterDeleted evt)
        {
        }
        void Apply(TransporterUndeleted evt)
        {
        }
    }
}
