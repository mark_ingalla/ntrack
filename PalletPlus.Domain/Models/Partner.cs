﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonDomain.Core;
using PalletPlus.Domain.infrastructure;
using PalletPlus.Domain.Validation;
using PalletPlus.Messages.Events.Partner;

namespace PalletPlus.Domain.Models
{
    public class Partner : AggregateBase
    {
        public Dictionary<Guid, List<string>> AllowedEquipments { get; private set; }
        public List<PartnerSupplier> PartnerSuppliers { get; private set; }
        public bool IsPTAEnabled { get; set; }


        public Partner() { }

        public Partner(Guid partnerId, string name,
            string addressLine1, string addressLine2, string suburb, string state, string postCode, string country,
            string contactName, string contactEmail, bool isPTAEnabled, List<string> tags)
        {
            RaiseEvent(new PartnerCreated(partnerId, name,
                addressLine1, addressLine2, suburb, state, postCode, country,
                contactName, contactEmail, isPTAEnabled, tags));
        }

        public void Update(string name,
            string addressLine1, string addressLine2, string suburb, string state, string postCode, string country,
            string contactName, string contactEmail, bool isPTAEnabled, List<string> tags)
        {
            //NOTE: @Jakub.. why put this logic here.. what happen when the user update the partner without changing the PTA??
            //if (IsPTAEnabled == isPTAEnabled)
            //    throw new Exception("PTA process for this partner is already " + (IsPTAEnabled ? "enabled" : "disabled"));

            RaiseEvent(new PartnerUpdated(Id, name,
                addressLine1, addressLine2, suburb, state, postCode, country,
                contactName, contactEmail, isPTAEnabled, tags));
        }

        public void Delete()
        {
            RaiseEvent(new PartnerDeleted(Id));
        }

        public void Undelete()
        {
            RaiseEvent(new PartnerUndeleted(Id));
        }

        public void AddSupplier(Guid supplierId, string accountNumber)
        {
            if (PartnerSuppliers.Any(x => x.SupplierId == supplierId))
                throw new Exception("This supplier has been already added");

            ISpecification<Partner> specs = new SupplierExists(supplierId);
            if (!specs.IsSatisfiedBy(this))
                throw new Exception("No supplier with this id");

            RaiseEvent(new SupplierToPartnerAdded(Id, supplierId, accountNumber));
        }
        public void RemoveSupplier(Guid supplierId)
        {
            if (!PartnerSuppliers.Any(x => x.SupplierId == supplierId))
                throw new Exception("There's no supplier with this id");

            RaiseEvent(new SupplierFromPartnerRemoved(Id, supplierId));
        }
        public void AllowEquipment(string equipmentCode, Guid supplierId)
        {
            var supplierExists = new SupplierExists(supplierId);
            var areAssetsEnabledForSupplier = new AreAssetsEnabledForSupplier(supplierId, new List<string>() { equipmentCode });

            ISpecification<Partner> specification = supplierExists.And<Partner>(areAssetsEnabledForSupplier);

            if (!specification.IsSatisfiedBy(this))
                throw new Exception("Supplier doesn't exist or equipment is not allowed for that supplier");

            if (!PartnerSuppliers.Any(x => x.SupplierId == supplierId))
                throw new Exception("Supplier is not allowed for that partner");

            if (AllowedEquipments[supplierId].Contains(equipmentCode))
                throw new Exception("Equipment already allowed");

            RaiseEvent(new EquipmentToPartnerAllowed(Id, supplierId, equipmentCode));
        }
        public void DisallowEquipment(string equipmentCode, Guid supplierId)
        {
            if (!PartnerSuppliers.Any(x => x.SupplierId == supplierId))
                throw new Exception("Supplier with this id is not allowed");

            if (!AllowedEquipments[supplierId].Contains(equipmentCode))
                throw new Exception("Equipment with this code is not allowed");

            RaiseEvent(new EquipmentToPartnerDisallowed(Id, supplierId, equipmentCode));
        }

        public void UpdateEnableExchange(Guid supplierId, Guid partnerId, bool enableExchage)
        {
            var partnersupplier = PartnerSuppliers.FirstOrDefault(x => x.SupplierId == supplierId);
            if (partnersupplier != null)
            {
                partnersupplier.EnableExchange = !partnersupplier.EnableExchange;


            }

            RaiseEvent(new EnableExchangeUpdated(supplierId, partnerId, enableExchage));
        }

        void Apply(PartnerCreated evt)
        {
            Id = evt.PartnerId;
            AllowedEquipments = new Dictionary<Guid, List<string>>();
            PartnerSuppliers = new List<PartnerSupplier>();
            IsPTAEnabled = evt.IsPTAEnabled;

        }
        void Apply(PartnerUpdated evt)
        {
            IsPTAEnabled = evt.IsPTAEnabled;
        }
        void Apply(PartnerDeleted evt)
        {
        }
        void Apply(PartnerUndeleted evt)
        {
        }
        void Apply(SupplierToPartnerAdded evt)
        {
            PartnerSuppliers.Add(new PartnerSupplier(evt.SupplierId, evt.AccountNumber));
            AllowedEquipments.Add(evt.SupplierId, new List<string>());
        }
        void Apply(SupplierFromPartnerRemoved evt)
        {
            PartnerSuppliers.RemoveAll(x => x.SupplierId == evt.SupplierId);
            AllowedEquipments.Remove(evt.SupplierId);
        }
        void Apply(EquipmentToPartnerAllowed evt)
        {
            AllowedEquipments[evt.SupplierId].Add(evt.EquipmentCode);
        }
        void Apply(EquipmentToPartnerDisallowed evt)
        {
            AllowedEquipments[evt.SupplierId].Remove(evt.EquipmentCode);
        }

        void Apply(EnableExchangeUpdated evt)
        {
            var partnersupplier = PartnerSuppliers.FirstOrDefault(x => x.SupplierId == evt.SupplierId);
            if (partnersupplier != null)
            {
                partnersupplier.EnableExchange = evt.EnabledExchange;
            }
        }
    }

    public class PartnerSupplier
    {
        public Guid SupplierId { get; set; }
        public string AccountNumber { get; set; }
        public bool EnableExchange { get; set; }

        public PartnerSupplier(Guid supplierId, string accountNumber)
        {
            SupplierId = supplierId;
            AccountNumber = accountNumber;

        }
    }
}
