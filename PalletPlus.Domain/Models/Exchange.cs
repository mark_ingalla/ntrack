﻿using System;
using System.Collections.Generic;
using CommonDomain.Core;
using PalletPlus.Domain.infrastructure;
using PalletPlus.Domain.Services;
using PalletPlus.Domain.Validation;
using PalletPlus.Messages.Events.Exchange;
using StructureMap;

namespace PalletPlus.Domain.Models
{
    public class Exchange : AggregateBase
    {

        public Guid SiteId { get; set; }
        public Guid SupplierId { get; set; }
        public Guid PartnerId { get; set; }

        public string DocketNumber { get; set; }
        public Dictionary<string, int> EquipmentsIn { get; set; }
        public Dictionary<string, int> EquipmentsOut { get; set; }

        public DateTime EffectiveDate { get; set; }
        public string PartnerReference { get; set; }
        public string ConsignmentReference { get; set; }

        public Exchange()
        {
        }

        public Exchange(Guid exchangeId, Guid siteId, Guid supplierId, Guid partnerId, DateTime effectiveDate, string partnerReference, string consignmentReference
            , Dictionary<string, int> equipmentsIn, Dictionary<string, int> equipmentsOut)
        {


            ISpecification<Exchange> isExchangeEnabled = new IsPartnerEnabledExchange(partnerId, supplierId);
            if (!isExchangeEnabled.IsSatisfiedBy(this))
                throw new Exception("Impossible to make an exchange");

            var service = ObjectFactory.GetInstance<DocketNumberService>();
            var documentNumber = service.GetExchangeSequenceNumber(siteId);

            RaiseEvent(new ExchangeCreated(exchangeId, siteId, supplierId, partnerId, effectiveDate, partnerReference, consignmentReference, equipmentsIn, equipmentsOut, documentNumber));
        }


        void Apply(ExchangeCreated evt)
        {
            Id = evt.ExchangeId;
            SiteId = evt.SiteId;
            SupplierId = evt.SupplierId;
            PartnerId = evt.PartnerId;
            DocketNumber = evt.DocketNumber;
            EffectiveDate = evt.EffectiveDate;
            PartnerReference = evt.PartnerReference;
            ConsignmentReference = evt.ConsignmentReference;
            EquipmentsIn = evt.EquipmentsIn;
            EquipmentsOut = evt.EquipmentsOut;
        }
    }
}
