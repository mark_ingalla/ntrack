﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonDomain.Core;
using PalletPlus.Messages.Events.Supplier;

namespace PalletPlus.Domain.Models
{
    //Logical Supplier (eg. CHEP)
    public class Supplier : AggregateBase
    {
        public List<Equipment> Equipment { get; set; }
        public Dictionary<Guid, string> Locations { get; set; } //Logical supplier locations (eg. CHEP Flemington) HashSet<accountNo>
        public bool IsPTAEnabled { get; set; }

        public Supplier() { }

        public Supplier(Guid supplierId, string name, string providerName, bool isPTAEnabled, string emailAddress)
        {
            RaiseEvent(new SupplierCreated_V2(supplierId, name, providerName, isPTAEnabled, emailAddress));
        }
        public void Update(string name, string providerName, bool isPTAEnabled, string emailAddress)
        {
            RaiseEvent(new SupplierUpdated_V2(Id, name, providerName, isPTAEnabled, emailAddress));
        }


        public void Delete()
        {
            RaiseEvent(new SupplierDeleted(Id));
        }
        public void Undelete()
        {
            RaiseEvent(new SupplierUndeleted(Id));
        }

        public void AddAsset(string equipmentCode, string equipmentName)
        {
            if (Equipment.Any(x => x.Code == equipmentCode))
                throw new Exception("Equipment type already added");

            RaiseEvent(new EquipmentSupplierAdded(Id, equipmentCode, equipmentName));
        }
        public void DiscontinueAsset(string equipmentCode)
        {
            if (Equipment.All(x => x.Code != equipmentCode))
                throw new Exception("There's no equipment with this code");

            RaiseEvent(new EquipmentSupplierDiscontinued(Id, equipmentCode));
        }
        public void UndiscontinueAsset(string equipmentCode)
        {
            if (Equipment.Any(x => x.Code == equipmentCode))
                throw new Exception("The equipment with this code is already active.");

            RaiseEvent(new EquipmentSupplierUndiscontinued(Id, equipmentCode));
        }
        public void AddLocation(Guid locationKey, string accountNumber, string locationName, string addressLine1, string addressLine2,
            string suburb, string state, string postcode, string country, string contactName, string contactEmail)
        {
            if (Locations.ContainsValue(accountNumber))
                throw new Exception("Location account number already exists.");

            if (Locations.ContainsKey(locationKey))
                throw new Exception("Location key already exists!");

            RaiseEvent(new LocationToSupplierAdded(Id, locationKey, locationName, accountNumber, addressLine1, addressLine2, suburb, state,
                postcode, country, contactName, contactEmail));
        }
        public void UpdateLocation(Guid locationKey, string accountNumber, string locationName, string addressLine1, string addressLine2,
            string suburb, string state, string postcode, string country, string contactName, string contactEmail)
        {
            if (!Locations.ContainsKey(locationKey))
                throw new Exception("LocationKey doesn't exist.");

            //NOTE: is this correct? accountNumber must be unique?
            if (Locations.ContainsValue(accountNumber) && Locations[locationKey] != accountNumber)
                throw new Exception("Location account number already exists.");

            RaiseEvent(new SupplierLocationUpdated(Id, locationKey, locationName, accountNumber, addressLine1, addressLine2, suburb, state,
                postcode, country, contactName, contactEmail));
        }
        public void RemoveLocation(Guid locationKey)
        {
            if (!Locations.ContainsKey(locationKey))
                throw new Exception("Location doesn't exist.");

            RaiseEvent(new SupplierLocationRemoved(Id, locationKey));
        }
        public void UnremoveLocation(Guid locationKey, string accountNumber)
        {
            if (Locations.ContainsKey(locationKey))
                throw new Exception("Location was never removed.");

            RaiseEvent(new SupplierLocationUnremoved(Id, locationKey, accountNumber));
        }



        void Apply(SupplierCreated_V2 evt)
        {
            Id = evt.SupplierId;
            Equipment = new List<Equipment>();
            Locations = new Dictionary<Guid, string>();
            IsPTAEnabled = evt.IsPTAEnabled;
        }
        void Apply(SupplierUpdated_V2 evt)
        {
            IsPTAEnabled = evt.IsPTAEnabled;
        }
        void Apply(SupplierDeleted evt)
        {
        }
        void Apply(SupplierUndeleted evt)
        {
        }
        void Apply(EquipmentSupplierAdded evt)
        {
            Equipment.Add(new Equipment { Code = evt.EquipmentCode, Name = evt.EquipmentName, IsActive = true });
        }
        void Apply(EquipmentSupplierDiscontinued evt)
        {
            Equipment.Single(x => x.Code == evt.EquipmentCode).IsActive = false;
        }
        void Apply(EquipmentSupplierUndiscontinued evt)
        {
            Equipment.Single(x => x.Code == evt.EquipmentCode).IsActive = true;
        }
        void Apply(LocationToSupplierAdded evt)
        {
            Locations.Add(evt.LocationKey, evt.AccountNumber);
        }
        void Apply(SupplierLocationUpdated evt)
        {
            Locations[evt.LocationKey] = evt.AccountNumber;
        }
        void Apply(SupplierLocationRemoved evt)
        {
            Locations.Remove(evt.LocationKey);
        }
        void Apply(SupplierLocationUnremoved evt)
        {
            Locations.Add(evt.LocationKey, evt.AccountNumber);
        }
    }


    public class Equipment
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
