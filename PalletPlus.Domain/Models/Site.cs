﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonDomain.Core;
using PalletPlus.Domain.infrastructure;
using PalletPlus.Domain.Validation;
using PalletPlus.Messages.Events.Site;

namespace PalletPlus.Domain.Models
{
    public class Site : AggregateBase
    {
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public List<SitePartner> AllowedPartners { get; private set; }
        public List<SiteSupplier> Suppliers { get; private set; }
        public List<SiteUser> Users { get; private set; }
        public bool IsPTAEnabled { get; private set; }

        public Site() { }

        public Site(Guid accountId, Guid siteId, string siteName,
            string addressLine1, string addressLine2, string suburb, string state, string postcode, string country)
        {
            ISpecification<Site> specs = new AccountExists(accountId);
            if (!specs.IsSatisfiedBy(this))
                throw new Exception("No account with this id" + "");

            RaiseEvent(new SiteCreated(accountId, siteId, siteName,
                                       addressLine1, addressLine2, suburb, state, postcode, country));
        }

        public void Update(string siteName,
            string addressLine1, string addressLine2, string suburb, string state, string postcode, string country)
        {
            RaiseEvent(new SiteUpdated(Id, siteName, addressLine1, addressLine2, suburb, state, postcode, country));
        }

        public void UpdatePTAInfo(bool isEnabled, string suffix, int sequenceNumber)
        {
            if (IsPTAEnabled == isEnabled)
                throw new Exception("PTA process for this site is already " + (isEnabled ? "enabled" : "disabled"));

            RaiseEvent(new PTAInfoUpdated(Id, isEnabled, suffix, sequenceNumber));
        }

        public void Delete()
        {
            RaiseEvent(new SiteDeleted(Id));
        }
        public void Undelete()
        {
            RaiseEvent(new SiteUndeleted(Id));
        }
        public void AddSupplierToSite(Guid supplierId, string supplierName, string supplierAccountNo, string prefix, string suffix, int sequence)
        {
            if (string.IsNullOrWhiteSpace(prefix) || string.IsNullOrWhiteSpace(suffix) || sequence < 0)
                throw new Exception("Invalid prefix, suffix or starting sequence no");

            ISpecification<Site> specs = new SupplierExists(supplierId);

            if (Suppliers.Any(x => x.SupplierId == supplierId))
                throw new Exception("There's already supplier with that id");

            if (!specs.IsSatisfiedBy(this))
                throw new Exception("No supplier with that id");

            RaiseEvent(new SupplierToSiteAdded(Id, supplierId, supplierName, supplierAccountNo, prefix, suffix, sequence));
        }
        public void UpdateSupplier(Guid supplierId, string prefix, string suffix, int sequence)
        {
            if (string.IsNullOrWhiteSpace(prefix) || string.IsNullOrWhiteSpace(suffix) || sequence < 0)
                throw new Exception("Invalid prefix, suffix or starting sequence no");

            if (!Suppliers.Any(x => x.SupplierId == supplierId))
                throw new Exception("There's no supplier with that id");

            RaiseEvent(new SiteSupplierUpdated(Id, supplierId, prefix, suffix, sequence));
        }
        public void RemoveSupplierFromSite(Guid supplierId)
        {
            if (Suppliers.All(x => x.SupplierId != supplierId))
                throw new Exception("There's no supplier with that id");

            RaiseEvent(new SupplierFromSiteRemoved(Id, supplierId));
        }
        public void AddPartner(Guid partnerId)
        {
            //var isPartnerEnabledInAccount = new IsPartnerEnabledInAccount(_accountId, partnerId);
            var partnerExists = new PartnerExists(partnerId);
            //var specs = partnerExists.And(isPartnerEnabledInAccount);

            if (AllowedPartners.Any(x => x.PartnerId == partnerId))
                throw new Exception("Partner with this Id already enabled");

            //if (!specs.IsSatisfiedBy(this))
            if (!partnerExists.IsSatisfiedBy(this))
                throw new Exception("No partner with this id");

            RaiseEvent(new PartnerForSiteEnabled(Id, partnerId));
        }
        public void RemovePartner(Guid partnerId)
        {
            if (!AllowedPartners.Any(x => x.PartnerId == partnerId))
                throw new Exception("Partner with this Id already enabled");

            RaiseEvent(new PartnerForSiteDisabled(Id, partnerId));
        }
        public void AddUserToSite(Guid userId, string email, string fullName)
        {
            if (Users.Any(x => x.UserId == userId))
                throw new Exception("There's already user with that id");

            RaiseEvent(new UserToSiteAdded(Id, userId, email, fullName));
        }
        public void RemoveUserFromSite(Guid userId)
        {
            if (!Users.Any(x => x.UserId == userId))
                throw new Exception("There's no user with that id");

            RaiseEvent(new UserFromSiteRemoved(Id, userId));
        }



        void Apply(SiteCreated evt)
        {
            Id = evt.SiteId;

            AllowedPartners = new List<SitePartner>();
            Suppliers = new List<SiteSupplier>();
            Users = new List<SiteUser>();
        }
        void Apply(SiteUpdated evt)
        {

        }
        void Apply(PTAInfoUpdated evt)
        {
            IsPTAEnabled = evt.IsEnabled;
        }
        void Apply(SiteDeleted evt)
        {

        }
        void Apply(SiteUndeleted evt)
        {

        }
        void Apply(SupplierToSiteAdded evt)
        {
            Suppliers.Add(new SiteSupplier(evt.SupplierId, evt.AccountNumber));
        }
        void Apply(SiteSupplierUpdated evt)
        {
        }
        void Apply(SupplierFromSiteRemoved evt)
        {
            Suppliers.RemoveAll(x => x.SupplierId == evt.SupplierId);
        }
        void Apply(UserToSiteAdded evt)
        {
            Users.Add(new SiteUser(evt.UserId, evt.Email, evt.FullName));
        }
        void Apply(UserFromSiteRemoved evt)
        {
            Users.RemoveAll(x => x.UserId == evt.UserId);
        }
        void Apply(PartnerForSiteEnabled evt)
        {
            AllowedPartners.Add(new SitePartner(evt.PartnerId));
        }
        void Apply(PartnerForSiteDisabled evt)
        {
            AllowedPartners.RemoveAll(x => x.PartnerId == evt.PartnerId);
        }
    }

    public class SitePartner
    {
        public Guid PartnerId { get; set; }

        public SitePartner(Guid partnerId)
        {
            PartnerId = partnerId;
        }
    }

    public class SiteSupplier
    {
        public Guid SupplierId { get; set; }
        public string AccountNo { get; set; }

        public SiteSupplier(Guid supplierId, string accountNo)
        {
            SupplierId = supplierId;
            AccountNo = accountNo;
        }
    }

    public class SiteUser
    {
        public Guid UserId { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }

        public SiteUser(Guid userId, string email, string fullName)
        {
            UserId = userId;
            Email = email;
            FullName = fullName;
        }
    }
}
