﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonDomain.Core;
using PalletPlus.Domain.infrastructure;
using PalletPlus.Domain.Validation;
using PalletPlus.Messages.Events.Account;

namespace PalletPlus.Domain.Models
{
    public class Account : AggregateBase
    {
        public Dictionary<Guid, string> AccountSuppliers { get; private set; }
        public List<Guid> Users { get; set; }

        public Account() { }
        public Account(Guid accountId, string companyName)
        {
            RaiseEvent(new AccountCreated(accountId, companyName));
        }
        public static Account CreateAccount(Guid accountId, string companyName)
        {
            return new Account(accountId, companyName);
        }

        public void AddSupplierToSite(Guid supplierId, string supplierName, string supplierAccountNo, string prefix, string suffix, int sequence)
        {
            if (string.IsNullOrWhiteSpace(prefix) || string.IsNullOrWhiteSpace(suffix) || sequence < 0)
                throw new Exception("Invalid prefix, suffix or starting sequence no");

            ISpecification<Account> specs = new SupplierExists(supplierId);

            if (AccountSuppliers.Any(x => x.Key == supplierId))
                throw new Exception("There's already supplier with that id");

            if (!specs.IsSatisfiedBy(this))
                throw new Exception("No supplier with that id");

            RaiseEvent(new SupplierToAccountAdded(Id, supplierId, supplierName, supplierAccountNo, prefix, suffix, sequence));
        }
        public void UpdateSupplier(Guid supplierId, string prefix, string suffix, int sequence)
        {
            if (string.IsNullOrWhiteSpace(prefix) || string.IsNullOrWhiteSpace(suffix) || sequence < 0)
                throw new Exception("Invalid prefix, suffix or starting sequence no");

            if (!AccountSuppliers.Any(x => x.Key == supplierId))
                throw new Exception("There's no supplier with that id");

            RaiseEvent(new AccountSupplierUpdated(Id, supplierId, prefix, suffix, sequence));
        }
        public void RemoveSupplierFromSite(Guid supplierId)
        {
            if (AccountSuppliers.All(x => x.Key != supplierId))
                throw new Exception("There's no supplier with that id");

            RaiseEvent(new SupplierFromAccountRemoved(Id, supplierId));
        }
        public void AddCorrectionReason(Guid reasonKey, string reason)
        {
            RaiseEvent(new CorrectionReasonAdded(Id, reasonKey, reason));
        }
        public void RemoveCorrectionReason(Guid reasonKey)
        {
            RaiseEvent(new CorrectionReasonRemoved(Id, reasonKey));
        }
        public void AddCancellationReason(Guid reasonKey, string reason)
        {
            RaiseEvent(new CancellationReasonAdded(Id, reasonKey, reason));
        }
        public void RemoveCancellationReason(Guid reasonKey)
        {
            RaiseEvent(new CancellationReasonRemoved(Id, reasonKey));
        }
        public void SetEquipemntDashboardColor(string equipmentCode, string equipmentName, int r, int g, int b)
        {
            RaiseEvent(new DashboardEquipmentColorSet(Id, Guid.NewGuid(), equipmentCode, equipmentName, r, g, b));
        }
        public void ChangeExportSettings(bool exportMovementIn, bool exportMovementOut)
        {
            RaiseEvent(new ExportSettingsChanged(Id, exportMovementIn, exportMovementOut));
        }
        public void EditRequirements(HashSet<string> requiredFields)
        {
            RaiseEvent(new RequirementsEdited(Id, requiredFields));
        }



        void Apply(RequirementsEdited evt)
        {

        }
        void Apply(AccountCreated evt)
        {
            Id = evt.AccountId;
            AccountSuppliers = new Dictionary<Guid, string>();
            //Sites = new List<Guid>();
            Users = new List<Guid>();
        }
        void Apply(SupplierToAccountAdded evt)
        {
            AccountSuppliers.Add(evt.SupplierId, evt.AccountNumber);
        }
        void Apply(AccountSupplierUpdated evt)
        {
        }
        void Apply(SupplierFromAccountRemoved evt)
        {
            AccountSuppliers.Remove(evt.SupplierId);
        }
        void Apply(CorrectionReasonAdded evt)
        { }
        void Apply(CorrectionReasonRemoved evt)
        { }
        void Apply(CancellationReasonAdded evt)
        { }
        void Apply(CancellationReasonRemoved evt)
        { }
        void Apply(DashboardEquipmentColorSet evt)
        { }
        void Apply(ExportSettingsChanged evt)
        { }
    }
}
