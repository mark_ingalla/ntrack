﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonDomain.Core;
using nTrack.Extension;
using PalletPlus.Messages.Events.SupplierInvoice;

namespace PalletPlus.Domain.Models
{
    /// <summary>
    /// Supplier Invoice
    /// </summary>
    public class SupplierInvoice : AggregateBase
    {
        public bool IsClosed { get; set; }
        public List<InvoiceDetail> Details { get; set; }



        public SupplierInvoice() { }
        public SupplierInvoice(Guid id, Guid supplierId, string invoiceNumber, DateTime invoiceDate)
        {
            RaiseEvent(new InvoiceCreated(id, supplierId, invoiceNumber, invoiceDate));
        }

        public void AddDetail(string code, string docketNumber, string equipmentCode, int qty, int daysHire,
            string accountNumber, string accountRef, string partnerAccountNumber, string partnerRef, string supplierRef, string consignmentRef,
            DateTime movementDate, DateTime effectiveDate, DateTime dehireDate)
        {
            if (IsClosed)
                throw new Exception("Can't add details to a closed invoice");

            RaiseEvent(new DetailAdded(Id, code, docketNumber, equipmentCode, qty, daysHire,
                accountNumber, accountRef, partnerAccountNumber, partnerRef, supplierRef, consignmentRef,
                movementDate, effectiveDate, dehireDate));
        }

        public void CloseInvoice()
        {
            RaiseEvent(new InvoiceClosed(Id, Details.Select(x => new InvoiceClosed.InvoiceDetail
                {
                    Key = Guid.NewGuid(),
                    TransactionCode = x.TransactionCode.ToString(),
                    DocketNumber = x.DocketNumber,
                    EquipmentCode = x.EquipmentCode,
                    Qty = x.Qty,
                    DaysHire = x.DaysHire,
                    AccountNumber = x.AccountNumber,
                    AccountRef = x.AccountRef,
                    PartnerAccountNumber = x.PartnerAccountNumber,
                    PartnerRef = x.PartnerRef,
                    SupplierRef = x.SupplierRef,
                    ConsignmentRef = x.ConsignmentRef,
                    MovementDate = x.MovementDate,
                    EffectiveDate = x.EffectiveDate,
                    DehireDate = x.DehireDate
                }).ToArray()));
        }




        public void ReconcileMovements(Dictionary<Guid, string> invoiceDetailsToReconcile, Dictionary<string, List<string>> movementDetailsToReconcile)
        {
            RaiseEvent(new MovementsReconciled(Id, invoiceDetailsToReconcile, movementDetailsToReconcile));
        }
        public void MatchRecords(DateTime matchedOn, Guid matchedBy,
            string movementId, string movementEquipmentCode, Guid detailKey, DateTime movementDate, string docketNumber, string destinationId,
            string movementType, string movementDirection, string equipmentCode, int quantity)
        {
            RaiseEvent(new MovementRecordMatched(matchedOn, matchedBy,
                movementId, movementEquipmentCode, Id, detailKey, movementDate, docketNumber, destinationId,
                movementType, movementDirection, equipmentCode, quantity));
        }
        public void RejectRecords(DateTime matchedOn, Guid matchedBy, Guid detailKey)
        {
            RaiseEvent(new MovementRecordRejected(matchedOn, matchedBy, Id, detailKey));
        }
        public void CreateTransaction(Guid movementId, Guid detailKey, DateTime createdOn, Guid createdBy)
        {
            RaiseEvent(new TransactionCreated(createdOn, createdBy, Id, detailKey, movementId));
        }



        void Apply(InvoiceCreated t)
        {
            Id = t.InvoiceId;
            Details = new List<InvoiceDetail>();
        }
        void Apply(DetailAdded t)
        {
            Details.Add(new InvoiceDetail
                {
                    TransactionCode = EnumHelpers.FromString<TranscationCode>(t.TransactionCode),
                    DocketNumber = t.DocketNumber,
                    EquipmentCode = t.EquipmentCode,
                    Qty = t.Qty,
                    DaysHire = t.DaysHire,
                    AccountNumber = t.AccountNumber,
                    AccountRef = t.AccountRef,
                    PartnerAccountNumber = t.PartnerAccountNumber,
                    PartnerRef = t.PartnerRef,
                    SupplierRef = t.SupplierRef,
                    ConsignmentRef = t.ConsignmentRef,
                    MovementDate = t.MovementDate,
                    EffectiveDate = t.EffectiveDate,
                    DehireDate = t.DehireDate
                });
        }
        void Apply(InvoiceClosed t)
        {
            IsClosed = true;
        }
        void Apply(MovementsReconciled t) { }
        void Apply(MovementRecordMatched t) { }
        void Apply(MovementRecordRejected t) { }
        void Apply(TransactionCreated t) { }





        public class InvoiceDetail
        {
            public TranscationCode TransactionCode { get; set; }
            public string DocketNumber { get; set; }
            public string EquipmentCode { get; set; }
            public int Qty { get; set; }
            public int DaysHire { get; set; }
            public string AccountNumber { get; set; }
            public string AccountRef { get; set; }
            public string PartnerAccountNumber { get; set; }
            public string PartnerRef { get; set; }
            public string SupplierRef { get; set; }
            public string ConsignmentRef { get; set; }
            public DateTime MovementDate { get; set; }
            public DateTime EffectiveDate { get; set; }
            public DateTime DehireDate { get; set; }
        }

        public enum TranscationCode
        {
            Issue,
            Return,
            Received,
            Sent,
            Unknown
        }
    }



}