﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using CommonDomain.Core;
//using PalletPlus.Messages.Commands.Supplier;
//using PalletPlus.Messages.Events.Supplier;
//using PalletPlus.Messages.Events.User;

//namespace PalletPlus.Domain.Models
//{
//    public class User : AggregateBase // user doesn't belong to aggregate route
//    {
//        private User(Guid id)
//        {
//            if (id == Guid.Empty)
//            {
//                throw new ArgumentException("id");
//            }

//            Id = id;
//        }

//        public User(Guid userId, string firstName, string lastName, string mobile, string phone, string contactEmail)
//        {
//            RaiseEvent(new UserCreated(userId, firstName, lastName, mobile, phone, contactEmail));
//        }

//        void Apply(UserCreated evt)
//        {
//            Id = evt.UserId;
//        }
//    }
//}
