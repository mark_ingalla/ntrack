﻿using EventStore.RavenDb.Conversion;
using PalletPlus.Messages.Events.Supplier;

namespace PalletPlus.Domain.Converters
{
    public class SupplierCreatedConverter :IUpconvertEvents<SupplierCreated, SupplierCreated_V2>
    {
        public SupplierCreated_V2 Convert(SupplierCreated source)
        {
            return new SupplierCreated_V2(source.SupplierId, source.Name, source.ProviderName, source.IsPTAEnabled, source.ContactEmail);
        }
        
    }

    public class SupplierUpdatedConverter:IUpconvertEvents<SupplierUpdated, SupplierUpdated_V2>
    {
        public SupplierUpdated_V2 Convert(SupplierUpdated source)
        {
            return new SupplierUpdated_V2(source.SupplierId, source.Name, source.ProviderName, source.IsPTAEnabled, source.ContactEmail);
        }
    }

}
