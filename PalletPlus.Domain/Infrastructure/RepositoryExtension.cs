﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonDomain;
using EventStore.RavenDb;
using NServiceBus;

namespace PalletPlus.Domain.infrastructure
{
    public static class RepositoryExtension
    {
        public static void Save(this IRepository repository, IAggregate aggregate, Guid commitId, IBus bus)
        {
            repository.Save(aggregate, commitId, headers => UpdateHeaders(headers, bus));
        }
        static void UpdateHeaders(IDictionary<string, object> headers, IBus bus)
        {
            var busHeaders = bus.CurrentMessageContext.Headers;
            foreach (
                var busHeader in
                    busHeaders.Where(busHeader => busHeader.Key.StartsWith("nTrack")))
            {
                headers.Add(busHeader.Key, busHeader.Value);
            }

        }
    }
}