﻿using System;
using System.Linq;
using EventStore.RavenDb;
using PalletPlus.Domain.infrastructure;
using StructureMap;

namespace PalletPlus.Domain.Validation
{
    public class IsInternalMovement : ISpecification<Models.Movement>
    {
        readonly Guid _accountId;
        readonly Guid _siteId;
        readonly Guid _destinationId;
        readonly Guid _supplierId;

        public IRepository Repository { get; set; }

        public IsInternalMovement(Guid accountId, Guid siteId, Guid destinationId, Guid supplierId)
        {
            if (accountId == Guid.Empty || siteId == Guid.Empty || destinationId == Guid.Empty || supplierId == Guid.Empty)
                throw new ArgumentNullException();

            Repository = ObjectFactory.GetInstance<IRepository>();

            _accountId = accountId;
            _siteId = siteId;
            _destinationId = destinationId;
            _supplierId = supplierId;
        }

        public bool IsSatisfiedBy(Models.Movement entity)
        {
            return CheckIfSatisfied(entity);
        }

        private bool CheckIfSatisfied(Models.Movement entity)
        {
            //If the destination is a Partner
            var isAPartner = new PartnerExists(_destinationId);
            if (isAPartner.IsSatisfiedBy(entity))
                return false;
            //If the destination is a Supplier
            var isASupplier = new SupplierExists(_destinationId);
            if (isASupplier.IsSatisfiedBy(entity))
                return false;

            var account = Repository.GetById<Models.Account>(_accountId);
            var accountSupplierInfo = account.AccountSuppliers.ContainsKey(_supplierId)
                                          ? account.AccountSuppliers[_supplierId]
                                          : string.Empty;

            //Site Account Number
            var site = Repository.GetById<Models.Site>(_siteId);
            var siteSupplierInfo = site.Suppliers.SingleOrDefault(x => x.SupplierId == _supplierId);
            var siteAccontNo = siteSupplierInfo != null ? siteSupplierInfo.AccountNo : accountSupplierInfo;
            if (string.IsNullOrEmpty(siteAccontNo))
                return false;

            //Destination Account Number
            var destination = Repository.GetById<Models.Site>(_destinationId);
            var destinationSupplierInfo = destination.Suppliers.SingleOrDefault(x => x.SupplierId == _supplierId);
            var destinationAccountNo = destinationSupplierInfo != null ? destinationSupplierInfo.AccountNo : accountSupplierInfo;
            if (string.IsNullOrEmpty(destinationAccountNo))
                return false;

            if (!siteAccontNo.Equals(destinationAccountNo))
                return false;

            return true;
        }
    }
}
