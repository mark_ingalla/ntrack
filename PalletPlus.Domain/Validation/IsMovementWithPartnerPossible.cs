﻿using System;
using System.Linq;
using EventStore.RavenDb;
using PalletPlus.Domain.infrastructure;
using StructureMap;

namespace PalletPlus.Domain.Validation
{
    public class IsMovementWithPartnerPossible : ISpecification<Models.Movement>
    {
        readonly Guid _siteId;
        readonly Guid _partnerId;

        public IRepository Repository { get; set; }

        public IsMovementWithPartnerPossible(Guid siteId, Guid partnerId)
        {
            if (siteId == Guid.Empty || partnerId == Guid.Empty)
                throw new ArgumentNullException();

            Repository = ObjectFactory.GetInstance<IRepository>();

            _siteId = siteId;
            _partnerId = partnerId;
        }

        public bool IsSatisfiedBy(Models.Movement entity)
        {
            return CheckIfSatisfied();
        }


        bool CheckIfSatisfied()
        {
            var site = Repository.GetById<Models.Site>(_siteId);
            return site.Version > 0 && site.AllowedPartners.Any(x => x.PartnerId == _partnerId);
        }
    }
}
