using System;
using EventStore.RavenDb;
using PalletPlus.Domain.infrastructure;
using PalletPlus.Domain.Models;
using StructureMap;

namespace PalletPlus.Domain.Validation
{
    public class SupplierLocationExists : ISpecification<Movement>
    {
        public IRepository Repository { get; set; }

        public int SupplierVersion { get; set; }
        public bool LocationExists { get; set; }

        public SupplierLocationExists(Guid supplierId, Guid locationId)
        {
            if (supplierId == Guid.Empty)
                throw new ArgumentNullException();

            if (locationId == Guid.Empty)
                throw new ArgumentNullException();

            Repository = ObjectFactory.GetInstance<IRepository>();

            try
            {
                var supplier = Repository.GetById<Models.Supplier>(supplierId);
                SupplierVersion = supplier.Version;
                LocationExists = supplier.Locations.ContainsKey(locationId);
            }
            catch (Exception)
            {
                SupplierVersion = 0;
            }
        }

        bool LocExists()
        {
            return SupplierVersion > 0 && LocationExists;
        }

        public bool IsSatisfiedBy(Movement entity)
        {
            return LocExists();
        }
    }
}