﻿using System;
using EventStore.RavenDb;
using PalletPlus.Domain.infrastructure;
using StructureMap;

namespace PalletPlus.Domain.Validation
{


    public class IsPartnerEnabledPTA : ISpecification<Models.Movement>
    {
        public IRepository Repository { get; set; }

        public Models.Site Site { get; set; }

        public IsPartnerEnabledPTA(Guid siteId, Guid partnerId)
        {
            if (siteId == Guid.Empty || partnerId == Guid.Empty)
                throw new ArgumentNullException();

            Repository = ObjectFactory.GetInstance<IRepository>();

            Site = Repository.GetById<Models.Site>(siteId);
            Partner = Repository.GetById<Models.Partner>(partnerId);
        }

        protected Models.Partner Partner { get; set; }

        public bool IsSatisfiedBy(Models.Movement entity)
        {
            return Check();
        }

        private bool Check()
        {
            return Site.IsPTAEnabled && Partner.IsPTAEnabled;
        }
    }
}
