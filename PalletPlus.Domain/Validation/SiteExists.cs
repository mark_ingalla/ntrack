﻿using System;
using EventStore.RavenDb;
using PalletPlus.Domain.infrastructure;
using PalletPlus.Domain.Models;
using StructureMap;

namespace PalletPlus.Domain.Validation
{
    public class SiteExists : ISpecification<Models.Account>, ISpecification<Movement>
    {
        public IRepository Repository { get; set; }

        public int SiteVersion { get; set; }

        public SiteExists(Guid siteId)
        {
            if (siteId == Guid.Empty)
                throw new ArgumentNullException();


            Repository = ObjectFactory.GetInstance<IRepository>();

            try
            {
                var site = Repository.GetById<Models.Site>(siteId);
                SiteVersion = site.Version;
            }
            catch (Exception)
            {
                SiteVersion = 0;
            }

        }

        public bool IsSatisfiedBy(Models.Account entity)
        {
            return SiteVersion > 0;
        }

        public bool IsSatisfiedBy(Movement entity)
        {
            return SiteVersion > 0;
        }
    }
}
