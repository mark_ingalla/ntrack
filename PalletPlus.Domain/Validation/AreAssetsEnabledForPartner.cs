﻿using System;
using System.Collections.Generic;
using System.Linq;
using EventStore.RavenDb;
using PalletPlus.Domain.infrastructure;
using StructureMap;

namespace PalletPlus.Domain.Validation
{
    public class AreAssetsEnabledForPartner : ISpecification<Models.Movement>
    {
        public IRepository Repository { get; set; }

        public Models.Partner Partner { get; set; }
        public List<string> AssetCodes { get; set; }
        public Guid SupplierId { get; set; }

        public AreAssetsEnabledForPartner(Guid partnerId, List<string> assetCodes, Guid supplierId)
        {
            if (partnerId == Guid.Empty || assetCodes == null || assetCodes.Count == 0)
                throw new ArgumentNullException();

            Repository = ObjectFactory.GetInstance<IRepository>();

            AssetCodes = assetCodes;
            SupplierId = supplierId;
            Partner = Repository.GetById<Models.Partner>(partnerId);
        }

        public bool IsSatisfiedBy(Models.Movement entity)
        {
            return Check();
        }

        public bool IsSatisfiedBy(Models.Partner entity)
        {
            return Check();
        }

        bool Check()
        {
            return Partner.AllowedEquipments.ContainsKey(SupplierId) &&
                AssetCodes.All(assetCode => Partner.AllowedEquipments[SupplierId].Contains(assetCode));
        }
    }
}
