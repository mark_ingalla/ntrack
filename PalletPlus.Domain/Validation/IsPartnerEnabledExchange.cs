﻿using System;
using System.Linq;
using EventStore.RavenDb;
using PalletPlus.Domain.infrastructure;
using PalletPlus.Domain.Models;
using StructureMap;

namespace PalletPlus.Domain.Validation
{
    public class IsPartnerEnabledExchange : ISpecification<Models.Exchange>
    {
        public IRepository Repository { get; set; }
        public Models.Partner Partner { get; set; }
        public PartnerSupplier PartnerSupplier { get; set; }

        public IsPartnerEnabledExchange(Guid partnerId, Guid supplierId)
        {
            if (partnerId == Guid.Empty)
                throw new ArgumentNullException();

            Repository = ObjectFactory.GetInstance<IRepository>();

            Partner = Repository.GetById<Models.Partner>(partnerId);
            PartnerSupplier = Partner.PartnerSuppliers.FirstOrDefault(x => x.SupplierId == supplierId);

        }

        #region Implementation of ISpecification<Exchange>



        public bool IsSatisfiedBy(Domain.Models.Exchange entity)
        {

            return PartnerSupplier.EnableExchange;
        }

        #endregion
    }
}
