﻿using System;
using EventStore.RavenDb;
using PalletPlus.Domain.infrastructure;
using StructureMap;

namespace PalletPlus.Domain.Validation
{
    public class PartnerExists : ISpecification<Models.Movement>, ISpecification<Models.Account>, ISpecification<Models.Site>
    {
        public IRepository Repository { get; set; }

        public Guid PartnerId { get; set; }
        public int PartnerVersion { get; set; }


        public PartnerExists(Guid partnerId)
        {
            if (partnerId == Guid.Empty)
                throw new ArgumentNullException();


            Repository = ObjectFactory.GetInstance<IRepository>();

            try
            {
                var partner = Repository.GetById<Models.Partner>(partnerId);
                PartnerVersion = partner.Version;
            }
            catch (Exception)
            {
                PartnerVersion = 0;
            }

        }

        public bool IsSatisfiedBy(Models.Movement entity)
        {
            return PartnerExistsMethod();
        }

        public bool IsSatisfiedBy(Models.Account entity)
        {
            return PartnerExistsMethod();
        }

        public bool IsSatisfiedBy(Models.Site entity)
        {
            return PartnerExistsMethod();
        }

        private bool PartnerExistsMethod()
        {
            return PartnerVersion > 0;
        }
    }
}
