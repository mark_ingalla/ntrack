﻿using System;
using System.Collections.Generic;
using System.Linq;
using EventStore.RavenDb;
using PalletPlus.Domain.infrastructure;
using StructureMap;

namespace PalletPlus.Domain.Validation
{
    public class AreAssetsEnabledForSupplier : ISpecification<Models.Movement>, ISpecification<Models.Partner>
    {
        public IRepository Repository { get; set; }

        public Models.Supplier Supplier { get; set; }
        public List<string> AssetCodes { get; set; }

        public AreAssetsEnabledForSupplier(Guid supplierId, List<string> equipmentCodes)
        {
            if (supplierId == Guid.Empty || equipmentCodes == null || equipmentCodes.Count == 0)
                throw new ArgumentNullException();

            Repository = ObjectFactory.GetInstance<IRepository>();

            Supplier = Repository.GetById<Models.Supplier>(supplierId);
            AssetCodes = equipmentCodes;

        }

        public bool IsSatisfiedBy(Models.Movement entity)
        {
            return Check();
        }

        public bool IsSatisfiedBy(Models.Partner entity)
        {
            return Check();
        }

        bool Check()
        {
            return AssetCodes.All(code => Supplier.Equipment.Any(x => x.Code == code));
        }
    }
}
