﻿using System;
using System.Linq;
using EventStore.RavenDb;
using PalletPlus.Domain.infrastructure;
using StructureMap;

namespace PalletPlus.Domain.Validation
{
    public class IsSupplierEnabledInSite : ISpecification<Models.Movement>
    {
        public IRepository Repository { get; set; }

        public Models.Site Site { get; set; }
        public Models.Account Account { get; set; }

        public IsSupplierEnabledInSite(Guid siteId, Guid accountId, Guid supplierId)
        {
            if (siteId == Guid.Empty || supplierId == Guid.Empty || accountId == Guid.Empty)
                throw new ArgumentNullException();

            Repository = ObjectFactory.GetInstance<IRepository>();

            Site = Repository.GetById<Models.Site>(siteId);
            Account = Repository.GetById<Models.Account>(accountId);
            Supplier = Repository.GetById<Models.Supplier>(supplierId);
        }

        protected Models.Supplier Supplier { get; set; }

        public bool IsSatisfiedBy(Models.Movement entity)
        {
            return Check();
        }

        private bool Check()
        {
            if (Account.AccountSuppliers.Any(x => x.Key == Supplier.Id))
                return true;
            return Site.Suppliers.Any(x => x.SupplierId == Supplier.Id);
        }
    }
}
