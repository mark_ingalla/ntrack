﻿using System;
using EventStore.RavenDb;
using PalletPlus.Domain.infrastructure;
using StructureMap;

namespace PalletPlus.Domain.Validation
{
    public class AccountExists : ISpecification<Models.Site>
    {
        public IRepository Repository { get; set; }

        public Models.Account Account { get; set; }

        public AccountExists(Guid accountId)
        {
            if (accountId == Guid.Empty)
                throw new ArgumentNullException();

            Repository = ObjectFactory.GetInstance<IRepository>();

            Account = Repository.GetById<Models.Account>(accountId);
        }

        public bool IsSatisfiedBy(Models.Site entity)
        {
            return Account.Version > 0;
        }
    }
}
