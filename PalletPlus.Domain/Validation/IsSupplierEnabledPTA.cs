﻿using System;
using EventStore.RavenDb;
using PalletPlus.Domain.infrastructure;
using StructureMap;

namespace PalletPlus.Domain.Validation
{


    public class IsSupplierEnabledPTA : ISpecification<Models.Movement>
    {
        public IRepository Repository { get; set; }

        public Models.Site Site { get; set; }

        public IsSupplierEnabledPTA(Guid siteId, Guid supplierId)
        {
            if (siteId == Guid.Empty || supplierId == Guid.Empty)
                throw new ArgumentNullException();

            Repository = ObjectFactory.GetInstance<IRepository>();

            Site = Repository.GetById<Models.Site>(siteId);
            Supplier = Repository.GetById<Models.Supplier>(supplierId);
        }

        protected Models.Supplier Supplier { get; set; }

        public bool IsSatisfiedBy(Models.Movement entity)
        {
            return Check();
        }

        private bool Check()
        {
            return Site.IsPTAEnabled && Supplier.IsPTAEnabled;
        }
    }
}
