﻿using System;
using EventStore.RavenDb;
using PalletPlus.Domain.infrastructure;
using PalletPlus.Domain.Models;
using StructureMap;

namespace PalletPlus.Domain.Validation
{
    public class SupplierExists : ISpecification<Models.Account>, ISpecification<Models.Site>, ISpecification<Models.Partner>, ISpecification<Movement>
    {
        public IRepository Repository { get; set; }

        public int SupplierVersion { get; set; }

        public SupplierExists(Guid supplierId)
        {
            if (supplierId == Guid.Empty)
                throw new ArgumentNullException();

            Repository = ObjectFactory.GetInstance<IRepository>();

            try
            {
                var supplier = Repository.GetById<Models.Supplier>(supplierId);
                SupplierVersion = supplier.Version;
            }
            catch (Exception)
            {
                SupplierVersion = 0;
            }
        }

        public bool IsSatisfiedBy(Models.Account entity)
        {
            return SuppExists();
        }

        public bool IsSatisfiedBy(Models.Site entity)
        {
            return SuppExists();
        }

        bool SuppExists()
        {
            return SupplierVersion > 0;
        }

        public bool IsSatisfiedBy(Models.Partner entity)
        {
            return SuppExists();
        }

        public bool IsSatisfiedBy(Movement entity)
        {
            return SuppExists();
        }
    }
}
