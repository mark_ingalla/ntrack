﻿using System;
using System.Linq;
using EventStore.RavenDb;
using PalletPlus.Domain.infrastructure;
using PalletPlus.Domain.Models;
using StructureMap;

namespace PalletPlus.Domain.Validation
{
    public class IsTransferMovement : ISpecification<Models.Movement>
    {
        readonly Guid _accountId;
        readonly Guid _siteId;
        readonly Guid _partnerId;
        readonly Guid _supplierId;

        public IRepository Repository { get; set; }

        public IsTransferMovement(Guid accountId, Guid siteId, Guid partnerId, Guid supplierId)
        {
            if (siteId == Guid.Empty || partnerId == Guid.Empty || supplierId == Guid.Empty || accountId == Guid.Empty)
                throw new ArgumentNullException();


            Repository = ObjectFactory.GetInstance<IRepository>();

            _accountId = accountId;
            _siteId = siteId;
            _partnerId = partnerId;
            _supplierId = supplierId;
        }

        public bool IsSatisfiedBy(Models.Movement entity)
        {
            return CheckIfSatisfied(entity);
        }

        private bool CheckIfSatisfied(Movement entity)
        {
            var account = Repository.GetById<Models.Account>(_accountId);
            var accountSupplierInfo = account.AccountSuppliers.ContainsKey(_supplierId)
                                          ? account.AccountSuppliers[_supplierId]
                                          : string.Empty;

            var site = Repository.GetById<Models.Site>(_siteId);
            var siteSupplierInfo = site.Suppliers.SingleOrDefault(x => x.SupplierId == _supplierId);

            //check if the other party is one of the following
            var isSupplier = new SupplierExists(_supplierId);
            var isPartner = new PartnerExists(_partnerId);
            var isSite = new SiteExists(_siteId);

            var partnerAccountNo = string.Empty;

            if (isSite.IsSatisfiedBy(entity))
            {
                var partner = Repository.GetById<Models.Site>(_partnerId);
                var partnerSupplierInfo = partner.Suppliers.SingleOrDefault(x => x.SupplierId == _supplierId);
                partnerAccountNo = partnerSupplierInfo != null ? partnerSupplierInfo.AccountNo : accountSupplierInfo;
            }

            if (isPartner.IsSatisfiedBy(entity))
            {
                var partner = Repository.GetById<Models.Partner>(_partnerId);
                var partnerSupplierInfo = partner.PartnerSuppliers.SingleOrDefault(x => x.SupplierId == _supplierId);
                partnerAccountNo = partnerSupplierInfo != null ? partnerSupplierInfo.AccountNumber : accountSupplierInfo;
            }

            if (isSupplier.IsSatisfiedBy(entity))
            {
                var partner = Repository.GetById<Models.Supplier>(_partnerId);
                partnerAccountNo = partner.AccountNumber;
            }

            var siteAccontNo = siteSupplierInfo != null ? siteSupplierInfo.AccountNo : accountSupplierInfo;


            if (string.IsNullOrEmpty(siteAccontNo) || string.IsNullOrEmpty(partnerAccountNo))
                return false;

            if (siteAccontNo == partnerAccountNo)
                return false;

            return true;
        }
    }
}