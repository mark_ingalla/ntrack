﻿using System;
using EventStore.RavenDb;
using PalletPlus.Domain.infrastructure;
using StructureMap;

namespace PalletPlus.Domain.Validation
{
    public class TransporterExists : ISpecification<Models.Movement>
    {
        public IRepository Repository { get; set; }

        public Guid TransporterId { get; set; }
        public Models.Transporter Transporter { get; set; }


        public TransporterExists(Guid transporterId)
        {
            if (transporterId == Guid.Empty)
                throw new ArgumentNullException();


            Repository = ObjectFactory.GetInstance<IRepository>();

            Transporter = Repository.GetById<Models.Transporter>(transporterId);
        }

        public bool IsSatisfiedBy(Models.Movement entity)
        {
            return Transporter.Version > 0;
        }
    }
}
