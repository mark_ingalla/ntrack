﻿using System;
using System.Linq;
using CommonDomain.Persistence;
using StructureMap;
using StructureMap.Attributes;
using nTrack.Core.Services;

namespace PalletPlus.Domain.Validation
{
    public class IsPartnerEnabledInAccount : ISpecification<Models.Site>
    {
        [SetterProperty]
        public IRepository Repository { get; set; }

        public Models.Partner Partner { get; set; }
        public Models.Site Site { get; set; }
        public Models.Account Account { get; set; }

        public IsPartnerEnabledInAccount(Guid accountId, Guid partnerId)
        {
            if (accountId == Guid.Empty || partnerId == Guid.Empty)
                throw new ArgumentNullException();

            Repository = ObjectFactory.GetInstance<IRepository>();
            Account = Repository.GetById<Models.Account>(accountId);
            Partner = Repository.GetById<Models.Partner>(partnerId);
        }

        public bool IsSatisfiedBy(Models.Site entity)
        {
            return Account.AccountPartners.Any(x => x == Partner.Id);
        }
    }
}
