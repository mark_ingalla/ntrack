﻿using System;

namespace Subscription.Messages.Events.Account
{
    public class UserInvited
    {
        public Guid AccountId { get; set; }
        public string Email { get; set; }

        public UserInvited(Guid accountId, string email)
        {
            AccountId = accountId;
            Email = email;
        }
    }
}
