using System;
using nTrack.Core.Messages;

namespace Subscription.Messages.Events.Account
{
    public class InvitationStatusChanged
    {
        public Guid AccountId { get; set; }
        public string UserEmail { get; set; }
        public string InvitationStatus { get; set; }

        public InvitationStatusChanged(Guid accountId, string userEmail, string invitationStatus)
        {
            AccountId = accountId;
            UserEmail = userEmail;
            InvitationStatus = invitationStatus;
        }
    }
}