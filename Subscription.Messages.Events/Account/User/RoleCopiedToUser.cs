﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Subscription.Messages.Events.Account.User
{
    public class RoleCopiedToUser
    {
        public Guid AccountId { get; set; }
        public Guid RoleId { get; set; }
        public Guid UserId { get; set; }

        public RoleCopiedToUser(Guid accountId, Guid roleId, Guid userId)
        {
            AccountId = accountId;
            RoleId = roleId;
            UserId = userId;
        }
    }
}
