﻿using System;

namespace Subscription.Messages.Events.Account.User
{
    public class UserPasswordChanged
    {
        public Guid AccountId { get; set; }
        public Guid UserKey { get; set; }
        public string Password { get; set; }

        public UserPasswordChanged(Guid accountId, Guid userKey, string password)
        {
            UserKey = userKey;
            AccountId = accountId;
            Password = password;
        }
    }
}
