﻿using System;

namespace Subscription.Messages.Events.Account.User
{
    public class UserRemoved
    {
        public Guid AccountId { get; set; }
        public Guid UserKey { get; set; }

        public UserRemoved(Guid accountId, Guid userKey)
        {
            AccountId = accountId;
            UserKey = userKey;
        }
    }
}
