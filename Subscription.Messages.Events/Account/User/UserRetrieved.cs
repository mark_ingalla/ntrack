﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Subscription.Messages.Events.Account.User
{
    public class UserRetrieved
    {
        public Guid AccountId { get; set; }
        public Guid UserKey { get; set; }

        public UserRetrieved(Guid accountId, Guid userKey)
        {
            AccountId = accountId;
            UserKey = userKey;
        }
    }
}
