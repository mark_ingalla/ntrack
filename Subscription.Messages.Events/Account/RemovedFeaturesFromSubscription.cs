﻿using System;
using System.Collections.Generic;

namespace Subscription.Messages.Events.Account
{
    public class RemovedFeaturesFromSubscription
    {
        public Guid AccountId { get; set; }
        public Guid ProductId { get; set; }
        public List<String> Features { get; set; }
        public decimal Amount { get; set; }

        public RemovedFeaturesFromSubscription(Guid accountId, Guid productId, List<string> features)
        {
            ProductId = productId;
            Features = features;
            AccountId = accountId;
        }
    }
}
