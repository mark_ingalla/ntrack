﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Subscription.Messages.Events.Account
{
    public class AccountActivated
    {
        public Guid AccountId { get; set; }

        public AccountActivated(Guid accountId)
        {
            AccountId = accountId;
        }
    }
}
