using System;

namespace Subscription.Messages.Events.Account
{
    public sealed class AccountCreated
    {
        public Guid AccountId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyPhone { get; set; }
        public string Country { get; set; }
        public string AddressIp { get; set; }

        public AccountCreated(Guid accountId, string companyName, string companyPhone, string country, string addressIp)
        {
            AccountId = accountId;
            CompanyName = companyName;
            CompanyPhone = companyPhone;
            Country = country;
            AddressIp = addressIp;
        }
    }
}