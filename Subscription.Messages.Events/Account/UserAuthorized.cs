﻿using System;

namespace Subscription.Messages.Events.Account
{
    public class UserAuthorized
    {
        public Guid AccountId { get; set; }
        public Guid UserId { get; set; }

        public UserAuthorized(Guid accountId, Guid userId)
        {
            AccountId = accountId;
            UserId = userId;
        }
    }
}
