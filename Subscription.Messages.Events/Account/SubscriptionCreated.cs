﻿using System;

namespace Subscription.Messages.Events.Account
{
    public sealed class SubscriptionCreated
    {
        public Guid AccountId { get; set; }
        public Guid ProductId { get; set; }
        public string ProductCode { get; set; }
        public Guid SubscriptionKey { get; set; }
        public Guid OfferKey { get; set; }
        public DateTime SubscribedOnUTC { get; set; }
        public decimal Amount { get; set; }
        public int OfferQTY { get; set; }

        public SubscriptionCreated(Guid accountId, Guid productId, string productCode, Guid subscriptionKey, Guid offerKey, DateTime subscribedOnUTC, decimal amount, int offerQTY)
        {
            AccountId = accountId;
            ProductId = productId;
            ProductCode = productCode;
            SubscriptionKey = subscriptionKey;
            OfferKey = offerKey;
            SubscribedOnUTC = subscribedOnUTC;
            Amount = amount;
            OfferQTY = offerQTY;
        }
    }
}
