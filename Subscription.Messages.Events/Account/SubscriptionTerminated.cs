using System;

namespace Subscription.Messages.Events.Account
{
    public sealed class SubscriptionTerminated
    {
        public Guid AccountId { get; set; }
        public Guid ProductId { get; set; }
        public DateTime TerminatedOnUTC { get; set; }
        public Guid SubscriptionKey { get; set; }

        public SubscriptionTerminated(Guid accountId, Guid productId, DateTime terminatedOnUTC, Guid subscriptionKey)
        {
            AccountId = accountId;
            ProductId = productId;
            TerminatedOnUTC = terminatedOnUTC;
            SubscriptionKey = subscriptionKey;
        }
    }
}