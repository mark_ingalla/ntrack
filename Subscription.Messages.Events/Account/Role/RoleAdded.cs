﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Subscription.Messages.Events.Account.Role
{
    public class RoleAdded
    {
        public Guid AccountId { get; set; }
        public Guid RoleId { get; set; }
        public string Name { get; set; }
        public Dictionary<Guid, List<string>> Permissions { get; set; }

        public RoleAdded(Guid accountId, Guid id, string name, Dictionary<Guid, List<string>> permissions)
        {
            AccountId = accountId;
            RoleId = id;
            Name = name;
            Permissions = permissions;
        }
    }
}
