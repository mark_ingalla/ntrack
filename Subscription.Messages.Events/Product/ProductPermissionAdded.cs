using System;

namespace Subscription.Messages.Events.Product
{
    public class ProductPermissionAdded
    {
        public Guid ProductId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        public ProductPermissionAdded(Guid productId, string code, string name)
        {
            ProductId = productId;
            Code = code;
            Name = name;
        }
    }
}