﻿
namespace Subscription.Messages.Events.Product
{
    public class OfferFeature
    {
        public string Code { get; set; }
        public int Limit { get; set; }
    }
}
