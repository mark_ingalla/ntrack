using System;
using System.Collections.Generic;

namespace Subscription.Messages.Events.Product
{
    [Obsolete("A newer version exists")]
    public class OfferFeaturesAdded
    {
        public Guid ProductId { get; set; }
        public Guid OfferKey { get; set; }
        public string[] FeatureCodes { get; set; }

        public OfferFeaturesAdded(Guid productId, Guid offerKey, string[] featureCodes)
        {
            ProductId = productId;
            OfferKey = offerKey;
            FeatureCodes = featureCodes;
        }
    }

    [Obsolete("A newer version exists. Use OfferFeaturesUpdated. ")]
    public class OfferFeaturesAdded_V2
    {
        public Guid ProductId { get; set; }
        public Guid OfferKey { get; set; }
        public List<OfferFeature> Features { get; set; }

        public OfferFeaturesAdded_V2(Guid productId, Guid offerKey, List<OfferFeature> offerFeatures)
        {
            ProductId = productId;
            OfferKey = offerKey;
            Features = offerFeatures;
        }
    }

    public class OfferFeaturesUpdated
    {
        public Guid ProductId { get; set; }
        public Guid OfferKey { get; set; }
        public List<OfferFeature> Features { get; set; }

        public OfferFeaturesUpdated(Guid productId, Guid offerKey, List<OfferFeature> offerFeatures)
        {
            ProductId = productId;
            OfferKey = offerKey;
            Features = offerFeatures;
        }
    }
}