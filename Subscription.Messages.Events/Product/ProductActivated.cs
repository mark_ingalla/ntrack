using System;

namespace Subscription.Messages.Events.Product
{
    public class ProductActivated
    {
        public Guid ProductId { get; set; }

        public ProductActivated(Guid productId)
        {
            ProductId = productId;
        }
    }
}