using System;

namespace Subscription.Messages.Events.Product
{
    public class ProductUpdated
    {
        public Guid ProductId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string AccessUrl { get; set; }
        public decimal BasePrice { get; set; }

        public ProductUpdated(Guid productId, string name, string code, string description, string accessUrl, decimal basePrice)
        {
            ProductId = productId;
            Name = name;
            Code = code;
            Description = description;
            BasePrice = basePrice;
            AccessUrl = accessUrl;
        }
    }
}