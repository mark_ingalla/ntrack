using System;

namespace Subscription.Messages.Events.Product
{
    public class OfferRemoved
    {
        public Guid ProductId { get; set; }
        public Guid OfferKey { get; set; }
        public string Reason { get; set; }

        public OfferRemoved(Guid productId, Guid offerKey, string reason)
        {
            ProductId = productId;
            OfferKey = offerKey;
            Reason = reason;
        }
    }
}