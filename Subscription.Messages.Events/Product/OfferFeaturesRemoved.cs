using System;

namespace Subscription.Messages.Events.Product
{
    [Obsolete("Use OfferFeaturesUpdated. ")]
    public class OfferFeaturesRemoved
    {
        public Guid ProductId { get; set; }
        public Guid OfferKey { get; set; }
        public string[] FeatureCodes { get; set; }


        public OfferFeaturesRemoved(Guid productId, Guid offerKey, string[] featureCodes)
        {
            ProductId = productId;
            OfferKey = offerKey;
            FeatureCodes = featureCodes;
        }
    }
}