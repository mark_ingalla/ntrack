using System;

namespace Subscription.Messages.Events.Product
{
    public class ProductDiscontinued
    {
        public Guid ProductId { get; set; }
        public string Reason { get; set; }

        public ProductDiscontinued(Guid productId, string reason)
        {
            ProductId = productId;
            Reason = reason;
        }
    }
}