using System;

namespace Subscription.Messages.Events.Product
{
    public class FeatureRemoved
    {
        public Guid ProductId { get; set; }
        public string FeatureCode { get; set; }
        public string Reason { get; set; }

        public FeatureRemoved(Guid productId, string featureCode, string reason)
        {
            ProductId = productId;
            FeatureCode = featureCode;
            Reason = reason;
        }
    }
}