using System;

namespace Subscription.Messages.Events.Product
{
    public class FeatureActivated
    {
        public Guid ProductId { get; set; }
        public string FeatureCode { get; set; }

        public FeatureActivated(Guid productId, string featureCode)
        {
            ProductId = productId;
            FeatureCode = featureCode;
        }
    }
}