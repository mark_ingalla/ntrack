﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using nTrack.Core.Utilities;

namespace ClientPortal.Utilities
{
    public static class ModelStateExtensions
    {
        /// <summary>
        /// Adds an error for the property if the property does not already have an error associated with it. If the error is not
        /// associated with a certain property (propertyName is null or empty), then the error is added anyway.
        /// </summary>
        public static void AddErrorIfPropertyHasNoError(this ModelStateDictionary modelState, string propertyName, string errorMessage)
        {
            if (string.IsNullOrEmpty(propertyName) || !modelState.Keys.Any(key => key == propertyName && modelState[key].Errors.Count > 0))
                modelState.AddModelError(propertyName ?? string.Empty, errorMessage);
        }

        /// <summary>
        /// Adds the model errors for properties that do not already have an error in the ModelStateDictionary. Errors that are not associated
        /// with a certain property are always added.
        /// </summary>
        public static void AddModelErrors(this ModelStateDictionary modelState, object model)
        {
            AddModelErrors(modelState, model, null);
        }

        /// <summary>
        /// Adds the model errors for properties that do not already have an error in the ModelStateDictionary. Errors that are not associated
        /// with a certain property are always added. The propertyToDisplayName dictionary is used to replace property names by display names
        /// in the error messages.
        /// </summary>
        public static void AddModelErrors(this ModelStateDictionary modelState, object model, Dictionary<string, string> propertyToDisplayName)
        {
            ICollection<ValidationResult> validationErrors = new ValidationUtilities().GetValidationErrors(model);
            if (propertyToDisplayName != null && propertyToDisplayName.Count > 0)
            {
                string propertyName;
                foreach (ValidationResult error in validationErrors)
                {
                    propertyName = error.MemberNames.FirstOrDefault();
                    if (!string.IsNullOrEmpty(propertyName) && propertyToDisplayName.ContainsKey(propertyName))
                        error.ErrorMessage = error.ErrorMessage.Replace(propertyName, propertyToDisplayName[propertyName]);
                }
            }

            foreach (ValidationResult error in validationErrors)
                modelState.AddErrorIfPropertyHasNoError(error.MemberNames.FirstOrDefault(), error.ErrorMessage);
        }
    }
}