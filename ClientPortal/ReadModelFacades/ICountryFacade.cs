﻿
namespace ClientPortal.ReadModelFacades
{
    public interface ICountryFacade
    {
        string[] GetAllCountries();
    }
}