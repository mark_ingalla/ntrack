﻿using System;
using System.Linq;
using ClientPortal.Models;
using nTrack.ReadModels;
using Raven.Client;
using Raven.Client.Linq;

namespace ClientPortal.ReadModelFacades
{
    public class ProductFacade : IProductFacade
    {
        private readonly IDocumentSession _dbSession;

        public ProductFacade(IDocumentSession dbSession)
        {
            _dbSession = dbSession;
        }


        public ProductSummary[] GetAllProductSummaries()
        {
            return (from product in _dbSession.Query<Product>().ToArray()
                    select new ProductSummary
                    {
                        ID = product.Id,
                        Name = product.Name
                    }).ToArray();
        }

        public Product GetProduct(Guid productID)
        {
            return _dbSession.Load<Product>(productID);
        }

        public Product[] GetProducts(Guid[] productIDs)
        {
            return (from product in _dbSession.Query<Product>()
                    where product.Id.In(productIDs)
                    select product).ToArray();
        }

        public string GetProductName(Guid productID)
        {
            string productName = null;

            Product product = _dbSession.Load<Product>(productID);
            if (product != null)
                productName = product.Name;

            return productName;
        }
    }
}