﻿using System;

namespace ClientPortal.ReadModelFacades
{
    public interface IEmailVerificationInfoFacade
    {
        bool VerifyToken(Guid token, out Guid userID);
    }
}