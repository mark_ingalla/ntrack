﻿using System;
using nTrack.ReadModels;
using Raven.Client;

namespace ClientPortal.ReadModelFacades
{
    public class AdminAccountsFacade : IAdminAccountsFacade
    {
        private readonly IDocumentSession _dbSession;

        public AdminAccountsFacade(IDocumentSession dbSession)
        {
            _dbSession = dbSession;
        }


        public AccountMinimal[] GetAccountsAdministeredByUser(Guid userID)
        {
            AccountMinimal[] accounts;

            AdminAccounts adminAccounts = _dbSession.Load<AdminAccounts>(userID);
            if (adminAccounts == null)
                accounts = new AccountMinimal[0];
            else
                accounts = adminAccounts.Accounts.ToArray();

            return accounts;
        }
    }
}