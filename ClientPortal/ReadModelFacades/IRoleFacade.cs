﻿using System;

namespace ClientPortal.ReadModelFacades
{
    public interface IRoleFacade
    {
        string[] GetAccountRoleNames(Guid accountID);
    }
}