﻿using System;
using System.Linq;
using nTrack.ReadModels;
using Raven.Client;

namespace ClientPortal.ReadModelFacades
{
    public class AccountUsersFacade : IAccountUsersFacade
    {
        private readonly IDocumentSession _dbSession;

        public AccountUsersFacade(IDocumentSession dbSession)
        {
            _dbSession = dbSession;
        }


        public AccountUsers GetAccountUsers(Guid accountID)
        {
            return _dbSession.Load<AccountUsers>(accountID);
        }

        public AccountUser GetAccountUser(Guid accountID, Guid userID)
        {
            return _dbSession.Load<AccountUsers>(accountID).Users.FirstOrDefault(user => user.UserID == userID);
        }
    }
}