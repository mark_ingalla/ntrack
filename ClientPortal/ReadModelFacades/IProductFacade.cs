﻿using System;
using ClientPortal.Models;
using nTrack.ReadModels;

namespace ClientPortal.ReadModelFacades
{
    public interface IProductFacade
    {
        ProductSummary[] GetAllProductSummaries();
        Product GetProduct(Guid productID);
        Product[] GetProducts(Guid[] productIDs);
        string GetProductName(Guid productID);
    }
}