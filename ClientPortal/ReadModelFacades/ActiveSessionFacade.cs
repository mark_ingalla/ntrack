﻿using System;
using System.Linq;
using Raven.Client;
using nTrack.ReadModels;

namespace ClientPortal.ReadModelFacades
{
    public class ActiveSessionFacade : IActiveSessionFacade
    {
        private readonly IDocumentSession _dbSession;

        public ActiveSessionFacade(IDocumentSession dbSession)
        {
            _dbSession = dbSession;
        }


        public Guid? GetUserID(Guid sessionToken)
        {
            ActiveSession session =  _dbSession.Query<ActiveSession>().Where(s => s.SessionToken == sessionToken).FirstOrDefault();
            return session == null ? (Guid?)null : session.UserID;
        }

        public bool SessionTokenExists(Guid sessionToken)
        {
            return _dbSession.Query<ActiveSession>().Any(s => s.SessionToken == sessionToken);
        }
    }
}