﻿using System;
using System.Linq;
using nTrack.ReadModels;
using Raven.Client;

namespace ClientPortal.ReadModelFacades
{
    public class EmailVerificationInfoFacade : IEmailVerificationInfoFacade
    {
        private readonly IDocumentSession _dbSession;

        public EmailVerificationInfoFacade(IDocumentSession dbSession)
        {
            _dbSession = dbSession;
        }


        public bool VerifyToken(Guid token, out Guid userID)
        {
            EmailVerificationInfo verificationInfo =
                _dbSession.Query<EmailVerificationInfo>().FirstOrDefault(info => info.VerificationToken == token);
            if (verificationInfo != null)
                userID = verificationInfo.UserID;
            else
                userID = Guid.Empty;

            return verificationInfo != null;
        }
    }
}