﻿using System;
using nTrack.ReadModels;

namespace ClientPortal.ReadModelFacades
{
    public interface IAdminAccountsFacade
    {
        AccountMinimal[] GetAccountsAdministeredByUser(Guid userID);
    }
}