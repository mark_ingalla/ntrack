﻿using System;

namespace ClientPortal.ReadModelFacades
{
    public interface IUserInvitationFacade
    {
        bool ValidateInvitationToken(Guid invitationToken, out string email);
    }
}