﻿using System;
using nTrack.ReadModels;
using Raven.Client;

namespace ClientPortal.ReadModelFacades
{
    public class UserInvitationFacade : IUserInvitationFacade
    {
        private readonly IDocumentSession _dbSession;

        public UserInvitationFacade(IDocumentSession dbSession)
        {
            _dbSession = dbSession;
        }


        public bool ValidateInvitationToken(Guid invitationToken, out string email)
        {
            UserInvitation userInvitation = _dbSession.Load<UserInvitation>(invitationToken);
            if (userInvitation == null)
                email = null;
            else
                email = userInvitation.Email;

            return userInvitation != null;
        }
    }
}