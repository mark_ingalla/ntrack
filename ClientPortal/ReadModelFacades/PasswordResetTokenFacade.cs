﻿using System;
using nTrack.ReadModels;
using Raven.Client;

namespace ClientPortal.ReadModelFacades
{
    public class PasswordResetTokenFacade : IPasswordResetTokenFacade
    {
        private readonly IDocumentSession _dbSession;

        public PasswordResetTokenFacade(IDocumentSession dbSession)
        {
            _dbSession = dbSession;
        }


        public bool ValidateToken(Guid token)
        {
            return _dbSession.Load<PasswordResetToken>(token) != null;
        }
    }
}