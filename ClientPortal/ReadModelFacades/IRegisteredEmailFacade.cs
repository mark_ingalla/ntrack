﻿
namespace ClientPortal.ReadModelFacades
{
    public interface IRegisteredEmailFacade
    {
        bool IsEmailRegistered(string email);
    }
}