﻿using System.Linq;
using nTrack.ReadModels;
using Raven.Client;

namespace ClientPortal.ReadModelFacades
{
    public class CountryFacade : ICountryFacade
    {
        private readonly IDocumentSession _dbSession;

        public CountryFacade(IDocumentSession dbSession)
        {
            _dbSession = dbSession;
        }


        public string[] GetAllCountries()
        {
            return _dbSession.Query<Countries>().FirstOrDefault().CountryNames;
        }
    }
}