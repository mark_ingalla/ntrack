﻿using System;
using nTrack.ReadModels;

namespace ClientPortal.ReadModelFacades
{
    public interface IAccountSubscriptionsFacade
    {
        Guid[] GetAccountSubscriptionProductIDs(Guid accountID);
        string[] GetSubscriptionFeatureCodes(Guid accountID, Guid productID);
    }
}