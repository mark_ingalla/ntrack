﻿using System.Linq;
using nTrack.ReadModels;
using Raven.Client;

namespace ClientPortal.ReadModelFacades
{
    public class RegisteredEmailFacade : IRegisteredEmailFacade
    {
        private readonly IDocumentSession _dbSession;

        public RegisteredEmailFacade(IDocumentSession dbSession)
        {
            _dbSession = dbSession;
        }


        public bool IsEmailRegistered(string email)
        {
            return _dbSession.Query<RegisteredEmail>().Any(registeredEmail => registeredEmail.Email == email);
        }
    }
}