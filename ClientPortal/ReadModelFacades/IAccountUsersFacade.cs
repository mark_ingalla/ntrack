﻿using System;
using nTrack.ReadModels;

namespace ClientPortal.ReadModelFacades
{
    public interface IAccountUsersFacade
    {
        AccountUsers GetAccountUsers(Guid accountID);
        AccountUser GetAccountUser(Guid accountID, Guid userID);
    }
}