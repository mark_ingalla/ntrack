﻿using System;

namespace ClientPortal.ReadModelFacades
{
    public interface IPasswordResetTokenFacade
    {
        bool ValidateToken(Guid token);
    }
}