﻿using System;
using System.Linq;
using nTrack.ReadModels;
using Raven.Client;

namespace ClientPortal.ReadModelFacades
{
    public class AccountSubscriptionsFacade : IAccountSubscriptionsFacade
    {
        private readonly IDocumentSession _dbSession;

        public AccountSubscriptionsFacade(IDocumentSession dbSession)
        {
            _dbSession = dbSession;
        }


        public Guid[] GetAccountSubscriptionProductIDs(Guid accountID)
        {
            Guid[] productIDs;
            
            AccountSubscriptions accountSubscriptions = _dbSession.Load<AccountSubscriptions>(accountID);
            if (accountSubscriptions == null)
                productIDs = new Guid[0];
            else
                productIDs = accountSubscriptions.Subscriptions.Keys.ToArray();

            return productIDs;
        }

        public string[] GetSubscriptionFeatureCodes(Guid accountID, Guid productID)
        {
            string[] featureCodes;

            AccountSubscriptions accountSubscriptions = _dbSession.Load<AccountSubscriptions>(accountID);
            if (accountSubscriptions != null && accountSubscriptions.Subscriptions.ContainsKey(productID))
                featureCodes = accountSubscriptions.Subscriptions[productID].ToArray();
            else
                featureCodes = new string[0];

            return featureCodes;
        }
    }
}