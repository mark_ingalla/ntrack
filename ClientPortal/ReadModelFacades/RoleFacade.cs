﻿using System;
using Raven.Client;
using System.Linq;
using nTrack.ReadModels;

namespace ClientPortal.ReadModelFacades
{
    public class RoleFacade : IRoleFacade
    {
        private readonly IDocumentSession _dbSession;

        public RoleFacade(IDocumentSession dbSession)
        {
            _dbSession = dbSession;
        }


        public string[] GetAccountRoleNames(Guid accountID)
        {
            return null;
            //return _dbSession.Query<UserRole>().Where(r => r.AccountID == accountID).Select(r => r.Name).ToArray();
        }
    }
}