﻿using System;
using nTrack.ReadModels;

namespace ClientPortal.ReadModelFacades
{
    public interface IUserFacade
    {
        User GetUserByEmail(string email);
        string GetUserFullName(Guid userID);
    }
}