﻿using System;

namespace ClientPortal.ReadModelFacades
{
    public interface IActiveSessionFacade
    {
        Guid? GetUserID(Guid sessionToken);
        bool SessionTokenExists(Guid sessionToken);
    }
}