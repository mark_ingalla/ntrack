﻿using System;
using System.Linq;
using nTrack.ReadModels;
using Raven.Client;

namespace ClientPortal.ReadModelFacades
{
    public class UserFacade : IUserFacade
    {
        private readonly IDocumentSession _dbSession;

        public UserFacade(IDocumentSession dbSession)
        {
            _dbSession = dbSession;
        }


        public User GetUserByEmail(string email)
        {
            return _dbSession.Query<User>().FirstOrDefault(user => user.Email == email);
        }
        
        public string GetUserFullName(Guid userID)
        {
            User user = _dbSession.Load<User>(userID);
            return user.FirstName + " " + user.LastName;
        }
    }
}