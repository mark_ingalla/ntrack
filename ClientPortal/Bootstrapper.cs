﻿using System.Web.Mvc;
using ClientPortal.Infrastructure;
using ClientPortal.ReadModelFacades;
using ClientPortal.Services;
using NServiceBus;
using nTrack.Core.Utilities;
using StructureMap;
using Raven.Client.Document;
using Raven.Client;

namespace ClientPortal
{
    public static class Bootstrapper
    {
        public static void Bootup()
        {
            IContainer container = BuildContainer();
            DependencyResolver.SetResolver(new StructureMapDependencyResolver(container));

            BuildServiceBus(container);
            BuildDocumentStore(container);
        }


        private static IContainer BuildContainer()
        {
            IContainer container = new Container();
            container.Configure(
                x =>
                {
                    x.Scan(
                        y =>
                        {
                            y.AssembliesFromApplicationBaseDirectory();
                            y.TheCallingAssembly();
                            y.WithDefaultConventions();
                        });

                    x.For<IApplicationSettingServices>().Use<ApplicationSettingServices>();
                    x.For<IClientSideStoreServices>().Use<ClientSideStoreServices>();
                    x.For<ICryptographyUtilities>().Use<CryptographyUtilities>();

                    x.For<IActiveSessionFacade>().Use<ActiveSessionFacade>();
                    x.For<IRegisteredEmailFacade>().Use<RegisteredEmailFacade>();
                    x.For<ICountryFacade>().Use<CountryFacade>();
                    x.For<IEmailVerificationInfoFacade>().Use<EmailVerificationInfoFacade>();
                    x.For<IAdminAccountsFacade>().Use<AdminAccountsFacade>();
                    x.For<IUserFacade>().Use<UserFacade>();
                    x.For<IProductFacade>().Use<ProductFacade>();
                    x.For<IAccountSubscriptionsFacade>().Use<AccountSubscriptionsFacade>();
                    x.For<IUserInvitationFacade>().Use<UserInvitationFacade>();
                    x.For<IAccountUsersFacade>().Use<AccountUsersFacade>();
                });

            return container;
        }

        private static void BuildServiceBus(IContainer container)
        {
            IBus bus = NServiceBus.Configure.With()
                .DefiningCommandsAs(type => type != null && type.Namespace == "Subscription.Messages.Commands")
                .DefiningEventsAs(type => type != null && type.Namespace == "Subscription.Messages.Events")
                .DefiningMessagesAs(type => type != null && type.Namespace == "Subscription.Messages")
                .StructureMapBuilder(container)
                .Log4Net()
                .DefaultBuilder()
                .JsonSerializer()
                .MsmqTransport()
                    .IsTransactional(false)
                    .PurgeOnStartup(false)
                .UnicastBus()
                    .ImpersonateSender(false)
                .CreateBus()
                .Start();

            container.Configure(x => x.ForSingletonOf<IBus>().Use(bus));
        }

        private static void BuildDocumentStore(IContainer container)
        {
            var documentStore = new DocumentStore { ConnectionStringName = "ReadModelDB" };
            documentStore.Initialize();

            container.Configure(x =>
                {
                    x.ForSingletonOf<IDocumentStore>().Use(documentStore);
                    x.For<IDocumentSession>().Use(ct => ct.GetInstance<IDocumentStore>().OpenSession());
                });
        }
    }
}