﻿using nTrack.ReadModels;

namespace ClientPortal.Areas.AccountAdminPanel.ViewModels
{
    public class UserManagePageViewModel
    {
        public AccountUsers AccountUsers { get; set; }

        public UserManagePageViewModel(AccountUsers accountUsers)
        {
            AccountUsers = accountUsers;
        }
    }
}