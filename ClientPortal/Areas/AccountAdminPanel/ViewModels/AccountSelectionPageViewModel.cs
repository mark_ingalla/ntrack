﻿using nTrack.ReadModels;

namespace ClientPortal.Areas.AccountAdminPanel.ViewModels
{
    public class AccountSelectionPageViewModel
    {
        public AccountMinimal[] AccountMinimals { get; private set; }

        public AccountSelectionPageViewModel(AccountMinimal[] accountOptions)
        {
            AccountMinimals = accountOptions;
        }
    }
}