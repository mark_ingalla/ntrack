﻿
namespace ClientPortal.Areas.AccountAdminPanel.ViewModels
{
    public class RoleIndexPageViewModel
    {
        public string[] RoleNames { get; set; }

        public RoleIndexPageViewModel(string[] roleNames)
        {
            RoleNames = roleNames;
        }
    }
}