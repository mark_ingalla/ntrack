﻿using System;
using nTrack.ReadModels;

namespace ClientPortal.Areas.AccountAdminPanel.ViewModels
{
    public class SubscriptionNewPageViewModel
    {
        public Product Product { get; private set; }

        public string[] SelectedFeatureCodes { get; set; }

        public SubscriptionNewPageViewModel()
        { }

        public SubscriptionNewPageViewModel(Product product)
        {
            Product = product;
        }
    }
}