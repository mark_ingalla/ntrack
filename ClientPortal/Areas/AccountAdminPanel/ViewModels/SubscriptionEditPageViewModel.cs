﻿using System;
using nTrack.ReadModels;

namespace ClientPortal.Areas.AccountAdminPanel.ViewModels
{
    public class SubscriptionEditPageViewModel
    {
        public Product Product { get; private set; }
        public string[] SelectedFeatureCodes { get; set; }

        public SubscriptionEditPageViewModel()
        { }
        
        public SubscriptionEditPageViewModel(Product product, string[] subscriptionFeatureCodes)
        {
            Product = product;
            SelectedFeatureCodes = subscriptionFeatureCodes;
        }
    }
}