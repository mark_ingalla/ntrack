﻿
namespace ClientPortal.Areas.AccountAdminPanel.ViewModels
{
    public class UserInvitedPageViewModel
    {
        public string Email { get; set; }

        public UserInvitedPageViewModel(string email)
        {
            Email = email;
        }
    }
}