﻿using nTrack.ReadModels;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System;

namespace ClientPortal.Areas.AccountAdminPanel.ViewModels
{
    public class UserInvitePageViewModel
    {
        public Product[] AccountProducts { get; private set; }

        [Required]
        public string Email { get; set; }
        public string[] SelectedPermissionStrs { get; set; }

        public UserInvitePageViewModel()
        { }
        
        public UserInvitePageViewModel(Product[] accountProducts)
        {
            AccountProducts = accountProducts;
        }

        public Dictionary<Guid, List<string>> GetSelectedPermissions()
        {
            Dictionary<Guid, List<string>> selectedPermissions = new Dictionary<Guid, List<string>>();

            if (SelectedPermissionStrs != null)
            {
                foreach (string selectedPermission in SelectedPermissionStrs)
                {
                    if (selectedPermission != "false")
                    {
                        string[] parts = selectedPermission.Split('|');
                        Guid productID = Guid.Parse(parts[0]);
                        string permissionCode = parts[1];

                        if (!selectedPermissions.ContainsKey(productID))
                            selectedPermissions.Add(productID, new List<string>());

                        selectedPermissions[productID].Add(permissionCode);
                    }
                }
            }

            return selectedPermissions;
        }
    }
}