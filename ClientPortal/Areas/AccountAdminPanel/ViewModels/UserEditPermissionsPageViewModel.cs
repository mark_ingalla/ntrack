﻿using nTrack.ReadModels;
using System.Collections.Generic;
using System;

namespace ClientPortal.Areas.AccountAdminPanel.ViewModels
{
    public class UserEditPermissionsPageViewModel
    {
        public Product[]   AccountProducts { get; private set; }
        public AccountUser User            { get; private set; }

        public string[] SelectedPermissionStrs { get; set; }


        public UserEditPermissionsPageViewModel()
        { }

        public UserEditPermissionsPageViewModel(Product[] accountProducts, AccountUser user)
        {
            AccountProducts = accountProducts;
            User = user;
            SetSelectedPermissions(user.Permissions);
        }


        public Dictionary<Guid, List<string>> GetSelectedPermissions()
        {
            Dictionary<Guid, List<string>> selectedPermissions = new Dictionary<Guid, List<string>>();

            if (SelectedPermissionStrs != null)
            {
                foreach (string selectedPermission in SelectedPermissionStrs)
                {
                    if (selectedPermission != "false")
                    {
                        string[] parts = selectedPermission.Split('|');
                        Guid productID = Guid.Parse(parts[0]);
                        string permissionCode = parts[1];

                        if (!selectedPermissions.ContainsKey(productID))
                            selectedPermissions.Add(productID, new List<string>());

                        selectedPermissions[productID].Add(permissionCode);
                    }
                }
            }

            return selectedPermissions;
        }

        public void SetSelectedPermissions(Dictionary<Guid, List<string>> selectedPermissions)
        {
            List<string> selectedPermissionStrs = new List<string>();

            foreach (KeyValuePair<Guid, List<string>> productPermissions in selectedPermissions)
            {
                foreach (string permissionCode in productPermissions.Value)
                    selectedPermissionStrs.Add(productPermissions.Key + "|" + permissionCode);
            }

            SelectedPermissionStrs = selectedPermissionStrs.ToArray();
        }
    }
}