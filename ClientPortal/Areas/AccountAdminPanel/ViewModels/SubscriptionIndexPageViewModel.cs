﻿using System;
using System.Linq;
using nTrack.ReadModels;
using ClientPortal.Models;

namespace ClientPortal.Areas.AccountAdminPanel.ViewModels
{
    public class SubscriptionIndexPageViewModel
    {
        public ProductSummary[] MyProducts { get; private set; }
        public ProductSummary[] OtherProducts { get; private set; }

        public SubscriptionIndexPageViewModel(ProductSummary[] allProducts, Guid[] subscriptionProductIDs)
        {
            MyProducts = (from product in allProducts
                          where subscriptionProductIDs.Contains(product.ID)
                          select new ProductSummary
                          {
                              ID = product.ID,
                              Name = product.Name
                          }).ToArray();

            OtherProducts = (from product in allProducts
                             where !subscriptionProductIDs.Contains(product.ID)
                             select new ProductSummary
                             {
                                 ID = product.ID,
                                 Name = product.Name
                             }).ToArray();
        }
    }
}