﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientPortal.Areas.AccountAdminPanel.ViewModels;
using nTrack.ReadModels;
using ClientPortal.ReadModelFacades;
using Subscription.Messages.Commands.Account;
using NServiceBus;
using Subscription.Messages.Commands.User;
using nTrack.Core.Utilities;
using ClientPortal.Controllers;
using nTrack.Core.Messages;

namespace ClientPortal.Areas.AccountAdminPanel.Controllers
{
    public class UserController : AdminPanelControllerBase
    {
        private readonly IBus _bus;
        private readonly IAccountUsersFacade _accountUsersFacade;
        private readonly IAccountSubscriptionsFacade _accountSubscriptionsFacade;
        private readonly IProductFacade _productFacade;

        private readonly IThreadingUtilities _threadingUtilities;

        public UserController(IBus bus, IAccountUsersFacade accountUsersFacade, IAccountSubscriptionsFacade accountSubscriptionFacade,
            IProductFacade productFacade, IThreadingUtilities threadingUtilities)
        {
            _bus = bus;
            _accountUsersFacade = accountUsersFacade;
            _accountSubscriptionsFacade = accountSubscriptionFacade;
            _productFacade = productFacade;

            _threadingUtilities = threadingUtilities;
        }


        //
        // GET: /AccountAdminPanel/User/Manage
        public ActionResult Manage()
        {
            return ManageUsersView();
        }

        //
        // POST: /AccountAdminPanel/User/Manage
        [HttpPost]
        public ActionResult Manage(Guid userID, string action)
        {
            if (action == "PromoteToAdmin")
            {
                _bus.Send(new PromoteUserToAdmin(userID, ContextAccountID));
                _threadingUtilities.WaitOnPredicate(ControllerSettings.WAIT_DELAY_MILLISEC, ControllerSettings.WAIT_TIMEOUT_SEC,
                    () => _accountUsersFacade.GetAccountUser(ContextAccountID, userID).IsAdmin);
            }
            else if (action == "DemoteToUser")
            {
                _bus.Send(new DemoteAdminToUser(userID, ContextAccountID));
                _threadingUtilities.WaitOnPredicate(ControllerSettings.WAIT_DELAY_MILLISEC, ControllerSettings.WAIT_TIMEOUT_SEC,
                    () => !_accountUsersFacade.GetAccountUser(ContextAccountID, userID).IsAdmin);
            }
            else if (action == "Delete")
            {
                _bus.Send(new RevokeUserFromAccount(ContextAccountID, userID));
                _threadingUtilities.WaitOnPredicate(ControllerSettings.WAIT_DELAY_MILLISEC, ControllerSettings.WAIT_TIMEOUT_SEC,
                    () => _accountUsersFacade.GetAccountUser(ContextAccountID, userID) == null);
            }

            return ManageUsersView();
        }
        
        //
        // GET: /AccountAdminPanel/User/Invite
        public ActionResult Invite()
        {
            return InviteUserView();
        }

        //
        // POST: /AccountAdminPanel/User/Invite
        [HttpPost]
        public ActionResult Invite(UserInvitePageViewModel vm)
        {
            ActionResult actionResult;

            if (!ModelState.IsValid)
            {
                actionResult = InviteUserView();
            }
            else
            {
                Guid invitationToken = Guid.NewGuid();
                _bus.Send(new InviteUser
                {
                    AccountId = ContextAccountID,
                    EmailInvitationToken = invitationToken,
                    ProductsPersmissions = vm.GetSelectedPermissions(),
                    RoleName = "User",
                    UserEmail = vm.Email
                });

                actionResult = View("Invited", new UserInvitedPageViewModel(vm.Email));

                /* Begin test code */
                ViewBag.AcceptInvitationUrl = @Url.Action(
                        "Accept", "Invitation", new { area = "", invitationToken = invitationToken }, Request.Url.Scheme);
                /* End test code */
            }

            return actionResult;
        }

        //
        // GET: /AccountAdminPanel/User/EditPermissions?userID=***
        public ActionResult EditPermissions(Guid userID)
        {
            AccountUser user = _accountUsersFacade.GetAccountUser(ContextAccountID, userID);
            return EditPermissionsView(user);
        }

        //
        // POST: /AccountAdminPanel/User/EditPermission?userID=***
        [HttpPost]
        public ActionResult EditPermissions(Guid userID, UserEditPermissionsPageViewModel vm)
        {
            // Compare the old user permissions with the new user permissions to get the lists of added and removed permissions.
            List<KeyValuePair<Guid, string>> addedPermissions = new List<KeyValuePair<Guid, string>>();
            List<KeyValuePair<Guid, string>> removedPermissions = new List<KeyValuePair<Guid,string>>();
            AccountUser user = _accountUsersFacade.GetAccountUser(ContextAccountID, userID);
            Dictionary<Guid, List<string>> oldPermissions = user.Permissions;
            Dictionary<Guid, List<string>> newPermissions = vm.GetSelectedPermissions();
            foreach (KeyValuePair<Guid, List<string>> newProductPermissions in newPermissions)
	        {
                foreach (string permissionCode in newProductPermissions.Value)
                {
                    if (!oldPermissions.ContainsKey(newProductPermissions.Key) || !oldPermissions[newProductPermissions.Key].Contains(permissionCode))
                        addedPermissions.Add(new KeyValuePair<Guid, string>(newProductPermissions.Key, permissionCode));
                }
	        }
            foreach (KeyValuePair<Guid, List<string>> oldProductPermissions in oldPermissions)
            {
                foreach (string permissionCode in oldProductPermissions.Value)
                {
                    if (!newPermissions.ContainsKey(oldProductPermissions.Key) || !newPermissions[oldProductPermissions.Key].Contains(permissionCode))
                        removedPermissions.Add(new KeyValuePair<Guid, string>(oldProductPermissions.Key, permissionCode));
                }
            }

            // Send a command for  each added/removed permission.
            List<AddPermissionToUser> addPermissionCommands = new List<AddPermissionToUser>();
            foreach (KeyValuePair<Guid, string> addedPermission in addedPermissions)
            {
                addPermissionCommands.Add(new AddPermissionToUser(
                    userID, ContextAccountID, AdminPanelControllerSettings.USER_ROLE, addedPermission.Key, addedPermission.Value));
            }
            List<RevokePermissionFromUser> revokePermissionCommands = new List<RevokePermissionFromUser>();
            foreach (KeyValuePair<Guid, string> removedPermission in removedPermissions)
            {
                revokePermissionCommands.Add(new RevokePermissionFromUser(
                    userID, ContextAccountID, AdminPanelControllerSettings.USER_ROLE, removedPermission.Key, removedPermission.Value));
            }

            if (addPermissionCommands.Count > 0)
                _bus.Send(addPermissionCommands.ToArray());
            if (revokePermissionCommands.Count > 0)
                _bus.Send(revokePermissionCommands.ToArray());

            ViewBag.ShowUpdateSuccessMessage = true;
            user.Permissions = newPermissions;
            return EditPermissionsView(user);
        }


        private ActionResult InviteUserView()
        {
            Guid[] subscriptionProductIDs = _accountSubscriptionsFacade.GetAccountSubscriptionProductIDs(ContextAccountID);
            Product[] accountProducts = _productFacade.GetProducts(subscriptionProductIDs);
            UserInvitePageViewModel vm = new UserInvitePageViewModel(accountProducts);
            return View("Invite", vm);
        }

        private ActionResult ManageUsersView()
        {
            AccountUsers accountUsers = _accountUsersFacade.GetAccountUsers(ContextAccountID);
            UserManagePageViewModel vm = new UserManagePageViewModel(accountUsers);
            return View("Manage", vm);
        }

        private ActionResult EditPermissionsView(AccountUser user)
        {
            Guid[] accountProductIDs = _accountSubscriptionsFacade.GetAccountSubscriptionProductIDs(ContextAccountID);
            Product[] accountProducts = _productFacade.GetProducts(accountProductIDs);
            UserEditPermissionsPageViewModel vm = new UserEditPermissionsPageViewModel(accountProducts, user);
            return View("EditPermissions", vm);
        }
    }
}