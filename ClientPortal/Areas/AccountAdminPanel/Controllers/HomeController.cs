﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClientPortal.Areas.AccountAdminPanel.Controllers
{
    public class HomeController : AdminPanelControllerBase
    {
        //
        // GET: /AccountAdminPanel/Home/
        public ActionResult Index()
        {
            return View("Index");
        }
    }
}