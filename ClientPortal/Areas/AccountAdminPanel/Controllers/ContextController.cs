﻿using System;
using System.Web.Mvc;
using ClientPortal.Areas.AccountAdminPanel.ViewModels;
using ClientPortal.Infrastructure;
using ClientPortal.ReadModelFacades;
using ClientPortal.Services;
using nTrack.ReadModels;

namespace ClientPortal.Areas.AccountAdminPanel.Controllers
{
    [Authorize]
    public class ContextController : Controller
    {
        private readonly IAdminAccountsFacade _adminAccountsFacade;
        private readonly IClientSideStoreServices _clientSideStoreServices;

        public ContextController(IAdminAccountsFacade adminAccountsFacade, IClientSideStoreServices clientSideStoreServices)
        {
            _adminAccountsFacade = adminAccountsFacade;
            _clientSideStoreServices = clientSideStoreServices;
        }


        //
        // GET: /AccountAdminPanel/Context/AccountSelection
        public ActionResult AccountSelection(bool forceSelection = false)
        {
            ActionResult actionResult;

            Guid userID = ((CustomIdentity)(User.Identity)).ID;
            AccountMinimal[] accountOptions = _adminAccountsFacade.GetAccountsAdministeredByUser(userID);
            if (accountOptions.Length == 0)
                actionResult = new HttpUnauthorizedResult();
            else if (accountOptions.Length == 1 && forceSelection == false)
            {
                _clientSideStoreServices.SetContextAccountID(Response.Cookies, accountOptions[0].Id);
                actionResult = RedirectToAction("Index", "Home");
            }
            else
            {
                AccountSelectionPageViewModel vm = new AccountSelectionPageViewModel(accountOptions);
                actionResult = View("AccountSelection", vm);
            }

            return actionResult;
        }

        //
        // GET: /AccountAdminPanel/Context/SetContextAccount?accountID=***
        public ActionResult SetContextAccount(Guid accountID)
        {
            _clientSideStoreServices.SetContextAccountID(Response.Cookies, accountID);
            return RedirectToAction("Index", "Home");
        }
    }
}