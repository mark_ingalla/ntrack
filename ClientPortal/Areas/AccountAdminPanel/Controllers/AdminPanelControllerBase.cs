﻿using System;
using System.Linq;
using System.Web.Mvc;
using ClientPortal.Infrastructure;
using ClientPortal.ReadModelFacades;
using ClientPortal.Services;
using nTrack.ReadModels;
using StructureMap.Attributes;

namespace ClientPortal.Areas.AccountAdminPanel.Controllers
{
    [Authorize]
    public class AdminPanelControllerBase : Controller
    {
        [SetterProperty]
        public IClientSideStoreServices ClientSideStoreServices { get; set; }
        [SetterProperty]
        public IAdminAccountsFacade AdminAccountsFacade { get; set; }

        protected Guid ContextAccountID { get; private set; }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Guid? contextAccountID = ClientSideStoreServices.GetContextAccountID(Request.Cookies);
            if (contextAccountID == null)
            {
                filterContext.Result = RedirectToAction("AccountSelection", "Context");
            }
            else
            {
                Guid userID = ((CustomIdentity)(User.Identity)).ID;
                AccountMinimal[] accountsAdministeredByUser = AdminAccountsFacade.GetAccountsAdministeredByUser(userID);
                AccountMinimal contextAccount = accountsAdministeredByUser.FirstOrDefault(account => account.Id == contextAccountID.Value);
                if (contextAccount == null)
                {
                    filterContext.Result = RedirectToAction("AccountSelection", "Context");
                }
                else
                {
                    ViewBag.ContextAccount = contextAccount.CompanyName;
                    ContextAccountID = contextAccountID.Value;
                }
            }
            
            base.OnActionExecuting(filterContext);
        }
    }
}
