﻿using System;
using System.Linq;
using System.Web.Mvc;
using ClientPortal.Areas.AccountAdminPanel.ViewModels;
using ClientPortal.Models;
using ClientPortal.ReadModelFacades;
using NServiceBus;
using nTrack.ReadModels;
using Subscription.Messages.Commands.Account;
using System.Collections.Generic;

namespace ClientPortal.Areas.AccountAdminPanel.Controllers
{
    public class SubscriptionController : AdminPanelControllerBase
    {
        private readonly IBus _bus;
        private readonly IProductFacade _productFacade;
        private readonly IAccountSubscriptionsFacade _accountSubscriptionsFacade;

        public SubscriptionController(IBus bus, IProductFacade productFacade, IAccountSubscriptionsFacade accountSubscriptionsFacade)
        {
            _bus = bus;
            _productFacade = productFacade;
            _accountSubscriptionsFacade = accountSubscriptionsFacade;
        }


        //
        // GET: /AccountAdminPanel/Subscription/
        public ActionResult Index()
        {
            ProductSummary[] allProducts = _productFacade.GetAllProductSummaries();
            Guid[] accountSubscriptionProductIDs = _accountSubscriptionsFacade.GetAccountSubscriptionProductIDs(ContextAccountID);
            SubscriptionIndexPageViewModel vm = new SubscriptionIndexPageViewModel(allProducts, accountSubscriptionProductIDs);
            return View("Index", vm);
        }

        //
        // GET: /AccountAdminPanel/Subscription/New?productID=***
        public ActionResult New(Guid productID)
        {
            ActionResult actionResult;

            Product product = _productFacade.GetProduct(productID);
            if (product == null)
            {
                actionResult = View("ProductNotFound", productID);
            }
            else
            {
                SubscriptionNewPageViewModel vm = new SubscriptionNewPageViewModel(product);
                actionResult = View("New", vm);
            }

            return actionResult;
        }

        //
        // POST: /AccountAdminPanel/Subscription/NewSubscription?productID=***
        [HttpPost]
        public ActionResult NewSubscription(Guid productID, SubscriptionNewPageViewModel vm)
        {
            ActionResult actionResult;

            string productName = _productFacade.GetProductName(productID);
            if (productName == null)
            {
                actionResult = View("ProductNotFound", productID);
            }
            else if (vm.SelectedFeatureCodes == null || vm.SelectedFeatureCodes.Length == 0)
            {
                actionResult = View(viewName: "Error", model: "You should select at least one of the product features.");
            }
            else
            {
                _bus.Send(new CreateSubscription
                {
                    AccountId = ContextAccountID,
                    ProductId = productID,
                });

                Dictionary<Guid, List<string>> productFeatures = new Dictionary<Guid,List<string>>
                {
                    { productID, vm.SelectedFeatureCodes.ToList() }
                };
                _bus.Send(new AddFeaturesToSubscription(ContextAccountID, productFeatures));

                actionResult = View(viewName: "Subscribed", model: productName);
            }

            return actionResult;
        }

        //
        // GET: /AccountAdminPanel/Subscription/Edit?productID=***
        public ActionResult Edit(Guid productID)
        {
            ActionResult actionResult;

            Product product = _productFacade.GetProduct(productID);
            if (product == null)
            {
                actionResult = View("ProductNotFound", productID);
            }
            else
            {
                string[] subscriptionFeatureCodes = _accountSubscriptionsFacade.GetSubscriptionFeatureCodes(ContextAccountID, productID);
                SubscriptionEditPageViewModel vm = new SubscriptionEditPageViewModel(product, subscriptionFeatureCodes);
                actionResult = View("Edit", vm);
            }

            return actionResult;
        }

        //
        // POST: /AccountAdminPanel/Subscription/Edit?productID=***
        [HttpPost]
        public ActionResult Edit(
            Guid productID, SubscriptionEditPageViewModel vm, string updateFeaturesCommand, string terminateSubscriptionCommand)
        {
            ActionResult actionResult;

            Product product = _productFacade.GetProduct(productID);
            if (product == null)
            {
                actionResult = View("ProductNotFound", productID);
            }
            else
            {
                if (!string.IsNullOrEmpty(updateFeaturesCommand))
                {
                    string[] oldFeatureCodes = _accountSubscriptionsFacade.GetSubscriptionFeatureCodes(ContextAccountID, productID);
                    string[] newFeatureCodes = vm.SelectedFeatureCodes;
                    List<string> addedFeatureCodes = newFeatureCodes.Except(oldFeatureCodes).ToList();
                    List<string> removedFeatureCodes = oldFeatureCodes.Except(newFeatureCodes).ToList();
                    Dictionary<Guid, List<string>> addedFeaturesDictionary = new Dictionary<Guid,List<string>>
                    {
                        { productID, addedFeatureCodes }
                    };
                    Dictionary<Guid, List<string>> removedFeaturesDictionary = new Dictionary<Guid,List<string>>
                    {
                        { productID, removedFeatureCodes }
                    };

                    _bus.Send(new AddFeaturesToSubscription(ContextAccountID, addedFeaturesDictionary));
                    _bus.Send(new RemoveFeaturesFromSubscription(ContextAccountID, removedFeaturesDictionary));

                    ViewBag.ShowUpdateSuccessMessage = true;
                    SubscriptionEditPageViewModel vm2 = new SubscriptionEditPageViewModel(product, vm.SelectedFeatureCodes);
                    actionResult = View("Edit", vm2);
                }
                else if (!string.IsNullOrEmpty(terminateSubscriptionCommand))
                {
                    _bus.Send(new TerminateSubscription(ContextAccountID, productID));
                    actionResult = View(viewName: "Terminated", model: product.Name);
                }
                else
                {
                    actionResult = null;
                }
            }

            return actionResult;
        }

        ////
        //// POST: /AccountAdminPanel/Subscription/AddFeature
        //public void AddFeature(Guid productID, string featureCode)
        //{
        //    AddFeatureToSubscription command = new AddFeatureToSubscription
        //    {
        //        AccountId = ContextAccountID,
        //        ProductId = productID,
        //        FeatureCode = featureCode
        //    };

        //    _bus.Send(command);
        //}

        ////
        //// POST: /AccountAdminPanel/Subscription/RemoveFeature
        //public void RemoveFeature(Guid productID, string featureCode)
        //{
        //    RemoveFeatureFromSubscription command = new RemoveFeatureFromSubscription
        //    {
        //        AccountId = ContextAccountID,
        //        ProductId = productID,
        //        FeatureCode = featureCode
        //    };

        //    _bus.Send(command);
        //}
    }
}