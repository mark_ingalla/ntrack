﻿using System.Web.Mvc;

namespace ClientPortal.Areas.AccountAdminPanel
{
    public class AccountAdminPanelAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "AccountAdminPanel";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "AccountAdminPanel_default",
                "AccountAdminPanel/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
