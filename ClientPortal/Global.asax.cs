﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ClientPortal.Infrastructure;
using System.Security.Principal;
using ClientPortal.Services;
using ClientPortal.ReadModelFacades;
using NServiceBus;
using Subscription.Messages.Commands;
using Subscription.Messages.Commands.User;

namespace ClientPortal
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                new string[] { "ClientPortal.Controllers" });
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            BundleTable.Bundles.EnableDefaultBundles();


            Bootstrapper.Bootup();
        }

        protected void Application_AuthenticateRequest()
        {
            Guid? sessionToken = DependencyResolver.Current.GetService<IClientSideStoreServices>().GetSessionToken(Request.Cookies);
            if (sessionToken != null)
            {
                Guid? userID = DependencyResolver.Current.GetService<IActiveSessionFacade>().GetUserID(sessionToken.Value);
                if (userID != null)
                {
                    string userName = DependencyResolver.Current.GetService<IUserFacade>().GetUserFullName(userID.Value);
                    CustomIdentity identity = new CustomIdentity(userID.Value, userName, "Token");
                    Context.User = new GenericPrincipal(identity, new string[0]);

                    DependencyResolver.Current.GetService<IBus>().Send<SeeToken>(cmd =>
                        {
                            cmd.Token = sessionToken.Value;
                            cmd.SeenOn = DateTime.UtcNow;
                        });
                }
            }
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            // If the exception is caused by an HTTP 401 (Unauthorized) and the user is not authenticated, then redirect to the login page.
            if (Context.Response.StatusCode == 401 &&
                (Context.User == null || Context.User.Identity == null || !Context.User.Identity.IsAuthenticated))
            {
                string loginPageUrl = DependencyResolver.Current.GetService<IApplicationSettingServices>()
                    .GetSettingValue(ApplicationSetting.LoginPageUrl);
                string redirectUrl = string.Concat(loginPageUrl, "?returnUrl=", Server.UrlEncode(Request.RawUrl));
                Response.Redirect(redirectUrl);
            }
        }
    }
}