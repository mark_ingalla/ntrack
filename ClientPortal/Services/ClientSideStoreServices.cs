﻿using System;
using System.Web;

namespace ClientPortal.Services
{
    public class ClientSideStoreServices : IClientSideStoreServices
    {
        private const string AUTHENTICATION_COOKIE_NAME = "Auth";
        private const string SESSION_TOKEN_KEY = "SessionToken";
        private const string CONTEXT_COOKIE_NAME = "Context";
        private const string CONTEXT_ACCOUNT_KEY = "Account";


        public Guid? GetSessionToken(HttpCookieCollection requestCookies)
        {
            Guid? sessionToken = null;

            HttpCookie authenticationCookie = requestCookies[AUTHENTICATION_COOKIE_NAME];
            if (authenticationCookie != null && authenticationCookie.Values[SESSION_TOKEN_KEY] != null)
                sessionToken = Guid.Parse(authenticationCookie.Values[SESSION_TOKEN_KEY]);

            return sessionToken;
        }

        public void SetSessionToken(HttpCookieCollection responseCookies, Guid token)
        {
            HttpCookie authCookie = new HttpCookie(AUTHENTICATION_COOKIE_NAME);
            authCookie[SESSION_TOKEN_KEY] = token.ToString();
            responseCookies.Add(authCookie);
        }

        public void DeleteSessionToken(HttpCookieCollection responseCookies)
        {
            HttpCookie expiredCookie = new HttpCookie(AUTHENTICATION_COOKIE_NAME);
            expiredCookie.Expires = DateTime.Now.AddDays(-1d);
            responseCookies.Add(expiredCookie);
        }

        public Guid? GetContextAccountID(HttpCookieCollection requestCookies)
        {
            Guid? contextAccountID = null;

            HttpCookie contextCookie = requestCookies[CONTEXT_COOKIE_NAME];
            if (contextCookie != null && contextCookie.Values[CONTEXT_ACCOUNT_KEY] != null)
                contextAccountID = Guid.Parse(contextCookie.Values[CONTEXT_ACCOUNT_KEY]);

            return contextAccountID;
        }

        public void SetContextAccountID(HttpCookieCollection responseCookies, Guid contextAccountID)
        {
            HttpCookie contextCookie = new HttpCookie(CONTEXT_COOKIE_NAME);
            contextCookie[CONTEXT_ACCOUNT_KEY] = contextAccountID.ToString();
            responseCookies.Add(contextCookie);
        }
    }
}