﻿using System;
using System.Web;

namespace ClientPortal.Services
{
    public interface IClientSideStoreServices
    {
        Guid? GetSessionToken(HttpCookieCollection requestCookies);
        void SetSessionToken(HttpCookieCollection responseCookies, Guid token);
        void DeleteSessionToken(HttpCookieCollection responseCookies);
        Guid? GetContextAccountID(HttpCookieCollection requestCookies);
        void SetContextAccountID(HttpCookieCollection responseCookies, Guid contextAccountID);
    }
}