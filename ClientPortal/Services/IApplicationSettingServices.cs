﻿
namespace ClientPortal.Services
{
    public enum ApplicationSetting { LoginPageUrl, AuthenticationServerUrl, AuthenticationServerEncryptionSecret }

    public interface IApplicationSettingServices
    {
        string GetSettingValue(ApplicationSetting setting);
    }
}