﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientPortal.ReadModelFacades;
using NServiceBus;
using Subscription.Messages.Commands.Account;
using ClientPortal.ViewModels;
using AutoMapper;

namespace ClientPortal.Controllers
{
    public class InvitationController : Controller
    {
        private readonly IBus _bus;
        private readonly IUserInvitationFacade _userInvitationFacade;
        private readonly IRegisteredEmailFacade _registeredEmailFacade;

        public InvitationController(IBus bus, IUserInvitationFacade userInvitationFacade, IRegisteredEmailFacade registeredEmailFacade)
        {
            _bus = bus;
            _userInvitationFacade = userInvitationFacade;
            _registeredEmailFacade = registeredEmailFacade;
        }


        //
        // GET: /Invitation/Accept?invitationToken=***
        public ActionResult Accept(Guid invitationToken)
        {
            ActionResult actionResult;

            string email;
            bool tokenValid = _userInvitationFacade.ValidateInvitationToken(invitationToken, out email);
            if (!tokenValid)
            {
                actionResult = View(viewName: "Error", model: "The invitation token is either invalid or expired.");
            }
            else
            {
                bool emailRegistered = _registeredEmailFacade.IsEmailRegistered(email);
                if (emailRegistered)
                {
                    _bus.Send(new AssignExistingUserToAccount(invitationToken));
                    actionResult = View("InvitationAccepted");
                }
                else
                {
                    actionResult = RedirectToAction("RegisterInvitedUser", new { invitationToken = invitationToken });
                }
            }

            return actionResult;
        }

        //
        // GET: /Invitation/RegisterInvitedUser?invitationToken=***
        public ActionResult RegisterInvitedUser(Guid invitationToken)
        {
            ActionResult actionResult;

            string email;
            bool tokenValid = _userInvitationFacade.ValidateInvitationToken(invitationToken, out email);
            if (!tokenValid)
            {
                actionResult = View(viewName: "Error", model: "The invitation token is either invalid or expired.");
            }
            else
            {
                bool emailRegistered = _registeredEmailFacade.IsEmailRegistered(email);
                if (emailRegistered)
                {
                    actionResult = View(viewName: "Error", model: string.Format("The email '{0}' is already registered.", email));
                }
                else
                {
                    actionResult = RegisterInvitedUserView(email);
                }
            }

            return actionResult;
        }

        //
        // POST: /Invitation/RegisterInvitedUser?invitationToken=***
        [HttpPost]
        public ActionResult RegisterInvitedUser(Guid invitationToken, RegisterInvitedUserPageViewModel vm)
        {
            ActionResult actionResult;

            string email;
            bool tokenValid = _userInvitationFacade.ValidateInvitationToken(invitationToken, out email);
            if (!tokenValid)
            {
                actionResult = View(viewName: "Error", model: "The invitation token is either invalid or expired.");
            }
            else
            {
                bool emailRegistered = _registeredEmailFacade.IsEmailRegistered(email);
                if (emailRegistered)
                {
                    actionResult = View(viewName: "Error", model: string.Format("The email '{0}' is already registered.", email));
                }
                else
                {
                    if (!ModelState.IsValid)
                    {
                        actionResult = RegisterInvitedUserView(email);
                    }
                    else
                    {
                        RegisterInvitedUser command = new RegisterInvitedUser
                        {
                            InvitationToken = invitationToken
                        };
                        Mapper.DynamicMap(vm, command);

                        _bus.Send(command);

                        actionResult = View("InvitedUserRegistered");
                    }
                }
            }

            return actionResult;
        }


        private ActionResult RegisterInvitedUserView(string email)
        {
            RegisterInvitedUserPageViewModel vm = new RegisterInvitedUserPageViewModel(email);
            return View("RegisterInvitedUser", vm);
        }
    }
}