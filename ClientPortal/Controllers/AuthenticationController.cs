﻿using System;
using System.Web.Mvc;
using ClientPortal.ReadModelFacades;
using ClientPortal.Services;
using NServiceBus;
using Subscription.Messages.Commands.User;
using nTrack.Core.Utilities;
using Subscription.Messages.Commands;

namespace ClientPortal.Controllers
{
    public class AuthenticationController : Controller
    {
        private const string AUTHENTICATION_REQUEST_TOKEN_KEY = "AuthenticationRequestToken";

        private readonly IBus _bus;
        private readonly IActiveSessionFacade _activeSessionFacade;
        private readonly IApplicationSettingServices _applicationSettingServices;
        private readonly IClientSideStoreServices _clientSideStoreServices;
        private readonly ICryptographyUtilities _cryptographyUtilities;
        private readonly IThreadingUtilities _threadingUtilities;

        public AuthenticationController(IBus bus, IActiveSessionFacade activeSessionFacade,
            IApplicationSettingServices applicationSettingServices,
            IClientSideStoreServices clientSideStoreServices, ICryptographyUtilities cryptographyUtilities,
            IThreadingUtilities threadingUtilities)
        {
            _bus = bus;
            _activeSessionFacade = activeSessionFacade;
            _applicationSettingServices = applicationSettingServices;
            _clientSideStoreServices = clientSideStoreServices;
            _cryptographyUtilities = cryptographyUtilities;
            _threadingUtilities = threadingUtilities;
        }


        //
        // GET: /Authentication/Login
        public ActionResult Login(string returnUrl)
        {
            Guid authenticationRequestToken = Guid.NewGuid();
            TempData[AUTHENTICATION_REQUEST_TOKEN_KEY] = authenticationRequestToken;
            string callbackUrl = Url.Action("Callback", "Authentication",
                new { requestToken = authenticationRequestToken, returnUrl = returnUrl }, Request.Url.Scheme);
            string registerUrl = Url.Action("Register", "Registration", null, Request.Url.Scheme);
            string forgotPasswordUrl = Url.Action("ForgotPassword", "UserPassword", null, Request.Url.Scheme);
            string authenticationServerUrl = _applicationSettingServices.GetSettingValue(ApplicationSetting.AuthenticationServerUrl);
            string redirectUrl = string.Concat(authenticationServerUrl, "?callbackUrl=", Server.UrlEncode(callbackUrl),
                "&registerUrl=", Server.UrlEncode(registerUrl), "&forgotPasswordUrl=", Server.UrlEncode(forgotPasswordUrl));
            
            return Redirect(redirectUrl);
        }

        //
        // GET: /Authentication/Callback
        public ActionResult Callback(Guid requestToken, string returnUrl, string sessionToken)
        {
            ActionResult actionResult;

            // Make sure that the URL was created for the current user. Not for somebody else and stolen by the current user.
            if (TempData[AUTHENTICATION_REQUEST_TOKEN_KEY] as Guid? != requestToken)
            {
                actionResult = View(viewName: "Error", model: "Login failed!");
            }
            else
            {
                // Login the user.
                string authenticationServerEncryptionSecret = _applicationSettingServices.GetSettingValue(
                    ApplicationSetting.AuthenticationServerEncryptionSecret);
                string sessionTokenStr = _cryptographyUtilities.Decrypt(sessionToken, authenticationServerEncryptionSecret);
                Guid sessionTokenGuid = Guid.Parse(sessionTokenStr);
                _clientSideStoreServices.SetSessionToken(Response.Cookies, sessionTokenGuid);

                // Redirect the user to the return URL or to the Account Admin Panel if no URl is specified.
                if (Url.IsLocalUrl(returnUrl))
                    actionResult = Redirect(returnUrl);
                else
                    actionResult = RedirectToAction("Index", "Home");
            }

            return actionResult;
        }

        //
        // GET: /Authentication/Logout
        public ActionResult Logout()
        {
            Guid? sessionToken = _clientSideStoreServices.GetSessionToken(Request.Cookies);
            if (sessionToken != null)
            {
                _bus.Send(new LogOut
                    {
                        SessionToken = sessionToken.Value
                    });

                // Wait till the read-model is updated.
                _threadingUtilities.WaitOnPredicate(ControllerSettings.WAIT_DELAY_MILLISEC, ControllerSettings.WAIT_TIMEOUT_SEC,
                    () => !_activeSessionFacade.SessionTokenExists(sessionToken.Value));

                _clientSideStoreServices.DeleteSessionToken(Response.Cookies);
            }

            return RedirectToAction("Login", "Authentication");
        }
    }
}