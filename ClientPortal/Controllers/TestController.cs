﻿using System.Web.Mvc;
using NServiceBus;
using Subscription.Messages.Commands.User;
using nTrack.Core.Utilities;
using Subscription.Messages.Commands;
using System;

namespace ClientPortal.Controllers
{
    public class TestController : Controller
    {
        private IBus _bus;

        public TestController(IBus bus)
        {
            _bus = bus;
        }


        //
        // GET: /Test/

        public ActionResult Index()
        {
            _bus.Send<SeeToken>(command =>
                {
                    command.Token = Guid.Parse("485e798a-7a0c-4425-8622-c3e08de4f35c");
                    command.SeenOn = new DateTime(1999, 4, 7, 6, 53, 4);
                });

            return null;
        }

    }
}
