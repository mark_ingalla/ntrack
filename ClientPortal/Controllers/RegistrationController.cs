﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientPortal.ViewModels;
using ClientPortal.ReadModelFacades;
using Subscription.Messages.Commands;
using NServiceBus;
using Subscription.Messages.Commands.Account;
using Subscription.Messages.Commands.User;

namespace ClientPortal.Controllers
{
    public class RegistrationController : Controller
    {
        private readonly IBus _bus;
        private readonly ICountryFacade _countryFacade;
        private readonly IEmailVerificationInfoFacade _emailVerificationInfoFacade;
        private readonly IRegisteredEmailFacade _registeredEmailFacade;

        public RegistrationController(IBus bus, ICountryFacade countryFacade, IEmailVerificationInfoFacade emailVerificationTokenFacade,
            IRegisteredEmailFacade registeredEmailFacade)
        {
            _bus = bus;
            _countryFacade = countryFacade;
            _emailVerificationInfoFacade = emailVerificationTokenFacade;
            _registeredEmailFacade = registeredEmailFacade;
        }


        //
        // GET: /Registration/Register
        public ActionResult Register()
        {
            return RegisterView();
        }

        //
        // POST: /Registration/Register
        [HttpPost]
        public ActionResult Register(RegisterPageViewModel vm)
        {
            ActionResult actionResult;

            Guid emailVerificationToken = Guid.NewGuid();
            Register command = new Register
            {
                AccountId = Guid.NewGuid(),
                AdminUserId = Guid.NewGuid(),
                IPAddress = Request.UserHostAddress,
                EmailVerificationToken = emailVerificationToken,
                EmailVerificationPageUrl = Url.Action("EmailVerification", "Registration", new { emailVerificationToken = emailVerificationToken }, "http")
            };
            vm.UpdateCommand(command, ModelState);

            if (ModelState["AdminEmail"] != null && ModelState["AdminEmail"].Errors.Count == 0 &&
                _registeredEmailFacade.IsEmailRegistered(command.AdminEmail))
            {
                ModelState.AddModelError("AdminEmail", string.Format("The email '{0}' already exists in our database.", command.AdminEmail));
            }

            if (!ModelState.IsValid)
            {
                actionResult = RegisterView();
            }
            else
            {
                _bus.Send(command);

                /* Start test code */
                ViewBag.EmailVerificationUrl = Url.Action("EmailVerification", "Registration", new { emailVerificationToken = emailVerificationToken }, "http");
                /* End test code */

                actionResult = View("RegistrationSuccess");
            }

            return actionResult;
        }

        //
        // GET: /Registration/EmailVerification
        public ActionResult EmailVerification(Guid emailVerificationToken)
        {
            ActionResult actionResult;

            Guid userID;
            if (!_emailVerificationInfoFacade.VerifyToken(emailVerificationToken, out userID))
            {
                actionResult = View(viewName: "Error", model: "Email verification failed. Invalid or expired token.");
            }
            else
            {
                MarkUserAsActive command = new MarkUserAsActive
                {
                    UserId = userID,
                    EmailVerificationToken = emailVerificationToken
                };
                _bus.Send(command);

                actionResult = View("EmailVerified");
            }

            return actionResult;
        }

        //
        // GET (using AJAX): /Registration/EmailRegistered?email=bassem.mf@gmail.com
        public bool EmailRegistered(string email)
        {
            return _registeredEmailFacade.IsEmailRegistered(email);
        }


        private ViewResult RegisterView()
        {
            string[] countryOptions = _countryFacade.GetAllCountries();
            RegisterPageViewModel vm = new RegisterPageViewModel(countryOptions);
            return View("Register", vm);
        }
    }
}