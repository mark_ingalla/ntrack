﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientPortal.ViewModels;
using ClientPortal.ReadModelFacades;
using NServiceBus;
using Subscription.Messages.Commands.User;

namespace ClientPortal.Controllers
{
    public class UserPasswordController : Controller
    {
        private readonly IBus _bus;
        private readonly IUserFacade _userFacade;
        private readonly IPasswordResetTokenFacade _passwordResetTokenFacade;

        public UserPasswordController(IBus bus, IUserFacade userFacade, IPasswordResetTokenFacade passwordResetTokenFacade)
        {
            _bus = bus;
            _userFacade = userFacade;
            _passwordResetTokenFacade = passwordResetTokenFacade;
        }


        //
        // GET: /UserPassword/ForgotPassword
        public ActionResult ForgotPassword()
        {
            return View("ForgotPassword", new ForgotPasswordPageViewModel());
        }

        //
        // POST: /UserPassword/ForgotPassword
        [HttpPost]
        public ActionResult ForgotPassword(ForgotPasswordPageViewModel vm)
        {
            ActionResult actionResult;

            nTrack.ReadModels.User user = null;
            if (!string.IsNullOrEmpty(vm.Email))
            {
                user = _userFacade.GetUserByEmail(vm.Email);
                if (user == null)
                    ModelState.AddModelError("Email", string.Format("The email '{0}' does not exist in our database.", vm.Email));
            }

            if (!ModelState.IsValid)
            {
                actionResult = View("ForgotPassword", vm);
            }
            else
            {
                Guid resetPasswordToken = Guid.NewGuid();
                string resetPasswordUrl = Url.Action("ResetPassword", "UserPassword", new { token = resetPasswordToken }, Request.Url.Scheme);
                _bus.Send(new PrepareResetPassword
                {
                    UserId = user.Id,
                    UserEmail = vm.Email,
                    ResetPasswordToken = resetPasswordToken,
                    ResetPasswordLink = resetPasswordUrl
                });

                /* Start test code */
                ViewBag.ResetPasswordUrl = resetPasswordUrl;
                /* End test code */

                actionResult = View("EmailSent");
            }

            return actionResult;
        }

        //
        // GET: /UserPassword/ResetPassword
        public ActionResult ResetPassword(Guid token)
        {
            ActionResult actionResult;

            bool tokenValid = _passwordResetTokenFacade.ValidateToken(token);
            if (tokenValid)
                actionResult = View("ResetPassword", new ResetPasswordPageViewModel());
            else
                actionResult = View(viewName: "Error", model: "The password reset token is invalid.");

            return actionResult;
        }

        //
        // POST: /UserPassword/ResetPassword
        [HttpPost]
        public ActionResult ResetPassword(Guid token, ResetPasswordPageViewModel vm)
        {
            ActionResult actionResult;

            if (!ModelState.IsValid)
            {
                actionResult = View("ResetPassword", vm);
            }
            else
            {
                _bus.Send(new DoResetPassword
                {
                    ResetPasswordToken = token,
                    NewPassword = vm.NewPassword
                });

                actionResult = View("ResetPasswordSuccess");
            }

            return actionResult;
        }
    }
}