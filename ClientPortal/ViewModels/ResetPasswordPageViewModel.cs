﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClientPortal.ViewModels
{
    public class ResetPasswordPageViewModel : IValidatableObject
    {
        [Required] [StringLength(30, MinimumLength = 8)] [Display(Name = "New pasword")]
        public string NewPassword    { get; set; }
        [Required] [Display(Name = "Repeat password")]
        public string RepeatPassword { get; set; }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (NewPassword != RepeatPassword)
                yield return new ValidationResult("Password and Repeat password do not match.");
        }
    }
}