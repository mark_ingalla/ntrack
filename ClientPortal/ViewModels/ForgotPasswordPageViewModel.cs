﻿using System.ComponentModel.DataAnnotations;

namespace ClientPortal.ViewModels
{
    public class ForgotPasswordPageViewModel
    {
        [Required]
        public string Email { get; set; }
    }
}