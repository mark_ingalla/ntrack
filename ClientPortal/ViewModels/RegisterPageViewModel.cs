﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using ClientPortal.Utilities;
using Subscription.Messages.Commands;
using Subscription.Messages.Commands.Account;

namespace ClientPortal.ViewModels
{
    public class RegisterPageViewModel
    {
        private static readonly Dictionary<string, string> _propertyToDisplayName = new Dictionary<string,string>
        {
            { "CompanyName", "Company name" },
            { "CompanyPhone", "Phone" },
            { "AddressLine1", "Address line 1" },
            { "AddressLine2", "Address line 2" },
            { "AddressLine3", "Address line 3" },
            { "CompanySubrub", "Suburb" },
            { "CompanyState", "State" },
            { "CompanyPostcode", "Postcode" },
            { "CompanyCountry", "Country" },
            { "AdminFirstName", "First name" },
            { "AdminLastName", "Last name" },
            { "AdminEmail", "Email" },
            { "AdminPhone", "Phone" },
            { "AdminMobile", "Mobile" },
            { "AdminPassword", "Password" }
        };

        public readonly IEnumerable<SelectListItem> CountryOptions;

        public string CompanyName     { get; set; }
        public string CompanyPhone    { get; set; }
        public string AddressLine1    { get; set; }
        public string AddressLine2    { get; set; }
        public string AddressLine3    { get; set; }
        public string CompanySubrub   { get; set; }
        public string CompanyState    { get; set; }
        public string CompanyPostcode { get; set; }
        public string CompanyCountry  { get; set; }
        public string AdminFirstName  { get; set; }
        public string AdminLastName   { get; set; }
        public string AdminEmail      { get; set; }
        public string AdminPassword   { get; set; }
        public string RepeatPassword  { get; set; }
        public string AdminPhone      { get; set; }
        public string AdminMobile     { get; set; }


        public RegisterPageViewModel()
        { }
        
        public RegisterPageViewModel(IEnumerable<string> countryOptions)
        {
            CountryOptions = from country in countryOptions
                             select new SelectListItem
                             {
                                 Text = country,
                                 Value = country
                             };
        }


        public void UpdateCommand(Register command, ModelStateDictionary errorsDictionary)
        {
            if (string.IsNullOrEmpty(RepeatPassword))
                errorsDictionary.AddModelError("RepeatPassword", "Repeat password field is required.");
            else if (AdminPassword != RepeatPassword)
                errorsDictionary.AddModelError("AdminPassword", "The Password and Repeat password fields do not match.");

            Mapper.DynamicMap(this, command);

            errorsDictionary.AddModelErrors(command, _propertyToDisplayName);
        }
    }
}