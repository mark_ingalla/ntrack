﻿using System.ComponentModel.DataAnnotations;

namespace ClientPortal.ViewModels
{
    public class RegisterInvitedUserPageViewModel
    {
        public string Email { get; private set; }

        [Required] [StringLength(20, MinimumLength = 5)] [Display(Name = "First name")]
        public string FirstName { get; set; }
        [Required] [StringLength(20, MinimumLength = 5)] [Display(Name = "Last name")]
        public string LastName  { get; set; }
        public string Phone     { get; set; }
        public string Mobile    { get; set; }
        [Required] [StringLength(30, MinimumLength = 8)]
        public string Password  { get; set; }

        public RegisterInvitedUserPageViewModel()
        { }

        public RegisterInvitedUserPageViewModel(string email)
        {
            Email = email;
        }
    }
}