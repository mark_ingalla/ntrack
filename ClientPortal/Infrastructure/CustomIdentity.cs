﻿using System;
using System.Security.Principal;

namespace ClientPortal.Infrastructure
{
    public class CustomIdentity : IIdentity
    {
        public string AuthenticationType { get; private set; }

        public Guid ID { get; private set; }
        public string Name
        {
            get
            {
                return ID.ToString();
            }
        }

        public string UserName { get; private set; }

        public bool IsAuthenticated
        {
            get { return true; }
        }

        public CustomIdentity(Guid id, string userName, string authenticationType)
        {
            ID = id;
            UserName = userName;
            AuthenticationType = authenticationType;
        }
    }
}