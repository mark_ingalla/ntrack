﻿using System;

namespace ClientPortal.Models
{
    public class ProductSummary
    {
        public Guid   ID   { get; set; }
        public string Name { get; set; }
    }
}