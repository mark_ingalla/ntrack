﻿using System;
using System.Linq;
using CommonDomain.Persistence;
using StructureMap;
using StructureMap.Attributes;
using nTrack.Core.Services;

namespace Subscription.Domain.Models.Validation
{
    public class ProductContainsFeature : ISpecification<Account.Account>
    {
        [SetterProperty]
        public IRepository Repository { get; set; }

        private Product.Product Product { get; set; }

        private string FeatureCode { get; set; }

        public ProductContainsFeature(Guid productId, string featureCode)
        {
            if (productId == Guid.Empty || string.IsNullOrEmpty(featureCode))
                throw new ArgumentNullException();
            
            Repository = ObjectFactory.GetInstance<IRepository>();
            FeatureCode = featureCode;
            Product = Repository.GetById<Product.Product>(productId);
        }

        public bool IsSatisfiedBy(Account.Account entity)
        {
            return Product.Features.ContainsKey(FeatureCode);
        }
    }
}
