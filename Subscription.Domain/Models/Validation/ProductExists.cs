﻿using System;
using EventStore.RavenDb;
using StructureMap;
using Subscription.Domain.Infrastructure;

namespace Subscription.Domain.Models.Validation
{
    public class ProductExists : ISpecification<Account.Account> //, ISpecification<User.User>
    {
        protected Product.Product ProductToCheck { get; set; }
        protected IRepository Repository { get; set; }

        public ProductExists(Guid productId)
        {
            if (productId == Guid.Empty)
                throw new Exception("Product id is empty");

            Repository = ObjectFactory.GetInstance<IRepository>();
            ProductToCheck = Repository.GetById<Product.Product>(productId);
        }

        public bool IsSatisfiedBy(Account.Account entity)
        {
            return Check();
        }

        //public bool IsSatisfiedBy(User.User entity)
        //{
        //    return Check();
        //}

        bool Check()
        {
            return ProductToCheck.Version > 0;
        }
    }
}
