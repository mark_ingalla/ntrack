﻿using System;
using CommonDomain.Persistence;
using StructureMap;
using StructureMap.Attributes;
using nTrack.Core.Services;

namespace Subscription.Domain.Models.Validation
{
    public class IsUserAuthorizedToAccount : ISpecification<User.User>
    {
        [SetterProperty]
        public IRepository Repository { get; set; }

        public Account.Account Account { get; set; }
        public Guid UserId { get; set; }

        public IsUserAuthorizedToAccount(Guid accountId, Guid userId)
        {
            if (accountId == Guid.Empty || userId == Guid.Empty)
                throw new ArgumentNullException();

            Repository = ObjectFactory.GetInstance<IRepository>();
            UserId = userId;

            Account = Repository.GetById<Account.Account>(accountId);
        }

        public bool IsSatisfiedBy(User.User entity)
        {
            return Account.AuthorizedUsers.Contains(UserId);
        }
    }
}
