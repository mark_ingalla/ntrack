﻿using System;
using CommonDomain.Persistence;
using StructureMap;
using nTrack.Core.Services;

namespace Subscription.Domain.Models.Validation
{
    class UserExists : ISpecification<Account.Account>
    {
        Guid _userId;

        public UserExists(Guid userId)
        {
            if (userId == Guid.Empty)
                throw new Exception("User id is empty");

            _userId = userId;
            Repository = ObjectFactory.GetInstance<IRepository>();
            UserToCheck = Repository.GetById<User.User>(userId);
        }

        protected User.User UserToCheck { get; set; }

        protected IRepository Repository { get; set; }

        public bool IsSatisfiedBy(Account.Account entity)
        {
            return UserToCheck.Version > 0;
        }
    }
}
