﻿using System;
using CommonDomain.Persistence;
using StructureMap;
using StructureMap.Attributes;
using nTrack.Core.Services;

namespace Subscription.Domain.Models.Validation
{
    public class DoesAccountHaveSubscriptionForProduct : ISpecification<User.User>
    {
        [SetterProperty]
        public IRepository Repository { get; set; }

        public Account.Account Account { get; set; }
        public Guid ProductId { get; set; }

        public DoesAccountHaveSubscriptionForProduct(Guid accountId, Guid productId)
        {
            if (accountId == Guid.Empty || productId == Guid.Empty)
                throw new ArgumentNullException();

            Repository = ObjectFactory.GetInstance<IRepository>();
            ProductId = productId;

            Account = Repository.GetById<Account.Account>(accountId);
        }

        public bool IsSatisfiedBy(User.User entity)
        {
            return Account.SubscribedProductFeatures.ContainsKey(ProductId);
        }
    }
}
