﻿using System;
using System.Linq;
using CommonDomain.Persistence;
using StructureMap;
using StructureMap.Attributes;
using nTrack.Core.Services;

namespace Subscription.Domain.Models.Validation
{
    public class ProductContainsPermission : ISpecification<Account.Account>
    {
        [SetterProperty]
        public IRepository Repository { get; set; }

        private Product.Product Product { get; set; }

        private string PermissionCode { get; set; }

        public ProductContainsPermission(Guid productId, string permissionCode)
        {
            if (productId == Guid.Empty || string.IsNullOrEmpty(permissionCode))
                throw new ArgumentNullException();

            Repository = ObjectFactory.GetInstance<IRepository>();
            PermissionCode = permissionCode;
            Product = Repository.GetById<Product.Product>(productId);
        }

        public bool IsSatisfiedBy(Account.Account entity)
        {
            return Product.PermissionList.Any(x => x == PermissionCode);
        }
    }
}
