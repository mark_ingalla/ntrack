﻿using System;
using CommonDomain.Persistence;
using StructureMap;
using nTrack.Core.Services;

namespace Subscription.Domain.Models.Validation
{
    class AccountExists : ISpecification<User.User>
    {
        protected Account.Account AccountToCheck { get; set; }
        protected IRepository Repository { get; set; }

        public AccountExists(Guid accountId)
        {
            if (accountId == Guid.Empty)
                throw new Exception("Account id is empty");

            Repository = ObjectFactory.GetInstance<IRepository>();
            AccountToCheck = Repository.GetById<Account.Account>(accountId);
        }

        public bool IsSatisfiedBy(User.User entity)
        {
            return AccountToCheck.Version > 0;
        }
    }
}
