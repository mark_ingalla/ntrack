﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonDomain.Persistence;
using StructureMap;
using StructureMap.Attributes;
using nTrack.Core.Services;

namespace Subscription.Domain.Models.Validation
{
    public class AccountContainProductSubscription : ISpecification<Account.Account>
    {
        [SetterProperty]
        public IRepository Repository { get; set; }

        protected Product.Product ProductToCheck { get; set; }

        public AccountContainProductSubscription(Guid productId)
        {
            if (productId == Guid.Empty)
                throw new Exception("Product id is empty");

            Repository = ObjectFactory.GetInstance<IRepository>();
            ProductToCheck = Repository.GetById<Product.Product>(productId);
        }

        public bool IsSatisfiedBy(Account.Account entity)
        {
            return entity.Subscriptions.ContainsKey(ProductToCheck.Id) && Check();
        }

        bool Check()
        {
            return ProductToCheck.Version > 0;
        }
    }
}
