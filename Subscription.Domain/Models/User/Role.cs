using System;
using System.Collections.Generic;

namespace Subscription.Domain.Models.User
{
    public class Role
    {
        public enum RoleType
        {
            Admin,
            User
        }

        public string Name { get; set; }

        public RoleType TypeName { get; set; }

        readonly Dictionary<Guid, List<string>> _permissions;

        public Role(string name, RoleType typeName = RoleType.User)
        {
            _permissions = new Dictionary<Guid, List<string>>();
            Name = name;
            TypeName = typeName;
        }

        public void PromoteToAdmin()
        {
            TypeName = RoleType.Admin;
        }

        public void DemoteToUser()
        {
            TypeName = RoleType.User;
        }

        public void AddPermission(Guid productId, string code)
        {
            List<string> permissions;
            if (_permissions.TryGetValue(productId, out permissions))
            {
                permissions.Add(code);
            }
            else
            {
                _permissions.Add(productId, new List<string> { code });
            }
        }

        public void RevokePermission(Guid productId, string code)
        {
            List<string> permissions;
            _permissions.TryGetValue(productId, out permissions);
            permissions.Remove(code);
        }
        public void RemoveProduct(Guid productId)
        {
            _permissions.Remove(productId);
        }
        public List<string> Permissions(Guid productId)
        {
            List<string> permissions;
            return !_permissions.TryGetValue(productId, out permissions) ? null : permissions;
        }
    }
}