using System;
using System.Collections.Generic;
using System.Linq;

namespace Subscription.Domain.User
{
    public class Roles
    {
        //TODO decide how to define admin role
        // ReSharper disable InconsistentNaming
        public const string ADMIN_ROLE_NAME = "Admin";
        // ReSharper restore InconsistentNaming

        readonly List<Role> _roles;

        public Roles()
        {
            _roles = new List<Role>();
        }

        public Role AddRole(string name, Guid accountId)
        {
            var role = new Role(name, accountId);
            _roles.Add(role);

            return role;
        }

        public void PromoteToAdmin(Guid accountId)
        {            
            if (_roles.Any(x => x.AccountId == accountId && x.TypeName == Role.RoleType.Admin))
                return;

            var role = AddRole(ADMIN_ROLE_NAME, accountId);

            role.PromoteToAdmin();
        }

        public void DemoteToUser(Guid accountId)
        {
            var role = _roles.SingleOrDefault(x => x.TypeName == Role.RoleType.Admin && x.AccountId == accountId);

            if (role != null)
                role.DemoteToUser();
        }

        public void AddPermissionToRole(Guid accountId, string roleName, Guid productId, string permission)
        {
            var role = _roles.SingleOrDefault(x => x.AccountId == accountId && x.Name == roleName) ??
                       AddRole(roleName, accountId);

            role.AddPermission(productId, permission);
        }

        public void RevokePermissionFromRole(Guid accountId, string roleName, Guid productId, string permission)
        {
            var role = _roles.SingleOrDefault(x => x.AccountId == accountId && x.Name == roleName);

            if (role == null) return;

            role.RevokePermission(productId, permission);
        }
    }
}