﻿using System;
using System.Collections.Generic;
using CommonDomain.Core;
using nTrack.Core.Exceptions;
using nTrack.Core.Services;
using Subscription.Domain.Models.Validation;
using Subscription.Messages.Events.User;

namespace Subscription.Domain.Models.User
{
    public class User : AggregateBase
    {
        public enum PasswordResetStatus
        {
            Pending,
            Completed,
            Timeouted,
        }

        public bool IsLocked { get; private set; }
        public bool EmailVerfied { get; private set; }
        string _password;

        public Dictionary<Guid, Role> AccountRoles { get; private set; }

        User(Guid id)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentException("id");
            }

            Id = id;
        }
        public User(Guid id, string firstName, string lastName, string email, string password)
        {
            if (id == Guid.Empty)
                throw new Exception("Id is empty");

            RaiseEvent(new UserCreated(id, firstName, lastName, email, password));
        }
        public static User CreateUser(Guid id, string firstName, string lastName, string email, string password)
        {
            return new User(id, firstName, lastName, email, password);
        }
        
        //the user require verification of email
        public void VerifyUser()
        {
            if (EmailVerfied)
                throw new Exception("User already verified");

            RaiseEvent(new UserVerified(Id));
        }
        
        //rest & change a password for user
        public void ResetPassword(string newPassword)
        {
            if (!EmailVerfied)
                throw new Exception("User not verified!");

            RaiseEvent(new PasswordReseted(Id, newPassword));
        }
        public void ChangePassword(string oldPassword, string newPassword)
        {
            if (!EmailVerfied)
                throw new Exception("User not verified!");

            if (_password != oldPassword)
                throw new Exception("Old password does not match current password");

            RaiseEvent(new PasswordChanged(Id, oldPassword, newPassword));
        }
        public void UpdateUserDetail(string firstName, string lastName, string phone, string mobile, string email)
        {
            RaiseEvent(new UserDetailUpdated(Id, firstName,lastName,phone,mobile,email));
        }

        //NOTE: this function is for business admin to temporarily disable access to system
        public void LockUser(string reason)
        {
            if (!EmailVerfied)
                throw new Exception("User not verified!");

            if (IsLocked)
                throw new Exception("User already locked");

            RaiseEvent(new UserLocked(Id, reason, DateTime.UtcNow));
        }
        public void UnlockUser(string reason)
        {
            if (!EmailVerfied)
                throw new Exception("User not verified!");

            if (!IsLocked)
                throw new Exception("User already unlocked");

            RaiseEvent(new UserUnlocked(Id, reason, DateTime.UtcNow));
        }
        public void AssignUserToAccount(Guid accountId, string roleName)
        {
            if (!EmailVerfied)
                throw new Exception("User not verified!");
            ISpecification<User> specification = new AccountExists(accountId);
            if (!specification.IsSatisfiedBy(this))
                throw new AggregateRootNotFoundException();
            Role r;
            if (AccountRoles.TryGetValue(accountId, out r))
                throw new Exception("Account already associated!");
            RaiseEvent(new UserAssignedToAccount(Id, accountId, roleName));
        }
        public void RemoveAssignmentToAccount(Guid accountId)
        {
            if (!EmailVerfied)
                throw new Exception("User not verified!");
            ISpecification<User> specification = new AccountExists(accountId);
            if (!specification.IsSatisfiedBy(this))
                throw new AggregateRootNotFoundException();
            Role r;
            if (!AccountRoles.TryGetValue(accountId, out r))
                throw new Exception("Account not associated!");

            RaiseEvent(new UserAssignmentToAccountRemoved(Id, accountId));
        }

        void CheckIfRoleExists(Guid accountId, string role)
        {
            Role r;
            AccountRoles.TryGetValue(accountId, out r);
            if (r.Name != role)
                throw new Exception("No role with this name");
        }

        //creating a new/existing role and assigning a product's permission to it
        public void AddPermission(Guid accountId, string role, Guid productId, string permissionCode)
        {
            if (!EmailVerfied)
                throw new Exception("User not verified!");

            var isUserAuthorizedToAccount = new IsUserAuthorizedToAccount(accountId, Id);
            var productContainsPermission = new ProductContainsPermission(productId, permissionCode);
            var accountExists = new AccountExists(accountId);
            var productExists = new ProductExists(productId);
            var hasAccountSubForProduct = new DoesAccountHaveSubscriptionForProduct(accountId, productId);
            var specs = accountExists.And(productExists).And(isUserAuthorizedToAccount).And(productContainsPermission)
                .And(hasAccountSubForProduct);

            if (!specs.IsSatisfiedBy(this))
                throw new Exception("There's no account with this id or there's no product with this id" +
                                    "user is not authorized to account or there is no permission with this code");

            CheckIfRoleExists(accountId, role);

            RaiseEvent(new UserPermissionAdded(Id, accountId, role, productId, permissionCode));
        }
        public void RevokePermission(Guid accountId, string role, Guid productId, string permissionCode)
        {
            if (!EmailVerfied)
                throw new Exception("User not verified!");

            var isUserAuthorizedToAccount = new IsUserAuthorizedToAccount(accountId, Id);
            var productContainsPermission = new ProductContainsPermission(productId, permissionCode);
            var accountExists = new AccountExists(accountId);
            var productExists = new ProductExists(productId);
            var specs = accountExists.And(productExists).And(isUserAuthorizedToAccount).And(productContainsPermission);

            CheckIfRoleExists(accountId, role);

            if (!specs.IsSatisfiedBy(this))
                throw new Exception("There's no account with this id or there's no product with this id" +
                                    "user is not authorized to account or there is no permission with this code");

            RaiseEvent(new UserPermissionRevoked(Id, accountId, role, productId, permissionCode));
        }

        public void RemoveProduct(Guid productId, Guid accountId)
        {
            if (!EmailVerfied)
                throw new Exception("User not verified!");
            
            ISpecification<User> specs = new ProductExists(productId);

            if(!specs.IsSatisfiedBy(this))
                throw new Exception("Product doesn't exist");
            
            if (AccountRoles[accountId].Permissions(productId) == null || AccountRoles[accountId].Permissions(productId).Count == 0)
                throw new Exception("User can't access that product");

            RaiseEvent(new UserProductRemoved(Id, accountId, productId));
        }
        public void PromoteToAdmin(Guid accountId)
        {
            if (!EmailVerfied)
                throw new Exception("User not verified!");

            Role role;
            if (!AccountRoles.TryGetValue(accountId, out role))
                throw new Exception("There's no role for this account");

            var accountExists = new AccountExists(accountId);
            var isUserAuthorizedToAccount = new IsUserAuthorizedToAccount(accountId, Id);
            var specs = accountExists.And(isUserAuthorizedToAccount);

            if (!specs.IsSatisfiedBy(this))
                throw new Exception("Account doesn't exist or user is not associated with account");

            RaiseEvent(new UserPromotedToAdmin(Id, accountId));
        }
        public void DemoteToUser(Guid accountId)
        {
            if (!EmailVerfied)
                throw new Exception("User not verified!");

            Role role;
            if (!AccountRoles.TryGetValue(accountId, out role))
                throw new Exception("There's no role for this account");

            var isUserAuthorizedToAccount = new IsUserAuthorizedToAccount(accountId, Id);
            var specs = new AccountExists(accountId).And(isUserAuthorizedToAccount);
            if (!specs.IsSatisfiedBy(this))
                throw new Exception("Account doesn't exist or user is not associated with account");

            RaiseEvent(new AdminDemotedToUser(Id, accountId));
        }



        void Apply(UserCreated @event)
        {
            Id = @event.UserId;
            _password = @event.Password;
            AccountRoles = new Dictionary<Guid, Role>();
            IsLocked = false;
        }
        void Apply(UserAssignedToAccount @event)
        {
            AccountRoles.Add(@event.AccountId, new Role(@event.Role));
        }
        void Apply(UserAssignmentToAccountRemoved evt)
        {
            AccountRoles.Remove(evt.AccountId);
        }
        void Apply(PasswordReseted @event)
        {
            _password = @event.Password;
        }
        void Apply(PasswordChanged evt)
        {
            _password = evt.NewPassword;
        }
        void Apply(UserDetailUpdated @event)
        {
            
        }
        void Apply(UserVerified @event)
        {
            EmailVerfied = true;
        }
        void Apply(UserPermissionAdded @event)
        {
            Role roles;
            //NOTE should never fail
            AccountRoles.TryGetValue(@event.AccountId, out roles);
            roles.AddPermission(@event.ProductId, @event.PermissionCode);
        }
        void Apply(UserPermissionRevoked @event)
        {
            Role role;
            //NOTE should never fail
            AccountRoles.TryGetValue(@event.AccountId, out role);

            role.RevokePermission(@event.ProductId, @event.PermissionCode);
        }
        void Apply(UserPromotedToAdmin @event)
        {
            Role role;
            AccountRoles.TryGetValue(@event.AccountId, out role);

            role.PromoteToAdmin();
        }
        void Apply(AdminDemotedToUser @event)
        {
            Role role;
            AccountRoles.TryGetValue(@event.AccountId, out role);

            role.DemoteToUser();
        }
        void Apply(UserLocked @event)
        {
            IsLocked = true;
        }
        void Apply(UserUnlocked @event)
        {
            IsLocked = false;
        }
        void Apply(UserProductRemoved evt)
        {
            AccountRoles.Remove(evt.ProductId);
        }
    }
}
