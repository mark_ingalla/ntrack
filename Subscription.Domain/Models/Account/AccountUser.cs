﻿using System;
using nTrack.Core.Messages;

namespace Subscription.Domain.Models.Account
{
    public class AccountUser
    {
        public Guid Key { get; set; }
        public string Email { get; set; }
        public bool IsLocked { get; set; }
        public bool IsActive { get; set; }
        public UserRole Role { get; set; }

        public AccountUser(Guid key, string email)
        {
            Key = key;
            Email = email;
            IsLocked = false;
        }
    }
}