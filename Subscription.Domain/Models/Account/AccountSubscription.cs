﻿using System;

namespace Subscription.Domain.Models.Account
{
    public class AccountSubscription
    {
        public Guid Key { get; set; }
        public Guid OfferKey { get; set; }
        public string ProductCode { get; set; }
        public decimal Amount { get; set; }
        public int OfferQTY { get; set; }
        public decimal Total { get { return Amount*OfferQTY; } }
    }
}