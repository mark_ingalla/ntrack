using System;
using System.Collections.Generic;
using System.Linq;

namespace Subscription.Domain.Account
{
    public class Subscription
    {
        public Guid ProductId { get; set; }

        public List<string> Features { get; set; }

        public Subscription(Guid productId)
        {
            ProductId = productId;
            Features = new List<string>();
        }

        //public bool FeatureEnabled(string featureCode)
        //{
        //    return Features.Any(x => x == featureCode);
        //}

        //public void EnableFeature(string featureCode)
        //{
        //    if (!FeatureEnabled(featureCode))
        //        Features.Add(featureCode);
        //}

        //public void DisableFeature(string featureCode)
        //{
        //    if (FeatureEnabled(featureCode))
        //        Features.Remove(featureCode);
        //}
    }
}