﻿
namespace Subscription.Domain.Models.Account
{
    public class ContactDetail
    {
        public string AddressLine1 { get; private set; }
        public string AddressLine2 { get; private set; }
        public string AddressLine3 { get; private set; }
        public string Suburb { get; private set; }
        public string State { get; private set; }
        public string Postcode { get; private set; }
        public string Country { get; private set; }
        public string Phone { get; private set; }

        public ContactDetail(string addressLine1, string addressLine2, string addressLine3, string suburb,
            string state, string postcode, string country, string phone)
        {
            AddressLine1 = addressLine1;
            AddressLine2 = addressLine2;
            AddressLine3 = addressLine3;
            Suburb = suburb;
            State = state;
            Postcode = postcode;
            Country = country;
            Phone = phone;
        }
    }
}
