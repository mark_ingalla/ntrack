using System;
using System.Collections.Generic;
using System.Linq;

namespace Subscription.Domain.Account
{
    public class AuthorizedUsers
    {
        private List<Guid> _users;

        public AuthorizedUsers()
        {
            _users = new List<Guid>();
        }

        public bool IsUserAuthorized(Guid userId)
        {
            return _users.Any(x => x == userId);
        }
        public void AuthorizeUser(Guid userId)
        {
            _users.Add(userId);
        }
        public void RevokeUser(Guid userId)
        {
            _users.Remove(userId);
        }
    }
}