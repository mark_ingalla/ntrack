﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonDomain.Core;
using nTrack.Core.Messages;
using Subscription.Domain.DomainServices;
using Subscription.Domain.Infrastructure;
using Subscription.Domain.Models.Validation;
using Subscription.Messages.Events.Account;
using Subscription.Messages.Events.Account.User;
using InvoiceDetail = Subscription.Messages.Events.Account.InvoiceDetail;

namespace Subscription.Domain.Models.Account
{
    public class Account : AggregateBase
    {
        public enum InvitationStatus
        {
            Pending,
            EmailSent,
            Completed,
            TimedOut,
            BouncedBack,
            Error
        }
        public enum PasswordResetStatus
        {
            Pending,
            Completed,
            Timeouted,
        }

        bool _isActive;

        List<AccountUser> Users { get; set; }
        Dictionary<Guid, AccountSubscription> Subscriptions { get; set; }


        public Account() { }
        public Account(Guid accountId, string companyName, string companyPhone, string country, string addressIp)
        {
            if (accountId == Guid.Empty)
                throw new Exception("AccountId is empty");

            RaiseEvent(new AccountCreated(accountId, companyName, companyPhone, country, addressIp));
        }
        public static Account CreateAccount(Guid accountId, string companyName, string companyPhone, string country, string addressIp)
        {
            return new Account(accountId, companyName, companyPhone, country, addressIp);
        }

        public void UpdateAccount(string companyName, string companyPhone, string addressLine1, string addressLine2,
                                  string suburb, string state, string postcode, string country)
        {
            RaiseEvent(new AccountUpdated(Id, companyName, companyPhone, addressLine1, addressLine2, suburb, state, postcode,
                                          country));
        }


        public void Activate()
        {
            RaiseEvent(new AccountActivated(Id));
        }
        public void Deactivate(string reason)
        {
            RaiseEvent(new AccountDeactivated(Id, reason));
        }

        //User Management
        public void AddUser(Guid userKey, string email, string password, string firstName, string lastName, UserRole role)
        {
            if (string.IsNullOrEmpty(email))
                throw new Exception("Email address can't be empty!");

            if (Users == null)
                Users = new List<AccountUser>();

            if (Users.Any(p => p.Email == email))
                throw new Exception("User with email: " + email + " already exists!");

            RaiseEvent(new UserAdded(Id, userKey, email, password, firstName, lastName, role));
        }
        public void RemoveUser(Guid userKey)
        {
            if (Users.All(p => p.Key != userKey))
                throw new Exception("User with key doesn't exists!");

            RaiseEvent(new UserRemoved(Id, userKey));
        }

        public void RetrieveUser(Guid userKey)
        {
            if (Users.All(p => p.Key != userKey))
                throw new Exception("User with key doesn't exists!");

            RaiseEvent(new UserRetrieved(Id, userKey));
        }

        public void UpdateUserDetail(Guid userKey, string firstName, string lastName, string phone, string mobile, string email)
        {
            if (Users.All(p => p.Key != userKey))
                throw new Exception("User with key doesn't exists!");

            if (Users.Any(p => p.Key != userKey && p.Email == email))
                throw new Exception("This email is used by another user");

            RaiseEvent(new UserDetailUpdated(Id, userKey, firstName, lastName, phone, mobile, email));
        }
        public void LockUser(Guid userKey, string reason)
        {
            var user = Users.SingleOrDefault(p => p.Key == userKey);
            if (user == null)
                throw new Exception("User with ID: " + userKey + " doesn't exists!");

            if (user.IsLocked)
                throw new Exception("User is already locked!");

            if (!Users.Any(p => p.Role == UserRole.Admin && p.Key != user.Key))
                throw new Exception("User is only admin in this account. Can't lock!");

            RaiseEvent(new UserLocked(Id, userKey, reason));
        }
        public void UnlockUser(Guid userKey, string reason)
        {
            var user = Users.SingleOrDefault(p => p.Key == userKey);
            if (user == null)
                throw new Exception("User with ID: " + userKey + " doesn't exists!");

            if (!user.IsLocked)
                throw new Exception("User is already unlocked!");

            RaiseEvent(new UserUnlocked(Id, userKey, reason));
        }
        public void ChangeUserPassword(Guid userKey, string newPassword)
        {
            if (Users.All(p => p.Key != userKey))
                throw new Exception("User with Key: " + userKey + " doesn't exist!");

            RaiseEvent(new UserPasswordChanged(Id, userKey, newPassword));
        }
        public void ChangeUserRole(Guid userKey, UserRole role)
        {
            RaiseEvent(new UserRoleChanged(Id, userKey, role));
        }

        //Billing
        public void Subscribe(Guid productId, string productCode, Guid subscriptionKey, Guid offerKey, decimal amount, int offerQTY)
        {
            ISpecification<Account> specification = new ProductExists(productId);

            if (!specification.IsSatisfiedBy(this))
                throw new Exception("Product with this Key doesn't exist");

            if (Subscriptions.ContainsKey(productId))
                throw new Exception("Product is already subscribed!");

            if (Subscriptions.Values.Any(x => x.Key == subscriptionKey))
                throw new Exception("Subscription Key already exists!");

            RaiseEvent(new SubscriptionCreated(Id, productId, productCode, subscriptionKey, offerKey, DateTime.UtcNow, amount, offerQTY));
        }
        public void Unsubscribe(Guid subscriptionKey)
        {
            if (Subscriptions.Values.Any(x => x.Key != subscriptionKey))
                throw new Exception("Subscription does not exist");

            var subscription = Subscriptions.Single(x => x.Value.Key == subscriptionKey);

            RaiseEvent(new SubscriptionTerminated(Id, subscription.Key, DateTime.UtcNow, subscriptionKey));
        }
        public void RaiseInvoice(Guid invoiceId)
        {
            if (Subscriptions.Count <= 0)
                throw new Exception("No active subscriptions found!");

            var invoiceNumber = new InvoiceNumberService().GetNextInvoiceNumber();
            var details = Subscriptions.Select(s => new InvoiceDetail(s.Key, s.Value.ProductCode, s.Value.OfferKey, s.Value.Amount)).ToArray();

            RaiseEvent(new InvoiceRaised(Id, invoiceId, invoiceNumber, details.Sum(x => x.Amount), DateTime.UtcNow, details));
        }
        public void PayInvoice(Guid invoiceId, decimal amount)
        {
            RaiseEvent(new InvoicePaid(Id, invoiceId, amount, DateTime.UtcNow));
        }


        void Apply(AccountCreated @event)
        {
            Id = @event.AccountId;
            Users = new List<AccountUser>();
            Subscriptions = new Dictionary<Guid, AccountSubscription>();


            _isActive = false;
        }
        void Apply(AccountUpdated @event)
        {
        }
        void Apply(AccountActivated @event)
        {
            _isActive = true;
        }
        void Apply(AccountDeactivated evt)
        {
            _isActive = false;
        }


        //User Management
        void Apply(UserAdded @event)
        {
            var user = new AccountUser(@event.UserKey, @event.Email);
            user.Role = @event.Role;
            Users.Add(new AccountUser(@event.UserKey, @event.Email));
        }
        void Apply(UserRemoved @event)
        {
          //  Users.RemoveAll(p => p.Key == @event.UserKey);
            Users.Single(p => p.Key == @event.UserKey).IsActive = false;

        }

        void Apply(UserRetrieved @event)
        {
            Users.Single(p => p.Key == @event.UserKey).IsActive = true;
        }


        void Apply(UserDetailUpdated @event)
        {
            Users.Single(p => p.Key == @event.UserKey).Email = @event.Email;
        }
        void Apply(UserLocked @event)
        {
            Users.Single(p => p.Key == @event.UserKey).IsLocked = true;
        }
        void Apply(UserUnlocked @event)
        {
            Users.Single(p => p.Key == @event.UserKey).IsLocked = false;
        }
        void Apply(UserPasswordChanged @event)
        {
        }
        void Apply(UserRoleChanged evt)
        {
            Users.Single(p => p.Key == evt.UserKey).Role = evt.Role;
        }

        //Billing 
        void Apply(SubscriptionCreated @event)
        {
            Subscriptions.Add(@event.ProductId,
                              new AccountSubscription
                                  {
                                      Key = @event.SubscriptionKey,
                                      Amount = @event.Amount,
                                      ProductCode = @event.ProductCode,
                                      OfferKey = @event.OfferKey,
                                      OfferQTY = @event.OfferQTY
                                  });
        }
        void Apply(SubscriptionTerminated @event)
        {
            Subscriptions.Remove(@event.ProductId);
        }
        void Apply(InvoiceRaised @event)
        {

        }
        void Apply(InvoicePaid @event)
        {
        }

    }
}
