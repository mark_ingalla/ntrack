using System;
using System.Collections.Generic;

namespace Subscription.Domain.Models.Account
{
    public class InvitedUserEmails
    {
        Dictionary<string, Account.InvitationStatus> _emails;

        public InvitedUserEmails()
        {
            _emails = new Dictionary<string, Account.InvitationStatus>();
        }

        public bool InvitationExists(string userEmail)
        {
            return _emails.ContainsKey(userEmail);
        }

        public Account.InvitationStatus GetInvitationStatus(string userEmail)
        {
            return _emails[userEmail];
        }

        public void InviteUser(String userEmail)
        {
            if (!InvitationExists(userEmail))
            {
                _emails.Add(userEmail, Account.InvitationStatus.Pending);
                return;
            }

            var status = _emails[userEmail];

            if (status == Account.InvitationStatus.Timeouted)
            {
                _emails[userEmail] = Account.InvitationStatus.Pending;
                return;
            }

            if (status == Account.InvitationStatus.Completed || status == Account.InvitationStatus.BouncedBack)
                throw new Exception("Cannot reinvite");
        }

        public void ChangeInvitationStatus(string userEmail, string newStatus)
        {
            if (!InvitationExists(userEmail))
                return;

            _emails[userEmail] = (Account.InvitationStatus) Enum.Parse(typeof (Account.InvitationStatus), newStatus);
        }
    }

   
}