using System;
using System.Collections.Generic;
using System.Linq;

namespace Subscription.Domain.Account
{
    public class AccountSubscriptions
    {
        public Dictionary<Guid, List<string>> Subscriptions { get; private set; }
        public AccountSubscriptions()
        {
            Subscriptions = new Dictionary<Guid, List<string>>();
        }

        public bool SubscriptionExists(Guid productId)
        {
            return Subscriptions.ContainsKey(productId);
        }

        public bool IsFeatureSubscribedForProduct(Guid productId, string featureCode)
        {
            return Subscriptions.ContainsKey(productId) && Subscriptions[productId].Contains(featureCode);
        }

        public void Subscribe(Guid productId)
        {
            Subscriptions.Add(productId, new List<string>());
        }

        public void Unsubscribe(Guid productId)
        {
            Subscriptions.Remove(productId);
        }

        public void AddFeatureForProduct(Guid productId, string featureCode)
        {
            Subscriptions[productId].Add(featureCode);
        }

        public void RemoveFeatureForProduct(Guid productId, string featureCode)
        {
            Subscriptions[productId].Remove(featureCode);
        }
    }
}