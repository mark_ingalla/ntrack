﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Subscription.Domain.Product
{
    public class ProductPermissions
    {
        private List<Permission> _permissions;

        public ProductPermissions()
        {
            _permissions = new List<Permission>();
        }

        public bool ContainsPermission(string code)
        {
            return _permissions.Any(x => x.Code == code);
        }

        public bool AddPermisssion(string name, string code)
        {
            if (!ContainsPermission(code))
            {
                _permissions.Add(new Permission(name, code));
                return true;
            }

            return false;
        }

        public bool RemovePermisssion(string code)
        {
            if (ContainsPermission(code))
            {
                _permissions.Remove(_permissions.Single(x => x.Code == code));
                return true;
            }

            return false;
        }
    }
}
