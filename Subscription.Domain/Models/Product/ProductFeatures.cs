﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Subscription.Domain.Product
{
    public class ProductFeatures
    {
        private readonly List<Feature> _features;

        public ProductFeatures()
        {
            _features = new List<Feature>();
        }

        public bool ContainsFeature(string featureCode)
        {
            return _features.Any(x => x.Code == featureCode);
        }

        public bool AddFeature(string name, string code, string description)
        {
            if (ContainsFeature(code))
                return false;

            var feature = new Feature(name, code, description);
            _features.Add(feature);

            return true;
        }

        public bool RemoveFeature(string code)
        {
            if (!ContainsFeature(code))
                return false;

            _features.Remove(_features.First(x => x.Code == code));
            return true;
        }
    }
}
