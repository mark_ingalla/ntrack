using System;
using System.Collections.Generic;
using System.Linq;
using CommonDomain.Core;
using Subscription.Messages.Events.Product;

namespace Subscription.Domain.Models.Product
{
    //a product is a the unit that customer will subscribe to and pay the amount indicated in the product.
    public class Product : AggregateBase
    {
        public List<ProductFeature> Features { get; private set; }
        public List<ProductOffer> Offers { get; private set; }
        public bool IsActive { get; private set; }
        public string Code { get; private set; }
        decimal _basePrice;


        public Product() { }
        public Product(Guid id, string name, string code, string descritpion, string accessUrl, decimal basePrice)
        {
            if (id == Guid.Empty)
                throw new Exception("Empty product Key");

            RaiseEvent(new ProductCreated(id, name, code, descritpion, accessUrl, basePrice));
        }
        public static Product CreateProduct(Guid id, string name, string code, string description, string accessUrl, decimal basePrice)
        {
            return new Product(id, name, code, description, accessUrl, basePrice);
        }
        public void Update(string name, string code, string description, string accessUrl, decimal basePrice)
        {
            RaiseEvent(new ProductUpdated(Id, name, code, description, accessUrl, basePrice));
        }
        public void ActivateProduct()
        {
            RaiseEvent(new ProductActivated(Id));
        }
        public void DiscontinueProduct(string reason)
        {
            RaiseEvent(new ProductDiscontinued(Id, reason));
        }

        //Features
        public void AddFeature(string code, string name, decimal price, int limit = 0)
        {
            if (Features.Any(x => x.Code == code))
                throw new Exception("There's already feature with that code");

            RaiseEvent(new FeatureAdded(Id, code, name, price, limit));
        }
        public void UpdateFeature(string code, string name, decimal price, int limit = 0)
        {
            if (Features.All(x => x.Code != code))
                throw new Exception("There's no feature with that code");

            RaiseEvent(new FeatureUpdated(Id, code, name, price, limit));
        }
        public void RemoveFeature(string code, string reason)
        {
            if (Features.Single(x => x.Code == code).IsLocked)
                throw new Exception("This feature is already used!");

            RaiseEvent(new FeatureRemoved(Id, code, reason));
        }
        public void ActivateFeature(string code)
        {
            RaiseEvent(new FeatureActivated(Id, code));
        }
        public void DeactivateFeature(string code, string reason)
        {
            if (Features.All(x => x.Code != code))
                throw new Exception("No feature with this code exists!");

            RaiseEvent(new FeatureDeactivated(Id, code));
        }

        //offers
        public void AddOffer(Guid key, string name)
        {
            if (Offers.Any(x => x.Key == key))
                throw new Exception("Offer with the same key already exists!");

            RaiseEvent(new OfferAdded(Id, key, name, _basePrice));
        }
        public void UpdateOffer(Guid key, string name)
        {
            if (Offers.All(x => x.Key != key))
                throw new Exception("Offer with this key does not exists!");

            RaiseEvent(new OfferUpdated(Id, key, name));
        }
        public void RemoveOffer(Guid key, string reason)
        {
            if (Offers.Single(x => x.Key == key).IsLocked)
                throw new Exception("Unable to remove this offer! Offer is locked.");

            RaiseEvent(new OfferRemoved(Id, key, reason));
        }
        public void LockOffer(Guid key)
        {
            if (Offers.All(x => x.Key != key))
                throw new Exception("Offer with this key does not exists!");

            RaiseEvent(new OfferLocked(Id, key));
        }

        public void ActivateOffer(Guid key)
        {
            if (Offers.All(x => x.Key != key))
                throw new Exception("Offer with this key does not exists!");

            RaiseEvent(new OfferActivated(Id, key));
        }

        public void DeactivateOffer(Guid key, string reason)
        {
            if (Offers.All(x => x.Key != key))
                throw new Exception("Offer with this key does not exists!");

            RaiseEvent(new OfferDeactivated(Id, key, reason));
        }

        //public void AddOfferFeatures(Guid key, string[] featureCodes)
        public void UpdateOfferFeatures(Guid key, List<ProductFeature> features)
        {
            // if (Features.Count(x => featureCodes.Contains(x.Code) && x.IsActive) != featureCodes.Count())
            if (Features.Count(x => features.Any(y => y.Code == x.Code) && x.IsActive) != features.Count())
                throw new Exception("One or more Features not found or not active!");

            if (Offers.All(x => x.Key != key))
                throw new Exception("No offer found for this key!");

            if (Offers.Single(x => x.Key == key).IsLocked)
                throw new Exception("Unable to updated features! Offer is locked.");

            //RaiseEvent(new OfferFeaturesAdded(Id, key, featureCodes));
            RaiseEvent(new OfferFeaturesUpdated(Id, key, features.Select(x => new OfferFeature
                                                                              {
                                                                                  Code = x.Code,
                                                                                  Limit = x.Limit
                                                                              }).ToList()));
        }


        void Apply(ProductCreated @event)
        {
            Id = @event.ProductId;
            Features = new List<ProductFeature>();
            Offers = new List<ProductOffer>();
            _basePrice = @event.BasePrice;
            Code = @event.Code;
        }
        void Apply(ProductUpdated evt)
        {
            Code = evt.Code;
            _basePrice = evt.BasePrice;
        }
        void Apply(ProductActivated evt)
        {
            IsActive = true;
        }
        void Apply(ProductDiscontinued evt)
        {
            IsActive = false;
        }

        //Features
        void Apply(FeatureAdded evt)
        {
            Features.Add(new ProductFeature(evt.Code, evt.Name, evt.Price, evt.Limit));
        }
        void Apply(FeatureUpdated evt)
        {
            var f = Features.Single(x => x.Code == evt.Code);
            f.Name = evt.Name;
            f.Price = evt.Price;
            f.Limit = evt.Limit;
        }
        void Apply(FeatureRemoved evt)
        {
            Features.RemoveAll(x => x.Code == evt.FeatureCode);
        }
        void Apply(FeatureActivated evt)
        {
            Features.Single(x => x.Code == evt.FeatureCode).IsActive = true;
        }
        void Apply(FeatureDeactivated evt)
        {
            Features.Single(x => x.Code == evt.FeatureCode).IsActive = true;
        }

        //Offers
        void Apply(OfferAdded evt)
        {
            Offers.Add(new ProductOffer(evt.OfferKey, evt.BasePrice, evt.Name));
        }
        void Apply(OfferUpdated evt)
        {
            Offers.Single(x => x.Key == evt.OfferKey).Name = evt.OfferName;
        }
        void Apply(OfferRemoved evt)
        {
            Offers.RemoveAll(x => x.Key == evt.OfferKey);
        }
        void Apply(OfferLocked evt)
        {
            Offers.Single(x => x.Key == evt.OfferKey).IsLocked = true;
        }
        void Apply(OfferActivated evt)
        {
            Offers.Single(x => x.Key == evt.OfferKey).IsActive = true;
        }
        void Apply(OfferDeactivated evt)
        {
            Offers.Single(x => x.Key == evt.OfferKey).IsActive = false;
        }
        void Apply(OfferFeaturesAdded_V2 evt)
        {
            var offer = Offers.Single(x => x.Key == evt.OfferKey);

            //get a list of all features selected
            // var features = Features.Where(x => evt.FeatureCodes.Contains(x.Code)).ToList();
            var features = Features.Where(x => evt.Features.Any(y => y.Code == x.Code)).ToList();
            var oFeatures = offer.Features;

            var uniqueFeatures = features.Union(oFeatures).Distinct();
            offer.Features.Clear();
            offer.Features.AddRange(uniqueFeatures);
        }

        void Apply(OfferFeaturesUpdated evt)
        {
            var offer = Offers.Single(x => x.Key == evt.OfferKey);

            //get a list of all features selected
            // var features = Features.Where(x => evt.FeatureCodes.Contains(x.Code)).ToList();
            var features = Features.Where(x => evt.Features.Any(y => y.Code == x.Code)).ToList();

            offer.Features.Clear();
            offer.Features.AddRange(features);
        }

        void Apply(OfferFeaturesRemoved evt)
        {
            var offer = Offers.Single(x => x.Key == evt.OfferKey);
            //foreach (var code in evt.FeatureCodes)
            foreach (var code in evt.FeatureCodes)
            {
                //offer.Features.RemoveAll(x => x.Code == code);
                offer.Features.RemoveAll(x => x.Code == code);
            }
        }
    }
}