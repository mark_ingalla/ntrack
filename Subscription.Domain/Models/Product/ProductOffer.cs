using System;
using System.Collections.Generic;
using System.Linq;

namespace Subscription.Domain.Models.Product
{
    public class ProductOffer
    {
        readonly decimal _basePrice;
        public Guid Key { get; private set; }
        public string Name { get; set; }

        public List<ProductFeature> Features { get; set; }
        public decimal Price { get { return Features != null ? _basePrice + Features.Sum(x => x.Price) : _basePrice; } }
        public bool IsActive { get; set; }
        public bool IsLocked { get; set; }

        public ProductOffer(Guid key, decimal basePrice, string name)
        {
            _basePrice = basePrice;
            Key = key;
            Name = name;
            Features = new List<ProductFeature>();
        }
    }
}