
namespace Subscription.Domain.Product
{
    public sealed class Feature
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public Feature(string name, string code, string description)
        {
            Name = name;
            Code = code;
            Description = description;
        }
    }
}