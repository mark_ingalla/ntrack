namespace Subscription.Domain.Models.Product
{
    public class ProductFeature
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Limit { get; set; }
        public bool IsActive { get; set; }
        public bool IsLocked { get; set; }

        public ProductFeature(string code, string name, decimal price, int limit = 0)
        {
            Code = code;
            Name = name;
            Limit = limit;
            Price = price;
        }
    }
}