namespace Subscription.Domain.Product
{
    public sealed class Permission
    {
        public string Name { get; set; }
        public string Code { get; set; }

        public Permission(string name, string code)
        {
            Name = name;
            Code = code;
        }
    }
}