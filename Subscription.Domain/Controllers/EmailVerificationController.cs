﻿using System;
using NServiceBus;
using NServiceBus.Saga;
using Subscription.Messages.Commands;
using Subscription.Messages.Events;

namespace Subscription.Controllers
{
    //class EmailVerificationController : Saga<EmailVerificationControllerData>,
    //    IAmStartedByMessages<UserCreated>,
    //    IHandleMessages<ActivateUser>,
    //    IHandleTimeouts<EmailVerificationController.EmailVerifyTimeout>
    //{
        // ReSharper disable InconsistentNaming
    //    const int TIMEOUT_INTERVAL = 24;
    //    // ReSharper restore InconsistentNaming

    //    public override void ConfigureHowToFindSaga()
    //    {
    //        ConfigureMapping<ActivateUser>(s => s.EmailVerificationToken, m => m.EmailVerificationToken);
    //    }

    //    public void Handle(UserCreated message)
    //    {
    //        //Data.EmailVerificationToken = message.EmailVerificationToken;
    //        Data.UserId = message.UserId;

    //        RequestUtcTimeout<EmailVerifyTimeout>(TimeSpan.FromHours(TIMEOUT_INTERVAL));
    //    }

    //    public void Handle(ActivateUser message)
    //    {
    //        Bus.Send(new ActivateUserDomain()
    //                     {
    //                         UserId = Data.UserId,
    //                     });

    //        Bus.Publish(new UserActivated()
    //                        {
    //                            UserId = Data.UserId
    //                        });

    //        MarkAsComplete();
    //    }

    //    public void Timeout(EmailVerifyTimeout state)
    //    {
    //        //TODO inform somebody about failure
    //        MarkAsComplete();
    //    }

    //    public class EmailVerifyTimeout { }
    //}
}
