﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NServiceBus.Saga;

namespace Subscription.Domain.Controllers
{
    public class BillingPaymentData : ISagaEntity
    {
        public Guid Id { get; set; }
        public string Originator { get; set; }
        public string OriginalMessageId { get; set; }

        [Unique]
        public Guid BillingPaymentToken { get; set; }

        public Guid AccountId { get; set; }

        public Guid ProductId { get; set; }
        public Guid UserId { get; set; }
     
    }

}
