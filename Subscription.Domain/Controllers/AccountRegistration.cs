using NServiceBus;
using NServiceBus.Saga;
using Subscription.Domain.Models.Product;
using nTrack.Core.Messages;
using nTrack.EmailService.Messages.Events;
using Subscription.Domain.Models.Account;
using Subscription.Messages.Commands;
using Subscription.Messages.Commands.Account;
using Subscription.Messages.Commands.Account.User;
using Subscription.Messages.Events;
using Subscription.Messages.Events.Account;
using Subscription.Services.Messages.Commands.Email;
using System;
using System.Collections.Generic;

namespace Subscription.Domain.Controllers
{
    public class AccountRegistration : Saga<AccountRegistrationData>,
        IAmStartedByMessages<RegisterAccount>,
        IHandleMessages<AccountCreated>,
        IHandleMessages<VerifyEmail>,
        IHandleTimeouts<RegistrationTimeout>,

    IHandleMessages<EmailSent>
    {
        const int TIMEOUT_INTERVAL = 24;

        public override void ConfigureHowToFindSaga()
        {
            ConfigureMapping<AccountCreated>(s => s.AccountId, m => m.AccountId);
            ConfigureMapping<VerifyEmail>(s => s.EmailVerificationToken, m => m.EmailVerificationToken);
            ConfigureMapping<EmailSent>(s => s.UserEmail, m => m.Email);
        }

        public void Handle(RegisterAccount message)
        {
            Data.EmailVerificationToken = message.EmailVerificationToken;
            Data.AccountId = message.AccountId;
            Data.UserKey = message.UserKey;
            Data.EmailVerificationPageUrl = message.EmailVerificationPageUrl;
            Data.UserEmail = message.Email;
            Data.Fullname = string.Format("{0} {1}", message.FirstName, message.LastName).Trim();
            Data.OfferKey = message.OfferKey;
            Data.ProductId = message.ProductId;
            Data.OfferName = message.OfferName;
            Data.ProductName = message.ProductName;
            Data.OfferQTY = message.OfferQTY;

            //create the company, add the admin user
            var cmds = new object[]
                {
                    new CreateAccount
                        {
                            AccountId = message.AccountId,
                            CompanyName = message.CompanyName,
                            CompanyPhone = message.CompanyPhone,
                            Country = message.Country,
                            AddressIP = message.IPAddress

                        },
                        new AddUser
                            {
                                AccountId = message.AccountId,
                                UserKey = message.UserKey,
                                Email = message.Email,
                                Password = message.Password,
                                FirstName = message.FirstName,
                                LastName = message.LastName,
                                Role = UserRole.Admin
                            }

                };
            Bus.Send(cmds);
        }
        public void Handle(AccountCreated message)
        {
            var toEmails = new Dictionary<string, string> { { Data.UserEmail, Data.Fullname } };
            Bus.Send(new SendVerificationEmail { ToEmails = toEmails, VerificationUrl = Data.EmailVerificationPageUrl, OfferName = Data.OfferName,
            ProductName = Data.ProductName});

            PublishProcessStatus(Account.InvitationStatus.Pending.ToString());
            RequestUtcTimeout<RegistrationTimeout>(TimeSpan.FromHours(TIMEOUT_INTERVAL));
        }
        public void Handle(VerifyEmail message)
        {
            Bus.Send(new ActivateAccount { AccountId = Data.AccountId });
            Bus.Send(new CreateSubscription
            {
                AccountId = Data.AccountId,
                ProductId = Data.ProductId,
                OfferKey = Data.OfferKey,
                SubscriptionKey = Guid.NewGuid(),
                OfferQTY = Data.OfferQTY
            });


            PublishProcessStatus(Account.InvitationStatus.Completed.ToString());
            MarkAsComplete();
        }
        public void Handle(EmailSent message)
        {
            if (!message.IsSucessful)
            {
                PublishProcessStatus(Account.InvitationStatus.Error.ToString());
                MarkAsComplete();
                return;
            }

            PublishProcessStatus(Account.InvitationStatus.EmailSent.ToString());
        }

        public void Timeout(RegistrationTimeout state)
        {
            PublishProcessStatus(Account.InvitationStatus.TimedOut.ToString());
            MarkAsComplete();
        }

        void PublishProcessStatus(string status)
        {
            Bus.Publish(new UserVerificationStatus(Data.AccountId, Data.UserKey, Data.EmailVerificationToken, status));
        }

    }

    public class RegistrationTimeout
    {

    }
}