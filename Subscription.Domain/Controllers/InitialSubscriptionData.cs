﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NServiceBus.Saga;

namespace Subscription.Domain.Controllers
{
    public class InitialSubscriptionData : ISagaEntity
    {
        public Guid Id { get; set; }
        public string Originator { get; set; }

        public string OriginalMessageId { get; set; }

        public Guid AccountId { get; set; }
        public Guid ProductId { get; set; }

        [Unique]
        public Guid SubscriptionKey { get; set; }

    }
}
