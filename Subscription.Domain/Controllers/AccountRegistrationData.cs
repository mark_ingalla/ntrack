using NServiceBus.Saga;
using System;

namespace Subscription.Domain.Controllers
{
    public class AccountRegistrationData : ISagaEntity
    {
        public Guid Id { get; set; }
        public string Originator { get; set; }
        public string OriginalMessageId { get; set; }

        public Guid EmailVerificationToken { get; set; }

        [Unique]
        public Guid AccountId { get; set; }

        public Guid UserKey { get; set; }
        public string UserEmail { get; set; }

        public string CompanyName { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyCountry { get; set; }
        public string EmailVerificationPageUrl { get; set; }
        public string IPAddress { get; set; }
        public string Fullname { get; set; }
        public Guid OfferKey { get; set; }
        public Guid ProductId { get; set; }
        public string OfferName { get; set; }
        public string ProductName { get; set; }
        public int OfferQTY { get; set; }
    }
}