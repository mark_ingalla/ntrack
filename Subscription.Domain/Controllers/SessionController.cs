﻿using System;
using CommonDomain.Persistence;
using NServiceBus;
using NServiceBus.Saga;
using StructureMap.Attributes;
using Subscription.Domain.Models.Account;
using Subscription.Messages.Commands.Account;
using Subscription.Messages.Commands.Account.User;
using Subscription.Messages.Events;

namespace Subscription.Domain.Controllers
{
    public class SessionController : Saga<SessionControllerData>,
        IAmStartedByMessages<LoginUser>,
        IHandleTimeouts<SessionController.TokenTimeout>,
        IHandleMessages<TokenSeen>,
        IHandleMessages<LogOut>
    {
        [SetterProperty]
        public IRepository Repository { get; set; }

        // ReSharper disable InconsistentNaming
        const int TIMEOUT_INTERVAL = 30;
        const int RETRY_TIMEOUT_INTERVAL = 4;
        // ReSharper restore InconsistentNaming

        public override void ConfigureHowToFindSaga()
        {
            ConfigureMapping<TokenSeen>(s => s.Token, t => t.Token);
            ConfigureMapping<LogOut>(s => s.Token, t => t.SessionToken);
        }

        public void Handle(LoginUser message)
        {
            Data.UserId = message.UserId;
            Data.Token = message.SessionToken;
            Data.LastSeen = DateTime.UtcNow;

            var account = Repository.GetById<Account>(message.AccountId);

            account.LoginUser(message.UserId, message.SessionToken);

#if DEBUG
            RequestUtcTimeout<TokenTimeout>(TimeSpan.FromSeconds(TIMEOUT_INTERVAL));
#else
            RequestUtcTimeout<TokenTimeout>(TimeSpan.FromMinutes(TIMEOUT_INTERVAL));            
#endif
        }

        public void Handle(LogOut message)
        {
            Bus.Publish<LoggedOut>(
                e =>
                {
                    e.UserId = Data.UserId;
                    e.SessionToken = Data.Token;
                    e.LoggedOutOn = DateTime.UtcNow;
                });

            MarkAsComplete();
        }
        public void Timeout(TokenTimeout state)
        {
            //check when the last token used
            if (Data.LastSeen.AddSeconds(24) < DateTime.UtcNow)
            {
                Bus.Publish<LoggedOut>(
                    e =>
                    {
                        e.SessionToken = Data.Token;
                        e.UserId = Data.UserId;
                    });
                MarkAsComplete();
            }
            else
            {
#if DEBUG
                RequestUtcTimeout<TokenTimeout>(TimeSpan.FromMinutes(RETRY_TIMEOUT_INTERVAL));
#else
                RequestUtcTimeout<TokenTimeout>(TimeSpan.FromMinutes(RETRY_TIMEOUT_INTERVAL));            
#endif
            }
        }

        public class TokenTimeout
        {
        }

        public void Handle(TokenSeen message)
        {
            Data.LastSeen = message.SeenOn;
        }
    }
}
