﻿using System;
using System.Linq;
using CommonDomain.Persistence;
using NServiceBus;
using NServiceBus.Saga;
using nTrack.Core.Messages;
using nTrack.EmailService.Messages.Events;
using Raven.Client;
using StructureMap;
using Subscription.Domain.DomainServices.Users;
using Subscription.Domain.Models.Account;
using Subscription.Domain.Models.User;
using Subscription.Messages.Commands.Account;
using Subscription.Messages.Events;

namespace Subscription.Domain.Controllers
{
    public class UserInvitationController : Saga<UserInvitationControllerData>,
        IAmStartedByMessages<InviteUser>,
        IHandleMessages<RegisterInvitedUser>,
        IHandleTimeouts<InvitationTimeout>,
        IHandleMessages<EmailServiceStatus>,
        IHandleMessages<AssignExistingUserToAccount>
    {
        private readonly IRepository _repository;
        private readonly IDocumentSession _session;

        // ReSharper disable InconsistentNaming
        const int TIMEOUT_INTERVAL = 24;
        // ReSharper restore InconsistentNaming

        public override void ConfigureHowToFindSaga()
        {
            ConfigureMapping<RegisterInvitedUser>(s => s.InvitationToken, m => m.InvitationToken);
            ConfigureMapping<AssignExistingUserToAccount>(s => s.InvitationToken, m => m.InvitationToken);
            ConfigureMapping<EmailServiceStatus>(s => s.InvitationToken, m => m.Token);
        }

        public UserInvitationController()
        {
            _repository = ObjectFactory.GetInstance<IRepository>();
            _session = ObjectFactory.GetInstance<IDocumentSession>();
        }

        public void Handle(InviteUser message)
        {
            Data.AccountId = message.AccountId;
            Data.RoleName = message.RoleName;
            Data.ProductsPersmissions = message.ProductsPersmissions;
            Data.UserEmail = message.Email;
            Data.InvitationToken = message.EmailInvitationToken;

            var account = _repository.GetById<Account>(message.AccountId);

            account.InviteUser(message.Email);
            _repository.Save(account, message.Id);

            var user = _session.Query<DomainUserModel>().SingleOrDefault(x => x.Email == message.Email);
            if (user != null)
            {
                Data.ExistingUserId = user.Id;
                // TODO: Uncomment this when the email service is ready.
                //Bus.Send(new SendInvitationConfirmationEmail(message.Email, Data.InvitationToken));
            }
            else
            {
                // TODO: Uncomment this when the email service is ready
                // Bus.Send(new SendInvitationEmail(message.Email, Data.InvitationToken));
            }

            PublishProcessStatus(Data.InvitationToken, Data.UserEmail, Account.InvitationStatus.Pending.ToString());
            RequestUtcTimeout<InvitationTimeout>(TimeSpan.FromHours(TIMEOUT_INTERVAL));
        }
        public void Handle(RegisterInvitedUser message)
        {
            var userId = Guid.NewGuid();

            var user = User.CreateUser(userId, message.FirstName, message.LastName, Data.UserEmail, message.Password);
            user.VerifyUser();
            user.AssignUserToAccount(Data.AccountId, Data.RoleName);

            _repository.Save(user, message.Id);

            var account = _repository.GetById<Account>(Data.AccountId);
            account.AuthorizeUser(userId);
            account.ChangeInvitationStatus(Data.UserEmail, Account.InvitationStatus.Completed);
            
            _repository.Save(account, message.Id);


            //for each product add permissions (assign them to invitation role)
            foreach (var productPerimissions in Data.ProductsPersmissions)
            {
                productPerimissions.Value.ForEach(x => user.AddPermission(Data.AccountId, Data.RoleName, productPerimissions.Key, x));
            }

            _repository.Save(user, Guid.NewGuid());

            PublishProcessStatus(Data.InvitationToken, Data.UserEmail, Account.InvitationStatus.Completed.ToString());

            MarkAsComplete();
        }
        public void Handle(AssignExistingUserToAccount message)
        {
            var account = _repository.GetById<Account>(Data.AccountId);

            account.ChangeInvitationStatus(Data.UserEmail, Account.InvitationStatus.Completed);
            account.AuthorizeUser(Data.ExistingUserId);

            _repository.Save(account, message.Id);

            var user = _repository.GetById<User>(Data.ExistingUserId);

            user.AssignUserToAccount(Data.AccountId, Data.RoleName);
            //for each product add permissions (assign them to invitation role)
            foreach (var productPerimissions in Data.ProductsPersmissions)
            {
                productPerimissions.Value.ForEach(x => user.AddPermission(Data.AccountId, Data.RoleName, productPerimissions.Key, x));
            }
            _repository.Save(user, message.Id);

            PublishProcessStatus(Data.InvitationToken, Data.UserEmail, Account.InvitationStatus.Completed.ToString());
            MarkAsComplete();
        }
        public void Handle(EmailServiceStatus message)
        {
            if (!message.Sucess)
            {
                var account = _repository.GetById<Account>(Data.AccountId);
                account.ChangeInvitationStatus(Data.UserEmail, Account.InvitationStatus.BouncedBack);

                _repository.Save(account, message.Id);
                PublishProcessStatus(Data.InvitationToken, Data.UserEmail, Account.InvitationStatus.BouncedBack.ToString());
                MarkAsComplete();

                return;
            }

            PublishProcessStatus(Data.InvitationToken, Data.UserEmail, Account.InvitationStatus.Pending.ToString());
        }
        public void Timeout(InvitationTimeout state)
        {
            var account = _repository.GetById<Account>(Data.AccountId);
            account.ChangeInvitationStatus(Data.UserEmail, Account.InvitationStatus.Timeouted);
            _repository.Save(account, state.Id);

            PublishProcessStatus(Data.InvitationToken, Data.UserEmail, Account.InvitationStatus.Timeouted.ToString());
            MarkAsComplete();
        }

        void PublishProcessStatus(Guid emailVerToken, string userEmail, string status)
        {
            Bus.Publish(new UserInvitationStatus(emailVerToken, userEmail, status));
        }
    }

    public class InvitationTimeout : MessageBase
    {
    }
}
