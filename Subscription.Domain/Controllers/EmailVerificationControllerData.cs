﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NServiceBus.Saga;

namespace Subscription.Controllers
{
    public class EmailVerificationControllerData : ISagaEntity
    {
        public Guid Id { get; set; }
        public string Originator { get; set; }
        public string OriginalMessageId { get; set; }

        public Guid EmailVerificationToken { get; set; }

        public Guid UserId { get; set; }
    }
}
