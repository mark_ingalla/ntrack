﻿using System;
using NServiceBus.Saga;

namespace Subscription.Domain.Controllers
{
    public class SessionControllerData : ISagaEntity
    {
        public Guid Id { get; set; }
        public string Originator { get; set; }
        public string OriginalMessageId { get; set; }

        [Unique]
        public Guid Token { get; set; }
        public Guid UserId { get; set; }
        public DateTime LastSeen { get; set; }
    }
}
