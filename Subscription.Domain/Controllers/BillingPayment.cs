﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonDomain.Persistence;
using NServiceBus;
using NServiceBus.Saga;
using StructureMap.Attributes;
using Subscription.Domain.Models.Account;
using Subscription.Messages.Commands.Account;
using nTrack.Core.Messages;
using nTrack.EmailService.Messages.Events;

namespace Subscription.Domain.Controllers
{
    public class BillingPayment : Saga<BillingPaymentData>,
        IAmStartedByMessages<StartPayment>,
        IHandleTimeouts<CheckPaymentTimeout>
    {
        // ReSharper disable InconsistentNaming
        const int TIMEOUT_INTERVAL = 30;
        // ReSharper restore InconsistentNaming

        [SetterProperty]
        public IRepository Repository { get; set; }

        public override void ConfigureHowToFindSaga()
        {
        }

        public void Handle(StartPayment message)
        {
            Data.BillingPaymentToken = message.BillingPaymentToken;
            Data.AccountId = message.AccountId;
            Data.ProductId = message.ProductId;

            var account = Repository.GetById<Account>(Data.AccountId);

            account.StartPayment(message.ProductId);

            Repository.Save(account, message.Id);

            RequestUtcTimeout<CheckPaymentTimeout>(TimeSpan.FromSeconds(TIMEOUT_INTERVAL));
        }

        public void Timeout(CheckPaymentTimeout state)
        {
            bool payed = true;

            if (payed)
            {
                var account = Repository.GetById<Account>(Data.AccountId);
                account.EndPayment(Data.ProductId);
                Repository.Save(account, state.Id);
                MarkAsComplete();
            }
            else
                RequestUtcTimeout<CheckPaymentTimeout>(TimeSpan.FromSeconds(TIMEOUT_INTERVAL));
        }
    }

    public class CheckPaymentTimeout : MessageBase
    {
        
    }
}
