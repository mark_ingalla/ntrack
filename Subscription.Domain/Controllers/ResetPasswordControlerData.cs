﻿using System;
using NServiceBus.Saga;

namespace Subscription.Domain.Controllers
{
    public class ResetPasswordControlerData : ISagaEntity
    {
        public Guid Id { get; set; }
        public string Originator { get; set; }
        public string OriginalMessageId { get; set; }

        [Unique]
        public Guid ResetPasswordToken { get; set; }

        public Guid AccountId { get; set; }
        public Guid UserKey { get; set; }

        public string Email { get; set; }
        public string UserFullname { get; set; }
    }
}
