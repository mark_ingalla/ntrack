﻿using System;
using System.Collections.Generic;
using NServiceBus.Saga;

namespace Subscription.Domain.Controllers
{
    public class UserInvitationControllerData : ISagaEntity
    {
        public Guid Id { get; set; }
        public string Originator { get; set; }
        public string OriginalMessageId { get; set; }

        [Unique]
        public Guid InvitationToken { get; set; }
        public Guid AccountId { get; set; }

        public string RoleName { get; set; }
        //products and permissions assigned to role
        public Dictionary<Guid, List<string>> ProductsPersmissions { get; set; }

        public Guid ExistingUserId { get; set; }

        public string UserEmail { get; set; }
    }
}
