﻿using System;
using System.Collections.Generic;
using NServiceBus;
using NServiceBus.Saga;
using Subscription.Domain.Models.Account;
using Subscription.Messages.Commands;
using Subscription.Messages.Commands.Account.User;

using Subscription.Messages.Events;
using Subscription.Services.Messages.Commands.Email;

namespace Subscription.Domain.Controllers
{
    public class ResetPasswordController : Saga<ResetPasswordControlerData>,
        IAmStartedByMessages<PrepareResetPassword>,
        IHandleMessages<ResetPassword>,
        IHandleTimeouts<ResetPassTimeout>
    {
        const int TIMEOUT_INTERVAL = 24;

        public override void ConfigureHowToFindSaga()
        {
            ConfigureMapping<ResetPassword>(s => s.ResetPasswordToken, m => m.ResetPasswordToken);
        }

        public void Handle(PrepareResetPassword message)
        {
            Data.AccountId = message.AccountId;
            Data.UserKey = message.UserKey;
            Data.Email = message.UserEmail;
            Data.ResetPasswordToken = message.ResetPasswordToken;
            Data.UserFullname = message.UserFullname;

            Bus.Send(new SendResetPasswordEmail
                {
                    ToEmails = new Dictionary<string, string> { { message.UserEmail, message.UserFullname } },
                    ResetPasswordUrl = message.ResetPasswordLink
                });

            PublishProcessStatus(Account.PasswordResetStatus.Pending);

            RequestUtcTimeout<ResetPassTimeout>(TimeSpan.FromHours(TIMEOUT_INTERVAL));
        }
        public void Handle(ResetPassword message)
        {

            Bus.Send(new ChangeUserPassword
                {
                    AccountId = Data.AccountId,
                    UserKey = Data.UserKey,
                    NewPassword = message.NewPassword
                });

            Bus.Send(new SendNewPasswordEmail
                {
                    ToEmails = new Dictionary<string, string> { { Data.Email, Data.UserFullname } },
                    NewPassword = message.NewPassword
                });

            PublishProcessStatus(Account.PasswordResetStatus.Completed);

            MarkAsComplete();
        }
        public void Timeout(ResetPassTimeout state)
        {
            PublishProcessStatus(Account.PasswordResetStatus.Timeouted);

            MarkAsComplete();
        }

        void PublishProcessStatus(Account.PasswordResetStatus status)
        {
            Bus.Publish(new UserPasswordResetStatus(Data.AccountId, Data.UserKey, Data.ResetPasswordToken, Data.Email,
                                                    status.ToString()));
        }
    }

    public class ResetPassTimeout { }
}
