﻿using CommonDomain.Persistence;
using NServiceBus;
using NServiceBus.Saga;
using Subscription.Messages.Commands.Account;
using nTrack.Core.Messages;
using StructureMap.Attributes;
using Subscription.Domain.Models.Account;
using Subscription.Messages.Events.Account;
using System;

namespace Subscription.Domain.Controllers
{
    public class InitialSubscription : Saga<InitialSubscriptionData>,
        IAmStartedByMessages<SubscriptionCreated>,
        IHandleTimeouts<SubscriptionCreatedTimeout>,
        IHandleMessages<SubscriptionTerminated>
    {
        // ReSharper disable InconsistentNaming
        const int TIMEOUT_INTERVAL = 1;
        // ReSharper restore InconsistentNaming

        public override void ConfigureHowToFindSaga()
        {
            ConfigureMapping<SubscriptionTerminated>(s => s.SubscriptionKey, m => m.SubscriptionKey);
        }

        public void Handle(SubscriptionCreated message)
        {
            Data.SubscriptionKey = message.SubscriptionKey;
            Data.ProductId = message.ProductId;
            Data.AccountId = message.AccountId;

            RequestUtcTimeout<SubscriptionCreatedTimeout>(TimeSpan.FromSeconds(TIMEOUT_INTERVAL));
        }

        public void Timeout(SubscriptionCreatedTimeout state)
        {
            //TODO: what happen when the timeout expire. Currently we raise the invoice

            Bus.Send(new RaiseInvoice {AccountId = Data.AccountId, InvoiceId = Guid.NewGuid()});

            MarkAsComplete();
        }

        public void Handle(SubscriptionTerminated message)
        {
            MarkAsComplete();
        }
    }

    public class SubscriptionCreatedTimeout : MessageBase
    {

    }
}
