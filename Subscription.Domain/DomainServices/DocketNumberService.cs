﻿using System;
using Raven.Client;
using StructureMap;

namespace Subscription.Domain.DomainServices
{
    public class InvoiceNumberService
    {
        public IDocumentSession Session { get; set; }

        public InvoiceNumberService()
        {
            Session = ObjectFactory.GetInstance<IDocumentSession>();
        }

        public int GetNextInvoiceNumber()
        {
            var sequence = Session.Load<InvoiceNumberSequence>(InvoiceNumberSequence.InvoiceNumberSequenceId);
            if (sequence == null)
            {
                sequence = new InvoiceNumberSequence {LastNumber = 0, Id = InvoiceNumberSequence.InvoiceNumberSequenceId};
                Session.Store(sequence);
            }

            var ret = sequence.GetNextNumber();
            Session.SaveChanges();
            return ret;
        }

    }
}
