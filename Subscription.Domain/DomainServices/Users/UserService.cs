﻿using NServiceBus;
using Raven.Client;
using Subscription.Messages.Events.User;

namespace Subscription.Domain.DomainServices.Users
{
    public class UserService : 
        IHandleMessages<UserCreated>,
        IHandleMessages<UserDetailUpdated>
    {
        readonly IDocumentSession _session;

        public UserService(IDocumentSession session)
        {
            _session = session;
        }

        public void Handle(UserCreated message)
        {
            var user = new DomainUserModel(message.UserId, message.Email);

            _session.Store(user);
            _session.SaveChanges();
        }

        public void Handle(UserDetailUpdated message)
        {
            var user = _session.Load<DomainUserModel>(message.UserId);
            user.Email = message.Email;
            _session.SaveChanges();
        }
    }
}
