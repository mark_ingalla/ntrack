﻿using System;

namespace Subscription.Domain.DomainServices.Users
{
    public class DomainUserModel
    {
        public Guid Id { get; set; }
        public string Email { get; set; }

        public DomainUserModel(Guid userId, string email)
        {
            Id= userId;
            Email = email;
        }
    }
}
