﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Subscription.Domain.DomainServices
{
    public class InvoiceNumberSequence
    {
        public static int InvoiceNumberSequenceId { get { return 1; } }
        public int Id { get; set; }
        public int LastNumber { get; set; }
        public int GetNextNumber()
        {
            LastNumber++;
            return LastNumber;
        }
    }
}
