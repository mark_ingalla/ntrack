﻿using System.Linq;
using EventStore.RavenDb.Conversion;
using Subscription.Messages.Events.Product;

namespace Subscription.Domain.Converters
{
    public class OfferFeatureConverter :
        IUpconvertEvents<OfferFeaturesAdded, OfferFeaturesAdded_V2>
    {
        public OfferFeaturesAdded_V2 Convert(OfferFeaturesAdded sourceEvent)
        {
            var xs = sourceEvent.FeatureCodes.Select(x => new OfferFeature { Code = x, Limit = 0 }).ToList();
            return new OfferFeaturesAdded_V2(sourceEvent.ProductId, sourceEvent.OfferKey, xs);
        }

    }
}
