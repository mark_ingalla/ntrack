﻿using CommonDomain.Persistence;
using NServiceBus;
using Subscription.Domain.Models.User;
using Subscription.Messages.Commands.User;

namespace Subscription.Domain.Handlers
{
    class UserCommandsHandler :
        IHandleMessages<UpdateUserDetail>,
        IHandleMessages<AssignRoleToUser>,
        IHandleMessages<RevokeRoleFromUser>,
        IHandleMessages<AddPermissionToUser>,
        IHandleMessages<RevokePermissionFromUser>,
        IHandleMessages<PromoteUserToAdmin>,
        IHandleMessages<DemoteAdminToUser>,
        IHandleMessages<LockUser>,
        IHandleMessages<UnlockUser>
    {
        private readonly IRepository _repository;
        private readonly IBus _bus;

        public UserCommandsHandler(IRepository repository, IBus bus)
        {
            _repository = repository;
            _bus = bus;
        }
        
        public void Handle(UpdateUserDetail message)
        {
            var user = _repository.GetById<User>(message.UserId);
            user.UpdateUserDetail(message.FirstName,message.LastName,message.Phone,message.Mobile,message.Email);
            _repository.Save(user, message.Id);
        }
        public void Handle(AssignRoleToUser message)
        {
            var user = _repository.GetById<User>(message.UserId);

            foreach (var productsPermission in message.RoleProductsPermissions)
            {
                productsPermission.Value.ForEach(x => user.AddPermission(message.AccountId, message.RoleName, productsPermission.Key, x));
            }

            _repository.Save(user, message.Id);
        }
        public void Handle(RevokeRoleFromUser message)
        {
            var user = _repository.GetById<User>(message.UserId);

            foreach (var productsPermission in message.RoleProductsPermissions)
            {
                productsPermission.Value.ForEach(x => user.RevokePermission(message.AccountId, message.RoleName, productsPermission.Key, x));
            }

            _repository.Save(user, message.Id);
        }
        public void Handle(AddPermissionToUser message)
        {
            var user = _repository.GetById<User>(message.UserId);

            user.AddPermission(message.AccountId, message.Role, message.ProductId, message.Permission);

            _repository.Save(user, message.Id);
        }
        public void Handle(RevokePermissionFromUser message)
        {
            var user = _repository.GetById<User>(message.UserId);

            user.RevokePermission(message.AccountId, message.RoleName, message.ProductId, message.Permission);

            _repository.Save(user, message.Id);
        }
        public void Handle(DemoteAdminToUser message)
        {
            var user = _repository.GetById<User>(message.UserId);
            user.DemoteToUser(message.AccountId);
            _repository.Save(user, message.Id);
        }
        public void Handle(PromoteUserToAdmin message)
        {
            var user = _repository.GetById<User>(message.UserId);
            user.PromoteToAdmin(message.AccountId);
            _repository.Save(user, message.Id);
        }
        public void Handle(LockUser message)
        {
            var user = _repository.GetById<User>(message.UserId);
            user.LockUser(message.Reason);
            _repository.Save(user, message.Id);
        }
        public void Handle(UnlockUser message)
        {
            var user = _repository.GetById<User>(message.UserId);
            user.UnlockUser(message.Reason);
            _repository.Save(user, message.Id);
        }

        
    }
}
