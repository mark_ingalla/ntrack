﻿using NServiceBus;
using Subscription.Messages.Commands;
using Subscription.Messages.Commands.Account;
using Subscription.Messages.Events;

namespace Subscription.Domain.Handlers
{
    class TokenHandler : IHandleMessages<SeeToken>
    {
        readonly IBus _bus;

        public TokenHandler(IBus bus)
        {
            _bus = bus;
        }

        public void Handle(SeeToken message)
        {
            _bus.Publish<TokenSeen>(e =>
                                         {
                                             e.Token = message.Token;
                                             e.SeenOn = message.SeenOn;
                                         });
        }
    }
}
