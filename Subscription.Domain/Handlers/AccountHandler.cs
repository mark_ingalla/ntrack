﻿using System.Linq;
using NServiceBus;
using nTrack.Core.Utilities;
using Subscription.Domain.Models.Product;
using Subscription.Messages.Commands.Account;
using Subscription.Messages.Commands.Account.User;
using Account = Subscription.Domain.Models.Account.Account;

namespace Subscription.Domain.Handlers
{
    public class AccountHandler : HandlerBase,

        IHandleMessages<CreateAccount>,
        IHandleMessages<UpdateAccount>,
        IHandleMessages<ActivateAccount>,
        IHandleMessages<DeactivateAccount>,


        //User Management
        IHandleMessages<AddUser>,
        IHandleMessages<RemoveUser>,
        IHandleMessages<UpdateUserDetail>,
        IHandleMessages<LockUser>,
        IHandleMessages<UnlockUser>,
        IHandleMessages<ChangeUserPassword>,
        IHandleMessages<ChangeUserRole>,
        IHandleMessages<RetrieveUser>,


                                  //Billing
        IHandleMessages<CreateSubscription>,
        IHandleMessages<TerminateSubscription>,
        IHandleMessages<RaiseInvoice>,
        IHandleMessages<PayInvoice>
    {
        public void Handle(CreateAccount message)
        {
            var account = Account.CreateAccount(message.AccountId, message.CompanyName, message.CompanyPhone,
                                                message.Country, message.AddressIP);
            Save(account, message.Id);

            //account.AddUser(message.UserKey, message.Email, message.Password, message.UserFullname, message.LastName);

            //TODO: set this user as admin for this account


        }
        public void Handle(UpdateAccount message)
        {
            var account = Repository.GetById<Account>(message.AccountId);
            account.UpdateAccount(message.CompanyName, message.CompanyPhone, message.AddressLine1, message.AddressLine2,
                                  message.Suburb, message.State, message.Postcode, message.Country);
            Save(account, message.Id);
        }
        public void Handle(ActivateAccount message)
        {
            var account = Repository.GetById<Account>(message.AccountId);
            account.Activate();
            Save(account, message.Id);
        }
        public void Handle(DeactivateAccount message)
        {
            var account = Repository.GetById<Account>(message.AccountId);
            account.Deactivate(message.Reason);
            Save(account, message.Id);
        }


        //User Management
        public void Handle(AddUser message)
        {
            //TODO: talk to jakub about not using this here.. this should be done at the command sender!
            //NOTE I need this, because with out it, users can't have the same password. If this is wrong usage of it, please, inform me. 
            //NOTE: moved hasing password to denormalizer

            var account = Repository.GetById<Account>(message.AccountId);
            account.AddUser(message.UserKey, message.Email, message.Password, message.FirstName, message.LastName,
                            message.Role);
            Save(account, message.Id);
        }

        public void Handle(RemoveUser message)
        {
            var account = Repository.GetById<Account>(message.AccountId);
            account.RemoveUser(message.UserKey);
            Save(account, message.Id);
        }

        public void Handle(RetrieveUser message)
        {
            var account = Repository.GetById<Account>(message.AccountId);
            account.RetrieveUser(message.UserKey);
            Save(account, message.Id);
        }

        public void Handle(UpdateUserDetail message)
        {
            var account = Repository.GetById<Account>(message.AccountId);
            account.UpdateUserDetail(message.UserKey, message.FirstName, message.LastName, message.Phone, message.Mobile,
                                     message.Email);
            Save(account, message.Id);
        }
        public void Handle(LockUser message)
        {
            var account = Repository.GetById<Account>(message.AccountId);
            account.LockUser(message.UserKey, message.Reason);
            Save(account, message.Id);
        }
        public void Handle(UnlockUser message)
        {
            var account = Repository.GetById<Account>(message.AccountId);
            account.UnlockUser(message.UserKey, message.Reason);
            Save(account, message.Id);
        }
        public void Handle(ChangeUserPassword message)
        {
            var cryptoService = new CryptographyService();

            var account = Repository.GetById<Account>(message.AccountId);
            account.ChangeUserPassword(message.UserKey, cryptoService.GetMD5Hash(message.NewPassword));
            Save(account, message.Id);
        }
        public void Handle(ChangeUserRole message)
        {
            var account = Repository.GetById<Account>(message.AccountId);
            account.ChangeUserRole(message.UserKey, message.Role);
            Save(account, message.Id);
        }

        //Billing
        public void Handle(CreateSubscription message)
        {
            var product = Repository.GetById<Product>(message.ProductId);
            var offer = product.Offers.Single(x => x.Key == message.OfferKey);

            var account = Repository.GetById<Account>(message.AccountId);
            account.Subscribe(message.ProductId, product.Code, message.SubscriptionKey, message.OfferKey, offer.Price, message.OfferQTY);
            Save(account, message.Id);
        }
        public void Handle(TerminateSubscription message)
        {
            var account = Repository.GetById<Account>(message.AccountId);
            account.Unsubscribe(message.SubscriptionKey);
            Save(account, message.Id);
        }
        public void Handle(RaiseInvoice message)
        {
            var account = Repository.GetById<Account>(message.AccountId);
            account.RaiseInvoice(message.InvoiceId);
            Save(account, message.Id);
        }
        public void Handle(PayInvoice message)
        {
            var account = Repository.GetById<Account>(message.AccountId);
            account.PayInvoice(message.InvoiceId, message.Amount);
            Save(account, message.Id);
        }

        
    }
}
