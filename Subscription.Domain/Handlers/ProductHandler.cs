﻿using System.Linq;
using NServiceBus;
using Subscription.Domain.Models.Product;
using Subscription.Messages.Commands.Product;

namespace Subscription.Domain.Handlers
{
    public class ProductCommandsHandler : HandlerBase,
        IHandleMessages<CreateProduct>,
        IHandleMessages<UpdateProduct>,
        IHandleMessages<ActivateProduct>,
        IHandleMessages<DiscontinueProduct>,

        //Features
        IHandleMessages<AddFeature>,
        IHandleMessages<UpdateFeature>,
        IHandleMessages<RemoveFeature>,
        IHandleMessages<ActivateFeature>,
        IHandleMessages<DeactivateFeature>,


        //Offers
        IHandleMessages<AddOffer>,
        IHandleMessages<UpdateOffer>,
        IHandleMessages<RemoveOffer>,
        IHandleMessages<LockOffer>,
        IHandleMessages<ActivateOffer>,
        IHandleMessages<DeactivateOffer>,
        IHandleMessages<UpdateOfferFeatures>
    {
        public void Handle(CreateProduct message)
        {
            var product = Product.CreateProduct(message.ProductId, message.Name, message.Code, message.Description, message.AccessUrl, message.BasePrice);
            Save(product, message.Id);
        }
        public void Handle(UpdateProduct message)
        {
            var product = Repository.GetById<Product>(message.ProductId);
            product.Update(message.Name, message.Code, message.Description, message.AccessUrl, message.BasePrice);
            Save(product, message.Id);
        }
        public void Handle(ActivateProduct message)
        {
            var product = Repository.GetById<Product>(message.ProductId);
            product.ActivateProduct();
            Save(product, message.Id);
        }
        public void Handle(DiscontinueProduct message)
        {
            var product = Repository.GetById<Product>(message.ProductId);
            product.DiscontinueProduct(message.Reason);
            Save(product, message.Id);
        }

        //Features
        public void Handle(AddFeature message)
        {
            var product = Repository.GetById<Product>(message.ProductId);
            product.AddFeature(message.Code, message.Name, message.Price, message.Limit);
            Save(product, message.Id);
        }
        public void Handle(UpdateFeature message)
        {
            var product = Repository.GetById<Product>(message.ProductId);
            product.UpdateFeature(message.Code, message.Name, message.Price, message.Limit);
            Save(product, message.Id);
        }
        public void Handle(RemoveFeature message)
        {
            var product = Repository.GetById<Product>(message.ProductId);
            product.RemoveFeature(message.Code, message.Reason);
            Save(product, message.Id);
        }
        public void Handle(ActivateFeature message)
        {
            var product = Repository.GetById<Product>(message.ProductId);
            product.ActivateFeature(message.FeatureCode);
            Save(product, message.Id);
        }
        public void Handle(DeactivateFeature message)
        {
            var product = Repository.GetById<Product>(message.ProductId);
            product.DeactivateFeature(message.FeatureCode, message.Reason);
            Save(product, message.Id);
        }

        //Offer
        public void Handle(AddOffer message)
        {
            var product = Repository.GetById<Product>(message.ProductId);
            product.AddOffer(message.OfferKey, message.Name);
            Save(product, message.Id);
        }
        public void Handle(UpdateOffer message)
        {
            var product = Repository.GetById<Product>(message.ProductId);
            product.UpdateOffer(message.OfferKey, message.Name);
            Save(product, message.Id);
        }
        public void Handle(RemoveOffer message)
        {
            var product = Repository.GetById<Product>(message.ProductId);
            product.RemoveOffer(message.OfferKey, message.Reason);
            Save(product, message.Id);
        }
        public void Handle(LockOffer message)
        {
            var product = Repository.GetById<Product>(message.ProductId);
            product.LockOffer(message.OfferKey);
            Save(product, message.Id);
        }
        public void Handle(ActivateOffer message)
        {
            var product = Repository.GetById<Product>(message.ProductId);
            product.ActivateOffer(message.OfferKey);
            Save(product, message.Id);
        }
        public void Handle(DeactivateOffer message)
        {
            var product = Repository.GetById<Product>(message.ProductId);
            product.DeactivateOffer(message.OfferKey, message.Reason);
            Save(product, message.Id);
        }

        public void Handle(UpdateOfferFeatures message)
        {
            var product = Repository.GetById<Product>(message.ProductId);

            product.UpdateOfferFeatures(message.OfferKey, message.Features.Select(x => new ProductFeature(x.Code, string.Empty, 0, x.Limit)).ToList());
            Save(product, message.Id);
        }
    }
}
