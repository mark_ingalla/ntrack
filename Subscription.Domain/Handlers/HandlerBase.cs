﻿using System;
using CommonDomain;
using EventStore.RavenDb;
using NServiceBus;
using StructureMap.Attributes;
using Subscription.Domain.Infrastructure;

namespace Subscription.Domain.Handlers
{
    public abstract class HandlerBase
    {
        public IBus Bus { get; set; }

        [SetterProperty]
        public IRepository Repository { get; set; }


        protected void Save(IAggregate aggregate, Guid commitId)
        {
            Repository.Save(aggregate, commitId, Bus);
        }
    }
}