﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PalletPlus.Website.Areas.Admin.Models.DataVisualization
{
    public class MovementChartModel
    {
        public MovementChartModel()
        {
        }

        public MovementChartModel(int week, double valueIn, double valueOut)
        {
            Week = week.ToString();
            ValueIN = valueIn;
            ValueOUT = valueOut;
        }

        public string Week { get; set; }

        public double ValueIN { get; set; } // movement count
        public double ValueOUT { get; set; }
    }
}