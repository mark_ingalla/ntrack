﻿

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using PalletPlus.Data;
using PalletPlus.Website.Models;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Company
{
    public class CompanySupplierEditViewModel
    {
        public Guid CompanyId { get; set; }
        public int OrderNo { get; set; }
        public bool IsDeleted { get; set; }

        [DisplayName("Supplier")]
        public Guid SupplierId { get; set; }

        [Required]
        [DisplayName("Supplier Account Number")]
        public string SupplierAccountNumber { get; set; }

        [Required]
        [DisplayName("Prefix")]
        public string Prefix { get; set; }

        [Required]
        [DisplayName("Suffix")]
        public string Suffix { get; set; }
     
        [DisplayName("Sequence Number")]
        public int SequenceStartNumber { get; set; }

        public CompanySupplierEditViewModel()
        {
        }
        public CompanySupplierEditViewModel(CompanySupplierViewModel vm, Guid companyId, int i)
        {
            CompanyId = companyId;
            OrderNo = i;
            IsDeleted = vm.IsDeleted;
            SupplierId = vm.SupplierId;
            SupplierAccountNumber = vm.AccountNumber;
            Prefix = vm.Prefix;
            Suffix = vm.Suffix;
            SequenceStartNumber = vm.SequenceStartNumber;
        }
    }
}