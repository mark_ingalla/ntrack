﻿

using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using PalletPlus.Data;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Company
{
    public class CompanySupplierViewModel
    {
        public bool IsDeleted { get; set; }

        [Required]
        [DisplayName("Supplier")]
        [Remote("IsValidSupplier", "Company", ErrorMessage = "Please choose supplier from the list of available items.")]
        public Guid SupplierId { get; set; }

        public string SupplierName { get; set; }

        [Required]
        [DisplayName("Supplier Account Number")]
        public string AccountNumber { get; set; }

        [Required]
        [DisplayName("Prefix")]
        public string Prefix { get; set; }

        [Required]
        [DisplayName("Suffix")]
        public string Suffix { get; set; }

        [DisplayName("Sequence Number")]
        public int SequenceStartNumber { get; set; }

        public CompanySupplierViewModel(CompanySupplier companySupplier, Guid id)
        {
            SupplierId = id;
            AccountNumber = companySupplier.AccountNumber;
            Prefix = companySupplier.Prefix;
            Suffix = companySupplier.Suffix;
            SequenceStartNumber = companySupplier.SequenceNumber;
        }

        public CompanySupplierViewModel()
        {
            
        }
    }
}