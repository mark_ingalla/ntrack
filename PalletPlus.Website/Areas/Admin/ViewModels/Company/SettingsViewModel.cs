﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Web.Mvc;
using PalletPlus.Data;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Company
{
    public class SettingsViewModel
    {
        [Required]
        [Remote("CorrectionReasonIsUnique", "Company", ErrorMessage = "Typed reason already exists.")]
        public string CorrectionReason { get; set; }

        [Required]
        [Remote("CancellationReasonIsUnique", "Company", ErrorMessage = "Typed reason already exists.")]
        public string CancellationReason { get; set; }

        public Dictionary<Guid, string> CorrectionReasonsVM { get; set; }
        public Dictionary<Guid, string> CancellationReasonsVM { get; set; }

       public Dictionary<Guid, DashboardEquipmentColor> EquipmentItemsColors { get; set; }
       
      

        public SettingsViewModel()
        {
            CorrectionReasonsVM = new Dictionary<Guid, string>();
            CancellationReasonsVM = new Dictionary<Guid, string>();

            EquipmentItemsColors = new Dictionary<Guid, DashboardEquipmentColor>();
        }
    }
}