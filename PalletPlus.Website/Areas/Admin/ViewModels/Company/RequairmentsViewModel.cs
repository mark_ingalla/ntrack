﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PalletPlus.Data;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Company
{
    public class RequirementViewModel
    {
        public bool Checked { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
    }

}