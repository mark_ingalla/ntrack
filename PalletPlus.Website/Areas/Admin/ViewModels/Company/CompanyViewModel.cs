﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using PalletPlus.Data;
using PalletPlus.Website.Areas.Admin.ViewModels.User;
using Raven.Client.Linq;
using nTrack.Data.Extenstions;
using nTrack.Data.Indices;
using nTrack.Extension;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Company
{
    public class CompanyViewModel
    {
        #region company details

        [ScaffoldColumn(false)]
        public Guid CompanyId { get; set; }

        public bool IsDeleted { get; set; }

        [DisplayName("Company name")]
        public string CompanyName { get; set; }

        [DisplayName("CompanyPhone")]
        public string CompanyPhone { get; set; }

        [DisplayName("Address 1")]
        public string AddressLine1 { get; set; }

        [DisplayName("Address 2")]
        public string AddressLine2 { get; set; }

        public string Suburb { get; set; }

        public string State { get; set; }

        public string Country { get; set; }

        [DisplayName("Postcode")]
        public string Postcode { get; set; }

        #endregion

        #region company suppliers

        public CompanySupplierViewModel SupplierVM { get; set; }
        public Dictionary<Guid, string> SupplierNames { get; set; }
        public IEnumerable<object> ComboboxSuppliers { get; set; }
        public IEnumerable<object> ComboboxEquipments { get; set; }
        //public KeyValuePair<Guid, DashboardEquipmentColor> EquipmentItemColor { get; set; }
        public DashboardEquipmentColor EquipmentItemColor { get; set; }

        #endregion

        #region company users

        public List<UserViewModel> UsersVM { get; set; }
        public List<CompanySupplierViewModel> SuppliersVM { get; set; }

        #endregion

        #region company settings

        public SettingsViewModel SettingsVM { get; set; }

        #endregion

        #region company export settings

        public bool ExportMovementIn { get; set; }
        public bool ExportMovementOut { get; set; }

        #endregion

        public List<RequirementViewModel> Requirements { get; set; }

        public CompanyViewModel()
        {
            UsersVM = new List<UserViewModel>();
            SupplierNames = new Dictionary<Guid, string>();
            SuppliersVM = new List<CompanySupplierViewModel>();
            SettingsVM = new SettingsViewModel();
            Requirements = new List<RequirementViewModel>();
        }

        public CompanyViewModel(Account account, List<Data.Supplier> suppliers, List<UserIndex.Result> userResults ) : this()
        {
            CompanyId = account.Id;
            IsDeleted = account.IsDeleted;
            CompanyName = account.CompanyName;
            foreach (var supplier in account.Suppliers)
            {
                SuppliersVM.Add(new CompanySupplierViewModel(supplier.Value, supplier.Key.ToGuidId()));
            }
            SettingsVM.CorrectionReasonsVM = account.CorrectionReasons;
            SettingsVM.CancellationReasonsVM = account.CancellationReasons;
            SettingsVM.EquipmentItemsColors = account.DashboardEquipmentColors
                .Where(x=> !string.IsNullOrWhiteSpace(x.Value.EquipmentCode) || !string.IsNullOrWhiteSpace(x.Value.EquipmentName) )
                .ToDictionary(x=>x.Key,y=>y.Value);
            ExportMovementIn = account.ExportMovementIn;
            ExportMovementOut = account.ExportMovementOut;

            Type type = typeof(FieldsForRequirements);

            var requirements = type.GetFields();

            foreach (var requirement in requirements)
            {
                Requirements.Add(new RequirementViewModel()
                {
                    Checked = account.RequiredFields.Any(p => p == (string)requirement.GetValue(null)),
                    Name = requirement.Name.ToSeparateWords(),
                    Key = (string)requirement.GetValue(null)
                });
            }

            var alreadyAdded = account.Suppliers.Select(x => x.Key).ToList();

            var comboboxSuppliers = suppliers
               .Where(x => !x.IsDeleted && !x.IdString.In(alreadyAdded))
               .Select(x => new { Id = x.IdString.ToGuidId(), SupplierName = x.CompanyName });

            ComboboxSuppliers = comboboxSuppliers;

            SupplierNames = suppliers.ToDictionary(x => x.IdString.ToGuidId(), y => y.CompanyName);

            UsersVM = userResults.Select(x => new UserViewModel(x.UserKey, x.Email, x.UserFullName, 
                x.UserPhone, x.UserMobile, x.Role, x.IsDeleted, x.IsLocked)).ToList();


            var alreadyAddedEquipment = account.DashboardEquipmentColors.Values.Where(x => !string.IsNullOrWhiteSpace(x.EquipmentCode) || !string.IsNullOrWhiteSpace(x.EquipmentName)).ToDictionary(x => x.EquipmentCode,
                                                                                             y => y.EquipmentName);

            var comboboxEquipments = from p in (suppliers.SelectMany(x => x.Equipments))
                                     where !p.Value.EquipmentCode.In(alreadyAddedEquipment.Keys)
                                     && !p.Value.EquipmentName.In(alreadyAddedEquipment.Values)
                                     group p by new { p.Value.EquipmentCode, p.Value.EquipmentName }
                                         into grp
                                         select new
                                         {
                                            Code =  grp.Key.EquipmentCode,
                                            Name =  grp.Key.EquipmentName
                                         };
            ComboboxEquipments = comboboxEquipments;
            EquipmentItemColor = new DashboardEquipmentColor
                                     {
                                         B = 255,
                                         R = 0,
                                         G = 0
                                     };
        }
    }
}