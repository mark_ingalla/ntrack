﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Wizard
{
    public class CorrectionReasonVM
    {
        [Required]
        [Remote("ReasonIsUnique", "Wizard", AdditionalFields = "Type", ErrorMessage = "Typed reason already exists.")]
        public string Reason { get; set; }

        public string Type { get; set; }

        public Dictionary<Guid, string> ReasonsVM { get; set; }

        public CorrectionReasonVM()
        {
            
        }

        public CorrectionReasonVM(string reason, Dictionary<Guid, string> reasonsVM, string type)
        {
            Reason = reason;
            ReasonsVM = reasonsVM;
            Type = type;
        }
    }
}