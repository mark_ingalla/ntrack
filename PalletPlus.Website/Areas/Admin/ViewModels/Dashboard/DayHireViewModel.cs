﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Dashboard
{
    public class DayHireViewModel
    {
        public string EquipmentCode { get; set; }
        public string EquipmentName { get; set; }
        public DateTime MovementDate { get; set; }
        public int ClosingBalance { get; set; }
        
        
        
        public int IncomingQty { get; set; }
        public int PreviousDayBalance { get; set; }
        public int DayHire
        {
            get { return IncomingQty + PreviousDayBalance; }
        }

        public string MovementDateString
        {
            get { return MovementDate.ToString("d/M/yy"); }

        }
       // public string MovementDateString { get; set; }


        public DayHireViewModel(string equipCode, string equipName, DateTime dayHire, int incomingQty,int closingBalance)
        {
            EquipmentCode = equipCode;
            EquipmentName = equipName;

            MovementDate = dayHire;
         //   MovementDateString = datestring;
            IncomingQty = incomingQty;
            ClosingBalance = closingBalance;


        }
    }
}