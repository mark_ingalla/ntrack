﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Reconciliation
{
    public class PartnerViewModel
    {
        [Required]
        [DisplayName("Partner name")]
        [Remote("CheckPartnerNameUniquiness", "Reconciliation", ErrorMessage = "This name is already in use.")]
        public string CompanyName { get; set; }

        public string SiteId { get; set; }
        public string SiteName { get; set;}

        public string SupplierId { get; set; }
        public string SupplierName { get; set; }
        public string AccountNumber { get; set; }
        public string EquipmentCode { get; set; }


        public Guid InvoiceId { get; set; }
        public Guid InvoiceDetailKey { get; set; }
    }
}