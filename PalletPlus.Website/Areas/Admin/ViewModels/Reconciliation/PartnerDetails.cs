﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Reconciliation
{
    public class PartnerDetails
    {
        public string CompanyId { get; set; }
        public string Name { get; set; }
        public string AccountNumber { get; set; }

        public PartnerDetails(string companyId, string name, string accountNumber)
        {
            CompanyId = companyId;
            Name = name;
            AccountNumber = accountNumber;
        }
    }
}
