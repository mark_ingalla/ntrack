﻿
using System;
using System.Collections.Generic;
using System.Linq;
using nTrack.Data.Extenstions;
using PalletPlus.Data;


namespace PalletPlus.Website.Areas.Admin.ViewModels.Reconciliation
{
    public class ReconciliationViewModel
    {
        public Guid InvoiceId { get; set; }
        public Guid SupplierId { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public List<ReconciliationLineModel> Details { get; set; }
        public List<ReconciliationLineModel> UnreconciledDetails { get; set; }

        public ReconciliationLineModel InvoiceDetailVM { get; set; }



        public ReconciliationViewModel()
        {
            Details = new List<ReconciliationLineModel>();
            UnreconciledDetails = new List<ReconciliationLineModel>();
        }

        public ReconciliationViewModel(SupplierInvoice invoice, Dictionary<string, string> destinations)
        {
            InvoiceId = invoice.Id;
            SupplierId = invoice.SupplierId.ToGuidId();
            InvoiceDate = invoice.InvoiceDate;
            InvoiceNumber = invoice.InvoiceNumber;

            Details = invoice.Details
                .Select(x => new ReconciliationLineModel
                {
                    Key = x.Key,
                    TransactionCode = x.TransactionCode,
                    DocketNumber = x.DocketNumber,
                    EquipmentCode = x.EquipmentCode,
                    Qty = x.Qty,
                    DaysHire = x.DaysHire,
                    AccountNumber = x.AccountNumber,
                    AccountRef = x.AccountRef,
                    DestinationName = DestinationName(x.DestinationAccountNumber, destinations),
                    DestinationAccountNumber = x.DestinationAccountNumber,
                    DestinationRef = x.DestinationRef,
                    SupplierRef = x.SupplierRef,
                    ConsignmentRef = x.ConsignmentRef,
                    MovementDate = x.MovementDate,
                    EffectiveDate = x.EffectiveDate,
                    DehireDate = x.DehireDate,
                    IsReconciled = x.IsReconciled
                }).ToList();

            UnreconciledDetails = Details.Where(x => !x.IsReconciled).ToList();
        }

        string DestinationName(string accountNumber, Dictionary<string, string> destinations)
        {
            string company;
            destinations.TryGetValue(accountNumber, out company);
            return company;
        }
    }

    public class ReconciliationLineModel
    {
        public Guid Key { get; set; }
        public string TransactionCode { get; set; }
        public string DocketNumber { get; set; }
        public string EquipmentCode { get; set; }
        public int Qty { get; set; }
        public int DaysHire { get; set; }
        public string AccountNumber { get; set; }
        public string AccountRef { get; set; }
        public string DestinationName { get; set; }
        public string DestinationAccountNumber { get; set; }
        public string DestinationRef { get; set; }
        public string SupplierRef { get; set; }
        public string ConsignmentRef { get; set; }
        public DateTime MovementDate { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime DehireDate { get; set; }
        public bool IsReconciled { get; set; }
    }
}