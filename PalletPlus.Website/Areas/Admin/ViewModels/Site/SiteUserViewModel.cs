﻿

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Site
{
    public class SiteUserViewModel
    {
        public bool IsDeleted { get; set; }

        [Required]
        [DisplayName("User")]
        [Remote("IsValidUser", "Site", ErrorMessage = "Please choose user from the list of available items.")]
        public string UserId { get; set; }

        public string FullName { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public SiteUserViewModel()
        {
        }
    }
}