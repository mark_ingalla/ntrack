﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using PalletPlus.Data;
using PalletPlus.Website.Models;
using Raven.Client.Linq;
using nTrack.Data.Extenstions;
using nTrack.Data.Indices;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Site
{
    public class SiteEditViewModel
    {
        #region site details

        [ScaffoldColumn(false)]
        public Guid SiteId { get; set; }

        [Required]
        [DisplayName("Site name")]
        public string SiteName { get; set; }

        [Required]
        [DisplayName("Address")]
        public string AddressLine1 { get; set; }

        [DisplayName("Address")]
        public string AddressLine2 { get; set; }

        [Required]
        public string Suburb { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        [DisplayName("Postcode")]
        [RegularExpression(@"^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$", ErrorMessage = "Please enter a valid post code.")]
        public string Postcode { get; set; }

        [Required]
        public string Country { get; set; }

        //PTA
        public bool IsPTAEnabled { get; set; }
        public string Suffix { get; set; }
        public int PTASequenceNumber { get; set; }

        #endregion 

        public SiteSupplierViewModel SupplierVM { get; set; }
        public SitePartnerViewModel PartnerVM { get; set; }
        public SiteUserViewModel UserVM { get; set; }

        public List<SiteSupplierViewModel> SuppliersVM { get; set; }
        public Dictionary<Guid, string> SupplierNames { get; set; }
        public List<SitePartnerViewModel> PartnersVM { get; set; }
        public List<SiteUserViewModel> UsersVM { get; set; }

        public IEnumerable<object> ComboboxSuppliers { get; set; }
        public IEnumerable<object> ComboboxPartners { get; set; }
        public IEnumerable<object> ComboboxUsers { get; set; }
        public List<SelectListItem> ComboboxCountries { get; set; }

        public SiteEditViewModel()
        {
            SupplierVM = new SiteSupplierViewModel();
            PartnerVM = new SitePartnerViewModel();
            UserVM = new SiteUserViewModel();

            SuppliersVM = new List<SiteSupplierViewModel>();
            PartnersVM = new List<SitePartnerViewModel>();
            UsersVM = new List<SiteUserViewModel>();
            SupplierNames = new Dictionary<Guid, string>();
        }

        public SiteEditViewModel(Data.Site site, List<Data.Supplier> suppliers,
            Dictionary<string, Data.CompanySupplier> suppliersFromAccount,
            List<Data.Partner> partners,
            List<UserIndex.Result> users, List<SelectListItem> comboboxCountries)
            : this()
        {
            SiteId = site.Id;
            SiteName = site.CompanyName;
            SuppliersVM = site.Suppliers.Values
                        .Select(x => new SiteSupplierViewModel(x.CompanyId.ToGuidId(), x.CompanyName,
                            x.AccountNumber, x.Prefix, x.Suffix, x.SequenceNumber, false)).ToList();
            AddressLine1 = site.AddressLine1;
            AddressLine2 = site.AddressLine2;
            Suburb = site.Suburb;
            State = site.State;
            Postcode = site.Postcode;
            Country = site.Country;
            IsPTAEnabled = site.IsPTAEnabled;
            Suffix = site.PTASuffix;
            PTASequenceNumber = site.PTASequenceNumber;

            var alreadyAdded = site.Suppliers.Select(x => x.Key).ToList();
            var comboboxSuppliers = suppliers.Select(x => new { IsDeleted = x.IsDeleted, SupplierId = x.IdString, SupplierName = x.CompanyName })
               .Where(x => !x.IsDeleted && !x.SupplierId.In(alreadyAdded))
               .Select(x => new { Id = x.SupplierId.ToGuidId(), SupplierName = x.SupplierName }).ToList();

            ComboboxSuppliers = comboboxSuppliers;

            var siteSuppliersIds = SuppliersVM.Select(x => x.SupplierId).ToList();
            var ss = suppliersFromAccount.ToDictionary(p => p.Key.ToGuidId(), p => p.Value);

            var suppliersInheritedFromAccount = ss.Where(x => !x.Key.In(siteSuppliersIds)).ToList();
            SuppliersVM.AddRange(suppliersInheritedFromAccount.Select(x => new SiteSupplierViewModel(x.Value.CompanyId.ToGuidId(),
                        x.Value.CompanyName, x.Value.AccountNumber, x.Value.Prefix, x.Value.Suffix, x.Value.SequenceNumber, true)));

            //SuppliersVM.AddRange(suppliersFromAccount.Select(x => new SiteSupplierViewModel(x.CompanyId.ToGuidId(),
            //            x.CompanyName, x.AccountNumber, x.Prefix, x.Suffix, x.SequenceNumber, true)));

            SupplierNames = suppliers.Select(x => new { IsDeleted = x.IsDeleted, SupplierId = x.IdString, SupplierName = x.CompanyName }).ToDictionary(x => x.SupplierId.ToGuidId(), y => y.SupplierName);


            var par = partners.Select(x => new SitePartnerViewModel
                                 {
                                     IsDeleted = x.IsDeleted,
                                     PartnerId = x.Id,
                                     PartnerName = x.CompanyName,
                                     Suburb = x.Suburb,
                                     State = x.State,
                                     ContactEmail = x.ContactEmail
                                 }).ToList();

            var comboboxPartners = partners
               .Select(x => new { Id = x.IdString.ToGuidId(), PartnerName = x.CompanyName });


            ComboboxPartners = comboboxPartners;
            PartnersVM = par;

            var alreadyAdded2 = site.Users.ToList();

            var siteUsers = users
                           .Where(x => x.UserKey.In(alreadyAdded2))
                           .Select(x => new SiteUserViewModel()
                           {
                               UserId = x.UserKey.ToString(),
                               FullName = x.UserFullName,
                               Email = x.Email,
                               Mobile = string.Empty,
                               Phone = string.Empty
                           }).ToList();

            var comboboxUsers = users
                .Where(x => !x.UserKey.In(alreadyAdded2))
                .Select(x => new { Id = x.UserKey, FullName = x.UserFullName });


            ComboboxUsers = comboboxUsers;
            UsersVM = siteUsers;
            ComboboxCountries = comboboxCountries;
        }
    }
}