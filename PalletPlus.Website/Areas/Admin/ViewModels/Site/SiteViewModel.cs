﻿

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Site
{
    public class SiteViewModel
    {
        [ScaffoldColumn(false)]
        public Guid SiteId { get; set; }

        public bool IsDeleted { get; set; }

        [Required]
        [DisplayName("Site name")]
        public string SiteName { get; set; }

        [Required]
        [DisplayName("Address")]
        public string AddressLine1 { get; set; }

        [DisplayName("Address")]
        public string AddressLine2 { get; set; }

        [Required]
        public string Suburb { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        [DisplayName("Postcode")]
        [RegularExpression(@"^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$", ErrorMessage = "Please enter a valid post code.")]
        public string Postcode { get; set; }

        [Required]
        public string Country { get; set; }

        public List<SelectListItem> ComboboxCountries { get; set; }

        //PTA
        public bool IsPTAEnabled { get; set; }
        public string Suffix { get; set; }
        public int PTASequenceNumber { get; set; }

        public SiteViewModel()
        {
            ComboboxCountries = new List<SelectListItem>();
        }

        public SiteViewModel(Data.Site site)
            : this()
        {
            SiteId = site.Id;
            IsDeleted = site.IsDeleted;
            SiteName = site.CompanyName;
            AddressLine1 = site.AddressLine1;
            AddressLine2 = site.AddressLine2;
            Suburb = site.Suburb;
            State = site.State;
            Postcode = site.Postcode;
            Country = site.Country;
            IsPTAEnabled = site.IsPTAEnabled;
            Suffix = site.PTASuffix;
            PTASequenceNumber = site.PTASequenceNumber;
        }
    }
}