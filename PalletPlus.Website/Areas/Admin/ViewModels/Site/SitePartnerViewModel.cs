﻿

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using PalletPlus.Data;
using PalletPlus.Website.Models;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Site
{
    public class SitePartnerViewModel
    {
        public bool IsDeleted { get; set; }

        [Required]
        [DisplayName("Partner")]
        [Remote("IsValidPartner", "Site", ErrorMessage = "Please choose partner from the list of available items.")]
        public Guid PartnerId { get; set; }

        [DisplayName("Partner Account Number")]
        public string PartnerAccountNo { get; set; }

        public string PartnerName { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string ContactEmail { get; set; }
    }
}