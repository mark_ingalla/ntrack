﻿

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using PalletPlus.Data;
using PalletPlus.Website.Models;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Site
{
    public class SiteSupplierViewModel
    {
        public bool IsDeleted { get; set; }
        public bool IsCompany { get; set; }

        [Required]
        [DisplayName("Supplier")]
        [Remote("IsValidSupplier", "Site", ErrorMessage = "Please choose supplier from the list of available items.")]
        public Guid SupplierId { get; set; }

        public string SupplierName { get; set; }

        [Required]
        [DisplayName("Supplier Account Number")]
        public string AccountNumber { get; set; }

        [Required]
        [DisplayName("Suffix")]
        public string Suffix { get; set; }

        [Required]
        [DisplayName("Prefix")]
        public string Prefix { get; set; }
        
        [DisplayName("Sequence Start Number")]
        public int SequenceStartNumber { get; set; }

        public SiteSupplierViewModel()
        {
        }

        public SiteSupplierViewModel(Guid id, string supplierName, string accountNo, string prefix, string suffix, int seqNo, bool isCompany)
        {
            SupplierId = id;
            SupplierName = supplierName;
            AccountNumber = accountNo;
            Prefix = prefix;
            Suffix = suffix;
            SequenceStartNumber = seqNo;
            IsCompany = isCompany;
        }
    }
}