﻿

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using PalletPlus.Data;
using PalletPlus.Website.Models;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Site
{
    public class SiteSupplierEditViewModel
    {
        public Guid SiteId { get; set; }
        public int OrderNo { get; set; }
        public bool IsDeleted { get; set; }

        [DisplayName("Supplier")]
        public Guid SupplierId { get; set; }

        public string SupplierName { get; set; }

        [Required]
        [DisplayName("Supplier Account Number")]
        public string SupplierAccountNumber { get; set; }

        [Required]
        [DisplayName("Suffix")]
        public string Suffix { get; set; }

        [Required]
        [DisplayName("Prefix")]
        public string Prefix { get; set; }
        
        [DisplayName("Sequence Start Number")]
        public int SequenceStartNumber { get; set; }

        public SiteSupplierEditViewModel()
        {
        }
        public SiteSupplierEditViewModel(SiteSupplierViewModel vm, Guid siteId, int i)
        {
            SiteId = siteId;
            IsDeleted = vm.IsDeleted;
            SupplierId = vm.SupplierId;
            SupplierName = vm.SupplierName;
            SupplierAccountNumber = vm.AccountNumber;
            Suffix = vm.Suffix;
            Prefix = vm.Prefix;
            SequenceStartNumber = vm.SequenceStartNumber;
            OrderNo = i;
        }
    }
}