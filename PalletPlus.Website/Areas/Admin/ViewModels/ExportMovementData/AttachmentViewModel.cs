﻿using System;
using nTrack.Data.Extenstions;

namespace PalletPlus.Website.Areas.Admin.ViewModels.ExportMovementData
{
    public class AttachmentViewModel
    {
        public Guid Id { get { return AttachmentId.ToGuidId(); } }
        public string AttachmentId { get; set; }
        public DateTime MovementDate { get; set; }
        public DateTime SentOn { get; set; }
        public string SupplierName { get; set; }
        public string SiteName { get; set; }
        public string FileName { get; set; }
        public int TotalDownloads { get; set; }
        public int TotalMovements { get; set; }
    }
}