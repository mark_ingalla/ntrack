﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PalletPlus.Website.Areas.Admin.ViewModels.ExportMovementData
{
    public class ExportMovementDataViewModel
    {
        [Required]
        [DisplayName("Supplier")]
        public Guid SupplierId { get; set; }

        public Dictionary<Guid, string> Suppliers { get; set; }

        public IEnumerable<AttachmentViewModel> Attachments { get; set; }
    }
}