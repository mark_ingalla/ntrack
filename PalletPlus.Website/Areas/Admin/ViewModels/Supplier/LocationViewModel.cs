﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PalletPlus.Data;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Supplier
{
    public class LocationViewModel
    {
        public Guid SupplierId { get; set; }
        public Guid LocationKey { get; set; }
        public int OrderNo { get; set; }

        [Required]
        [DisplayName("Location name")]
        public string LocationName { get; set; }

        [DisplayName("Account number")]
        public string AccountNumber { get; set; }

        [Required]
        [DisplayName("Address")]
        public string AddressLine1 { get; set; }

        [DisplayName("Address")]
        public string AddressLine2 { get; set; }

        [Required]
        public string Suburb { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        [DisplayName("Postcode")]
        [RegularExpression(@"^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$", ErrorMessage = "Please enter a valid post code.")]
        public string Postcode { get; set; }

        [Required]
        public string Country { get; set; }

        [DisplayName("Contact name")]
        public string ContactName { get; set; }

        [DisplayName("Contact email")]
        public string ContactEmail { get; set; }

        public List<SelectListItem> ComboboxCountries { get; set; }

        public LocationViewModel(){}

        public LocationViewModel(SupplierLocation location, Guid supplierId, List<SelectListItem> comboboxCountries, int i)
        {
            SupplierId = supplierId;
            LocationKey = location.LocationKey;
            LocationName = location.CompanyName;
            AccountNumber = location.AccountNumber;
            AddressLine1 = location.AddressLine1;
            AddressLine2 = location.AddressLine2;
            Suburb = location.Suburb;
            State = location.State;
            Postcode = location.Postcode;
            Country = location.Country;
            ContactName = location.ContactName;
            ContactEmail = location.ContactEmail;

            ComboboxCountries = comboboxCountries;

            OrderNo = i;
        }
    }
}