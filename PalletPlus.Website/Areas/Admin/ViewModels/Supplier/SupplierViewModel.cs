﻿

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Supplier
{
    public class SupplierViewModel
    {
        [ScaffoldColumn(false)]
        public Guid SupplierId { get; set; }

        public bool IsDeleted { get; set; }

        [Required]
        [DisplayName("Account number")]
        public string AccountNumber { get; set; }

        [Required]
        [DisplayName("Location name")]
        public string LocationName { get; set; }

        [Required]
        [DisplayName("Supplier name")]
        public string SupplierName { get; set; }

        [Required]
        [DisplayName("Address")]
        public string AddressLine1 { get; set; }

        [DisplayName("Address")]
        public string AddressLine2 { get; set; }

        [Required]
        public string Suburb { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        [DisplayName("Postcode")]
        [RegularExpression(@"^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$", ErrorMessage = "Please enter a valid post code.")]
        public string Postcode { get; set; }

        [Required]
        public string Country { get; set; }

        [DisplayName("Contact name")]
        public string ContactName { get; set; }

        [Required]
        [DisplayName("Contact email")]
        [DataType(DataType.EmailAddress)]
        public string ContactEmail { get; set; }

        [DisplayName("Provider Name")]
        public string ProviderName { get; set; }

        public bool IsPTAEnabled { get; set; }

        public Dictionary<string, string> ExportProviderNames { get; set; }
        public List<SelectListItem> ComboboxCountries { get; set; }

        public SupplierViewModel()
        {
            ExportProviderNames = new Dictionary<string, string>();
            ExportProviderNames.Add("No providers", "No providers");
            
            ComboboxCountries = new List<SelectListItem>();
        }

        public SupplierViewModel(Data.Supplier supplier)
        {
            SupplierId = supplier.Id;
            SupplierName = supplier.CompanyName;
            ContactEmail = supplier.ContactEmail;
            ProviderName = supplier.ProviderName;
            IsPTAEnabled = supplier.IsPTAEnabled;
            IsDeleted = supplier.IsDeleted;
            
        }
    }
}