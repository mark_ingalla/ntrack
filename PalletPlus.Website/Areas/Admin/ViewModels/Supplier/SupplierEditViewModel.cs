﻿

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using PalletPlus.Data;
using System.Linq;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Supplier
{
    public class SupplierEditViewModel
    {
        #region supplier details

        [ScaffoldColumn(false)]
        public Guid SupplierId { get; set; }

        [Required]
        [DisplayName("Supplier name")]
        public string SupplierName { get; set; }

        [Required]
        [DisplayName("Contact email")]
        [DataType(DataType.EmailAddress)]
        public string ContactEmail { get; set; }

        [DisplayName("Provider Name")]
        public string ProviderName { get; set; }

        public bool IsPTAEnabled { get; set; }

        #endregion

        #region collections

        //locations
        public LocationViewModel LocationVM { get; set; }
        public Dictionary<Guid, SupplierLocation> Locations { get; set; }

        //equipment
        public EquipmentViewModel EquipmentVM { get; set; }
        public Dictionary<string, string> ProviderNames { get; set; }
        public List<Equipment> Equipment { get; set; }
        public List<SelectListItem> ComboboxCountries { get; set; }

        #endregion

        public SupplierEditViewModel()
        {
            Locations = new Dictionary<Guid, SupplierLocation>();
            ComboboxCountries = new List<SelectListItem>();

            Equipment = new List<Equipment>();
            ProviderNames = new Dictionary<string, string>();
            ProviderNames.Add("No providers", "No providers");
        }

        public SupplierEditViewModel(Data.Supplier supplier, List<SelectListItem> countries, Dictionary<string, string> providerNames)
            : this()
        {
            SupplierId = supplier.Id;
            SupplierName = supplier.CompanyName;
            ContactEmail = supplier.ContactEmail;
            ProviderName = supplier.ProviderName;
            IsPTAEnabled = supplier.IsPTAEnabled;
            ComboboxCountries = countries;
            foreach (var pn in providerNames)
            {
                ProviderNames.Add(pn.Key, pn.Value);
            }

            Equipment = supplier.Equipments.Values.Select(x => new Equipment
                                                                   {
                                                                       EquipmentCode = x.EquipmentCode,
                                                                       EquipmentName = x.EquipmentName,
                                                                       IsDeleted = x.IsDeleted
                                                                   }).ToList();

            Locations = supplier.Locations;


        }

        public void AddEquipment(EquipmentViewModel item)
        {
            Equipment.Add(new Equipment { EquipmentCode = item.EquipmentCode, EquipmentName = item.EquipmentName });
        }
    }
}