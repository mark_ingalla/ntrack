﻿

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using PalletPlus.Data;
using PalletPlus.Website.Models;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Supplier
{
    public class EquipmentViewModel
    {
        [Required]
        [DisplayName("Equipment code")]
        public string EquipmentCode { get; set; }

        [Required]
        [DisplayName("Equipment name")]
        public string EquipmentName { get; set; }
    }
}