﻿

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using PalletPlus.Data;
using PalletPlus.Website.Models;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Partner
{
    public class PartnerSiteViewModel
    {
        public bool IsDeleted { get; set; }

        [Required]
        [DisplayName("Site")]
        [Remote("IsValidSite", "Partner", ErrorMessage = "Please choose site from the list of available items.")]
        public Guid SiteId { get; set; }

        [DisplayName("Site Account Number")]
        public string SiteAccountNo { get; set; }

        public string SiteName { get; set; }

        public string Suburb { get; set; }

        public string State { get; set; }

        public string ContactEmail { get; set; }
    }
}