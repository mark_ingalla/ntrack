﻿
namespace PalletPlus.Website.Areas.Admin.ViewModels.Partner
{
    public class PartnerSupplierInfo
    {
        public string AccountNumber { get; set; }

        public PartnerSupplierInfo(string accountNumber)
        {
            AccountNumber = accountNumber;
        }
    }
}