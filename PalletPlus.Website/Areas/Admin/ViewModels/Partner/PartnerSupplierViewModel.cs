﻿

using System.Web.Mvc;
using PalletPlus.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Partner
{
    public class PartnerSupplierViewModel
    {
        public bool IsDeleted { get; set; }

        public Guid PartnerId { get; set; }

        [Required]
        [DisplayName("Supplier")]
        [Remote("IsValidSupplier", "Partner", ErrorMessage = "Please choose supplier from the list of available items.")]
        public Guid SupplierId { get; set; }

        public string SupplierName { get; set; }

        [DisplayName("Supplier Account Number")]
        public string SupplierAccountNo { get; set; }

        public string Suburb { get; set; }

        public string State { get; set; }

        public string ContactEmail { get; set; }


        [DisplayName("Enable Exchange")]
        public bool EnableExchange { get; set; }

        public Dictionary<string, string> Equipment { get; set; }
        public HashSet<string> EquipmentAvailable { get; set; }
    }
}