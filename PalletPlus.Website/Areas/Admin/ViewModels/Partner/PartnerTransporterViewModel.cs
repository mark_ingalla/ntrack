﻿

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using PalletPlus.Data;
using PalletPlus.Website.Models;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Partner
{
    public class PartnerTransporterViewModel
    {
        public bool IsDeleted { get; set; }

        [DisplayName("Transporter")]
        public Guid TransporterId { get; set; }

        [DisplayName("Transporter Account Number")]
        public string TransporterAccountNo { get; set; }

        public string TransporterName { get; set; }

        public string Suburb { get; set; }

        public string State { get; set; }

        public string ContactEmail { get; set; }
    }
}