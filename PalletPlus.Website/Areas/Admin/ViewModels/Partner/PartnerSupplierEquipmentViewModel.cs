﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PalletPlus.Data;
using Raven.Client.Linq;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Partner
{
    public class PartnerSupplierEquipmentViewModel
    {
        public Guid SupplierId { get; set; }
        public Guid PartnerId { get; set; }

        public Dictionary<string, string> EquipmentNames { get; set; } 
        public Dictionary<string, bool> CheckedNodes { get; set; } 

        public PartnerSupplierEquipmentViewModel()
        {
            
        }

        public PartnerSupplierEquipmentViewModel(Guid partnerId, Guid supplierId, Dictionary<string, string> equipment, HashSet<string> equipmentAvailable)
        {
            SupplierId = supplierId;
            PartnerId = partnerId;
            CheckedNodes = equipment.ToDictionary(x => x.Key, y => y.Key.In(equipmentAvailable));
            EquipmentNames = equipment.ToDictionary(x => x.Key, y => y.Value);
        }
    }
}