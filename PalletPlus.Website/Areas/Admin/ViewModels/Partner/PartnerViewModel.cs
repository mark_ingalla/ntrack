﻿

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using PalletPlus.Website.Models;
using Subscription.Messages.Commands;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Partner
{
    public class PartnerViewModel
    {
        public bool IsDeleted { get; set; }

        [ScaffoldColumn(false)]
        public Guid PartnerId { get; set; }

        [Required]
        [DisplayName("Partner name")]
        public string CompanyName { get; set; }

        [Required]
        [DisplayName("Address")]
        public string AddressLine1 { get; set; }

        [DisplayName("Address")]
        public string AddressLine2 { get; set; }

        [Required]
        public string Suburb { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        [DisplayName("Postcode")]
        [RegularExpression(@"^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$", ErrorMessage = "Please enter a valid post code.")]
        public string PostCode { get; set; }

        [Required]
        public string Country { get; set; }

        [DisplayName("Contact name")]
        public string ContactName { get; set; }

        [DisplayName("Contact email")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(ValidationPatterns.EMAIL, ErrorMessage = "Email is invalid.")]
        public string ContactEmail { get; set; }

        public bool IsPTAEnabled { get; set; }

        public List<string> Tags { get; set; }

        public List<SelectListItem> ComboboxCountries { get; set; }

        public PartnerViewModel()
        {}

        public PartnerViewModel(Data.Partner partner)
        {
            PartnerId = partner.Id;
            CompanyName = partner.CompanyName;
            IsDeleted = partner.IsDeleted;
            AddressLine1 = partner.AddressLine1;
            AddressLine2 = partner.AddressLine2;
            Suburb = partner.Suburb;
            State = partner.State;
            PostCode = partner.Postcode;
            Country = partner.Country;
            ContactName = partner.ContactName;
            ContactEmail = partner.ContactEmail;
            IsPTAEnabled = partner.IsPTAEnabled;
            Tags = partner.Tags;
        }
    }
}