﻿

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using PalletPlus.Data;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Partner
{
    public class PartnerEditViewModel
    {
        #region partner details

        [ScaffoldColumn(false)]
        public Guid PartnerId { get; set; }

        [Required]
        [DisplayName("Partner name")]
        public string PartnerName { get; set; }

        [Required]
        [DisplayName("Address")]
        public string AddressLine1 { get; set; }

        [DisplayName("Address")]
        public string AddressLine2 { get; set; }

        [Required]
        public string Suburb { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        [DisplayName("Postcode")]
        [RegularExpression(@"^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$", ErrorMessage = "Please enter a valid post code.")]
        public string Postcode { get; set; }

        [Required]
        public string Country { get; set; }

        [DisplayName("Contact name")]
        public string ContactName { get; set; }

        [DisplayName("Contact email")]
        [DataType(DataType.EmailAddress)]
        public string ContactEmail { get; set; }

        public bool IsPTAEnabled { get; set; }
        public List<string> Tags { get; set; }

        #endregion

        public PartnerSupplierViewModel SupplierVM { get; set; }
        public PartnerSiteViewModel SiteVM { get; set; }
        public PartnerTransporterViewModel TransporterVM { get; set; }

        public List<PartnerSupplierViewModel> SuppliersVM { get; set; }
        public List<PartnerSiteViewModel> SitesVM { get; set; }
        public List<PartnerTransporterViewModel> TransportersVM { get; set; }
        public Dictionary<Guid,PartnerDocument> DocumentsVM { get; set; }

        public IEnumerable<object> ComboboxSuppliers { get; set; }
        public IEnumerable<object> ComboboxSites { get; set; }
        public IEnumerable<object> ComboboxTransporters { get; set; }
        public List<SelectListItem> ComboboxCountries { get; set; }

        public PartnerEditViewModel()
        {
            SupplierVM = new PartnerSupplierViewModel();
            SiteVM = new PartnerSiteViewModel();
            TransporterVM = new PartnerTransporterViewModel();

            SuppliersVM = new List<PartnerSupplierViewModel>();
            SitesVM = new List<PartnerSiteViewModel>();
            TransportersVM = new List<PartnerTransporterViewModel>();
            Tags = new List<string>();
        }

        public PartnerEditViewModel(Data.Partner partner) : this()
        {
            PartnerId = partner.Id;
            PartnerName = partner.CompanyName;
            AddressLine1 = partner.AddressLine1;
            AddressLine2 = partner.AddressLine2;
            Suburb = partner.Suburb;
            State = partner.State;
            Postcode = partner.Postcode;
            Country = partner.Country;
            ContactEmail = partner.ContactEmail;
            ContactName = partner.ContactName;
            IsPTAEnabled = partner.IsPTAEnabled;
            Tags = partner.Tags;
        }
    }
}