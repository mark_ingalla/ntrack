﻿

using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using PalletPlus.Website.Models;

namespace PalletPlus.Website.Areas.Admin.ViewModels.Transporter
{
    public class TransporterViewModel
    {
        [ScaffoldColumn(false)]
        public Guid TransporterId { get; set; }

        public bool IsDeleted { get; set; }

        [Required]
        [DisplayName("Transporter name")]
        public string TransporterName { get; set; }

        [Required]
        [DisplayName("Address name")]
        public string AddressLine1 { get; set; }

        [DisplayName("Address")]
        public string AddressLine2 { get; set; }

        [Required]
        public string Suburb { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        [DisplayName("Postcode")]
        [RegularExpression(@"^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$", ErrorMessage = "Please enter a valid post code.")]
        public string Postcode { get; set; }

        [DisplayName("Contact name")]
        public string ContactName { get; set; }

        [Required]
        [DisplayName("Contact email")]
        [DataType(DataType.EmailAddress)]
        public string ContactEmail { get; set; }
    }
}