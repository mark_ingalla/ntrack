﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PalletPlus.Website.Areas.Admin.ViewModels.User
{
    public class UserEditViewModel
    {
        public Guid UserKey { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        public string Phone { get; set; }

        public string Mobile { get; set; }

        public string Role { get; set; }

        public bool IsDeleted { get; set; }
        public bool IsLocked { get; set; }

        public SelectList Roles { get; set; }

        public UserEditViewModel(){}

        public UserEditViewModel(string[] roles)
        {
            var list = new List<SelectListItem>();
            foreach (var role in roles)
            {
                list.Add(new SelectListItem()
                {
                    Text = role,
                    Value = role
                });
            }

            Roles = new SelectList(roles);
        }
    }
}