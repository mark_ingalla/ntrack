﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PalletPlus.Website.Areas.Admin.ViewModels.User
{
    public class UserViewModel
    {
        [ScaffoldColumn(false)]
        public Guid UserKey { get; set; }

        public string Email { get; set; }

        public string FullName { get; set; }

        public string Phone { get; set; }

        public string Mobile { get; set; }

        public string Role { get; set; }

        public bool IsDeleted { get; set; }
        public bool IsLocked { get; set; }

        public UserViewModel(Guid userKey, string email, string fullName, string phone, string mobile, string role, bool isDeleted, bool isLocked)
        {
            UserKey = userKey;
            Email = email;
            FullName = fullName;
            Phone = phone;
            Mobile = mobile;
            Role = role;
            IsDeleted = isDeleted;
            IsLocked = isLocked;
        }

        public UserViewModel()
        {}
    }
}