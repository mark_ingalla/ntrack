﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PalletPlus.Website.Areas.Admin.ViewModels.CorrectionRequests
{
    public class CorrectionsViewModel
    {
        public IEnumerable<CorrectionRequestViewModel> PendingCorrections { get; set; }
        public IEnumerable<CorrectionRequestViewModel> HistoricalCorrections { get; set; } 
    }
}