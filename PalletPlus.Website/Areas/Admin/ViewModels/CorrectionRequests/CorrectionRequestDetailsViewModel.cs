﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PalletPlus.Website.Areas.Admin.ViewModels.CorrectionRequests
{
    public class EquipmentQuantityInfo
    {
        public string EquipmentName { get; set; }
        public int OldQuantity { get; set; }
        public int NewQuantity { get; set; }

        public EquipmentQuantityInfo(string eqName, int oldQnt, int newQnt)
        {
            EquipmentName = eqName;
            OldQuantity = oldQnt;
            NewQuantity = newQnt;
        }
    }

    public class CorrectionRequestDetailsViewModel
    {
        public DateTime RequestedOn { get; set; }
        public DateTime? ClosedOn { get; set; }

        public string RequestedBy { get; set; }
        public string ClosedBy { get; set; }

        public string DocketNumber { get; set; }
        public string PartnerKind { get; set; }
        public string PartnerName { get; set; }

        public Guid CorrectionKey { get; set; }//movement or stocktake
        public Guid StocktakeMovementId { get; set; }
        public CorrectionType CorrectionType { get; set; }

        public Dictionary<string, EquipmentQuantityInfo> Equipment { get; set; }

        public DateTime MovementDate { get; set; }
        public DateTime CorrectedDate { get; set; }

        public DateTime EffectiveDate { get; set; }
        public DateTime CorrectedEffectiveDate { get; set; }

        public string Status { get; set; }

        public string Reason { get; set; }
    }
}