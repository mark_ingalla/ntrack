﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PalletPlus.Website.Areas.Admin.ViewModels.CorrectionRequests
{
    public enum CorrectionType
    {
        Unknown,
        Stocktake,
        Movement
    }

    public enum CorrectionKind
    {
        Unknown,
        Correction,
        Cancellation
    }

    public class CorrectionRequestViewModel
    {
        private DateTime _date { get; set; }
        private DateTime _closedOn { get; set; }

        public string Date { get { return _date.ToShortDateString(); } }
        public string DocketNumber { get; set; }
        public Guid SiteId { get; set; }
        public string SiteName { get; set; }
        public string User { get; set; }
        public string Summary { get; set; }
        public string ClosedOn { get { return _closedOn.ToShortDateString(); } }
        public string ClosedBy { get; set; }

        public CorrectionKind CorrectionKind
        {
            get
            {
                var summary = Summary.Split(' ');
                switch (summary[1])
                {
                    case "Correction":
                        return CorrectionKind.Correction;
                    case "Cancellation":
                        return CorrectionKind.Cancellation;
                }
                if (summary[0] == "Stocktake") return CorrectionKind.Correction;
                return CorrectionKind.Unknown;
            }
        }

        public CorrectionType CorrectionType
        {
            get
            {
                switch (Summary.Split(' ')[0])
                {
                    case "Stocktake":
                        return CorrectionType.Stocktake;
                    case "Movement":
                        return CorrectionType.Movement;
                }
                return CorrectionType.Unknown;
            }
        }

        public string Status { get; set; }
        public Guid StocktakeId { get; set; }
        public Guid CorrectionKey { get; set; }//movement or stocktake

        public CorrectionRequestViewModel(){}

        public CorrectionRequestViewModel(DateTime date, Guid movementStocktakeId, Guid correctionKey, Guid siteId, 
            string siteName, string user, string summary, string docketNumber, bool? isApproved)
        {
            _date = date;
            StocktakeId = movementStocktakeId;
            CorrectionKey = correctionKey;
            SiteId = siteId;
            SiteName = siteName;
            User = user;
            Summary = summary;
            DocketNumber = docketNumber;

            switch(isApproved)
            {
                case null:
                    Status = "PendingCorrection";
                    break;
                case true:
                    Status = "Approved";
                    break;
                case false:
                    Status = "Declined";
                    break;
            }
        }

        public CorrectionRequestViewModel(DateTime date, Guid movementStocktakeId, Guid correctionKey, Guid siteId, string siteName, 
            string user, string summary, string docketNumber, bool? isApproved, DateTime closedOn, string closedBy)
        {
            _date = date;
            StocktakeId = movementStocktakeId;
            CorrectionKey = correctionKey;
            SiteId = siteId;
            SiteName = siteName;
            User = user;
            Summary = summary;
            _closedOn = closedOn;
            ClosedBy = closedBy;
            DocketNumber = docketNumber;

            switch (isApproved)
            {
                case null:
                    Status = "PendingCorrection";
                    break;
                case true:
                    Status = "Approved";
                    break;
                case false:
                    Status = "Declined";
                    break;
            }
        }
    }
}