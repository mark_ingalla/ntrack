﻿
using System;
using System.Collections.Generic;
using nTrack.Data.Extenstions;
using PalletPlus.Data;


namespace PalletPlus.Website.Areas.Admin.ViewModels.ImportSupplierInvoiceData
{
    public class InvoiceDetailsViewModel
    {
        public string InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }

        public Guid InvoiceId { get; set; }
        public Guid SupplierId { get; set; }
        public List<SupplierInvoice.InvoiceDetail> Details { get; set; }

        public InvoiceDetailsViewModel()
        {
            Details = new List<SupplierInvoice.InvoiceDetail>();
        }

        public InvoiceDetailsViewModel(SupplierInvoice invoice)
        {
            InvoiceId = invoice.Id;
            SupplierId = invoice.SupplierId.ToGuidId();
            InvoiceNumber = invoice.InvoiceNumber;
            InvoiceDate = invoice.InvoiceDate;
            Details = invoice.Details;
        }
    }
}