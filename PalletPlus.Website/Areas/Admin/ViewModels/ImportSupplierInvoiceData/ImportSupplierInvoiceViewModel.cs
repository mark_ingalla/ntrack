﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PalletPlus.Website.Areas.Admin.ViewModels.ImportSupplierInvoiceData
{
    public class ImportSupplierInvoiceViewModel
    {
        [Required]
        [DisplayName("Supplier:")]
        public Guid SupplierId { get; set; }

        public Dictionary<Guid, string> Suppliers { get; set; }

        public ImportedInvoiceVM[] PreviousInvoices { get; set; }
    }
}