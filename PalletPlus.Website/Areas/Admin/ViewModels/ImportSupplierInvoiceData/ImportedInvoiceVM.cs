﻿using System;
using nTrack.Data.Extenstions;

namespace PalletPlus.Website.Areas.Admin.ViewModels.ImportSupplierInvoiceData
{
    public class ImportedInvoiceVM
    {
        public Guid Id { get { return IdString.ToGuidId(); } }
        public string IdString { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string InvoiceNumber { get; set; }
        public string SupplierId { get; set; }
        public string SupplierName { get; set; }

        public int TotalItems { get; set; }
        public bool IsReconciled { get; set; }

    }
}