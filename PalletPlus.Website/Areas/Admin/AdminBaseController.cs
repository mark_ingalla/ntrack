﻿using System;
using System.Collections.Generic;
using System.Linq;
using AttributeRouting;
using AttributeRouting.Web.Mvc;
using nTrack.Data;
using nTrack.Extension;
using nTrack.Web.Bootstrap;
using PalletPlus.Website.Infrastructure;
using PalletPlus.Website.Services;
using System.Web.Mvc;
using System.Web.Routing;

namespace PalletPlus.Website.Areas.Admin
{
    [RouteArea("Admin")]
    [Authorize(Roles = "Admin")]
    public abstract class AdminBaseController : BaseController
    {
        public AdminBaseController()
        {
            
        }

        public IThreadingService ThreadingService { get; set; }

        protected AdminBaseController(IThreadingService threadingService)
        {
            ThreadingService = threadingService;
        }

        protected RouteValueDictionary SetRouteValues(bool showDeleted)
        {
            var routeValues = new RouteValueDictionary();
            routeValues.Add("showDeleted", showDeleted);

            return routeValues;
        }

        protected void SetTabValue(string tabRouteValue)
        {
            TempData["tab-value"] = tabRouteValue;
        }

        protected void Success()
        {
            TempData["alert-type"] = AlertType.Success;
            TempData["alert-message"] = "Your request finished with success.";
        }
        protected void Info(string message)
        {
            TempData["alert-type"] = AlertType.Info;
            TempData["alert-message"] = message;
        }
        protected void Failure(string message)
        {
            TempData["alert-type"] = AlertType.Error;
            TempData["alert-message"] = message;
        }
        protected void RecordCreated(string tabRouteValue)
        {
            TempData["alert-type"] = AlertType.Success;
            TempData["alert-message"] = "Your record has been created.";
            TempData["tab-value"] = tabRouteValue;
        }
        protected void RecordUpdated(string tabRouteValue)
        {
            TempData["alert-type"] = AlertType.Success;
            TempData["alert-message"] = "Your record has been updated.";
            TempData["tab-value"] = tabRouteValue;
        }
        protected void Failure(string tabRouteValue, string message)
        {
            TempData["alert-type"] = AlertType.Error;
            TempData["alert-message"] = message;
            TempData["tab-value"] = tabRouteValue;
        }

        protected List<SelectListItem> GetCountries()
        {
            var ret = new List<SelectListItem>();
            ret.Add(new SelectListItem{Text = "", Value = ""});

            using (var session = MvcApplication.SystemStore.OpenSession())
            {
                var countries = session.Load<Countries>(Countries.EntityId);
                if (countries != null)
                {
                    ret.AddRange(countries.CountryNames.Select(x => new SelectListItem {Text = x, Value = x}).ToList());
                }
            }
            return ret;
        }
    }
}