﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using nTrack.Data.Extenstions;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Commands;
using PalletPlus.Website.Areas.Admin.ViewModels.ExportMovementData;
using PalletPlus.Website.Services;
using Raven.Client;

namespace PalletPlus.Website.Areas.Admin.Controllers
{

    public class ExportMovementController : AdminBaseController
    {
        readonly IDocumentSession _session;
        readonly IAttachmentService _attachmentService;


        public ExportMovementController(IDocumentSession session, IThreadingService threadingService, IAttachmentService attachmentService)
            : base(threadingService)
        {
            _session = session;
            _attachmentService = attachmentService;
        }

        [GET("ExportMovements")]
        public ActionResult Index()
        {
            var vm = new ExportMovementDataViewModel();

            //get all suppliers
            vm.Suppliers = _session.Advanced.LoadStartingWith<Supplier>(DocHelpers.CollectionName<Supplier>())
                .ToDictionary(x => x.Id, y => y.CompanyName);

            //get last 25 files
            vm.Attachments = _session.Query<ExportAttachment, ExportAttachmentIndex>()
                                     .Take(25)
                                     .OrderByDescending(x => x.MovementDate)
                                     .As<AttachmentViewModel>()
                                     .ToList();


            return View(vm);
        }

        [POST("ExportMovements")]
        public ActionResult ExportMovements(ExportMovementDataViewModel model)
        {
            var sites = _session.Advanced.LoadStartingWith<Site>(DocHelpers.CollectionName<Site>()).Select(x => x.Id);

            foreach (var siteId in sites)
            {
                Bus.Send<ExportMovements>(cmd =>
                    {
                        cmd.SiteId = siteId;
                        cmd.SupplierId = model.SupplierId;
                    });
            }

            return RedirectToAction("Index");
        }

        [GET("Download/{id}")]
        public ActionResult DownloadFile(Guid id)
        {
            var attRef = _session.Load<ExportAttachment>(id);
            attRef.TotalDownloads++;
            _session.SaveChanges();

            var att = _attachmentService.GetAttachment(attRef.IdString);

            return File(att.Data(), "text/csv", attRef.FileName);
        }

        [GET("Send/{id}")]
        public ActionResult SendFile(Guid id)
        {
            return new HttpStatusCodeResult(HttpStatusCode.Accepted);
        }
    }
}