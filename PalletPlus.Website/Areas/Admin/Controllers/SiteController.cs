﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using nTrack.Data.Extenstions;
using nTrack.Data.Indices;
using nTrack.Extension;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Commands.Site;
using PalletPlus.Website.Areas.Admin.ViewModels.Site;
using PalletPlus.Website.Services;
using Raven.Client;
using Raven.Client.Linq;
using Account = PalletPlus.Data.Account;

namespace PalletPlus.Website.Areas.Admin.Controllers
{

    public class SiteController : AdminBaseController
    {
        readonly IDocumentSession _session;

        public SiteController(IDocumentSession session, IThreadingService threadingService)
            : base(threadingService)
        {
            _session = session;
        }

        [GET("Sites")]
        public ActionResult Index(bool showDeleted = false)
        {
            ViewBag.ShowDeleted = showDeleted;
            return View(GetSites(showDeleted));
        }

        [GET("Site/Create")]
        public ActionResult Create()
        {
            var vm = new SiteViewModel();

            vm.ComboboxCountries = GetCountries();
            return View(vm);
        }

        [POST("Site/Create")]
        public ActionResult Create(SiteViewModel site)
        {
            //TODO: write a static index
            var siteEntity = _session.Query<Site>().FirstOrDefault(x => x.CompanyName == site.SiteName);
            if (siteEntity != null)
            {
                Failure("A site with that name already exists.");
                site.ComboboxCountries = GetCountries();
                site.SiteName = string.Empty;
                return View(site);
            }

            site.SiteId = Guid.NewGuid();
            Bus.Send<CreateSite>(cmd =>
                {
                    cmd.SiteId = site.SiteId;
                    cmd.Name = site.SiteName;
                    cmd.AddressLine1 = site.AddressLine1;
                    cmd.AddressLine2 = site.AddressLine2;
                    cmd.Suburb = site.Suburb;
                    cmd.State = site.State;
                    cmd.Postcode = site.Postcode;
                    cmd.Country = site.Country;
                });

            if (site.IsPTAEnabled)
            {
                Bus.Send(new UpdatePTAInfo
                {
                    IsEnabled = site.IsPTAEnabled,
                    SequenceNumber = site.PTASequenceNumber,
                    SiteId = site.SiteId,
                    Suffix = site.Suffix
                });
            }

            ThreadingService.ObjectCreated<Site>(site.SiteId);
            RecordCreated("details");

            return RedirectToAction("Edit", new { id = site.SiteId });
        }

        [GET("Site/{id}")]
        public ActionResult Edit(Guid id)
        {
            var site = _session.Load<Site>(id);
            var vm = new SiteEditViewModel();

            if (site != null)
            {
                if (Request.QueryString.Get("Grid-filter") != null)
                    SetTabValue("partners");

                var suppliers = _session.Query<Supplier>(typeof(SupplierIndex).Name);

                var account = _session.Load<Account>(Identity.AccountId);

                var sitePartners = _session.Query<Partner>(typeof(PartnerIndex).Name)
                    .Where(x => !x.IsDeleted && x.Sites.Any(y => y == site.IdString));

                var users = new List<UserIndex.Result>();
                var countries = GetCountries();

                using (var session = MvcApplication.SystemStore.OpenSession())
                {
                    users = session.Query<UserIndex.Result, UserIndex>()
                        .Where(x => x.AccountId == Identity.AccountId.ToStringId<Account>())
                        .ToList();
                }

                vm = new SiteEditViewModel(site, suppliers.ToList(), account.Suppliers, sitePartners.ToList(), users, countries);
                //    vm = Mapper.Map<SiteEditViewModel>(site);
                //GetSuppliers(ref vm, site);
                //GetPartners(ref vm, site);
                //GetUsers(ref vm, site);
                //  vm.ComboboxCountries = GetCountries();

                return View(vm);
            }

            Failure("Couldn't load entity.");
            return View(vm);
        }

        [POST("Site/{id}")]
        public ActionResult Edit(SiteEditViewModel site)
        {
            var siteEntity = _session.Query<Site>()
                .FirstOrDefault(x => x.CompanyName == site.SiteName && site.SiteId != x.Id);

            var sitee = _session.Load<Site>(site.SiteId);

            if (sitee.IsPTAEnabled != site.IsPTAEnabled || sitee.PTASequenceNumber != site.PTASequenceNumber || sitee.PTASuffix != site.Suffix)
            {
                if (site.IsPTAEnabled)
                {
                    Bus.Send(new UpdatePTAInfo()
                                 {
                                     IsEnabled = site.IsPTAEnabled,
                                     SequenceNumber = site.PTASequenceNumber,
                                     SiteId = site.SiteId,
                                     Suffix = site.Suffix
                                 });
                }
                else
                {
                    Bus.Send(new UpdatePTAInfo()
                    {
                        IsEnabled = site.IsPTAEnabled,
                        SequenceNumber = sitee.PTASequenceNumber,
                        SiteId = sitee.Id,
                        Suffix = sitee.PTASuffix
                    });
                }
            }

            if (siteEntity != null)
            {
                Failure("A site with that name already exists.");
                return RedirectToAction("Edit", new { id = site.SiteId });
            }

            Bus.Send<UpdateSite>(cmd =>
            {
                cmd.SiteId = site.SiteId;
                cmd.Name = site.SiteName;
                cmd.AddressLine1 = site.AddressLine1;
                cmd.AddressLine2 = site.AddressLine2;
                cmd.Suburb = site.Suburb;
                cmd.State = site.State;
                cmd.Postcode = site.Postcode;
                cmd.Country = site.Country;
            });

            //ThreadingService.ObjectUpdated<Site>(site.SiteId, UtcNow);

            RecordUpdated("details");

            return RedirectToAction("Edit", new { id = site.SiteId });
        }

        [GET("Site/Delete/{id}")]
        public ActionResult Delete(Guid id, bool showDeleted)
        {
            Bus.Send<DeleteSite>(cmd => { cmd.SiteId = id; });

            ThreadingService.ObjectDeleted<Site>(id);

            return RedirectToAction("Index", SetRouteValues(showDeleted));
        }

        [GET("Site/{id}/Undelete")]
        public ActionResult Undelete(Guid id, bool showDeleted)
        {
            Bus.Send<UndeleteSite>(cmd => cmd.SiteId = id);

            ThreadingService.ObjectUndeleted<Site>(id);

            return RedirectToAction("Index", SetRouteValues(showDeleted));
        }

        [POST("Site/{id}/AddSupplier")]
        public ActionResult AddSupplier(SiteEditViewModel site)
        {
            if (site.SupplierVM.SupplierId.IsEmpty())
            {
                Failure("suppliers", "Please choose supplier from the list of available items.");
                return RedirectToAction("Edit", new { id = site.SiteId });
            }

            if (!ValidateAccountNumberUniquiness(site.SiteId, site.SupplierVM.AccountNumber))
                return RedirectToAction("Edit", new { id = site.SiteId });

            site.SupplierVM.SupplierName = _session.Load<Supplier>(site.SupplierVM.SupplierId).CompanyName;

            Bus.Send<AddSupplierToSite>(cmd =>
                {
                    cmd.SiteId = site.SiteId;
                    cmd.SupplierId = site.SupplierVM.SupplierId;
                    cmd.SupplierName = site.SupplierVM.SupplierName;
                    cmd.SupplierAccountNumber = site.SupplierVM.AccountNumber;
                    cmd.Prefix = site.SupplierVM.Prefix;
                    cmd.Suffix = site.SupplierVM.Suffix;
                    cmd.SequenceNumber = site.SupplierVM.SequenceStartNumber;
                });

            ThreadingService.ObjectUpdated<Site>(site.SiteId, DateTime.UtcNow);
            RecordUpdated("suppliers");

            return RedirectToAction("Edit", new { id = site.SiteId });
        }

        [POST("Site/{id}/EditSupplier")]
        public ActionResult EditSupplier(SiteSupplierEditViewModel site)
        {
            Bus.Send<UpdateSiteSupplier>(cmd =>
                {
                    cmd.SiteId = site.SiteId;
                    cmd.SupplierId = site.SupplierId;
                    cmd.Prefix = site.Prefix;
                    cmd.Suffix = site.Suffix;
                    cmd.SequenceStartNumber = site.SequenceStartNumber;
                });

            ThreadingService.ObjectUpdated<Site>(site.SiteId, DateTime.UtcNow);
            RecordUpdated("suppliers");

            return RedirectToAction("Edit", new { id = site.SiteId });
        }

        [POST("Site/{id}/OverrideSupplier")]
        public ActionResult OverrideSupplier(SiteSupplierEditViewModel site)
        {
            if (!ValidateAccountNumberUniquiness(site.SiteId, site.SupplierAccountNumber))
                return RedirectToAction("Edit", new { id = site.SiteId });

            //var now = Bus.SendMap<AddSupplierToSite>(site);

            Bus.Send<AddSupplierToSite>(cmd =>
                {
                    cmd.SiteId = site.SiteId;
                    cmd.SupplierId = site.SupplierId;
                    cmd.SupplierName = site.SupplierName;
                    cmd.SupplierAccountNumber = site.SupplierAccountNumber;
                    cmd.Prefix = site.Prefix;
                    cmd.Suffix = site.Suffix;
                    cmd.SequenceNumber = site.SequenceStartNumber;
                });

            ThreadingService.ObjectUpdated<Site>(site.SiteId, DateTime.UtcNow);
            RecordUpdated("suppliers");

            return RedirectToAction("Edit", new { id = site.SiteId });
        }

        [GET("Site/{id}/DeleteSupplier/{supplierId}")]
        public ActionResult DeleteSupplier(Guid id, Guid supplierId)
        {
            Bus.Send<RemoveSupplierFromSite>(
                cmd =>
                {
                    cmd.SiteId = id;
                    cmd.SupplierId = supplierId;
                });

            ThreadingService.ObjectUpdated<Site>(id, DateTime.UtcNow);
            RecordUpdated("suppliers");

            return RedirectToAction("Edit", new { id });
        }

        [POST("Site/{id}/EnablePartner")]
        public ActionResult EnablePartner(SiteEditViewModel site)
        {
            if (site.PartnerVM.PartnerId.IsEmpty())
            {
                Failure("partners", "Please choose supplier from the list of available items.");
                return RedirectToAction("Edit", new { id = site.SiteId });
            }

            Bus.Send<AddPartnerToSite>(
                cmd =>
                {
                    cmd.SiteId = site.SiteId;
                    cmd.PartnerId = site.PartnerVM.PartnerId;
                });

            ThreadingService.ObjectUpdated<Site>(site.SiteId, DateTime.UtcNow);
            RecordUpdated("partners");

            return RedirectToAction("Edit", new { id = site.SiteId });
        }

        [GET("Site/{id}/DisablePartner/{partnerId}")]
        public ActionResult DisablePartner(Guid id, Guid partnerId)
        {
            Bus.Send<RemovePartnerFromSite>(
                cmd =>
                {
                    cmd.SiteId = id;
                    cmd.PartnerId = partnerId;
                });

            ThreadingService.ObjectUpdated<Site>(id, DateTime.UtcNow);
            RecordUpdated("partners");

            return RedirectToAction("Edit", new { id });
        }

        [POST("Site/{id}/EnableUser")]
        public ActionResult EnableUser(SiteEditViewModel site)
        {
            var account = _session.Load<Account>(Identity.AccountId);
            if (account != null)
            {
                var userKeyGuid = Guid.Parse(site.UserVM.UserId);

                using (var session = MvcApplication.SystemStore.OpenSession())
                {
                    var userResult = session.Query<UserIndex.Result, UserIndex>()
                        .FirstOrDefault(x => x.AccountId == Identity.AccountId.ToStringId<Account>() && x.UserKey == userKeyGuid);

                    if (userResult != null)
                    {
                        Bus.Send<AddUserToSite>(
                            cmd =>
                            {
                                cmd.SiteId = site.SiteId;
                                cmd.UserId = Guid.Parse(site.UserVM.UserId);
                                cmd.Email = userResult.Email;
                                cmd.FullName = userResult.UserFullName;
                            });

                        ThreadingService.ObjectUpdated<Site>(site.SiteId, DateTime.UtcNow);
                        RecordUpdated("users");

                        return RedirectToAction("Edit", new { id = site.SiteId });
                    }
                }
            }

            Failure("Failed to enable user.");
            return RedirectToAction("Edit", new { id = site.SiteId });
        }

        [GET("Site/{id}/DisableUser/{userId}")]
        public ActionResult DisableUser(Guid id, Guid userId)
        {
            Bus.Send<RemoveUserFromSite>(
                cmd =>
                {
                    cmd.SiteId = id;
                    cmd.UserId = userId;
                });

            ThreadingService.ObjectUpdated<Site>(id, DateTime.UtcNow);
            RecordUpdated("users");

            return RedirectToAction("Edit", new { id });
        }

        [GET("Site/{id}/IsValidSupplier")]
        public ActionResult IsValidSupplier(SiteSupplierViewModel supplierVM)
        {
            bool res;

            if (supplierVM.SupplierId.IsEmpty())
                res = false;
            else
            {
                res = _session.Query<Supplier>(typeof(SupplierIndex).Name)
                    .Any(x => x.Id == supplierVM.SupplierId);
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [GET("Site/{id}/IsValidPartner")]
        public ActionResult IsValidPartner(SitePartnerViewModel partnerVM)
        {
            bool res;

            if (partnerVM.PartnerId.IsEmpty())
                res = false;
            else
            {
                res = _session.Query<Partner>(typeof(PartnerIndex).Name)
                    .Any(x => x.Id == partnerVM.PartnerId);
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [GET("Site/{id}/IsValidUser")]
        public ActionResult IsValidUser(SiteUserViewModel userVM)
        {
            bool res = true;
            Guid g;
            var parsed = Guid.TryParse(userVM.UserId, out g);

            if (!parsed)
                res = false;

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        void GetSuppliers(ref SiteEditViewModel viewModel, Site site)
        {
            var alreadyAdded = site.Suppliers.Select(x => x.Key).ToList();

            var suppliers = _session.Query<Supplier>(typeof(SupplierIndex).Name)
                .Select(x => new { IsDeleted = x.IsDeleted, SupplierId = x.IdString, SupplierName = x.CompanyName })
                .ToList();

            var comboboxSuppliers = suppliers
                .Where(x => !x.IsDeleted && !x.SupplierId.In(alreadyAdded))
                .Select(x => new { Id = x.SupplierId.ToGuidId(), SupplierName = x.SupplierName }).ToList();

            var account = _session.Load<Account>(Identity.AccountId);
            if (account != null)
            {
                var accountSuppliers = account.Suppliers.Values.Select(x => new SiteSupplierViewModel(x.CompanyId.ToGuidId(),
                    x.CompanyName, x.AccountNumber, x.Prefix, x.Suffix, x.SequenceNumber, true));

                var siteSuppliersIds = viewModel.SuppliersVM.Select(x => x.SupplierId).ToList();
                var suppliersInheritedFromAccount = accountSuppliers.Where(x => !x.SupplierId.In(siteSuppliersIds)).ToList();
                viewModel.SuppliersVM.AddRange(suppliersInheritedFromAccount);
            }

            viewModel.ComboboxSuppliers = comboboxSuppliers;
            viewModel.SupplierNames = suppliers.ToDictionary(x => x.SupplierId.ToGuidId(), y => y.SupplierName);
        }
        void GetPartners(ref SiteEditViewModel viewModel, Site site)
        {
            var partners = _session.Query<Partner>(typeof(PartnerIndex).Name).ToList();

            var sitePartners = partners
                .Where(x => !x.IsDeleted && x.Sites.Any(y => y == site.IdString))
                .Select(x => new SitePartnerViewModel
                                 {
                                     IsDeleted = x.IsDeleted,
                                     PartnerId = x.Id,
                                     PartnerName = x.CompanyName,
                                     Suburb = x.Suburb,
                                     State = x.State,
                                     ContactEmail = x.ContactEmail
                                 }).ToList();

            var comboboxPartners = partners
                .Where(x => !x.IsDeleted && x.Sites.All(y => y != site.IdString))
                .Select(x => new { Id = x.IdString.ToGuidId(), PartnerName = x.CompanyName });

            viewModel.PartnersVM = sitePartners;
            viewModel.ComboboxPartners = comboboxPartners;
        }
        void GetUsers(ref SiteEditViewModel viewModel, Site site)
        {
            var alreadyAdded = site.Users.ToList();

            using (var session = MvcApplication.SystemStore.OpenSession())
            {
                var users = session.Query<UserIndex.Result, UserIndex>()
                    .Where(x => x.AccountId == Identity.AccountId.ToStringId<Account>())
                    .ToList();

                var siteUsers = users
                    .Where(x => x.UserKey.In(alreadyAdded))
                    .Select(x => new SiteUserViewModel()
                                     {
                                         UserId = x.UserKey.ToString(),
                                         FullName = x.UserFullName,
                                         Email = x.Email,
                                         Mobile = string.Empty,
                                         Phone = string.Empty
                                     }).ToList();

                var comboboxUsers = users
                    .Where(x => !x.UserKey.In(alreadyAdded))
                    .Select(x => new { Id = x.UserKey, FullName = x.UserFullName });

                viewModel.UsersVM = siteUsers;
                viewModel.ComboboxUsers = comboboxUsers;
            }
        }
        IEnumerable<SiteViewModel> GetSites(bool showDeleted = false)
        {

            var sites = _session.Advanced.LoadStartingWith<Site>("sites/");

            if (!showDeleted)
                sites = sites.Where(x => !x.IsDeleted).ToArray();

            return sites.Select(x => new SiteViewModel(x));

            //return Mapper.Map<IEnumerable<SiteIndex.Result>, IEnumerable<SiteViewModel>>(sites.OrderBy(x => x.SiteName));
        }
        bool ValidateAccountNumberUniquiness(Guid siteId, string accountNumber)
        {
            var accountNumbers = _session.Query<AccountNumbersIndex.Result, AccountNumbersIndex>();
            var accoutnNumber = accountNumbers.FirstOrDefault(x => x.AccountNumber == accountNumber);
            if (accoutnNumber != null)
            {
                Failure("suppliers", "Account number already in use.");
                return false;
            }
            return true;
        }
    }
}