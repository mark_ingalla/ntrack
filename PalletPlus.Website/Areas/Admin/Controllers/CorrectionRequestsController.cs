﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using nTrack.Data.Extenstions;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Commands.Movement;
using PalletPlus.Messages.Commands.Stocktake;
using PalletPlus.Website.Areas.Admin.ViewModels.CorrectionRequests;
using Raven.Client;

namespace PalletPlus.Website.Areas.Admin.Controllers
{
    public class CorrectionRequestsController : AdminBaseController
    {
        readonly IDocumentSession _session;

        public CorrectionRequestsController(IDocumentSession session)
            : base(null)
        {
            _session = session;
        }

        [GET("CorrectionRequests")]
        public ActionResult Index()
        {
            #region set tab value
            bool historicalFilter = false;
            bool pendingFilter = false;

            if (Request.QueryString.Get("Historical-filter") != null)
            {
                historicalFilter = true;
                SetTabValue("historic");
            }
            if (Request.QueryString.Get("PendingCorrection-filter") != null)
            {
                pendingFilter = true;
                SetTabValue("pending");
            }

            if (historicalFilter && pendingFilter)
            {
                if (Request.QueryString.AllKeys[0] == "PendingCorrection-sort")
                    SetTabValue("historic");
                else
                    SetTabValue("pending");
            }
            #endregion
            var vm = new CorrectionsViewModel
                         {
                             PendingCorrections = GetRequests(),
                             HistoricalCorrections = GetHistoricalRequests()
                         };

            return View(vm);
        }

        [GET("CorrectionRequestDetails/{id}/{key}")]
        public ActionResult CorrectionRequestDetails(Guid id, Guid key, CorrectionType type) //correction key
        {
            CorrectionRequestDetailsViewModel vm = null;

            switch (type)
            {
                case CorrectionType.Stocktake:
                    vm = SetStocktakeDetailsPage(id, key, type);
                    break;
                default:
                    vm = SetMovementDetailsPage(id, key, type);
                    break;
            }

            if (vm != null)
                return View(vm);

            return HttpNotFound();
        }

        [GET("CancellationRequestDetails/{id}/{key}")]
        public ActionResult CancellationRequestDetails(Guid id, Guid key, CorrectionType type) //correction key
        {
            return View(SetMovementCancellationDetailsPage(id, key, type));
        }

        [GET("HistoricalRequestDetails/{id}/{key}")]
        public ActionResult HistoricalRequestDetails(Guid id, Guid key, CorrectionType type) //correction key
        {
            CorrectionRequestDetailsViewModel vm = null;

            switch (type)
            {
                case CorrectionType.Stocktake:
                    vm = SetHistoricalStocktakeDetailsPage(id, key, type);
                    break;
                default:
                    vm = SetHistoricalMovementDetailsPage(id, key, type);
                    break;
            }

            if (vm != null)
                return View(vm);

            return HttpNotFound();
        }

        [GET("HistoricalCancellationRequestDetails/{id}/{key}")]
        public ActionResult HistoricalCancellationRequestDetails(Guid id, Guid key, CorrectionType type) //correction key
        {
            return View(SetHistoricalMovementCancellationDetailsPage(id, key, type));
        }

        [POST("ProcessCorrection/{id}/{key}")]
        public ActionResult ProcessCorrection(Guid StocktakeMovementId, Guid CorrectionKey, CorrectionType CorrectionType, string submit)
        {
            switch (CorrectionType)
            {
                case CorrectionType.Stocktake:
                    {
                        SendStocktakeCorrectionRequest(submit, CorrectionKey, StocktakeMovementId);
                        break;
                    }
                default:
                    {
                        SendMovementCorrectionRequest(submit, CorrectionKey, StocktakeMovementId);
                        break;
                    }
            }

            return RedirectToAction("Index");
        }

        [POST("ProcessCancellation/{id}/{key}")]
        public ActionResult ProcessCancellation(Guid StocktakeMovementId, Guid CorrectionKey, CorrectionType CorrectionType, string submit)
        {
            SendMovementCancellationRequest(submit, CorrectionKey, StocktakeMovementId);

            return RedirectToAction("Index");
        }

        private IEnumerable<CorrectionRequestViewModel> GetRequests()
        {
            var ret = new List<CorrectionRequestViewModel>();

            var requests = _session.Query<CorrectionRequestIndex.Result, CorrectionRequestIndex>()
                .OrderByDescending(x => x.UpdatedOn).ToList();

            foreach (var request in requests)
            {
                ret.Add(new CorrectionRequestViewModel(
                            request.RequestedOn,
                            request.MovementStocktakeId.ToGuidId(), //stocktake or movement id
                            request.CorrectionKey,
                            request.SiteId.ToGuidId(),
                            request.SiteName,
                            request.RequestedBy.Value,
                            request.Name,
                            request.DocketNumber,
                            request.IsApproved
                            ));
            }

            return ret;
        }

        private IEnumerable<CorrectionRequestViewModel> GetHistoricalRequests()
        {
            var ret = new List<CorrectionRequestViewModel>();

            //TODO: consolidate this index
            var requests = _session.Query<HistoricalCorrectionRequestIndex.Result, HistoricalCorrectionRequestIndex>()
                .OrderByDescending(x => x.UpdatedOn)
                .ToList();

            foreach (var request in requests)
            {
                ret.Add(new CorrectionRequestViewModel(
                            request.RequestedOn,
                            request.MovementStocktakeId.ToGuidId(), //stocktake or movement id
                            request.CorrectionKey,
                            request.SiteId.ToGuidId(),
                            request.SiteName,
                            request.RequestedBy.Value,
                            request.Name,
                            request.DocketNumber,
                            request.IsApproved,
                            request.ClosedOn,
                            request.ClosedBy.Value
                            ));
            }

            return ret;
        }

        private CorrectionRequestDetailsViewModel SetStocktakeDetailsPage(Guid id, Guid key, CorrectionType type)
        {
            var stocktake = _session.Load<Stocktake>(id);
            if (stocktake != null)
            {
                var equipmentToCorrect = stocktake.EquipmentCounts
                    .FirstOrDefault(y => y.Value.PendingCorrection != null && y.Value.PendingCorrection.CorrectionKey == key);

                var equipmentQuantityInfo = new Dictionary<string, EquipmentQuantityInfo>
                                            {
                                                {
                                                    equipmentToCorrect.Key,
                                                    new EquipmentQuantityInfo(equipmentToCorrect.Value.EquipmentName,
                                                                          equipmentToCorrect.Value.PendingCorrection.OldQuantity,
                                                                          equipmentToCorrect.Value.PendingCorrection.NewQuantity)
                                                }
                                            };

                return new CorrectionRequestDetailsViewModel
                           {
                               StocktakeMovementId = id,
                               CorrectionKey = key,
                               RequestedBy = equipmentToCorrect.Value.PendingCorrection.RequestedBy.Value,
                               RequestedOn = equipmentToCorrect.Value.PendingCorrection.RequestedOn,
                               Reason = equipmentToCorrect.Value.PendingCorrection.Reason,
                               Equipment = equipmentQuantityInfo,
                               CorrectionType = type
                           };
            }
            return null;
        }

        private CorrectionRequestDetailsViewModel SetHistoricalStocktakeDetailsPage(Guid id, Guid key, CorrectionType type)
        {
            var stocktake = _session.Load<Stocktake>(id);
            if (stocktake != null)
            {
                var correctedEquipment =
                    stocktake.EquipmentCounts
                    .FirstOrDefault(y => y.Value.HistoricalCorrections.FirstOrDefault(x => x.CorrectionKey == key) != null);

                var historicalCorrection =
                    correctedEquipment.Value.HistoricalCorrections.FirstOrDefault(x => x.CorrectionKey == key);

                if (historicalCorrection != null)
                {
                    var equipmentQuantityInfo = new Dictionary<string, EquipmentQuantityInfo>
                                                {
                                                    {
                                                        correctedEquipment.Key,
                                                        new EquipmentQuantityInfo(correctedEquipment.Value.EquipmentName,
                                                                              historicalCorrection.OldQuantity,
                                                                              historicalCorrection.NewQuantity)
                                                    }
                                                };

                    return new CorrectionRequestDetailsViewModel
                               {
                                   StocktakeMovementId = id,
                                   CorrectionKey = key,
                                   RequestedOn = historicalCorrection.RequestedOn,
                                   RequestedBy = historicalCorrection.RequestedBy.Value,
                                   ClosedOn = historicalCorrection.ClosedOn,
                                   ClosedBy = historicalCorrection.ClosedBy.Value,
                                   Reason = historicalCorrection.Reason,
                                   Equipment = equipmentQuantityInfo,
                                   CorrectionType = type
                               };
                }
            }
            return null;
        }

        private CorrectionRequestDetailsViewModel SetMovementDetailsPage(Guid id, Guid key, CorrectionType type)
        {
            var movement = _session.Load<Movement>(id);
            if (movement != null)
            {
                var requestedOn = movement.PendingMovementCorrection.RequestedOn;
                var reason = movement.PendingMovementCorrection.Reason;
                var oldMovementDate = movement.MovementDate;
                var newMovementDate = movement.PendingMovementCorrection.NewMovementDate;
                var oldEffectiveDate = movement.EffectiveDate;
                var newEffectiveDate = movement.PendingMovementCorrection.NewEffectiveDate;

                var quantitiesData = movement.PendingMovementCorrection.AssetCodeQuantity;
                var equipmentNames =
                    _session.Query<EquipmentIndex.Result, EquipmentIndex>().ToDictionary(x => x.EquipmentCode,
                                                                                         y => y.EquipmentName);
                var equipmentQuantityInfo = quantitiesData
                    .ToDictionary(equipmentInfo => equipmentInfo.Key,
                                  i =>
                                  new EquipmentQuantityInfo(equipmentNames[i.Key],
                                      quantitiesData[i.Key].OldQuantity,
                                      quantitiesData[i.Key].NewQuantity));

                return new CorrectionRequestDetailsViewModel
                           {
                               StocktakeMovementId = id,
                               CorrectionKey = key,
                               RequestedBy = movement.PendingMovementCorrection.RequestedBy.Value,
                               RequestedOn = requestedOn,
                               Reason = reason,
                               Equipment = equipmentQuantityInfo,
                               CorrectionType = type,
                               MovementDate = oldMovementDate,
                               CorrectedDate = newMovementDate,
                               EffectiveDate = oldEffectiveDate,
                               CorrectedEffectiveDate = newEffectiveDate,
                               Status = "Pending Correction",
                               DocketNumber = movement.DocketNumber,
                               PartnerName = movement.DestinationName,
                               PartnerKind = movement.DestinationKind,
                           };
            }
            return null;
        }

        private CorrectionRequestDetailsViewModel SetMovementCancellationDetailsPage(Guid id, Guid key, CorrectionType type)
        {
            var movement = _session.Load<Movement>(id);
            var requestedOn = movement.PendingMovementCancellation.RequestedOn;
            var reason = movement.PendingMovementCancellation.Reason;

            var quantitiesData = movement.Equipments;

            var equipmentQuantityInfo = quantitiesData
                .ToDictionary(equipmentInfo => equipmentInfo.Key,
                              i =>
                              new EquipmentQuantityInfo(quantitiesData[i.Key].EquipmentName,
                                  quantitiesData[i.Key].Quantity,
                                  0));

            var result = new CorrectionRequestDetailsViewModel
            {
                StocktakeMovementId = id,
                CorrectionKey = key,
                RequestedBy = movement.PendingMovementCancellation.RequestedBy.Value,
                RequestedOn = requestedOn,
                Reason = reason,
                Equipment = equipmentQuantityInfo,
                CorrectionType = type,
                Status = "Pending Cancellation",
                DocketNumber = movement.DocketNumber,
                PartnerName = movement.DestinationName,
                PartnerKind = movement.DestinationKind,
                MovementDate = movement.MovementDate,
                EffectiveDate = movement.EffectiveDate
            };

            return result;
        }

        private CorrectionRequestDetailsViewModel SetHistoricalMovementDetailsPage(Guid id, Guid key, CorrectionType type)
        {
            var movement = _session.Load<Movement>(id);
            if (movement != null)
            {
                var historicalCorrection = movement.HistoricalCorrections[key];

                var equipmentNames = _session.Query<EquipmentIndex.Result, EquipmentIndex>()
                    .ToDictionary(x => x.EquipmentCode, y => y.EquipmentName);

                var equipmentQuantityInfo = historicalCorrection.AssetCodeQuantity.ToDictionary(x => x.Key,
                                                                                            y =>
                                                                                            new EquipmentQuantityInfo(
                                                                                                equipmentNames[y.Key],
                                                                                                y.Value.OldQuantity,
                                                                                                y.Value.NewQuantity));

                var result = new CorrectionRequestDetailsViewModel
                {
                    StocktakeMovementId = id,
                    CorrectionKey = key,
                    RequestedOn = historicalCorrection.RequestedOn,
                    RequestedBy = historicalCorrection.RequestedBy.Value,
                    ClosedOn = historicalCorrection.ClosedOn,
                    ClosedBy = historicalCorrection.ClosedBy.Value,
                    Reason = historicalCorrection.Reason,
                    Equipment = equipmentQuantityInfo,
                    CorrectionType = type,
                    MovementDate = historicalCorrection.OldMovementDate,
                    EffectiveDate = historicalCorrection.OldEffectiveDate,
                    CorrectedDate = historicalCorrection.NewMovementDate,
                    CorrectedEffectiveDate = historicalCorrection.NewEffectiveDate,
                    DocketNumber = movement.DocketNumber,
                    PartnerName = movement.DestinationName,
                    PartnerKind = movement.DestinationKind
                };

                switch (historicalCorrection.IsApproved)
                {
                    case true:
                        result.Status = "Approved";
                        break;
                    case false:
                        result.Status = "Declined";
                        break;
                }

                return result;
            }
            return null;
        }

        private CorrectionRequestDetailsViewModel SetHistoricalMovementCancellationDetailsPage(Guid id, Guid key, CorrectionType type)
        {
            var movement = _session.Load<Movement>(id);

            if (movement != null)
            {
                var historicalCancellation = movement.PendingMovementCancellation;
                var requestedOn = historicalCancellation.RequestedOn;
                var reason = historicalCancellation.Reason;

                var quantitiesData = movement.Equipments;

                var equipmentQuantityInfo = quantitiesData
                    .ToDictionary(equipmentInfo => equipmentInfo.Key,
                                  i =>
                                  new EquipmentQuantityInfo(quantitiesData[i.Key].EquipmentName,
                                                            quantitiesData[i.Key].Quantity,
                                                            0));

                var result = new CorrectionRequestDetailsViewModel
                {
                    StocktakeMovementId = id,
                    CorrectionKey = key,
                    RequestedBy = historicalCancellation.RequestedBy.Value,
                    RequestedOn = requestedOn,
                    Reason = reason,
                    Equipment = equipmentQuantityInfo,
                    CorrectionType = type,
                    ClosedOn = historicalCancellation.ClosedOn,
                    ClosedBy = historicalCancellation.ClosedBy.Value,
                    DocketNumber = movement.DocketNumber,
                    PartnerName = movement.DestinationName,
                    PartnerKind = movement.DestinationKind,
                    MovementDate = movement.MovementDate,
                    EffectiveDate = movement.EffectiveDate
                };

                switch (historicalCancellation.IsApproved)
                {
                    case true:
                        result.Status = "Approved";
                        break;
                    case false:
                        result.Status = "Declined";
                        break;
                }
                return result;
            }

            return null;
        }

        private void SendStocktakeCorrectionRequest(string submit, Guid correctionKey, Guid stocktakeId)
        {
            Bus.Send<ChangeStocktakeCorrectionAcceptance>(cmd =>
            {
                cmd.Accept = (submit == "approve");
                cmd.CorrectionKey = correctionKey;
                cmd.StocktakeId = stocktakeId;
                cmd.ClosedBy = Identity.UserKey;
            });
        }

        private void SendMovementCorrectionRequest(string submit, Guid correctionKey, Guid movId)
        {
            Bus.Send<ChangeCorrectionAcceptanceStatus>(cmd =>
            {
                cmd.Accept = (submit == "approve");
                cmd.AccountId = Identity.AccountId;
                cmd.CorrectionKey = correctionKey;
                cmd.MovementId = movId;
                cmd.ClosedBy = Identity.UserKey;
            });
        }

        private void SendMovementCancellationRequest(string submit, Guid correctionKey, Guid movId)
        {
            Bus.Send<ChangeCancellationAcceptanceStatus>(cmd =>
            {
                cmd.Accept = (submit == "approve");
                cmd.AccountId = Identity.AccountId;
                cmd.CancellationKey = correctionKey;
                cmd.MovementId = movId;
                cmd.ClosedBy = Identity.UserKey;
            });
        }
    }
}
