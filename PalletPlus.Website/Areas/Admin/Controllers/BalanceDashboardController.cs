﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using Newtonsoft.Json;
using nTrack.Data.Extenstions;
using nTrack.Extension;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Website.Models.DataVisualization;
using PalletPlus.Website.Services;
using Raven.Client;

namespace PalletPlus.Website.Areas.Admin.Controllers
{
    public class BalanceDashboardController : AdminBaseController
    {
        private readonly IDocumentSession _session;


        public BalanceDashboardController(IDocumentSession session, IThreadingService threadingService)
            : base(threadingService)
        {
            _session = session;
        }

        [GET("BalanceDashboard")]
        public ActionResult Index()
        {
            //TODO: Clean this up.. it looks like a code smell
            ViewBag.Sites = _session.Advanced.LoadStartingWith<Site>("sites/").Select(x => x.CompanyName).ToArray();
            //.Query<SiteIndex.Result, SiteIndex>()
            //.Select(x => x.SiteName).ToArray();
            return View();
        }

        [GET("GetAccountBalanceViewModel")]
        public JsonResult GetAccountBalanceViewModel()
        {
            var data = new List<Dictionary<string, object>>();


            //var vm = new List<SiteEquipmentBalanceModel>();

            var sites = _session.Advanced.LoadStartingWith<Site>("sites/")
                .ToDictionary(x => x.Id, y => y.CompanyName);

            var balances = _session.Query<MovementBalanceIndex.Result, MovementBalanceIndex>()
                .Where(x => x.MovementType == "Transfer")
                .ToList();

            var equipmentindex = _session.Query<EquipmentIndex.Result, EquipmentIndex>().ToList();

            var equipments = (from p in equipmentindex
                             group p by new {p.EquipmentCode, p.EquipmentName}
                             into grp
                             select new 
                                        {
                                            grp.Key.EquipmentCode,
                                            grp.Key.EquipmentName
                                        }).ToList();

            var series = equipments.Select(x => new { name = x.EquipmentName, field = "e{0}".Fill(x.EquipmentCode) }).ToList();

            foreach (var site in sites)
            {
                var siteDic = new Dictionary<string, object>();
                siteDic.Add("site", site.Value);

                foreach (var equipment in equipments)
                {
                    var balance =
                        balances.SingleOrDefault(
                            x => x.EquipmentCode == equipment.EquipmentCode && x.SiteId == site.Key.ToStringId<Site>());

                    siteDic.Add("e{0}".Fill(equipment.EquipmentCode), balance != null ? balance.Quantity : 0);
                }

                data.Add(siteDic);

             
            }

          

            return Json(new { series = series, data = data }, JsonRequestBehavior.AllowGet);
        }

        void WriteJSONProperty(JsonWriter writer, List<SiteEquipmentBalanceModel> source, string site, string equipment)
        {
            var query = (from p in source
                         group p by new { p.EquipmentName, p.SiteName }
                             into grp
                             where grp.Key.EquipmentName == equipment && grp.Key.SiteName == site
                             select new SiteEquipmentBalanceModel
                                        {
                                            EquipmentName = grp.Key.EquipmentName,
                                            SiteName = grp.Key.SiteName,
                                            Quantity = grp.Sum(x => x.Quantity)
                                        }).FirstOrDefault();

            writer.WritePropertyName(equipment);
            if (query != null)
            {

                writer.WriteValue(query.Quantity);
            }
            else
            {
                writer.WriteValue(0);
            }
        }


        [GET("BalanceDashboard/GetDashboardColors")]
        public JsonResult GetDashboardColors()
        {
            var account = _session.Load<Account>(Identity.AccountId);
            if (account != null)
            {
                var colors = account.DashboardEquipmentColors.Select(x => x.Value);
                var ret = colors.Select(x => string.Format("rgb({0},{1},{2})", x.R, x.G, x.B)).ToArray();

                return Json(ret, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
    }
}
