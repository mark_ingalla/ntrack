﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using PalletPlus.Data.Indices;
using PalletPlus.Website.Areas.Admin.ViewModels.Dashboard;
using PalletPlus.Website.Services;
using Raven.Client;

namespace PalletPlus.Website.Areas.Admin.Controllers
{
    public class DayHireDashboardController : AdminBaseController
    {
        private readonly IDocumentSession _session;

        public DayHireDashboardController(IDocumentSession session, IThreadingService threadingService)
            : base(threadingService)
        {
            _session = session;
        }

        [GET("GetDayHireBalanceViewModel")]
        public JsonResult GetDayHireBalanceViewModel()
        {

            var dayHires = new List<DayHireViewModel>();


            var start = 0;
            while (true)
            {
                var hireDateBalance = _session.Query<DayHireBalanceDashboardIndex.Result, DayHireBalanceDashboardIndex>()
                    .Take(1024).Skip(start).ToList()
                    .Select(y => new
                    {
                        y.EquipmentCode,
                        y.IncomingQty,
                        y.Week,
                        y.Year,
                        y.EquipmentName,
                        y.ClosingBalance,
                        y.MovementDate

                    }).ToList();
                
                if (hireDateBalance.Count == 0)
                    break;

                start += hireDateBalance.Count;

                dayHires.AddRange((from p in hireDateBalance
                            group p by new { p.EquipmentCode, p.EquipmentName, p.Year, p.Week}
                                into grp
                                select
                                    new DayHireViewModel(grp.Key.EquipmentCode,
                                                         grp.Key.EquipmentName,
                                                         //new DateTime(grp.Key.Year, grp.Key.Month, grp.Key.Day),
                                                        grp.FirstOrDefault().MovementDate.Date,
                                                         grp.Sum(x => x.IncomingQty),grp.Sum(x=>x.ClosingBalance))).ToList());

                
            }


            //process for previous balance
            foreach (var dayHire in dayHires)
            {
                dayHire.PreviousDayBalance = GetPreviousBalance(dayHire.EquipmentCode, dayHire.MovementDate, dayHires);
            }

            // 6weeks
            var dateFrom = DateTime.Now.AddMonths(-1).AddDays(-14).Date;


            return Json(dayHires.Where(x => x.MovementDate >= dateFrom && x.MovementDate <= DateTime.Now).OrderBy(x => x.MovementDate).Select(x => new
            {
                x.MovementDateString,
                x.EquipmentCode,
                x.EquipmentName,
                x.DayHire,

            }), JsonRequestBehavior.AllowGet);
        }

        private DateTime GetDayofWeek(DateTime input, DayOfWeek day)
        {

            int delta = day - input.DayOfWeek;
            DateTime dayofWeek = input.AddDays(delta);
            return dayofWeek;
        }

        private int GetPreviousBalance(string equipcode, DateTime movementDate, List<DayHireViewModel> source)
        {
            var data =
                source
                .Where(x => x.EquipmentCode == equipcode && x.MovementDate < movementDate)
                .OrderByDescending(x => x.MovementDate).FirstOrDefault();
            if (data != null)
            {
                return data.ClosingBalance;
            }
            return 0;
        }









    }
}
