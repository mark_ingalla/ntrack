﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Commands.Supplier;
using PalletPlus.Website.Areas.Admin.ViewModels.Supplier;
using PalletPlus.Website.Services;
using Raven.Client;

namespace PalletPlus.Website.Areas.Admin.Controllers
{

    public class SupplierController : AdminBaseController
    {
        readonly IDocumentSession _session;

        public SupplierController(IDocumentSession session, IThreadingService threadingService)
            : base(threadingService)
        {
            _session = session;
        }

        [GET("Suppliers")]
        public ActionResult Index(bool showDeleted = false)
        {
            ViewBag.ShowDeleted = showDeleted;
            return View(GetSuppliers(showDeleted));
        }

        [GET("Supplier/Create")]
        public ActionResult Create()
        {
            var integrtionLookup = _session.Load<SupplierIntegrationLookup>(SupplierIntegrationLookup.LookupId);

            var vm = new SupplierViewModel
                {
                    ComboboxCountries = GetCountries(),
                    ExportProviderNames = integrtionLookup.Lookup
                };

            return View(vm);
        }

        [POST("Supplier/Create")]
        public ActionResult Create(SupplierViewModel supplier)
        {
            var supplierEntity = _session.Query<Supplier>().FirstOrDefault(x => x.CompanyName == supplier.SupplierName);
            if (supplierEntity != null)
            {
                Failure("A supplier with that name already exists.");
                supplier.SupplierName = string.Empty;
                return View(supplier);
            }

            supplier.SupplierId = Guid.NewGuid();
            Bus.Send<CreateSupplier>(cmd =>
                {
                    cmd.SupplierId = supplier.SupplierId;
                    cmd.Name = supplier.SupplierName;
                    cmd.ProviderName = supplier.ProviderName;
                    cmd.EmailAddress = supplier.ContactEmail;
                    cmd.IsPTAEnabled = supplier.IsPTAEnabled;
                });

            ThreadingService.ObjectCreated<Supplier>(supplier.SupplierId);
            RecordCreated("details");

            return RedirectToAction("Edit", "Supplier", new { id = supplier.SupplierId });
        }

        [GET("Supplier/{id}")]
        public ActionResult Edit(Guid id)
        {
            var integrationLookup = _session.Load<SupplierIntegrationLookup>(SupplierIntegrationLookup.LookupId);
            var supplier = _session.Load<Supplier>(id);

            var vm = new SupplierEditViewModel(supplier, GetCountries(), integrationLookup.Lookup);

            return View(vm);
        }

        [POST("Supplier/{id}")]
        public ActionResult Edit(SupplierEditViewModel supplier)
        {
            var supplierEntity = _session.Query<Supplier>()
                .FirstOrDefault(x => x.CompanyName == supplier.SupplierName && supplier.SupplierId != x.Id);

            if (supplierEntity != null)
            {
                Failure("A supplier with that name already exists.");
                return RedirectToAction("Edit", new { id = supplier.SupplierId });
            }

            Bus.Send<UpdateSupplier>(cmd =>
                {
                    cmd.SupplierId = supplier.SupplierId;
                    cmd.Name = supplier.SupplierName;
                    cmd.ProviderName = supplier.ProviderName;
                    cmd.EmailAddress = supplier.ContactEmail;
                    cmd.IsPTAEnabled = supplier.IsPTAEnabled;
                });

            ThreadingService.ObjectUpdated<Supplier>(supplier.SupplierId, DateTime.UtcNow);
            RecordUpdated("details");

            return RedirectToAction("Edit", "Supplier", new { id = supplier.SupplierId });
        }

        [GET("Supplier/Delete/{id}")]
        public ActionResult Delete(Guid id, bool showDeleted)
        {
            Bus.Send<DeleteSupplier>(cmd => cmd.SupplierId = id);

            ThreadingService.ObjectDeleted<Supplier>(id);

            return RedirectToAction("Index", SetRouteValues(showDeleted));
        }

        [GET("Supplier/Undelete/{id}")]
        public ActionResult Undelete(Guid id, bool showDeleted)
        {
            Bus.Send<UndeleteSupplier>(cmd => cmd.SupplierId = id);

            ThreadingService.ObjectUndeleted<Supplier>(id);

            return RedirectToAction("Index", SetRouteValues(showDeleted));
        }

        [POST("Supplier/{id}/AddEquipment")]
        public ActionResult AddEquipment(SupplierEditViewModel supplier)
        {
            Bus.Send<AddEquipmentToSupplier>(
                cmd =>
                {
                    cmd.SupplierId = supplier.SupplierId;
                    cmd.EquipmentCode = supplier.EquipmentVM.EquipmentCode;
                    cmd.EquipmentName = supplier.EquipmentVM.EquipmentName;
                });

            ThreadingService.ObjectUpdated<Supplier>(supplier.SupplierId, DateTime.UtcNow);
            RecordUpdated("equipment");

            return RedirectToAction("Edit", new { id = supplier.SupplierId });
        }

        [GET("Supplier/{id}/DeleteEquipment/{equipmentCode}")]
        public ActionResult DeleteEquipment(Guid id, string equipmentCode)
        {
            Bus.Send<DiscontinueEquipment>(
                cmd =>
                {
                    cmd.SupplierId = id;
                    cmd.EquipmentCode = equipmentCode;
                });

            ThreadingService.ObjectUpdated<Supplier>(id, DateTime.UtcNow);
            RecordUpdated("equipment");

            return RedirectToAction("Edit", new { id });
        }

        [GET("Supplier/{id}/UndeleteEquipment/{equipmentCode}")]
        public ActionResult UndeleteEquipment(Guid id, string equipmentCode)
        {
            Bus.Send<UndiscontinueEquipment>(
                cmd =>
                {
                    cmd.SupplierId = id;
                    cmd.EquipmentCode = equipmentCode;
                });

            ThreadingService.ObjectUpdated<Supplier>(id, DateTime.UtcNow);
            RecordUpdated("equipment");

            return RedirectToAction("Edit", new { id });
        }

        [POST("Supplier/{id}/AddLocation")]
        public ActionResult AddLocation(SupplierEditViewModel supplier)
        {
            Bus.Send<AddLocationToSupplier>(
                cmd =>
                {
                    cmd.SupplierId = supplier.SupplierId;
                    cmd.LocationKey = Guid.NewGuid();
                    cmd.Name = supplier.LocationVM.LocationName;
                    cmd.AccountNumber = supplier.LocationVM.AccountNumber;
                    cmd.AddressLine1 = supplier.LocationVM.AddressLine1;
                    cmd.AddressLine2 = supplier.LocationVM.AddressLine2;
                    cmd.Postcode = supplier.LocationVM.Postcode;
                    cmd.Country = supplier.LocationVM.Country;
                    cmd.State = supplier.LocationVM.State;
                    cmd.Suburb = supplier.LocationVM.Suburb;
                    cmd.ContactEmail = supplier.LocationVM.ContactEmail;
                    cmd.ContactName = supplier.LocationVM.ContactName;
                });

            ThreadingService.ObjectUpdated<Supplier>(supplier.SupplierId, DateTime.UtcNow);
            RecordUpdated("locations");

            return RedirectToAction("Edit", new { id = supplier.SupplierId });
        }

        [POST("Supplier/{id}/EditLocation")]
        public ActionResult EditLocation(LocationViewModel location)
        {
            Bus.Send<UpdateSupplierLocation>(
                cmd =>
                {
                    cmd.SupplierId = location.SupplierId;
                    cmd.LocationKey = location.LocationKey;
                    cmd.Name = location.LocationName;
                    cmd.AccountNumber = location.AccountNumber;
                    cmd.AddressLine1 = location.AddressLine1;
                    cmd.AddressLine2 = location.AddressLine2;
                    cmd.Postcode = location.Postcode;
                    cmd.Country = location.Country;
                    cmd.State = location.State;
                    cmd.Suburb = location.Suburb;
                    cmd.ContactEmail = location.ContactEmail;
                    cmd.ContactName = location.ContactName;
                });

            ThreadingService.ObjectUpdated<Supplier>(location.SupplierId, DateTime.UtcNow);
            RecordUpdated("locations");

            return RedirectToAction("Edit", new { id = location.SupplierId });
        }

        [GET("Supplier/{id}/RemoveLocation")]
        public ActionResult RemoveLocation(Guid id, Guid locationKey)
        {
            Bus.Send<RemoveSupplierLocation>(
                cmd =>
                {
                    cmd.SupplierId = id;
                    cmd.LocationKey = locationKey;
                });

            ThreadingService.ObjectUpdated<Supplier>(id, DateTime.UtcNow);
            RecordUpdated("locations");

            return RedirectToAction("Edit", new { id = id });
        }

        [GET("Supplier/{id}/UnremoveLocation")]
        public ActionResult UnremoveLocation(Guid id, Guid locationKey, string accountNumber)
        {
            Bus.Send<UnremoveSupplierLocation>(
                cmd =>
                {
                    cmd.SupplierId = id;
                    cmd.LocationKey = locationKey;
                    cmd.AccountNumber = accountNumber;
                });

            ThreadingService.ObjectUpdated<Supplier>(id, DateTime.UtcNow);
            RecordUpdated("locations");

            return RedirectToAction("Edit", new { id = id });
        }

        private IEnumerable<SupplierViewModel> GetSuppliers(bool showDeleted = false)
        {
            var results = showDeleted ?
                _session.Query<Supplier, SupplierIndex>() :
                _session.Query<Supplier, SupplierIndex>().Where(x => !x.IsDeleted);

            return results.ToList().Select(p => new SupplierViewModel(p));

        }
    }
}