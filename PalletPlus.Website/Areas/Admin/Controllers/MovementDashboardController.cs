﻿using AttributeRouting.Web.Mvc;
using PalletPlus.Data.Indices;
using PalletPlus.Website.Areas.Admin.Models.DataVisualization;
using PalletPlus.Website.Services;
using PalletPlus.Website.ViewModels.Dashboard;
using Raven.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PalletPlus.Website.Areas.Admin.Controllers
{
    public class MovementDashboardController : AdminBaseController
    {
        private readonly IDocumentSession _session;

        public MovementDashboardController(IDocumentSession session, IThreadingService threadingService)
            : base(threadingService)
        {
            _session = session;
        }

        [GET("MovementDashboard")]
        public ActionResult Index()
        {
            var movements =
                _session.Query<MovementDashboardIndex.Result, MovementDashboardIndex>().Where(
                    p => p.Year == DateTime.UtcNow.Year).ToList();


            var vm = new DashboardViewModel()
                         {
                             InOutChart = new VizViewModel(GetMovementChartList(movements, "Transfer", "Partner"), "Movements", "transferchart"),
                             IssueReturnChart = new VizViewModel(GetMovementChartList(movements, "Transfer", "Supplier"), "Issues/Returns", "issuereturnchart"),
                             InternalChart = new VizViewModel(GetMovementChartList(movements, "Internal", "Site"), "Internal Movements", "internalchart")
                         };

            return View(vm);
        }

        private IEnumerable<MovementChartModel> GetMovementChartList(IEnumerable<MovementDashboardIndex.Result> movements, string type, string destinationKind)
        {
            var vm = new List<MovementChartModel>();

            var dt = (int)Math.Floor((decimal)DateTime.Now.Date.DayOfYear / 7);
            var filteredMovements = movements.Where(x =>
                        x.MovementType == type && x.DestinationKind == destinationKind).ToList();

            for (int i = dt - 4; i <= dt; i++)
            {
                var itemsOfWeekOfType =
                    filteredMovements.Where(
                        x => x.Week == i).ToList();

                var inItem = itemsOfWeekOfType.Where(x => x.Direction == "In");
                var outItem = itemsOfWeekOfType.Where(x => x.Direction == "Out");

                var item = new MovementChartModel(i, inItem.Sum(p => p.Counts), outItem.Sum(p => p.Counts));
                vm.Add(item);
            }

            return vm;
        }
    }
}
