﻿//using AttributeRouting.Web.Mvc;
//using AutoMapper;
//using nTrack.Extension;
//using PalletPlus.Data;
//using PalletPlus.Data.Indices;
//using PalletPlus.Messages.Commands.Transporter;
//using PalletPlus.Website.Areas.Admin.ViewModels.Transporter;
//using PalletPlus.Website.Services;
//using Raven.Client;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web.Mvc;

//namespace PalletPlus.Website.Areas.Admin.Controllers
//{

//    public class TransporterController : AdminBaseController
//    {
//        readonly IDocumentSession _session;

//        public TransporterController(IDocumentSession session, IThreadingService threadingService)
//            : base(threadingService)
//        {
//            _session = session;
//        }

//        [GET("Transporters")]
//        public ActionResult Index(bool showDeleted = false)
//        {
//            ViewBag.ShowDeleted = showDeleted;
//            return View(GetTransporters(showDeleted));
//        }

//        [GET("Transporter/Create")]
//        public ActionResult Create()
//        {
//            return View(new TransporterViewModel());
//        }

//        [POST("Transporter/Create")]
//        public ActionResult Create(TransporterViewModel transporter)
//        {
//            transporter.TransporterId = Guid.NewGuid();
//            Bus.SendMap<CreateTransporter>(transporter);

//            ThreadingService.ObjectCreated<Transporter>(transporter.TransporterId);
//            RecordCreated("details");

//            return RedirectToAction("Edit", new { id = transporter.TransporterId });
//        }

//        [GET("Transporter/{id}")]
//        public ActionResult Edit(Guid id)
//        {
//            var transporter = _session.Load<Transporter>(id);
//            var vm = Mapper.Map<TransporterViewModel>(transporter);

//            return View(vm);
//        }

//        [POST("Transporter/{id}")]
//        public ActionResult Edit(TransporterViewModel transporter)
//        {
//            var now = Bus.SendMap<UpdateTransporter>(transporter);

//            ThreadingService.ObjectUpdated<Transporter>(transporter.TransporterId, now);
//            RecordUpdated("details");

//            return View(transporter);
//        }

//        [GET("Transporter/Delete/{id}")]
//        public ActionResult Delete(Guid id, bool showDeleted)
//        {
//            Bus.Send<DeleteTransporter>(cmd => cmd.TransporterId = id);
//            ThreadingService.ObjectDeleted<Transporter>(id);

//            return RedirectToAction("Index", SetRouteValues(showDeleted));
//        }

//        [GET("Transporter/Undelete/{id}")]
//        public ActionResult Undelete(Guid id, bool showDeleted)
//        {
//            Bus.Send<UndeleteTransporter>(cmd => cmd.TransporterId = id);
//            ThreadingService.ObjectUndeleted<Transporter>(id);

//            return RedirectToAction("Index", SetRouteValues(showDeleted));
//        }

//        private IEnumerable<TransporterViewModel> GetTransporters(bool showDeleted = false)
//        {
//            var result = showDeleted
//                             ? _session.Query<TransporterIndex.Result, TransporterIndex>()
//                             : _session.Query<TransporterIndex.Result, TransporterIndex>().Where(x => !x.IsDeleted);

//            return Mapper.Map<IEnumerable<TransporterIndex.Result>, IEnumerable<TransporterViewModel>>(result.OrderBy(x => x.TransporterName));
//        }
//    }
//}