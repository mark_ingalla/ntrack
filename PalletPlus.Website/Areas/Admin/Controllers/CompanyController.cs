﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using nTrack.Core.Messages;
using nTrack.Data.Extenstions;
using nTrack.Data.Indices;
using nTrack.Extension;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Commands.Account;
using PalletPlus.Website.Areas.Admin.ViewModels.Company;
using PalletPlus.Website.Areas.Admin.ViewModels.User;
using PalletPlus.Website.Services;
using Raven.Client;
using Raven.Client.Linq;
using Subscription.Messages.Commands.Account.User;
using Account = PalletPlus.Data.Account;

namespace PalletPlus.Website.Areas.Admin.Controllers
{

    public class CompanyController : AdminBaseController
    {
        private readonly IDocumentSession _session;

        public CompanyController(IDocumentSession session, IThreadingService service)
            : base(service)
        {
            _session = session;
        }

        [GET("Company")]
        public ActionResult Index()
        {
            var account = _session.Load<Account>(Identity.AccountId);
            var suppliers = _session.Query<Supplier>(typeof(SupplierIndex).Name);
            List<UserIndex.Result> userResults;

            using (var systemSession = MvcApplication.SystemStore.OpenSession())
            {
                userResults = systemSession.Query<UserIndex.Result, UserIndex>()
                    .Where(x => x.AccountId == Identity.AccountId.ToStringId<Account>())
                    //.Where(x => x.AccountId == Identity.AccountId.ToStringId<Account>() && !x.IsDeleted)
                    .ToList();
            }

            var vm = new CompanyViewModel(account, suppliers.ToList(), userResults);

            //var vm = Mapper.Map<CompanyViewModel>(account);

            ////working, we have validation on client side, with that we don't have to move it to Authentication and store requirements in session
            //Type type = typeof(FieldsForRequirements); 

            //var requirements = type.GetFields();

            //foreach (var requirement in requirements)
            //{

            //    vm.Requirements.Add(new RequirementViewModel()
            //                            {
            //                                Checked = account.RequiredFields.Any(p => p == (string)requirement.GetValue(null)),
            //                                Name = requirement.Name.ToSeparateWords(),
            //                                Key = (string)requirement.GetValue(null)
            //                            });
            //}

            //GetSuppliers(ref vm, account);

            //using (var systemSession = MvcApplication.SystemStore.OpenSession())
            //{
            //    var userResults = systemSession.Query<UserIndex.Result, UserIndex>()
            //        .Where(x => x.AccountId == Identity.AccountId.ToStringId<Account>())
            //        .ToList();

            //    vm.UsersVM = userResults.Select(x => new UserViewModel
            //                                             {
            //                                                 Email = x.Email,
            //                                                 FullName = x.UserFullName,
            //                                                 Mobile = x.UserMobile,
            //                                                 Phone = x.UserPhone,
            //                                                 Role = x.Role,
            //                                                 UserKey = x.UserKey,
            //                                                 IsDeleted = x.IsDeleted,
            //                                                 IsLocked = x.IsLocked
            //                                             }).ToList();
            //}

            return View(vm);
        }

        [GET("Company/EditUser/{key}")]
        public ActionResult EditUser(Guid key)
        {
            UserEditViewModel vm = new UserEditViewModel(Enum.GetNames(typeof(UserRole)));

            using (var systemSession = MvcApplication.SystemStore.OpenSession())
            {
                var userResult = systemSession.Query<UserIndex.Result, UserIndex>()
                    .FirstOrDefault(x => x.UserKey == key);

                var userFullNameSplit = userResult.UserFullName.Split(' ');
                vm.FirstName = userFullNameSplit[0];
                vm.LastName = userFullNameSplit[1];
                vm.Role = userResult.Role;
                vm.Email = userResult.Email;
                vm.Mobile = userResult.UserMobile;
                vm.Phone = userResult.UserPhone;
                vm.UserKey = userResult.UserKey;
            }

            return View("User/EditUser", vm);
        }

        [POST("Company/EditUser/{key}")]
        public ActionResult EditUser(UserEditViewModel vm)
        {
            using (var systemSession = MvcApplication.SystemStore.OpenSession())
            {
                if (!IsUniqueEmail(systemSession, vm.Email, vm.UserKey))
                {
                    Failure("Email already in use.");
                    return RedirectToAction("EditUser", new { key = vm.UserKey });
                }

                Bus.Send<UpdateUserDetail>(cmd =>
                                               {
                                                   cmd.AccountId = Identity.AccountId;
                                                   cmd.FirstName = vm.FirstName;
                                                   cmd.LastName = vm.LastName;
                                                   cmd.Email = vm.Email;
                                                   cmd.Mobile = vm.Mobile;
                                                   cmd.Phone = vm.Phone;
                                                   cmd.UserKey = vm.UserKey;
                                               });

                var user = systemSession.Query<UserIndex.Result, UserIndex>()
                    .FirstOrDefault(c => c.Email == vm.Email && c.UserKey != vm.UserKey);
                if (user != null)
                {
                    if (user.Role != vm.Role)
                    {
                        var userRole = (UserRole)Enum.Parse(typeof(UserRole), vm.Role);

                        Bus.Send(new ChangeUserRole { AccountId = Identity.AccountId, Role = userRole, UserKey = user.UserKey });
                    }
                }
            }

            //ThreadingService.UserUpdated(Identity.AccountId, vm.UserKey, DateTime.UtcNow);
            ThreadingService.ObjectUpdated<Account>(Identity.AccountId, DateTime.Now);
            RecordUpdated("users");
            return RedirectToAction("EditUser", new { key = vm.UserKey });
        }

        [GET("Company/LockUser/{key}")]
        public ActionResult LockUser(Guid key)
        {
            Bus.Send(new LockUser
            {
                AccountId = Identity.AccountId,
                Reason = string.Empty,
                UserKey = key
            });
            ThreadingService.UserUpdated(Identity.AccountId, key, DateTime.UtcNow);
            RecordUpdated("users");
            return RedirectToAction("Index");
        }

        [GET("Company/UnlockUser/{key}")]
        public ActionResult UnlockUser(Guid key)
        {
            Bus.Send(new UnlockUser
            {
                AccountId = Identity.AccountId,
                Reason = string.Empty,
                UserKey = key
            });

            ThreadingService.UserUpdated(Identity.AccountId, key, DateTime.UtcNow);
            RecordUpdated("users");
            return RedirectToAction("Index");
        }

        [GET("Company/RemoveUser/{key}")]
        public PartialViewResult RemoveUser(Guid key)
        {
            using (var systemSession = MvcApplication.SystemStore.OpenSession())
            {
                var userResult = systemSession.Query<UserIndex.Result, UserIndex>()
                    .FirstOrDefault(x => x.UserKey == key);


                if (userResult != null)
                {
                    var vm = new UserViewModel
                    {
                        FullName = userResult.UserFullName,
                        UserKey = userResult.UserKey
                    };

                    return PartialView("User/_ConfirmDeleteUser",vm);
    
                }
                
            }
            
            return PartialView("User/_ConfirmDeleteUser");
        }

        [POST("Company/RemoveUser/{key}")]
        public ActionResult RemoveUser(UserViewModel viewModel)
        {
            Bus.Send(new RemoveUser
            {
                AccountId = Identity.AccountId,
                UserKey = viewModel.UserKey
            });

           // ThreadingService.UserUpdated(Identity.AccountId, key, DateTime.UtcNow);
            ThreadingService.ObjectUpdated<Account>(Identity.AccountId, DateTime.UtcNow);
            RecordUpdated("users");
            return RedirectToAction("Index");
        }


        [GET("Company/RetrieveUser/{key}")]
        public ActionResult RetrieveUser(Guid key)
        {
            Bus.Send(new RetrieveUser
            {
                AccountId = Identity.AccountId,
                UserKey = key
            });

          //  ThreadingService.UserUpdated(Identity.AccountId, key, DateTime.UtcNow);
            ThreadingService.ObjectUpdated<Account>(Identity.AccountId, DateTime.UtcNow);
            RecordUpdated("users");
            return RedirectToAction("Index");
        }

        [POST("Company/AddEquipmentColor")]
        public ActionResult AddEquipmentColor(CompanyViewModel company)
        {
            if (string.IsNullOrWhiteSpace(company.EquipmentItemColor.EquipmentCode))
            {
                Failure("suppliers", "Please choose equipment from the list of available items.");
                return RedirectToAction("Index");
            }

            var account = _session.Load<Account>(Identity.AccountId);

            account.DashboardEquipmentColors.Add(Guid.NewGuid(), new DashboardEquipmentColor
                                                                    {
                                                                        EquipmentCode = company.EquipmentItemColor.EquipmentCode,
                                                                        EquipmentName = company.EquipmentItemColor.EquipmentName,
                                                                        R = company.EquipmentItemColor.R,
                                                                        G = company.EquipmentItemColor.G,
                                                                        B = company.EquipmentItemColor.B
                                                                    });
            _session.SaveChanges();


            ThreadingService.ObjectUpdated<Account>(Identity.AccountId, DateTime.UtcNow);
            RecordUpdated("colors");

            return RedirectToAction("Index");


        }

        [POST("Company/AddSupplier")]
        public ActionResult AddSupplier(CompanyViewModel company)
        {
            if (company.SupplierVM.SupplierId.IsEmpty())
            {
                Failure("suppliers", "Please choose supplier from the list of available items.");
                return RedirectToAction("Index");
            }

            company.SupplierVM.SupplierName = _session.Load<Supplier>(company.SupplierVM.SupplierId.ToStringId<Supplier>()).CompanyName;

            Bus.Send(new AddSupplierToAccount
                {
                    SupplierId = company.SupplierVM.SupplierId,
                    SupplierName = company.SupplierVM.SupplierName,
                    AccountNumber = company.SupplierVM.AccountNumber,
                    Prefix = company.SupplierVM.Prefix,
                    Suffix = company.SupplierVM.Suffix,
                    SequenceStartNumber = company.SupplierVM.SequenceStartNumber,
                    AccountId = Identity.AccountId
                });

            var now = DateTime.UtcNow;

            ThreadingService.ObjectUpdated<Account>(Identity.AccountId, now);
            RecordUpdated("suppliers");

            return RedirectToAction("Index");
        }

        [POST("Company/EditSupplier")]
        public ActionResult EditSupplier(CompanySupplierEditViewModel supplier)
        {
            Bus.Send(new UpdateAccountSupplier()
            {
                AccountId = Identity.AccountId,
                Prefix = supplier.Prefix,
                Suffix = supplier.Suffix,
                SequenceStartNumber = supplier.SequenceStartNumber,
                SupplierId = supplier.SupplierId
            });

            var now = DateTime.UtcNow;

            ThreadingService.ObjectUpdated<Account>(Identity.AccountId, now);
            RecordUpdated("suppliers");

            return RedirectToAction("Index");
        }

        [GET("Company/DeleteSupplier/{supplierId}")]
        public ActionResult DeleteSupplier(Guid supplierId)
        {
            Bus.Send<RemoveSupplierFromAccount>(
                cmd =>
                {
                    cmd.AccountId = Identity.AccountId;
                    cmd.SupplierId = supplierId;
                });

            ThreadingService.ObjectUpdated<Account>(Identity.AccountId, DateTime.UtcNow);
            RecordUpdated("suppliers");

            return RedirectToAction("Index");
        }

        [POST("Company/EditRequirements")]
        public ActionResult EditRequirements(List<RequirementViewModel> vm)
        {
            var account = _session.Load<Account>(Identity.AccountId);

            account.RequiredFields = new HashSet<string>();

            Bus.Send(new EditRequirements()
                         {
                             AccountId = Identity.AccountId,
                             RequiredFields = vm.Where(p => p.Checked).Select(p => p.Key).ToHashSet()
                         });

            //foreach (var requirement in vm)
            //{
            //    if (requirement.Checked)
            //    {
            //        account.RequiredFields.Add(requirement.Key);
            //    }
            //}

            RecordUpdated("requirements");

            //     _session.SaveChanges();

            return RedirectToAction("Index");
        }

        [GET("CorrectionReasonIsUnique")]
        public JsonResult CorrectionReasonIsUnique(CompanyViewModel vm)
        {
            return Json(!_session.Load<Account>(Identity.AccountId).CorrectionReasons.ContainsValue(vm.SettingsVM.CorrectionReason),
                        JsonRequestBehavior.AllowGet);
        }

        [GET("CancellationReasonIsUnique")]
        public JsonResult CancellationReasonIsUnique(CompanyViewModel vm)
        {
            return Json(!_session.Load<Account>(Identity.AccountId).CancellationReasons.ContainsValue(vm.SettingsVM.CancellationReason),
                        JsonRequestBehavior.AllowGet);
        }

        [POST("Company/AddCorrectionReason")]
        public ActionResult AddCorrectionReason(CompanyViewModel utility)
        {
            if (!string.IsNullOrEmpty(utility.SettingsVM.CorrectionReason))
            {
                Bus.Send<AddCorrectionReason>(cmd =>
                {
                    cmd.ReasonKey = Guid.NewGuid();
                    cmd.AccountId = Identity.AccountId;
                    cmd.Reason = utility.SettingsVM.CorrectionReason;
                });

                RecordUpdated("correction-reasons");
            }

            return RedirectToAction("Index");
        }

        [GET("Company/RemoveCorrectionReason/{id}")]
        public ActionResult RemoveCorrectionReason(Guid id)
        {
            Bus.Send<RemoveCorrectionReason>(cmd =>
            {
                cmd.AccountId = Identity.AccountId;
                cmd.ReasonKey = id;
            });

            RecordUpdated("correction-reasons");

            return RedirectToAction("Index");
        }

        [POST("Company/AddCancellationReason")]
        public ActionResult AddCancellationReason(CompanyViewModel utility)
        {
            if (!string.IsNullOrEmpty(utility.SettingsVM.CancellationReason))
            {
                Bus.Send<AddCancellationReason>(cmd =>
                {
                    cmd.ReasonKey = Guid.NewGuid();
                    cmd.AccountId = Identity.AccountId;
                    cmd.Reason = utility.SettingsVM.CancellationReason;
                });

                RecordUpdated("cancellation-reasons");
            }

            return RedirectToAction("Index");
        }

        [GET("Company/RemoveCancellationReason/{id}")]
        public ActionResult RemoveCancellationReason(Guid id)
        {
            Bus.Send<RemoveCancellationReason>(cmd =>
            {
                cmd.AccountId = Identity.AccountId;
                cmd.ReasonKey = id;
            });

            RecordUpdated("cancellation-reasons");

            return RedirectToAction("Index");
        }

        [POST("Company/SetColor/{code}/{r}/{g}/{b}")]
        public ActionResult SetColor(Guid code, int r, int g, int b)
        {
            var account = _session.Load<Account>(Identity.AccountId);
            var color = new DashboardEquipmentColor()
            {
                R = r,
                G = g,
                B = b
            };
            if (!account.DashboardEquipmentColors.ContainsKey(code))
                account.DashboardEquipmentColors.Add(code, color);
            else
            {
                //existing already
                var existingcolor = account.DashboardEquipmentColors[code];
                color.EquipmentCode = existingcolor.EquipmentCode;
                color.EquipmentName = existingcolor.EquipmentName;
                account.DashboardEquipmentColors[code] = color;
            }

            _session.SaveChanges();

            RecordUpdated("colors");

            return RedirectToAction("Index");
        }

        [POST("Company/ChangeExportSettings")]
        public ActionResult ChangeExportSettings(bool exportMovementIn, bool exportMovementOut)
        {
            var account = _session.Load<Account>(Identity.AccountId);

            if (account.ExportMovementIn != exportMovementIn || account.ExportMovementOut != exportMovementOut)
            {
                Bus.Send(new ChangeExportSettings()
                             {
                                 AccountId = Identity.AccountId,
                                 ExportMovementIn = exportMovementIn,
                                 ExportMovementOut = exportMovementOut
                             });
            }

            RecordUpdated("exportSettings");

            return RedirectToAction("Index");
        }


        [GET("Company/IsValidSupplier")]
        public ActionResult IsValidSupplier(CompanySupplierViewModel supplierVM)
        {
            bool res;

            if (supplierVM.SupplierId.IsEmpty())
                res = false;
            else
            {
                res = _session.Load<Supplier>(supplierVM.SupplierId) != null;
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        void GetSuppliers(ref CompanyViewModel viewModel, Account account)
        {
            var alreadyAdded = account.Suppliers.Select(x => x.Key).ToList();

            var suppliers = _session.Query<Supplier>(typeof(SupplierIndex).Name)
                .Select(x => new { IsDeleted = x.IsDeleted, SupplierId = x.IdString, SupplierName = x.CompanyName })
                .ToList();

            var comboboxSuppliers = suppliers
                .Where(x => !x.IsDeleted && !x.SupplierId.In(alreadyAdded))
                .Select(x => new { Id = x.SupplierId.ToGuidId(), SupplierName = x.SupplierName });

            viewModel.ComboboxSuppliers = comboboxSuppliers;
            viewModel.SupplierNames = suppliers.ToDictionary(x => x.SupplierId.ToGuidId(), y => y.SupplierName);
        }

        bool IsUniqueEmail(IDocumentSession systemSession, string email, Guid userKey)
        {
            bool exists = false;
            if (string.IsNullOrEmpty(email)) return false;

            exists = systemSession.Query<UserIndex.Result, UserIndex>()
                .Any(c => c.Email == email && c.UserKey != userKey);

            return !exists;
        }


    }
}
