﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using nTrack.Data.Extenstions;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Commands;
using PalletPlus.Website.Areas.Admin.ViewModels.ImportSupplierInvoiceData;
using PalletPlus.Website.Services;
using Raven.Client;

namespace PalletPlus.Website.Areas.Admin.Controllers
{
    public class ImportSupplierInvoiceController : AdminBaseController
    {
        readonly IDocumentSession _session;
        readonly IAttachmentService _attachmentService;

        public ImportSupplierInvoiceController(IDocumentSession session, IThreadingService threadingService, IAttachmentService attachmentService)
            : base(threadingService)
        {
            _session = session;
            _attachmentService = attachmentService;
        }

        [GET("SupplierInvoices")]
        public ActionResult Index()
        {
            var vm = new ImportSupplierInvoiceViewModel();

            vm.Suppliers = _session.Query<Supplier, SupplierIndex>()
                .Where(x => x.ProviderName != null)
                .ToDictionary(x => x.Id, y => y.CompanyName);

            //TODO: page the results
            var invoices = _session.Query<SupplierInvoice, SupplierInvoiceIndex>()
                                   .OrderByDescending(x => x.InvoiceDate)
                                   .As<ImportedInvoiceVM>()
                                   .ToArray();

            vm.PreviousInvoices = invoices;

            return View(vm);
        }

        [POST("ImportInvoice")]
        public ActionResult ImportInvoice(IEnumerable<HttpPostedFileBase> files, ImportSupplierInvoiceViewModel model)
        {
            var supplier = _session.Load<Supplier>(model.SupplierId);
            if (supplier == null)
                return HttpNotFound("Supplier was not found!");

            if (files != null)
            {
                foreach (var f in files)
                {
                    //upload the file to database
                    var attachment = new InvoiceAttachment
                        {
                            Id = Guid.NewGuid(),
                            FileName = f.FileName,
                            SupplierId = model.SupplierId.ToStringId<Supplier>(),
                            UploadedOn = DateTime.Now
                        };
                    _session.Store(attachment);
                    _attachmentService.PutAttachment(attachment.Id.ToStringId<InvoiceAttachment>(), f.InputStream);

                    //send the import invoice command
                    Bus.Send<ImportInvoice>(cmd =>
                        {
                            cmd.Id = Guid.NewGuid();
                            cmd.AttachmentId = attachment.Id;
                            cmd.SupplierId = model.SupplierId;
                            cmd.ProviderName = supplier.ProviderName;
                        });


                    Success();
                }
            }

            return RedirectToAction("Index");
        }


        //void AutomaticReconciliation(InvoiceAttachment invoiceAttachment)
        //{
        //    #region Get&Set Invoice Details and Movements

        //    var supplierUnreconciledMovements = _session.Query<MovementIndex.Result, MovementIndex>()
        //        .Where(x => x.MovementType == "Transfer" && !x.IsReconciled && x.SupplierId == invoiceAttachment.SupplierId)
        //        .ToList();

        //    var tradingPartnerNamesAccountNumbers = _session.Query<DestinationNamesAccountNumbersIndex.Result, DestinationNamesAccountNumbersIndex>()
        //            .ToList()
        //            .Select(x => new PartnerDetails(x.CompanyId, x.CompanyName, x.AccountNumber))
        //            .ToList();

        //    var equipmentNames = _session.Query<EquipmentIndex.Result, EquipmentIndex>()
        //        .Where(x => x.SupplierId == invoiceAttachment.SupplierId)
        //        .ToDictionary(x => x.EquipmentCode, y => y.EquipmentName);

        //    var unreconciledMovements = (from movement in supplierUnreconciledMovements
        //                                 from equipment in movement.Equipments
        //                                 let destinationData = tradingPartnerNamesAccountNumbers.FirstOrDefault(x => x.CompanyId == movement.DestinationId)
        //                                 select new ReconciliationLineModel()
        //                                            {
        //                                                MovementId = movement.MovementId,
        //                                                DocketNumber = movement.DocketNumber,
        //                                                TradingPartnerId = movement.LocationKey.HasValue ? movement.LocationKey.Value.ToString() : movement.DestinationId,
        //                                                MovementDate = movement.MovementDate.ToShortDateString(),
        //                                                EquipmentCode = equipment.Key,
        //                                                Quantity = equipment.Value.Quantity,
        //                                                EquipmentName = equipmentNames[equipment.Key],
        //                                                MovementKind = movement.MovementType,
        //                                                TradingPartnerName = destinationData != null ? destinationData.Name : string.Empty,
        //                                                TradingPartnerAccountNumber = destinationData != null ? destinationData.AccountNumber : string.Empty,
        //                                                IsLocation = movement.LocationKey.HasValue
        //                                            }).ToList();

        //    #endregion

        //    var invoiceDetailsToReconcile = new Dictionary<Guid, string>();    //<DetailKeys, MovementID>
        //    var movementsToReconcile = new Dictionary<string, List<string>>(); //<MovementID, List<EquipmentCodes>>

        //    foreach (var item in invoiceAttachment.Details)
        //    {
        //        foreach (var movement in unreconciledMovements)
        //        {
        //            //check if all needed properties match, if they do, reconcile movement with invoice item
        //            if (item.TRANSDATE.HasValue)
        //            {
        //                if (item.TRANSDATE.Value.ToShortDateString() != movement.MovementDate)
        //                    continue;
        //                if (item.PCMSDOCKET != movement.DocketNumber)
        //                    continue;
        //                if (item.OTHERPARTY != movement.TradingPartnerAccountNumber)
        //                    continue;
        //                if (item.EQUIPCODE != movement.EquipmentCode)
        //                    continue;
        //                if (CompareTransCodeAndMovementType(item.TRANSCODE, movement.MovementType, movement.MovementKind))
        //                    continue;
        //                if (item.QUANTITY != movement.Quantity.ToString())
        //                    continue;

        //                invoiceDetailsToReconcile.Add(item.DetailKey, movement.MovementId);
        //                if (!movementsToReconcile.ContainsKey(movement.MovementId))
        //                    movementsToReconcile.Add(movement.MovementId, new List<string> { movement.EquipmentCode });
        //                else
        //                {
        //                    movementsToReconcile[movement.MovementId].Add(movement.EquipmentCode);
        //                }
        //                break;
        //            }
        //        }
        //    }

        //    if (invoiceDetailsToReconcile.Any() && movementsToReconcile.Any() && invoiceDetailsToReconcile.Count == movementsToReconcile.Count)
        //    {
        //        Bus.Send<ReconcileMovements>(cmd =>
        //                                         {
        //                                             cmd.InvoiceId = invoiceAttachment.Id;
        //                                             cmd.InvoiceDetailsToReconcile = invoiceDetailsToReconcile;
        //                                             cmd.MovementDetailsToReconcile = movementsToReconcile;
        //                                         });
        //    }
        //}
        //void SetInvoiceDetails(InvoiceHeader header, List<Data.InvoiceDetail> details)
        //{
        //    var equipmentNames = _session.Query<EquipmentIndex.Result, EquipmentIndex>()
        //        .ToDictionary(x => x.EquipmentCode, y => y.EquipmentName);

        //    details.ForEach(action =>
        //    {
        //        action.DetailKey = Guid.NewGuid();
        //        if (string.IsNullOrEmpty(action.NARRATION) && equipmentNames.ContainsKey(action.EQUIPCODE))
        //        {
        //            action.NARRATION = equipmentNames[action.EQUIPCODE];
        //        }
        //        else
        //        {
        //            action.NARRATION = action.EQUIPCODE;
        //        }
        //        action.QUANTITY = Math.Abs(Convert.ToInt32(action.QUANTITY.Split('.')[0])).ToString();
        //        //action.OTHERPARTY = action.OTHERPARTYGLID;
        //        action.INVOICENO = header.INVOICENO;
        //        action.INVOICEDATE = header.INVOICEDATE;
        //    });
        //}
        //bool CompareTransCodeAndMovementType(string transCode, string movementType, string movementDirection)
        //{
        //    var movementKind = string.Format("{0} {1}", movementType, movementDirection);
        //    if (transCode == "Transfer Out" || transCode == "Return")
        //        return "Transfer Out" == movementKind;
        //    if (transCode == "Transfer In" || transCode == "Issue")
        //        return "Transfer In" == movementKind;

        //    return false;
        //}
    }
}