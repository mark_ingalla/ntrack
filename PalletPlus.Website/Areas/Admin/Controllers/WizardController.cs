﻿using System;
using System.Linq;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using Kendo.Mvc.Extensions;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Commands.Account;
using PalletPlus.Messages.Commands.Partner;
using PalletPlus.Messages.Commands.Site;
using PalletPlus.Messages.Commands.Supplier;
using PalletPlus.Services;
using PalletPlus.Website.Areas.Admin.ViewModels.Partner;
using PalletPlus.Website.Areas.Admin.ViewModels.Site;
using PalletPlus.Website.Areas.Admin.ViewModels.Supplier;
using PalletPlus.Website.Areas.Admin.ViewModels.Wizard;
using PalletPlus.Website.Services;
using Raven.Client;
using Raven.Client.Linq;
using StructureMap;
using nTrack.Data;
using nTrack.Data.Extenstions;
using nTrack.Extension;
using Account = PalletPlus.Data.Account;

namespace PalletPlus.Website.Areas.Admin.Controllers
{
    public class WizardController : AdminBaseController
    {

        readonly IDocumentSession _session;

        public WizardController(IDocumentSession session, IThreadingService threadingService)
            : base(threadingService)
        {
            _session = session;
        }

        [GET("Wizard")]
        public ActionResult Index()
        {
            return RedirectToAction("CreateSite");
        }

        [GET("Wizard/CreateSite")]
        public ActionResult CreateSite()
        {
            Session["wizardProgress"] = 0;
            var vm = new SiteViewModel();

            vm.ComboboxCountries = GetCountries();
            return View(vm);
        }

        [POST("Wizard/CreateSite")]
        public ActionResult CreateSite(SiteViewModel site)
        {
            var siteEntity = _session.Query<Site>().FirstOrDefault(x => x.CompanyName == site.SiteName);
            if (siteEntity != null)
            {
                Failure("A site with that name already exists.");
                site.SiteName = string.Empty;
                return View(site);
            }

            site.SiteId = Guid.NewGuid();
            Bus.SendMap<CreateSite>(site);

            ThreadingService.ObjectCreated<Site>(site.SiteId);
            RecordCreated("details");

            return View("CreateSiteSuccess");
        }

        [GET("Wizard/CorrectionReason")]
        public ActionResult CorrectionReason()
        {
            var account = _session.Load<Account>(Identity.AccountId);
            var vm = new CorrectionReasonVM(null, account.CorrectionReasons, "correction");
            Session["wizardProgress"] = 25;
            ViewBag.PreviousLink = Url.Action("CreateSite");
            ViewBag.NextLink = Url.Action("CancellationReason");
            return View(vm);
        }

        [POST("Wizard/AddReason")]
        public ActionResult AddReason(CorrectionReasonVM utility)
        {
            if (!string.IsNullOrEmpty(utility.Reason))
            {
                Bus.Send<AddCorrectionReason>(cmd =>
                {
                    cmd.ReasonKey = Guid.NewGuid();
                    cmd.AccountId = Identity.AccountId;
                    cmd.Reason = utility.Reason;
                });

                RecordUpdated("reasons");
            }

            return RedirectToAction("CorrectionReason");
        }

        [POST("Wizard/DeleteReason/{id}")]
        public ActionResult DeleteReason(Guid id)
        {
            Bus.Send<RemoveCorrectionReason>(cmd =>
            {
                cmd.AccountId = Identity.AccountId;
                cmd.ReasonKey = id;
            });

            RecordUpdated("reasons");

            return RedirectToAction("CorrectionReason");
        }

        [GET("Wizard/ReasonIsUnique")]
        public JsonResult ReasonIsUnique(CorrectionReasonVM vm)
        {
            if (vm.Type == "correction")
                return Json(!_session.Load<Account>(Identity.AccountId).CorrectionReasons.ContainsValue(vm.Reason),
                        JsonRequestBehavior.AllowGet);
            else
            {
                return Json(!_session.Load<Account>(Identity.AccountId).CancellationReasons.ContainsValue(vm.Reason),
                        JsonRequestBehavior.AllowGet);
            }
        }

        [GET("Wizard/CancellationReason")]
        public ActionResult CancellationReason()
        {
            var account = _session.Load<Account>(Identity.AccountId);
            var vm = new CorrectionReasonVM(null, account.CancellationReasons, "cancellation");
            Session["wizardProgress"] = 25;
            ViewBag.PreviousLink = Url.Action("CorrectionReason");
            ViewBag.NextLink = Url.Action("CreateSupplier");
            return View(vm);
        }

        [POST("Wizard/AddCancellationReason")]
        public ActionResult AddCancellationReason(CorrectionReasonVM utility)
        {
            if (!string.IsNullOrEmpty(utility.Reason))
            {
                Bus.Send<AddCancellationReason>(cmd =>
                {
                    cmd.ReasonKey = Guid.NewGuid();
                    cmd.AccountId = Identity.AccountId;
                    cmd.Reason = utility.Reason;
                });

                RecordUpdated("cancellation-reasons");
            }

            return RedirectToAction("CancellationReason");
        }

        [GET("Wizard/RemoveCancellationReason/{id}")]
        public ActionResult RemoveCancellationReason(Guid id)
        {
            Bus.Send<RemoveCancellationReason>(cmd =>
            {
                cmd.AccountId = Identity.AccountId;
                cmd.ReasonKey = id;
            });

            RecordUpdated("cancellation-reasons");

            return RedirectToAction("CancellationReason");
        }

        [GET("Wizard/CreateSupplier")]
        public ActionResult CreateSupplier()
        {
            var vm = new SupplierViewModel();

            vm.ComboboxCountries = GetCountries();

            var serviceInstances = ObjectFactory.GetAllInstances<IExportProvider>();
            if (serviceInstances != null)
                vm.ExportProviderNames.AddRange(serviceInstances.ToDictionary(x => x.Name, y => y.Name));

            ViewBag.PreviousLink = Url.Action("CorrectionReason");
            return View(vm);
        }

        [POST("Wizard/CreateSupplier")]
        public ActionResult CreateSupplier(SupplierViewModel supplier)
        {
            var supplierEntity = _session.Query<Supplier>().FirstOrDefault(x => x.CompanyName == supplier.SupplierName);
            if (supplierEntity != null)
            {
                Failure("A supplier with that name already exists.");
                supplier.SupplierName = string.Empty;
                return View(supplier);
            }

            supplier.SupplierId = Guid.NewGuid();
            Bus.SendMap<CreateSupplier>(supplier);

            ThreadingService.ObjectCreated<Supplier>(supplier.SupplierId);
            RecordCreated("details");

            return RedirectToAction("AddEquipment", "Wizard", new { id = supplier.SupplierId });
        }

        [GET("Wizard/AddEquipment/{id}")]
        public ActionResult AddEquipment(Guid id)
        {
            var supplier = _session.Load<Supplier>(id);
            var vm = Mapper.Map<SupplierEditViewModel>(supplier);

            vm.ComboboxCountries = GetCountries();

            var serviceInstances = ObjectFactory.GetAllInstances<IExportProvider>();
            if (serviceInstances != null)
                vm.ProviderNames.AddRange(serviceInstances.ToDictionary(x => x.Name, y => y.Name));

            if (supplier.Equipments.Count > 0)
                ViewBag.NextLink = Url.Action("AddLocation", new {id = id});

            return View(vm);
        }


        [POST("Wizard/AddEquipment/{id}")]
        public ActionResult AddEquipment(SupplierEditViewModel supplier)
        {
            var now = Bus.SendMap<AddEquipmentToSupplier>(
                cmd =>
                {
                    cmd.SupplierId = supplier.SupplierId;
                    cmd.EquipmentCode = supplier.EquipmentVM.EquipmentCode;
                    cmd.EquipmentName = supplier.EquipmentVM.EquipmentName;
                });

            ThreadingService.ObjectUpdated<Supplier>(supplier.SupplierId, now);
            RecordUpdated("equipment");

            return RedirectToAction("AddEquipment", new { id = supplier.SupplierId });
        }

        [GET("Wizard/AddLocation/{id}")]
        public ActionResult AddLocation(Guid id)
        {
            var supplier = _session.Load<Supplier>(id);
            var vm = Mapper.Map<SupplierEditViewModel>(supplier);

            vm.ComboboxCountries = GetCountries();

            var serviceInstances = ObjectFactory.GetAllInstances<IExportProvider>();
            if (serviceInstances != null)
                vm.ProviderNames.AddRange(serviceInstances.ToDictionary(x => x.Name, y => y.Name));

            ViewBag.PreviousLink = Url.Action("AddEquipment", new {id});

            if (supplier.Locations.Count > 0)
                ViewBag.NextLink = Url.Action("SupplierSuccess");

            return View(vm);
        }


        [POST("Wizard/AddLocation/{id}")]
        public ActionResult AddLocation(SupplierEditViewModel supplier)
        {
            var now = Bus.SendMap<AddLocationToSupplier>(
                cmd =>
                {
                    cmd.SupplierId = supplier.SupplierId;
                    cmd.LocationKey = Guid.NewGuid();
                    cmd.Name = supplier.LocationVM.LocationName;
                    cmd.AccountNumber = supplier.LocationVM.AccountNumber;
                    cmd.AddressLine1 = supplier.LocationVM.AddressLine1;
                    cmd.AddressLine2 = supplier.LocationVM.AddressLine2;
                    cmd.Postcode = supplier.LocationVM.Postcode;
                    cmd.Country = supplier.LocationVM.Country;
                    cmd.State = supplier.LocationVM.State;
                    cmd.Suburb = supplier.LocationVM.Suburb;
                    cmd.ContactEmail = supplier.LocationVM.ContactEmail;
                    cmd.ContactName = supplier.LocationVM.ContactName;
                });

            ThreadingService.ObjectUpdated<Supplier>(supplier.SupplierId, now);
            RecordUpdated("locations");

            return RedirectToAction("AddLocation", new { id = supplier.SupplierId });
        }

        [GET("Wizard/SupplierSuccess")]
        public ActionResult SupplierSuccess()
        {
            return View();
        }

        [GET("Wizard/CreatePartner")]
        public ActionResult CreatePartner()
        {
            var vm = new PartnerViewModel();

            vm.ComboboxCountries = GetCountries();

            ViewBag.PreviousLink = Url.Action("CreateSupplier");
            return View(vm);
        }

        [POST("Wizard/CreatePartner")]
        public ActionResult CreatePartner(PartnerViewModel partner)
        {
            var partnerEntity = _session.Query<Partner>().FirstOrDefault(x => x.CompanyName == partner.CompanyName);
            if (partnerEntity != null)
            {
                partner.ComboboxCountries = GetCountries();
                Failure("A partner with that name already exists.");
                partner.CompanyName = string.Empty;
                return View(partner);
            }

            partner.PartnerId = Guid.NewGuid();
            Bus.SendMap<CreatePartner>(partner);

            ThreadingService.ObjectCreated<Partner>(partner.PartnerId);

            RecordCreated("details");

            return RedirectToAction("PartnerSuppliers", new {id = partner.PartnerId});
        }

        [GET("Wizard/PartnerSuppliers")]
        public ActionResult PartnerSuppliers(Guid id)
        {
            var partner = _session.Load<Partner>(id);
            var vm = new PartnerEditViewModel();
            ViewBag.NextLink = Url.Action("PartnerSuccess");

            if (partner != null)
            {
                vm = Mapper.Map<PartnerEditViewModel>(partner);
                GetSuppliers(ref vm, partner);
                vm.ComboboxCountries = GetCountries();

                return View(vm);
            }


            Failure("Couldn't load entity.");
            return View(vm);
        }

        private void GetSuppliers(ref PartnerEditViewModel viewModel, Partner partner)
        {
            var alreadyAdded = partner.Suppliers;

            var suppliers = _session.Query<Supplier>(typeof(SupplierIndex).Name)
                .ToDictionary(x => x.Id, y => y);

            //NOTE: Not so sure about this, and how to solve this (because we access suppliers[Key] 
            //NOTE: not knowing if it's null. Shall we create index for them?
            var partnerSuppliers = alreadyAdded
                .Select(x => new PartnerSupplierViewModel
                {
                    IsDeleted = suppliers[x.Key.ToGuidId()].IsDeleted,
                    SupplierId = x.Key.ToGuidId(),
                    SupplierName = suppliers[x.Key.ToGuidId()].CompanyName,
                    ContactEmail = suppliers[x.Key.ToGuidId()].ContactEmail,
                    SupplierAccountNo = x.Value.AccountNumber,
                    PartnerId = partner.Id,
                    Equipment = suppliers[x.Key.ToGuidId()].Equipments.Where(y => !y.Value.IsDeleted)
                    .ToDictionary(y => y.Value.EquipmentCode, z => z.Value.EquipmentName),
                    EquipmentAvailable = x.Value.Equipments
                }).ToList();


            var inSuppliers = alreadyAdded.Select(x => x.Key).ToList();
            var comboboxSuppliers = suppliers
                .Where(x => !x.Value.IsDeleted && !x.Value.IdString.In(inSuppliers))
                .Select(x => new { Id = x.Value.Id, SupplierName = x.Value.CompanyName }).ToList();

            viewModel.SuppliersVM = partnerSuppliers;
            viewModel.ComboboxSuppliers = comboboxSuppliers;
        }

        [POST("Wizard/PartnerSuppliers")]
        public ActionResult PartnerSuppliers(PartnerEditViewModel partner)
        {
            if (!ValidateAccountNumberUniquiness(partner.SupplierVM.SupplierAccountNo))
                return RedirectToAction("PartnerSuppliers", new { id = partner.PartnerId });

            var now = Bus.SendMap<AddSupplierToPartner>(partner);

            ThreadingService.ObjectUpdated<Partner>(partner.PartnerId, now);
            RecordUpdated("suppliers");

            return RedirectToAction("PartnerSuppliers", new { id = partner.PartnerId });
        }

        [GET("Wizard/DeletePartnerSupplier")]
        public ActionResult DeletePartnerSupplier(Guid id, Guid supplierId)
        {
            var partner = _session.Load<Partner>(id);
            if (partner != null)
            {
                if (partner.Suppliers.ContainsKey(supplierId.ToStringId<Supplier>()))
                {
                    var supplier = partner.Suppliers[supplierId.ToStringId<Supplier>()];
                    var assets = supplier.Equipments;

                    Bus.Send<DisallowEquipmentToPartner>(
                        cmd =>
                        {
                            cmd.PartnerId = id;
                            cmd.SupplierId = supplierId;
                            cmd.Equipments = assets.ToList();
                        });

                    var now = Bus.SendMap<RemoveSupplierFromPartner>(
                        cmd =>
                        {
                            cmd.PartnerId = id;
                            cmd.SupplierId = supplierId;
                        });

                    ThreadingService.ObjectUpdated<Partner>(id, now);
                }
            }

            RecordUpdated("suppliers");

            return RedirectToAction("PartnerSuppliers", new { id });
        }

        private bool ValidateAccountNumberUniquiness(string accountNumber)
        {
            var accountNumbers = _session.Query<AccountNumbersIndex.Result, AccountNumbersIndex>();
            var accoutnNumber = accountNumbers.FirstOrDefault(x => x.AccountNumber == accountNumber);
            if (accoutnNumber != null)
            {
                Failure("suppliers", "Account number already in use.");
                return false;
            }
            return true;
        }

        [GET("Wizard/PartnerSuccess")]
        public ActionResult PartnerSuccess()
        {
            return View();
        }

        [GET("Wizard/Linking")]
        public ActionResult Linking(bool? previous)
        {
            var sites = _session.Query<Site>().Where(p => p.AccountId == Identity.AccountId.ToStringId<Account>()).ToList();

            if (Session["linkCount"] == null)
                Session["linkCount"] = -1;

            if (previous == null || !previous.Value)
            {
                if ((int) Session["linkCount"] == sites.Count - 1)
                    return RedirectToAction("Stage2");

                Session["linkCount"] = ((int) Session["linkCount"]) + 1;
            }
            else
            {
                Session["linkCount"] = ((int)Session["linkCount"]) - 1;

                if ((int)Session["linkCount"] == -1)
                    return RedirectToAction("CreatePartner");

            }

            return RedirectToAction("SupplierToSite", new {id = sites[(int) Session["linkCount"]].Id});
        }

        [GET("Wizard/{id}/SupplierToSite")]
        public ActionResult SupplierToSite(Guid id)
        {
            var site = _session.Load<Site>(id);
            var vm = new SiteEditViewModel();

            ViewBag.NextLink = Url.Action("Linking");
            ViewBag.PreviousLink = Url.Action("Linking", new { previous = true });

            if (site != null)
            {
                vm = Mapper.Map<SiteEditViewModel>(site);
                GetSuppliers(ref vm, site);
                vm.ComboboxCountries = GetCountries();

                return View(vm);
            }


            Failure("Couldn't load entity.");
            return View(vm);
        }


        [POST("Wizard/{id}/AddSupplier")]
        public ActionResult AddSupplierToSite(SiteEditViewModel site)
        {
            if (!ValidateAccountNumberUniquiness(site.SupplierVM.AccountNumber))
                return RedirectToAction("SupplierToSite", new { id = site.SiteId });

            site.SupplierVM.SupplierName = GetEntityName<Supplier>(site.SupplierVM.SupplierId.ToStringId<Supplier>());
            var now = Bus.SendMap<AddSupplierToSite>(site);

            ThreadingService.ObjectUpdated<Site>(site.SiteId, now);
            RecordUpdated("suppliers");

            return RedirectToAction("SupplierToSite", new { id = site.SiteId });
        }

        [POST("Wizard/{id}/EditSupplier")]
        public ActionResult EditSupplierToSite(SiteSupplierEditViewModel site)
        {
            var now = Bus.SendMap<UpdateSiteSupplier>(site);

            ThreadingService.ObjectUpdated<Site>(site.SiteId, now);
            RecordUpdated("suppliers");

            return RedirectToAction("SupplierToSite", new { id = site.SiteId });
        }

        [POST("Wizard/{id}/OverrideSupplier")]
        public ActionResult OverrideSupplierToSite(SiteSupplierEditViewModel site)
        {
            if (!ValidateAccountNumberUniquiness(site.SupplierAccountNumber))
                return RedirectToAction("SupplierToSite", new { id = site.SiteId });

            var now = Bus.SendMap<AddSupplierToSite>(site);

            ThreadingService.ObjectUpdated<Site>(site.SiteId, now);
            RecordUpdated("suppliers");

            return RedirectToAction("SupplierToSite", new { id = site.SiteId });
        }

        [GET("Wizard/{id}/DeleteSupplier/{supplierId}")]
        public ActionResult DeleteSupplierToSite(Guid id, Guid supplierId)
        {
            var now = Bus.SendMap<RemoveSupplierFromSite>(
                cmd =>
                {
                    cmd.SiteId = id;
                    cmd.SupplierId = supplierId;
                });

            ThreadingService.ObjectUpdated<Site>(id, now);
            RecordUpdated("suppliers");

            return RedirectToAction("SupplierToSite", new { id });
        }

        [GET("Wizard/{id}/SupplierToSite")]
        public ActionResult PartnerToSite(Guid id)
        {
            var site = _session.Load<Site>(id);
            var vm = new SiteEditViewModel();

            ViewBag.NextLink = Url.Action("Linking");
            ViewBag.PreviousLink = Url.Action("Linking", new { previous = true });

            if (site != null)
            {
                vm = Mapper.Map<SiteEditViewModel>(site);
                GetSuppliers(ref vm, site);
                vm.ComboboxCountries = GetCountries();

                return View(vm);
            }


            Failure("Couldn't load entity.");
            return View(vm);
        }

        //TODO: fix this->sarmaad
        string GetEntityName<T>(string id)
        {
            var entity = _session.Load<T>(id);
            if (entity != null)
            {
                if (entity is ICompany)
                    return (entity as ICompany).CompanyName;
                if (entity is Supplier)                         //this is not nice, but just for now until we clear things out with the suppliers/locations
                    return (entity as Supplier).CompanyName;
            }

            return string.Empty;
        }

        void GetSuppliers(ref SiteEditViewModel viewModel, Site site)
        {
            var alreadyAdded = site.Suppliers.Select(x => x.Key).ToList();

            var suppliers = _session.Query<Supplier>(typeof(SupplierIndex).Name)
                .Select(x => new { IsDeleted = x.IsDeleted, SupplierId = x.IdString, SupplierName = x.CompanyName })
                .ToList();

            var comboboxSuppliers = suppliers
                .Where(x => !x.IsDeleted && !x.SupplierId.In(alreadyAdded))
                .Select(x => new { Id = x.SupplierId.ToGuidId(), SupplierName = x.SupplierName }).ToList();

            var account = _session.Load<Account>(Identity.AccountId);
            if (account != null)
            {
                var accountSuppliers = account.Suppliers.Values.Select(x => new SiteSupplierViewModel(x.CompanyId.ToGuidId(),
                    x.CompanyName, x.AccountNumber, x.Prefix, x.Suffix, x.SequenceNumber, true));

                var siteSuppliersIds = viewModel.SuppliersVM.Select(x => x.SupplierId).ToList();
                var suppliersInheritedFromAccount = accountSuppliers.Where(x => !x.SupplierId.In(siteSuppliersIds)).ToList();
                viewModel.SuppliersVM.AddRange(suppliersInheritedFromAccount);
            }

            viewModel.ComboboxSuppliers = comboboxSuppliers;
            viewModel.SupplierNames = suppliers.ToDictionary(x => x.SupplierId.ToGuidId(), y => y.SupplierName);
        }

        [GET("Wizard/Stage2")]
        public ActionResult Stage2()
        {
            var vm = new SiteViewModel();

            vm.ComboboxCountries = GetCountries();
            Session["wizardProgress"] = 50;
            return View(vm);
        }
    }
}
