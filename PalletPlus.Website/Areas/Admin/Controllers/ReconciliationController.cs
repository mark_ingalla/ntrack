﻿using System;
using System.Linq;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using nTrack.Data.Extenstions;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Commands.Invoice;
using PalletPlus.Messages.Commands.Partner;
using PalletPlus.Messages.Commands.Site;
using PalletPlus.Website.Areas.Admin.ViewModels.ImportSupplierInvoiceData;
using PalletPlus.Website.Areas.Admin.ViewModels.Reconciliation;
using PalletPlus.Website.Services;
using Raven.Client;
using Raven.Client.Linq;

namespace PalletPlus.Website.Areas.Admin.Controllers
{

    public class ReconciliationController : AdminBaseController
    {
        readonly IDocumentSession _session;

        public ReconciliationController(IDocumentSession session, IThreadingService threadingService)
            : base(threadingService)
        {
            _session = session;
        }

        [GET("Reconciliation")]
        public ActionResult Index()
        {
            var vm = new ImportSupplierInvoiceViewModel();

            vm.Suppliers = _session.Query<Supplier, SupplierIndex>()
                .ToDictionary(x => x.Id, y => y.CompanyName);

            var invoices = _session.Query<SupplierInvoice, SupplierInvoiceIndex>()
                .Where(x => !x.IsReconciled)
                .As<ImportedInvoiceVM>()
                .ToArray();

            vm.PreviousInvoices = invoices;

            return View(vm);
        }

        [GET("Reconcile/{id}")]
        public ActionResult Reconcile(Guid id)
        {
            var invoice = _session.Load<SupplierInvoice>(id);
            //TODO: improve not found handling in UI
            if (invoice == null)
                return HttpNotFound();


            var accountNumbers = invoice.Details.Select(x => x.DestinationAccountNumber).ToArray();
            var destinations = _session
                .Query<DestinationNamesAccountNumbersIndex.Result, DestinationNamesAccountNumbersIndex>()
                .Where(x => x.AccountNumber.In(accountNumbers))
                .ToList();

            //var invoiceDetails = invoice.Details.Where(x => !x.IsReconciled).ToList();

            var vm = new ReconciliationViewModel(invoice, destinations.ToDictionary(x => x.AccountNumber, y => y.CompanyName));

            return View(vm);
        }

        [POST("Reconcile/{id}/InvoiceItem/{key}")]
        public ActionResult GetInvoiceItem(Guid id, Guid key)
        {
            var invoice = _session.Load<SupplierInvoice>(id);
            if (invoice != null)
            {
                var detail = invoice.Details.FirstOrDefault(x => x.Key == key);
                return
                    Json(new
                        {
                            detail.Key,
                            detail.MovementDate,
                            detail.DocketNumber,
                            detail.DestinationAccountNumber,
                            detail.EquipmentCode,
                            detail.TransactionCode,
                            detail.Qty,
                            detail.DestinationRef
                        });
            }
            return null;
        }

        [POST("Reconcile/{id}/Movements/{supplierId}")]
        public ActionResult GetMovements(Guid id, string supplierId,
                                         string movementDate, string docketNumber, string destinationAccountNumber,
                                         string equipmentCode, string transactionCode, string destinationRef,
                                         [DataSourceRequest] DataSourceRequest request)
        {

            if (string.IsNullOrWhiteSpace(movementDate) &&
                string.IsNullOrWhiteSpace(docketNumber) &&
                string.IsNullOrWhiteSpace(destinationAccountNumber) &&
                string.IsNullOrWhiteSpace(equipmentCode) &&
                string.IsNullOrWhiteSpace(transactionCode) &&
                string.IsNullOrWhiteSpace(destinationRef))
            {
                return null;
            }

            var query = _session.Query<MovementIndex.Result, MovementIndex>()
                                .Where(x => !x.IsReconciled && !x.IsPending && x.MovementType == "Transfer");

            if (!string.IsNullOrWhiteSpace(movementDate))
                query = query.Where(x => x.MovementDate == DateTime.Parse(movementDate));

            if (!string.IsNullOrEmpty(docketNumber))
                query = query.Where(x => x.DocketNumber == docketNumber);

            if (!string.IsNullOrEmpty(destinationAccountNumber))
                query = query.Where(x => x.DestinationAccountNumber == destinationAccountNumber);

            if (!string.IsNullOrEmpty(equipmentCode))
                query = query.Where(x => x.EquipmentKeys.Contains(equipmentCode));

            if (!string.IsNullOrEmpty(transactionCode))
            {
                switch (transactionCode)
                {
                    case "Sent":
                        query = query.Where(x => x.DestinationKind == "Partner" && x.Direction == "Sent");
                        break;
                    case "Received":
                        query = query.Where(x => x.DestinationKind == "Partner" && x.Direction == "Received");
                        break;
                    case "Issue":
                        query = query.Where(x => x.DestinationKind == "Supplier" && x.Direction == "Received");
                        break;
                    case "Return":
                        query = query.Where(x => x.DestinationKind == "Supplier" && x.Direction == "Sent");
                        break;
                }
            }

            if (!string.IsNullOrEmpty(destinationRef))
                query = query.Where(x => x.DestinationReference == destinationRef);

            var results = query.Take(10).ToList();

            var movements = (from r in results
                             from e in r.Equipment
                             select new
                                 {
                                     r.MovementId,
                                     MovementDate = r.MovementDate.ToShortDateString(),
                                     r.DocketNumber,
                                     r.DestinationAccountNumber,
                                     r.DestinationName,
                                     r.DestinationReference,
                                     EquipmentCode = e.Key,
                                     Qty = e.Value.Quantity,
                                     TransactionCode = TranslateFromMovementToTransactionCode(r.DestinationKind, r.Direction)

                                 }).ToDataSourceResult(request);
            return Json(movements);

            //supplierId = "suppliers/" + supplierId;

            //#region Get&Set Invoice Details and Movements

            //var whereQuery = _session.Query<MovementIndex.Result, MovementIndex>()
            //                         .Where(x => x.MovementType == "Transfer" && !x.IsReconciled);


            //if (!string.IsNullOrEmpty(docketNumber))
            //    whereQuery = whereQuery.Where(x => x.DocketNumber == docketNumber);
            //if (!string.IsNullOrEmpty(movementDirection))
            //    whereQuery = whereQuery.Where(x => x.Direction == movementDirection);

            //var supplierUnreconciledMovements = whereQuery.ToList();

            //var tradingPartnerNamesAccountNumbers =
            //    _session.Query<DestinationNamesAccountNumbersIndex.Result, DestinationNamesAccountNumbersIndex>()
            //            .ToList()
            //            .Select(x => new PartnerDetails(x.CompanyId, x.CompanyName, x.AccountNumber))
            //            .ToList();

            //var equipmentNames = _session.Query<EquipmentIndex.Result, EquipmentIndex>()
            //                             .Where(x => x.SupplierId == supplierId)
            //                             .ToDictionary(x => x.EquipmentCode, y => y.EquipmentName);

            //var unreconciledMovements = (from movement in supplierUnreconciledMovements
            //                             from equipment in movement.Equipments
            //                             where !movement.ReconciledEquipment.ContainsKey(equipment.Key)
            //                             let destinationData =
            //                                 tradingPartnerNamesAccountNumbers.FirstOrDefault(x => x.CompanyId == movement.DestinationId)
            //                             select new ReconciliationLineModel()
            //                                 {
            //                                     MovementId = movement.MovementId,
            //                                     DocketNumber = movement.DocketNumber,
            //                                     TradingPartnerId =
            //                                         movement.LocationKey.HasValue ? movement.LocationKey.Value.ToString() : movement.DestinationId,
            //                                     MovementDate = movement.MovementDate.ToShortDateString(),
            //                                     EquipmentCode = equipment.Key,
            //                                     Quantity = equipment.Value.Quantity,
            //                                     EquipmentName = equipmentNames[equipment.Key],
            //                                     MovementKind = movement.MovementType,
            //                                     MovementDirection = movement.Direction,
            //                                     TradingPartnerName = destinationData != null ? destinationData.Name : string.Empty,
            //                                     TradingPartnerAccountNumber = destinationData != null ? destinationData.AccountNumber : string.Empty,
            //                                     IsLocation = movement.LocationKey.HasValue
            //                                 }).ToList();

            //if (!string.IsNullOrEmpty(equipmentCode))
            //    unreconciledMovements = unreconciledMovements
            //        .Where(x => x.EquipmentCode == equipmentCode)
            //        .ToList();

            //if (!string.IsNullOrEmpty(tradingPartnerAccountNumber))
            //    unreconciledMovements = unreconciledMovements
            //        .Where(x => x.TradingPartnerAccountNumber == tradingPartnerAccountNumber)
            //        .ToList();

            //#endregion

            //var xx = unreconciledMovements.ToDataSourceResult(request);
            //return Json(xx);
        }

        string TranslateFromMovementToTransactionCode(string destinationKind, string direction)
        {
            switch (destinationKind)
            {
                case "Partner":
                case "Site":
                    return direction;

                case "Supplier":
                    if (direction == "Received")
                        return "Issue";
                    if (direction == "Sent")
                        return "Return";

                    break;
            }

            return "Unknown";
        }

        [POST("Reconciliation/ViewDetails/{id}/MatchRecords")]
        public ActionResult MatchRecords(Guid invoiceId, Guid detailKey, string movementEquipmentCode, string movementId, string movementDate, string docketNumber,
            string tradingPartnerAccountNumber, string equipmentCode, string movementDirection, int quantity)
        {
            var tradingPartner =
                _session.Query<DestinationNamesAccountNumbersIndex.Result, DestinationNamesAccountNumbersIndex>()
                .FirstOrDefault(x => x.AccountNumber == tradingPartnerAccountNumber);

            if (tradingPartner != null)
            {
                Bus.Send<MatchMovementRecord>(cmd =>
                {
                    cmd.MatchedOn = DateTime.UtcNow;
                    cmd.MatchedBy = Identity.UserKey;

                    cmd.MovementId = movementId;
                    cmd.MovementEquipmentCode = movementEquipmentCode;

                    cmd.InvoiceId = invoiceId;
                    cmd.DetailKey = detailKey;
                    cmd.MatchedMovementDate = DateTime.Parse(movementDate);
                    cmd.MatchedDocketNumber = docketNumber;
                    cmd.MatchedDestinationId = tradingPartner.CompanyId;
                    cmd.MatchedEquipmentCode = equipmentCode;
                    cmd.MatchedMovementType = "Transfer";
                    cmd.MatchedMovementDirection = movementDirection;
                    cmd.MatchedQuantity = quantity;
                });
            }
            else
            {
                Failure("No such account number in our database.");
            }

            return RedirectToAction("Reconcile", "Reconciliation", new { id = invoiceId });
        }

        [POST("Reconciliation/ViewDetails/{id}/RejectRecord")]
        public ActionResult RejectRecord(Guid invoiceId, Guid detailKey)
        {
            if (invoiceId != Guid.Empty && detailKey != Guid.Empty)
            {
                Bus.Send<RejectMovementRecord>(cmd =>
                                                   {
                                                       cmd.RejectedOn = DateTime.UtcNow;
                                                       cmd.RejectedBy = Identity.UserKey;

                                                       cmd.InvoiceId = invoiceId;
                                                       cmd.DetailKey = detailKey;
                                                   });
            }
            else
            {
                Failure("Invoice ID and Detail Key failure.");
            }

            return RedirectToAction("Reconcile", "Reconciliation", new { id = invoiceId });
        }

        //TODO: MAKE THE CODE CLEARER
        [POST("Reconciliation/ViewDetails/{id}/CreateTransaction")]
        public ActionResult CreateTransaction(Guid invoiceId, Guid detailKey, bool createdPartner = false)
        {
            //var invoice = _session.Load<SupplierInvoice>(invoiceId);
            //if (invoice != null)
            //{
            //    var item = invoice.Details.FirstOrDefault(x => x.DetailKey == detailKey);
            //    if (item != null)
            //    {
            //        //get site suppliers and find the one with an ID of CUSTCODE from invoice detail record
            //        var accountNumbers = _session.Query
            //            <DestinationNamesAccountNumbersIndex.Result, DestinationNamesAccountNumbersIndex>()
            //            .Where(x => x.AccountNumber == item.CUSTCODE || x.AccountNumber == item.OTHERPARTY)
            //            .ToList();

            //        var siteCreatingMovement = accountNumbers.FirstOrDefault(x => x.AccountNumber == item.CUSTCODE);
            //        var movementDestination = accountNumbers.FirstOrDefault(x => x.AccountNumber == item.OTHERPARTY || x.CompanyName == item.OTHERPARTYNAME);

            //        if (siteCreatingMovement == null)
            //        {
            //            Failure("Site with that account number doesn't exist in our database.");

            //            return null;
            //        }

            //        if (movementDestination == null)
            //        {
            //            //We need to create partner because partner with such account number doesn't exist in our database.
            //            Info("Partner with that account number doesn't exist in our database. Please add one in order to create transaction.");

            //            var vm = new PartnerViewModel
            //                         {
            //                             SupplierId = invoice.SupplierId,
            //                             SupplierName = invoice.SupplierName,
            //                             AccountNumber = item.OTHERPARTY,
            //                             EquipmentCode = item.EQUIPCODE,
            //                             CompanyName = item.OTHERPARTYNAME,
            //                             SiteId = siteCreatingMovement.CompanyId,
            //                             SiteName = siteCreatingMovement.CompanyName,
            //                             InvoiceId = invoiceId,
            //                             InvoiceDetailKey = item.DetailKey
            //                         };

            //            return PartialView("_CreatePartner", vm);
            //        }

            //        var movementId = Guid.NewGuid();
            //        var destinationId = movementDestination.CompanyId.ToGuidId();
            //        Guid? locationId = null;
            //        if (IsTypeOfByStringId<Supplier>(movementDestination.CompanyId))
            //        {
            //            destinationId = invoice.SupplierId.ToGuidId();
            //            locationId = movementDestination.CompanyId.ToGuidId();
            //        }

            //        var equipment = new Dictionary<string, int> { { item.EQUIPCODE, Convert.ToInt32(item.QUANTITY) } };
            //        Bus.Send<CreateMovement>(cmd =>
            //                                     {
            //                                         cmd.MovementId = movementId;
            //                                         cmd.SiteId = siteCreatingMovement.CompanyId.ToGuidId();
            //                                         cmd.DestinationId = destinationId;
            //                                         cmd.SupplierId = invoice.SupplierId.ToGuidId();
            //                                         cmd.Equipments = equipment;
            //                                         cmd.Direction = EvaluateDirectionByTransCode(item.TRANSCODE);
            //                                         cmd.MovementDate = item.TRANSDATE.HasValue
            //                                                                ? item.TRANSDATE.Value
            //                                                                : DateTime.UtcNow;

            //                                         cmd.ConsignmentNote = item.CONNOTE;
            //                                         cmd.DocketNumber = item.PCMSDOCKET;
            //                                         cmd.DestinationReference = item.REF2;
            //                                         cmd.MovementReference = item.REF1;
            //                                         cmd.LocationId = locationId;
            //                                     });

            //        Bus.Send<CreateTransaction>(cmd =>
            //                                        {
            //                                            cmd.CreatedOn = DateTime.UtcNow;
            //                                            cmd.CreatedBy = Identity.UserKey;

            //                                            cmd.InvoiceId = invoiceId;
            //                                            cmd.DetailKey = detailKey;

            //                                            cmd.MovementId = movementId;
            //                                        });


            //        if (createdPartner)
            //        {
            //            ThreadingService.ObjectCreated<Movement>(movementId);
            //            Success();

            //            return RedirectToAction("ViewDetails", "Reconciliation", new { id = invoiceId });
            //        }

            //        return null;
            //    }
            //}

            return HttpNotFound();
        }

        [POST("Reconciliation/ViewDetails/{id}/CreatePartner")]
        public ActionResult CreatePartner(PartnerViewModel vm)
        {
            var partnerId = Guid.NewGuid();
            Bus.Send<CreatePartner>(cmd =>
                                        {
                                            cmd.PartnerId = partnerId;
                                            cmd.Name = vm.CompanyName;
                                        });

            Bus.Send<AddSupplierToPartner>(cmd =>
                                               {
                                                   cmd.AccountNumber = vm.AccountNumber;
                                                   cmd.PartnerId = partnerId;
                                                   cmd.SupplierId = vm.SupplierId.ToGuidId();
                                               });

            Bus.Send<AddPartnerToSite>(cmd =>
                                              {
                                                  cmd.SiteId = vm.SiteId.ToGuidId();
                                                  cmd.PartnerId = partnerId;
                                              });

            Bus.Send<AllowEquipmentToPartner>(
                    cmd =>
                    {
                        cmd.PartnerId = partnerId;
                        cmd.SupplierId = vm.SupplierId.ToGuidId();
                        cmd.EquipmentCodes = new[] { vm.EquipmentCode };
                    });

            ThreadingService.ObjectCreated<Partner>(partnerId);

            return CreateTransaction(vm.InvoiceId, vm.InvoiceDetailKey, true);
        }

        [GET("CheckPartnerNameUniquiness")]
        public JsonResult CheckPartnerNameUniquiness(string companyName)
        {
            var exists = _session.Query<Partner>(typeof(PartnerIndex).Name).Any(x => x.CompanyName == companyName);

            return Json(!exists, JsonRequestBehavior.AllowGet);
        }


    }
}