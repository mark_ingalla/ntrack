﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using PalletPlus.Data;
using PalletPlus.Messages.Commands;
using PalletPlus.Website.Services;
using Raven.Client;
using nTrack.Data.Extenstions;

namespace PalletPlus.Website.Areas.Admin.Controllers
{
    public class ImportPartnersController : AdminBaseController
    {
        readonly IDocumentSession _session;
        readonly IAttachmentService _attachmentService;

        public ImportPartnersController(IDocumentSession session,IAttachmentService attachmentService)
        {
            _session = session;
            _attachmentService = attachmentService;
        }


        [GET("ImportPartners")]
        public ActionResult ImportPartners()
        {
            return View("ImportPartners");
        }

        [POST("ImportPartners")]
        public ActionResult ImportPartners(IEnumerable<HttpPostedFileBase> files)
        {
            if (files != null)
            {
                foreach (var file in files)
                {
                    //upload the file to database
                    var attachment = new ImportPartnersAttachment
                    {
                        Id = Guid.NewGuid(),
                        FileName = file.FileName,
                        UploadedOn = DateTime.Now
                    };
                    _session.Store(attachment);
                    _attachmentService.PutAttachment(attachment.Id.ToStringId<ImportPartnersAttachment>(), file.InputStream);

                    //send the import partners command
                    Bus.Send<ImportPartners>(cmd =>
                    {
                        cmd.AttachmentId = attachment.Id;
                    });
                }
            }

            //TODO: fix import service logic
            //if (files != null)
            //{
            //    foreach (var f in files)
            //    {
            //        try
            //        {
            //            ErrorLogModel errorLog;
            //            var partners = _importPartnersService.ImportPartners(f.InputStream, f.FileName, out errorLog);

            //            object[] commands = partners.Select(x => new CreatePartner
            //                                                         {
            //                                                             Id = Guid.NewGuid(),
            //                                                             PartnerId = Guid.NewGuid(),
            //                                                             Name = x.PartnerName,
            //                                                             AddressLine1 = x.AddressLine1,
            //                                                             AddressLine2 = x.AddressLine2,
            //                                                             ContactEmail = x.ContactEmail,
            //                                                             ContactName = x.ContactName,
            //                                                             Postcode = x.Postcode,
            //                                                             State = x.State
            //                                                         }).ToArray();
            //            Bus.Send(commands);

            //            if (partners.Any())
            //                Bus.Send(new ImportPartners()
            //                             {
            //                                 AcountId = Identity.AccountId,
            //                                 FileName = f.FileName,
            //                                 ImportDateUTC = DateTime.UtcNow,
            //                                 TotalItems = partners.Count,
            //                                 Log = errorLog.ErrorMessage
            //                             });

            //            if(errorLog.RowErrors.Any())
            //                Info(errorLog.ErrorMessage);
            //            else
            //            {
            //                Success();
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            Failure("Failed to parse the document - " + ex.Message);
            //        }
            //    }
            //}

            Success();

            return View("ImportPartners");
        }
    }
}
