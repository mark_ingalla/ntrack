﻿using System;
using AttributeRouting.Web.Mvc;
using PalletPlus.Website.Services;
using Raven.Client;
using System.Web.Mvc;
using nTrack.Extension;

namespace PalletPlus.Website.Areas.Admin.Controllers
{
    public class HomeController : AdminBaseController
    {
        private readonly IDocumentSession _session;

        public HomeController(IDocumentSession session, IThreadingService threadingService)
            : base(threadingService)
        {
            _session = session;
        }

        [GET("")]
        public ActionResult Index()
        {
            return RedirectToAction("Index", "BalanceDashboard", new { area = "Admin" });
        }
    }
}
