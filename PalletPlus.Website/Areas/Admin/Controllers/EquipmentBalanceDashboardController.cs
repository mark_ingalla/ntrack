﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using Newtonsoft.Json;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Website.Models.DataVisualization;
using PalletPlus.Website.Services;
using Raven.Client;
using nTrack.Data.Extenstions;

namespace PalletPlus.Website.Areas.Admin.Controllers
{
    public class EquipmentBalanceDashboardController : AdminBaseController
    {
        private readonly IDocumentSession _session;

        public EquipmentBalanceDashboardController(IDocumentSession session, IThreadingService threadingService)
            : base(threadingService)
        {
            _session = session;
        }


        [GET("EquipmentBalanceDashboard")]
        public ActionResult Index()
        {
            //TODO: this looks like a code smell
            ViewBag.Sites = _session.Advanced.LoadStartingWith<Site>("sites/").Select(x => x.CompanyName).ToArray();


            // .Query<SiteIndex.Result, SiteIndex>()
            //.Select(x => x.SiteName).ToArray();
            return View();


        }

        [POST("SetEquipmentBalanceViewModel")]
        public ActionResult SetEquipmentBalanceViewModel()
        {
            var vm = new List<SiteEquipmentBalanceModel>();

            var sites = _session.Advanced.LoadStartingWith<Site>("sites/")
                
                //.Query<SiteIndex.Result, SiteIndex>()
                .ToDictionary(x => x.Id, y => y.CompanyName);

            var equipmentBalances = _session.Query<MovementBalanceIndex.Result, MovementBalanceIndex>()
                .ToList();

            foreach (var s in sites)
            {
                var balance = equipmentBalances.Where(x => x.SiteId == s.Key.ToStringId<Site>())
                    .Select(x => new SiteEquipmentBalanceModel(x.SiteName, x.EquipmentName, x.Quantity))
                    .OrderBy(x => x.SiteName);

                if (balance.Any())
                    vm.AddRange(balance);
                else
                {
                    var eqNames = _session.Query<EquipmentIndex.Result, EquipmentIndex>()
                        .Select(x => x.EquipmentName)
                        .ToList();

                    vm.AddRange(eqNames
                                    .Select(x => new SiteEquipmentBalanceModel(s.Value, x, 0)));
                }
            }

            #region New Code
            var msites = from p in vm
                         group p by p.SiteName
                             into grp
                             select grp.Key;

            var equipments = from p in vm
                             group p by p.EquipmentName
                                 into grp
                                 select grp.Key;

            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);

            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                writer.Formatting = Formatting.Indented;
                writer.WriteStartObject();


                //chart data
                writer.WritePropertyName("chartdata");
                writer.WriteStartArray();
                foreach (var s in msites)
                {
                    writer.WriteStartObject();
                    writer.WritePropertyName("SiteName");
                    writer.WriteValue(s);

                    foreach (var e in equipments.ToList())
                    {

                        WriteJSONProperty(writer, vm, s, e);


                    }
                    //  writer.WriteEnd();
                    writer.WriteEndObject();

                }
                writer.WriteEndArray();
                //end chart data

                //start series
                //writer.WriteStartObject();
                writer.WritePropertyName("seriesdata");
                writer.WriteStartArray();
                foreach (var equipment in equipments)
                {
                    writer.WriteStartObject();
                    writer.WritePropertyName("field");
                    writer.WriteValue(equipment);
                    writer.WritePropertyName("name");
                    writer.WriteValue(equipment);
                    writer.WriteEndObject();
                }
                writer.WriteEndArray();
                //   writer.WriteEndObject();

                writer.WriteEndObject();




            }


            #endregion

            return Content(sb.ToString(), "application/json");
        }

        private void WriteJSONProperty(JsonWriter writer, List<SiteEquipmentBalanceModel> source, string site, string equipment)
        {
            var query = (from p in source
                         group p by new { p.EquipmentName, p.SiteName }
                             into grp
                             where grp.Key.EquipmentName == equipment && grp.Key.SiteName == site
                             select new SiteEquipmentBalanceModel
                             {
                                 EquipmentName = grp.Key.EquipmentName,
                                 SiteName = grp.Key.SiteName,
                                 Quantity = grp.Sum(x => x.Quantity)
                             }).FirstOrDefault();

            writer.WritePropertyName(equipment);
            if (query != null)
            {

                writer.WriteValue(query.Quantity);
            }
            else
            {
                writer.WriteValue(0);
            }
        }


    }
}
