﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using nTrack.Core.Enums;
using nTrack.Data.Extenstions;
using nTrack.Extension;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Commands.Partner;
using PalletPlus.Messages.Commands.Site;
using PalletPlus.Website.Areas.Admin.ViewModels.Partner;
using PalletPlus.Website.Services;
using Raven.Client;
using Raven.Client.Linq;

namespace PalletPlus.Website.Areas.Admin.Controllers
{

    public class PartnerController : AdminBaseController
    {
        readonly IDocumentSession _session;
        readonly IAttachmentService _attachmentService;

        public PartnerController(IDocumentSession session, IThreadingService threadingService, IAttachmentService attachmentService)
            : base(threadingService)
        {
            _session = session;
            _attachmentService = attachmentService;
        }

        [GET("Partners")]
        public ActionResult Index(bool? showDeleted = false)
        {
            ViewBag.ShowDeleted = showDeleted;
            return View();
        }

        [GET("PartnersRead")]
        public ActionResult PartnersRead([DataSourceRequest]DataSourceRequest request, bool showDeleted = false)
        {
            var result = GetPartners(request, showDeleted);

            return Json(result);
        }

        [GET("Partner/Create")]
        public ActionResult Create()
        {
            var vm = new PartnerViewModel();

            vm.ComboboxCountries = GetCountries();

            return View(vm);
        }

        [POST("Partner/Create")]
        public ActionResult Create(PartnerViewModel partner)
        {
            var partnerEntity = _session.Query<Partner>().FirstOrDefault(x => x.CompanyName == partner.CompanyName);
            if (partnerEntity != null)
            {
                partner.ComboboxCountries = GetCountries();
                Failure("A partner with that name already exists.");
                partner.CompanyName = string.Empty;
                return View(partner);
            }

            partner.PartnerId = Guid.NewGuid();
            //Bus.SendMap<CreatePartner>(partner);
            Bus.Send(new CreatePartner
                         {
                             AddressLine1 = partner.AddressLine1,
                             AddressLine2 = partner.AddressLine2,
                             ContactEmail = partner.ContactEmail,
                             ContactName = partner.ContactName,
                             Country = partner.Country,
                             //    Id = Guid.NewGuid(),
                             IsPTAEnabled = partner.IsPTAEnabled,
                             PartnerId = partner.PartnerId,
                             Name = partner.CompanyName,
                             Postcode = partner.PostCode,
                             Tags = partner.Tags,
                             State = partner.State,
                             Suburb = partner.Suburb
                         });

            ThreadingService.ObjectCreated<Partner>(partner.PartnerId);

            RecordCreated("details");

            return RedirectToAction("Edit", new { id = partner.PartnerId });
        }

        [GET("Partner")]
        public ActionResult Edit(Guid id)
        {
            var partner = _session.Load<Partner>(id);
            var vm = new PartnerEditViewModel();

            if (partner != null)
            {
                // vm = Mapper.Map<PartnerEditViewModel>(partner);
                vm = new PartnerEditViewModel(partner);
                GetSuppliers(ref vm, partner);
                GetSites(ref vm, partner);
                GetTransporters(ref vm, partner);
                GetDocuments(ref vm, partner);
                vm.ComboboxCountries = GetCountries();

                return View(vm);
            }

            Failure("Couldn't load entity.");
            return View(vm);
        }

        private void GetDocuments(ref PartnerEditViewModel vm, Partner partner)
        {
            vm.DocumentsVM = partner.Documents;
        }

        [POST("Partner")]
        public ActionResult Edit(PartnerEditViewModel partner)
        {
            var partnerEntity =
                _session.Query<Partner>().FirstOrDefault(
                    x => x.CompanyName == partner.PartnerName && partner.PartnerId != x.Id);

            if (partnerEntity != null)
            {
                Failure("A partner with that name already exists.");
                return RedirectToAction("Edit", new { id = partner.PartnerId });
            }

            //var now = Bus.SendMap<UpdatePartner>(partner);

            //ThreadingService.ObjectUpdated<Partner>(partner.PartnerId, now);
            Bus.Send(new UpdatePartner
            {
                AddressLine1 = partner.AddressLine1,
                AddressLine2 = partner.AddressLine2,
                ContactEmail = partner.ContactEmail,
                ContactName = partner.ContactName,
                Country = partner.Country,
                //    Id = Guid.NewGuid(),
                IsPTAEnabled = partner.IsPTAEnabled,
                PartnerId = partner.PartnerId,
                Name = partner.PartnerName,
                Postcode = partner.Postcode,
                Tags = partner.Tags,
                State = partner.State,
                Suburb = partner.Suburb
            });

            ThreadingService.ObjectCreated<Partner>(partner.PartnerId);

            RecordUpdated("details");

            return RedirectToAction("Edit", new { id = partner.PartnerId });
        }

        [GET("Partner/Delete")]
        public ActionResult Delete(Guid id, bool showDeleted)
        {
            Bus.Send<DeletePartner>(cmd => cmd.PartnerId = id);

            ThreadingService.ObjectDeleted<Partner>(id);

            return RedirectToAction("Index", SetRouteValues(showDeleted));
        }

        [GET("Partner/Undelete")]
        public ActionResult Undelete(Guid id, bool showDeleted)
        {
            Bus.Send<UndeletePartner>(cmd => cmd.PartnerId = id);

            ThreadingService.ObjectUndeleted<Partner>(id);

            return RedirectToAction("Index", SetRouteValues(showDeleted));
        }

        [POST("Partner/AddSupplier")]
        public ActionResult AddSupplier(PartnerEditViewModel partner)
        {
            if (partner.SupplierVM.SupplierId.IsEmpty())
            {
                Failure("suppliers", "Please choose supplier from the list of available items.");
                return RedirectToAction("Index");
            }

            if (!ValidateAccountNumberUniquiness(partner.PartnerId, partner.SupplierVM.SupplierAccountNo))
                return RedirectToAction("Edit", new { id = partner.PartnerId });

            //var now = Bus.SendMap<AddSupplierToPartner>(partner);


            Bus.Send<AddSupplierToPartner>(
                   cmd =>
                   {
                       cmd.AccountNumber = partner.SupplierVM.SupplierAccountNo;
                       cmd.PartnerId = partner.PartnerId;
                       cmd.SupplierId = partner.SupplierVM.SupplierId;

                   }
                   );

            if (partner.SupplierVM.EnableExchange)
            {
                Bus.Send<EditEnableExchange>(
                    cmd =>
                    {
                        cmd.EnabledExchange = true;
                        cmd.PartnerId = partner.PartnerId;
                        cmd.SupplierId = partner.SupplierVM.SupplierId;

                    }
                    );
            }

            ThreadingService.ObjectUpdated<Partner>(partner.PartnerId, DateTime.UtcNow);
            RecordUpdated("suppliers");

            return RedirectToAction("Edit", new { id = partner.PartnerId });
        }

        //not used currently
        [POST("Partner/EditSupplier")]
        public void EditSupplier(PartnerSupplierEquipmentViewModel model, Guid partnerId)
        {
            bool commandSent = false;

            var supplier = _session.Load<Partner>(partnerId).Suppliers[model.SupplierId.ToStringId<Supplier>()];
            if (supplier != null)
            {
                var assetsAlreadyAdded = supplier.Equipments;
                var checkedNodes = model.CheckedNodes.Where(x => x.Value).ToDictionary(x => x.Key, y => y.Value);
                var assetsToAdd = checkedNodes.Keys.Except(assetsAlreadyAdded).ToList();
                var assetsToRemove = assetsAlreadyAdded.Except(checkedNodes.Keys).ToList();

                //Allow checked assets
                if (assetsToAdd.Any())
                {
                    Bus.Send<AllowEquipmentToPartner>(
                        cmd =>
                        {
                            cmd.PartnerId = partnerId;
                            cmd.SupplierId = model.SupplierId;
                            cmd.EquipmentCodes = assetsToAdd.ToArray();
                        });
                    commandSent = true;
                }

                //Disallow unchecked assets
                if (assetsToRemove.Any())
                {
                    Bus.Send<DisallowEquipmentToPartner>(
                        cmd =>
                        {
                            cmd.PartnerId = partnerId;
                            cmd.SupplierId = model.SupplierId;
                            cmd.Equipments = assetsToRemove;
                        });
                    commandSent = true;
                }
            }

            if (commandSent)
            {
                ThreadingService.ObjectUpdated<Partner>(partnerId, DateTime.UtcNow);
                RecordUpdated("suppliers");
            }
            else
            {
                Failure("No assets were chosen for addition nor removal.");
            }
        }

        [POST("Partner/EditSupplierEquipmentItem/{id}")]
        public void EditSupplierEquipmentItem(Guid id, bool assetAction, Guid supplierId, string equipmentCode)
        {
            var asset = new List<string> { equipmentCode };
            if (assetAction)
            {
                Bus.Send<AllowEquipmentToPartner>(
                    cmd =>
                    {
                        cmd.PartnerId = id;
                        cmd.SupplierId = supplierId;
                        cmd.EquipmentCodes = asset.ToArray();
                    });
            }
            else
            {
                Bus.Send<DisallowEquipmentToPartner>(
                    cmd =>
                    {
                        cmd.PartnerId = id;
                        cmd.SupplierId = supplierId;
                        cmd.Equipments = asset;
                    });
            }
        }

        [GET("Partner/DeleteSupplier")]
        public ActionResult DeleteSupplier(Guid id, Guid supplierId)
        {
            var partner = _session.Load<Partner>(id);
            if (partner != null)
            {
                if (partner.Suppliers.ContainsKey(supplierId.ToStringId<Supplier>()))
                {
                    var supplier = partner.Suppliers[supplierId.ToStringId<Supplier>()];
                    var assets = supplier.Equipments;

                    Bus.Send<DisallowEquipmentToPartner>(
                        cmd =>
                        {
                            cmd.PartnerId = id;
                            cmd.SupplierId = supplierId;
                            cmd.Equipments = assets.ToList();
                        });

                    Bus.Send<RemoveSupplierFromPartner>(
                        cmd =>
                        {
                            cmd.PartnerId = id;
                            cmd.SupplierId = supplierId;
                        });

                    ThreadingService.ObjectUpdated<Partner>(id, DateTime.UtcNow);
                }
            }

            RecordUpdated("suppliers");

            return RedirectToAction("Edit", new { id });
        }

        [POST("Partner/AddSite")]
        public ActionResult AddSite(PartnerEditViewModel partner)
        {
            if (partner.SiteVM.SiteId.IsEmpty())
            {
                Failure("sites", "Please choose site from the list of available items.");
                return RedirectToAction("Edit", new { id = partner.PartnerId });
            }

            Bus.Send<AddPartnerToSite>(
                cmd =>
                {
                    cmd.SiteId = partner.SiteVM.SiteId;
                    cmd.PartnerId = partner.PartnerId;
                });

            ThreadingService.ObjectUpdated<Partner>(partner.PartnerId, DateTime.UtcNow);
            RecordUpdated("sites");

            return RedirectToAction("Edit", new { id = partner.PartnerId });
        }

        [GET("Partner/DeleteSite/{siteId}")]
        public ActionResult DeleteSite(Guid id, Guid siteId)
        {
            Bus.Send<RemovePartnerFromSite>(
                cmd =>
                {
                    cmd.PartnerId = id;
                    cmd.SiteId = siteId;
                });

            ThreadingService.ObjectUpdated<Partner>(id, DateTime.UtcNow);
            RecordUpdated("sites");

            return RedirectToAction("Edit", new { id });
        }

        [POST("Partner/AddTransporter")]
        public ActionResult AddTransporter(PartnerEditViewModel partner)
        {
            //RecordUpdated("transporters");
            Failure("transporters", "not implemented");

            return RedirectToAction("Edit", new { id = partner.PartnerId });
        }

        [GET("Partner/DeleteTransporter/{transporterId}")]
        public ActionResult DeleteTransporter(Guid id, Guid transporterId)
        {
            //RecordUpdated("transporters");
            Failure("transporters", "not implemented");

            return RedirectToAction("Edit", new { id });
        }

        [GET("Partner/IsValidSupplier")]
        public ActionResult IsValidSupplier(PartnerSupplierViewModel supplierVM)
        {
            bool res;

            if (supplierVM.SupplierId.IsEmpty())
                res = false;
            else
            {
                res = _session.Query<Supplier>(typeof(SupplierIndex).Name)
                    .Any(x => x.Id == supplierVM.SupplierId);
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [GET("Partner/IsValidSite")]
        public ActionResult IsValidSite(PartnerSiteViewModel siteVM)
        {
            bool res;

            if (siteVM.SiteId.IsEmpty())
                res = false;
            else
            {
                res = _session.Load<Site>(siteVM.SiteId) != null;
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [POST("Partner/EditEnableExchange/{supplierId}/{partnerId}/{enableExchange}")]
        public void EditEnableExchange(Guid supplierId, Guid partnerId, bool enableExchange)
        {

            Bus.Send<EditEnableExchange>(
                     cmd =>
                     {
                         cmd.PartnerId = partnerId;
                         cmd.SupplierId = supplierId;
                         cmd.EnabledExchange = enableExchange;
                     });
        }


        void GetSuppliers(ref PartnerEditViewModel viewModel, Partner partner)
        {
            var alreadyAdded = partner.Suppliers;

            var suppliers = _session.Query<Supplier>(typeof(SupplierIndex).Name)
                .ToDictionary(x => x.Id, y => y);

            //NOTE: Not so sure about this, and how to solve this (because we access suppliers[Key] 
            //NOTE: not knowing if it's null. Shall we create index for them?
            var partnerSuppliers = alreadyAdded
                .Select(x => new PartnerSupplierViewModel
                {
                    IsDeleted = suppliers[x.Key.ToGuidId()].IsDeleted,
                    SupplierId = x.Key.ToGuidId(),
                    SupplierName = suppliers[x.Key.ToGuidId()].CompanyName,
                    ContactEmail = suppliers[x.Key.ToGuidId()].ContactEmail,
                    SupplierAccountNo = x.Value.AccountNumber,
                    PartnerId = partner.Id,
                    Equipment = suppliers[x.Key.ToGuidId()].Equipments.Where(y => !y.Value.IsDeleted)
                    .ToDictionary(y => y.Value.EquipmentCode, z => z.Value.EquipmentName),
                    EquipmentAvailable = x.Value.Equipments,
                    EnableExchange = x.Value.IsExchangeEnabled
                }).ToList();


            var inSuppliers = alreadyAdded.Select(x => x.Key).ToList();
            var comboboxSuppliers = suppliers
                .Where(x => !x.Value.IsDeleted && !x.Value.IdString.In(inSuppliers))
                .Select(x => new { Id = x.Value.Id, SupplierName = x.Value.CompanyName }).ToList();

            viewModel.SuppliersVM = partnerSuppliers;
            viewModel.ComboboxSuppliers = comboboxSuppliers;
        }
        void GetSites(ref PartnerEditViewModel viewModel, Partner partner)
        {
            var alreadyAdded = partner.Sites.ToList();

            var sites = _session.Advanced.LoadStartingWith<Site>("sites/");
            //.Query<SiteIndex.Result, SiteIndex>().ToList();

            var partnerSites = sites
                .Where(x => !x.IsDeleted && x.IdString.In(alreadyAdded))
                .Select(x => new PartnerSiteViewModel
                {
                    IsDeleted = x.IsDeleted,
                    SiteId = x.Id,
                    SiteName = x.CompanyName,
                    Suburb = x.Suburb,
                    State = x.State,
                    ContactEmail = x.ContactEmail
                }).ToList();

            var comboboxSites = sites
                .Where(x => !x.IsDeleted && !x.IdString.In(alreadyAdded))
                .Select(x => new { Id = x.Id, SiteName = x.CompanyName });

            viewModel.SitesVM = partnerSites;
            viewModel.ComboboxSites = comboboxSites;
        }
        void GetTransporters(ref PartnerEditViewModel viewModel, Partner partner)
        {
            var alreadyAdded = partner.Transporters;

            var transporters = _session.Query<TransporterIndex.Result, TransporterIndex>().ToList();

            var partnerTransporters = transporters
                .Where(x => !x.IsDeleted && x.TransporterId.In(alreadyAdded))
                .Select(x => new PartnerTransporterViewModel
                {
                    IsDeleted = x.IsDeleted,
                    TransporterId = x.TransporterId.ToGuidId(),
                    TransporterName = x.TransporterName,
                    Suburb = x.Suburb,
                    State = x.State,
                    ContactEmail = x.ContactEmail
                }).ToList();

            var comboboxTransporters = transporters
                .Where(x => !x.IsDeleted && !x.TransporterId.In(alreadyAdded))
                .Select(x => new { Id = x.TransporterId.ToGuidId(), TransporterName = x.TransporterName });

            viewModel.TransportersVM = partnerTransporters;
            viewModel.ComboboxTransporters = comboboxTransporters;
        }

        [POST("GetPartners")]
        public ActionResult GetPartners([DataSourceRequest()]DataSourceRequest request, bool showDeleted = false)
        {
            if (request.Sorts == null)
            {
                request.Sorts = new List<SortDescriptor>();
                request.Sorts.Add(new SortDescriptor("CompanyName", ListSortDirection.Descending));
            }

            if (request.PageSize == 0)
            {
                request.PageSize = 10;
                request.Page = 0;
            }

            var ret =
                    _session.Query<Partner>(typeof(PartnerIndex).Name).Where(x => x.IsDeleted == showDeleted);


            if (request.Filters != null && request.Filters.Count > 0)
            {
                var companyName = (request.Filters[0] as CompositeFilterDescriptor).FilterDescriptors.Single(
                    p => (p as FilterDescriptor).Member == "CompanyName") as FilterDescriptor;
                var contactEmail = (request.Filters[0] as CompositeFilterDescriptor).FilterDescriptors.Single(
                    p => (p as FilterDescriptor).Member == "ContactEmail") as FilterDescriptor;



                request.Filters = null;


                ret =
                    ret.Where(
                        p =>
                        p.CompanyName.StartsWith((string)companyName.Value) ||
                        p.ContactEmail.StartsWith((string)contactEmail.Value));

            }



            var result = ret.ToList().ToDataSourceResult(request, x => new PartnerViewModel(x));

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private bool ValidateAccountNumberUniquiness(Guid siteId, string accountNumber)
        {
            var accountNumbers = _session.Query<AccountNumbersIndex.Result, AccountNumbersIndex>();
            var accoutnNumber = accountNumbers.FirstOrDefault(x => x.AccountNumber == accountNumber);
            if (accoutnNumber != null)
            {
                Failure("suppliers", "Account number already in use.");
                return false;
            }
            return true;
        }

        [GET("Partner/DeleteFile")]
        public ActionResult DeleteFile(Guid partnerId, Guid attachmentKey, string returnUrl)
        {
            var partner = _session.Load<Partner>(partnerId);

            if (partner != null)
            {
                if (partner.Documents.ContainsKey(attachmentKey))
                {
                    _attachmentService.DeleteAttachment("{0}_{1}".Fill(AttachmentEntities.PartnersDocuments, attachmentKey));

                    partner.Documents.Remove(attachmentKey);

                    _session.SaveChanges();

                    RecordUpdated("documents");
                }
            }

            return Redirect(returnUrl);
        }


        [GET("Partner/GetFile")]
        public ActionResult GetFile(Guid partnerId, Guid attachmentKey)
        {
            var partner = _session.Load<Partner>(partnerId);

            if (partner != null)
            {
                if (partner.Documents.ContainsKey(attachmentKey))
                {
                    Raven.Abstractions.Data.Attachment attachment =
                        _attachmentService.GetAttachment("{0}_{1}".Fill(AttachmentEntities.PartnersDocuments, attachmentKey));

                    if (attachment == null)
                        return null;

                    return File(attachment.Data(), partner.Documents[attachmentKey].MIME);
                }
            }
            return null;
        }

        [POST("Partner/PutFile")]
        public ActionResult PutFile(Guid partnerId, string returnUrl, IEnumerable<HttpPostedFileBase> files)
        {
            if (files != null)
            {
                foreach (var file in files)
                {

                    if (file.ContentLength > 0)
                    {
                        Guid attachmentId = Guid.NewGuid();

                        _attachmentService.PutAttachment("{0}_{1}".Fill(AttachmentEntities.PartnersDocuments, attachmentId), file.InputStream);

                        var partner = _session.Load<Partner>(partnerId);

                        partner.Documents.Add(attachmentId, new PartnerDocument()
                                                                {
                                                                    Name = file.FileName,
                                                                    ByteSize = file.ContentLength,
                                                                    MIME = file.ContentType,
                                                                    UploadDateUTC = DateTime.UtcNow
                                                                });

                        _session.SaveChanges();

                        RecordUpdated("documents");
                    }
                }
            }

            return Redirect(returnUrl);
        }

        [GET("Partner/AvailableTags")]
        public JsonResult AvailableTags(string term)
        {
            var tags = _session.Query<PartnerTagIndex.Result, PartnerTagIndex>()
                .ToList()
                .Where(x => x.Tag.Contains(term)).Select(x => new
                                                                {
                                                                    value = x.Tag,
                                                                    label = x.Tag
                                                                });


            return Json(tags, JsonRequestBehavior.AllowGet);
        }
    }
}