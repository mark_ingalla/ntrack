﻿using System.ComponentModel;
using PalletPlus.Website.Areas.User.ViewModels.Enumerators;
using System;

namespace PalletPlus.Website.Areas.User.ViewModels.Stocktake
{
    public class StocktakeViewModel
    {
        public Guid StocktakeId { get; set; }

        public DateTime StocktakeDate { get; set; }

        public string EquipmentCode { get; set; }

        [DisplayName("Equipment")]
        public string EquipmentName { get; set; }

        public int Quantity { get; set; }

        public Guid User { get; set; }

        public CorrectionStatus Status { get; set; }

        public StocktakeViewModel(Guid stocktakeId, DateTime stocktakeDate, string equipmentCode, string equipmentName, int quantity,
            bool gotPendingCorrection, bool? isApproved, Guid correctionRequestedBy, Guid userKey)
        {
            StocktakeId = stocktakeId;
            StocktakeDate = stocktakeDate;
            EquipmentCode = equipmentCode;
            EquipmentName = equipmentName;
            Quantity = quantity;

            if (gotPendingCorrection)
            {
                SetStatus(isApproved, correctionRequestedBy, userKey);
            }
        }

        public void SetStatus(bool? isApproved, Guid userWhoCreated, Guid userWhoSees)
        {
            if (isApproved == null && userWhoCreated == userWhoSees)
            {
                Status = CorrectionStatus.PendingCorrectionAndCreatedByMe;
                return;
            }

            if (isApproved == null)
                Status = CorrectionStatus.PendingCorrection;
            else if (isApproved == true)
                Status = CorrectionStatus.Approved;
            else
                Status = CorrectionStatus.Declined;
        }
    }
}