﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PalletPlus.Website.Areas.User.ViewModels.Stocktake
{
    public class StocktakeCorrectionDetailsViewModel
    {
        public Guid EquipmentProvider { get; set; }

        public string EquipmentProviderName { get; set; }

        public Guid StocktakeId { get; set; }

        public DateTime StocktakeDate { get; set; }

        public DateTime RequestedOn { get; set; }

        public string EquipmentCode { get; set; }

        public string EquipmentName { get; set; }

        public int OldQuantity { get; set; }

        public int NewQuantity { get; set; }

        public string Reason { get; set; }

        public string User { get; set; }

        public StocktakeCorrectionDetailsViewModel(){}

        public StocktakeCorrectionDetailsViewModel(Guid eqProvider, string eqProviderName, Guid stocktakeId, 
            DateTime stocktakeDate, DateTime requestedOn, string eqCode, string eqName, int oldQnt, int newQnt, string user, string reason)
        {
            EquipmentProvider = eqProvider;
            EquipmentProviderName = eqProviderName;
            StocktakeId = stocktakeId;
            StocktakeDate = stocktakeDate;
            RequestedOn = requestedOn;
            EquipmentCode = eqCode;
            EquipmentName = eqName;
            OldQuantity = oldQnt;
            NewQuantity = newQnt;
            User = user;
            Reason = reason;
        }
    }
}