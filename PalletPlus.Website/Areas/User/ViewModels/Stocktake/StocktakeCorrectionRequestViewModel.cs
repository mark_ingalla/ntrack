﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PalletPlus.Website.Areas.User.ViewModels.Stocktake
{
    public class StocktakeCorrectionRequestViewModel
    {
        public Guid EquipmentProvider { get; set; }

        public string EquipmentProviderName { get; set; }

        public Guid StocktakeId { get; set; }

        public DateTime StocktakeDate { get; set; }

        public string EquipmentCode { get; set; }

        public string EquipmentName { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        public string Reason { get; set; }

        public Dictionary<string, string> Reasons { get; set; }

        public StocktakeCorrectionRequestViewModel(){}

        public StocktakeCorrectionRequestViewModel(Guid equipmentProvider, string equipmentProviderName, Guid stocktakeId,
            DateTime stocktakeDate, string equipmentCode, string equipmentName, int quantity, Dictionary<string, string> reasons)
        {
            EquipmentProvider = equipmentProvider;
            EquipmentProviderName = equipmentProviderName;
            StocktakeId = stocktakeId;
            StocktakeDate = stocktakeDate;
            EquipmentCode = equipmentCode;
            EquipmentName = equipmentName;
            Quantity = quantity;
            Reasons = reasons;
        }
    }
}