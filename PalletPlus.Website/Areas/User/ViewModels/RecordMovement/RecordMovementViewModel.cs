﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using PalletPlus.Website.Areas.User.ViewModels.Enumerators;
using PalletPlus.Website.Helpers;
using Supplier = PalletPlus.Data.Supplier;

namespace PalletPlus.Website.Areas.User.ViewModels.RecordMovement
{
    public class RecordMovementViewModel
    {
        public Dictionary<string, string> IdToValidate { get; set; }

        #region movement details

        [Required]
        [DisplayName("Equipment Supplier")]
        public Guid SupplierId { get; set; }

        [DisplayName("Trading Partner")]
        public string TradingPartner { get; set; }

        [DisplayName("Transporter")]
        public Guid Transporter { get; set; }

        [DisplayName("Trading Partner")]
        public Guid TradingPartnerID { get; set; }

        [Required]
        [DisplayName("Partner Account Number")]
        public string PartnerAccountNumber { get; set; }

        [Required]
        [DisplayName("Effective Date")]
        [DataType(DataType.Date)]
        public DateTime MovementDate { get; set; }

        [DisplayName("Consignment Note")]
        public string ConNote { get; set; }

        [DisplayName("Docket Number")]
        [Remote("CheckDocketNumberUniqueness", "RecordMovementBase", ErrorMessage = "This docket number already exists in this site.")]
        public string ProviderDocketNo { get; set; }

        //[Required]
        //Doesn't work, i don't know why. 
        //[Remote("CheckRequirements", "RecordMovementBase")]
        [DisplayName("Partner Reference")]
        public string PartnerReference { get; set; }

        //[Required]
        [DisplayName("Reference")]
        public string Reference { get; set; }

        [DisplayName("Received via Transporter")]
        public bool ReceivedViaTransporter { get; set; }

        public Guid? LocationId { get; set; }

        public TransferType MovementType { get; set; }
        public MovementDirection MovementDirection { get; set; }

        public bool RequairePTAEnabled { get; set; }

        #endregion

        public Dictionary<string, int?> Equipment { get; set; } //int - amount; string - eqCode
        public Dictionary<string, string> EquipmentNames { get; set; }

        public Dictionary<Guid, string> Providers { get; set; }
        public Dictionary<string, string> TradingSubjects { get; set; }

        public Dictionary<string, string> Transporters { get; set; }

        public RecordMovementViewModel() { }

        public RecordMovementViewModel(MovementDirection movementDirection, Supplier[] providers)
        {
            MovementDirection = movementDirection;

            var providersDict = providers.ToDictionary(x => x.Id, y => y.CompanyName);
            Providers = new Dictionary<Guid, string>(providersDict);
        }

        public RecordMovementViewModel(MovementDirection movementDirection, Guid supplierId,
            Dictionary<string, string> partners, Dictionary<string, string> sites, Dictionary<Guid, string> suppliers)
        {
            MovementDirection = movementDirection;
            SupplierId = supplierId;

            TradingSubjects = new Dictionary<string, string>();
            foreach (var partner in partners)
                TradingSubjects.Add(partner.Key.Replace('/', '_'), partner.Value);
            foreach (var site in sites)
                TradingSubjects.Add(site.Key.Replace('/', '_'), site.Value);
            foreach (var supplier in suppliers)
                TradingSubjects.Add("suppliers_" + supplier.Key, supplier.Value);
        }
    }
}