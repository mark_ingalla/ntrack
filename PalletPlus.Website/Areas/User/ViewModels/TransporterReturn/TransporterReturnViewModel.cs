﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using PalletPlus.Data;

namespace PalletPlus.Website.Areas.User.ViewModels.TransporterReturn
{
    public class TransporterReturnViewModel
    {
        [Required]
        [DisplayName("Equipment Provider")]
        public Guid EquipmentProvider { get; set; }

        [Required]
        [DisplayName("Transporter")]
        public Guid Transporter { get; set; }

        public Dictionary<Guid, string> Transporters { get; set; }
        public Dictionary<Guid, string> Providers { get; set; }

        public Dictionary<string, int?> Assets { get; set; } //int - amount; string - eqCode
        public Dictionary<string, string> AssetNames { get; set; }

        public TransporterReturnViewModel() { }

        public TransporterReturnViewModel(IEnumerable<Supplier> providers)
        {
            var providersDict = providers.ToDictionary(x => x.Id, y => y.CompanyName);
            Providers = new Dictionary<Guid, string>(providersDict);
        }
    }
}