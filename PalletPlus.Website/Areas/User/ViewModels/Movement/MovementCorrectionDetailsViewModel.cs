﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PalletPlus.Website.Areas.User.ViewModels.Movement
{
    public class MovementCorrectionDetailsViewModel
    {
        public string DocketNumber { get; set; }

        public string EquipmentProviderName { get; set; }

        public string TradingPartnerName { get; set; }

        public string MovementId { get; set; }

        public string Direction { get; set; }

        public DateTime MovementDate { get; set; }

        public DateTime CorrectedMovementDate { get; set; }

        public DateTime EffectiveDate { get; set; }

        public DateTime CorrectedEffectiveDate { get; set; }

        public string MovementType { get; set; }

        public List<MovementEquipmentModel> Equipment { get; set; }

        public Dictionary<string, int> NewQuantities { get; set; }

        public string RequestedBy { get; set; }

        [Required]
        public string Reason { get; set; }
    }
}