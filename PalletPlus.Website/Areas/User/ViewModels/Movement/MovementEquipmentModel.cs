﻿

using System.ComponentModel;

namespace PalletPlus.Website.Areas.User.ViewModels.Movement
{
    public class MovementEquipmentModel
    {
        public string EquipmentCode { get; set; }

        [DisplayName("Equipment")]
        public string EquipmentName { get; set; }

        public int Quantity { get; set; }

        public string EffectiveDate { get; set; }
        public string DateTimeStamp { get; set; }

        public MovementEquipmentModel(string equipmentCode, string equipmentName, int quantity,string effectiveDate,string datetimestamp)
        {
            EquipmentCode = equipmentCode;
            EquipmentName = equipmentName;
            Quantity = quantity;
            EffectiveDate = effectiveDate;
            DateTimeStamp = datetimestamp;
        }
    }
}