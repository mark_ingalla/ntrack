﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PalletPlus.Website.Areas.User.ViewModels.Movement
{
    public class MovementCancellationRequestViewModel
    {
        public string DocketNumber { get; set; }

        public string EquipmentProviderName { get; set; }

        public string TradingPartnerName { get; set; }

        public Guid MovementId { get; set; }

        public string Direction { get; set; }

        public DateTime MovementDate { get; set; }

        public DateTime EffectiveDate { get; set; }

        public string MovementType { get; set; }

        public List<MovementEquipmentModel> Equipment { get; set; }

        [Required]
        public string Reason { get; set; }

        public SelectListItem[] Reasons { get; set; }

        public MovementCancellationRequestViewModel() { }

    }
}