﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PalletPlus.Website.Areas.User.ViewModels.Movement
{
    public class MovementListVM
    {
        public MovementGridVM Pending { get; set; }
        public MovementGridVM Recent { get; set; }
        public MovementGridVM Historic { get; set; }

        public MovementListVM(MovementGridVM pending, MovementGridVM recent, MovementGridVM historic)
        {
            Pending = pending;
            Recent = recent;
            Historic = historic;
        }

        public MovementListVM()
        {
        }
    }
}