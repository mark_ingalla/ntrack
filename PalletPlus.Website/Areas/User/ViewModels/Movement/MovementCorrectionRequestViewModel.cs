﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using PalletPlus.Data;

namespace PalletPlus.Website.Areas.User.ViewModels.Movement
{
    public class MovementCorrectionRequestViewModel
    {
        public string DocketNumber { get; set; }

        public Guid SupplierId { get; set; }

        public string EquipmentProviderName { get; set; }

        public string TradingPartnerName { get; set; }

        public Guid MovementId { get; set; }

        public string Direction { get; set; }

        public DateTime MovementDate { get; set; }

        public DateTime EffectiveDate { get; set; }

        public DateTime NewEffectiveDate { get; set; }

        public DateTime NewMovementDate { get; set; }

        public string MovementType { get; set; }

        public List<MovementEquipmentModel> Equipment { get; set; }

        public Dictionary<string, int> NewQuantities { get; set; }

        [Required]
        public string Reason { get; set; }

        public SelectListItem[] Reasons { get; set; }

        public MovementCorrectionRequestViewModel(){}

    }
}