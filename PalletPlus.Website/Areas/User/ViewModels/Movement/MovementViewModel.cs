﻿using PalletPlus.Data;
using PalletPlus.Website.Areas.User.ViewModels.Enumerators;
using System;

namespace PalletPlus.Website.Areas.User.ViewModels.Movement
{
    public class MovementViewModel
    {
        public Guid MovementId { get; set; }

        public string Direction { get; set; }

        public DateTime MovementDate { get; set; }

        public DateTime EffectiveDate { get; set; }

        public string MovementType { get; set; }

        public Guid CorrectionKey { get; set; }

        public string DocketNumber { get; set; }

        public string DestinationName { get; set; }

        public string Status { get; set; }

        public MovementViewModel() { }

        public MovementViewModel(Guid movementId, DateTime movementDate, DateTime effectiveDate, string direction, string movementType, string destinationName, string docketNumber)
        {
            MovementId = movementId;
            Direction = direction;
            MovementDate = movementDate;
            MovementType = movementType;
            DestinationName = destinationName;
            DocketNumber = docketNumber;
            Status = CorrectionStatus.None.ToString();
            EffectiveDate = effectiveDate;
        }

        public MovementViewModel(Guid movementId, DateTime movementDate, DateTime effectiveDate, string direction, string movementType, string destinationName, string docketNumber,
            MovementCorrection correction, MovementCancellation cancellation, Guid userKey, bool isPending)
        {
            MovementId = movementId;
            Direction = direction;
            MovementDate = movementDate;
            MovementType = movementType;
            DestinationName = destinationName;
            DocketNumber = docketNumber;
            EffectiveDate = effectiveDate;

            
            if (correction != null)
                SetStatus(correction.IsApproved, correction.CorrectionKey, correction.RequestedBy.Key, userKey);

            if (cancellation != null)
                SetCancellationStatus(cancellation.IsApproved, cancellation.CorrectionKey, cancellation.RequestedBy.Key,
                                      userKey);

            if (isPending)
                Status = "Pending";
        }

        public void SetStatus(bool? status, Guid correctionKey, Guid userWhoCreated, Guid userWhoSees)
        {
            CorrectionKey = correctionKey;
            if (status == null && userWhoCreated == userWhoSees)
            {
                Status = CorrectionStatus.PendingCorrectionAndCreatedByMe.ToString();
                return;
            }

            if (status == null)
                Status = CorrectionStatus.PendingCorrection.ToString();
            else if (status == true)
                Status = CorrectionStatus.Approved.ToString();
            else
                Status = CorrectionStatus.Declined.ToString();
        }

        public void SetCancellationStatus(bool? status, Guid correctionKey, Guid userWhoCreated, Guid userWhoSees)
        {
            CorrectionKey = correctionKey;
            if (status == null && userWhoCreated == userWhoSees)
            {
                Status = CorrectionStatus.PendingCancellationAndCreatedByMe.ToString();
                return;
            }

            if (status == null)
                Status = CorrectionStatus.PendingCancellation.ToString();
            else if (status == true)
                Status = CorrectionStatus.Cancelled.ToString();
            else
                Status = CorrectionStatus.None.ToString();
        }
    }
}