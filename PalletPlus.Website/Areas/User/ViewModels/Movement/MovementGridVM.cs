﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PalletPlus.Website.Areas.User.ViewModels.Movement
{
    public class MovementGridVM
    {
        public string Name { get; set; }
        public int Total { get; set; }
        public IEnumerable<MovementViewModel> MovementsList { get; set; }

        public MovementGridVM(string gridName, int total, IEnumerable<MovementViewModel> movementsList)
        {
            Name = gridName;
            Total = total;
            MovementsList = movementsList;
        }
    }
}