﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PalletPlus.Reports;

namespace PalletPlus.Website.Areas.User.ViewModels.Movement
{
    public class MovementReportViewModel
    {
        public Guid MovementId { get; set; }
        public TransferDocument Report { get; set; }
    }
}