﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PalletPlus.Data;
using PalletPlus.Website.Areas.User.ViewModels.Enumerators;
using PalletPlus.Website.Helpers;

namespace PalletPlus.Website.Areas.User.ViewModels.Exchange
{
    public class ExchangeViewModel
    {
        #region Details

        public Guid ExchangeId { get; set; }

        [Required]
        [DisplayName("Equipment Supplier")]
        public Guid SupplierId { get; set; }

        [DisplayName("Trading Partner")]
        public string TradingPartner { get; set; }

        [DisplayName("Trading Partner")]
        public Guid TradingPartnerID { get; set; }


        [DisplayName("Consignment Note")]
        public string ConNote { get; set; }

        
        [DisplayName("Partner Reference")]
        public string PartnerReference { get; set; }

        [Required]
        [DisplayName("Effective Date")]
        public DateTime EffectiveDate { get; set; }

        

  

       
       

        #endregion

        public Dictionary<string, string> IdToValidate { get; set; }

        public Dictionary<string, int?> EquipmentIn { get; set; } //int - amount; string - eqCode
        public Dictionary<string, int?> EquipmentOut { get; set; }
        public Dictionary<string, string> EquipmentNames { get; set; }

        public Dictionary<Guid, string> Providers { get; set; }
        public Dictionary<string, string> TradingSubjects { get; set; }

        public string PartnerAccountNumber { get; set; }

        public ExchangeViewModel(){}

        public ExchangeViewModel( Supplier[] providers)
        {
            

            var providersDict = providers.ToDictionary(x => x.Id, y => y.CompanyName);
            Providers = new Dictionary<Guid, string>(providersDict);
        }

        public ExchangeViewModel(Guid supplierId,
            Dictionary<string, string> partners)
        {
            
            SupplierId = supplierId;

            TradingSubjects = new Dictionary<string, string>();
            foreach (var partner in partners)
                TradingSubjects.Add(partner.Key.Replace('/', '_'), partner.Value);
            //foreach (var site in sites)
            //    TradingSubjects.Add(site.Key.Replace('/', '_'), site.Value);
            //foreach (var supplier in suppliers)
            //    TradingSubjects.Add("suppliers_" + supplier.Key, supplier.Value);
        }

        public ExchangeViewModel(Guid exchangeId,Guid supplierId,Guid partnerId,string partner,DateTime effectiveDate,string partnerReference)
        {
            SupplierId = supplierId;
            TradingPartnerID = partnerId;
            TradingPartner = partner;
            EffectiveDate = effectiveDate;
            PartnerReference = partnerReference;
            ExchangeId = exchangeId;
        }
        
    }
}