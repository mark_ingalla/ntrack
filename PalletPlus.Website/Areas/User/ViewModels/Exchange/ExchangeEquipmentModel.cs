﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PalletPlus.Website.Areas.User.ViewModels.Exchange
{
    public class ExchangeEquipmentModel
    {
        public string EquipmentCode { get; set; }

        public string EquipmentName { get; set; }

        public int QtySend { get; set; }
        public int QtyReceive { get; set; }
        public int QtyBalance
        {
            get { return QtySend - QtyReceive; }
        }
    }
}