﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PalletPlus.Website.Areas.User.ViewModels.PTAMovements
{
    public class ResolveVM
    {
        public Guid MovementId { get; set; }
        [DisplayName("Movement Date")]
        public DateTime MovementDateUTC { get; set; }
        [DisplayName("Effective Date")]
        public DateTime EffectiveDateUTC { get; set; }
        [DisplayName("PTA Docket Number")]
        public string PTADocketNumber { get; set; }
        [Required]
        [Remote("CheckDocketNumberUniqueness", "PTAMovements", ErrorMessage = "Provided docket number already exists!")]
        [DisplayName("Correct Docket Number")]
        public string NewDocketNumber { get; set; }
        public string Reference { get; set; }
        public string PartnerReference { get; set; }
        public string ConsigmentNote { get; set; }

        public Dictionary<string, int> Equipment { get; set; } //int - amount; string - eqCode
        public Dictionary<string, string> EquipmentNames { get; set; }

        public ResolveVM()
        {}

        public ResolveVM(Data.Movement movement)
        {
            MovementId = movement.Id;
            MovementDateUTC = movement.MovementDate;
            EffectiveDateUTC = movement.EffectiveDate;
            PTADocketNumber = movement.DocketNumber;
           
            Reference = movement.MovementReference;
            PartnerReference = movement.DestinationReference;
            ConsigmentNote = movement.ConsignmentNote;
            Equipment = movement.Equipments.ToDictionary(p => p.Key, p => p.Value.Quantity);
            EquipmentNames = movement.Equipments.ToDictionary(p => p.Key, p => p.Value.EquipmentName);
        }
    }
}