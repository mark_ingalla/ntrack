﻿using PalletPlus.Data;
using PalletPlus.Website.Areas.User.ViewModels.Enumerators;
using System;

namespace PalletPlus.Website.Areas.User.ViewModels.PTAMovements
{
    public class MovementViewModel
    {
        public Guid MovementId { get; set; }

        public string Direction { get; set; }

        public DateTime MovementDate { get; set; }

        public DateTime EffectiveDate { get; set; }

        public string MovementType { get; set; }

        public string DocketNumber { get; set; }

        public string DestinationName { get; set; }

        public MovementViewModel() { }

        public MovementViewModel(Guid movementId, DateTime movementDate, DateTime effectiveDate, string direction, string movementType, string destinationName, string docketNumber)
        {
            MovementId = movementId;
            Direction = direction;
            MovementDate = movementDate;
            MovementType = movementType;
            DestinationName = destinationName;
            DocketNumber = docketNumber;
            EffectiveDate = effectiveDate;
        }
    }
}