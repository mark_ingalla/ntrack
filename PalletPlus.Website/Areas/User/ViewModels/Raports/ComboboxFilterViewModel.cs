﻿using System;
using System.Collections.Generic;

namespace PalletPlus.Website.Areas.User.ViewModels.Raports
{
    public class ComboboxFilterViewModel
    {
        public int OrderNo { get; set; }
        public string FilterTitle { get; set; }
        public string FilterName { get; set; }
        public IEnumerable<object> ComboboxItems { get; set; }

        public string ChosenItemId { get; set; }
        public bool Chosen { get; set; }

        public ComboboxFilterViewModel(string filterTitle, IEnumerable<object> comboboxItems, int orderNo, string filterName, string chosenPartnerId, bool chosen)
        {
            OrderNo = orderNo;
            FilterName = filterName;
            FilterTitle = filterTitle;
            ComboboxItems = comboboxItems;
            ChosenItemId = chosenPartnerId;
            Chosen = chosen;
        }
    }
}