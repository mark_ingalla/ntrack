﻿using PalletPlus.Website.Areas.User.ViewModels.Enumerators;
using System;

namespace PalletPlus.Website.Areas.User.ViewModels.Raports
{
    public class MovementViewModel
    {
        public string MovementId { get; set; }

        public string Direction { get; set; }

        public DateTime MovementDate { get; set; }

        public string MovementType { get; set; }

        public Guid CorrectionKey { get; set; }

        public string DocketNumber { get; set; }

        public string Partner { get; set; }

        public CorrectionStatus Status { get; set; }

        public MovementViewModel() { }

        public MovementViewModel(string movementId, DateTime dt, string direction, string movementType, string partner)
        {
            MovementId = movementId;
            Direction = direction;
            MovementDate = dt;
            MovementType = movementType;
            Partner = partner;
        }


        public void SetStatus(bool? status, Guid correctionKey, Guid userWhoCreated, Guid userWhoSees)
        {
            CorrectionKey = correctionKey;
            if (status == null && userWhoCreated == userWhoSees)
            {
                Status = CorrectionStatus.PendingCorrectionAndCreatedByMe;
                return;
            }

            if (status == null)
                Status = CorrectionStatus.PendingCorrection;
            else if (status == true)
                Status = CorrectionStatus.Approved;
            else
                Status = CorrectionStatus.Declined;
        }

        public void SetCancellationStatus(bool? status, Guid correctionKey, Guid userWhoCreated, Guid userWhoSees)
        {
            CorrectionKey = correctionKey;
            if (status == null && userWhoCreated == userWhoSees)
            {
                Status = CorrectionStatus.PendingCancellationAndCreatedByMe;
                return;
            }

            if (status == null)
                Status = CorrectionStatus.PendingCancellation;
            else if (status == true)
                Status = CorrectionStatus.Cancelled;
            else
                Status = CorrectionStatus.None;
        }
    }
}