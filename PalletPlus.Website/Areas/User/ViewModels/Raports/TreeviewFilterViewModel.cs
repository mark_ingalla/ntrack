﻿using System;
using System.Collections.Generic;

namespace PalletPlus.Website.Areas.User.ViewModels.Raports
{
    public class TreeviewFilterViewModel
    {
        public int OrderNo { get; set; }
        public string FilterTitle { get; set; }
        public string FilterName { get; set; }
        public Dictionary<string, string> FilterValues { get; set; }
        public List<string> SelectedKeys { get; set; }  

        public TreeviewFilterViewModel(string filterTitle, Dictionary<string, string> filterValues, List<string> selectedKeys,  int orderNo, string filterName)
        {
            OrderNo = orderNo;
            SelectedKeys = selectedKeys;
            if (SelectedKeys == null)
                SelectedKeys = new List<string>();

            FilterTitle = filterTitle;
            FilterValues = filterValues;
            FilterName = filterName;
        }
    }
}