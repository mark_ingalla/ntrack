﻿using System;
using PalletPlus.Reports;

namespace PalletPlus.Website.Areas.User.ViewModels.Raports
{
    public class SummaryMovementReportViewModel
    {
        public MovementSummariesReport Report { get; set; }

        public SummaryMovementReportViewModel(MovementSummariesReport report)
        {
            Report = report;
        }
    }
}