﻿using System;
using PalletPlus.Reports;

namespace PalletPlus.Website.Areas.User.ViewModels.Raports
{

    public class StocktakesReportViewModel
    {
        public StocktakesReport Report { get; set; }

        public StocktakesReportViewModel(StocktakesReport report)
        {
            Report = report;
        }
    }
}