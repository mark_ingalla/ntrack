﻿using System;
using System.Collections.Generic;

namespace PalletPlus.Website.Areas.User.ViewModels.Raports
{
    public class DatetimeFilterViewModel
    {
        public int OrderNo { get; set; }
        public string FilterTitle { get; set; }
        public string FilterName { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public bool Chosen { get; set; }

        public DatetimeFilterViewModel(string filterTitle, int orderNo, string filterName, bool chosen, DateTime from, DateTime to)
        {
            OrderNo = orderNo;
            FilterName = filterName;
            FilterTitle = filterTitle;
            Chosen = chosen;
        }
    }
}