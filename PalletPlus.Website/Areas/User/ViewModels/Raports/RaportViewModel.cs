﻿
using PalletPlus.Reports;

namespace PalletPlus.Website.Areas.User.ViewModels.Raports
{
    public class RaportViewModel
    {
        public TreeviewFilterViewModel SiteFilter { get; set; }
        public TreeviewFilterViewModel MovementTypeFilter { get; set; }
        public TreeviewFilterViewModel MovementDirection { get; set; }
        public ComboboxFilterViewModel PartnerFilter { get; set; }
        public DatetimeFilterViewModel DateTimeRangeFilter { get; set; }

        public SummaryMovementReportViewModel SummaryMovementReport { get; set; }
        public DetailedMovementReportViewModel DetailedMovementReport { get; set; }
        public StocktakesReportViewModel StocktakesReport { get; set; }
    }
}