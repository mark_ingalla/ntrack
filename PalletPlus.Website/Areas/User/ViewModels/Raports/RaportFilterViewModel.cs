﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PalletPlus.Website.Areas.User.ViewModels.Raports
{

    public class RaportFilterViewModel
    {
        public bool CheckedAll_Sites { get; set; }
        public Guid[] Checked_Sites { get; set; }
        public bool CheckedAll_MovementTypes { get; set; }
        public string[] Checked_MovementTypes { get; set; }
        public bool CheckedAll_MovementDirections { get; set; }
        public string[] Checked_MovementDirections { get; set; }
        public bool CheckedAll_Partners { get; set; }
        public Guid? PartnerId { get; set; }
        public bool CheckedAll_DateTime { get; set; }
        public string From { get; set; }
        public string To { get; set; }
    }
}