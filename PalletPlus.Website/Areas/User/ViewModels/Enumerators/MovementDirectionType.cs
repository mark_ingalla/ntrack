﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PalletPlus.Website.Areas.User.ViewModels.Enumerators
{
    public enum MovementDirection
    {
        In,
        Out
    }
}