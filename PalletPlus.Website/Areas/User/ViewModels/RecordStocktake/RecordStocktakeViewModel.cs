﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using PalletPlus.Data;
using PalletPlus.Website.Areas.User.ViewModels.Stocktake;

namespace PalletPlus.Website.Areas.User.ViewModels.RecordStocktake
{
    public class RecordStocktakeViewModel
    {
        [Required]
        [DisplayName("Equipment Provider")]
        public Guid EquipmentProvider { get; set; }

        [Required]
        [DisplayName("Stocktake Date")]
        public DateTime StocktakeDate { get; set; }

        public Dictionary<Guid, string> Suppliers { get; set; }

        public Dictionary<string, int?> Equipment { get; set; } //int - amount; string - eqCode
        public Dictionary<string, string> EquipmentNames { get; set; }

        public IEnumerable<StocktakeViewModel> ListOf10LastStocktakes { get; set; } 

        public RecordStocktakeViewModel()
        {
            Suppliers = new Dictionary<Guid, string>();

            Equipment = new Dictionary<string, int?>();
            EquipmentNames = new Dictionary<string, string>();

            ListOf10LastStocktakes = new List<StocktakeViewModel>();
        }

        public RecordStocktakeViewModel(Dictionary<Guid, string> suppliers, IEnumerable<StocktakeViewModel> last10Stocktakes)
        {
            Suppliers = suppliers;
            ListOf10LastStocktakes = last10Stocktakes;
        }
    }
}