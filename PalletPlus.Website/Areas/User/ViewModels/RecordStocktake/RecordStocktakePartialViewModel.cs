﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using PalletPlus.Data;

namespace PalletPlus.Website.Areas.User.ViewModels.RecordStocktake
{
    public class RecordStocktakePartialViewModel
    {
        public DateTime StocktakeDate { get; set; }
        public Guid EquipmentProvider { get; set; }

        public Dictionary<string, int?> Equipment { get; set; } //int - amount; string - eqCode
        public Dictionary<string, string> EquipmentNames { get; set; } 

        public RecordStocktakePartialViewModel()
        {
            Equipment = new Dictionary<string, int?>();
            EquipmentNames = new Dictionary<string, string>();
        }
    }
}