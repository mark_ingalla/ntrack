﻿//using AttributeRouting.Web.Mvc;
//using PalletPlus.Data;
//using PalletPlus.Website.Areas.User.ViewModels.RecordStocktake;
//using PalletPlus.Website.Areas.User.ViewModels.TransporterReturn;
//using Raven.Client;
//using Raven.Client.Linq;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web.Mvc;

//namespace PalletPlus.Website.Areas.User.Controllers
//{
//    public class TransporterReturnController : UserBaseController
//    {
//        readonly IDocumentSession _session;

//        public TransporterReturnController(IDocumentSession session)
//        {
//            _session = session;
//        }

//        [GET("TransporterReturn")]
//        public ActionResult Index()
//        {
//            var contextSite = _session.Load<Site>(base.ContextSiteID);
//            var contextSiteSuppliers = contextSite.Suppliers.Select(x => x.Value.SiteId);
//            var providers = _session.Query<Supplier>().Where(x => x.IdString.In(contextSiteSuppliers));
//            var vm = new TransporterReturnViewModel(providers);

//            return View(vm);
//        }

//        [POST("TransporterReturn/GetCascadeTransporters/{id}")]
//        public PartialViewResult GetCascadeTransporters(Guid id)
//        {
//            var vm = new TransporterReturnViewModel();
//            vm.Transporters = _session.Query<Transporter>().ToDictionary(x => x.Id, y => y.CompanyName);

//            return PartialView("_TransporterReturnComboboxPartial", vm);
//        }

//        [POST("TransporterReturn/GetCascadeDetails/{id}")]
//        public PartialViewResult GetCascadeDetails(Guid id)
//        {
//            //var equipmentTransfered = _session.Load<Site>(ContextSiteID).EquipmentTransferBalance;
//            var supplierEquipment = _session.Load<Supplier>(id).Equipments.Where(x => !x.Value.IsDeleted);

//            var model = new TransporterReturnViewModel();
//            //model.AssetNames =
//            //    supplierEquipment.Where(x => x.Value.EquipmentCode.In(equipmentTransfered.Keys))
//            //    .ToDictionary(y => y.Value.EquipmentCode, z => z.Value.EquipmentName);

//            model.Assets = new Dictionary<string, int?>();
//            foreach (var e in model.AssetNames.Keys)
//            {
//                model.Assets.Add(e, null);
//            }

//            return PartialView("_TransporterReturnDetailsPartial", model);
//        }

//        [POST("TransporterReturn/Save")]
//        public ActionResult Save(RecordStocktakeViewModel model)
//        {


//            Failure("Not implemented!");
//            return RedirectToAction("Index");
//        }
//    }
//}