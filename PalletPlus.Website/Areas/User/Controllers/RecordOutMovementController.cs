﻿using AttributeRouting.Web.Mvc;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Website.Areas.User.ViewModels.Enumerators;
using PalletPlus.Website.Areas.User.ViewModels.RecordMovement;
using Raven.Client;
using Raven.Client.Linq;
using System.Linq;
using System.Web.Mvc;
using nTrack.Data.Extenstions;

namespace PalletPlus.Website.Areas.User.Controllers
{
    public class RecordOutMovementController : RecordMovementBaseController
    {
        public RecordOutMovementController(IDocumentSession session)
            : base(session)
        {
        }

        [GET("RecordOutMovement")]
        public ActionResult Index()
        {
            var contextSiteIdString = ContextSiteID.ToStringId<Site>();
            var companyIdString = Identity.AccountId.ToStringId<Account>();

            var suppliers = _session.Query<SiteSupplierIndex.Result, SiteSupplierIndex>()
                .Where(x => x.SiteId == contextSiteIdString || x.SiteId == companyIdString)
                .ToList()
                .Select(x => x.SupplierId)
                .Distinct()
                .ToArray();

            var vm = new RecordMovementViewModel(MovementDirection.Out, _session.Load<Supplier>(suppliers));

            return View(vm);
        }
    }
}