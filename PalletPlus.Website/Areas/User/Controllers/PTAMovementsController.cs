﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Commands.Movement;
using PalletPlus.Website.Areas.User.ViewModels.PTAMovements;
using Raven.Client;
using nTrack.Data.Extenstions;
using MovementViewModel = PalletPlus.Website.Areas.User.ViewModels.Movement.MovementViewModel;

namespace PalletPlus.Website.Areas.User.Controllers
{
    public class PTAMovementsController : UserBaseController
    {
        readonly IDocumentSession _session;

        public PTAMovementsController(IDocumentSession session)
        {
            _session = session;
        }
        //
        // GET: /User/PTAMovements/

        [GET("PTAMovements")]
        public ActionResult Index()
        {
            return View();
        }

        [GET("PTAMovements/Resolve")]
        public ActionResult ResolvePTA(Guid movementId)
        {
            var movement = _session.Load<Movement>(movementId);

            if (movement != null)
            {
                var vm = new ResolveVM(movement);

                return View(vm);
            }

            return HttpNotFound();

        }

        [GET("PTAMovements/CheckDocketNumberUniqueness")]
        public JsonResult CheckDocketNumberUniqueness(string newDocketNumber)
        {
            bool result = true;
            string contextSiteIdString = ContextSiteID.ToStringId<Site>();
            if (!string.IsNullOrWhiteSpace(newDocketNumber))
            {
                result = !_session.Query<MovementIndex.Result, MovementIndex>()
                    .Any(p => p.DocketNumber == newDocketNumber && p.SiteId == contextSiteIdString);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [POST("PTAMovements/Resolve")]
        public ActionResult ResolvePTA(ResolveVM vm)
        {
            var movement = _session.Load<Movement>(vm.MovementId);

            if (movement != null)
            {
                Bus.Send(new ResolvePTAMovement()
                             {
                                 MovementId = movement.Id,
                                 EffectiveDateUTC = vm.EffectiveDateUTC,
                                 Equipments = vm.Equipment,
                                 DocketNumber = vm.NewDocketNumber.ToUpper()
                             });

                return RedirectToAction("Index");
            }

            return HttpNotFound();
        }

        [POST("GetPTAMovements")]
        public ActionResult GetPTAMovements([DataSourceRequest()]DataSourceRequest request)
        {
            DataSourceResult result = null;

            var contextSiteIdString = ContextSiteID.ToStringId<Site>();

            if (request.Sorts == null)
            {
                request.Sorts = new List<SortDescriptor>();
                request.Sorts.Add(new SortDescriptor("MovementDate", ListSortDirection.Descending));
            }

            if (request.PageSize == 0) request.PageSize = 10;


            result = _session.Query<Movement>().Where(
                x =>
                x.SiteId == contextSiteIdString && x.IsPendingPTA).ToDataSourceResult(
                request, x => new MovementViewModel(x.Id, x.MovementDate, x.EffectiveDate, x.Direction == "Out" ? "Sent" : "Received", x.MovementType, x.DestinationName,
                                                   x.DocketNumber));


            return Json(result, JsonRequestBehavior.AllowGet);
        }



    }
}
