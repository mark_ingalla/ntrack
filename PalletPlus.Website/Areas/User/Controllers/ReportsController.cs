﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using nTrack.Core.Enums;
using nTrack.Data.Extenstions;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Reports;
using PalletPlus.Website.Areas.User.ViewModels.Raports;
using PalletPlus.Website.Services;
using Raven.Abstractions.Data;
using Raven.Client;
using Raven.Client.Indexes;
using Raven.Client.Linq;
using nTrack.Extension;

namespace PalletPlus.Website.Areas.User.Controllers
{

    public class ReportsController : UserBaseController
    {
        readonly IDocumentSession _session;
        readonly IAttachmentService _attachmentService;

        public ReportsController(IDocumentSession session, IAttachmentService attachmentService)
        {
            _session = session;
            _attachmentService = attachmentService;
        }

        [GET("MovementReports")]
        public ActionResult Index()
        {
            var vm = new RaportViewModel();

            Session["report-filter"] = null;

            RaportFilterViewModel raportFilterViewModel = new RaportFilterViewModel();
            raportFilterViewModel.Checked_Sites = new Guid[] { ContextSiteID };

            //vm.SiteFilter = new TreeviewFilterViewModel("Sites", GetSites(), null, 0, "Sites");
            vm.MovementTypeFilter = new TreeviewFilterViewModel("Movement Types", GetMovementTypes(), null, 1, "MovementTypes");
            vm.MovementDirection = new TreeviewFilterViewModel("Movement Direction", GetMovementDirections(), null, 2, "MovementDirections");
            vm.PartnerFilter = new ComboboxFilterViewModel("Partners", GetPartners(), 3, "Partners", null, false);
            vm.DateTimeRangeFilter = new DatetimeFilterViewModel("Date", 4, "DateTime", false, new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, 1), DateTime.UtcNow);



            //vm.SummaryMovementReport = SummaryMovementReport(raportFilterViewModel);
            //vm.DetailedMovementReport = DetailedMovementReport(raportFilterViewModel);
            //vm.StocktakesReport = StocktakesReport(raportFilterViewModel);

            return View(vm);
        }

        [POST("GetReport")]
        public ActionResult GetReport(string tab)
        {
            RaportFilterViewModel raportFilterViewModel = Session["report-filter"] as RaportFilterViewModel;

            if (raportFilterViewModel == null)
            {
                raportFilterViewModel = new RaportFilterViewModel();
            }
            raportFilterViewModel.Checked_Sites = new Guid[] { ContextSiteID };


            if (tab == "Report1")
            {
                var report = SummaryMovementReport(raportFilterViewModel);
                return PartialView("_MovementSummaryReport", report);
            }
            if (tab == "Report3")
            {
                var report = DetailedMovementReport(raportFilterViewModel);
                return PartialView("_DetailedMovementReport", report);
            }
            if (tab == "Report4")
            {
                var report = StocktakesReport(raportFilterViewModel);
                return PartialView("_StocktakesReport", report);
            }
            if (tab == "Report5")
            {
                var report = HireMovementBalanceReport(raportFilterViewModel);
                return PartialView("_HireMovementBalanceReport", report);
            }

            return null;
        }

        [POST("GetFiltered")]
        public void GetFiltered(RaportFilterViewModel vm)
        {
            Session["report-filter"] = vm;
        }

        private Dictionary<string, string> GetMovementTypes()
        {
            var types = _session.Query<MovementIndex.Result, MovementIndex>()
                .Select(x => new { x.MovementType })
                .Distinct()
                .ToList();

            return types.ToDictionary(x => x.MovementType, y => y.MovementType);
        }

        private Dictionary<string, string> GetMovementDirections()
        {
            Dictionary<string, string> res = new Dictionary<string, string>();
            //res.Add("In", "In");
            //res.Add("Out", "Out");
            res.Add("In", "Receive");
            res.Add("Out", "Send");
            return res;
        }

        private IEnumerable<object> GetPartners()
        {
            var contextSiteId = ContextSiteID.ToStringId<Site>();

            var partners = _session.Query<Partner>(typeof(PartnerIndex).Name)
                .Where(x => !x.IsDeleted && x.Sites.Any(p => p == contextSiteId))
                .Select(x => new { Id = x.Id, PartnerName = x.CompanyName })
                .ToList().OrderBy(p => p.PartnerName);

            return partners.Select(x => new { x.Id, x.PartnerName });
        }

        private void Filter<T1>(ref IDocumentQuery<T1> results, RaportFilterViewModel filter) where T1 : IRaportIndexResult
        {
            if (filter != null)
            {
                var movementTypesCount = filter.Checked_MovementTypes != null ? filter.Checked_MovementTypes.Count() : 0;
                var directionTypesCount = filter.Checked_MovementDirections != null
                                              ? filter.Checked_MovementDirections.Count()
                                              : 0;
                var sitesCount = filter.Checked_Sites != null ? filter.Checked_Sites.Count() : 0;


                if (movementTypesCount > 0)
                {
                    string names = "";

                    for (int i = 0; i < movementTypesCount; i++)
                    {
                        if (i == 0)
                        {
                            names += filter.Checked_MovementTypes[i];
                        }
                        else
                        {
                            names += " OR " + filter.Checked_MovementTypes[i];
                        }
                    }

                    results = results.Where("MovementType: (" + names + ")");

                }

                if (directionTypesCount > 0)
                {
                    string names = "";

                    for (int i = 0; i < directionTypesCount; i++)
                    {
                        if (i == 0)
                        {
                            names += filter.Checked_MovementDirections[i];
                        }
                        else
                        {
                            names += " OR " + filter.Checked_MovementDirections[i];
                        }
                    }

                    results = results.Where("MovementDirection: (" + names + ")");

                }

                if (sitesCount > 0)
                {
                    string names = "";

                    for (int i = 0; i < sitesCount; i++)
                    {
                        if (i == 0)
                        {
                            names += "sites/" + filter.Checked_Sites[i];
                        }
                        else
                        {
                            names += " OR " + "sites/" + filter.Checked_Sites[i];
                        }
                    }

                    results = results.Where("SiteId: (" + names + ")");
                }

                if (filter.CheckedAll_Partners)
                {
                    if (filter.PartnerId != null && filter.PartnerId != Guid.Empty)
                    {
                        results = results.Where("PartnerId: (partners/" + filter.PartnerId + ")");
                    }
                }

                var from = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, 1, 0, 0, 0);
                var to = DateTime.UtcNow.Date.Add(new TimeSpan(0, 23, 59, 59));

                if (filter.CheckedAll_DateTime)
                {
                    from = DateTime.Parse(filter.From);
                    to = DateTime.Parse(filter.To).Add(new TimeSpan(0, 23, 59, 59));
                }

                results = results.WhereBetweenOrEqual(p => p.MovementDate, from, to);

            }
        }

        private SummaryMovementReportViewModel SummaryMovementReport(RaportFilterViewModel filter = null)
        {

            var results = _session.Advanced.LuceneQuery<ReportingMovementSummariesIndex.Result, ReportingMovementSummariesIndex>()
                                  .UsingDefaultOperator(QueryOperator.And);

            Filter(ref results, filter);


            var source = new MovementSummariesReport();

            var ret = TakeAll(results);

            var rres = (from r in ret
                        group r by new { r.SiteId, r.PartnerId, r.MovementType, r.MovementDirection, r.EquipmentCode }

                            into gr
                            select new ReportingMovementSummariesIndex.Result()
                            {
                                EquipmentCode = gr.Key.EquipmentCode,
                                SiteId = gr.Key.SiteId,
                                SiteName = gr.First().SiteName,

                                Quantity = gr.Sum(x => x.Quantity),
                                MovementType = gr.Key.MovementType,
                                EquipmentName = gr.First().EquipmentName,
                                MovementDirection = gr.Key.MovementDirection,

                                PartnerId = gr.Key.PartnerId,
                                PartnerName = gr.First().PartnerName,

                                SupplierId = gr.First().SupplierId,
                                SupplierName = gr.First().SupplierName
                            }).OrderBy(x => x.SiteName).ThenBy(x => x.EquipmentName)
                                       .ThenBy(x => x.MovementDirection)
                                       .ThenBy(x => x.PartnerName)
                                       .ToList();

            source.DataSource = rres;

            System.Drawing.Image logo = _attachmentService.GetImageFromSystemDb(Identity.AccountId, AttachmentEntities.AccountLogos.ToString());
            source.SetLogo(logo);

            return new SummaryMovementReportViewModel(source);
        }

        private HireMovementBalanceReportViewModel HireMovementBalanceReport(RaportFilterViewModel filter = null)
        {

            var results =
                _session.Advanced.LuceneQuery<HireBalanceIndex.Result, HireBalanceIndex>().UsingDefaultOperator(QueryOperator.And);

            Filter(ref results, filter);

            var source = new HireMovementBalanceReport();

            var ret = TakeAll(results);


            var rres = (from r in ret
                        group r by new { r.SiteId, r.SupplierId, r.EquipmentCode, r.MovementDate.Month, r.MovementDate.Year }

                            into gr
                            select new HireBalanceIndex.Result()
                            {

                                EquipmentCode = gr.Key.EquipmentCode,
                                SiteId = gr.Key.SiteId,
                                SiteName = gr.First().SiteName,
                                EquipmentName = gr.First().EquipmentName,
                                SupplierId = gr.Key.SupplierId,
                                SupplierName = gr.First().SupplierName,
                                CorrectionsIn = gr.Sum(p => p.CorrectionsIn),
                                CorrectionsOut = gr.Sum(p => p.CorrectionsOut),
                                TransfersIn = gr.Sum(p => p.TransfersIn),
                                TransfersOut = gr.Sum(p => p.TransfersOut),
                                Issues = gr.Sum(p => p.Issues),
                                Returns = gr.Sum(p => p.Returns),
                                Year = gr.Key.Year,
                                Month = gr.Key.Month,
                                ClosingBalance = gr.Sum(p => p.ClosingBalance)
                            }).OrderBy(x => x.SiteName).ThenBy(x => x.SupplierName)
                                       .ThenBy(x => x.EquipmentName)
                                       .ToList();

            source.DataSource = rres;

            System.Drawing.Image logo = _attachmentService.GetImageFromSystemDb(Identity.AccountId, AttachmentEntities.AccountLogos.ToString());
            source.SetLogo(logo);

            return new HireMovementBalanceReportViewModel(source);
        }


        private DetailedMovementReportViewModel DetailedMovementReport(RaportFilterViewModel filter = null)
        {

            var results =
               _session.Advanced.LuceneQuery<ReportingMovementIndexResult, ReportingMovementIndex>()
                   .UsingDefaultOperator(QueryOperator.And);


            Filter(ref results, filter);

            var ret = TakeAll(results);

            var source = new DetailedMovementReport();
            source.DataSource = ret;

            System.Drawing.Image logo = _attachmentService.GetImageFromSystemDb(Identity.AccountId, AttachmentEntities.AccountLogos.ToString());
            source.SetLogo(logo);

            return new DetailedMovementReportViewModel(source);
        }

        private StocktakesReportViewModel StocktakesReport(RaportFilterViewModel filter = null)
        {
            var results =
                _session.Advanced.LuceneQuery<StocktakeIndex.Result, StocktakeIndex>()
                    .UsingDefaultOperator(QueryOperator.And);

            if (filter != null)
            {
                var sitesCount = filter.Checked_Sites != null ? filter.Checked_Sites.Count() : 0;

                if (sitesCount > 0)
                {
                    string names = "";

                    for (int i = 0; i < sitesCount; i++)
                    {
                        if (i == 0)
                        {
                            names += "sites/" + filter.Checked_Sites[i];
                        }
                        else
                        {
                            names += " OR " + "sites/" + filter.Checked_Sites[i];
                        }
                    }

                    results = results.Where("SiteId: (" + names + ")");
                }

                var from = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, 1);
                var to = DateTime.UtcNow.Date.Add(new TimeSpan(0, 23, 59, 59));

                if (filter.CheckedAll_DateTime)
                {
                    from = DateTime.Parse(filter.From);
                    to = DateTime.Parse(filter.To).Add(new TimeSpan(0, 23, 59, 59));
                }

                results = results.WhereBetweenOrEqual(p => p.StocktakeDate, from, to);
            }

            var ret = TakeAll(results);

            var source = new StocktakesReport { DataSource = ret };

            System.Drawing.Image logo = _attachmentService.GetImageFromSystemDb(Identity.AccountId, AttachmentEntities.AccountLogos.ToString());
            source.SetLogo(logo);

            return new StocktakesReportViewModel(source);
        }

        //[GET("MovementReports/Sites")]
        //public JsonResult Sites()
        //{
        //    var sites = _session.Query<SiteIndex.Result, SiteIndex>()
        //        .Select(x => new { x.SiteId, x.SiteName })
        //        .ToList();

        //    return Json(sites, JsonRequestBehavior.AllowGet);
        //}

        private IEnumerable<T1> TakeAll<T1>(IDocumentQuery<T1> results)
        {
            RavenQueryStatistics stats;
            var ret = new List<T1>();

            while (true)
            {
                var x = ret.Count();
                var r1 = results
                    .Statistics(out stats).Skip(x).Take(int.MaxValue).ToList();
                ret.AddRange(r1);

                if (r1.Count == 0 || ret.Count == stats.TotalResults)
                    return ret;
            }
        }




        private IEnumerable<T1> QueryForReportData<T1, T2>(IDocumentSession session) where T2 : AbstractIndexCreationTask, new()
        {
            RavenQueryStatistics stats;
            var ret = new List<T1>();

            while (true)
            {
                var x = ret.Count();
                var r1 = session.Query<T1, T2>()
                    .Statistics(out stats).Skip(x).Take(int.MaxValue).ToList();
                ret.AddRange(r1);

                if (r1.Count == 0 || ret.Count == stats.TotalResults)
                    return ret;
            }
        }
    }

}
