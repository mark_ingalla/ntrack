﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using nTrack.Data.Extenstions;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Commands.Stocktake;
using PalletPlus.Website.Areas.User.ViewModels.RecordStocktake;
using PalletPlus.Website.Areas.User.ViewModels.Stocktake;
using Raven.Client;
using Raven.Client.Linq;

namespace PalletPlus.Website.Areas.User.Controllers
{
    public class RecordStocktakeController : UserBaseController
    {
        readonly IDocumentSession _session;

        public RecordStocktakeController(IDocumentSession session)
        {
            _session = session;
        }

        [GET("RecordStocktake")]
        public ActionResult Index()
        {
            List<string> supplierIds = new List<string>();

            var contextSite = _session.Load<Site>(ContextSiteID);
            if (contextSite != null)
            {
                supplierIds.AddRange(contextSite.Suppliers.Select(x => x.Key));

            }
            var account = _session.Load<Account>(Identity.AccountId);
            if (account != null)
            {
                supplierIds.AddRange(account.Suppliers.Select(x => x.Key));
            }
            var suppliers = _session.Load<Supplier>(supplierIds.Distinct()).ToDictionary(x => x.Id, y => y.CompanyName);

            var last10Stocktakes = GetStocktakes();

            var vm = new RecordStocktakeViewModel(suppliers, last10Stocktakes);

            return View(vm);
        }

        [POST("RecordStocktake/GetCascadeEquipment/{id}")]
        public PartialViewResult GetCascadeEquipment(Guid id)
        {
            var supplierId = id;
            var model = new RecordStocktakePartialViewModel();

            var supplier = _session.Load<Supplier>(id);
            if (supplier != null)
            {
                var equipment = supplier.Equipments.Select(x => x.Value).Where(x => !x.IsDeleted);

                model.EquipmentNames = equipment
                    .ToDictionary(x => x.EquipmentCode, y => y.EquipmentName);
                model.Equipment = equipment
                    .ToDictionary(x => x.EquipmentCode, y => (int?)null);

                model.EquipmentProvider = supplierId;
            }
            return PartialView("_RecordStocktakePartial", model);
        }

        [POST("RecordStocktake/Save")]
        public ActionResult Save(RecordStocktakeViewModel model)
        {
            var stocktakeId = Guid.NewGuid();

            var cmds = new List<object>();

            cmds.Add(new RecordStocktake
                {
                    StocktakeId = stocktakeId,
                    SiteId = ContextSiteID,
                    StocktakeDate = model.StocktakeDate,
                    SupplierId = model.EquipmentProvider
                });

            //Bus.SendMap<RecordStocktake>(cmd =>
            //                                 {
            //                                     cmd.StocktakeId = stocktakeId;
            //                                     cmd.SiteId = ContextSiteID;
            //                                     cmd.StocktakeDate = model.StocktakeDate;
            //                                     cmd.SupplierId = model.EquipmentProvider;
            //                                 });

            foreach (var asset in model.Equipment)
            {
                if (asset.Value == null || asset.Value <= 0) continue;
                var equipmentCode = asset.Key;
                var quantity = (int)asset.Value;
                cmds.Add(new CountStock
                    {
                        CountKey = Guid.NewGuid(),
                        EquipmentCode = equipmentCode,
                        Quantity = quantity,
                        StocktakeId = stocktakeId,
                        SupplierId = model.EquipmentProvider,
                        SiteId = ContextSiteID
                    });
                //Bus.Send<CountStock>(cmd =>
                //                         {
                //                             cmd.CountKey = Guid.NewGuid();
                //                             cmd.EquipmentCode = equipmentCode;
                //                             cmd.Quantity = quantity;
                //                             cmd.StocktakeId = stocktakeId;
                //                             cmd.SupplierId = model.EquipmentProvider;
                //                             cmd.SiteId = ContextSiteID;
                //                         });
            }

            Bus.Send(cmds.ToArray());

            ThreadingService.ObjectCreated<Stocktake>(stocktakeId);

            Success();

            return RedirectToAction("Index", "RecordStocktake");
        }

        private IEnumerable<StocktakeViewModel> GetStocktakes()
        {
            var stocktakes = _session.Query<StocktakeIndex.Result, StocktakeIndex>()
                .Where(x => x.SiteId == ContextSiteID.ToStringId<Site>())
                .OrderByDescending(x => x.StocktakeDate)
                .Take(10)
                .ToList();

            var ret = stocktakes.Select(x =>
                                        new StocktakeViewModel(x.StocktakeId.ToGuidId(), x.StocktakeDate,
                                                               x.EquipmentCode, x.EquipmentName, x.Quantity,
                                                               x.GotPendingCorrection, x.IsApproved,
                                                               x.CorrectionRequestedBy, Identity.UserKey));

            return ret;
        }
    }
}