﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Commands.Exchange;
using PalletPlus.Website.Areas.User.ViewModels.Enumerators;
using PalletPlus.Website.Areas.User.ViewModels.Exchange;
using PalletPlus.Website.Areas.User.ViewModels.RecordMovement;
using PalletPlus.Website.Helpers;
using Raven.Client;
using Raven.Client.Linq;
using nTrack.Data.Extenstions;
using nTrack.Extension;

namespace PalletPlus.Website.Areas.User.Controllers
{
    public class ExchangeController : UserBaseController
    {
        private readonly IDocumentSession _session;

        public ExchangeController(IDocumentSession session)
        {
            _session = session;
        }

        [GET("Exchanges")]
        public ActionResult Index()
        {
            return View(GetExchanges());
        }

        [GET("Exchange")]
        public ActionResult Create()
        {
            var contextSiteIdString = ContextSiteID.ToStringId<Site>();
            var companyIdString = Identity.AccountId.ToStringId<Account>();

            var suppliers = _session.Query<SiteSupplierIndex.Result, SiteSupplierIndex>()
                .Where(x => x.SiteId == contextSiteIdString || x.SiteId == companyIdString)
                .ToList()
                .Select(x => x.SupplierId)
                .Distinct()
                .ToArray();

            var vm = new ExchangeViewModel(_session.Load<Supplier>(suppliers));

            return View(vm);
        }

        [POST("Exchange/Save")]
        public ActionResult Save(ExchangeViewModel model)
        {
            if ((model.EquipmentIn != null && model.EquipmentIn.Any()) || (model.EquipmentOut != null && model.EquipmentOut.Any()))
            {
                var assetsIn = model.EquipmentIn
                    .Where(x => x.Value != null)
                    .ToDictionary(x => x.Key, y => (int)y.Value);

                 var assetsOut = model.EquipmentOut
                    .Where(x => x.Value != null)
                    .ToDictionary(x => x.Key, y => (int)y.Value);

                Bus.Send<CreateExchange>(
                    cmd =>
                        {
                            cmd.ExchangeId = Guid.NewGuid();
                            cmd.ConsignmentReference = model.ConNote;
                            cmd.PartnerReference = model.PartnerReference;
                            cmd.EffectiveDate = model.EffectiveDate;
                            cmd.EquipmentsIn = assetsIn;
                            cmd.EquipmentsOut = assetsOut;
                            cmd.SiteId = ContextSiteID;
                            cmd.SupplierId = model.SupplierId;
                            cmd.PartnerId = model.TradingPartnerID;

                        }
                    );

                Success();
            }
            else
            {
                Failure("There are no assets");
            }

            return RedirectToAction("Index","Exchange");
        }

        [POST("Exchange/GetPartners/{id}")]
        public PartialViewResult GetPartners(Guid id)
        {
            var supplierId = id.ToStringId<Supplier>();
            var contextSiteId = ContextSiteID.ToStringId<Site>();

            var partnersInSite = _session.Query<Partner>(typeof(PartnerIndex).Name)
                .Where(x => x.Sites.Any(e => e == contextSiteId))
                .Select(x => new { x.IdString, x.CompanyName, Suppliers = x.Suppliers })
                .ToList();

            //Site <=> Partner (Partners for combobox)
            var partners = partnersInSite
                .Where(x => x.Suppliers.Keys.Any(e => e == supplierId)
                && x.Suppliers.Values.Any(f=>f.IsExchangeEnabled))
                .ToDictionary(x => x.IdString, y => y.CompanyName);

           // var sites = new Dictionary<string, string>();
            //var account = _session.Load<Account>(Identity.AccountId);
            //if (account != null)
            //{
            //    var isSupplierInCompany = account.Suppliers.Select(x => x.Value).Any(x => x.CompanyId == supplierId);
            //    if (isSupplierInCompany)
            //    {
            //        //Site <=> Site (All sites for combobox, because: isSupplierInCompany => all sites inherit it)
            //        sites = _session.Query<SiteIndex.Result, SiteIndex>()
            //            .Where(x => x.SiteId != contextSiteId)
            //            .ToDictionary(x => x.SiteId, y => y.AccountName + " - " + y.SiteName);
            //    }
            //    else
            //    {
            //        var sitesWithSupplier = _session.Query<SiteIndex.Result, SiteIndex>()
            //            .Select(x => new { x.SiteId, x.SiteName, x.Suppliers, x.AccountName })
            //            .ToList();

            //        //Site <=> Site (Sites for combobox that are associated with supplier)
            //        sites = sitesWithSupplier
            //            .Where(x => x.SiteId != contextSiteId && x.Suppliers.ContainsKey(supplierId))
            //            .ToDictionary(x => x.SiteId, y => y.AccountName + " - " + y.SiteName);
            //    }
            //}

            //Site <=> Supplier (for issue/return) choose locations from a supplier
            //var suppliers = new Dictionary<Guid, string>();
            //var supplier = _session.Load<Supplier>(supplierId);
            //if (supplier != null)
            //{
            //    suppliers = supplier.Locations.Where(p => !p.Value.IsDeleted).ToDictionary(x => x.Key, y => supplier.CompanyName + " - " + y.Value.CompanyName);
            //}

            var vm = new ExchangeViewModel( id, partners);

            return PartialView("_TradingPartnerComboboxPartial", vm);
        }


        [POST("Exchange/GetAccountNumbers/{supplierId}/{partnerId}")]
        public ActionResult GetAccountNumbers(Guid supplierId, string partnerId)
        {
            var model = new ExchangeViewModel();

           
            model.SupplierId = supplierId;
            model.TradingPartner = partnerId;

            //model.Transporters = _session.Query<TransporterIndex.Result, TransporterIndex>()
            //    .ToDictionary(x => x.TransporterId, y => y.TransporterName);

            var supplier = _session.Load<Supplier>(supplierId);
            if (supplier != null)
            {
                model.EquipmentNames = supplier.Equipments.Select(x => x.Value).Where(p => !p.IsDeleted)
                       .ToDictionary(x => x.EquipmentCode, y => y.EquipmentName);
            }

            PartialViewResult ret = new PartialViewResult();

            model.IdToValidate = new Dictionary<string, string>();

            var account = DSession.Load<Account>(Identity.AccountId);

            foreach (var requirement in account.RequiredFields)
            {
                
                model.IdToValidate.Add(requirement, nTrack.Extension.StringExtensions.ToSeparateWords(requirement) + " is required.");
            }

            var subject = model.TradingPartner.Split('_');
       
            ret = TradingPartnerIsPartner(model, Guid.Parse(subject[1]));
       

            ModelState.Clear();

            return ret;
        }

        [POST("Exchange/GetExchangetAssets")]
        public ActionResult GetMovementAssets(Guid exchangeId, [DataSourceRequest] DataSourceRequest request)
        {
            var exchange = _session.Load<Exchange>(exchangeId);
            var result = new List<ExchangeEquipmentModel>();

            if (exchange != null)
            {
                result = (from p in exchange.EquipmentsIn
                                join q in exchange.EquipmentsOut on p.Value.EquipmentName equals q.Value.EquipmentName
                                select new ExchangeEquipmentModel
                                           {
                                               EquipmentName = p.Value.EquipmentName,
                                               QtySend = q.Value.Quantity,
                                               QtyReceive = p.Value.Quantity
                                           }).ToList();

                

            }
            return Json(result.ToDataSourceResult(request));

        }
        

        private PartialViewResult TradingPartnerIsPartner(ExchangeViewModel model, Guid tradingSubjectId)
        {
            model.TradingPartnerID = tradingSubjectId;

            var supplierOfAPartner = _session.Load<Partner>(model.TradingPartnerID)
                                             .Suppliers.Select(x => x.Value)
                                             .FirstOrDefault(x => x.SupplierId == model.SupplierId.ToStringId<Supplier>());

            if (supplierOfAPartner != null)
            {
                var equipmentIn = new HashSet<string>();
                var equipmentOut = new HashSet<string>();

                var supplier = _session.Load<Supplier>(supplierOfAPartner.SupplierId);
                foreach (var equipment in supplierOfAPartner.Equipments)
                {
                    if (supplier.Equipments.ContainsKey(equipment))
                    {
                        if (!supplier.Equipments[equipment].IsDeleted)
                        {
                            equipmentIn.Add(equipment);
                            equipmentOut.Add(equipment);
                        }
                    }
                }

                model.PartnerAccountNumber = supplierOfAPartner.AccountNumber;


                model.EquipmentIn = equipmentIn.ToDictionary(x => x, y => (int?)null);
                model.EquipmentOut = equipmentOut.ToDictionary(x => x, y => (int?)null);
            }

            return PartialView("_ExchangePartial", model);
        }

        IEnumerable<ExchangeViewModel> GetExchanges()
        {
            var contextSiteId = ContextSiteID.ToStringId<Site>();

            var exchanges = _session.Query<ExchangeIndex.Result, ExchangeIndex>().Where(p => p.SiteId == contextSiteId)
                .OrderByDescending(x => x.EffectiveDate)
                .ToList();

            var ret = exchanges.Select(x =>
                                        new ExchangeViewModel(x.ExchangeId.ToGuidId(), x.SupplierId.ToGuidId(),x.PartnerId.ToGuidId(),x.PartnerName,x.EffectiveDate,x.PartnerReference));

            return ret;
        }
     



    }
}
