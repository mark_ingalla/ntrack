﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using nTrack.Data.Extenstions;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Commands.Movement;
using PalletPlus.Reports;
using PalletPlus.Website.Areas.User.ViewModels.Enumerators;
using PalletPlus.Website.Areas.User.ViewModels.Movement;
using Raven.Client;
using Raven.Client.Linq;

namespace PalletPlus.Website.Areas.User.Controllers
{
    public class MovementsController : UserBaseController
    {
        readonly IDocumentSession _session;

        public MovementsController(IDocumentSession session)
        {
            _session = session;
        }

        [GET("Movements")]
        public ActionResult Index()
        {
            SetTabValue("recent");

            return View();
        }

        [POST("GetGrid")]
        public ActionResult GetGrid(string tab)
        {
            if (tab != null)
                SetTabValue(tab.ToLower());

            if (tab == "Search")
                return PartialView("_SearchGridAjax", tab);

            return PartialView("_MovementsGridAjax", tab);
        }

        [POST("GetMovements")]
        public ActionResult GetMovements([DataSourceRequest()]DataSourceRequest request, string tab)
        {
            DataSourceResult result = null;

            if (tab == "Recent")
                result = GetRecentMovements(request);
            else if (tab == "Pending")
                result = GetPendingMovements(request);
            else if (tab == "Historic")
                result = GetHistoricMovements(request);
            else if (tab == "Search")
                result = GetSearchMovements(request);

            if (tab != null)
                SetTabValue(tab.ToLower());

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [GET("Movements/RequestCorrection")]
        public ActionResult RequestCorrection(Guid id)
        {
            var movement = _session.Load<Movement>(id);
            var movementType = SetMovementBasingOnType(movement);

            if (movement != null)
            {
                var destinationName = movement.DestinationName;

                var reasons = new Dictionary<Guid, string>();
                var account = _session.Load<Account>(Identity.AccountId);
                if (account != null)
                    reasons = account.CorrectionReasons;

                var equipment = movement.Equipments
                    .Select(x => new MovementEquipmentModel(x.Key, x.Value.EquipmentName, x.Value.Quantity, movement.EffectiveDate.ToShortDateString(), movement.MovementDate.ToString())).ToList();

                var vm = new MovementCorrectionRequestViewModel()
                             {
                                 DocketNumber = movement.DocketNumber,
                                 Equipment = equipment,
                                 MovementDate = movement.MovementDate,
                                 EffectiveDate = movement.EffectiveDate,
                                 MovementType = movementType,
                                 Direction = movement.Direction,
                                 MovementId = id,
                                 EquipmentProviderName = movement.SupplierName,
                                 TradingPartnerName = destinationName,
                                 NewQuantities = movement.Equipments.ToDictionary(x => x.Key, y => y.Value.Quantity),
                                 Reasons = reasons.Select(x => new SelectListItem { Text = x.Value, Value = x.Value }).ToArray()
                             };

                return View(vm);
            }

            Failure("An error has occured.");
            return View("Index");
        }

        [POST("Movements/RequestCorrection")]
        public ActionResult RequestCorrection(MovementCorrectionRequestViewModel model)
        {
            //saga start
            Bus.Send<IssueQuantityCorrection>(cmd =>
                                                  {
                                                      cmd.AccountId = Identity.AccountId;
                                                      cmd.EquipmentQuantity = model.NewQuantities;
                                                      cmd.MovementId = model.MovementId;
                                                      cmd.RequestedBy = Identity.UserKey;
                                                      cmd.RequestedOn = DateTime.UtcNow;
                                                      cmd.Reason = model.Reason;
                                                      cmd.NewMovementDate = model.NewMovementDate;
                                                      cmd.NewEffectiveDate = model.NewEffectiveDate;
                                                  });

            Success();
            return RedirectToAction("Index");
        }

        [GET("Movements/CorrectionDetails")]
        public ActionResult CorrectionDetails(Guid id)
        {
            var movement = _session.Load<Movement>(id);

            if (movement != null)
            {
                var destinationName = movement.DestinationName;

                var vm = SetMovementCorrectionDetails(movement, movement.SupplierName, destinationName);
                if (vm != null)
                    return View(vm);
            }

            Failure("An error has occured.");
            return View("Index");
        }

        [GET("Movements/RequestDelete")]
        public ActionResult RequestDelete(Guid id)
        {
            var movement = _session.Load<Movement>(id);
            var movementType = SetMovementBasingOnType(movement);

            if (movement != null)
            {
                var destinationCompany = _session.Query<DestinationNamesIndex.Result, DestinationNamesIndex>()
                    .FirstOrDefault(x => x.CompanyId == movement.DestinationId);
                var destinationName = destinationCompany != null ? destinationCompany.CompanyName : string.Empty;

                var reasons = new Dictionary<Guid, string>();
                var account = _session.Load<Account>(Identity.AccountId);
                if (account != null)
                    reasons.AddRange(account.CancellationReasons);

                var equipment = movement.Equipments
                    .Select(x => new MovementEquipmentModel(x.Key, x.Value.EquipmentName, x.Value.Quantity, movement.EffectiveDate.ToShortDateString(), movement.MovementDate.ToString())).ToList();

                var vm = new MovementCancellationRequestViewModel()
                             {
                                 DocketNumber = movement.DocketNumber,
                                 Equipment = equipment,
                                 MovementDate = movement.MovementDate,
                                 EffectiveDate = movement.EffectiveDate,
                                 MovementType = movementType,
                                 Direction = movement.Direction,
                                 MovementId = id,
                                 EquipmentProviderName = movement.SupplierName,
                                 TradingPartnerName = destinationName,
                                 Reasons = reasons.Select(x => new SelectListItem { Text = x.Value, Value = x.Value }).ToArray()
                             };

                return View(vm);
            }

            Failure("An error has occured.");
            return View("Index");
        }

        [POST("Movements/RequestDelete")]
        public ActionResult RequestDelete(MovementCancellationRequestViewModel model)
        {
            //saga start
            Bus.Send<RequestMovementCancellation>(cmd =>
            {
                cmd.AccountId = Identity.AccountId;
                cmd.MovementId = model.MovementId;
                cmd.RequestedBy = Identity.UserKey;
                cmd.RequestedOn = DateTime.UtcNow;
                cmd.Reason = model.Reason;
            });

            Success();
            return RedirectToAction("Index");
        }

        [GET("Movements/Report")]
        public ActionResult Report(Guid id)
        {
            var source = new TransferDocument();
            var movement = _session.Load<Movement>(id);

            var tdvm = new TransferDocumentViewModel(movement);
            source.DataSource = tdvm;

            var vm = new MovementReportViewModel
                         {
                             MovementId = id,
                             Report = source
                         };

            return View("Report", vm);
        }

        [GET("Movements/WithdrawRequest")]
        public ActionResult WithdrawRequest(Guid id, Guid correctionKey, CorrectionStatus status)
        {
            if (status == CorrectionStatus.PendingCorrectionAndCreatedByMe)
            {
                Bus.Send<WithdrawQuantityCorrection>(cmd =>
                                                         {
                                                             cmd.AccountId = Identity.AccountId;
                                                             cmd.MovementId = id;
                                                             cmd.CorrectionKey = correctionKey;
                                                         });
            }
            else if (status == CorrectionStatus.PendingCancellationAndCreatedByMe)
            {
                Bus.Send<WithdrawCancellation>(cmd =>
                                                   {
                                                       cmd.AccountId = Identity.AccountId;
                                                       cmd.MovementId = id;
                                                       cmd.CancellationKey = correctionKey;
                                                   });
            }

            return RedirectToAction("Index");
        }

        [POST("Movements/GetMovementAssets")]
        public ActionResult GetMovementAssets(Guid movementId, [DataSourceRequest] DataSourceRequest request)
        {
            var movement = _session.Load<Movement>(movementId);

            List<MovementEquipmentModel> result = new List<MovementEquipmentModel>();
            if (movement != null)
            {
                result = movement.Equipments
                    .Select(x => new MovementEquipmentModel(x.Key, x.Value.EquipmentName, x.Value.Quantity, movement.EffectiveDate.ToShortDateString(), movement.MovementDate.ToString())).ToList()
                    .ToList();

                return Json(result.ToDataSourceResult(request));
            }

            Failure("Failed to load movement assets.");
            return Json(result.ToDataSourceResult(request));
        }

        [GET("Movements/CompleteMovement")]
        public ActionResult CompleteMovement(Guid movementId)
        {
            var movement = _session.Load<Movement>(movementId);

            if (movement != null)
            {
                var vm = new CompleteMovementVM(movement);

                return View(vm);
            }

            return HttpNotFound();

        }

        [POST("Movements/CompleteMovement")]
        public ActionResult CompleteMovement(CompleteMovementVM vm)
        {
            var movement = _session.Load<Movement>(vm.MovementId);

            if (movement != null)
            {
                Bus.Send(new CompleteMovement()
                {
                    MovementId = movement.Id,
                    EffectiveDateUTC = vm.EffectiveDateUTC,
                    DocketNumber = vm.NewDocketNumber.ToUpper()
                });

                return RedirectToAction("Index");
            }

            return HttpNotFound();
        }

        DataSourceResult GetSearchMovements(DataSourceRequest request)
        {
            if (request.Sorts == null)
            {
                request.Sorts = new List<SortDescriptor>();
                request.Sorts.Add(new SortDescriptor("MovementDate", ListSortDirection.Descending));
            }

            if (request.PageSize == 0) request.PageSize = 25;


            if (request.Filters != null && request.Filters.Count > 0)
            {
                var value = request.Filters.Single(
                    p => (p as FilterDescriptor).Member == "DocketNumber") as FilterDescriptor;

                request.Filters = null;

                var ret =
                    _session.Query<MovementIndex.Result, MovementIndex>()
                    .Where(
                        p =>
                        p.DocketNumber.StartsWith((string)value.Value) ||
                        p.DestinationName.StartsWith((string)value.Value))
                    .ToDataSourceResult(
                            request,
                            x =>
                            new MovementViewModel(x.MovementId.ToGuidId(), x.MovementDate, x.EffectiveDate, x.Direction, x.MovementType,
                                                  x.DestinationName,
                                                  x.DocketNumber));

                return ret;
            }
            return null;
        }
        DataSourceResult GetRecentMovements(DataSourceRequest requestRecent)
        {
            var contextSiteIdString = ContextSiteID.ToStringId<Site>();

            if (requestRecent.Sorts == null)
            {
                requestRecent.Sorts = new List<SortDescriptor>();
                requestRecent.Sorts.Add(new SortDescriptor("MovementDate", ListSortDirection.Descending));
            }

            if (requestRecent.PageSize == 0)
                requestRecent.PageSize = 25;

            DateTime dt = DateTime.UtcNow;
            dt = dt.AddDays(-1);


            var recent = _session.Query<MovementIndex.Result, MovementIndex>()
                                 .Where(
                                     x =>
                                     x.SiteId == contextSiteIdString && x.DocketNumber != null &&
                                     x.PendingMovementCorrection == null &&
                                     x.PendingMovementCancellation == null &&
                                     ((x.MovementType == MovementTransferType.Transfer.ToString() && !x.IsExported)
                                      ||
                                      ((x.MovementType == MovementTransferType.Internal.ToString()) &&
                                       x.MovementDate >= dt)))
                                 .ToDataSourceResult(
                                     requestRecent,
                                     x =>
                                     new MovementViewModel(x.MovementId.ToGuidId(), x.MovementDate, x.EffectiveDate,
                                                           x.Direction, x.MovementType, x.DestinationName,
                                                           x.DocketNumber));

            return recent;
        }
        DataSourceResult GetPendingMovements(DataSourceRequest requestPending)
        {
            var contextSiteIdString = ContextSiteID.ToStringId<Site>();

            if (requestPending.Sorts == null)
            {
                requestPending.Sorts = new List<SortDescriptor>();
                requestPending.Sorts.Add(new SortDescriptor("MovementDate", ListSortDirection.Descending));
            }

            if (requestPending.PageSize == 0) requestPending.PageSize = 25;

            var recent = _session.Query<MovementIndex.Result, MovementIndex>().Where(x => x.SiteId == contextSiteIdString &&
                                !x.IsExported &&
                                (x.PendingMovementCorrection != null || x.PendingMovementCancellation != null || x.IsPending)).ToDataSourceResult(
                                requestPending, x => new MovementViewModel(x.MovementId.ToGuidId(), x.MovementDate, x.EffectiveDate, x.Direction, x.MovementType, x.DestinationName,
                                                   x.DocketNumber, x.PendingMovementCorrection, x.PendingMovementCancellation,
                                                   Identity.UserKey, x.IsPending));
            return recent;
        }
        DataSourceResult GetHistoricMovements(DataSourceRequest requestHistoric)
        {

            var contextSiteIdString = ContextSiteID.ToStringId<Site>();

            if (requestHistoric.Sorts == null)
            {
                requestHistoric.Sorts = new List<SortDescriptor>();
                requestHistoric.Sorts.Add(new SortDescriptor("MovementDate", ListSortDirection.Descending));
            }

            if (requestHistoric.PageSize == 0) requestHistoric.PageSize = 25;

            DateTime dt = DateTime.UtcNow;
            dt = dt.AddDays(-1);

            var result = _session.Query<MovementIndex.Result, MovementIndex>().Where(
                x => x.SiteId == contextSiteIdString &&
                     (x.IsExported ||
                      ((x.MovementType == MovementTransferType.Internal.ToString()) &&
                       x.MovementDate < dt))).ToDataSourceResult(
                           requestHistoric,
                           x =>
                           new MovementViewModel(x.MovementId.ToGuidId(), x.MovementDate, x.EffectiveDate, x.Direction, x.MovementType,
                                                 x.DestinationName,
                                                 x.DocketNumber,
                                                 x.PendingMovementCorrection, x.PendingMovementCancellation,
                                                 Identity.UserKey, x.IsPending));
            return result;

        }

        string SetMovementBasingOnType(Movement movement)
        {
            return movement.MovementType;
        }

        MovementCorrectionDetailsViewModel SetMovementCorrectionDetails(Movement movement, string supplierName,
            string tradingPartnerName)
        {
            var pendingCorrection = movement.PendingMovementCorrection;
            var pendingCancellation = movement.PendingMovementCancellation;

            var equipment = movement.Equipments
                .Select(x => new MovementEquipmentModel(x.Key, x.Value.EquipmentName, x.Value.Quantity, movement.EffectiveDate.ToShortDateString(), movement.MovementDate.ToString())).ToList();

            if (pendingCorrection != null)
            {
                return new MovementCorrectionDetailsViewModel()
                {
                    DocketNumber = movement.DocketNumber,
                    Equipment = equipment,
                    MovementDate = movement.MovementDate,
                    EffectiveDate = movement.EffectiveDate,
                    CorrectedEffectiveDate = pendingCorrection.NewEffectiveDate,
                    CorrectedMovementDate = pendingCorrection.NewMovementDate,
                    MovementType = movement.MovementType,
                    Direction = movement.Direction,
                    MovementId = movement.IdString,
                    Reason = pendingCorrection.Reason,
                    EquipmentProviderName = supplierName,
                    TradingPartnerName = tradingPartnerName,
                    NewQuantities = pendingCorrection.AssetCodeQuantity.ToDictionary(x => x.Key, y => y.Value.NewQuantity),
                    RequestedBy = pendingCorrection.RequestedBy.Value

                };
            }
            if (pendingCancellation != null)
            {
                return new MovementCorrectionDetailsViewModel()
                {
                    DocketNumber = movement.DocketNumber,
                    Equipment = equipment,
                    MovementDate = movement.MovementDate,
                    MovementType = movement.MovementType,
                    EffectiveDate = movement.EffectiveDate,
                    Direction = movement.Direction,
                    MovementId = movement.IdString,
                    Reason = pendingCancellation.Reason,
                    EquipmentProviderName = supplierName,
                    TradingPartnerName = tradingPartnerName
                };
            }
            return null;
        }
    }
}