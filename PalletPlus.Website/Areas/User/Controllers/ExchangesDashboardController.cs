﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Website.Areas.Admin;
using PalletPlus.Website.Areas.Admin.Models.DataVisualization;
using PalletPlus.Website.Services;
using PalletPlus.Website.ViewModels.Dashboard;
using Raven.Client;
using nTrack.Data.Extenstions;
using nTrack.Extension;

namespace PalletPlus.Website.Areas.User.Controllers
{
    public class ExchangesDashboardController : UserBaseController
    {
        private readonly IDocumentSession _session;

        public ExchangesDashboardController(IDocumentSession session)
        {
            _session = session;
        }


        [GET("ExchangesDashboard")]
        public ActionResult Index()
        {
            return View();
        }

        [GET("GetExchangeMovementViewModel")]
        public PartialViewResult GetExchangeMovementViewModel()
        {
             var contextSiteId = ContextSiteID.ToStringId<Site>();
            var site = _session.Load<Site>(contextSiteId);
            var vm = new List<MovementChartModel>();

            var movements = _session.Query<ExchangeDashboardMovementIndex.Result, ExchangeDashboardMovementIndex>()
                .Where(x=>x.SiteId == site.IdString );

            var dt = (int)Math.Floor((decimal)DateTime.Now.Date.DayOfYear / 7);


            for (int i = dt - 4; i <= dt; i++)
            {
                var itemsOfWeekOfType =
                    movements.Where(
                        x => x.Week == i).ToList();

                //var inItem = itemsOfWeekOfType.Where(x => x. == "In");
                //var outItem = itemsOfWeekOfType.Where(x => x.Direction == "Out");

                // var item = new MovementChartModel(i, inItem.Sum(p => p.Counts), outItem.Sum(p => p.Counts));
                var item = new MovementChartModel(i, itemsOfWeekOfType.Sum(p => p.InCount), itemsOfWeekOfType.Sum(p => p.OutCount));
                vm.Add(item);
            }

            var data = new VizViewModel(vm, "Exchange Movements", "exchangemovementchart");

            return PartialView("_VizPartial",data);
        }

        [GET("GetExchangeBalanceViewModel")]
        public JsonResult GetExchangeBalanceViewModel()
        {
            var data = new List<Dictionary<string, object>>();


            var contextSiteId = ContextSiteID.ToStringId<Site>();
            var site = _session.Load<Site>(contextSiteId);

            //var sites = _session.Advanced.LoadStartingWith<Site>("sites/")
            //    .ToDictionary(x => x.Id, y => y.CompanyName);

            var balances = _session.Query<ExchangeDashboardBalanceIndex.Result, ExchangeDashboardBalanceIndex>()
                .Where(x=>x.SiteId == site.IdString)
                .ToList();

            var equipmentindex = _session.Query<EquipmentIndex.Result, EquipmentIndex>().ToList();

            var equipments = (from p in equipmentindex
                              group p by new {p.EquipmentCode, p.EquipmentName}
                              into grp
                              select new
                                         {
                                             grp.Key.EquipmentCode,
                                             grp.Key.EquipmentName
                                         }).ToList();

            var series =
                equipments.Select(x => new {name = x.EquipmentName, field = "e{0}".Fill(x.EquipmentCode)}).ToList();

           
                var siteDic = new Dictionary<string, object>();
                siteDic.Add("site", site.CompanyName);

                foreach (var equipment in equipments)
                {
                    var balance =
                        balances.SingleOrDefault(
                            x => x.EquipmentCode == equipment.EquipmentCode && x.SiteId == site.IdString);

                    siteDic.Add("e{0}".Fill(equipment.EquipmentCode),
                                balance != null ? balance.QuantityIn - balance.QuantityOut : 0);
                }

                data.Add(siteDic);



            

            return Json(new {series = series, data = data}, JsonRequestBehavior.AllowGet);
        }

        
    }
}
