﻿using System;
using System.Collections.Generic;
using AttributeRouting.Web.Mvc;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Website.Areas.User.ViewModels.Enumerators;
using PalletPlus.Website.Areas.User.ViewModels.RecordMovement;
using Raven.Client;
using Raven.Client.Linq;
using System.Linq;
using System.Web.Mvc;
using nTrack.Data.Extenstions;

namespace PalletPlus.Website.Areas.User.Controllers
{
    public class RecordInMovementController : RecordMovementBaseController
    {
        public RecordInMovementController(IDocumentSession session)
            : base(session)
        {
        }

        [GET("RecordInMovement")]
        public ActionResult Index()
        {
            var contextSiteIdString = ContextSiteID.ToStringId<Site>();
            var companyIdString = Identity.AccountId.ToStringId<Account>();

            var suppliers = _session.Query<SiteSupplierIndex.Result, SiteSupplierIndex>()
                .Where(x => x.SiteId == contextSiteIdString || x.SiteId == companyIdString)
                .ToList()
                .Select(x => x.SupplierId)
                .Distinct()
                .ToArray();

            var vm = new RecordMovementViewModel(MovementDirection.In, _session.Load<Supplier>(suppliers));

            return View(vm);
        }
    }
}