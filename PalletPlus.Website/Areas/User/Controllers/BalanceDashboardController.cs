﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using nTrack.Data.Extenstions;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Website.Models.DataVisualization;
using Raven.Client;

namespace PalletPlus.Website.Areas.User.Controllers
{
    public class BalanceDashboardController : UserBaseController
    {
        private readonly IDocumentSession _session;


        public BalanceDashboardController(IDocumentSession session)
        {
            _session = session;
        }

        [GET("BalanceDashboard")]
        public ActionResult Index()
        {
            ViewBag.Sites = DSession.Query<SiteUserIndex.Result, SiteUserIndex>()
                   .Where(x => x.UserKey == Identity.UserKey)
                   .Select(x => x.SiteName)
                   .ToArray();

            return View();
        }

        [GET("GetAccountBalance")]
        public ActionResult GetAccountBalanceViewModel()
        {
            var vm = new List<SiteEquipmentBalanceModel>();

            var contextSiteId = ContextSiteID.ToStringId<Site>();
            var site = _session.Load<Site>(contextSiteId);

            //var sites = _session.Query<SiteUserIndex.Result, SiteUserIndex>()
            //    .Where(x => x.UserKey == Identity.UserKey)
            //    .ToDictionary(x => x.SiteId, y => y.SiteName);

            var equipmentBalances = _session.Query<MovementBalanceIndex.Result, MovementBalanceIndex>()
                .Where(x => x.MovementType == "Transfer")
                .ToList();

            //foreach (var s in sites)
            //{
            var balance = equipmentBalances.Where(x => x.SiteId == site.IdString)
                .Select(x => new SiteEquipmentBalanceModel(x.SiteName, x.EquipmentName, x.Quantity))
                .OrderBy(x => x.SiteName);

            if (balance.Any())
                vm.AddRange(balance);
            else
            {
                var eqNames = _session.Query<EquipmentIndex.Result, EquipmentIndex>()
                    .Select(x => x.EquipmentName)
                    .ToList();

                vm.AddRange(eqNames
                                .Select(x => new SiteEquipmentBalanceModel(site.CompanyName, x, 0)));
            }
            //  }

            return Json(vm.ToArray(), JsonRequestBehavior.AllowGet);
        }

        [GET("GetEquipmentBalance")]
        public ActionResult GetEquipmentBalance()
        {
            var vm = new List<SiteEquipmentBalanceModel>();

            var contextSiteId = ContextSiteID.ToStringId<Site>();
            var site = _session.Load<Site>(contextSiteId);

            var equipmentBalances = _session.Query<MovementBalanceIndex.Result, MovementBalanceIndex>()
                .Where(x => x.MovementType == "Transfer")
                .ToList();

            var balance = equipmentBalances.Where(x => x.SiteId == site.IdString)
                .Select(x => new SiteEquipmentBalanceModel(x.SiteName, x.EquipmentName, x.Quantity))
                .OrderBy(x => x.SiteName);

            if (balance.Any())
                vm.AddRange(balance);
            else
            {
                var eqNames = _session.Query<EquipmentIndex.Result, EquipmentIndex>()
                    .Select(x => x.EquipmentName)
                    .ToList();

                vm.AddRange(eqNames.Select(x => new SiteEquipmentBalanceModel(site.CompanyName, x, 0)));
            }

            return Json(vm.ToArray(), JsonRequestBehavior.AllowGet);
        }

        [GET("GetDashboardColors")]
        public JsonResult GetDashboardColors()
        {
            var account = _session.Load<Account>(Identity.AccountId);
            if (account != null)
            {
                var colors = account.DashboardEquipmentColors.Select(x => x.Value);
                var ret = colors.Select(x => string.Format("rgb({0},{1},{2})", x.R, x.G, x.B)).ToArray();

                return Json(ret, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
    }
}
