﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using Newtonsoft.Json;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Website.Areas.Admin;
using PalletPlus.Website.Models.DataVisualization;
using PalletPlus.Website.Services;
using Raven.Client;
using nTrack.Data.Extenstions;

namespace PalletPlus.Website.Areas.User.Controllers
{
    public class EquipmentBalanceDashboardController : UserBaseController
    {
        private readonly IDocumentSession _session;

        public EquipmentBalanceDashboardController(IDocumentSession session)
        {
            _session = session;
        }


        [GET("EquipmentBalanceDashboard")]
        public ActionResult Index()
        {
            //TODO: this looks like a code smell
            ViewBag.Sites = _session.Advanced.LoadStartingWith<Site>("sites/").Select(x => x.CompanyName).ToArray();
                
                
            return View();

  
        }

        [POST("SetEquipmentBalanceViewModel")]
        public ActionResult SetEquipmentBalanceViewModel()
        {
            var vm = new List<SiteEquipmentBalanceModel>();

            var contextSiteId = ContextSiteID.ToStringId<Site>();
            var site = _session.Load<Site>(contextSiteId);

            

            var equipmentBalances = _session.Query<MovementBalanceIndex.Result, MovementBalanceIndex>()
                .ToList();

                var balance = equipmentBalances.Where(x => x.SiteId == site.IdString)
                    .Select(x => new SiteEquipmentBalanceModel(x.SiteName, x.EquipmentName, x.Quantity))
                    .OrderBy(x => x.SiteName);

                if (balance.Any())
                    vm.AddRange(balance);
                else
                {
                    var eqNames = _session.Query<EquipmentIndex.Result, EquipmentIndex>()
                        .Select(x => x.EquipmentName)
                        .ToList();

                    vm.AddRange(eqNames
                                    .Select(x => new SiteEquipmentBalanceModel(site.CompanyName, x, 0)));
                }

            return Json(vm.ToArray());
        }



    }
}
