﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using nTrack.Data.Extenstions;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Commands.Movement;
using PalletPlus.Website.Areas.User.ViewModels.Enumerators;
using PalletPlus.Website.Areas.User.ViewModels.RecordMovement;
using PalletPlus.Website.Helpers;
using Raven.Client;
using Raven.Client.Linq;
using nTrack.Extension;

namespace PalletPlus.Website.Areas.User.Controllers
{
    public class RecordMovementBaseController : UserBaseController
    {
        protected readonly IDocumentSession _session;

        protected ActionResult IndexView;

        public RecordMovementBaseController(IDocumentSession session)
        {
            _session = session;
        }

        [POST("RecordMovement/GetCascadeDestinations/{movementDirection}/{id}")]
        public PartialViewResult GetCascadeDestinations(MovementDirection movementDirection, Guid id)
        {
            var supplierId = id.ToStringId<Supplier>();

            var partnersInSite = _session.Query<Partner, PartnerIndex>()
                .Where(x => x.Sites.Any(e => e == ContextSiteID.ToStringId<Site>()))
                .Select(x => new { x.IdString, x.CompanyName, Suppliers = x.Suppliers })
                .ToList();

            //Site <=> Partner (Partners for combobox)
            var partners = partnersInSite
                .Where(x => x.Suppliers.Keys.Any(e => e == supplierId))
                .ToDictionary(x => x.IdString, y => y.CompanyName);


            var sites = new Dictionary<string, string>();
            var account = _session.Load<Account>(Identity.AccountId);
            if (account != null)
            {
                var isSupplierInCompany = account.Suppliers.Select(x => x.Value).Any(x => x.CompanyId == supplierId);
                if (isSupplierInCompany)
                {
                    //Site <=> Site (All sites for combobox, because: isSupplierInCompany => all sites inherit it)
                    sites = _session
                        .Advanced.LoadStartingWith<Site>("sites/")
                        //.Query<SiteIndex.Result, SiteIndex>()
                        .Where(x => x.Id != ContextSiteID)
                        .ToDictionary(x => x.IdString, y => "{0} - {1}".Fill(account.CompanyName, y.CompanyName));
                }
                else
                {
                    sites = _session
                        .Advanced.LoadStartingWith<Site>("sites/")
                        .Where(x => x.Id != ContextSiteID && x.Suppliers.ContainsKey(supplierId))
                        .ToDictionary(x => x.IdString, y => "{0} - {1}".Fill(account.CompanyName, y.CompanyName));

                    //var sitesWithSupplier = _session
                    //    .Advanced.LoadStartingWith<Site>("sites/")
                    //    //.Query<SiteIndex.Result, SiteIndex>()
                    //    .Select(x => new { x.IdString, SiteName= x.CompanyName, x.Suppliers,AccountName=account.CompanyName })
                    //    .ToList();

                    ////Site <=> Site (Sites for combobox that are associated with supplier)
                    //sites = sitesWithSupplier
                    //    .Where(x => x.SiteId != contextSiteId && x.Suppliers.ContainsKey(supplierId))
                    //    .ToDictionary(x => x.SiteId, y => y.AccountName + " - " + y.SiteName);
                }
            }

            //Site <=> Supplier (for issue/return) choose locations from a supplier
            var suppliers = new Dictionary<Guid, string>();
            var supplier = _session.Load<Supplier>(supplierId);
            if (supplier != null)
            {
                suppliers = supplier.Locations.Where(p => !p.Value.IsDeleted).ToDictionary(x => x.Key, y => supplier.CompanyName + " - " + y.Value.CompanyName);
            }

            var vm = new RecordMovementViewModel(movementDirection, id, partners, sites, suppliers);

            return PartialView("_TradingPartnerComboboxPartial", vm);
        }

        [GET("CheckDocketNumberUniqueness")]
        public JsonResult CheckDocketNumberUniqueness(string providerDocketNo)
        {
            bool result = true;
            string contextSiteIdString = ContextSiteID.ToStringId<Site>();
            if (!string.IsNullOrWhiteSpace(providerDocketNo))
            {
                result = !_session.Query<MovementIndex.Result, MovementIndex>()
                    .Any(p => p.DocketNumber == providerDocketNo && p.SiteId == contextSiteIdString);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [POST("RecordMovement/GetCascadeAccountNumber/{movementDirection}/{supplierId}/{partnerId}")]
        public ActionResult GetCascadeAccountNumber(MovementDirection movementDirection, Guid supplierId, string partnerId)
        {
            var model = new RecordMovementViewModel();

            model.MovementDirection = movementDirection;
            model.SupplierId = supplierId;
            model.TradingPartner = partnerId;

            model.Transporters = _session.Query<TransporterIndex.Result, TransporterIndex>()
                .ToDictionary(x => x.TransporterId, y => y.TransporterName);

            var supplier = _session.Load<Supplier>(supplierId);
            if (supplier != null)
            {
                model.EquipmentNames = supplier.Equipments.Select(x => x.Value).Where(p => !p.IsDeleted)
                       .ToDictionary(x => x.EquipmentCode, y => y.EquipmentName);
            }

            PartialViewResult ret = new PartialViewResult();

            model.IdToValidate = new Dictionary<string, string>();

            var account = DSession.Load<Account>(Identity.AccountId);

            foreach (var requirement in account.RequiredFields)
            {
                if (requirement == FieldsForRequirements.DocketNumber && movementDirection == MovementDirection.Out)
                {
                    continue;
                }
                model.IdToValidate.Add(requirement, requirement.ToSeparateWords() + " is required.");
            }

            var subject = model.TradingPartner.Split('_');
            switch (subject[0])
            {
                case "partners":
                    ret = TradingPartnerIsPartner(model, Guid.Parse(subject[1]));
                    break;
                case "sites":
                    ret = TradingPartnerIsSite(model, Guid.Parse(subject[1]));
                    break;
                case "suppliers":
                    ret = TradingDestinationIsSupplier(model, supplierId, Guid.Parse(subject[1]));
                    break;
            }

            ModelState.Clear();

            return ret;
        }

        [POST("RecordMovement/Save")]
        public ActionResult Save(RecordMovementViewModel model, bool? overrideDocketNo)
        {
            //var result = Validate(model);

            //if (result != null)
            //    return result;

            //How in current implementation return a View with ModelState errors? 
            if (model.MovementDirection == MovementDirection.Out)
            {
                if (overrideDocketNo == null || !overrideDocketNo.Value)
                {
                    model.ProviderDocketNo = null;
                }
            }

            if (model.Equipment != null && model.Equipment.Any())
            {
                var assets = model.Equipment
                    .Where(x => x.Value != null)
                    .ToDictionary(x => x.Key, y => (int)y.Value);

                SendMovement(model, assets);

                Success();
            }
            else
            {
                Failure("There are no assets in this movement.");
            }

            if (model.MovementDirection == MovementDirection.In)
                return RedirectToAction("Index", "RecordInMovement");

            return RedirectToAction("Index", "RecordOutMovement");
        }

        private void SendMovement(RecordMovementViewModel model, Dictionary<string, int> assets)
        {
            Bus.Send<CreateMovement>(
                cmd =>
                {
                    cmd.MovementId = Guid.NewGuid();
                    cmd.SiteId = ContextSiteID;
                    cmd.DestinationId = model.TradingPartnerID;
                    cmd.SupplierId = model.SupplierId;
                    cmd.Equipments = assets;
                    cmd.Direction = model.MovementDirection.ToString();
                    cmd.MovementDate = DateTime.Now;
                    cmd.EffectiveDate = model.MovementDate;

                    cmd.ConsignmentNote = model.ConNote;
                    cmd.DocketNumber = model.ProviderDocketNo;
                    cmd.DestinationReference = model.PartnerReference;
                    cmd.MovementReference = model.Reference;
                    //TODO: remove from model and view the references to transporters
                    //cmd.ReceivedViaTransporter = model.ReceivedViaTransporter;
                    //cmd.TransporterId = model.Transporter;
                    cmd.LocationId = model.LocationId;
                });
        }

        private TransferType EvaluateMovementType(PartnerSupplier supplierOfAPartner)
        {
            return TransferType.Transfer;
        }

        private PartialViewResult TradingDestinationIsSupplier(RecordMovementViewModel model, Guid destinationId, Guid locationId)
        {
            model.TradingPartnerID = destinationId;
            model.LocationId = locationId;

            var destinationSupplier = _session.Load<Supplier>(destinationId);

            var site = _session.Load<Site>(ContextSiteID.ToStringId<Site>());

            if (destinationSupplier.IsPTAEnabled)
            {
                if (!site.IsPTAEnabled)
                {
                    model.RequairePTAEnabled = true;
                }
            }

            if (destinationSupplier != null)
            {
                if (destinationSupplier.Locations.ContainsKey(locationId))
                {
                    model.PartnerAccountNumber = destinationSupplier.Locations[locationId].AccountNumber;
                }
            }

            var equipmentAvailable = _session.Load<Supplier>(model.SupplierId).Equipments
                                 .Select(x => x.Value).Where(p => !p.IsDeleted)
                                 .Select(x => x.EquipmentCode);
            model.Equipment = equipmentAvailable.ToDictionary(x => x, y => (int?)null);

            return PartialView("_RecordMovementPartial", model);
        }

        private PartialViewResult TradingPartnerIsPartner(RecordMovementViewModel model, Guid tradingSubjectId)
        {
            model.TradingPartnerID = tradingSubjectId;

            var partner = _session.Load<Partner>(model.TradingPartnerID);
            var supplierOfAPartner = partner
                                             .Suppliers.Select(x => x.Value)
                                             .FirstOrDefault(x => x.SupplierId == model.SupplierId.ToStringId<Supplier>());
            var site = _session.Load<Site>(ContextSiteID.ToStringId<Site>());

            if (partner.IsPTAEnabled)
            {
                if (!site.IsPTAEnabled)
                {
                    model.RequairePTAEnabled = true;
                }
            }

            if (supplierOfAPartner != null)
            {
                var equipmentAvailable = new HashSet<string>();

                var supplier = _session.Load<Supplier>(supplierOfAPartner.SupplierId);
                foreach (var equipment in supplierOfAPartner.Equipments)
                {
                    if (supplier.Equipments.ContainsKey(equipment))
                    {
                        if (!supplier.Equipments[equipment].IsDeleted)
                            equipmentAvailable.Add(equipment);
                    }
                }

                model.PartnerAccountNumber = supplierOfAPartner.AccountNumber;
                model.MovementType = TransferType.Transfer;

                model.Equipment = equipmentAvailable.ToDictionary(x => x, y => (int?)null);
            }

            return PartialView("_RecordMovementPartial", model);
        }

        private PartialViewResult TradingPartnerIsSite(RecordMovementViewModel model, Guid tradingSubjectId)
        {
            model.TradingPartnerID = tradingSubjectId;
            model.MovementType = TransferType.Internal;

            var supplierOfASite = _session.Load<Site>(model.TradingPartnerID)
                                    .Suppliers.Select(x => x.Value)
                                    .FirstOrDefault(x => x.CompanyId == model.SupplierId.ToStringId<Supplier>());

            if (supplierOfASite != null)
            {
                model.PartnerAccountNumber = supplierOfASite.AccountNumber;
            }
            else
            {
                var supplierOfACompany =
                    _session.Load<Account>(Identity.AccountId)
                    .Suppliers.Select(x => x.Value)
                    .FirstOrDefault(x => x.CompanyId == model.SupplierId.ToStringId<Supplier>());

                if (supplierOfACompany != null)
                {
                    model.PartnerAccountNumber = supplierOfACompany.AccountNumber;
                }
            }

            var equipmentAvailable = _session.Load<Supplier>(model.SupplierId).Equipments
                                             .Select(x => x.Value).Where(p => !p.IsDeleted)
                                             .Select(x => x.EquipmentCode);
            model.Equipment = equipmentAvailable.ToDictionary(x => x, y => (int?)null);

            return PartialView("_RecordMovementPartial", model);
        }
    }
}