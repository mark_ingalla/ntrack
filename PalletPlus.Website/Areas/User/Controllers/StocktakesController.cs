﻿using AttributeRouting.Web.Mvc;
using nTrack.Data.Extenstions;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Commands.Stocktake;
using PalletPlus.Website.Areas.User.ViewModels.Stocktake;
using Raven.Client;
using Raven.Client.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PalletPlus.Website.Areas.User.Controllers
{
    public class StocktakesController : UserBaseController
    {
        readonly IDocumentSession _session;

        public StocktakesController(IDocumentSession session)
        {
            _session = session;
        }

        [GET("Stocktakes")]
        public ActionResult Index()
        {
            return View(GetStocktakes());
        }

        [GET("Stocktakes/{id}/RequestCorrection/{key}")]
        public ActionResult RequestCorrection(Guid id, string key)
        {
            var stocktake = _session.Load<Stocktake>(id);
            if (stocktake != null)
            {
                var reasons = new Dictionary<string, string>();
                var account = _session.Load<Account>(Identity.AccountId);
                if (account != null)
                    reasons = account.CorrectionReasons.ToDictionary(x => x.Value, y => y.Value);

                if (stocktake.EquipmentCounts.ContainsKey(key))
                {
                    var equipment = stocktake.EquipmentCounts[key];

                    var vm = new StocktakeCorrectionRequestViewModel(stocktake.SupplierId.ToGuidId(), stocktake.SupplierName, id,
                        stocktake.TakenOn, key, equipment.EquipmentName, equipment.Quantity, reasons);
                    return View(vm);
                }
            }

            return HttpNotFound();
        }

        [POST("Stocktakes/{id}/RequestCorrection/{key}")]
        public ActionResult RequestCorrection(StocktakeCorrectionRequestViewModel model)
        {
            //saga start
            Bus.Send<RequestStockCountCorrection>(cmd =>
                         {
                             cmd.EquipmentCode = model.EquipmentCode;
                             cmd.Quantity = model.Quantity;
                             cmd.Reason = model.Reason;
                             cmd.StocktakeId = model.StocktakeId;
                             cmd.CorrectionKey = Guid.NewGuid();
                             cmd.UserKey = Identity.UserKey;
                         });

            Success();
            return RedirectToAction("Index");
        }

        [GET("Stocktakes/{id}/CorrectionDetails/{key}")]
        public ActionResult CorrectionDetails(Guid id, string key)
        {
            var stocktake = _session.Load<Stocktake>(id);
            if (stocktake != null)
            {
                var supplierName = string.Empty;
                var supplier = _session.Load<Supplier>(stocktake.SupplierId);
                if (supplier != null)
                    supplierName = supplier.CompanyName;

                if (stocktake.EquipmentCounts.ContainsKey(key))
                {
                    var equipment = stocktake.EquipmentCounts[key];

                    var vm = new StocktakeCorrectionDetailsViewModel(stocktake.SupplierId.ToGuidId(), supplierName, id,
                                                                     stocktake.TakenOn,
                                                                     equipment.PendingCorrection.RequestedOn, key,
                                                                     equipment.EquipmentName,
                                                                     equipment.Quantity, equipment.PendingCorrection.NewQuantity,
                                                                     equipment.PendingCorrection.RequestedBy.Value,
                                                                     equipment.PendingCorrection.Reason);

                    return View(vm);
                }
            }

            return HttpNotFound();
        }

        IEnumerable<StocktakeViewModel> GetStocktakes()
        {
            var contextSiteId = ContextSiteID.ToStringId<Site>();

            var stocktakes = _session.Query<StocktakeIndex.Result, StocktakeIndex>().Where(p => p.SiteId == contextSiteId)
                .OrderByDescending(x => x.StocktakeDate)
                .ToList();

            var ret = stocktakes.Select(x =>
                                        new StocktakeViewModel(x.StocktakeId.ToGuidId(), x.StocktakeDate,
                                                               x.EquipmentCode, x.EquipmentName, x.Quantity,
                                                               x.GotPendingCorrection, x.IsApproved,
                                                               x.CorrectionRequestedBy, Identity.UserKey));

            return ret;
        }
    }
}