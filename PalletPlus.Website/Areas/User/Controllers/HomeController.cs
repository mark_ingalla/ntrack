﻿using System.Collections.Generic;
using System.Linq;
using AttributeRouting.Web.Mvc;
using System;
using System.Web.Mvc;
using PalletPlus.Data;
using Raven.Client;

namespace PalletPlus.Website.Areas.User.Controllers
{

    public class HomeController : UserBaseController
    {
        [GET("")]
        public ActionResult Index()
        {
            //var points = new List<Movement>();
            //var nextGroupOfPoints = new List<Movement>();
            //const int ElementTakeCount = 1024;
            //int i = 0;
            //int skipResults = 0;
            //RavenQueryStatistics stats;
            //do
            //{
            //    nextGroupOfPoints = DSession.Query<Movement>().Statistics(out stats).Skip(i * ElementTakeCount + skipResults).Take(ElementTakeCount).ToList();
            //    i++;
            //    skipResults += stats.SkippedResults;

            //    foreach (var movement in nextGroupOfPoints)
            //    {
            //        movement.IsPending = false;
            //    }

            //    DSession.SaveChanges();

            //}
            //while (nextGroupOfPoints.Count == ElementTakeCount);


            ////foreach (var movement in DSession.Query<Movement>())
            ////{
            ////    movement.IsPending = false;
            ////}

            ////DSession.SaveChanges();

            return RedirectToAction("Index", "BalanceDashboard", new { area = "User" });
            //return View();
        }

        [GET("SetContextSite/{siteID}")]
        public ActionResult SetContextSite(Guid siteID)
        {
            var returnUrl = Request.UrlReferrer;

            ClientSideStoreServices.SetContextSiteID(Response.Cookies, siteID);

            return Redirect(returnUrl.AbsoluteUri);
        }
    }
}
