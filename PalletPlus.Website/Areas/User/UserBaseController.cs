﻿using System.Web.Security;
using AttributeRouting;
using nTrack.Data;
using nTrack.Web.Bootstrap;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Website.Infrastructure;
using PalletPlus.Website.Services;
using Raven.Client;
using StructureMap;
using StructureMap.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PalletPlus.Website.Areas.User
{
    [RouteArea("User")]
    [Authorize(Roles = "Admin, User")]
    public abstract class UserBaseController : BaseController
    {
        [SetterProperty]
        public IClientSideStoreServices ClientSideStoreServices { get; set; }

        [SetterProperty]
        public IDocumentSession DSession { get; set; }

        protected Guid ContextSiteID { get; private set; }
        protected Dictionary<Guid, string> Sites { get; private set; }

        public IThreadingService ThreadingService { get; set; }

        public UserBaseController()
        {
            ThreadingService = ObjectFactory.GetInstance<IThreadingService>();
        }

        protected void SetTabValue(string tabRouteValue)
        {
            TempData["tab-value"] = tabRouteValue;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var userId = Identity.UserKey;
            PopulateSites(userId);

            var contextSiteID = ClientSideStoreServices.GetContextSiteID(Request.Cookies);
            if (contextSiteID == null || (!Sites.ContainsKey(contextSiteID.Value)))
            {
                if (Sites.Count == 0)
                {
                    //TODO: redirect to an appropriate page
                    FormsAuthentication.SignOut();
                    FormsAuthentication.RedirectToLoginPage();
                    return;
                }

                ContextSiteID = Sites.First().Key;
                ClientSideStoreServices.SetContextSiteID(Response.Cookies, ContextSiteID);
            }
            else
            {
                ContextSiteID = contextSiteID.Value;
            }

            ViewBag.ContextSite = Sites[ContextSiteID];
            ViewBag.AllSites = Sites.OrderBy(p => p.Value).ToList();

            base.OnActionExecuting(filterContext);
        }

        void PopulateSites(Guid userKey)
        {
            var userSites = DSession.Query<SiteUserIndex.Result, SiteUserIndex>()
                   .Where(x => x.UserKey == userKey)
                   .Select(x => x.SiteId)
                   .ToList();

            Sites = DSession.Load<Site>(userSites).ToDictionary(x => x.Id, y => y.CompanyName);
        }

        protected void Success()
        {
            TempData["alert-type"] = AlertType.Success;
            TempData["alert-message"] = "Your request finished with success.";
        }

        protected void Failure(string message)
        {
            TempData["alert-type"] = AlertType.Error;
            TempData["alert-message"] = message;
        }

        protected string HandleErrors(ModelStateDictionary modelState)
        {
            string errors = "";
            var err = ModelState.Values.Where(x => x.Errors != null);
            foreach (var x in err)
            {
                foreach (var y in x.Errors)
                {
                    errors += y.ErrorMessage + '\n';
                }
            }
            return errors;
        }
    }
}