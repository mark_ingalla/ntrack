﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PalletPlus.Website.Areas.User.ViewModels.Movement.MovementReportViewModel>" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.ReportViewer.WebForms" Assembly="Telerik.ReportViewer.WebForms, Version=6.2.13.110, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" %>

<script runat="server">
    public override void VerifyRenderingInServerForm(Control control)
    {
        // to avoid the server form (<form runat="server"> requirement
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        var instanceReportSource = new Telerik.Reporting.InstanceReportSource();
        instanceReportSource.ReportDocument = Model.Report;

        ReportViewer1.ReportSource = instanceReportSource;
        ReportViewer1.ParametersAreaVisible = false;
    }
</script>

<telerik:ReportViewer ID="ReportViewer1" Width="100%" Height="400" runat="server" CssClass="TReport">
</telerik:ReportViewer>
