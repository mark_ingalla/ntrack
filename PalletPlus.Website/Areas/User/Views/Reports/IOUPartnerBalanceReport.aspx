﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PalletPlus.Website.Areas.User.ViewModels.Raports.IoUPartnerBalanceReportViewModel>" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.ReportViewer.WebForms" Assembly="Telerik.ReportViewer.WebForms, Version=6.2.13.110, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>Raport</title>
</head>
<body>
    
    <form id="form1" runat="server">
        <div>
    
        <script runat="server">
        
            public override void VerifyRenderingInServerForm(Control control)
            {
                // to avoid the server form (<form runat="server">) requirement
            }

            protected void Page_Load(object sender, EventArgs e)
            {
                var instanceReportSource = new Telerik.Reporting.InstanceReportSource();
                instanceReportSource.ReportDocument = Model.Report;
            
                ReportViewer1.ReportSource = instanceReportSource;
                ReportViewer1.ParametersAreaVisible = false;
            }
        </script>
        <telerik:ReportViewer runat="server" ID="ReportViewer1" Height="800px" Width="100%"/>

        </div>
    </form>

</body>
</html>
