﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<PalletPlus.Website.Areas.User.ViewModels.Raports.StocktakesReportViewModel>" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.ReportViewer.WebForms" Assembly="Telerik.ReportViewer.WebForms, Version=6.2.13.110, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" %>

<script runat="server">
    public override void VerifyRenderingInServerForm(Control control)
    {
        // to avoid the server form (<form runat="server"> requirement
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        var instanceReportSource = new Telerik.Reporting.InstanceReportSource();
        instanceReportSource.ReportDocument = Model.Report;

        ReportViewer4.ReportSource = instanceReportSource;
        ReportViewer4.ParametersAreaVisible = false;
    }
</script>

<telerik:ReportViewer ID="ReportViewer4" Height="440" Width="100%" ClientIDMode="AutoID" runat="server" CssClass="TReport">
</telerik:ReportViewer>
