﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Import Namespace="PalletPlus.Data.Indices" %>
<%@ Import Namespace="PalletPlus.Reports" %>
<%@ Import Namespace="PalletPlus.Website.Helpers" %>
<%@ Import Namespace="PalletPlus.Website.Services" %>
<%@ Import Namespace="Raven.Client" %>
<%@ Import Namespace="StructureMap" %>
<%@ Import Namespace="nTrack.Core.Enums" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.ReportViewer.WebForms" Assembly="Telerik.ReportViewer.WebForms, Version=6.2.13.110, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" %>

<script runat="server">
    public override void VerifyRenderingInServerForm(Control control)
    {
        // to avoid the server form (<form runat="server"> requirement
    }
    
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        using(var session = StructureMap.ObjectFactory.GetInstance<IDocumentSession>())
        {
            var results = session.Query<ReportingMovementIndexResult, ReportingMovementIndex>()
                .ToList();
            var source = new DetailedMovementReport();
            var vm = new DetailedMovementReportViewModel(results);
            source.DataSource = vm.MovementData;
            
            CustomIdentity customUser = User.Identity as CustomIdentity;

            if (customUser != null)
            {
                System.Drawing.Image logo = ObjectFactory.GetInstance<IAttachmentService>().GetImage(customUser.AccountId, AttachmentEntities.AccountLogos.ToString());
                source.SetLogo(logo);
            }
            
            var instanceReportSource = new Telerik.Reporting.InstanceReportSource();
            instanceReportSource.ReportDocument = source;
            ReportViewer5.ReportSource = instanceReportSource;
        }
    }
</script>

<telerik:ReportViewer ID="ReportViewer5" Width="100%" Height="800px" runat="server" CssClass="TReport">
</telerik:ReportViewer>