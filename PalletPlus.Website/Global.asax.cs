﻿
using System;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Security;
using NServiceBus;
using nTrack.Data.Extenstions;
using nTrack.Data.Indices;
using PalletPlus.Website.App_Start;
using PalletPlus.Website.Helpers;
using Raven.Client;
using Raven.Client.Document;

namespace PalletPlus.Website
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static IBus Bus { get; set; }
        public static string Version { get; set; }
        public static IDocumentStore SystemStore { get; private set; }

        protected void Application_Start()
        {
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            AssemblyVersion.LoadAssemblyVersion();

            BundleTable.EnableOptimizations = true;

            SystemStore = new DocumentStore() { ConnectionStringName = "SystemDB" };
            SystemStore.Initialize();
        }



        protected void Application_AuthenticateRequest()
        {
            if (Request.Url.AbsoluteUri.Contains("/Content") || Request.Url.AbsoluteUri.Contains("/Scripts"))
                return;

            try
            {
                var cookie = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value);

                using (var session = SystemStore.OpenSession())
                {
                    var userKey = Guid.Parse(cookie.Name);
                    var userResult = session.Query<UserIndex.Result, UserIndex>()
                                            .FirstOrDefault(x => x.UserKey == userKey && !x.IsDeleted && !x.IsLocked);

                    if (userResult == null)
                    {
                        FormsAuthentication.SignOut();
                        return;
                    }

                    var productCode = ConfigurationManager.AppSettings["ProductCode"];

                    var db = session.Query<AccountDatabaseIndex.Result, AccountDatabaseIndex>()
                                    .SingleOrDefault(
                                        x => x.AccountId == userResult.AccountId && x.ProductCode == productCode);

                    if (db == null) return;

                    var ident = new CustomIdentity(userResult.AccountId.ToGuidId(), userResult.UserKey, userResult.UserFullName, db.DatabaseName);
                    var roles = from role in userResult.Role.Split(',') select role.Trim();
                    Context.User = new CustomPrincipal(ident, roles.ToArray());
                }
            }
            catch (Exception exception)
            {

            }
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            // If the exception is caused by an HTTP 401 (Unauthorized) and the user is not authenticated, then redirect to the login page.
            if (Context.Response.StatusCode != 401 ||
                (Context.User != null && Context.User.Identity != null && Context.User.Identity.IsAuthenticated))
                return;

            FormsAuthentication.RedirectToLoginPage();
        }
    }
}