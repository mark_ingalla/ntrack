﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PalletPlus.Website.ViewModels.Profile
{


    public class ProfileUserViewModel : PageViewModel
    {
        public Guid UserKey { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        public string Phone { get; set; }

        public string Mobile { get; set; }

      

        public bool IsDeleted { get; set; }
        public bool IsLocked { get; set; }

        [DataType(DataType.Password)]
        [Display(Name="New Password")]
        public string ChangePassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Retype new Password")]
        [Compare("ChangePassword")]
        public string ConfirmPassword { get; set; }

        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

      

        public ProfileUserViewModel() { }

        public ProfileUserViewModel(string[] roles)
        {
          
        }
    }
}