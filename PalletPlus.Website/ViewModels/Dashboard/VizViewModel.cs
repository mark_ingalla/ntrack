﻿using System.Collections.Generic;
using PalletPlus.Website.Areas.Admin.Models.DataVisualization;

namespace PalletPlus.Website.ViewModels.Dashboard
{
    public class VizViewModel
    {
        public IEnumerable<MovementChartModel> Collection { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }

        public VizViewModel(IEnumerable<MovementChartModel> collection, string title, string name)
        {
            Collection = collection;
            Title = title;
            Name = name;
        }
    }
}