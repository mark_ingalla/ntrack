﻿namespace PalletPlus.Website.ViewModels.Dashboard
{
    public class DashboardViewModel
    {
        public VizViewModel InOutChart { get; set; }

        public VizViewModel IssueReturnChart { get; set; }

        public VizViewModel InternalChart { get; set; }
    }
}