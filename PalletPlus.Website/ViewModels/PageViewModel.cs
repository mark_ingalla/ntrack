﻿

using System.ComponentModel.DataAnnotations;
using PalletPlus.Website.Models;

namespace PalletPlus.Website.ViewModels
{
    public class PageViewModel
    {
        [Display(Name = "Login")]
        public string Login { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
}