﻿/* ===================================================
* Copyright 2013 IDP Solutions.
*
* nTrack palletes application
* ========================================================== */

function resetNav() {
    $('.bs-docs-sidenav').each(function () {
        $(this).find('li').each(function () {
            $(this).removeClass("active");
        });
    });
}

function ValidateItem(id, validate, msg) {
    if (validate == false) {
        $("#"+id).removeAttr("data-val");
        $("#" + id).removeAttr("data-val-required");
    }
    else {
        $("#"+id).attr("data-val", "true");
        $("#" + id).attr("data-val-required", msg);
    }
    $('form').removeData('unobtrusiveValidation');
    $('form').removeData('validator');
    $.validator.unobtrusive.parse($('form'));
}

function CallView(url, divId) {
    $.ajax({
        type: "POST",
        url: url,
        cache: false
    }).done(function (msg) {

        $("#" + divId).html(msg);
    });
}

function IsGuidValidate(str) {
    var re = /^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$/;
    return re.test(str);
}


function isDate(txtDate) {
    var currVal = txtDate;
    if (currVal == '')
        return false;

    //Declare Regex  
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
        return false;

    //Checks for dd/mm/yyyy format.        
    dtDay = dtArray[1];
    dtMonth = dtArray[3];
    dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay > 31)
        return false;
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
        return false;
    else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
            return false;
    }
    return true;
}

$.extend({
    getUrlVars: function () {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    getUrlVar: function (name) {
        return $.getUrlVars()[name];
    }
});
