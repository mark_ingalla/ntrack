﻿using System;
using System.Web;

namespace PalletPlus.Website.Services
{
    public interface IClientSideStoreServices
    {
        Guid? GetContextSiteID(HttpCookieCollection requestCookies);
        void SetContextSiteID(HttpCookieCollection responseCookies, Guid contextSiteID);
    }
}