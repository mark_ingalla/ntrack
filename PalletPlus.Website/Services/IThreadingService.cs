﻿using System;
using PalletPlus.Data;
using nTrack.Data;

namespace PalletPlus.Website.Services
{
    //TODO: how do u know if the service timed out?
    public interface IThreadingService
    {
        void ObjectCreated<T>(Guid id) where T : BaseModel;
        void ObjectDeleted<T>(Guid id) where T : BaseModel;
        void ObjectUndeleted<T>(Guid id) where T : BaseModel;
        void ObjectUpdated<T>(Guid id, DateTime now) where T : BaseModel;
        void UserUpdated(Guid accountId, Guid key, DateTime now);
    }
}