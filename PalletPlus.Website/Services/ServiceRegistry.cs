﻿using System.Web;
using StructureMap.Configuration.DSL;
using nTrack.Extension;

namespace PalletPlus.Website.Services
{
    public class ServiceRegistry : Registry
    {
        public ServiceRegistry()
        {
            For<IClientSideStoreServices>().Use<ClientSideStoreServices>();
            //For<ISessionService>().Use<SessionService>();
            For<IThreadingService>().Use<BaseThreadingService>();
            For<HttpSessionStateBase>().Use(ctx => new HttpSessionStateWrapper(HttpContext.Current.Session));
            For<IAttachmentService>().Use<AttachmentService>();
        }
    }
}