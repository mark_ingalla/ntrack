﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PalletPlus.Website.Services
{
    public interface ISessionService
    {
        void Clear();

        void Delete(string key);

        object Get(string key);

        void Store(string key, object value);
    }
}