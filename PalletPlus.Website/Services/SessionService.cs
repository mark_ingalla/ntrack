﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PalletPlus.Website.Services
{
    public class SessionService : ISessionService
    {
        private readonly HttpSessionStateBase _session;

        public SessionService(HttpSessionStateBase session)
        {
            this._session = session;
        }

        public void Clear()
        {
            _session.RemoveAll();
        }

        public void Delete(string key)
        {
            _session.Remove(key);
        }

        public object Get(string key)
        {
            return _session[key];
        }

        public void Store(string key, object value)
        {
            _session[key] = value;
        }
    }

    public static class SessionExtensions
    {
        public static T Get<T>(this ISessionService sessionState, string key) where T : class
        {
            return sessionState.Get(key) as T;
        }
    }
}