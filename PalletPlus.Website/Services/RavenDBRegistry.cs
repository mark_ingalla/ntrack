﻿using System.Threading;
using PalletPlus.Website.Helpers;
using PalletPlus.Website.Infrastructure;
using Raven.Client;
using Raven.Client.Document;
using StructureMap.Configuration.DSL;

namespace PalletPlus.Website.Services
{
    public class RavenDBRegistry : Registry
    {
        public RavenDBRegistry()
        {
            ForSingletonOf<IDocumentStore>().Use(
                () =>
                {
                    var store = new DocumentStore { ConnectionStringName = "ClientDB" };
                    store.RegisterListener(new UpdateDateListener());
                    store.RegisterListener(new UpdateStringIdListener());
                    store.Initialize();

                    return store;
                });

            For<IDocumentSession>().HybridHttpOrThreadLocalScoped()
                .Use(context =>
            {

                string database;

                var userIdentity = Thread.CurrentPrincipal.Identity as CustomIdentity;

                if (userIdentity != null)
                    database = userIdentity.DatabaseName;
                else
                    return null;

                var store = context.GetInstance<IDocumentStore>();

                var session = string.IsNullOrEmpty(database) ? store.OpenSession() : store.OpenSession(database);
                session.Advanced.UseOptimisticConcurrency = true;

                return session;
            });
        }
    }
}