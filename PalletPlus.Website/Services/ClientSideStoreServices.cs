﻿using System;
using System.Web;

namespace PalletPlus.Website.Services
{
    public class ClientSideStoreServices : IClientSideStoreServices
    {
        private const string CONTEXT_COOKIE_NAME = "PalletPlusContext";
        private const string CONTEXT_ACCOUNT_KEY = "Account";

        public Guid? GetContextSiteID(HttpCookieCollection requestCookies)
        {
            Guid? contextSiteID = null;

            HttpCookie contextCookie = requestCookies[CONTEXT_COOKIE_NAME];
            if (contextCookie != null && contextCookie.Values[CONTEXT_ACCOUNT_KEY] != null)
                contextSiteID = Guid.Parse(contextCookie.Values[CONTEXT_ACCOUNT_KEY]);

            return contextSiteID;
        }

        public void SetContextSiteID(HttpCookieCollection responseCookies, Guid contextSiteID)
        {
            HttpCookie contextCookie = new HttpCookie(CONTEXT_COOKIE_NAME);
            contextCookie[CONTEXT_ACCOUNT_KEY] = contextSiteID.ToString();
            responseCookies.Add(contextCookie);
        }
    }
}