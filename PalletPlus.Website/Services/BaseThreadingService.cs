﻿using System;
using System.Linq;
using nTrack.Core.Utilities;
using nTrack.Data;
using nTrack.Data.Extenstions;
using nTrack.Data.Indices;
using Raven.Client;

namespace PalletPlus.Website.Services
{
    public class BaseThreadingService : IThreadingService
    {
        readonly IDocumentSession _session;
        readonly IThreadingUtilities _threadingUtilities;
        const int DELAY = 50;


        public BaseThreadingService(IDocumentSession session, IThreadingUtilities threadingUtilities)
        {
            this._session = session;
            this._threadingUtilities = threadingUtilities;
        }

        public void ObjectCreated<T>(Guid id) where T : BaseModel
        {
            _threadingUtilities.WaitOnPredicate(DELAY, 2, () => _session.Load<T>(id) != null);
        }

        public void ObjectDeleted<T>(Guid id) where T : BaseModel
        {
            _threadingUtilities.WaitOnPredicate(DELAY, 2, () => _session.Load<T>(id).IsDeleted);
        }

        public void ObjectUndeleted<T>(Guid id) where T : BaseModel
        {
            _threadingUtilities.WaitOnPredicate(DELAY, 2, () => !_session.Load<T>(id).IsDeleted);
        }

        public void ObjectUpdated<T>(Guid id, DateTime now) where T : BaseModel
        {
            _threadingUtilities.WaitOnPredicate(DELAY, 2, () => _session.Load<T>(id).UpdatedOn >= now);
        }

        public void UserUpdated(Guid accountId, Guid key, DateTime now)
        {
            using (var systemSession = MvcApplication.SystemStore.OpenSession())
            {
                _threadingUtilities.WaitOnPredicate(DELAY, 2,
                                                    () =>
                                                    systemSession.Query<UserIndex.Result, UserIndex>()
                                                    .Customize(x => x.WaitForNonStaleResultsAsOfLastWrite(TimeSpan.FromSeconds(1)))
                                                    .Any(
                                                         x =>
                                                         x.UserKey == key && x.AccountId == accountId.ToStringId<nTrack.Data.Account>() &&
                                                         x.AccountUpdateOn > now));
            }
        }
    }
}