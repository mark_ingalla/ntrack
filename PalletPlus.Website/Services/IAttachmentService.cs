﻿using System;
using System.Drawing;
using System.IO;
using System.Web;
using PalletPlus.Website.Helpers;
using Raven.Abstractions.Data;
using Raven.Client;

namespace PalletPlus.Website.Services
{
    public interface IAttachmentService
    {
        Attachment GetAttachment(string id);
        void PutAttachment(string id, Stream data);
        void DeleteAttachment(string id);
        Image GetImageFromSystemDb(Guid id, string entity);
    }

    public class AttachmentService : IAttachmentService
    {
        readonly IDocumentStore _store;
        string _databaseName;


        public AttachmentService(IDocumentStore store)
        {
            _store = store;

            var identity = HttpContext.Current.User.Identity as CustomIdentity;
            _databaseName = identity.DatabaseName;

        }

        public Attachment GetAttachment(string id)
        {
            var attachment = _store.DatabaseCommands.ForDatabase(_databaseName).GetAttachment(id);
            return attachment;
        }

        public void PutAttachment(string id, Stream data)
        {
            _store.DatabaseCommands.ForDatabase(_databaseName).PutAttachment(id, null, data, null);
        }

        public void DeleteAttachment(string id)
        {
            _store.DatabaseCommands.ForDatabase(_databaseName).DeleteAttachment(id, null);
        }

        public Image GetImageFromSystemDb(Guid id, string entity)
        {
            var attachment = MvcApplication.SystemStore.DatabaseCommands.GetAttachment(entity + "/" + id.ToString());

            if (attachment == null)
                return null;
            return Image.FromStream(attachment.Data());
        }

    }
}
