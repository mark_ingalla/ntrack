﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PalletPlus.Website.Areas.User.ViewModels.RecordMovement;
using PalletPlus.Website.Helpers;

namespace PalletPlus.Website
{
    public static class FieldsForRequirements
    {
        public static readonly string PartnerReference =
                ReflectionUtility.GetPropertyName((RecordMovementViewModel s) => s.PartnerReference);
        public static readonly string ConsignmentNote =
            ReflectionUtility.GetPropertyName((RecordMovementViewModel s) => s.ConNote);
        //public static readonly string MovementINDocketNumber =
        //    ReflectionUtility.GetPropertyName((RecordMovementViewModel s) => s.ProviderDocketNo);
        public static readonly string DocketNumber =
            ReflectionUtility.GetPropertyName((RecordMovementViewModel s) => s.ProviderDocketNo);
        public static readonly string MovementReference =
            ReflectionUtility.GetPropertyName((RecordMovementViewModel s) => s.Reference);
    }
}