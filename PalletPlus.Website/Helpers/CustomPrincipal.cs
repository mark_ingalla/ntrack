﻿using System.Linq;
using System.Security.Principal;

namespace PalletPlus.Website.Helpers
{
    public class CustomPrincipal : IPrincipal
    {
        public IIdentity Identity { get; private set; }
        public string[] Roles { get; private set; }

        public CustomPrincipal(CustomIdentity identity, string[] roles)
        {
            Identity = identity;
            Roles = roles;
        }

        public bool IsInRole(string role)
        {
            return Roles.Contains(role);
        }
    }
}