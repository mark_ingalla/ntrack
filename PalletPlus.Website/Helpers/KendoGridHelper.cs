﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PalletPlus.Website.Helpers
{
    public class KendoGridPost
    {
        public KendoGridPost()
        {
            if (HttpContext.Current != null)
            {
                HttpRequest curRequest = HttpContext.Current.Request;
                Page = Convert.ToInt32(curRequest["page"]);
                PageSize = Convert.ToInt32(curRequest["pageSize"]);
                Skip = Convert.ToInt32(curRequest["skip"]);
                Take = Convert.ToInt32(curRequest["take"]);

                SortOrd = curRequest["sort[0][dir]"];
                SortOn = curRequest["sort[0][field]"];
            }
        }

        public int Page { get; set; }
        public int PageSize { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
        public string SortOrd { get; set; }
        public string SortOn { get; set; }
    }
}