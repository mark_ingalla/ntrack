﻿using System.Configuration;

namespace PalletPlus.Website.Helpers
{
    public static class SettingsUtilities
    {
        public static string GetSettingValue(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
        
        public static string AuthenticationCookieName
        {
            get { return ConfigurationManager.AppSettings["AuthenticationCookieName"]; }
        }
    }
}