﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PalletPlus.Website.Helpers
{
    public static class TreeViewHelper
    {
        public static Dictionary<Guid, List<string>> ExtractSupplierFromTreeView(string[] checkedNodes)
        {
            if(checkedNodes == null) return new Dictionary<Guid, List<string>>();

            var ret = new Dictionary<Guid, List<string>>();

            for (int i = 0; i < checkedNodes.Length; i++)
            {
                var node = checkedNodes[i];
                if (node.StartsWith("SupplierItem")) continue;

                Guid supplier;
                List<string> equipment = ExtractCheckedEquipmentFromSupplier(ref i, out supplier, checkedNodes);
                ret.Add(supplier, equipment);
            }

            return ret;
        }

        public static List<string> ExtractCheckedEquipmentFromSupplier(ref int i, out Guid supplier, string[] checkedNodes)
        {
            var equipment = new List<string>();

            supplier = Guid.Parse(checkedNodes[i].Substring(0, 36));
            equipment.Add(checkedNodes[i].Substring(37));
            i++;
            int j;
            for (j = i; j < checkedNodes.Length; j++)
            {
                if (checkedNodes[j].StartsWith("SupplierItem"))  break; 

                Guid g = Guid.Parse(checkedNodes[j].Substring(0, 36));
                if (supplier != g) break;

                equipment.Add(checkedNodes[j].Substring(37));
            }
            i = j - 1;
            return equipment;
        }
    }
}
