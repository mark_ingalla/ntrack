﻿using System;
using System.Security.Principal;

namespace PalletPlus.Website.Helpers
{

    public class CustomIdentity : IIdentity
    {
        public string AuthenticationType { get { return "Forms"; } }
        public bool IsAuthenticated { get; private set; }
        public string Name { get; private set; }

        public Guid AccountId { get; private set; }
        public Guid UserKey { get; private set; }
        public string DatabaseName { get; private set; }
        public string UserFullName { get; private set; }

        public CustomIdentity(Guid accountId, Guid userKey, string userFullName, string databaseName)
        {
            AccountId = accountId;
            UserKey = userKey;

            IsAuthenticated = true;
            Name = userKey.ToString();
            UserFullName = userFullName;
            DatabaseName = databaseName;
        }

    }
}