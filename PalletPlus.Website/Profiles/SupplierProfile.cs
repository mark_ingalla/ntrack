﻿using System.Linq;
using AutoMapper;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Commands.Supplier;
using PalletPlus.Website.Areas.Admin.ViewModels.Supplier;

namespace PalletPlus.Website.Profiles
{
    public class SupplierProfile:Profile
    {
        public SupplierProfile()
        {
            Mapper.CreateMap<Supplier, SupplierViewModel>()
                .ForMember(d => d.SupplierId, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.SupplierName, opt => opt.MapFrom(s => s.CompanyName));

            Mapper.CreateMap<SupplierViewModel, CreateSupplier>()
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.SupplierName));

            Mapper.CreateMap<Supplier, SupplierEditViewModel>()
                .ForMember(d => d.SupplierId, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.SupplierName, opt => opt.MapFrom(s => s.CompanyName))
                .AfterMap((entity, vm) =>
                              {
                                  vm.Equipment = entity.Equipments.Values.ToList();
                              });

            Mapper.CreateMap<SupplierEditViewModel, UpdateSupplier>()
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.SupplierName));
        }
    }
}