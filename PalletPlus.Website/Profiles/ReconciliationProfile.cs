﻿using System;
using AutoMapper;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Website.Areas.Admin.ViewModels.ImportSupplierInvoiceData;
using PalletPlus.Website.Areas.Admin.ViewModels.Reconciliation;


namespace PalletPlus.Website.Profiles
{
    public class ReconciliationProfile : Profile
    {
        public ReconciliationProfile()
        {
            Mapper.CreateMap<InvoiceDetail, ReconciliationLineModel>()
                .ForMember(d => d.MovementDate, opt => opt.MapFrom(s => s.TRANSDATE))
                .ForMember(d => d.DocketNumber, opt => opt.MapFrom(s => s.PCMSDOCKET))
                .ForMember(d => d.EquipmentCode, opt => opt.MapFrom(s => s.EQUIPCODE))
                .ForMember(d => d.Quantity, opt => opt.MapFrom(s => s.QUANTITY))
                .ForMember(d => d.TradingPartnerName, opt => opt.MapFrom(s => s.OTHERPARTYNAME))
                .ForMember(d => d.TradingPartnerAccountNumber, opt => opt.MapFrom(s => s.OTHERPARTY))
                .AfterMap((detail, lm) =>
                              {
                                  lm.MovementKind = MovementKind.Transfer.ToString();

                                  if (detail.TRANSCODE == "Transfer Out" || detail.TRANSCODE == "Return")
                                      lm.MovementDirection = "Out";
                                  else if (detail.TRANSCODE == "Transfer In" || detail.TRANSCODE == "Issue")
                                      lm.MovementDirection = "In";

                                  lm.MovementDate = detail.TRANSDATE.HasValue
                                                        ? detail.TRANSDATE.Value.ToShortDateString()
                                                        : string.Empty;
                              }); ;
        }
    }
}