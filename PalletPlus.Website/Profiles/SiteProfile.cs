﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Commands.Site;
using PalletPlus.Website.Areas.Admin.ViewModels.Site;
using nTrack.Data.Extenstions;

namespace PalletPlus.Website.Profiles
{
    public class SiteProfile : Profile
    {
        public SiteProfile()
        {
            //Mapper.CreateMap<SiteIndex.Result, SiteViewModel>()
            //.ForMember(d => d.SiteId, opt => opt.MapFrom(s => s.SiteId.ToGuidId()));

            Mapper.CreateMap<SiteViewModel, CreateSite>()
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.SiteName));

            Mapper.CreateMap<Site, SiteEditViewModel>()
                .ForMember(d => d.SiteId, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.SiteName, opt => opt.MapFrom(s => s.CompanyName))
                .AfterMap((entity, vm) =>
                {
                    vm.SuppliersVM = entity.Suppliers.Values
                        .Select(x => new SiteSupplierViewModel(x.CompanyId.ToGuidId(), x.CompanyName,
                            x.AccountNumber, x.Prefix, x.Suffix, x.SequenceNumber, false)).ToList();
                });

            Mapper.CreateMap<CompanySupplier, SiteSupplierViewModel>()
                .ForMember(d => d.SupplierId, opt => opt.MapFrom(s => s.CompanyId.ToGuidId()));
            
            Mapper.CreateMap<SiteEditViewModel, UpdateSite>()
                 .ForMember(d => d.Name, opt => opt.MapFrom(s => s.SiteName));

            Mapper.CreateMap<SiteEditViewModel, AddSupplierToSite>()
                .ForMember(d => d.SupplierAccountNumber, opt => opt.MapFrom(s => s.SupplierVM.AccountNumber))
                .ForMember(d => d.SupplierId, opt => opt.MapFrom(s => s.SupplierVM.SupplierId))
                .ForMember(d => d.SupplierName, opt => opt.MapFrom(s => s.SupplierVM.SupplierName))
                .ForMember(d => d.Prefix, opt => opt.MapFrom(s => s.SupplierVM.Prefix))
                .ForMember(d => d.Suffix, opt => opt.MapFrom(s => s.SupplierVM.Suffix))
                .ForMember(d => d.SequenceNumber, opt => opt.MapFrom(s => s.SupplierVM.SequenceStartNumber));

            Mapper.CreateMap<SiteSupplierEditViewModel, AddSupplierToSite>();

            Mapper.CreateMap<SiteSupplierEditViewModel, UpdateSiteSupplier>();
        }
    }
}