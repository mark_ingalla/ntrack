﻿using System;
using System.Linq;
using System.Threading;
using AutoMapper;
using PalletPlus.Data;
using PalletPlus.Messages.Commands.Account;
using PalletPlus.Website.Areas.Admin.ViewModels.Company;
using PalletPlus.Website.Helpers;
using nTrack.Data.Extenstions;

namespace PalletPlus.Website.Profiles
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            //Mapper.CreateMap<Account, CompanyViewModel>()
            //    .ForMember(d => d.CompanyId, opt => opt.MapFrom(s => s.Id))
            //    .AfterMap((entity, vm) =>
            //                  {
            //                      vm.SuppliersVM = entity.Suppliers
            //                          .Select(x => new CompanySupplierViewModel
            //                                           {
            //                                               SupplierId = x.Key.ToGuidId(),
            //                                               AccountNumber = x.Value.AccountNumber,
            //                                               Prefix = x.Value.Prefix,
            //                                               Suffix = x.Value.Suffix,
            //                                               SequenceStartNumber = x.Value.SequenceNumber
            //                                           }).ToList();

            //                      vm.SettingsVM.CorrectionReasonsVM = entity.CorrectionReasons;
            //                      vm.SettingsVM.CancellationReasonsVM = entity.CancellationReasons;

            //                      vm.SettingsVM.EquipmentItemsColors = entity.DashboardEquipmentColors;
            //                  });


            //Mapper.CreateMap<CompanyViewModel, Account>()
            //    .ForMember(d => d.Id, opt => opt.MapFrom(s => s.CompanyId));


            //Mapper.CreateMap<CompanyViewModel, AddSupplierToAccount>()
            //    .ForMember(d => d.SupplierId, opt => opt.MapFrom(s => s.SupplierVM.SupplierId))
            //    .ForMember(d => d.SupplierName, opt => opt.MapFrom(s => s.SupplierVM.SupplierName))
            //    .ForMember(d => d.AccountNumber, opt => opt.MapFrom(s => s.SupplierVM.AccountNumber))
            //    .ForMember(d => d.Prefix, opt => opt.MapFrom(s => s.SupplierVM.Prefix))
            //    .ForMember(d => d.Suffix, opt => opt.MapFrom(s => s.SupplierVM.Suffix))
            //    .ForMember(d => d.SequenceStartNumber, opt => opt.MapFrom(s => s.SupplierVM.SequenceStartNumber))
            //    .AfterMap((vm, cmd) =>
            //    {
            //        var identity = Thread.CurrentPrincipal.Identity as CustomIdentity;
            //        cmd.AccountId = identity == null ? Guid.Empty : identity.AccountId;
            //    });

            //Mapper.CreateMap<CompanySupplierEditViewModel, UpdateAccountSupplier>()
            //    .AfterMap((vm, cmd) =>
            //    {
            //        var identity = Thread.CurrentPrincipal.Identity as CustomIdentity;
            //        cmd.AccountId = identity == null ? Guid.Empty : identity.AccountId;
            //    });

            //Mapper.CreateMap<CompanySupplier, CompanySupplierViewModel>()
            //    .ForMember(d => d.SupplierId, opt => opt.MapFrom(s => s.CompanyId));

            //Mapper.CreateMap<CompanySupplierViewModel, CompanySupplier>()
            //    .ForMember(d => d.CompanyId, opt => opt.MapFrom(s => s.SupplierId));

        }
    }
}