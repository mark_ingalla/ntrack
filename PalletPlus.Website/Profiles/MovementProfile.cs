﻿using AutoMapper;
using nTrack.Data.Extenstions;
using PalletPlus.Data.Indices;
using PalletPlus.Website.Areas.User.ViewModels.Movement;

namespace PalletPlus.Website.Profiles
{
    public class MovementProfile : Profile
    {
        public MovementProfile()
        {
            Mapper.CreateMap<MovementIndex.Result, MovementViewModel>()
                .ForMember(d => d.MovementId, opt => opt.MapFrom(s => s.MovementId.ToGuidId()))
                .ForMember(d => d.Direction, opt => opt.MapFrom(s => s.Direction))
                .ForMember(d => d.MovementDate, opt => opt.MapFrom(s => s.MovementDate))
                .ForMember(d => d.MovementType, opt => opt.MapFrom(s => s.MovementType))
                .ForMember(d => d.DocketNumber, opt => opt.MapFrom(s => s.DocketNumber));
        }
    }
}