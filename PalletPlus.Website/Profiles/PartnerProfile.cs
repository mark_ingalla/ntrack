﻿using AutoMapper;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Commands.Partner;
using PalletPlus.Website.Areas.Admin.ViewModels.Partner;

namespace PalletPlus.Website.Profiles
{
    public class PartnerProfile:Profile
    {
        public PartnerProfile()
        {
            Mapper.CreateMap<Partner, PartnerViewModel>()
                .ForMember(d => d.PartnerId, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.CompanyName, opt => opt.MapFrom(s => s.CompanyName));

            Mapper.CreateMap<PartnerViewModel, CreatePartner>()
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.CompanyName));

            Mapper.CreateMap<PartnerEditViewModel, UpdatePartner>()
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.PartnerName));

            Mapper.CreateMap<Partner, PartnerEditViewModel>()
                .ForMember(d => d.PartnerId, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.PartnerName, opt => opt.MapFrom(s => s.CompanyName));

            Mapper.CreateMap<PartnerEditViewModel, AddSupplierToPartner>()
                .ForMember(d => d.AccountNumber, opt => opt.MapFrom(s => s.SupplierVM.SupplierAccountNo))
                .ForMember(d => d.SupplierId, opt => opt.MapFrom(s => s.SupplierVM.SupplierId));
            //  .ForMember(d => d.EnableExchange, opt => opt.MapFrom(s => s.SupplierVM.EnableExchange));
        }
    }
}