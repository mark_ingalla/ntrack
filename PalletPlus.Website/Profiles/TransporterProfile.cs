﻿using AutoMapper;
using PalletPlus.Data;
using PalletPlus.Data.Indices;
using PalletPlus.Messages.Commands.Transporter;
using PalletPlus.Website.Areas.Admin.ViewModels.Transporter;
using nTrack.Data.Extenstions;

namespace PalletPlus.Website.Profiles
{
    public class TransporterProfile:Profile
    {
        public TransporterProfile()
        {
            Mapper.CreateMap<TransporterIndex.Result, TransporterViewModel>()
                .ForMember(d => d.TransporterId, opt => opt.MapFrom(s => s.TransporterId.ToGuidId()));

            Mapper.CreateMap<Transporter, TransporterViewModel>()
                .ForMember(d => d.TransporterId, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.TransporterName, opt => opt.MapFrom(s => s.CompanyName));

            Mapper.CreateMap<TransporterViewModel, CreateTransporter>()
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.TransporterName));

            Mapper.CreateMap<TransporterViewModel, UpdateTransporter>()
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.TransporterName));
        }
    }
}