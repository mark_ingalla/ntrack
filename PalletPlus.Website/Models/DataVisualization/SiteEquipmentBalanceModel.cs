﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PalletPlus.Website.Models.DataVisualization
{
    public class SiteEquipmentBalanceModel
    {
        public SiteEquipmentBalanceModel()
        {
        }

        public SiteEquipmentBalanceModel(string siteName, string equipmentName, int quantity)
        {
            SiteName = siteName;
            EquipmentName = equipmentName;
            Quantity = quantity;
        }

        public string SiteName { get; set; }
        public string EquipmentName { get; set; }
        public int Quantity { get; set; }
    }
}