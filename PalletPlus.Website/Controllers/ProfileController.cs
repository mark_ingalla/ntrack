﻿using System;
using System.Linq;
using System.Web.Mvc;
using AttributeRouting.Web.Mvc;
using nTrack.Data.Indices;
using nTrack.Web.Bootstrap;
using PalletPlus.Data;
using PalletPlus.Website.Infrastructure;
using PalletPlus.Website.Services;
using PalletPlus.Website.ViewModels.Profile;
using Raven.Client;
using Subscription.Messages.Commands.Account.User;

namespace PalletPlus.Website.Controllers
{
    [Authorize]
    public class ProfileController : BaseController
    {
        public IThreadingService ThreadingService { get; set; }

        public ProfileController(IThreadingService threadingService)
        {
            ThreadingService = threadingService;
        }

        [GET("Profile/EditProfile/{role}/{key}")]
        public ActionResult EditUser(Guid key, string role)
        {
            ViewBag.SidebarPartial = role == "admin" ? "~/Areas/Admin/Views/Shared/_SidebarPartial.cshtml" : "~/Areas/User/Views/Shared/_SidebarPartial.cshtml";
            ViewBag.LoginPartial = role == "admin" ? "~/Areas/Admin/Views/Shared/_LoginPartial.cshtml" : "~/Areas/User/Views/Shared/_LoginPartial.cshtml";
            var vm = new ProfileUserViewModel();

            using (var systemSession = MvcApplication.SystemStore.OpenSession())
            {
                var userResult = systemSession.Query<UserIndex.Result, UserIndex>()
                    .FirstOrDefault(x => x.UserKey == key);

                var userFullNameSplit = userResult.UserFullName.Split(' ');
                vm.FirstName = userFullNameSplit[0];
                vm.LastName = userFullNameSplit[1];

                vm.Email = userResult.Email;
                vm.Mobile = userResult.UserMobile;
                vm.Phone = userResult.UserPhone;
                vm.UserKey = userResult.UserKey;
            }

            return View("Index", vm);
        }

        [POST("Profile/EditProfile/{role}/{key}")]
        public ActionResult Update(ProfileUserViewModel vm, string role)
        {
            using (var systemSession = MvcApplication.SystemStore.OpenSession())
            {
                if (!IsUniqueEmail(systemSession, vm.Email, vm.UserKey))
                {
                    Failure("Email already in use.");
                    return RedirectToAction("EditUser", new { key = vm.UserKey });
                }

                Bus.Send<UpdateUserDetail>(cmd =>
                {
                    cmd.AccountId = Identity.AccountId;
                    cmd.FirstName = vm.FirstName;
                    cmd.LastName = vm.LastName;
                    cmd.Email = vm.Email;
                    cmd.Mobile = vm.Mobile;
                    cmd.Phone = vm.Phone;
                    cmd.UserKey = vm.UserKey;
                });

                var user = systemSession.Query<UserIndex.Result, UserIndex>()
                    .FirstOrDefault(c => c.Email == vm.Email && c.UserKey == vm.UserKey);
                if (user != null)
                {
                    //if (user.Role != vm.Role)
                    //{
                    //    UserRole userRole = (UserRole)Enum.Parse(typeof(UserRole), vm.Role);
                    //    Bus.Send(new ChangeUserRole() { AccountId = Identity.AccountId, Role = userRole, UserKey = user.UserKey });
                    //}

                    if (!string.IsNullOrWhiteSpace(vm.ChangePassword))
                    {
                        Bus.Send(new ChangeUserPassword()
                                     {
                                         AccountId = Identity.AccountId,
                                         NewPassword = vm.ChangePassword,
                                         UserKey = user.UserKey
                                     });
                    }
                }
            }

            //  ThreadingService.UserUpdated(Identity.AccountId, vm.UserKey, DateTime.UtcNow);
            ThreadingService.ObjectUpdated<Account>(Identity.AccountId, DateTime.Now);
            //if (!string.IsNullOrWhiteSpace(vm.ChangePassword))
            //{
            //    PasswordChanged();
            //}
            //else
            //{
            //    RecordUpdated("users");    
            //}
            
            return RedirectToAction("EditUser", new { key = vm.UserKey, role = role });
        }

        bool IsUniqueEmail(IDocumentSession systemSession, string email, Guid userKey)
        {
            bool exists = false;
            if (string.IsNullOrEmpty(email)) return false;

            exists = systemSession.Query<UserIndex.Result, UserIndex>()
                .Any(c => c.Email == email && c.UserKey != userKey);

            return !exists;
        }

        protected void RecordUpdated(string tabRouteValue)
        {
            TempData["alert-type"] = AlertType.Success;
            TempData["alert-message"] = "Your record has been updated.";
            TempData["tab-value"] = tabRouteValue;
        }

        protected void PasswordChanged()
        {
            TempData["alert-type"] = AlertType.Success;
            TempData["alert-message"] = "Your Password has been changed.";
            TempData["tab-value"] = "users";
        }

        protected void Failure(string message)
        {
            TempData["alert-type"] = AlertType.Error;
            TempData["alert-message"] = message;
        }

    }
}
