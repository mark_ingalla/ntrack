﻿using AttributeRouting.Web.Mvc;
using System.Web.Mvc;

namespace PalletPlus.Website.Controllers
{
    public class HomeController : Controller
    {
        [GET("")]
        public ActionResult Index()
        {
            if (User.IsInRole("Admin"))
                return RedirectToAction("Index", "Home", new { area = "Admin" });
            if (User.IsInRole("User"))
                return RedirectToAction("Index", "Home", new { area = "User" });

            return View();
        }
    }
}
