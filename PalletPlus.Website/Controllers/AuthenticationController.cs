﻿
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using AttributeRouting.Web.Mvc;
using nTrack.Core.Utilities;
using nTrack.Data.Indices;
using PalletPlus.Website.ViewModels;

namespace PalletPlus.Website.Controllers
{
    public class AuthenticationController : Controller
    {
        readonly ICryptographyService _cryptographyService;

        public AuthenticationController(ICryptographyService cryptographyService)
        {
            _cryptographyService = cryptographyService;
        }

        [GET("Authentication/Login")]
        public ActionResult Login()
        {
            if (Request.IsAuthenticated)
            {
                //FormsAuthentication.RedirectFromLoginPage(User.Identity.Name,);
            }

            return View("Login", new PageViewModel());
        }

        [POST("Authentication/Login")]
        public ActionResult Login(PageViewModel viewModel)
        {
            if (string.IsNullOrEmpty(viewModel.Login))
            {
                ModelState.AddModelError("Login", "You must provide an email for authentication!");
                viewModel.Password = string.Empty;
                return View(viewModel);
            }

            using (var session = MvcApplication.SystemStore.OpenSession())
            {
                var user = session.Query<UserIndex.Result, UserIndex>()
                                    .SingleOrDefault(
                                        x =>
                                        x.Email == viewModel.Login &&
                                        x.Password == _cryptographyService.GetMD5Hash(viewModel.Password));

                if (user == null)
                {
                    ModelState.AddModelError("Login", "Login or password incorrect.");
                    viewModel.Password = string.Empty;
                    return View(viewModel);
                }

                FormsAuthentication.SetAuthCookie(user.UserKey.ToString(), viewModel.RememberMe);

                FormsAuthentication.RedirectFromLoginPage(user.UserKey.ToString(), viewModel.RememberMe);
            }
            return null;
        }

        [GET("Authentication/Logout")]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}
