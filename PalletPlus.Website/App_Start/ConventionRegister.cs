using System.Web.Mvc;

namespace PalletPlus.Website.App_Start
{
    public class ConventionRegister
    {
        public static void Start()
        {
            ModelMetadataProviders.Current = new ConventionProvider();
        }
    }
}