﻿using System.Web;
using System.Web.Optimization;

namespace PalletPlus.Website.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-1.8.*",
                        "~/Scripts/jquery.unobtrusive-ajax.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/Bootstrap/bootstrap.min.js",
                        "~/Scripts/Bootstrap/bootstrap-combobox.js"));

            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                        "~/Scripts/Kendo/kendo.all*",
                        "~/Scripts/Kendo/kendo.aspnetmvc*",
                        "~/Scripts/Kendo/kendo.dropdownlist*",
                        "~/Scripts/Kendo/kendo.culture*"));

            bundles.Add(new ScriptBundle("~/bundles/dataviz").Include(
                        "~/Scripts/Kendo/kendo.dataviz.min*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate.*",
                        "~/Scripts/jquery.validate.unobtrusive.*"));

            bundles.Add(new ScriptBundle("~/bundles/nTrack").Include(
                        "~/Scripts/nTrackApp.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/bootstrap.css",
                        "~/Content/bootstrap-responsive.css",
                        "~/Content/bootstrap-combobox*",
                        "~/Content/validation*",
                        "~/Content/custom*",
                        "~/Content/telerik-report*"));

            bundles.Add(new StyleBundle("~/Content/Kendo/css").Include(
                        "~/Content/Kendo/kendo.common*",
                        "~/Content/Kendo/kendo.default*",
                        "~/Content/Kendo/kendo.dataviz*"));

            bundles.IgnoreList.Ignore("*.intellisense.js");
            bundles.IgnoreList.Ignore("*-vsdoc.js");
            bundles.IgnoreList.Ignore("*.debug.js", OptimizationMode.WhenEnabled);
        }
    }
}