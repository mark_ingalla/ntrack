using System.Linq;
using AutoMapper;
using PalletPlus.Website.App_Start;
using StructureMap;

[assembly: WebActivator.PostApplicationStartMethod(typeof(AutoMapperStart), "Start")]
namespace PalletPlus.Website.App_Start
{
    public static class AutoMapperStart
    {
        public static void Start()
        {
            var profiles = ObjectFactory.GetAllInstances<Profile>().ToList();
            profiles.ForEach(Mapper.AddProfile);
        }
    }
}