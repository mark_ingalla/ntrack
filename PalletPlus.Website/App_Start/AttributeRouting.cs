using System.Reflection;
using System.Web.Mvc;
using System.Web.Routing;
using AttributeRouting.Web.Mvc;

[assembly: WebActivator.PreApplicationStartMethod(typeof(PalletPlus.Website.App_Start.AttributeRouting), "Start")]


namespace PalletPlus.Website.App_Start
{
    public static class AttributeRouting
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapAttributeRoutes(
                config =>
                {
                    config.AddRoutesFromAssembly(Assembly.GetExecutingAssembly());
                    config.InheritActionsFromBaseController = true;
                    config.UseLowercaseRoutes = true;
                });
        }

        public static void Start()
        {
            RegisterRoutes(RouteTable.Routes);
        }
    }
}
