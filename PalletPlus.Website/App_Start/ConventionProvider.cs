﻿
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using nTrack.Extension;
using PalletPlus.Website.App_Start;

[assembly: WebActivator.PreApplicationStartMethod(typeof(ConventionRegister), "Start")]

namespace PalletPlus.Website.App_Start
{
    public class ConventionProvider : DataAnnotationsModelMetadataProvider
    {
        protected override ModelMetadata CreateMetadata(
            IEnumerable<Attribute> attributes,
            Type containerType,
            Func<object> modelAccessor,
            Type modelType,
            string propertyName)
        {
            var meta = base.CreateMetadata(attributes, containerType, modelAccessor, modelType, propertyName);

            if (meta.DisplayName == null)
                meta.DisplayName = meta.PropertyName.ToSeparateWords();

            return meta;
        }
    }
}