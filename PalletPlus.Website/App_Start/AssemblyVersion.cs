﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Web.Mvc;
using nTrack.Extension;
using PalletPlus.Website.App_Start;

namespace PalletPlus.Website.App_Start
{
    public class AssemblyVersion
    {
        public static void LoadAssemblyVersion()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);

            MvcApplication.Version = fvi.FileVersion;
        }
    }
}