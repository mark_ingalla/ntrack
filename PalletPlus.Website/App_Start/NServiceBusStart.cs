﻿using System.Configuration;
using NServiceBus;
using PalletPlus.Website.App_Start;
using StructureMap;

[assembly: WebActivator.PostApplicationStartMethod(typeof(NServiceBusStart), "Start")]

namespace PalletPlus.Website.App_Start
{
    public static class NServiceBusStart
    {
        public static void Start()
        {
            MvcApplication.Bus = Configure.With()
                .DefineEndpointName(ConfigurationManager.AppSettings["EndpointQueue"])
                .StructureMapBuilder(ObjectFactory.Container)
                .DefiningCommandsAs(
                    type => type != null && type.Namespace != null && type.Namespace.Contains("Messages.Commands"))
                .XmlSerializer()
                .MsmqTransport()
                .UnicastBus()
                .SendOnly();

        }
    }
}