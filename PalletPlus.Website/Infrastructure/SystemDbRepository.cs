﻿using nTrack.Data;
using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Extensions;
using System;
using System.Linq;

namespace PalletPlus.Website.Infrastructure
{
    public class SystemDbRepository : IDisposable
    {
        readonly IDocumentStore _clientStore;
        readonly IDocumentStore _store;

        public SystemDbRepository(IDocumentStore clientStore)
        {
            _clientStore = clientStore;
            _store = SystemDatabases();
        }

        IDocumentStore SystemDatabases()
        {
            var store = new DocumentStore { ConnectionStringName = "SystemDB" };

            store.RegisterListener(new NoStaleQueriesListener());

            store.Initialize();

            store.ResourceManagerId = Guid.NewGuid();

            return store;
        }

        public string ClientDatabaseName(Guid accountId)
        {
            string databaseName;
            using (var session = _store.OpenSession())
            {
                var database = session.Load<AccountDatabase>(accountId);
                if (database == null)
                {
                    database = new AccountDatabase { Id = accountId };
                    database.DatabaseName = string.Format("nTrack.C_{0}", accountId.ToString().Replace("-", ""));
                    session.Store(database);
                    session.SaveChanges();
                }

                databaseName = database.DatabaseName;
                _clientStore.DatabaseCommands.EnsureDatabaseExists(databaseName);


            }

            return databaseName;
        }

        public string[] ClientDatabases()
        {
            using (var session = _store.OpenSession())
            {
                return session.Query<AccountDatabase>().Select(x => x.DatabaseName).ToArray();
            }
        }

        public void Dispose()
        {
            if (_store != null)
            {
                _store.Dispose();
            }
        }
    }


}