﻿using Raven.Client;
using StructureMap.Configuration.DSL;

namespace PalletPlus.Website.Infrastructure
{
    public class SystemDbRepositoryRegistry : Registry
    {
        public SystemDbRepositoryRegistry()
        {
            ForSingletonOf<SystemDbRepository>()
                .Use(context => new SystemDbRepository(context.GetInstance<IDocumentStore>()));
        }
    }
}