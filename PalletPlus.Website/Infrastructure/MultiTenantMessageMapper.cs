﻿using System;
using System.Reflection;
using System.Threading;
using System.Web;
using NServiceBus.MessageMutator;
using NServiceBus.Unicast.Transport;
using PalletPlus.Website.Helpers;
using StructureMap.Configuration.DSL;

namespace PalletPlus.Website.Infrastructure
{
    public class MultiTenantMessageMapper : IMutateOutgoingTransportMessages
    {
        const string AccountHeader = "nTrack.AccountId";
        const string UserHeader = "nTrack.UserId";
        const string ApplicationHeader = "nTrack.ApplicationName";
        const string ApplicationVersionHeader = "nTrack.Version";
        private const string IPHeader = "nTrack.IP";

        public static Func<CustomIdentity> Identity = () => null;
        public static Func<string> IPAddress = () => "";

        public void MutateOutgoing(object[] messages, TransportMessage transportMessage)
        {
            if (Identity != null)
            {
                var identity = Identity.Invoke();

                if (identity != null)
                {
                    transportMessage.Headers[AccountHeader] = identity.AccountId.ToString();
                    transportMessage.Headers[UserHeader] = identity.UserKey.ToString();
                }
            }

            transportMessage.Headers[ApplicationHeader] = "PalletsPlusWebsite";
            transportMessage.Headers[ApplicationVersionHeader] = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            transportMessage.Headers[IPHeader] = IPAddress.Invoke();
        }
    }

    public class NServiceBusRegistry : Registry
    {
        public NServiceBusRegistry()
        {
            ForSingletonOf<IMutateOutgoingTransportMessages>()
                .Use(context =>
                    {
                        MultiTenantMessageMapper.Identity = () => Thread.CurrentPrincipal.Identity as CustomIdentity;
                        MultiTenantMessageMapper.IPAddress = () => HttpContext.Current.Request.UserHostAddress;
                        return new MultiTenantMessageMapper();
                    });
        }
    }
}