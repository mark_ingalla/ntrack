using nTrack.Data;
using Raven.Client.Listeners;
using Raven.Json.Linq;

namespace PalletPlus.Website.Infrastructure
{
    public class UpdateStringIdListener : IDocumentStoreListener
    {
        public bool BeforeStore(string key, object entityInstance, RavenJObject metadata, RavenJObject original)
        {
            var entity = entityInstance as BaseModel;
            if (entity != null)
            {
                entity.IdString = key;
            }

            return true;
        }

        public void AfterStore(string key, object entityInstance, RavenJObject metadata)
        {

        }
    }
}