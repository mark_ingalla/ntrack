﻿
using System.Web.Mvc;
using NServiceBus;
using PalletPlus.Data;
using PalletPlus.Website.Helpers;
using Raven.Client;
using StructureMap;

namespace PalletPlus.Website.Infrastructure
{
    public abstract class BaseController : Controller
    {
        protected IBus Bus { get { return MvcApplication.Bus; } }
        public CustomIdentity Identity { get { return User.Identity as CustomIdentity; } }



        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.Exception == null)
            {
                var session = ObjectFactory.GetInstance<IDocumentSession>();
                session.SaveChanges();
            }


            base.OnActionExecuted(filterContext);
        }


        //public bool IsTypeOfByStringId<T>(string id)
        //{
        //    switch(id.Split('/')[0])
        //    {
        //        case "sites":
        //            return typeof(T) == typeof (Site);
        //        case "suppliers":
        //            return typeof(T) == typeof (Supplier);
        //        case "partners":
        //            return typeof(T) == typeof (Partner);
        //        case "transporters":
        //            return typeof(T) == typeof (Transporter);
        //        default:
        //            return false;
        //    }
        //}
    }
}