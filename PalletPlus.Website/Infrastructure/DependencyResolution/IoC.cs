using StructureMap;

namespace PalletPlus.Website.Infrastructure.DependencyResolution
{

    public static class IoC
    {
        public static IContainer Initialize()
        {
            ObjectFactory.Initialize(x => x.Scan(
                scan =>
                {
                    scan.AssembliesFromApplicationBaseDirectory();
                    scan.TheCallingAssembly();
                    scan.LookForRegistries();
                    scan.WithDefaultConventions();
                    //scan.AddAllTypesOf<IImportPartnersService>();
                    //scan.AddAllTypesOf<ISupplierIntegration>();
                    //  scan.AddAllTypesOf<Profile>();
                })
                );
            return ObjectFactory.Container;
        }
    }
}