﻿using nTrack.Core.Messages;

namespace nTrack.EmailService.Messages.Commands
{
    public class SendEmail : MessageBase
    {
        public string FromName { get; set; }
        public string FromEmail { get; set; }
        public string ToName { get; set; }
        public string Bcc { get; set; }
        public string ToEmail { get; set; }
        public string Subject { get; set; }
        public string HtmlBody { get; set; }
        public string TextBody { get; set; }
    }
}
