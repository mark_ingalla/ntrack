﻿
namespace nTrack.EmailService.Messages.Events
{
    public class EmailSent
    {
        public bool IsSucessful { get; set; }
        public string Email { get; set; }
        public string ErrorMessage { get; set; }

        public EmailSent(bool isSucessful, string email, string errorMessage)
        {
            IsSucessful = isSucessful;
            Email = email;
            ErrorMessage = errorMessage;
        }
    }
}
