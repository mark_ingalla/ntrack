using System;

namespace PalletPlus.Messages
{
    public class ClosePendingStocktakeCorrectionSaga
    {
        public Guid StocktakeId { get; set; }
        public Guid CorrectionKeyToDecline { get; set; }

        public ClosePendingStocktakeCorrectionSaga(Guid stocktakeId, Guid correctionKey)
        {
            StocktakeId = stocktakeId;
            CorrectionKeyToDecline = correctionKey;
        }
    }
}