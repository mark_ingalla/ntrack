using System;

namespace PalletPlus.Messages
{
    public class ClosedPendingStocktakeCorrectionSaga
    {
        public Guid StocktakeId { get; set; }
        public Guid DeclinedCorrectionKey { get; set; }

        public ClosedPendingStocktakeCorrectionSaga(Guid stocktakeId, Guid correctionKey)
        {
            StocktakeId = stocktakeId;
            DeclinedCorrectionKey = correctionKey;
        }
    }
}