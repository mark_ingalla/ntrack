using System;

namespace PalletPlus.Messages.Events.Supplier
{
    public class SupplierUndeleted
    {
        public Guid SupplierId { get; set; }

        public SupplierUndeleted(Guid supplierId)
        {
            SupplierId = supplierId;
        }
    }
}