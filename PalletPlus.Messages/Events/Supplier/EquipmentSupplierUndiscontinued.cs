using System;

namespace PalletPlus.Messages.Events.Supplier
{
    public class EquipmentSupplierUndiscontinued
    {
        public Guid SupplierId { get; set; }
        public string EquipmentCode { get; set; }

        public EquipmentSupplierUndiscontinued(Guid supplierId, string equipmentCode)
        {
            SupplierId = supplierId;
            EquipmentCode = equipmentCode;
        }
    }
}