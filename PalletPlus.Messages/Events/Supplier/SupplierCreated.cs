using System;

namespace PalletPlus.Messages.Events.Supplier
{
    public class SupplierCreated
    {
        public Guid SupplierId { get; set; }
        public string Name { get; set; }
        public Guid LocationId { get; set; }
        public string AccountNumber { get; set; }
        public string LocationName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string ProviderName { get; set; }

        public SupplierCreated(Guid supplierId, string name, Guid locationId, string accountNumber, string locationName, string addressLine1, string addressLine2,
            string suburb, string state, string postcode, string country, string contactName, string contactEmail, string providerName)
        {
            SupplierId = supplierId;
            Name = name;
            LocationId = locationId;
            LocationName = locationName;
            AccountNumber = accountNumber;
            AddressLine1 = addressLine1;
            AddressLine2 = addressLine2;
            Suburb = suburb;
            State = state;
            Postcode = postcode;
            Country = country;
            ContactEmail = contactEmail;
            ContactName = contactName;
            ProviderName = providerName;
        }
    }
}