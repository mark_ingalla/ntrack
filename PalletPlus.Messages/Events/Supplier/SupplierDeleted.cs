using System;

namespace PalletPlus.Messages.Events.Supplier
{
    public class SupplierDeleted
    {
        public Guid SupplierId { get; set; }

        public SupplierDeleted(Guid supplierId)
        {
            SupplierId = supplierId;
        }
    }
}