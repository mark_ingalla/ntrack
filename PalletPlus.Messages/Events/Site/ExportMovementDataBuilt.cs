﻿using System;

namespace PalletPlus.Messages.Events.Site
{
    public class ExportMovementDataBuilt
    {
        public Guid SiteId { get; set; }
        public Guid SupplierId { get; set; }

        public ExportMovementDataBuilt(Guid siteId, Guid supplierId)
        {
            SiteId = siteId;
            SupplierId = supplierId;
        }
    }
}
