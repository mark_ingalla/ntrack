using System;

namespace PalletPlus.Messages.Events.Site
{
    public class PartnerForSiteEnabled
    {
        public Guid PartnerId { get; set; }
        public Guid SiteId { get; set; }

        public PartnerForSiteEnabled(Guid siteId, Guid partnerId)
        {
            PartnerId = partnerId;
            SiteId = siteId;
        }
    }
}