using System;

namespace PalletPlus.Messages.Events.Site
{
    public class SiteSupplierUpdated
    {
        public Guid SiteId { get; set; }
        public Guid SupplierId { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public int SequenceStartNumber { get; set; }

        public SiteSupplierUpdated(Guid siteId, Guid supplierId, string prefix, string suffix, int sequenceStartNumber)
        {
            SiteId = siteId;
            SupplierId = supplierId;
            Prefix = prefix;
            Suffix = suffix;
            SequenceStartNumber = sequenceStartNumber;
        }
    }
}