using System;

namespace PalletPlus.Messages.Events.Site
{
    public class SupplierToSiteAdded
    {
        public Guid SiteId { get; set; }
        public Guid SupplierId { get; set; }
        public string SupplierName { get; set; }
        public string AccountNumber { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public int SequenceStartNumber { get; set; }

        public SupplierToSiteAdded(Guid siteId, Guid supplierId, string supplierName,
            string accountNumber, string prefix, string suffix, int sequenceStartNumber)
        {
            SiteId = siteId;
            SupplierId = supplierId;
            SupplierName = supplierName;
            AccountNumber = accountNumber;
            Prefix = prefix;
            Suffix = suffix;
            SequenceStartNumber = sequenceStartNumber;
        }
    }
}
