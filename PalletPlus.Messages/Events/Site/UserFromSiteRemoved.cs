using System;

namespace PalletPlus.Messages.Events.Site
{
    public class UserFromSiteRemoved
    {
        public Guid SiteId { get; set; }
        public Guid UserId { get; set; }

        public UserFromSiteRemoved(Guid siteId, Guid userId)
        {
            SiteId = siteId;
            UserId = userId;
        }
    }
}