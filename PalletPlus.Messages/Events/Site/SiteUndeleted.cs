using System;

namespace PalletPlus.Messages.Events.Site
{
    public class SiteUndeleted
    {
        public Guid SiteId { get; set; }

        public SiteUndeleted(Guid siteId)
        {
            SiteId = siteId;
        }
    }
}