using System;

namespace PalletPlus.Messages.Events.Movement
{
    public class CorrectionDeclined
    {
        public Guid MovementId { get; set; }
        public Guid CorrectionKey { get; set; }
        public Guid ClosedBy { get; set; }

        public CorrectionDeclined(Guid movementId, Guid correctionKey, Guid closedBy)
        {
            MovementId = movementId;
            CorrectionKey = correctionKey;
            ClosedBy = closedBy;
        }
    }
}