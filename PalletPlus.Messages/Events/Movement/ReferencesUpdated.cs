using System;

namespace PalletPlus.Messages.Events.Movement
{
    public class ReferencesUpdated
    {
        public Guid MovementId { get; set; }
        public string MovementReference { get; set; }
        public string PartnerReference { get; set; }
        public string TransporterReference { get; set; }

        public ReferencesUpdated(Guid movementId, string mReference, string partnerReference, string transporterReference = "")
        {
            MovementId = movementId;
            MovementReference = mReference;
            PartnerReference = partnerReference;
            TransporterReference = transporterReference;
        }
    }
}