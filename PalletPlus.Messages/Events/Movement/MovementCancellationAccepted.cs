using System;

namespace PalletPlus.Messages.Events.Movement
{
    public class MovementCancellationAccepted
    {
        public Guid SiteId { get; set; }
        public Guid MovementId { get; set; }
        public Guid CancellationKey { get; set; }
        public Guid ClosedBy { get; set; }
        public string MovementType { get; set; }
        public string Reason { get; set; }

        public MovementCancellationAccepted(Guid siteId, Guid movementId, Guid cancellationKey, Guid closedBy, string movementType, string reason)
        {
            SiteId = siteId;
            MovementId = movementId;
            CancellationKey = cancellationKey;
            Reason = reason;
            ClosedBy = closedBy;
            MovementType = movementType;
        }
    }
}