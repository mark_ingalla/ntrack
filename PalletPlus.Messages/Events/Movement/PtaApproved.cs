using System;
using System.Collections.Generic;

namespace PalletPlus.Messages.Events.Movement
{
    public class PtaApproved
    {
        public Guid SiteId { get; set; }
        public Guid MovementId { get; set; }
        public Dictionary<string, int> Assets { get; set; }
        public string PtaApprovalNum { get; set; }
        public Guid SupplierId { get; set; }

        public PtaApproved(Guid siteId, Guid movementId, string ptaApprovalNum, Guid supplierId, Dictionary<string, int> assets)
        {
            SiteId = siteId;
            MovementId = movementId;
            PtaApprovalNum = ptaApprovalNum;
            SupplierId = supplierId;
            Assets = assets;
        }
    }
}