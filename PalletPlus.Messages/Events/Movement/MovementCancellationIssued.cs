using System;

namespace PalletPlus.Messages.Events.Movement
{
    public class MovementCancellationIssued
    {
        public Guid MovementId { get; set; }
        public Guid CancellationKey { get; set; }
        public DateTime RequestedOn { get; set; }
        public Guid RequestedBy { get; set; }
        public string Reason { get; set; }

        public MovementCancellationIssued(Guid movementId, Guid cancellationKey, DateTime requestedOn, Guid requestedBy, string reason)
        {
            MovementId = movementId;
            CancellationKey = cancellationKey;
            RequestedOn = requestedOn;
            RequestedBy = requestedBy;
            Reason = reason;
        }
    }
}