using System;

namespace PalletPlus.Messages.Events.Movement
{
    public class TransporterAdded
    {
        public Guid MovementId { get; set; }
        public Guid TransporterId { get; set; }

        public TransporterAdded(Guid movementId, Guid transporterId)
        {
            MovementId = movementId;
            TransporterId = transporterId;
        }
    }
}