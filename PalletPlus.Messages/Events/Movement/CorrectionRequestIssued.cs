using System;
using System.Collections.Generic;

namespace PalletPlus.Messages.Events.Movement
{
    public class CorrectionRequestIssued
    {
        public Dictionary<string, int> AssetCodeQuantity { get; set; }
        public Guid MovementId { get; set; }
        public Guid CorrectionKey { get; set; }
        public Guid RequestedBy { get; set; }
        public DateTime RequestedOn { get; set; }
        public DateTime NewDate { get; set; }
        public string Reason { get; set; }

        public CorrectionRequestIssued(Guid movementId, Guid correctionKey, Guid userId, DateTime requestedOn, Dictionary<string, int> assetCodeQuantity, 
            DateTime newDate, string reason)
        {
            MovementId = movementId;
            CorrectionKey = correctionKey;
            AssetCodeQuantity = assetCodeQuantity;
            RequestedBy = userId;
            RequestedOn = requestedOn;
            NewDate = newDate;
            Reason = reason;
        }
    }
}