using System;

namespace PalletPlus.Messages.Events.Movement
{
    public class PtaDeclined
    {
        public Guid MovementId { get; set; }

        public PtaDeclined(Guid movementId)
        {
            MovementId = movementId;
        }
    }
}