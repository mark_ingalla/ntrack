using System;

namespace PalletPlus.Messages.Events.Partner
{
    public class PartnerUndeleted
    {
        public Guid PartnerId { get; set; }

        public PartnerUndeleted(Guid partnerId)
        {
            PartnerId = partnerId;
        }
    }
}