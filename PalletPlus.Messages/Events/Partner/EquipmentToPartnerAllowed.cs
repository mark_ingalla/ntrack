using System;

namespace PalletPlus.Messages.Events.Partner
{
    public class EquipmentToPartnerAllowed
    {
        public Guid PartnerId { get; set; }
        public string EquipmentCode { get; set; }
        public Guid SupplierId { get; set; }

        public EquipmentToPartnerAllowed(Guid partnerId, Guid supplierId, string equipmentCode)
        {
            PartnerId = partnerId;
            EquipmentCode = equipmentCode;
            SupplierId = supplierId;
        }
    }
}