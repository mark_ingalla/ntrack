﻿using System;

namespace PalletPlus.Messages.Events.Stocktake
{
    public class StockCountCorrectionRequested
    {
        public Guid StocktakeId { get; set; }
        public Guid CorrectionKey { get; set; }
        public string Reason { get; set; }
        public string AssetCode { get; set; }
        public int Quantity { get; set; }
        public Guid UserKey { get; set; }

        public StockCountCorrectionRequested(Guid stocktakeId, Guid correctionKey, string assetCode, int quantity, string reason, Guid userKey)
        {
            StocktakeId = stocktakeId;
            CorrectionKey = correctionKey;
            Reason = reason;
            AssetCode = assetCode;
            Quantity = quantity;
            UserKey = userKey;
        }
    }
}
