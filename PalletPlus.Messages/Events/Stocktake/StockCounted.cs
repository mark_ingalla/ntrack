﻿using System;

namespace PalletPlus.Messages.Events.Stocktake
{
    public class StockCounted
    {
        public Guid StocktakeId { get; set; }
        public Guid SupplierId { get; set; }
        public Guid SiteId { get; set; }
        public Guid CountKey { get; set; }
        public string EquipmentCode { get; set; }
        public int Quantity { get; set; }

        public StockCounted(Guid stocktakeId, Guid supplierId, Guid siteId, Guid countKey, string equipmentCode, int quantity)
        {
            StocktakeId = stocktakeId;
            SupplierId = supplierId;
            EquipmentCode = equipmentCode;
            CountKey = countKey;
            Quantity = quantity;
            SiteId = siteId;
        }
    }
}
