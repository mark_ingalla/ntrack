﻿using System;
using System.Collections.Generic;

namespace PalletPlus.Messages.Events.Stocktake
{
    public class StocktakeCorrectionDeclined
    {
        public Guid StocktakeId { get; set; }
        public Guid CorrectionKey { get; set; }
        public Guid ClosedBy { get; set; }

        public StocktakeCorrectionDeclined(Guid stocktakeId, Guid correctionKey, Guid closedBy)
        {
            StocktakeId = stocktakeId;
            CorrectionKey = correctionKey;
            ClosedBy = closedBy;
        }
    }
}