using PalletPlus.Messages.Commands.Invoice;
using System;
using System.Collections.Generic;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Events.Invoice
{
    public class InvoiceImported
    {
        public Guid InvoiceId { get; set; }
        public string FileName { get; set; }
        public Guid SupplierId { get; set; }
        public nTrack.Core.Messages.Invoice Invoice { get; set; }

        public InvoiceImported(Guid invoiceId, string fileName, Guid supplierId, nTrack.Core.Messages.Invoice items)
        {
            InvoiceId = invoiceId;
            FileName = fileName;
            Invoice = items;
            SupplierId = supplierId;
        }
    }
}