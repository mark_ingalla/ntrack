using System;

namespace PalletPlus.Messages.Events.Transporter
{
    public class TransporterCreated
    {
        public Guid TransporterId { get; set; }
        public string Name { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }

        public string ContactName { get; set; }
        public string ContactEmail { get; set; }

        public TransporterCreated(Guid transporterId, string name, string addressLine1, string addressLine2,
            string suburb, string state, string postcode, string country, string contactName, string contactEmail)
        {
            TransporterId = transporterId;
            Name = name;
            AddressLine1 = addressLine1;
            AddressLine2 = addressLine2;
            Suburb = suburb;
            State = state;
            Postcode = postcode;
            Country = country;
            ContactEmail = contactEmail;
            ContactName = contactName;
        }
    }
}