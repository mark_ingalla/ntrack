using System;

namespace PalletPlus.Messages.Events.Transporter
{
    public class TransporterDeleted
    {
        public Guid TransporterId { get; set; }

        public TransporterDeleted(Guid transporterId)
        {
            TransporterId = transporterId;
        }
    }
}