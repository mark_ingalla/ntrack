using System;

namespace PalletPlus.Messages.Events.Account
{
    public class SiteInAccountClosed
    {
        public Guid AccountId { get; set; }
        public Guid SiteId { get; set; }

        public SiteInAccountClosed(Guid accountId, Guid siteId)
        {
            AccountId = accountId;
            SiteId = siteId;
        }
    }
}