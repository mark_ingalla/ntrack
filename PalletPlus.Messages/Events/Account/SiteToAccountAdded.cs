using System;

namespace PalletPlus.Messages.Events.Account
{
    public class SiteToAccountAdded
    {
        public Guid AccountId { get; set; }
        public Guid SiteId { get; set; }

        public SiteToAccountAdded(Guid accountId, Guid siteId)
        {
            AccountId = accountId;
            SiteId = siteId;
        }
    }
}