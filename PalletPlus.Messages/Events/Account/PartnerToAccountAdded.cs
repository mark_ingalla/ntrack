using System;

namespace PalletPlus.Messages.Events.Account
{
    public class PartnerToAccountAdded
    {
        public Guid AccountId { get; set; }
        public Guid PartnerId { get; set; }
        public string PartnerAccountNo { get; set; }

        public PartnerToAccountAdded(Guid accountId, Guid partnerId, string partnerAccountNo)
        {
            AccountId = accountId;
            PartnerId = partnerId;
            PartnerAccountNo = partnerAccountNo;
        }
    }
}