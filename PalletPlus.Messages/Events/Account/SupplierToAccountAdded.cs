using System;

namespace PalletPlus.Messages.Events.Account
{
    public class SupplierToAccountAdded
    {
        public Guid AccountId { get; set; }
        public Guid SupplierId { get; set; }
        public string SupplierName { get; set; }
        public string AccountNumber { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public int SequenceStartNumber { get; set; }

        public SupplierToAccountAdded(Guid accountId, Guid supplierId, string supplierName, string accountNumber, string prefix, string suffix, int sequenceStartNumber)
        {
            AccountId = accountId;
            SupplierId = supplierId;
            SupplierName = supplierName;
            AccountNumber = accountNumber;
            Prefix = prefix;
            Suffix = suffix;
            SequenceStartNumber = sequenceStartNumber;
        }
    }
}
