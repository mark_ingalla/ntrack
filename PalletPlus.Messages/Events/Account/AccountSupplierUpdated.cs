using System;

namespace PalletPlus.Messages.Events.Account
{
    public class AccountSupplierUpdated
    {
        public Guid AccountId { get; set; }
        public Guid SupplierId { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public int SequenceStartNumber { get; set; }

        public AccountSupplierUpdated(Guid accountId, Guid supplierId, string prefix, string suffix, int sequenceStartNumber)
        {
            AccountId = accountId;
            SupplierId = supplierId;
            Prefix = prefix;
            Suffix = suffix;
            SequenceStartNumber = sequenceStartNumber;
        }
    }
}