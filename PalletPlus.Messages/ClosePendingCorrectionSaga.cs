using System;

namespace PalletPlus.Messages
{
    public class ClosePendingCorrectionSaga
    {
        public Guid MovementId { get; set; }
        public Guid CorrectionKeyToDecline { get; set; }

        public ClosePendingCorrectionSaga(Guid movementId, Guid correctionKey)
        {
            MovementId = movementId;
            CorrectionKeyToDecline = correctionKey;
        }
    }
}