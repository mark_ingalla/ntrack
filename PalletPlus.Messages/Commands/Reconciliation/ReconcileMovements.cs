using System.Collections.Generic;
using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Reconciliation
{
    public class ReconcileMovements : MessageBase
    {
        public                             Guid InvoiceId                  { get; set; }
        public         Dictionary<Guid, string> InvoiceDetailsToReconcile  { get; set; }
        public Dictionary<string, List<string>> MovementDetailsToReconcile { get; set; }
    }
}