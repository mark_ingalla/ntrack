using System.Collections.Generic;
using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Reconciliation
{
    public class CreateTransaction : MessageBase
    {
        public DateTime CreatedOn { get; set; }
        public Guid CreatedBy { get; set; }

        public Guid MovementId { get; set; }

        public Guid InvoiceId { get; set; }
        public Guid DetailKey { get; set; }
    }
}