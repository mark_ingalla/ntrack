using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Transporter
{
    public class DeleteTransporter : MessageBase
    {
        public Guid TransporterId { get; set; }
    }
}