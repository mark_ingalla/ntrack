using nTrack.Core.Messages;
using System;
namespace PalletPlus.Messages.Commands.Invoice
{
    public class ImportInvoice : MessageBase
    {
        public Guid InvoiceId { get; set; }
        public Guid SupplierId { get; set; }
        public string FileName { get; set; }

        public nTrack.Core.Messages.Invoice Invoice { get; set; }
    }
}