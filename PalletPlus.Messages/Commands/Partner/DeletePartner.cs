using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Partner
{
    public class DeletePartner : MessageBase
    {
        public Guid PartnerId { get; set; }
    }
}