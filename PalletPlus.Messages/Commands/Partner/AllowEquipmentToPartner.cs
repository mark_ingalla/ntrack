using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Partner
{
    public class AllowEquipmentToPartner : MessageBase
    {
        public Guid PartnerId { get; set; }
        public Guid SupplierId { get; set; }
        public string[] EquipmentCodes { get; set; }
    }
}