using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Movement
{
    public class RequestMovementCancellation : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid MovementId { get; set; }
        public DateTime RequestedOn { get; set; }
        public Guid RequestedBy { get; set; }

        public string Reason { get; set; }
    }
}