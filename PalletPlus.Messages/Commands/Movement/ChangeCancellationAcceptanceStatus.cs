﻿using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Movement
{
    public class ChangeCancellationAcceptanceStatus : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid MovementId { get; set; }
        public Guid ClosedBy { get; set; }
        public Guid CancellationKey { get; set; }
        public bool Accept { get; set; }
    }
}
