using nTrack.Core.Messages;
using System;
using System.Collections.Generic;

namespace PalletPlus.Messages.Commands.Movement
{
    public class CreateMovement : MessageBase
    {
        public Guid MovementId { get; set; }
        public Guid SiteId { get; set; }
        /// <summary>
        /// SiteID or PartnerID or SupplierID
        /// </summary>
        public Guid DestinationId { get; set; }

        public Guid SupplierId { get; set; }

        /// <summary>
        /// This is a supplier location to support Site <==> Supplier movement
        /// </summary>
        public Guid? LocationId { get; set; }

        public string Direction { get; set; }
        public DateTime MovementDate { get; set; }
        public DateTime EffectiveDate { get; set; }
        public Dictionary<string, int> Equipments { get; set; }

        public string DocketNumber { get; set; }
        public string MovementReference { get; set; }
        public string DestinationReference { get; set; }
        public string ConsignmentNote { get; set; }
    }
}