﻿using nTrack.Core.Messages;
using System;
using System.Collections.Generic;

namespace PalletPlus.Messages.Commands.Movement
{
    public class ChangePtaMovementState : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid MovementId { get; set; }
        public bool Accept { get; set; }
        public Dictionary<string, int> Equipments { get; set; }
        public string PtaApprovalNumber { get; set; }
    }
}
