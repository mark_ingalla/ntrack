using PalletPlus.Data;
using nTrack.Core.Messages;
using System;
using System.Collections.Generic;

namespace PalletPlus.Messages.Commands.Movement
{
    public class IssueQuantityCorrection : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid MovementId { get; set; }
        public Guid RequestedBy { get; set; }
        public DateTime NewDate { get; set; }
        public DateTime RequestedOn { get; set; }
        public Dictionary<string, int> EquipmentQuantity { get; set; }
        public string Reason { get; set; }
    }
}