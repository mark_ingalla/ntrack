using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Movement
{
    public class UseTransporterMovement : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid MovementId { get; set; }
        public Guid TransporterId { get; set; }
    }
}