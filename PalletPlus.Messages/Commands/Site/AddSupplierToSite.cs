using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Site
{
    public class AddSupplierToSite : MessageBase
    {
        public Guid SiteId { get; set; }
        public Guid SupplierId { get; set; }
        public string SupplierName { get; set; }
        public string SupplierAccountNumber { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public int SequenceStartNumber { get; set; }
    }
}