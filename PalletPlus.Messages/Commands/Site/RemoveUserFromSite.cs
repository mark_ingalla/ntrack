using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Site
{
    public class RemoveUserFromSite : MessageBase
    {
        public Guid SiteId { get; set; }
        public Guid UserId { get; set; }
    }
}