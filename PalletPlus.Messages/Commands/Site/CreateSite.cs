using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Site
{
    public class CreateSite : MessageBase
    {
        public Guid SiteId { get; set; }
        public string Name { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
    }
}