﻿using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Stocktake
{
    public class CountStock : MessageBase
    {
        public Guid CountKey { get; set; }
        public Guid SiteId { get; set; }
        public Guid StocktakeId { get; set; }
        public Guid SupplierId { get; set; }
        public string EquipmentCode { get; set; }
        public int Quantity { get; set; }

    }
}
