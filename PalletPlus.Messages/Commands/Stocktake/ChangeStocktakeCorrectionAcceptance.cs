﻿using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Stocktake
{
    public class ChangeStocktakeCorrectionAcceptance : MessageBase
    {
        public Guid StocktakeId { get; set; }
        public Guid CorrectionKey { get; set; }
        public Guid ClosedBy { get; set; }
        public bool Accept { get; set; }
    }
}
