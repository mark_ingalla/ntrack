﻿using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Stocktake
{
    public class RequestStockCountCorrection : MessageBase
    {
        public Guid StocktakeId { get; set; }
        public Guid CorrectionKey { get; set; }
        public Guid UserKey { get; set; }
        public string EquipmentCode { get; set; }
        public int Quantity { get; set; }
        public string Reason { get; set; }
    }
}
