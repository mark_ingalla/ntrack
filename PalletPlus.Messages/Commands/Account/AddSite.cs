using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Account
{
    public class AddSite : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid SiteId { get; set; }

        public AddSite(Guid accountId, Guid siteId)
        {
            AccountId = accountId;
            SiteId = siteId;
        }
    }
}