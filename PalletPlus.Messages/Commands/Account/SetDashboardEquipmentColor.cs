﻿using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Account
{
    public class SetDashboardEquipmentColor : MessageBase
    {
        public Guid AccountId { get; set; }
        public string EquipmentCode { get; set; }
        public string EquipmentName { get; set; }
        public int R { get; set; }
        public int G { get; set; }
        public int B { get; set; }
    }
}
