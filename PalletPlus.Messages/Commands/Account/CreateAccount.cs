﻿using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Account
{
    public class CreateAccount : MessageBase
    {
        public Guid AccountId { get; set; }
        public string CompanyName { get; set; }
    }
}
