using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Account
{
    public class DisablePartnerInAccount : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid PartnerId { get; set; }
    }
}