using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Account
{
    public class UpdateAccountSupplier : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid SupplierId { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public int SequenceStartNumber { get; set; }
    }
}