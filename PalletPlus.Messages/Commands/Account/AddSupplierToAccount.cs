using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Account
{
    public class AddSupplierToAccount : MessageBase
    {
        public Guid AccountId { get; set; }
        public Guid SupplierId { get; set; }
        public string SupplierName { get; set; }
        public string AccountNumber { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public int SequenceStartNumber { get; set; }
    }
}