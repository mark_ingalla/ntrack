﻿using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Account
{
    public class RemoveCorrectionReason : MessageBase
    {
        public Guid ReasonKey { get; set; }
        public Guid AccountId { get; set; }
    }
}
