using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Account
{
    public class AddPartnerToAccount : MessageBase
    { 
        public Guid AccountId { get; set; }
        public Guid PartnerId { get; set; }
        public string PartnerAccountNumber { get; set; }
    }
}