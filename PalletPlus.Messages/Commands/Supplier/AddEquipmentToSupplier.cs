using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Commands.Supplier
{
    public class AddEquipmentToSupplier : MessageBase
    {
        public Guid SupplierId { get; set; }
        public string EquipmentCode { get; set; }
        public string EquipmentName { get; set; }
    }
}