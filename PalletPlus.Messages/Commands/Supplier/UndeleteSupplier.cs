using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Supplier
{
    public class UndeleteSupplier : MessageBase
    {
        public Guid SupplierId { get; set; }
    }
}