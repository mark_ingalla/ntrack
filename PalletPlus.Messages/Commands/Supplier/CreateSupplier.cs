using nTrack.Core.Messages;
using System;

namespace PalletPlus.Messages.Commands.Supplier
{
    public class CreateSupplier : MessageBase
    {
        public Guid SupplierId { get; set; }
        public string Name { get; set; }
        public string LocationName { get; set; }
        public string AccountNumber { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string ProviderName { get; set; }
    }
}