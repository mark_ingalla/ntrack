﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AuthenticationServer.ViewModels
{
    public class LoginForm
    {
        [Required]
        public string Email             { get; set; }
        [Required]
        public string Password          { get; set; }
        public bool   Remember          { get; set; }
        public string CallbackUrl       { get; set; }
        public string RegisterUrl       { get; set; }
        public string ForgotPasswordUrl { get; set; }
    }
}