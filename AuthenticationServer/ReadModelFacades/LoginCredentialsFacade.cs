﻿using System;
using System.Linq;
using Subscription.Data;
using nTrack.Core.Utilities;
using Raven.Client;

namespace AuthenticationServer.ReadModelFacades
{
    public class LoginCredentialsFacade : ILoginCredentialsFacade
    {
        private readonly IDocumentSession _dbSession;
        private readonly ICryptographyUtilities _cryptographyUtilities;

        public LoginCredentialsFacade(IDocumentSession dbSession, ICryptographyUtilities cryptographyUtilities)
        {
            _dbSession = dbSession;
            _cryptographyUtilities = cryptographyUtilities;
        }


        public bool CredentialsValid(string email, string password, out Guid userID)
        {
            string passwordHash = _cryptographyUtilities.GetMD5Hash(password);
            LoginCredentials loginInfo = _dbSession.Query<LoginCredentials>().FirstOrDefault(info => info.Email == email && info.PasswordHash == passwordHash);
            if (loginInfo != null)
                userID = loginInfo.UserID;
            else
                userID = Guid.Empty;

            return loginInfo != null;
        }
    }
}