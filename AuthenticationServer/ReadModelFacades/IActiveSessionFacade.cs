﻿using System;

namespace AuthenticationServer.ReadModelFacades
{
    public interface IActiveSessionFacade
    {
        bool ValidateSessionToken(Guid sessionToken);
    }
}