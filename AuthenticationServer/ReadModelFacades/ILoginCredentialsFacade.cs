﻿using System;

namespace AuthenticationServer.ReadModelFacades
{
    public interface ILoginCredentialsFacade
    {
        bool CredentialsValid(string email, string password, out Guid userID);
    }
}