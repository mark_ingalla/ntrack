﻿using System;
using System.Linq;
using Raven.Client;
using Subscription.Data;

namespace AuthenticationServer.ReadModelFacades
{
    public class ActiveSessionFacade : IActiveSessionFacade
    {
        private readonly IDocumentSession _dbSession;

        public ActiveSessionFacade(IDocumentSession dbSession)
        {
            _dbSession = dbSession;
        }

        public bool ValidateSessionToken(Guid sessionToken)
        {
            return _dbSession.Query<ActiveSession>().Any(s => s.SessionToken == sessionToken);
        }
    }
}