﻿
namespace AuthenticationServer.Services
{
    public enum ApplicationSetting { EncryptionSecret }

    public interface IApplicationSettingServices
    {
        string GetSettingValue(ApplicationSetting setting);
    }
}