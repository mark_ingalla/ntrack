﻿using System.Configuration;

namespace AuthenticationServer.Services
{
    public class ApplicationSettingServices : IApplicationSettingServices
    {
        public string GetSettingValue(ApplicationSetting setting)
        {
            return ConfigurationManager.AppSettings[setting.ToString()];
        }
    }
}