﻿using System;
using System.Web;

namespace AuthenticationServer.Services
{
    public interface IClientSideStoreServices
    {
        Guid? GetSessionToken(HttpCookieCollection requestCookies);
        void SetSessionToken(HttpCookieCollection responseCookies, Guid token, int daysToKeepCookie);
    }
}