﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthenticationServer.Services
{
    public class ClientSideStoreServices : IClientSideStoreServices
    {
        private const string AUTHENTICATION_COOKIE_NAME = "CentralAuth";
        private const string SESSION_TOKEN_KEY = "SessionToken";


        public Guid? GetSessionToken(HttpCookieCollection requestCookies)
        {
            Guid? sessionToken = null;

            HttpCookie authenticationCookie = requestCookies[AUTHENTICATION_COOKIE_NAME];
            if (authenticationCookie != null && authenticationCookie.Values[SESSION_TOKEN_KEY] != null)
                sessionToken = Guid.Parse(authenticationCookie.Values[SESSION_TOKEN_KEY]);

            return sessionToken;
        }

        public void SetSessionToken(HttpCookieCollection responseCookies, Guid token, int daysToKeepCookie)
        {
            HttpCookie authCookie = new HttpCookie(AUTHENTICATION_COOKIE_NAME);
            authCookie[SESSION_TOKEN_KEY] = token.ToString();
            if (daysToKeepCookie > 0)
                authCookie.Expires = DateTime.Now.AddDays(daysToKeepCookie);
            responseCookies.Add(authCookie);
        }
    }
}