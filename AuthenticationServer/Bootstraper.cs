﻿using System.Web.Mvc;
using AuthenticationServer.Infrastructure;
using AuthenticationServer.ReadModelFacades;
using AuthenticationServer.Services;
using NServiceBus;
using nTrack.Core.Utilities;
using Raven.Client;
using Raven.Client.Document;
using StructureMap;

namespace AuthenticationServer
{
    public static class Bootstraper
    {
        public static void Bootup()
        {
            IContainer container = BuildContainer();
            DependencyResolver.SetResolver(new StructureMapDependencyResolver(container));
            BuildServiceBus(container);
            BuildDocumentStore(container);
        }


        private static IContainer BuildContainer()
        {
            IContainer container = new Container();
            container.Configure(
                x =>
                {
                    x.Scan(
                        y =>
                        {
                            y.AssembliesFromApplicationBaseDirectory();
                            y.TheCallingAssembly();
                            y.WithDefaultConventions();
                        });

                    x.For<IActiveSessionFacade>().Use<ActiveSessionFacade>();
                    x.For<ILoginCredentialsFacade>().Use<LoginCredentialsFacade>();
                    x.For<IClientSideStoreServices>().Use<ClientSideStoreServices>();
                    x.For<IApplicationSettingServices>().Use<ApplicationSettingServices>();

                    x.For<ICryptographyUtilities>().Use<CryptographyUtilities>();
                    x.For<IThreadingUtilities>().Use<ThreadingUtilities>();
                });

            return container;
        }

        private static void BuildServiceBus(IContainer container)
        {
            IBus bus = NServiceBus.Configure.With()
                .DefiningCommandsAs(type => type != null && type.Namespace == "Subscription.Messages.Commands")
                .DefiningEventsAs(type => type != null && type.Namespace == "Subscription.Messages.Events")
                .DefiningMessagesAs(type => type != null && type.Namespace == "Subscription.Messages")
                .StructureMapBuilder(container)
                .Log4Net()
                .DefaultBuilder()
                .JsonSerializer()
                .MsmqTransport()
                    .IsTransactional(false)
                    .PurgeOnStartup(false)
                .UnicastBus()
                    .ImpersonateSender(false)
                .CreateBus()
                .Start();

            container.Configure(x => x.For<IBus>().Use(bus));
        }

        private static void BuildDocumentStore(IContainer container)
        {
            var documentStore = new DocumentStore { ConnectionStringName = "ReadModelDB" };
            documentStore.Initialize();

            container.Configure(x =>
            {
                x.ForSingletonOf<IDocumentStore>().Use(documentStore);
                x.For<IDocumentSession>().Use(ct => ct.GetInstance<IDocumentStore>().OpenSession());
            });
        }
    }
}