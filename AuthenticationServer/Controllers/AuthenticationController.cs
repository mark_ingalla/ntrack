﻿using System;
using System.Web;
using System.Web.Mvc;
using AuthenticationServer.ReadModelFacades;
using AuthenticationServer.Services;
using AuthenticationServer.ViewModels;
using NServiceBus;
using Subscription.Messages.Commands.User;
using nTrack.Core.Utilities;
using Subscription.Messages.Commands;

namespace AuthenticationServer.Controllers
{
    public class AuthenticationController : Controller
    {
        private const int AUTHENTICATION_COOKIE_LIFETIME_DAYS = 7;


        private readonly IBus _bus;
        private readonly IActiveSessionFacade _activeSessionFacade;
        private readonly ILoginCredentialsFacade _loginCredentialsFacade;
        private readonly IClientSideStoreServices _clientSideStoreServices;
        private readonly IApplicationSettingServices _applicationSettingServices;
        private readonly ICryptographyUtilities _cryptographyUtilities;
        private readonly IThreadingUtilities _threadingUtilities;


        public AuthenticationController(IBus bus, IActiveSessionFacade activeSessionFacade,
            ILoginCredentialsFacade loginCredentialsFacade, IClientSideStoreServices clientSideStoreServices,
            IApplicationSettingServices applicationSettingServices, ICryptographyUtilities cryptographyUtilities,
            IThreadingUtilities threadingUtilities)
        {
            _bus = bus;
            _activeSessionFacade = activeSessionFacade;
            _loginCredentialsFacade = loginCredentialsFacade;
            _clientSideStoreServices = clientSideStoreServices;
            _applicationSettingServices = applicationSettingServices;
            _cryptographyUtilities = cryptographyUtilities;
            _threadingUtilities = threadingUtilities;
        }


        //
        // GET: /Authenticate?callbackUrl=***&registerUrl=***&forgotPasswordUrl=***
        public ActionResult Authenticate(string callbackUrl, string registerUrl, string forgotPasswordUrl)
        {
            ActionResult actionResult = null;

            if (string.IsNullOrEmpty(callbackUrl) || !Uri.IsWellFormedUriString(callbackUrl, UriKind.Absolute))
            {
                actionResult = View(viewName: "Error", model: "Missing or invalid callback URL.");
            }
            else
            {
                // Check if the client is already logged in. If he is, then redirect to the relying party immediately.
                bool clientAlreadyAuthenticated = false;
                Guid? sessionToken = _clientSideStoreServices.GetSessionToken(Request.Cookies);
                if (sessionToken != null)
                {
                    bool sessionTokenValid = _activeSessionFacade.ValidateSessionToken(sessionToken.Value);
                    if (sessionTokenValid)
                    {
                        string callbackUrlWithSessionToken = AppendSessionTokenParameter(callbackUrl, sessionToken.Value);
                        actionResult = Redirect(callbackUrlWithSessionToken);
                        clientAlreadyAuthenticated = true;
                    }
                }

                // The client is not logged in. Display the login form.
                if (!clientAlreadyAuthenticated)
                {
                    LoginForm loginForm = new LoginForm
                    {
                        CallbackUrl = callbackUrl,
                        RegisterUrl = registerUrl,
                        ForgotPasswordUrl = forgotPasswordUrl
                    };
                    actionResult = View("Login", loginForm);
                }
            }

            return actionResult;
        }

        //
        // POST: /Authenticate
        [HttpPost]
        public ActionResult Authenticate(LoginForm loginForm)
        {
            ActionResult actionResult;

            if (!ModelState.IsValid)
            {
                actionResult = View("Login", loginForm);
            }
            else
            {
                Guid userID;
                bool credentialsValid = _loginCredentialsFacade.CredentialsValid(loginForm.Email, loginForm.Password, out userID);
                if (!credentialsValid)
                {
                    ModelState.AddModelError(string.Empty, "Invalid login credentials.");
                    actionResult = View("Login", loginForm);
                }
                else
                {
                    Guid sessionToken = Guid.NewGuid();
                    _bus.Send<LoginUser>(cmd =>
                        {
                            cmd.UserId = userID;
                            cmd.SessionToken = sessionToken;
                        });

                    // Wait till the read-model is updated.
                    bool loginSuccess = _threadingUtilities.WaitOnPredicate(ControllerSettings.WAIT_DELAY_MILLISEC,
                        ControllerSettings.WAIT_TIMEOUT_SEC, () => _activeSessionFacade.ValidateSessionToken(sessionToken));

                    if (loginSuccess)
                    {
                        _clientSideStoreServices.SetSessionToken(Response.Cookies, sessionToken,
                            loginForm.Remember ? AUTHENTICATION_COOKIE_LIFETIME_DAYS : 0);

                        string callbackUrlWithSessionToken = AppendSessionTokenParameter(loginForm.CallbackUrl, sessionToken);
                        actionResult = Redirect(callbackUrlWithSessionToken);
                    }
                    else
                    {
                        actionResult = View(viewName: "Error", model: "Server error! Could not log you in.");
                    }
                }
            }

            return actionResult;
        }


        private string AppendSessionTokenParameter(string callbackUrl, Guid sessionToken)
        {
            UriBuilder callbackUrlBuilder = new UriBuilder(callbackUrl);
            string encryptionSecret = _applicationSettingServices.GetSettingValue(ApplicationSetting.EncryptionSecret);
            string encryptedSessionToken = _cryptographyUtilities.Encrypt(sessionToken.ToString(), encryptionSecret);
            string newQueryString = "sessionToken=" + HttpUtility.UrlEncode(encryptedSessionToken);
            if (!string.IsNullOrEmpty(callbackUrlBuilder.Query))
                newQueryString = callbackUrlBuilder.Query.Substring(1) + "&" + newQueryString;
            callbackUrlBuilder.Query = newQueryString;

            return callbackUrlBuilder.Uri.AbsoluteUri;
        }
    }
}