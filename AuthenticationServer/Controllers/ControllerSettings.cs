﻿
namespace AuthenticationServer.Controllers
{
    public static class ControllerSettings
    {
        public const int WAIT_DELAY_MILLISEC = 500;
        public const int WAIT_TIMEOUT_SEC = 6;
    }
}