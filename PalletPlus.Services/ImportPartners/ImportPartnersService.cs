﻿using System;
using System.IO;
using System.Linq;
using FileHelpers;
using PalletPlus.Services.ImportPartners.Models;

namespace PalletPlus.Services.ImportPartners
{
    public sealed class ImportPartnerService
    {
        public ImportedPartnerModel[] ImportPartners(byte[] fileBytes, string fileName, out ErrorLogModel errorLog)
        {
            var eDetail = new FileHelperEngine<ImportedPartnerModel>();
            eDetail.AfterReadRecord += engine_AfterReadRecord;
            eDetail.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;

            using (var stream = new MemoryStream(fileBytes))
            using (TextReader textReader = new StreamReader(stream))
            {
                var dArray = eDetail.ReadStream(textReader);

                if (!dArray.Any())
                    throw new Exception("Incorrect format.");

                errorLog = new ErrorLogModel
                    {
                        TotalRecords = eDetail.TotalRecords,
                        ValidRecords = dArray.Length,
                        RowErrors = eDetail.ErrorManager.Errors.ToDictionary(x => x.LineNumber, y => y.ExceptionInfo.Message)
                    };

                return dArray;
            }
        }

        void engine_AfterReadRecord(EngineBase engine, AfterReadRecordEventArgs<ImportedPartnerModel> e)
        {
            if (String.IsNullOrWhiteSpace(e.Record.PartnerName))
                throw new Exception("Partner Name is empty.");

            if (String.IsNullOrWhiteSpace(e.Record.AddressLine1))
                throw new Exception("Address line 1 is empty.");

            if (String.IsNullOrWhiteSpace(e.Record.State))
                throw new Exception("State is empty");

            if (String.IsNullOrWhiteSpace(e.Record.Postcode))
                throw new Exception("Postcode is empty");

            if (String.IsNullOrWhiteSpace(e.Record.ContactName))
                throw new Exception("Contact Name is empty");

            if (String.IsNullOrWhiteSpace(e.Record.ContactEmail))
                throw new Exception("Contact Email is empty");
        }
    }
}
