﻿using System;
using FileHelpers;

namespace PalletPlus.Services.ImportPartners.Models
{
    [IgnoreFirst(1)]
    [DelimitedRecord(";")]
    public class ImportedPartnerModel
    {
        //compulsory (except for the AddressLine2)
        public string PartnerName;
        public string AddressLine1;
        public string AddressLine2;
        public string State;
        public string Postcode;
        public string ContactName;
        public string ContactEmail;

        //optional
        public string Site1;
        public string Site2;
        public string Site3;
        public string Site4;
        public string Site5;

        public string SupplierName;
        public string SupplierAccountNumber;

        [FieldNullValue(false)]
        public bool IsExchangeEnabled;

        public string EquipmentCode1;
        public string EquipmentCode2;
        public string EquipmentCode3;
        public string EquipmentCode4;
        public string EquipmentCode5;
    }
}
