﻿using System;
using System.Collections.Generic;
using System.Text;
using FileHelpers;

namespace PalletPlus.Services.ImportPartners.Models
{
    public class ErrorLogModel
    {
        public int TotalRecords { get; set; }
        public int ValidRecords { get; set; }
        public Dictionary<int, string> RowErrors { get; set; }

        public string ErrorMessage
        {
            get 
            { 
                var sb = new StringBuilder();
                foreach(var err in RowErrors)
                {
                    sb.AppendLine(string.Format("Line No. {0} is invalid : {1}", err.Key, err.Value));
                }

                return sb.ToString();
            }
        }
    }
}
