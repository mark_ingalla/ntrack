﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FileHelpers;
using nTrack.Extension;
using PalletPlus.Data;
using PalletPlus.Services.Suppliers.Loscam.Models;
using PalletPlus.Services.Suppliers.Loscam.Models.Export;
using PalletPlus.Services.Suppliers.Loscam.Models.ImportInvoice.InvoiceFormat1;

namespace PalletPlus.Services.Suppliers.Loscam
{
    public class LoscamIntegration : ISupplierIntegration
    {
        public string Name { get { return "Loscam"; } }

        //public Stream Export(KeyValuePair<string, string> siteAccountNumber, Dictionary<Guid, KeyValuePair<string, string>> destinationsSupplier, Movement[] movements)
        //{
        //    //details section
        //    var details = new List<ExportDetails>();
        //    foreach (var movement in movements)
        //    {
        //        var destinationIdGuid = movement.LocationKey == null ? movement.DestinationId.ToGuidId() : (Guid)movement.LocationKey;
        //        var destinationAccount = destinationsSupplier[destinationIdGuid];
        //        if (string.IsNullOrEmpty(destinationAccount.Key))
        //            throw new Exception(
        //                "found a partner without an account number. Movement.Id: {0}".Fill(movement.Id));

        //        var recordType = ExportRecordType.B;
        //        foreach (var asset in movement.Equipments)
        //        {
        //            var item = new ExportDetails
        //                           {
        //                               RECORD_TYPE = recordType,
        //                               EQUIPMENT_CODE = asset.Key,
        //                               CUSTOMER_ACCOUNT_NUMBER = siteAccountNumber.Key,
        //                               CUSTOMER_NAME = siteAccountNumber.Value,
        //                               TRANS_DATE = movement.MovementDate,
        //                               EFFECT_DATE = movement.EffectiveDate,
        //                               QUANTITY = asset.Value.Quantity,
        //                               DOCKET_NUMBER = movement.DocketNumber,
        //                               REFERENCE = movement.MovementReference,
        //                               TRADING_PARTNER_ACCOUNTNO = destinationAccount.Key,
        //                               TRADING_PARTNER_NAME = destinationAccount.Value
        //                           };

        //            details.Add(item);
        //        }
        //    }

        //    var summary = new ExportSummary
        //    {
        //        RECORD_TYPE = ExportRecordType.T,
        //        TOTAL_QUANTITY = details.Sum(x => x.QUANTITY),
        //        TOTAL_RECORDS = details.Count
        //    };

        //    var header0 = new ExportHeader0();
        //    var header1 = new ExportHeader1 { TRANS_COUNT = details.Count };
        //    var header2 = new ExportHeader2();
        //    var header3 = new ExportHeader3
        //                      {
        //                          CUSTOMERS_ACCOUNT_BATCH =
        //                              string.Format("{0}.{1}", siteAccountNumber.Key, "065WTFISTHIS?"),
        //                          CUSTOMERS_ACCOUNTNO = siteAccountNumber.Key,
        //                          CUSTOMERS_NAME = siteAccountNumber.Value
        //                      };

        //    var eSummary = new FileHelperEngine<ExportSummary>();
        //    var eHeaders0 = new FileHelperEngine<ExportHeader0>();
        //    var eHeaders1 = new FileHelperEngine<ExportHeader1>();
        //    var eHeaders2 = new FileHelperEngine<ExportHeader2>();
        //    var eHeaders3 = new FileHelperEngine<ExportHeader3>();
        //    var eDetails = new FileHelperEngine<ExportDetails>();

        //    var memStream = new MemoryStream();

        //    var stream = new StreamWriter(memStream);

        //    stream.Write(eHeaders0.WriteString(new[] { header0 }));
        //    stream.Write(eHeaders1.WriteString(new[] { header1 }));
        //    stream.Write(eHeaders2.WriteString(new[] { header2 }));
        //    stream.Write(eHeaders3.WriteString(new[] { header3 }));
        //    stream.Write(eDetails.WriteString(details));
        //    stream.Write(eSummary.WriteString(new[] { summary }));
        //    stream.Flush();

        //    return memStream;
        //}

        public byte[] Export(Movement[] movements)
        {
            //TODO: fix the site issue
            var siteIds = movements.Select(x => x.SiteId).Distinct().ToList();
            //var sites = _session.Load<Site>(siteIds);

            //details section
            var details = new List<ExportDetails>();
            foreach (var movement in movements)
            {
                //var site = sites.Single(x => x.IdString == movement.SiteId);
                if (string.IsNullOrEmpty(movement.DestinationAccountNumber))
                    throw new Exception("found a partner without an account number. Movement.Id: {0}".Fill(movement.Id));

                var recordType = ExportRecordType.B;
                foreach (var asset in movement.Equipments)
                {
                    var item = new ExportDetails
                    {
                        RECORD_TYPE = recordType,
                        EQUIPMENT_CODE = asset.Key,
                        CUSTOMER_ACCOUNT_NUMBER = movement.SiteAccountNumber,
                        //CUSTOMER_NAME = site.CompanyName,
                        TRANS_DATE = movement.MovementDate,
                        EFFECT_DATE = movement.EffectiveDate,
                        QUANTITY = asset.Value.Quantity,
                        DOCKET_NUMBER = movement.DocketNumber,
                        REFERENCE = movement.MovementReference,
                        TRADING_PARTNER_ACCOUNTNO = movement.DestinationAccountNumber,
                        TRADING_PARTNER_NAME = movement.DestinationName
                    };

                    details.Add(item);
                }
            }

            var summary = new ExportSummary
            {
                RECORD_TYPE = ExportRecordType.T,
                TOTAL_QUANTITY = details.Sum(x => x.QUANTITY),
                TOTAL_RECORDS = details.Count
            };

            var header0 = new ExportHeader0();
            var header1 = new ExportHeader1 { TRANS_COUNT = details.Count };
            var header2 = new ExportHeader2();
            var header3 = new ExportHeader3
            {
                CUSTOMERS_ACCOUNT_BATCH = string.Format("{0}.{1}", movements.First().SiteAccountNumber, "065WTFISTHIS?"),
                CUSTOMERS_ACCOUNTNO = movements.First().SiteAccountNumber,
                //CUSTOMERS_NAME = sites.First(x => x.IdString == movements.First().SiteId).CompanyName
            };

            var eSummary = new FileHelperEngine<ExportSummary>();
            var eHeaders0 = new FileHelperEngine<ExportHeader0>();
            var eHeaders1 = new FileHelperEngine<ExportHeader1>();
            var eHeaders2 = new FileHelperEngine<ExportHeader2>();
            var eHeaders3 = new FileHelperEngine<ExportHeader3>();
            var eDetails = new FileHelperEngine<ExportDetails>();

            using (var memStream = new MemoryStream())
            {
                using (var stream = new StreamWriter(memStream))
                {
                    stream.Write(eHeaders0.WriteString(new[] { header0 }));
                    stream.Write(eHeaders1.WriteString(new[] { header1 }));
                    stream.Write(eHeaders2.WriteString(new[] { header2 }));
                    stream.Write(eHeaders3.WriteString(new[] { header3 }));
                    stream.Write(eDetails.WriteString(details));
                    stream.Write(eSummary.WriteString(new[] { summary }));
                    stream.Flush();
                }
                return memStream.ToArray();
            }
        }

        public Suppliers.Models.SupplierInvoice ImportInvoice(string fileName, byte[] file)
        {
            return null;
        }

        public InvoiceAttachment ImportInvoice(Stream stream, string fileName)
        {
            var eDetail = new FileHelperEngine<InvoiceFormat1Detail>();

            TextReader textReader = new StreamReader(stream);
            var dArray = eDetail.ReadStream(textReader);
            var details = dArray.ToList();

            var customerAccountNumber = fileName.Split('.')[0];
            details.ForEach(action => action.CUSTOMERNUMBER = customerAccountNumber);

            var invoice = new InvoiceFormat1() { Details = details };

            //TODO: map invoice
            //return AutoMapper.Mapper.Map<InvoiceFormat1, InvoiceAttachment>(invoice);

            return null;
        }
    }
}
