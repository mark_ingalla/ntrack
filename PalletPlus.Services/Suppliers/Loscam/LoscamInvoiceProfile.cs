﻿using System;
using System.Collections.Generic;
using AutoMapper;
using PalletPlus.Data;
using PalletPlus.Services.Suppliers.Loscam.Models;
using PalletPlus.Services.Suppliers.Loscam.Models.ImportInvoice.InvoiceFormat1;

namespace PalletPlus.Services.Profiles
{
    public class LoscamInvoiceProfile : Profile
    {
        public LoscamInvoiceProfile()
        {
            Mapper.CreateMap<InvoiceFormat1, InvoiceAttachment>()
                .AfterMap((invoiceFormat1, invoice) =>
                              {
                                  invoice.Details = Mapper.Map<List<InvoiceFormat1Detail>, List<InvoiceDetail>>(invoiceFormat1.Details);
                              });

            Mapper.CreateMap<InvoiceFormat1Detail, InvoiceDetail>()
                .ForMember(d => d.TRANSNUMBER, opt => opt.MapFrom(s => s.TRANSNUMBER))
                .ForMember(d => d.OTHERPARTY, opt => opt.MapFrom(s => s.CUSTOMERNUMBER))
                .ForMember(d => d.OTHERPARTYNAME, opt => opt.MapFrom(s => s.CUSTOMERNAME))
                .ForMember(d => d.EQUIPCODE, opt => opt.MapFrom(s => s.EQUIPCODE))
                .ForMember(d => d.TRANSDATE, opt => opt.MapFrom(s => s.TRANSDATE))
                .ForMember(d => d.EFFECTDATE, opt => opt.MapFrom(s => s.EFFECTDATE))
                .ForMember(d => d.PCMSDOCKET, opt => opt.MapFrom(s => s.CUSTDOCKET))
                .ForMember(d => d.CONNOTE, opt => opt.MapFrom(s => s.AUTHORITY))
                .ForMember(d => d.QUANTITY, opt => opt.MapFrom(s => s.QUANTITY))
                .ForMember(d => d.TRANSPORTREF, opt => opt.MapFrom(s => s.TRANSPORTREF))
                .ForMember(d => d.TRANSPORTREGISTRATION, opt => opt.MapFrom(s => s.TRANSPORTREGISTRATION))
                .ForMember(d => d.INVOICENO, opt => opt.MapFrom(s => s.INVOICENUMBER))
                .ForMember(d => d.CORRECTION, opt => opt.MapFrom(s => s.CORRECTION))
                .AfterMap((if1, id) =>
                {
                    switch (if1.TRANSTYPE)
                    {
                        case TransCode.ON:
                            id.TRANSCODE = "Transfer In";
                            break;
                        case TransCode.OFF:
                            id.TRANSCODE = "Transfer Out";
                            break;
                        case TransCode.ISS:
                            id.TRANSCODE = "Issue";
                            break;
                        case TransCode.DEH:
                            id.TRANSCODE = "Return";
                            break;
                        default:
                            id.TRANSCODE = TransCode.UnKnown.ToString();
                            break;
                    }
                });
        }
    }
}