﻿using System;
using FileHelpers;

namespace PalletPlus.Services.Suppliers.Loscam.Models.ImportInvoice.InvoiceFormat1
{
    [DelimitedRecord(",")]
    public class InvoiceFormat1Detail
    {
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string TRANSNUMBER;
        [FieldConverter(typeof(TransCode_Converter))][FieldQuoted(QuoteMode.AlwaysQuoted)] public TransCode TRANSTYPE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CUSTOMERNUMBER;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CUSTOMERNAME;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string EQUIPCODE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)][FieldConverter(ConverterKind.Date, "ddMMyy")] public DateTime TRANSDATE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)][FieldConverter(ConverterKind.Date, "ddMMyy")] public DateTime EFFECTDATE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CUSTDOCKET;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string AUTHORITY;
        [FieldQuoted(QuoteMode.OptionalForBoth)] public int QUANTITY;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string TRANSPORTREF;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string TRANSPORTREGISTRATION;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string INVOICENUMBER;
        [FieldQuoted(QuoteMode.OptionalForBoth)] public bool CORRECTION;
    }
}
