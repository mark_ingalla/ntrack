﻿using System.Collections.Generic;

namespace PalletPlus.Services.Suppliers.Loscam.Models.ImportInvoice.InvoiceFormat1
{
    public class InvoiceFormat1
    {
        public List<InvoiceFormat1Detail> Details { get; set; }
    }
}
