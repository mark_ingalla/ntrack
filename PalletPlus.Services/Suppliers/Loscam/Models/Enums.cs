namespace PalletPlus.Services.Suppliers.Loscam.Models
{
    public enum ImportFormats
    {
        Format1
    }

    public enum TransCode
    {
        ISS,
        DEH,
        OFF,
        ON,
        UnKnown
    }

    public enum ExportRecordType
    {
        H0,
        H1,
        H2,
        H3,
        B,
        T
    }
}