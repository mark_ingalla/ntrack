﻿using FileHelpers;

namespace PalletPlus.Services.Suppliers.Loscam.Models.Export
{
    [DelimitedRecord(",")]
    public class ExportHeader0
    {
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public ExportRecordType RECORD_TYPE = ExportRecordType.H0;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string EDI_VERSION = "EDI Version 5c";
    }

    [DelimitedRecord(",")]
    public class ExportHeader1
    {
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public ExportRecordType RECORD_TYPE = ExportRecordType.H1;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public int TRANS_COUNT;
    }

    [DelimitedRecord(",")]
    public class ExportHeader2
    {
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public ExportRecordType RECORD_TYPE = ExportRecordType.H2;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CLIENT_SYSTEM = "CLIENT SYSTEM";
    }

    [DelimitedRecord(",")]
    public class ExportHeader3
    {
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public ExportRecordType RECORD_TYPE = ExportRecordType.H3;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CUSTOMERS_ACCOUNT_BATCH;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CUSTOMERS_ACCOUNTNO;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CUSTOMERS_NAME;
    }
}
