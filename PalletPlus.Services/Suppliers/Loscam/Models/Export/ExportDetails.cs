﻿using System;
using FileHelpers;

namespace PalletPlus.Services.Suppliers.Loscam.Models.Export
{
    [DelimitedRecord(",")]
    public class ExportDetails
    {
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public ExportRecordType RECORD_TYPE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string SUP_NAME = "LOSCAM";

        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string EQUIPMENT_CODE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CUSTOMER_ACCOUNT_NUMBER;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CUSTOMER_NAME;

        [FieldConverter(ConverterKind.Date, "dd/MM/yy")] public DateTime TRANS_DATE;
        [FieldConverter(ConverterKind.Date, "dd/MM/yy")] public DateTime EFFECT_DATE;

                                              public int QUANTITY;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string DOCKET_NUMBER;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string REFERENCE; //CON NOTE
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string TRADING_PARTNER_ACCOUNTNO;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string TRADING_PARTNER_NAME;
    }
}
