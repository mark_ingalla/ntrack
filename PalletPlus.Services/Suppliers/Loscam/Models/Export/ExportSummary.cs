﻿using FileHelpers;

namespace PalletPlus.Services.Suppliers.Loscam.Models.Export
{
    [DelimitedRecord(",")]
    public class ExportSummary
    {
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public ExportRecordType RECORD_TYPE;
        public int TOTAL_QUANTITY;
        public int TOTAL_RECORDS;
    }
}
