﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FileHelpers;
using nTrack.Extension;
using PalletPlus.Data;
using PalletPlus.Services.Suppliers.Chep.Models;
using PalletPlus.Services.Suppliers.Chep.Models.Export;
using PalletPlus.Services.Suppliers.Chep.Models.ImportInvoice.InvoiceFormat1;
using PalletPlus.Services.Suppliers.Chep.Models.ImportInvoice.InvoiceFormat2;
using PalletPlus.Services.Suppliers.Chep.Models.ImportInvoice.InvoiceFormat3;
using SupplierInvoice = PalletPlus.Services.Suppliers.Models.SupplierInvoice;

namespace PalletPlus.Services.Suppliers.Chep
{
    public class ChepIntegration : ISupplierIntegration
    {
        public string Name { get { return "CHEP"; } }

        public byte[] Export(Movement[] movements)
        {
            //details section
            var details = new List<ExportDetails>();
            foreach (var movement in movements)
            {
                if (string.IsNullOrEmpty(movement.DestinationAccountNumber))
                    throw new Exception("found a partner without an account number. Movement.Id: {0}".Fill(movement.Id));

                var recordType = ExportRecordType.D;
                foreach (var asset in movement.Equipments)
                {
                    var item = new ExportDetails
                        {
                            RECORD_TYPE = recordType,
                            PCMS_DOCKET = movement.DocketNumber,
                            DI_DE_DATE = movement.MovementDate,
                            DI_DISPATCH_DATE = movement.MovementDate,
                            DI_EFFECT_DATE = movement.EffectiveDate,
                            DI_TRANS_DATE = movement.MovementDate,
                            DI_RECEIPT_DATE = movement.MovementDate,
                            DI_REF1 = "",
                            DI_REF2 = movement.MovementReference,
                            DI_REF3 = "",
                            DI_REF4 = movement.DestinationAccountNumber,
                            PT_CUST_CODE = movement.SiteAccountNumber,
                            PT_DE_EQUIP = asset.Key,
                            PT_QUANTITY = asset.Value.Quantity,
                            SENDER = movement.SiteAccountNumber,
                            RECEIVER = movement.DestinationAccountNumber,
                            TRANSCODE = TransCode.TF
                        };

                    details.Add(item);

                    recordType = ExportRecordType.C;
                }
            }


            //summary section
            var summary = new ExportSummary
                {
                    DATE_CREATE = DateTime.Now,
                    ACCOUNT_NUMBER = movements.First().SiteAccountNumber.ToInt(),
                    APP_NAME = Settings.Current.AppName,
                    APP_VERSION = Settings.Current.AppVersion,
                    ROW_INDEX = 1,
                    RECORD_TYPE = ExportRecordType.H,
                    CHEP_REGION = Settings.Current.ChepRegion,
                    TOTAL_RECORDS = details.Count + 2
                };

            //set header fields
            var header = new ExportHeader();

            //ensure resets in export details
            var rowIndex = 3;
            foreach (var item in details)
            {
                item.ROW_INDEX = rowIndex;
                rowIndex++;
            }

            var eSummary = new FileHelperEngine<ExportSummary>();
            var eHeaders = new FileHelperEngine<ExportHeader>();
            var eDetails = new FileHelperEngine<ExportDetails>();

            using (var memStream = new MemoryStream())
            {
                using (var stream = new StreamWriter(memStream))
                {
                    stream.Write(eSummary.WriteString(new[] { summary }));
                    stream.Write(eHeaders.WriteString(new[] { header }));
                    stream.Write(eDetails.WriteString(details));
                    stream.Flush();
                }

                return memStream.ToArray();
            }
        }

        public PalletPlus.Services.Suppliers.Models.SupplierInvoice ImportInvoice(string fileName, byte[] fileBytes)
        {
            var fileNameHeader = fileName.Split('_')[0];

            switch (fileNameHeader)
            {
                case "CHEPMate":
                    return MapProcessInvoice(fileBytes);
                case "AU":
                    throw new Exception("This invoice format is not currently supported.");
                default:
                    throw new Exception("Are you sure it is an invoice file?");
            }
        }

        SupplierInvoice MapProcessInvoice(byte[] fileBytes)
        {
            var invoice = ImportInvoiceFormat1(fileBytes);

            var supplierInvoice = new SupplierInvoice
                {
                    InvoiceDate = invoice.Header.INVOICEDATE,
                    InvoiceNumber = invoice.Header.INVOICENO,
                    Details = invoice.Details.Select(MapInvoiceDetails).ToArray()
                };

            return supplierInvoice;
        }

        SupplierInvoice.InvoiceDetail MapInvoiceDetails(InvoiceFormat1Detail detail)
        {
            var result = new SupplierInvoice.InvoiceDetail();

            result.DocketNumber = detail.PCMSDOCKET;
            result.EquipmentCode = detail.DEEQUIPCODE;
            result.Qty = detail.QUANTITY.ToInt();
            result.DaysHire = detail.DAYSHIRE.ToInt();
            result.AccountNumber = detail.CUSTCODE;
            result.PartnerAccountNumber = detail.OTHERPARTY;
            result.SupplierRef = detail.CHEPREF;
            result.ConsignmentRef = detail.CONNOTE;
            result.MovementDate = detail.DISPATCHDATE.GetValueOrDefault();
            result.EffectiveDate = detail.EFFECTDATE.GetValueOrDefault();
            result.DehireDate = detail.DEDATE.GetValueOrDefault();

            switch (detail.TRANSCODE)
            {
                case TransCode.TF:
                    result.TransactionCode = "Sent";
                    result.AccountRef = detail.REF1;
                    result.PartnerRef = detail.REF2;
                    break;

                case TransCode.TN:
                    result.TransactionCode = "Received";
                    result.AccountRef = detail.REF2;
                    result.PartnerRef = detail.REF1;
                    break;

                case TransCode.IS:
                    result.TransactionCode = "Issue";
                    result.AccountRef = detail.REF2;
                    result.PartnerRef = detail.REF1;
                    break;

                case TransCode.DE:
                    result.TransactionCode = "Return";
                    result.AccountRef = detail.REF2;
                    result.PartnerRef = detail.REF1;
                    break;

                default:
                    result.TransactionCode = "Unknown";
                    result.AccountRef = detail.REF1;
                    result.PartnerRef = detail.REF2;
                    break;
            }

            return result;
        }


        /// <summary>
        /// This is an import function for invoice format 1 (eg. ChepMate_xxxx)
        /// </summary>
        /// <param name="fileBytes"></param>
        /// <returns>Invoice record.</returns>
        public InvoiceFormat1 ImportInvoiceFormat1(byte[] fileBytes)
        {
            var eHeader = new FileHelperEngine<InvoiceFormat1Header>();
            var eDetail = new FileHelperEngine<InvoiceFormat1Detail>();

            using (var stream = new MemoryStream(fileBytes))
            using (TextReader textReader = new StreamReader(stream))
            {
                var hArray = eHeader.ReadStream(textReader);
                stream.Position = 0;
                var dArray = eDetail.ReadStream(textReader);

                var header = hArray.ToList();
                var details = dArray.ToList();

                var finalDetails =
                    details.Where(col =>
                                  !string.IsNullOrEmpty(col.PCMSDOCKET) &&
                                  col.TRANSCODE != TransCode.FE &&
                                  col.TRANSCODE != TransCode.TE)
                           .ToList();

                var summaryDetails = details.Where(col => string.IsNullOrEmpty(col.PCMSDOCKET)).ToList();

                var invoice = new InvoiceFormat1();
                invoice.Header = header.Take(1).SingleOrDefault();
                invoice.DetailsSummary = summaryDetails;
                invoice.Details = finalDetails;

                return invoice;
            }
        }

        /// <summary>
        /// This is an import function for invoice format 2 (eg. AU_ZE9D_xxx)
        /// </summary>
        public InvoiceFormat2 ImportInvoiceFormat2(byte[] fileBytes)
        {
            var eHeader = new FileHelperEngine<InvoiceFormat2Header>();
            var eDetail = new FileHelperEngine<InvoiceFormat2Detail>();

            using (var stream = new MemoryStream(fileBytes))
            using (var textReader = new StreamReader(stream))
            {
                var hArray = eHeader.ReadStream(textReader);
                stream.Position = 0;
                var dArray = eDetail.ReadStream(textReader);

                var header = hArray.ToList();
                var details = dArray.ToList();

                var invoice = new InvoiceFormat2();
                invoice.Header = header.Take(1).SingleOrDefault();
                invoice.Details = details;

                return invoice;
            }
        }

        /// <summary>
        /// This is an import function for invoice format 3.
        /// </summary>
        public InvoiceFormat3 ImportInvoiceFormat3(byte[] fileBytes)
        {
            var eDetail = new FileHelperEngine<InvoiceFormat3Detail>();

            using (var stream = new MemoryStream(fileBytes))
            using (var t = new StreamReader(stream))
            {
                var dArray = eDetail.ReadStream(t);

                var details = dArray.ToList();

                var finalDetails = details.Where(col => !string.IsNullOrEmpty(col.DI_DOCKET_NO)).ToList();

                var summaryDetails = details.Where(col => string.IsNullOrEmpty(col.DI_DOCKET_NO)).ToList();

                var invoice = new InvoiceFormat3();
                invoice.Header = new InvoiceFormat3Header();
                invoice.Details = finalDetails;
                invoice.SummaryDetails = summaryDetails;

                return invoice;
            }
        }
    }
}
