﻿using System;
using System.Collections.Generic;
using AutoMapper;
using PalletPlus.Data;
using PalletPlus.Services.Suppliers.Chep.Models;
using PalletPlus.Services.Suppliers.Chep.Models.ImportInvoice.InvoiceFormat1;
using PalletPlus.Services.Suppliers.Chep.Models.ImportInvoice.InvoiceFormat2;

namespace PalletPlus.Services.Suppliers.Chep
{
    public class CHEPInvoiceProfile : Profile
    {
        public CHEPInvoiceProfile()
        {
            Mapper.CreateMap<InvoiceFormat1Header, InvoiceHeader>();
            Mapper.CreateMap<InvoiceFormat1Detail, InvoiceDetail>()
                .AfterMap((if1, id) =>
                {
                    switch (if1.TRANSCODE)
                    {
                        case TransCode.TN:
                            id.TRANSCODE = "Transfer In";
                            break;
                        case TransCode.TF:
                            id.TRANSCODE = "Transfer Out";
                            break;
                        case TransCode.IS:
                            id.TRANSCODE = "Issue";
                            break;
                        case TransCode.DE:
                            id.TRANSCODE = "Return";
                            break;
                        default:
                            id.TRANSCODE = TransCode.UnKnown.ToString();
                            break;
                    }
                });

            Mapper.CreateMap<InvoiceFormat1, InvoiceAttachment>()
                .AfterMap((invoiceFormat1, invoice) =>
                              {
                                  invoice.Header = Mapper.Map<InvoiceFormat1Header, InvoiceHeader>(invoiceFormat1.Header);
                                  invoice.Details = Mapper.Map<List<InvoiceFormat1Detail>, List<InvoiceDetail>>(invoiceFormat1.Details);
                                  invoice.DetailsSummary = Mapper.Map<List<InvoiceFormat1Detail>, List<InvoiceDetail>>(invoiceFormat1.DetailsSummary);
                              });
        }
    }
}