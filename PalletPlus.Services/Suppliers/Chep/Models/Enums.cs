namespace PalletPlus.Services.Suppliers.Chep.Models
{
    public enum ImportFormats
    {
        Format1,
        Format2,
        Format3
    }

    public enum ImportRecordType
    {
        MIM01,
        MID01,

        H1,
        H2,
        D1,
        D2,
        T1
    }

    public enum ExportRecordType
    {
        H,
        F,
        D,
        C
    }
    public enum TransCode
    {
        TX,
        CF,
        TF,
        RF,
        CN,
        TN,
        RN,
        TE,
        IS,
        DE,
        FE,
        UnKnown
    }
}