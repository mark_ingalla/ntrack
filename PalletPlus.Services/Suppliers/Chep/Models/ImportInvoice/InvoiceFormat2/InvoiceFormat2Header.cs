﻿using FileHelpers;

namespace PalletPlus.Services.Suppliers.Chep.Models.ImportInvoice.InvoiceFormat2
{
    [DelimitedRecord(",")]
    [ConditionalRecord(RecordCondition.IncludeIfBegins, "\"H1\"")]
    public class InvoiceFormat2Header
    {
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public ImportRecordType RECORDTYPE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CUSTNAME;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CUSTCODE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string INVOICECODE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string INVDATE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string INVOICENAME;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string INVOICEREF1;
    }
}
