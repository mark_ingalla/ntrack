﻿using System.Collections.Generic;

namespace PalletPlus.Services.Suppliers.Chep.Models.ImportInvoice.InvoiceFormat2
{
    public class InvoiceFormat2
    {
        public InvoiceFormat2Header Header { get; set; }
        public List<InvoiceFormat2Detail> DetailsSummary { get; set; }
        public List<InvoiceFormat2Detail> Details { get; set; }
    }
}
