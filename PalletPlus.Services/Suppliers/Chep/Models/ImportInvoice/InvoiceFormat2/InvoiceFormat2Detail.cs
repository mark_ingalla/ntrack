﻿using System;
using FileHelpers;

namespace PalletPlus.Services.Suppliers.Chep.Models.ImportInvoice.InvoiceFormat2
{
    [IgnoreFirst(2)]
    [DelimitedRecord(",")]
    [ConditionalRecord(RecordCondition.IncludeIfBegins, "\"D1\"")]
    public class InvoiceFormat2Detail
    {
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public ImportRecordType RECORDTYPE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CUSTCODE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string EQUIPMENTCODE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string DOCKETNUMBER;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")] public DateTime TRANSDATE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CONNOTE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CUSTREF;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string TRANSREF;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string TRADINGPARTNERNAME;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string TRANSTYPE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string DEPOT;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string QUANTITY;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string NOOFDAYS;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string EQUIPDAYS;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string DELAYDAYS;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string EQUIPDAYSOFFSET;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string DELAYEQUIPDAYS;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string AMMOUNT;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string COMMENT;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string ORIGINALDOCKET;
                                              public string T1DOCKET;
                                              public string CORRECTIONTEXT;
    }
}
