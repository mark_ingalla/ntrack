﻿using System.Collections.Generic;

namespace PalletPlus.Services.Suppliers.Chep.Models.ImportInvoice.InvoiceFormat3
{
    public class InvoiceFormat3
    {
        public InvoiceFormat3Header Header { get; set; }
        public List<InvoiceFormat3Detail> Details { get; set; }
        public List<InvoiceFormat3Detail> SummaryDetails { get; set; }
    }
}
