﻿using System;
using FileHelpers;

namespace PalletPlus.Services.Suppliers.Chep.Models.ImportInvoice.InvoiceFormat3
{
    [IgnoreFirst(1)]
    [IgnoreEmptyLines]
    [DelimitedRecord(",")]
    public class InvoiceFormat3Detail
    {
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted('"')]
        public string DI_TRANSACTION;

        [FieldTrim(TrimMode.Both)]
        [FieldQuoted('"')]
        [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")] 
        public DateTime DI_EFFECT_DATE;

        [FieldTrim(TrimMode.Both)]
        [FieldQuoted('"')]
        public string DI_DOCKET_NO;

        [FieldTrim(TrimMode.Both)]
        [FieldQuoted('"')]
        [FieldConverter(ConverterKind.Date, "dd/MM/yyyy")] 
        public DateTime DI_MOV_DATE;

        [FieldTrim(TrimMode.Both)]
        [FieldQuoted('"')]
        public string DI_VERSION;

        [FieldTrim(TrimMode.Both)]
        [FieldQuoted('"')]
        public string DI_REF1;
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted('"')]
        public string DI_REF2;
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted('"')]
        public string DI_REF3;
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted('"')]
        public string DI_EQUIP;
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted('"')]
        public string DI_QUANTITY;
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted('"')]
        public string DI_GLID;

        [FieldTrim(TrimMode.Both)]
        [FieldQuoted('"')]
        public string DI_INVOICE_DATE;

        [FieldTrim(TrimMode.Both)]
        [FieldQuoted('"')]
        public string DI_INVOICE_NO;
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted('"')]
        public string DI_TR_PARTNER_GLID;
        [FieldTrim(TrimMode.Both)]
        [FieldQuoted('"')]
        public string DI_TR_PARTNER_NAME;
    }
}
