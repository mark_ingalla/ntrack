﻿using FileHelpers;

namespace PalletPlus.Services.Suppliers.Chep.Models.ImportInvoice.InvoiceFormat3
{
    
    //[IgnoreFirst(2)]
    [IgnoreEmptyLines]
    [DelimitedRecord(",")]
    [ConditionalRecord(RecordCondition.IncludeIfBegins, "\"MIM01\"")]
    public class InvoiceFormat3Header
    {
        public string TRANSACTION     = "Transaction";
        public string EFFECT_DATE     = "Effective Date";
        public string DOCKET_NO       = "Docket No.";
        public string MOV_DATE        = "Movement Date";
        public string VERSION         = "Version";
        public string REF1            = "Reference 1";
        public string REF2            = "Reference 2";
        public string REF3            = "Reference 3";
        public string EQUIP           = "Equipment";
        public string QUANTITY        = "Qty";
        public string GLID            = "GLID";
        public string INVOICE_DATE    = "Invoice Date";
        public string INVOICE_NO      = "Invoice No.";
        public string TR_PARTNER_GLID = "Trading Partner GLID";
        public string TR_PARTNER_NAME = "Trading Partner Name";
    }
}
