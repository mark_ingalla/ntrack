﻿using System.Collections.Generic;

namespace PalletPlus.Services.Suppliers.Chep.Models.ImportInvoice.InvoiceFormat1
{
    public class InvoiceFormat1
    {
        public InvoiceFormat1Header Header { get; set; }
        public List<InvoiceFormat1Detail> DetailsSummary { get; set; }
        public List<InvoiceFormat1Detail> Details { get; set; }
    }
}
