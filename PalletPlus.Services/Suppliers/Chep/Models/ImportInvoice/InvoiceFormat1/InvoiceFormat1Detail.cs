﻿using System;
using FileHelpers;

namespace PalletPlus.Services.Suppliers.Chep.Models.ImportInvoice.InvoiceFormat1
{
    [IgnoreFirst(6)]
    [DelimitedRecord(",")]
    [ConditionalRecord(RecordCondition.IncludeIfBegins, "\"MID01\"")]
    public class InvoiceFormat1Detail
    {
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public ImportRecordType RECORDTYPE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CUSTCODE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string INVOICENO;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string PCMSDOCKET;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CHEPREF;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CONNOTE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string ITEM;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string DEPOT;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string EQUIPCODE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string SUNDRYCODE;
        [FieldConverter(typeof (TransCode_Converter))] [FieldQuoted(QuoteMode.AlwaysQuoted)] public TransCode TRANSCODE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string STATUS;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] [FieldConverter(ConverterKind.Date, "yyyy-MM-dd hh:mm:ss")] public DateTime? EFFECTDATE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] [FieldConverter(ConverterKind.Date, "yyyy-MM-dd hh:mm:ss")] public DateTime? TRANSDATE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string NARRATION;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string QUANTITY;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string VALUE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string DAYSHIRE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string TRACDAYS;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string OTHERDOCKET;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] [FieldConverter(ConverterKind.Date, "yyyy-MM-dd hh:mm:ss")] public DateTime? OTHERDATE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string INVOICECUST;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] [FieldConverter(ConverterKind.Date, "yyyy-MM-dd hh:mm:ss")] public DateTime? INVOICEDATE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string INVOICEBALANCE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string OTHERPARTY;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string REF1;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string REF2;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string REF3;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string REF4;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string REF5;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string DEEQUIPCODE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] [FieldConverter(ConverterKind.Date, "yyyy-MM-dd hh:mm:ss")] public DateTime? DEDATE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] [FieldConverter(ConverterKind.Date, "yyyy-MM-dd hh:mm:ss")] public DateTime? DISPATCHDATE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] [FieldConverter(ConverterKind.Date, "yyyy-MM-dd hh:mm:ss")] public DateTime? RECEIPTDATE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CUSTCODEGLID;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string SCPLANTCODE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string INVOICECUSTGLID;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string OTHERPARTYGLID;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string EQUIPMATNR;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string DEEQUIPMATNR;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CHEPCOMMENT1;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CHEPCOMMENT2;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CHEPCOMMENT3;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string INFORMER;
    }
}
