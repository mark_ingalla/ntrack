﻿using System;
using FileHelpers;

namespace PalletPlus.Services.Suppliers.Chep.Models.ImportInvoice.InvoiceFormat1
{
    
    [IgnoreFirst(2)]
    [DelimitedRecord(",")]
    [ConditionalRecord(RecordCondition.IncludeIfBegins, "\"MIM01\"")]
    public class InvoiceFormat1Header
    {
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public ImportRecordType RECORDTYPE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string INVOICENO;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CUSTCODE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string BATCHID;

        [FieldConverter(ConverterKind.Date, "yyyy-MM-dd hh:mm:ss")] 
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public DateTime INVOICEDATE;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string INVOICEYEAR;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string INVOICEPERIOD;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string INVOICETOTAL;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string DOLLARDRTOT;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string DOLLARCRTOT;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string QTYDRTOTAL;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string QTYCRTOTAL;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string STATUS;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string DEPOT;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string REF1DI;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string REF2;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string REF3;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string REF4;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string CUSTCODEGLID;
        [FieldQuoted(QuoteMode.AlwaysQuoted)] public string SCPLANTCODE;

    }
}
