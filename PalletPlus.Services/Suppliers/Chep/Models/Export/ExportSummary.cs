﻿using System;
using FileHelpers;

namespace PalletPlus.Services.Suppliers.Chep.Models.Export
{
    [ConditionalRecord(RecordCondition.IncludeIfBegins, "H")]
    [DelimitedRecord(",")]
    public class ExportSummary 
    {
        public ExportRecordType RECORD_TYPE;
        public int ROW_INDEX;
        public int TOTAL_RECORDS;
        [FieldConverter(ConverterKind.Date, "yyyyMMdd 00:00:00")]
        public DateTime DATE_CREATE;
        public string APP_NAME;
        public string APP_VERSION;
        public int ACCOUNT_NUMBER;
        public string CHEP_REGION;
    }
}
