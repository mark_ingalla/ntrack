﻿using System;
using FileHelpers;

namespace PalletPlus.Services.Suppliers.Chep.Models.Export
{
    [ConditionalRecord(RecordCondition.IncludeIfMatchRegex, "(D,)|(C,)")]
    [DelimitedRecord(",")]
    public class ExportDetails
    {
        public ExportRecordType RECORD_TYPE;
        public int ROW_INDEX;
        public string PCMS_DOCKET;

        [FieldConverter(ConverterKind.Date, "yyyyMMdd 00:00:00")] 
        public DateTime DI_DE_DATE;

        [FieldConverter(ConverterKind.Date, "yyyyMMdd 00:00:00")] 
        public DateTime DI_DISPATCH_DATE;

        [FieldConverter(ConverterKind.Date, "yyyyMMdd 00:00:00")] 
        public DateTime DI_EFFECT_DATE;

        [FieldConverter(ConverterKind.Date, "yyyyMMdd 00:00:00")] 
        public DateTime DI_TRANS_DATE;

        [FieldConverter(ConverterKind.Date, "yyyyMMdd 00:00:00")] 
        public DateTime DI_RECEIPT_DATE;

        public string DI_REF1;
        public string DI_REF2;
        public string DI_REF3;
        public string DI_REF4;
        public string PT_CUST_CODE;
        public string PT_DE_EQUIP;
        public int PT_QUANTITY;
        public string SENDER;
        public string RECEIVER;
        public TransCode TRANSCODE;
    }
}
