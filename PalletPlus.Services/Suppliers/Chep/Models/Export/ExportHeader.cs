﻿using FileHelpers;

namespace PalletPlus.Services.Suppliers.Chep.Models.Export
{
    [ConditionalRecord(RecordCondition.IncludeIfBegins, "F")]
    [DelimitedRecord(",")]
    public class ExportHeader 
    {
        public ExportRecordType RECORD_TYPE = ExportRecordType.F;
        public int ROW_INDEX =2;
        public string PCMS_DOCKET = "PCMS-DOCKET";
        public string DI_DE_DATE = "DI-DE-DATE";
        public string DI_DISPATCH_DATE = "DI-DISPATCH-DATE";
        public string DI_EFFECT_DATE = "DI-EFFECT-DATE";
        public string DI_TRANS_DATE = "DI-TRANS-DATE";
        public string DI_RECEIPT_DATE = "DI-RECEIPT-DATE";
        public string DI_REF1 = "DI-REF1";
        public string DI_REF2 = "DI-REF2";
        public string DI_REF3 = "DI-REF3";
        public string DI_REF4 = "DI-REF4";
        public string PT_CUST_CODE = "PT-CUST-CODE";
        public string PT_DE_EQUIP = "PT-DE-EQUIP";
        public string PT_QUANTITY = "PT-QUANTITY";
        public string SENDER = "SENDER";
        public string RECEIVER = "RECEIVER";
        public string TRANSCODE = "TRANSCODE";
    }
}
