﻿using System.Collections.Generic;

namespace PalletPlus.Services.Export.CHEP.Models
{
    public class ExportModel
    {
       
        public ExportSummary Summary { get; set; }
        public ExportHeader Header { get; set; }
        public List<ExportDetails> Details { get; set; }

       public ExportModel()
       {
           Summary = new ExportSummary();

           //setup the summary record
           Summary.APP_NAME = Settings.Current.AppName;
           Summary.APP_VERSION = Settings.Current.AppVersion;
           Summary.ROW_INDEX = 1;
           Summary.RECORD_TYPE = ExportRecordType.H;
           Summary.CHEP_REGION = Settings.Current.ChepRegion;

           //setup the header record
           Header = new ExportHeader();

           //setup blank details
           Details = new List<ExportDetails>();
       }
    }
}
