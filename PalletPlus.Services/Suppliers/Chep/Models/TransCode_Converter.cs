﻿using System;
using FileHelpers;

namespace PalletPlus.Services.Suppliers.Chep.Models
{
    public class TransCode_Converter:ConverterBase
    {
        public override object StringToField(string from)
        {
            return Enum.IsDefined(typeof (TransCode), from)
                       ? Enum.Parse(typeof (TransCode), from, true)
                       : TransCode.UnKnown;
        }
    }
}
