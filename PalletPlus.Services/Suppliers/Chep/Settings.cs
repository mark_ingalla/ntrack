﻿using System.Configuration;

namespace PalletPlus.Services.Suppliers.Chep
{
    public class Settings : ConfigurationSection
    {
        public static Settings Current
        {
            get
            {
                return (Settings)ConfigurationManager.GetSection("CHEP");
            }
        }
        
        [ConfigurationProperty("AppName")]
        public string AppName
        {
            get { return (string)this["AppName"]; }
            set { this["AppName"] = value; }
        }

        [ConfigurationProperty("AppVersion")]
        public string AppVersion
        {
            get { return (string)this["AppVersion"]; }
            set { this["AppVersion"] = value; }
        }

        [ConfigurationProperty("Region")]
        public string ChepRegion
        {
            get { return (string)this["Region"]; }
            set { this["Region"] = value; }
        }
    }
}
