﻿
using PalletPlus.Data;


namespace PalletPlus.Services.Suppliers
{
    public interface ISupplierIntegration
    {
        string Name { get; }
        byte[] Export(Movement[] movements);
        Models.SupplierInvoice ImportInvoice(string fileName, byte[] file);
    }
}
