﻿
using System;

namespace PalletPlus.Services.Suppliers.Models
{
    public class SupplierInvoice
    {
        public string InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public InvoiceDetail[] Details { get; set; }
        


        public class InvoiceDetail
        {
            public string TransactionCode { get; set; }
            public string DocketNumber { get; set; }
            public string EquipmentCode { get; set; }
            public int Qty { get; set; }
            public int DaysHire { get; set; }
            public string AccountNumber { get; set; }
            public string AccountRef { get; set; }
            public string PartnerAccountNumber { get; set; }
            public string PartnerRef { get; set; }
            public string SupplierRef { get; set; }
            public string ConsignmentRef { get; set; }
            public DateTime MovementDate { get; set; }
            public DateTime EffectiveDate { get; set; }
            public DateTime DehireDate { get; set; }
        }
    }
}
