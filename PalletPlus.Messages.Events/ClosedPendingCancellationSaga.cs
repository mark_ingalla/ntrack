using System;

namespace PalletPlus.Messages.Events
{
    public class ClosedPendingCancellationSaga
    {
        public Guid MovementId { get; set; }
        public Guid DeclinedCancellationKey { get; set; }

        public ClosedPendingCancellationSaga(Guid movementId, Guid declinedCancellationKey)
        {
            MovementId = movementId;
            DeclinedCancellationKey = declinedCancellationKey;
        }
    }
}