﻿using System;
using System.Collections.Generic;

namespace PalletPlus.Messages.Events.Stocktake
{
    public class StocktakeCorrectionApproved
    {
        public Guid SiteId { get; set; }
        public Guid StocktakeId { get; set; }
        public Guid CorrectionKey { get; set; }
        public Guid SupplierId { get; set; }
        public Guid ClosedBy { get; set; }
        public string AssetCode { get; set; }
        public int Quantity { get; set; }

        public StocktakeCorrectionApproved(Guid siteId, Guid stocktakeId, Guid correctionKey, Guid supplierId, Guid closedBy, string assetCode, int assetQuantity)
        {
            SiteId = siteId;
            StocktakeId = stocktakeId;
            CorrectionKey = correctionKey;
            SupplierId = supplierId;
            AssetCode = assetCode;
            Quantity = assetQuantity;
            ClosedBy = closedBy;
        }
    }
}