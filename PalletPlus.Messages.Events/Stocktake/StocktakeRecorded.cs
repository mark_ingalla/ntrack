using System;
using System.Collections.Generic;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Events.Stocktake
{
    public class StocktakeRecorded
    {
        public Guid StocktakeId { get; set; }
        public Guid SiteId { get; set; }
        public DateTime TakenOn { get; set; }
        public Guid SupplierId { get; set; }

        public StocktakeRecorded(Guid stocktakeId, Guid siteId, Guid supplierId, DateTime takenOn)
        {
            StocktakeId = stocktakeId;
            SiteId = siteId;
            SupplierId = supplierId;
            TakenOn = takenOn;
        }
    }
}