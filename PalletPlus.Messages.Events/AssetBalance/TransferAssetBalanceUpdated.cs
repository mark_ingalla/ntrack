using System;

namespace PalletPlus.Messages.Events.AssetBalance
{
    public class TransferAssetBalanceUpdated
    {
        public Guid SiteId { get; set; }
        public string AssetCode { get; set; }
        public int Quantity { get; set; }

        public TransferAssetBalanceUpdated(Guid siteId, string assetCode, int quantity)
        {
            SiteId = siteId;
            AssetCode = assetCode;
            Quantity = quantity;
        }
    }
}