﻿using System;
using System.Collections.Generic;

namespace PalletPlus.Messages.Events.Exchange
{
    public class ExchangeCreated
    {
        public Guid ExchangeId { get; set; }

        public Guid SiteId { get; set; }
        public Guid SupplierId { get; set; }
        public Guid PartnerId { get; set; }

        public string DocketNumber { get; set; }
        public Dictionary<string, int> EquipmentsIn { get; set; }
        public Dictionary<string, int> EquipmentsOut { get; set; }

        public DateTime EffectiveDate { get; set; }
        public string PartnerReference { get; set; }
        public string ConsignmentReference { get; set; }




        public ExchangeCreated(Guid exchangeId, Guid siteId, Guid supplierId, Guid partnerId, DateTime effectiveDate, string partnerReference, string consignmentReference
            , Dictionary<string, int> equipmentsIn, Dictionary<string, int> equipmentsOut, string docketNumber)
        {
            ExchangeId = exchangeId;
            SiteId = siteId;
            SupplierId = supplierId;
            PartnerId = partnerId;
            EquipmentsIn = equipmentsIn;
            EquipmentsOut = equipmentsOut;
            EffectiveDate = effectiveDate;
            PartnerReference = partnerReference;
            ConsignmentReference = consignmentReference;
            DocketNumber = docketNumber;



        }
    }
}
