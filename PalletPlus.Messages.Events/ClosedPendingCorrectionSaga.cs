using System;

namespace PalletPlus.Messages.Events
{
    public class ClosedPendingCorrectionSaga
    {
        public Guid MovementId { get; set; }
        public Guid DeclinedCorrectionKey { get; set; }

        public ClosedPendingCorrectionSaga(Guid movementId, Guid correctionKey)
        {
            MovementId = movementId;
            DeclinedCorrectionKey = correctionKey;
        }
    }
}