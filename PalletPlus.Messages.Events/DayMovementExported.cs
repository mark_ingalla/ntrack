﻿
using System;

namespace PalletPlus.Messages.Events
{
    public class DayMovementExported
    {
        public Guid SagaId { get; set; }
        public Guid SiteId { get; set; }
        public Guid SupplierId { get; set; }
        public DateTime MovementDate { get; set; }
        public Guid[] MovementIds { get; set; }
        public int TotalMovements { get; set; }



        public DayMovementExported(Guid sagaId, Guid siteId, Guid supplierId, DateTime movementDate, Guid[] movementIds)
        {
            SagaId = sagaId;
            SiteId = siteId;
            SupplierId = supplierId;
            MovementDate = movementDate;
            MovementIds = movementIds;
            TotalMovements = movementIds.Length;
        }
    }
}
