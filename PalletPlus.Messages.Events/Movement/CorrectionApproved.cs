using System;
using System.Collections.Generic;

namespace PalletPlus.Messages.Events.Movement
{
    public class CorrectionApproved
    {
        public Guid SiteId { get; set; }
        public Guid MovementId { get; set; }
        public Guid CorrectionKey { get; set; }
        public Guid SupplierId { get; set; }
        public Guid ClosedBy { get; set; }
        public string MovementType { get; set; }
        public Dictionary<string, int> AssetCodeQuantity { get; set; }
        public DateTime NewDate { get; set; }
        public DateTime NewEffectiveDate { get; set; }

        public CorrectionApproved(Guid siteId, Guid movementId, Guid correctionKey, Guid supplierId, Guid closedBy, string movementType, Dictionary<string, int> assetCodeQuantity,
            DateTime newDate, DateTime newEffectiveDate)
        {
            SiteId = siteId;
            MovementId = movementId;
            CorrectionKey = correctionKey;
            SupplierId = supplierId;
            AssetCodeQuantity = assetCodeQuantity;
            ClosedBy = closedBy;
            MovementType = movementType;
            NewDate = newDate;
            NewEffectiveDate = newEffectiveDate;
        }
    }
}