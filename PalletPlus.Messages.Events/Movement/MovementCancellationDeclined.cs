using System;

namespace PalletPlus.Messages.Events.Movement
{
    public class MovementCancellationDeclined
    {
        public Guid MovementId { get; set; }
        public Guid CancellationKey { get; set; }
        public Guid ClosedBy { get; set; }

        public MovementCancellationDeclined(Guid movementId, Guid cancellationKey, Guid closedBy)
        {
            MovementId = movementId;
            CancellationKey = cancellationKey;
            ClosedBy = closedBy;
        }
    }
}