﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PalletPlus.Messages.Events.Movement
{
    public class PTAResolved
    {
        public Guid MovemendId { get; set; }
        public DateTime EffectiveDateUTC { get; set; }
        public Dictionary<string, int> Equipments { get; set; }
        public string DocketNumber { get; set; }

        public PTAResolved(Guid movemendId, DateTime effectiveDateUTC, Dictionary<string, int> equipments, string docketNumber)
        {
            MovemendId = movemendId;
            EffectiveDateUTC = effectiveDateUTC;
            Equipments = equipments;
            DocketNumber = docketNumber;
        }
    }
}
