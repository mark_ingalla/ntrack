﻿
using System;

namespace PalletPlus.Messages.Events.Movement
{
    public class MovementExported
    {
        public Guid MovementId { get; set; }

        public MovementExported(Guid movementId)
        {
            MovementId = movementId;
        }
    }
}
