using System;
using System.Collections.Generic;

namespace PalletPlus.Messages.Events.Movement
{
    public class MovementCreated
    {
        public Guid MovementId { get; set; }
        public Guid SiteId { get; set; }
        public Guid DestinationId { get; set; }
        public Guid SupplierId { get; set; }
        /// <summary>
        /// Supplier Location Id to support Issue/Returns from Sites
        /// </summary>
        public Guid? LocationId { get; set; }

        public string DestinationKind { get; set; }
        public string MovementType { get; set; }
        public string Direction { get; set; }
        public DateTime MovementDate { get; set; }
        public DateTime EffectiveDate { get; set; }
        public Dictionary<string, int> Equipments { get; set; }


        public string DocketNumber { get; set; }
        public string MovementReference { get; set; }
        public string DestinationReference { get; set; }
        public string ConsignmentNote { get; set; }
        public bool IsPTA { get; set; } //is pending pta movement
        public bool IsPending { get; set; }

        public MovementCreated(Guid movementId, Guid siteId, Guid destinationId, Guid supplierId, Guid? locationId,
            string destinationKind, string movementType, string direction, DateTime movementDate, DateTime effectiveDate,
            Dictionary<string, int> equipments, string docketNumber, string movementReference, string destinationReference,
            string consignmentNote, bool isPTA, bool isNotComplete)
        {
            MovementId = movementId;
            SiteId = siteId;
            DestinationId = destinationId;
            SupplierId = supplierId;
            LocationId = locationId;
            DestinationKind = destinationKind;
            MovementType = movementType;
            Direction = direction;
            MovementDate = movementDate;
            EffectiveDate = effectiveDate;
            Equipments = equipments;
            DocketNumber = docketNumber;
            MovementReference = movementReference;
            DestinationReference = destinationReference;
            ConsignmentNote = consignmentNote;
            IsPTA = isPTA;
            IsPending = isNotComplete;
        }
    }
}