using System;
using nTrack.Core.Messages;

namespace PalletPlus.Messages.Events.Movement
{
    public class QuantityCorrectionWithdrawn : MessageBase
    {
        public Guid MovementId { get; set; }
        public Guid CorrectionKey { get; set; }

        public QuantityCorrectionWithdrawn(Guid id, Guid key)
        {
            MovementId = id;
            CorrectionKey = key;
        }
    }
}