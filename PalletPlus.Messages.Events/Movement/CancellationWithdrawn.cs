using System;

namespace PalletPlus.Messages.Events.Movement
{
    public class CancellationWithdrawn
    {
        public Guid MovementId { get; set; }
        public Guid CorrectionKey { get; set; }

        public CancellationWithdrawn(Guid movementId, Guid correctionKey)
        {
            MovementId = movementId;
            CorrectionKey = correctionKey;
        }
    }
}