﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PalletPlus.Messages.Events.Movement
{
    public class MovementCompleted
    {
        public Guid MovementId { get; set; }
        public DateTime EffectiveDateUTC { get; set; }
        public string DocketNumber { get; set; }

        public MovementCompleted(Guid movementId, DateTime effectiveDateUTC, string docketNumber)
        {
            MovementId = movementId;
            EffectiveDateUTC = effectiveDateUTC;
            DocketNumber = docketNumber;
        }
    }
}
