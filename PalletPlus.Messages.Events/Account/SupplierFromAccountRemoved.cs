using System;

namespace PalletPlus.Messages.Events.Account
{
    public class SupplierFromAccountRemoved
    {
        public Guid AccountId { get; set; }
        public Guid SupplierId { get; set; }

        public SupplierFromAccountRemoved(Guid accountId, Guid supplierId)
        {
            AccountId = accountId;
            SupplierId = supplierId;
        }
    }
}