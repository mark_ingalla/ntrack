﻿
using System;

namespace PalletPlus.Messages.Events.Account
{
    public class DashboardEquipmentColorSet
    {
        public Guid AccountId { get; set; }
        public Guid ColorKey { get; set; }
        public string EquipmentCode { get; set; }
        public string EquipmentName { get; set; }
        public int R { get; set; }
        public int G { get; set; }
        public int B { get; set; }

        public DashboardEquipmentColorSet(Guid accountId, Guid colorKey, string equipmentCode, string equipmentName, int r, int g, int b)
        {
            AccountId = accountId;
            ColorKey = colorKey;
            EquipmentCode = equipmentCode;
            EquipmentName = equipmentName;
            R = r;
            G = g;
            B = b;
        }
    }
}
