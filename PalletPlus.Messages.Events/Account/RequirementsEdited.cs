﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PalletPlus.Messages.Events.Account
{
    public class RequirementsEdited
    {
        public Guid AccountId { get; set; }
        public HashSet<string> RequiredFields { get; set; }

        public RequirementsEdited(Guid accountId, HashSet<string> requiredFields)
        {
            AccountId = accountId;
            RequiredFields = requiredFields;
        }
    }
}
