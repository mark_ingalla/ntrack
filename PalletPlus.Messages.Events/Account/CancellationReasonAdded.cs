﻿using System;

namespace PalletPlus.Messages.Events.Account
{
    public class CancellationReasonAdded
    {
        public Guid AccountId { get; set; }
        public Guid ReasonKey { get; set; }
        public string Reason { get; set; }

        public CancellationReasonAdded(Guid accountId, Guid reasonKey, string reason)
        {
            AccountId = accountId;
            ReasonKey = reasonKey;
            Reason = reason;
        }
    }
}
