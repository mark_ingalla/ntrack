﻿using System;

namespace PalletPlus.Messages.Events.Account
{
    public class CancellationReasonRemoved
    {
        public Guid AccountId { get; set; }
        public Guid ReasonKey { get; set; }

        public CancellationReasonRemoved(Guid accountId, Guid reasonKey)
        {
            AccountId = accountId;
            ReasonKey = reasonKey;
        }
    }
}
