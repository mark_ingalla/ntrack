﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PalletPlus.Messages.Events.Account
{
    public class ExportSettingsChanged
    {
        public Guid AccountId { get; set; }
        public bool ExportMovementIn { get; set; }
        public bool ExportMovementOut { get; set; }

        public ExportSettingsChanged(Guid accountId, bool exportMovementIn, bool exportMovementOut)
        {
            AccountId = accountId;
            ExportMovementIn = exportMovementIn;
            ExportMovementOut = exportMovementOut;
        }
    }
}
