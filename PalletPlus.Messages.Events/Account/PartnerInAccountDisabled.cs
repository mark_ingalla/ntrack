using System;

namespace PalletPlus.Messages.Events.Account
{
    public class PartnerInAccountDisabled
    {
        public Guid AccountId { get; set; }
        public Guid PartnerId { get; set; }

        public PartnerInAccountDisabled(Guid accountId, Guid partnerId)
        {
            AccountId = accountId;
            PartnerId = partnerId;
        }
    }
}