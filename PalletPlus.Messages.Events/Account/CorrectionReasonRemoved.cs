﻿using System;

namespace PalletPlus.Messages.Events.Account
{
    public class CorrectionReasonRemoved
    {
        public Guid AccountId { get; set; }
        public Guid ReasonKey { get; set; }

        public CorrectionReasonRemoved(Guid accountId, Guid reasonKey)
        {
            AccountId = accountId;
            ReasonKey = reasonKey;
        }
    }
}
