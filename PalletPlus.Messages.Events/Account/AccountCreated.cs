﻿using System;

namespace PalletPlus.Messages.Events.Account
{
    public class AccountCreated
    {
        public Guid AccountId { get; set; }
        public string CompanyName { get; set; }
        
        public AccountCreated(Guid accountId, string companyName)
        {
            AccountId = accountId;
            CompanyName = companyName;
        }
    }
}
