﻿using System;

namespace PalletPlus.Messages.Events.Account
{
    public class CorrectionReasonAdded
    {
        public Guid AccountId { get; set; }
        public Guid ReasonKey { get; set; }
        public string Reason { get; set; }

        public CorrectionReasonAdded(Guid accountId, Guid reasonKey, string reason)
        {
            AccountId = accountId;
            ReasonKey = reasonKey;
            Reason = reason;
        }
    }
}
