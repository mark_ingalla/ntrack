using System;

namespace PalletPlus.Messages.Events.Supplier
{
    public class EquipmentSupplierAdded
    {
        public Guid SupplierId { get; set; }
        public string EquipmentCode { get; set; }
        public string EquipmentName { get; set; }

        public EquipmentSupplierAdded(Guid supplierId, string equipmentCode, string equipmentName)
        {
            SupplierId = supplierId;
            EquipmentCode = equipmentCode;
            EquipmentName = equipmentName;
        }
    }
}