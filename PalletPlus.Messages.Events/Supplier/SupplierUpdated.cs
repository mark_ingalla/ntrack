using System;

namespace PalletPlus.Messages.Events.Supplier
{
    [Obsolete]
    public class SupplierUpdated
    {
        public Guid SupplierId { get; set; }
        public string Name { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }

        public string AccountNumber { get; set; }
        public string ProviderName { get; set; }
        public bool IsPTAEnabled { get; set; }

        public SupplierUpdated(Guid supplierId, string name, string accountNumber, string addressLine1, string addressLine2,
            string suburb, string state, string postcode, string country, string contactName, string contactEmail, string exportProviderName, bool isPTAEnabled)
        {
            SupplierId = supplierId;
            Name = name;
            AccountNumber = accountNumber;
            AddressLine1 = addressLine1;
            AddressLine2 = addressLine2;
            Suburb = suburb;
            State = state;
            Postcode = postcode;
            Country = country;
            ContactEmail = contactEmail;
            ContactName = contactName;
            ProviderName = exportProviderName;
            IsPTAEnabled = isPTAEnabled;
        }
    }

    public class SupplierUpdated_V2
    {
        public Guid SupplierId { get; set; }
        public string Name { get; set; }
        public string ProviderName { get; set; }
        public bool IsPTAEnabled { get; set; }
        public string EmailAddress { get; set; }

        public SupplierUpdated_V2(Guid supplierId, string name, string providerName, bool isPTAEnabled, string emailAddress)
        {
            SupplierId = supplierId;
            Name = name;
            ProviderName = providerName;
            IsPTAEnabled = isPTAEnabled;
            EmailAddress = emailAddress;
        }
    }
}