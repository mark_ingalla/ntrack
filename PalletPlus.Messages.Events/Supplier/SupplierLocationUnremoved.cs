using System;

namespace PalletPlus.Messages.Events.Supplier
{
    public class SupplierLocationUnremoved
    {
        public Guid SupplierId { get; set; }
        public Guid LocationKey { get; set; }
        public string AccountNumber { get; set; }

        public SupplierLocationUnremoved(Guid supplierId, Guid locationKey, string accountNumber)
        {
            SupplierId = supplierId;
            LocationKey = locationKey;
            AccountNumber = accountNumber;
        }
    }
}