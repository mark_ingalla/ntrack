using System;

namespace PalletPlus.Messages.Events.Supplier
{
    public class EquipmentSupplierDiscontinued
    {
        public Guid SupplierId { get; set; }
        public string EquipmentCode { get; set; }

        public EquipmentSupplierDiscontinued(Guid supplierId, string equipmentCode)
        {
            SupplierId = supplierId;
            EquipmentCode = equipmentCode;
        }
    }
}