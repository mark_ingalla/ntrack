using System;

namespace PalletPlus.Messages.Events.Supplier
{
    public class SupplierLocationRemoved
    {
        public Guid SupplierId { get; set; }
        public Guid LocationKey { get; set; }

        public SupplierLocationRemoved(Guid supplierId, Guid locationKey)
        {
            SupplierId = supplierId;
            LocationKey = locationKey;
        }
    }
}