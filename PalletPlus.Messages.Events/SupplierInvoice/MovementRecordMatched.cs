using System;

namespace PalletPlus.Messages.Events.SupplierInvoice
{
    public class MovementRecordMatched
    {
        public DateTime MatchedOn { get; set; }
        public Guid MatchedBy { get; set; }

        public string MovementId { get; set; }
        public string MovementEquipmentCode { get; set; }

        public Guid InvoiceId { get; set; }
        public Guid DetailKey { get; set; }
        public DateTime MatchedMovementDate { get; set; }
        public string MatchedDocketNumber { get; set; }
        public string MatchedDestinationId { get; set; }
        public string MatchedMovementType { get; set; }
        public string MatchedMovementDirection { get; set; }
        public string MatchedEquipmentCode { get; set; }
        public int MatchedQuantity { get; set; }

        public MovementRecordMatched(DateTime matchedOn, Guid matchedBy,
            string movementId, string movementEquipmentCode,
            Guid invoiceId, Guid detailKey, DateTime matchedMovementDate, string matchedDocketNumber, string matchedDestinationId,
            string matchedMovementType, string matchedMovementDirection, string matchedEquipmentCode, int matchedQuantity)
        {
            MatchedOn = matchedOn;
            MatchedBy = matchedBy;

            MovementId = movementId;
            MovementEquipmentCode = movementEquipmentCode;

            InvoiceId = invoiceId;
            DetailKey = detailKey;
            MatchedMovementDate = matchedMovementDate;
            MatchedDocketNumber = matchedDocketNumber;
            MatchedDestinationId = matchedDestinationId;
            MatchedMovementType = matchedMovementType;
            MatchedMovementDirection = matchedMovementDirection;
            MatchedEquipmentCode = matchedEquipmentCode;
            MatchedQuantity = matchedQuantity;
        }
    }
}