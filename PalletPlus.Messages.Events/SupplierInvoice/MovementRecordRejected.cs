using System;

namespace PalletPlus.Messages.Events.SupplierInvoice
{
    public class MovementRecordRejected
    {
        public DateTime RejectedOn { get; set; }
        public Guid RejectedBy { get; set; }

        public Guid InvoiceId { get; set; }
        public Guid DetailKey { get; set; }

        public MovementRecordRejected(DateTime rejectedOn, Guid rejectedBy, Guid invoiceId, Guid detailKey)
        {
            RejectedOn = rejectedOn;
            RejectedBy = rejectedBy;

            InvoiceId = invoiceId;
            DetailKey = detailKey;
        }
    }
}