﻿using System;

namespace PalletPlus.Messages.Events.SupplierInvoice
{
    public class DetailAdded
    {
        public Guid InvoiceId { get; set; }
        public string TransactionCode { get; set; }
        public string DocketNumber { get; set; }
        public string EquipmentCode { get; set; }
        public int Qty { get; set; }
        public int DaysHire { get; set; }
        public string AccountNumber { get; set; }
        public string AccountRef { get; set; }
        public string PartnerAccountNumber { get; set; }
        public string PartnerRef { get; set; }
        public string SupplierRef { get; set; }
        public string ConsignmentRef { get; set; }
        public DateTime MovementDate { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime DehireDate { get; set; }

        public DetailAdded(Guid invoiceId, string transactionCode, string docketNumber, string equipmentCode, int qty, int daysHire,
            string accountNumber, string accountRef, string partnerAccountNumber, string partnerRef, string supplierRef, string consignmentRef,
            DateTime movementDate, DateTime effectiveDate, DateTime dehireDate)
        {
            InvoiceId = invoiceId;
            TransactionCode = transactionCode;
            DocketNumber = docketNumber;
            EquipmentCode = equipmentCode;
            Qty = qty;
            DaysHire = daysHire;
            AccountNumber = accountNumber;
            AccountRef = accountRef;
            PartnerAccountNumber = partnerAccountNumber;
            PartnerRef = partnerRef;
            SupplierRef = supplierRef;
            ConsignmentRef = consignmentRef;
            MovementDate = movementDate;
            EffectiveDate = effectiveDate;
            DehireDate = dehireDate;
        }
    }
}