using System;
using System.Collections.Generic;

namespace PalletPlus.Messages.Events.SupplierInvoice
{
    public class MovementsReconciled
    {
        public Guid InvoiceId { get; set; }
        public Dictionary<string, List<string>> MovementDetailsToReconcile { get; set; }
        public Dictionary<Guid, string> InvoiceDetailsToReconcile { get; set; }

        public MovementsReconciled(Guid invoiceId, Dictionary<Guid, string> invoiceDetailsToReconcile,
                                   Dictionary<string, List<string>> movementDetailsToReconcile)
        {
            InvoiceId = invoiceId;
            InvoiceDetailsToReconcile = invoiceDetailsToReconcile;
            MovementDetailsToReconcile = movementDetailsToReconcile;
        }
    }
}