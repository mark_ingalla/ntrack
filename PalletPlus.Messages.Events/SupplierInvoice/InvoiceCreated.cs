using System;

namespace PalletPlus.Messages.Events.SupplierInvoice
{
    public class InvoiceCreated
    {
        public Guid InvoiceId { get; set; }
        public Guid SupplierId { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }

        public InvoiceCreated(Guid invoiceId, Guid supplierId, string invoiceNumber, DateTime invoiceDate)
        {
            InvoiceId = invoiceId;
            SupplierId = supplierId;
            InvoiceNumber = invoiceNumber;
            InvoiceDate = invoiceDate;
        }
    }
}