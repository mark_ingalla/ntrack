using System;

namespace PalletPlus.Messages.Events.SupplierInvoice
{
    public class TransactionCreated
    {
        public DateTime CreatedOn { get; set; }
        public Guid CreatedBy { get; set; }

        public Guid InvoiceId { get; set; }
        public Guid DetailKey { get; set; }

        public Guid MovementId { get; set; }

        public TransactionCreated(DateTime createdOn, Guid createdBy,
            Guid invoiceId, Guid detailKey, Guid movementId)
        {
            CreatedOn = createdOn;
            CreatedBy = createdBy;

            InvoiceId = invoiceId;
            DetailKey = detailKey;

            MovementId = movementId;
        }
    }
}