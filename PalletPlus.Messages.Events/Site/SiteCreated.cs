using System;

namespace PalletPlus.Messages.Events.Site
{
    public class SiteCreated
    {
        public Guid AccountId { get; set; }
        public Guid SiteId { get; set; }
        public string Name { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }

        public SiteCreated(Guid accountId, Guid siteId, string name,
            string addressLine1, string addressLine2, string suburb, string state, string postcode, string country)
        {
            AccountId = accountId;
            Name = name;
            SiteId = siteId;
            AddressLine1 = addressLine1;
            AddressLine2 = addressLine2;
            Suburb = suburb;
            State = state;
            Postcode = postcode;
            Country = country;
        }
    }
}