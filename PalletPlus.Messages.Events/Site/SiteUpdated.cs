using System;

namespace PalletPlus.Messages.Events.Site
{
    public class SiteUpdated
    {
        public Guid SiteId { get; set; }
        public string SiteName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }

        public SiteUpdated(Guid siteId, string siteName,
            string addressLine1, string addressLine2, string suburb, string state, string postcode, string country)
        {
            SiteName = siteName;
            SiteId = siteId;
            AddressLine1 = addressLine1;
            AddressLine2 = addressLine2;
            Suburb = suburb;
            State = state;
            Postcode = postcode;
            Country = country;
        }
    }
}