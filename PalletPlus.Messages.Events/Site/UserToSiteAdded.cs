using System;

namespace PalletPlus.Messages.Events.Site
{
    public class UserToSiteAdded
    {
        public Guid SiteId { get; set; }
        public Guid UserId { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }

        public UserToSiteAdded(Guid siteId, Guid userId, string email, string fullName)
        {
            SiteId = siteId;
            UserId = userId;
            Email = email;
            FullName = fullName;
        }
    }
}