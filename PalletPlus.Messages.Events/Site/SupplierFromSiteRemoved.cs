using System;

namespace PalletPlus.Messages.Events.Site
{
    public class SupplierFromSiteRemoved
    {
        public Guid SiteId { get; set; }
        public Guid SupplierId { get; set; }

        public SupplierFromSiteRemoved(Guid siteId, Guid supplierId)
        {
            SiteId = siteId;
            SupplierId = supplierId;
        }
    }
}