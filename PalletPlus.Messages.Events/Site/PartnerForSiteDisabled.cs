using System;

namespace PalletPlus.Messages.Events.Site
{
    public class PartnerForSiteDisabled
    {
        public Guid SiteId { get; set; }
        public Guid PartnerId { get; set; }

        public PartnerForSiteDisabled(Guid siteId, Guid partnerId)
        {
            SiteId = siteId;
            PartnerId = partnerId;
        }
    }
}