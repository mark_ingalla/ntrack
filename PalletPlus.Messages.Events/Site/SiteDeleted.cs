using System;

namespace PalletPlus.Messages.Events.Site
{
    public class SiteDeleted
    {
        public Guid SiteId { get; set; }

        public SiteDeleted(Guid siteId)
        {
            SiteId = siteId;
        }
    }
}