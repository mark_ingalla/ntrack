﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PalletPlus.Messages.Events.Site
{
    public class PTAInfoUpdated
    {
        public Guid SiteId { get; set; }
        public bool IsEnabled { get; set; }
        public string Suffix { get; set; }
        public int SequenceNumber { get; set; }

        public PTAInfoUpdated(Guid siteId, bool isEnabled, string suffix, int sequenceNumber)
        {
            SiteId = siteId;
            IsEnabled = isEnabled;
            Suffix = suffix;
            SequenceNumber = sequenceNumber;
        }
    }
}
