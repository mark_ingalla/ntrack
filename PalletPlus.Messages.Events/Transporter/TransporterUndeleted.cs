using System;

namespace PalletPlus.Messages.Events.Transporter
{
    public class TransporterUndeleted
    {
        public Guid TransporterId { get; set; }

        public TransporterUndeleted(Guid transporterId)
        {
            TransporterId = transporterId;
        }
    }
}