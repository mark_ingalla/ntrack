using System;

namespace PalletPlus.Messages.Events.Partner
{
    public class SupplierToPartnerAdded
    {
        public Guid ParnterId { get; set; }
        public Guid SupplierId { get; set; }
        public string AccountNumber { get; set; }
        

        public SupplierToPartnerAdded(Guid parnterId, Guid supplierId, string accountNumber)
        {
            ParnterId = parnterId;
            SupplierId = supplierId;
            AccountNumber = accountNumber;
            
        }
    }
}