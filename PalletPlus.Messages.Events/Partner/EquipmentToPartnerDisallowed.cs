using System;

namespace PalletPlus.Messages.Events.Partner
{
    public class EquipmentToPartnerDisallowed
    {
        public Guid PartnerId { get; set; }
        public string EquipmentCode { get; set; }
        public Guid SupplierId { get; set; }

        public EquipmentToPartnerDisallowed(Guid partnerId, Guid supplierId, string equipmentCode)
        {
            PartnerId = partnerId;
            EquipmentCode = equipmentCode;
            SupplierId = supplierId;
        }
    }
}