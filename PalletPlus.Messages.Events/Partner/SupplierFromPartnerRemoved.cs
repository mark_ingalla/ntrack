using System;

namespace PalletPlus.Messages.Events.Partner
{
    public class SupplierFromPartnerRemoved
    {
        public Guid PartnerId { get; set; }
        public Guid SupplierId { get; set; }

        public SupplierFromPartnerRemoved(Guid partnerId, Guid supplierId)
        {
            PartnerId = partnerId;
            SupplierId = supplierId;
        }
    }
}