using System;

namespace PalletPlus.Messages.Events.Partner
{
    public class PartnerDeleted
    {
        public Guid PartnerId { get; set; }

        public PartnerDeleted(Guid partnerId)
        {
            PartnerId = partnerId;
        }
    }
}