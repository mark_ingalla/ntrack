﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PalletPlus.Messages.Events.Partner
{
    public class EnableExchangeUpdated
    {
        public Guid SupplierId { get; set; }
        public Guid PartnerId { get; set; }
        public bool EnabledExchange { get; set; }

        public EnableExchangeUpdated(Guid _supplierId , Guid _partnerId,bool enableExchange)
        {
            SupplierId = _supplierId;
            PartnerId = _partnerId;
            EnabledExchange = enableExchange;
        }
    }
}
