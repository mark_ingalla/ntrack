using System;
using System.Collections.Generic;

namespace PalletPlus.Messages.Events.Partner
{
    public class PartnerUpdated
    {
        public Guid PartnerId { get; set; }
        public string Name { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }

        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public bool IsPTAEnabled { get; set; }
        public List<string> Tags { get; set; }

        public PartnerUpdated(Guid partnerId, string name,
            string addressLine1, string addressLine2, string suburb, string state, string postCode, string country,
            string contactName, string contactEmail, bool isPTAEnabled,List<string> tags)
        {
            PartnerId = partnerId;
            Name = name;
            AddressLine1 = addressLine1;
            AddressLine2 = addressLine2;
            Suburb = suburb;
            State = state;
            Postcode = postCode;
            Country = country;
            ContactEmail = contactEmail;
            ContactName = contactName;
            IsPTAEnabled = isPTAEnabled;
            Tags = tags;
        }
    }
}