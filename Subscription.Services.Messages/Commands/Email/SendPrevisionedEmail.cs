﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Subscription.Services.Messages.Commands.Email
{
    public class SendPrevisionedEmail : EmailMessageBase
    {
        public string ProductName { get; set; }
        public string ProductUrl { get; set; }
    }
}
