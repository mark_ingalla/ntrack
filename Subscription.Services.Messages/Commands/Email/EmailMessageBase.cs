﻿using System.Collections.Generic;
using nTrack.Core.Messages;

namespace Subscription.Services.Messages.Commands.Email
{
    public abstract class EmailMessageBase : MessageBase
    {
        public Dictionary<string, string> ToEmails { get; set; }
    }
}
