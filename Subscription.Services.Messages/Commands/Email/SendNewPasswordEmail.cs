
namespace Subscription.Services.Messages.Commands.Email
{
    public class SendNewPasswordEmail : EmailMessageBase
    {
        public string NewPassword { get; set; }
    }
}