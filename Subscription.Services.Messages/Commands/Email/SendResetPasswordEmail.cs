
namespace Subscription.Services.Messages.Commands.Email
{
    public class SendResetPasswordEmail : EmailMessageBase
    {
        public string ResetPasswordUrl { get; set; }

    }
}