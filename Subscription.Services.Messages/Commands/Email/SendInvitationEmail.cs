﻿using System;

namespace Subscription.Services.Messages.Commands.Email
{
    public class SendInvitationEmail : EmailMessageBase
    {
        public Guid InvitationToken { get; set; }
    }
}
