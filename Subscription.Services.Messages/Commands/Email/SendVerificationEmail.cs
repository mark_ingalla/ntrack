﻿
namespace Subscription.Services.Messages.Commands.Email
{
    public class SendVerificationEmail : EmailMessageBase
    {
        public string VerificationUrl { get; set; }
        public string OfferName { get; set; }
        public string ProductName { get; set; }
    }
}
