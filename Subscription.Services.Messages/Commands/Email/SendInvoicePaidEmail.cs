﻿using System;

namespace Subscription.Services.Messages.Commands.Email
{
    public class SendInvoicePaidEmail : EmailMessageBase
    {
        public DateTime PaidOnUTC { get; set; }
    }
}
