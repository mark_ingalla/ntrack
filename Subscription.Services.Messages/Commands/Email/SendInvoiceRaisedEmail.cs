﻿using System;

namespace Subscription.Services.Messages.Commands.Email
{
    public class SendInvoiceRaisedEmail : EmailMessageBase
    {
        public DateTime InvoiceDateUTC { get; set; }

    }
}
