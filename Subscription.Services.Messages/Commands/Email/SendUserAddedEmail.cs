﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Subscription.Services.Messages.Commands.Email
{
    public class SendUserAddedEmail : EmailMessageBase
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string LoginLink { get; set; }
    }
}
