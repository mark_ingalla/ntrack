﻿using System;

namespace Subscription.Data
{
    public class UserInvitation
    {
        public Guid   InvitationToken { get; set; }
        public string Email           { get; set; }

        public Guid Id
        {
            get
            { return InvitationToken; }
            set
            { InvitationToken = value; }
        }
    }
}