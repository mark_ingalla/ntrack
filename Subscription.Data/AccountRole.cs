using System;
using System.Collections.Generic;

namespace Subscription.Data
{
    public class AccountRole 
    {
        public Guid Key { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string Name { get; set; }
        public Dictionary<Guid, List<string>> Permissions { get; set; }

        public AccountRole()
        {
            Permissions = new Dictionary<Guid, List<string>>();
        }
    }
}