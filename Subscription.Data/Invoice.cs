﻿using System;
using System.Collections.Generic;
using System.Linq;
using nTrack.Data;

namespace Subscription.Data
{
    public class Invoice : BaseModel
    {
        public string AccountId { get; set; }
        public int InvoiceNumber { get; set; }
        public DateTime IssuedOnUTC { get; set; }
        public string FileName { get; set; }

        public decimal Total { get { return Details.Sum(x => x.LineTotal); } }
        public bool IsPaid { get { return Details.Sum(x => x.LineTotal) - Payments.Sum(x => x.Amount) <= 0; } }

        public InvoiceDetail[] Details { get; set; }
        public List<InvoicePayment> Payments { get; set; }


        public Invoice()
        {
            Details = new InvoiceDetail[0];
            Payments = new List<InvoicePayment>();
        }
    }

    public class InvoicePayment
    {
        public DateTime PaidOnUTC { get; set; }
        public decimal Amount { get; set; }
    }
}
