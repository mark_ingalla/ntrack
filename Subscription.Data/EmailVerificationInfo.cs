﻿using System;

namespace Subscription.Data
{
    public class EmailVerificationInfo
    {
        public Guid UserID            { get; set; }
        public Guid VerificationToken { get; set; }
    }
}