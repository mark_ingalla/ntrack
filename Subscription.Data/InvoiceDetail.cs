﻿using System;

namespace Subscription.Data
{
    public class InvoiceDetail
    {
        public string ProductId { get; set; }
        public string ProductCode { get; set; }
        public Guid OfferKey { get; set; }
        public string LineDescription { get; set; }
        public decimal LineTotal { get; set; }
    }
}