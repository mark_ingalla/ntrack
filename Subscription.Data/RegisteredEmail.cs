﻿
namespace Subscription.Data
{
    public class RegisteredEmail
    {
        public string Email { get; set; }
    }
}