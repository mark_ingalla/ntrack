﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Subscription.Data
{
    public class ProductOffer
    {
        public readonly decimal BasePrice;

        public Guid Key { get; private set; }
        public string Name { get; set; }

        public List<ProductFeatureSummary> Features { get; set; }
        public decimal Price { get { return Features != null ? BasePrice + Features.Sum(x => x.Price) : BasePrice; } }
        public bool IsActive { get; set; }
        public bool IsLocked { get; set; }

        public ProductOffer(Guid key, decimal basePrice, string name)
        {
            BasePrice = basePrice;
            Key = key;
            Name = name;
            Features = new List<ProductFeatureSummary>();
        }
    }
}