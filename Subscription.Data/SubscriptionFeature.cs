using System;

namespace Subscription.Data
{
    public class SubscriptionFeature
    {
        public string Code { get; set; }
        public DateTime SubscribedOnUTC { get; set; }
        public DateTime? UnsubscribedOnUTC { get; set; }
        
        public bool IsDiscontinued { get; set; }
    }
}