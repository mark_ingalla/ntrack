﻿using System;
using System.Collections.Generic;
using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace Subscription.Data.Indices
{
    public class UserIndex : AbstractIndexCreationTask<Account, UserIndex.Result>
    {
        public class Result
        {
            public string AccountId { get; set; }
            public Guid UserKey { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
            public bool UserLocked { get; set; }
            public DateTime AccountUpdateOn { get; set; }
            public string UserFullName { get; set; }
            public string UserFirstName { get; set; }
            public string Role { get; set; }
            public List<Guid> ResetPasswordTokens { get; set; }

            public Guid AccountIdGuid { get { return new Guid(AccountId.Replace("accounts/",""));}} 
        }

        public UserIndex()
        {
            Map = accounts => from account in accounts
                              from user in account.Users
                              select new
                                  {
                                      AccountId = account.Id,
                                      Password = user.Password,
                                      Email = user.Email,
                                      UserKey = user.UserKey,
                                      UserLocked = user.IsLocked,
                                      AccountUpdateOn = account.UpdatedOn,
                                      UserFirstName = user.FirstName,
                                      UserFullName = user.FirstName + " " + user.LastName,
                                      Role = user.Role,
                                      ResetPasswordTokens = user.PendingResetPasswordTokens
                                  };

            Reduce = results => from r in results
                                group r by r.UserKey
                                    into gr
                                    from g in gr
                                    select new Result
                                    {
                                        AccountId = g.AccountId,
                                        UserKey = g.UserKey,
                                        Email = g.Email,
                                        Password = g.Password,
                                        UserLocked = g.UserLocked,
                                        AccountUpdateOn = g.AccountUpdateOn,
                                        UserFullName = g.UserFullName,
                                        UserFirstName = g.UserFirstName,
                                        Role = g.Role,
                                        ResetPasswordTokens = g.ResetPasswordTokens
                                    };
            

            Index(x => x.Email, FieldIndexing.NotAnalyzed);
            Index(x => x.Password, FieldIndexing.NotAnalyzed);

            StoreAllFields(FieldStorage.Yes);
        }
    }
}
