using System;
using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace Subscription.Data.Indices
{
    public class AccountUserIndex : AbstractIndexCreationTask<User>
    {
        //public class Result
        //{
        //    public Guid AccountId { get; set; }
        //    public string UserId { get; set; }
        //    public bool? IsAdmin { get; set; }
        //}

        public AccountUserIndex()
        {
            Map = users => from user in users
                              select new// Result
                                         {
                                             AccountId = user.AccountId,
                                             UserId = user.Id.ToString(),
                                             IsAdmin = user.IsAdmin
                                         };

            //Reduce = results => from result in results
            //                    group result by new { result.AccountId, result.UserId }
            //                        into gResult
            //                        select new// Result
            //                                   {
            //                                       AccountId = gResult.Key.AccountId,
            //                                       UserId = gResult.Key.UserId,
            //                                       IsAdmin = gResult.FirstOrDefault() != null ? gResult.FirstOrDefault().IsAdmin : false
            //                                       //IsAdmin = gResult.FirstOrDefault() ?? gResult.FirstOrDefault().IsAdmin
            //                                       //IsAdmin = true
            //                                   };

            //StoreAllFields(FieldStorage.Yes);
        }
    }
}