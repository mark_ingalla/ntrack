﻿using System;
using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;


namespace Subscription.Data.Indices
{
    public class AccountProductIndex : AbstractIndexCreationTask<Account, AccountProductIndex.Result>
    {
        public class Result
        {
            public string AccountId { get; set; }
            public string ProductId { get; set; }
            public DateTime? TerminatedOnUTC { get; set; }
        }

        public AccountProductIndex()
        {
            Map = accounts => from account in accounts
                              from subscription in account.Subscriptions
                              select new Result
                                         {
                                             AccountId = account.IdString,
                                             ProductId = subscription.ProductId,
                                             TerminatedOnUTC = subscription.TerminatedOnUTC
                                         };

            Reduce = results => from r in results
                                group r by new { r.AccountId }
                                    into g
                                    from gr in g
                                    select new Result
                                               {
                                                   AccountId = g.Key.AccountId,
                                                   ProductId = gr.ProductId,
                                                   TerminatedOnUTC = gr.TerminatedOnUTC
                                               };

            Index(p => p.TerminatedOnUTC, FieldIndexing.NotAnalyzed);

            Store(x => x.AccountId, FieldStorage.Yes);
            Store(x => x.ProductId, FieldStorage.Yes);
        }
    }
}
