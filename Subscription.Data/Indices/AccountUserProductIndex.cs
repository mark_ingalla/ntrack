using System;
using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace Subscription.Data.Indices
{
    public class AccountUserProductIndex : AbstractIndexCreationTask<User, AccountUserProductIndex.Result>
    {
        public class Result
        {
            public string UserId { get; set; }
            public Guid ProductId { get; set; }
        }

        public AccountUserProductIndex()
        {
            Map = users => from user in users
                              from permission in user.Permissions                          
                              select new
                                         {
                                             UserId = user.Id.ToString(),
                                             ProductId = permission.Key
                                         };

            Reduce = results => from r in results
                                group r by r.UserId 
                                    into g
                                    select new
                                               {
                                                   UserId = g.Key,
                                                   ProductId = g.Select(p => p.ProductId).FirstOrDefault()
                                               };
                                

            //Store(x=>x.UserId,FieldStorage.Yes);
            //Store(x=>x.ProductId,FieldStorage.Yes);
        }
    }
}