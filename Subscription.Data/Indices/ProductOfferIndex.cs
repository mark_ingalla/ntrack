﻿using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;
using System;
using System.Linq;

namespace Subscription.Data.Indices
{

    public class ProductOfferIndex : AbstractIndexCreationTask<Product, ProductOfferIndex.Result>
    {
        public class Result
        {
            public string ProductId { get; set; }
            public string ProductName { get; set; }
            public Guid OfferKey { get; set; }
            public string ImageUrl { get; set; }
            public string OfferName { get; set; }
            public bool IsOfferActive { get; set; }
            public bool IsProductActive { get; set; }
            public decimal Price { get; set; }
        }

        public ProductOfferIndex()
        {
            Map = products => from product in products
                              from offer in product.Offers
                              select new
                              {
                                  ProductId = product.IdString,
                                  ProductName = product.Name,
                                  OfferKey = offer.Key,
                                  OfferName = offer.Name,
                                  ImageUrl = product.LargeImageUrl,
                                  IsOfferActive = offer.IsActive,
                                  IsProductActive = offer.IsActive,
                                  Price = offer.Price

                              };

            Reduce = results => from r in results
                                group r by r.OfferKey
                                    into gr
                                    from g in gr
                                    select new Result
                                    {
                                        ProductId = g.ProductId,
                                        ProductName = g.ProductName,
                                        OfferKey = g.OfferKey,
                                        OfferName = g.OfferName,
                                        ImageUrl = g.ImageUrl,
                                        IsOfferActive = g.IsOfferActive,
                                        IsProductActive = g.IsProductActive,
                                        Price = g.Price
                                    };



            StoreAllFields(FieldStorage.Yes);
        }
    }
}
