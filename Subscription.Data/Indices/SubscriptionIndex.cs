﻿using System;
using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace Subscription.Data.Indices
{
    public class SubscriptionIndex : AbstractIndexCreationTask<Account, SubscriptionIndex.Result>
    {
        public class Result
        {
            public string AccountId { get; set; }
            public Guid SubscriptionKey { get; set; }

            public string ProductId { get; set; }
            public string ProductName { get; set; }
            public string ProductCode { get; set; }

            public Guid OfferKey { get; set; }
            public string OfferName { get; set; }

            public DateTime SubscribedOnUTC { get; set; }
            public DateTime? TerminatedOnUTC { get; set; }

            public decimal Amount { get; set; }
        }

        public SubscriptionIndex()
        {
            Map = accounts => from account in accounts
                              from subscription in account.Subscriptions
                              let product = LoadDocument<Product>(subscription.ProductId)
                              let offer = product.Offers.SingleOrDefault(x => x.Key == subscription.OfferKey)
                              select new
                              {
                                  AccountId = account.IdString,
                                  SubscriptionKey = subscription.Key,

                                  ProductId = subscription.ProductId,
                                  ProductName = product.Name,
                                  ProductCode = subscription.ProductCode,

                                  OfferKey = subscription.OfferKey,
                                  OfferName = offer.Name,

                                  SubscribedOnUTC = subscription.SubscribedOnUTC,
                                  TerminatedOnUTC = subscription.TerminatedOnUTC,

                                  Amount = subscription.Amount
                              };

            Reduce = results => from r in results
                                group r by r.SubscriptionKey
                                    into gr
                                    from g in gr
                                    select new Result
                                               {
                                                   AccountId = g.AccountId,
                                                   SubscriptionKey = g.SubscriptionKey,

                                                   ProductId = g.ProductId,
                                                   ProductName = g.ProductName,
                                                   ProductCode = g.ProductCode,

                                                   OfferKey = g.OfferKey,
                                                   OfferName = g.OfferName,

                                                   SubscribedOnUTC = g.SubscribedOnUTC,
                                                   TerminatedOnUTC = g.TerminatedOnUTC,

                                                   Amount = g.Amount
                                               };


            StoreAllFields(FieldStorage.Yes);
        }


    }

}
