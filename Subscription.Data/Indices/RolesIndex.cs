﻿using System;
using System.Collections.Generic;
using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;


namespace Subscription.Data.Indices
{
    public class RolesIndex:AbstractIndexCreationTask<Account,RolesIndex.Result>
    {
        public class Result
        {
            public string AccountId { get; set; }
            public Guid RoleKey { get; set; }
            public List<Guid> PermissionsKey { get; set; }
        }

        public RolesIndex()
        {
            Map = accounts => from account in accounts
                              from role in account.Roles
                              select new Result
                                         {
                                             AccountId = account.Id.ToString(),
                                             RoleKey = role.Key,
                                             PermissionsKey = role.Permissions.Select(p => p.Key).ToList()
                                         };

            Reduce = results => from r in results
                                group r by new { r.AccountId }
                                    into g
                                    from gr in g
                                    select new Result
                                               {
                                                   AccountId = g.Key.AccountId,
                                                   RoleKey = gr.RoleKey,
                                                   PermissionsKey = gr.PermissionsKey
                                               };

            Index(p => p.PermissionsKey, FieldIndexing.Analyzed);

            StoreAllFields(FieldStorage.Yes);
            //Store(x=>x.AccountId,FieldStorage.Yes);
            //Store(x=>x.ProductId,FieldStorage.Yes);           
        }
    }
}
