﻿using System;
using System.Linq;
using Raven.Client.Indexes;

namespace Subscription.Data.Indices
{
    public class SubscriptionFeatureIndex : AbstractIndexCreationTask<Account, SubscriptionFeatureIndex.Result>
    {
        public class Result
        {
            public string AccountId { get; set; }
            public Guid ProductId { get; set; }
            public DateTime SubscribedOnUTC { get; set; }
            public DateTime? TerminatedOnUTC { get; set; }
        }

        public SubscriptionFeatureIndex()
        {
            Map = accounts => from account in accounts
                              from subscription in account.Subscriptions
                              select new
                              {
                                  AccountId = account.IdString,
                                  ProductId = subscription.ProductId,
                                  SubscribedOnUTC = subscription.SubscribedOnUTC,
                                  TerminatedOnUTC = subscription.TerminatedOnUTC
                              };

            Reduce = results => from r in results
                                group r by new { }
                                    into gr
                                    from g in gr
                                    select new Result
                                               {
                                                   AccountId = g.AccountId,
                                                   ProductId = g.ProductId,
                                                   SubscribedOnUTC = g.SubscribedOnUTC,
                                                   TerminatedOnUTC = g.TerminatedOnUTC,
                                               };

        }
    }

}
