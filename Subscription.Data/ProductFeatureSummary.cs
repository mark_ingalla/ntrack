﻿namespace Subscription.Data
{
    public class ProductFeatureSummary
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Limit { get; set; }
        public bool IsIncluded { get; set; }
    }
}