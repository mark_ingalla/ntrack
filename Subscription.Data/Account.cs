﻿using System;
using System.Collections.Generic;
using nTrack.Data;

namespace Subscription.Data
{
    public class Account : nTrack.Data.Account
    {
        
        public List<AccountSubscription> Subscriptions { get; set; }

        public Guid PendingVerificationToken { get; set; }

        public string Ip { get; set; }

        public Account()
        {
            Subscriptions = new List<AccountSubscription>();
        }
    }
}