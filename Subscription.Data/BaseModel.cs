﻿using System;

namespace Subscription.Data
{
    public class BaseModel
    {
        public Guid Id { get; set; }
        public string IdString { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool IsDeleted { get; set; }
    }
}
