﻿using System;
using System.Collections.Generic;

namespace Subscription.Data
{
    public class AdminAccounts
    {
        /// <summary>
        /// Same as the AdminUserID
        /// </summary>
        public Guid Id
        {
            get
            { return AdminUserID; }
            set
            { AdminUserID = value; }
        }
        public Guid AdminUserID { get; set; }
        /// <summary>
        /// The accounts administered by the admin user.
        /// </summary>
        public List<AccountMinimal> Accounts { get; set; }


        public AdminAccounts()
        {
            Accounts = new List<AccountMinimal>();
        }
    }
}