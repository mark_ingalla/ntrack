﻿using System;

namespace Subscription.Data
{
    public class PasswordResetToken
    {
        public Guid Token { get; set; }

        public Guid Id
        {
            get { return Token; }
            set { Token = value; }
        }
    }
}