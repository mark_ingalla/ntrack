﻿
namespace Subscription.Data
{
    public class ProductPermission
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}