﻿
using System;

namespace Subscription.Data
{
    public class AccountSubscription
    {
        public Guid Key { get; set; }
        public string ProductId { get; set; }
        public string ProductCode { get; set; }
        public Guid OfferKey { get; set; }
        public decimal Amount { get; set; }
        public int OfferQTY { get; set; }
        public decimal Total { get { return Amount*OfferQTY; } }

        public DateTime SubscribedOnUTC { get; set; }
        public DateTime? TerminatedOnUTC { get; set; }
    }
}