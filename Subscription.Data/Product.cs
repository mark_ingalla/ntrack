﻿using System.Collections.Generic;
using nTrack.Data;

namespace Subscription.Data
{
    public class Product : BaseModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string LargeImageUrl { get; set; }
        public string AccessUrl { get; set; }
        public string DiscontinueReason { get; set; }
        public decimal BasePrice { get; set; }

        public List<ProductFeature> Features { get; set; }
        public List<ProductOffer> Offers { get; set; }

        public bool IsLocked { get; set; }

        public Product()
        {
            Features = new List<ProductFeature>();
            Offers = new List<ProductOffer>();
        }
    }
}