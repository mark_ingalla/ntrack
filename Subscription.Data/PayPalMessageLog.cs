﻿using System;

namespace Subscription.Data
{
    public class PayPalMessageLog
    {
        // Scalar properties
        public int ID { get; set; }
        public DateTime Time { get; set; }
        public string NameValuePairs { get; set; }

        private string _messageDirection;
        public string MessageDirection
        {
            get
            {
                return _messageDirection;
            }
            set
            {
                if (value == "Request" || value == "Response")
                    _messageDirection = value;
                else
                    throw new Exception("MessageDirection should be either 'Request' or 'Response'.");
            }
        }

        // Foreign keys and navigation properties.
        public int InvoiceNumber { get; set; }
    }
}
