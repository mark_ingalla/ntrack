﻿using System;

namespace Subscription.Data
{
    public class LoginCredentials
    {
        public string Id { get; set; }
        public Guid   UserID       { get; set; }
        public string Email        { get; set; }
        public string PasswordHash { get; set; }
    }
}